﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic


' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://WSRest.APP.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WSRest
    Inherits System.Web.Services.WebService
    <WebMethod()> _
    Public Function fnCargarIdUsuarios() As Data.DataSet
        Dim List As New List(Of String)
        Dim dt As New Data.DataSet
        Dim cm As New SqlCommand
        Dim Da As New SqlClient.SqlDataAdapter
        cm.CommandText = "Select Id_Usuario,Nombre,Iniciales,Estado From Usuarios where Estado = 0 ORDER BY Nombre"
        clsEnlace.spCargarDataSet(cm, dt, "Seguridad")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function
    <WebMethod()> _
    Public Function fnDameConexion() As String
        Return clsEnlace.fnStrConexion("Restaurante")

    End Function
    <WebMethod()> _
    Function fnLogin(_IdUsuario As String, _Clave As String) As String
        Try

            Dim dts As New Data.DataTable
            Dim dtUsuario As New Data.DataTable
            Dim sqlcommand As New SqlCommand
            sqlcommand.CommandText = "SELECT DISTINCT Nombre, Clave_Entrada,Perfil, Iniciales,Id_Usuario,Estado FROM dbo.Usuarios AS a WHERE (Id_Usuario IN (SELECT dbo.Perfil_x_Usuario.Id_Usuario FROM dbo.Perfil_x_Modulo INNER JOIN dbo.Modulos ON dbo.Perfil_x_Modulo.Id_Modulo = dbo.Modulos.Id_modulo INNER JOIN dbo.Perfil_x_Usuario ON dbo.Perfil_x_Modulo.Id_Perfil = dbo.Perfil_x_Usuario.Id_Perfil WHERE (dbo.Modulos.Grupo = 'A&B'))) AND (Estado = 0) ORDER BY Nombre"
            clsEnlace.spCargarDatos(sqlcommand, dts, "Seguridad")

            If dts.Rows.Count = 0 Then
                Return "Error." & clsEnlace.resultado
            End If
            If clsEnlace.fnValidarLogin(dts, _IdUsuario, _Clave) Then
                sqlcommand.CommandText = "Select e.Id_Perfil from Perfil_x_Usuario AS e WHERE e.Id_Usuario = @IdUsu"
                sqlcommand.Parameters.AddWithValue("@IdUsu", _IdUsuario)
                clsEnlace.spCargarDatos(sqlcommand, dtUsuario, "Seguridad")
                If dtUsuario.Rows.Count > 0 Then
                    Return "Bienvenido!"
                Else
                    Return "Este usuario no posee perfil de acceso. Agreguele un perfil en el módulo de seguridad."

                End If
            Else

                Return "Usuario o Contraseña incorrecta"

            End If
        Catch ex As Exception
            Return "Error"
        End Try
    End Function
    <WebMethod()> _
    Public Function fnCargarSalones() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cm As New SqlCommand
        cm.CommandText = "Select id,nombre_grupo from Grupos_Mesas"
        clsEnlace.spCargarDataSet(cm, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarMesas(_IdGrupoMesa As Integer) As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "Select id,nombre_mesa,posicion,activa, id_Seccion from Mesas where id_Grupomesa= @id_Grupomesa ORDER BY nombre_mesa "
        cmd.Parameters.AddWithValue("@id_Grupomesa", _IdGrupoMesa)
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarCategoria() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "Select Id,Nombre,Color,PosicionApp from Categorias_Menu ORDER BY PosicionApp"
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function
    <WebMethod()> _
    Public Function fnCargarMenus(_IdCategoria As Integer) As Data.DataSet
        Dim dtMenu As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT Id_Menu,Nombre_Menu,Id_Categoria,Precio,Color,PosicionApp FROM vs_categoriaMenu where Id_Categoria = @IdCategoria ORDER BY PosicionApp"
        cmd.Parameters.AddWithValue("@IdCategoria", _IdCategoria)
        clsEnlace.spCargarDataSet(cmd, dtMenu, "Restaurante")
        If dtMenu.Tables.Count > 0 Then
            Return dtMenu
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarMenuTodos() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT Id_Menu,Nombre_Menu,Id_Categoria,Precio,Color,Modificador_Forzado,Numero_Acompañamientos,ImpVenta,ImpServ,Impresora,PosicionApp FROM vs_categoriaMenu ORDER BY PosicionApp"
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarModificadores() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT id,nombre,MOD_AGREGADO.posicion,precio,MOD_AGREGADO.IdCategoriaMenu FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion"
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarTodosModificadores() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT id,nombre,posicion,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion"
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnCargarAcompanamientos() As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT A.Id, A.Nombre, A.Costo_Real, C.IdAgregado, C.IdCategoriaMenu FROM dbo.Acompañamientos_Menu AS A INNER JOIN dbo.Categoria_Agregado AS C ON A.Id = C.IdAgregado WHERE (C.TipoAgregado = 'ACO')"
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function
    <WebMethod()> _
    Public Function fnCargarComandas(_IdMesa As Integer) As Data.DataSet
        Dim dt As New Data.DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT C.cProducto, C.cCodigo, C.cCantidad, C.cDescripcion, C.cPrecio, C.cMesa, C.Estado, ISNULL(M.ImpVenta, 0) AS ImpVenta, ISNULL(M.ImpServ, 0)  AS ImpServ, C.Impresora, C.id, C.Tipo_Plato FROM dbo.ComandaTemporal AS C LEFT OUTER JOIN  dbo.Menu_Restaurante AS M ON C.cProducto = M.Id_Menu WHERE (C.Estado = 'Activo') AND (C.cMesa=@IdMesa) ORDER BY C.id "
        cmd.Parameters.AddWithValue("@IdMesa", _IdMesa)
        clsEnlace.spCargarDataSet(cmd, dt, "Restaurante")
        If dt.Tables.Count > 0 Then
            Return dt
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function fnInsertarComandasProcedimiento(_Producto As String, _cDescripcion As String, _Descripcion As String, _cPrecio As String, _cCodigo As String, _
                                                    _cMesa As String, _cImpreso As String, _Impresora As String, _cont As String, _tipo As String, _Cedula As String, _
                                                    _Hab As String, _primero As String, _separada As String, _Express As String, _cCantidad As String) As String
        Dim dt As New DataTable
        Dim cmd As New SqlCommand
        cmd.CommandText = "InsertarComandaMovil"
        cmd.Parameters.AddWithValue("@Producto", _Producto)
        cmd.Parameters.AddWithValue("@cDescripcion", _cCantidad)
        cmd.Parameters.AddWithValue("@cPrecio", _Descripcion)
        cmd.Parameters.AddWithValue("@cMesa", _cMesa)
        cmd.Parameters.AddWithValue("@cImpreso", _cImpreso)
        cmd.Parameters.AddWithValue("@Impresora", _Impresora)
        cmd.Parameters.AddWithValue("@cont", _cont)
        cmd.Parameters.AddWithValue("@tipo", _tipo)
        cmd.Parameters.AddWithValue("@Hab", _Hab)
        cmd.Parameters.AddWithValue("@primero", _primero)
        cmd.Parameters.AddWithValue("@separada", _separada)
        cmd.Parameters.AddWithValue("@cCantidad", _cCantidad)
        clsEnlace.spEjecutar(cmd, "Restaurante")
        Return clsEnlace.resultado
    End Function
    <WebMethod()> _
    Public Function fnTerminarComanda(_cProducto As String, _cDescripcion As String, _Descripcion As String, _cPrecio As String, _cCodigo As String, _
                                                    _cMesa As String, _cImpreso As String, _Impresora As String, _tipo As String, _Cedula As String, _
                                                   _cCantidad As String, _solicitud As String) As String
        If _Impresora.Equals("anyType{}") Then
            _Impresora = ""

        End If
        'TODO: Opción de preparar primero
        'TODO: Cuentas separadas
        Dim cmd As New SqlCommand
        cmd.CommandText = "EXECUTE [Restaurante].[dbo].[ActualizaComandaMovil]   @cProducto  ,@cDescripcion  ,@cPrecio  ,@cCodigo  ,@cMesa  ,@cImpreso  ,@Impresora  ,@cont  ," & _
            "@tipo  ,@Cedula  ,@Hab  ,@primero  ,@separada  ,@Express  ,@cCantidad, @solicitud"
        cmd.Parameters.AddWithValue("@cProducto", _cProducto)
        cmd.Parameters.AddWithValue("@cDescripcion", _Descripcion)
        cmd.Parameters.AddWithValue("@cPrecio", _cPrecio)
        cmd.Parameters.AddWithValue("@cCodigo", _cCodigo)
        cmd.Parameters.AddWithValue("@cMesa", _cMesa)
        cmd.Parameters.AddWithValue("@cImpreso", _cImpreso)
        cmd.Parameters.AddWithValue("@Impresora", _Impresora)
        cmd.Parameters.AddWithValue("@cont", 1)
        cmd.Parameters.AddWithValue("@tipo", _tipo)
        cmd.Parameters.AddWithValue("@Cedula", _Cedula)
        cmd.Parameters.AddWithValue("@Hab", "")
        cmd.Parameters.AddWithValue("@primero", 0)
        cmd.Parameters.AddWithValue("@separada", 0)
        cmd.Parameters.AddWithValue("@Express", 0)
        cmd.Parameters.AddWithValue("@cCantidad", _cCantidad)
        cmd.Parameters.AddWithValue("@solicitud", _solicitud)
        Return clsEnlace.spEjecutarConResultado(cmd, "Restaurante")

    End Function
    <WebMethod()> _
    Public Function fnComandaMesa(_id As String) As String
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT ISNULL(MAX(cCodigo), 0) AS Ultima FROM ComandaTemporal WHERE (cMesa = @Mesa) AND (Estado = 'Activo') "
        cmd.Parameters.AddWithValue("@Mesa", _id)
        Return clsEnlace.spEjecutarConResultado(cmd, "Restaurante").ToString
    End Function
    <WebMethod()> _
    Function fnNumeroSolicitud(ByVal cNumero As Integer) As Integer
        Dim dt As New DataTable
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT     ISNULL(MAX(solicitud), 0) AS UltimaSolicitud FROM ComandaTemporal WHERE Estado='Activo'and (cCodigo = " & cNumero & ")"
        clsEnlace.spCargarDatos(cmd, dt, "Restaurante")
        If dt.Rows.Count > 0 Then

            Return dt.Rows(0).Item(0) + 1
        Else
            Return 0
        End If

    End Function
    <WebMethod()> _
    Public Function fnQuitarLineaComanda(_id As String) As String
        'TODO: Opción de preparar primero

        Dim cmd As New SqlCommand
        cmd.CommandText = "UPDATE [Restaurante].[dbo].[ComandaTemporal] " & _
            " SET Estado = 'Inactivo' " & _
            " WHERE id = @id"
        cmd.Parameters.AddWithValue("@id", _id)
        clsEnlace.spEjecutar(cmd, "Restaurante")
        If clsEnlace.resultado.Equals("") Then
            Return "OK"
        Else
            Return "Error"
        End If
        Return ""

    End Function

    <WebMethod()> _
    Public Function fnActualizaMesa(_id As String, _activa As String) As String
        'TODO: Opción de preparar primero
        'TODO: Cuentas separadas
        Dim cmd As New SqlCommand
        cmd.CommandText = "UPDATE [Restaurante].[dbo].[Mesas] " & _
            " SET activa = '" & _activa & "' " & _
            " WHERE Id = @id"
        cmd.Parameters.AddWithValue("@id", _id)
        clsEnlace.spEjecutar(cmd, "Restaurante")
        If clsEnlace.resultado.Equals("") Then
            Return "OK"
        Else
            Return "Error"
        End If
        Return ""
    End Function
End Class