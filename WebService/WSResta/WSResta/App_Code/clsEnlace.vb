﻿Imports System.Data
Public Class clsEnlace
    Public modulo As String
    Public Shared resultado As String = "Resta"

    Public Shared Function fnValidarLogin(_dtUsuario As DataTable, _IdUsuario As String, _Clave As String) As Boolean
        Dim NombreUsuario As String = ""
        For i As Integer = 0 To _dtUsuario.Rows.Count - 1
            If _dtUsuario.Rows(i).Item("Id_Usuario").Equals(_IdUsuario) And _dtUsuario.Rows(i).Item("Clave_Entrada").Equals(_Clave) Then
                NombreUsuario = _dtUsuario.Rows(i).Item("Nombre")
                SaveSetting("SeeSOFT", "Restaurante", "LastUser", NombreUsuario)
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Sub spEjecutar(_sqlcommand As SqlClient.SqlCommand, ByRef _mod As String)
        resultado = ""
        Try

            Try
                _sqlcommand.Connection = New SqlClient.SqlConnection(fnStrConexion(_mod))
                _sqlcommand.Connection.Open()
                _sqlcommand.ExecuteNonQuery()

            Catch ex As SqlClient.SqlException
                resultado = ex.Message

            End Try

        Catch ex As Exception
            resultado = ""
        End Try
    End Sub
    Public Shared Function spEjecutarConResultado(_sqlcommand As SqlClient.SqlCommand, ByRef _mod As String) As String
        resultado = ""
        Try

            Try
                _sqlcommand.Connection = New SqlClient.SqlConnection(fnStrConexion(_mod))
                _sqlcommand.Connection.Open()
                resultado = _sqlcommand.ExecuteScalar()


            Catch ex As SqlClient.SqlException
                resultado = ex.Message

            End Try

        Catch ex As Exception
            resultado = ""
        End Try
        Return resultado

    End Function
    Public Shared Sub spCargarDataSet(_sqlcommand As SqlClient.SqlCommand, ByRef _dt As Data.DataSet, ByRef _modulo As String)
        Try
            Try
                _sqlcommand.Connection = New SqlClient.SqlConnection(fnStrConexion(_modulo))
                Dim Da As New SqlClient.SqlDataAdapter
                Da.SelectCommand = _sqlcommand
                _dt.Clear()
                Da.Fill(_dt)
            Catch ex As SqlClient.SqlException
                resultado = ex.Message
            End Try
        Catch ex As Exception
            resultado = ex.Message

        End Try
    End Sub
    Public Shared Sub spCargarDatos(_sqlcommand As SqlClient.SqlCommand, ByRef _dt As Data.DataTable, ByRef _modulo As String)
        Try
            Try
                _sqlcommand.Connection = New SqlClient.SqlConnection(fnStrConexion(_modulo))
                Dim Da As New SqlClient.SqlDataAdapter
                Da.SelectCommand = _sqlcommand
                _dt.Clear()
                Da.Fill(_dt)
            Catch ex As SqlClient.SqlException
                resultado = ex.Message
            End Try
        Catch ex As Exception
            resultado = ex.Message

        End Try
    End Sub

    Public Shared Sub spProblemaConexion()

    End Sub

    Public Shared Function fnStrConexion(_mod As String) As String
        Dim str As String = fnConf(_mod)
        Return str
    End Function

    Public Shared Function fnConf(_modulo As String) As String

        Dim conexion As String = fnConexion()
        conexion = conexion.Replace("Restaurante", _modulo)
        Return conexion
    End Function
  
    Public Shared Sub spConf(_valor As String, _nuevo As String)
        SaveSetting("TourCo", "conf", _valor, _nuevo)
    End Sub
    Private Shared Function fnConexion() As String
        Dim rootWebConfig As System.Configuration.Configuration
        rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot")
        Dim connString As System.Configuration.ConnectionStringSettings
        If (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count) Then
            connString = rootWebConfig.ConnectionStrings.ConnectionStrings("RestauranteConnectionString")
            If Not (Nothing = connString.ConnectionString) Then
                Return connString.ConnectionString
            End If
        End If
        Return ""
    End Function

End Class
