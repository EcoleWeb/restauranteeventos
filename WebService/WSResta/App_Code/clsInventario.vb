﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class clsInventario
    Private Function CalculaCostoReceta_Mejorado(ByVal codigo_receta As Integer) As Double

        Try
            Dim dt As New DataTable
            Dim cm As New SqlClient.SqlCommand
            cm.CommandText = "Select * From Recetas WHere ID = " & codigo_receta
            clsEnlace.spCargarDatos(cm, dt, "Restaurante")
            Dim porc As Double = dt.Rows(0).Item("Porciones")
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            cm.CommandText = "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta
            Dim dtDet As New DataTable

            clsEnlace.spCargarDatos(cm, dtDet, "Restaurante")
            For i As Integer = 0 To dt.Rows.Count - 1
                If dtDet.Rows(i).Item("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta_Mejorado(dtDet.Rows(i).Item("Codigo")) * dtDet.Rows(i).Item("Cantidad")
                Else
                    calculando_costo += (CalculaCostoArticulo_Mejorado(dtDet.Rows(i).Item("Codigo"), dtDet.Rows(i).Item("idBodegaDescarga"), dtDet.Rows(i).Item("Descripcion"), dtDet.Rows(i).Item("idreceta")) * dtDet.Rows(i).Item("Cantidad"))
                End If
            Next

            Dim especies As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("Especies")) / 100)

            Dim desaprovechamiento As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("aprovechamiento")) / 100)

            calculando_costo = (calculando_costo + especies + desaprovechamiento) '/ dt.Rows(0).Item("Porciones")

            Return calculando_costo / porc

        Catch ex As Exception
            Return 0


        End Try

    End Function
    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticulo_Mejorado(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double

            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            Dim dt As New DataTable
            Dim cm As New SqlClient.SqlCommand
            cm.CommandText = "Select CostoUnit FROM VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & Receta
            clsEnlace.spCargarDatos(cm, dt, "Restaurante")


            If costoXarticulo = 0 Then
                'Cambio por Diego lo deshabilite para que no salga donde los clientes
                'MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizará el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0
            End If
            Return costoXarticulo
            cnx.Close()
        Catch ex As Exception
            Return 0
        End Try
    End Function
End Class
