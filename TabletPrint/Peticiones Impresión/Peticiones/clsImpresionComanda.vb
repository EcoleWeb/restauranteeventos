﻿Public Class clsImpresionComanda

    Public rptComanda As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public rptComandaIzquierda As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Public rptComandaExpres As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    Sub inicializarComandas()
        Try

       
        If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
            rptComanda.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandas"))
            CrystalReportsConexion.LoadReportViewer(Nothing, rptComanda, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        Else
            rptComandaIzquierda.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandasIzq"))
            CrystalReportsConexion.LoadReportViewer(Nothing, rptComandaIzquierda, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        End If
        rptComandaExpres.Load(GetSetting("SeeSoft", "A&B Reports", "rptComandasexpress"))
            CrystalReportsConexion.LoadReportViewer(Nothing, rptComandaExpres, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Inicializando Comandas")
            End
        End Try
    End Sub

End Class
