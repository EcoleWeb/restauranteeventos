﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms.VisualStyles.VisualStyleElement.ListView

Public Class frmCliente

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Dim tiposIdCliente As New DataTable
    Dim clientes As New DataTable
    Dim clientesCorrres As New DataTable
    Dim correos As New List(Of CorreoControl)
    Dim selCorreos As New List(Of CorreoControl)
    Dim numCorreos As Integer = 0
    Dim enterPress As Boolean = False
    Dim correoTablaCliente As New DataTable
    Private m_fOkToUpdateAutoComplete As Boolean
    Private m_sLastSearchedFor As String = ""

    Public ID As Integer = 0

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conexionCliente.cargarDatos("SELECT [id_Tipo_Identificacion],[Desc_Tipo_Identificacion] FROM [dbo].[TB_CE_TIPO_IDENTIFICACION]", tiposIdCliente)
        tipoCliente.DataSource = tiposIdCliente
        tipoCliente.DisplayMember = "Desc_Tipo_Identificacion"
        tipoCliente.ValueMember = "id_Tipo_Identificacion"
        conexionCliente.cargarDatos("SELECT Cedula, Nombre FROM Cliente", clientes)

        cmbIdentificacion.DataSource = clientes
        cmbIdentificacion.DisplayMember = "Cedula"
        cmbIdentificacion.SelectedIndex = -1

        CargarTipoCliente()
        'txIdentiCliente.AutoCompleteCustomSource.Clear()
        '      txNombreCliente.AutoCompleteCustomSource.Clear()
        '      For Each fila As DataRow In clientes.Rows
        '          txIdentiCliente.AutoCompleteCustomSource.Add(fila.Item("Cedula"))
        '          txNombreCliente.AutoCompleteCustomSource.Add(fila.Item("Nombre"))
        '      Next

    End Sub

    Private Sub CargarTipoCliente()
        Try
            Dim dtTipoCliente As New DataTable
            Dim Consulta As String = "Select * from TipoCliente "
            dtTipoCliente.Clear()

            conexionCliente.cargarDatos(Consulta, dtTipoCliente)

            If dtTipoCliente.Rows.Count > 0 Then

                Me.cboTipoCliente.DataSource = dtTipoCliente
                Me.cboTipoCliente.DisplayMember = "descripcion"
                Me.cboTipoCliente.ValueMember = "Id"
            End If
        Catch ex As Exception
            MsgBox("No se pudo cargar los tipo de cliente. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Private Sub identiCliente_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        enterPress = True
        If e.KeyCode = Keys.Enter Then
            Buscar()
        End If
        If e.KeyCode = Keys.Delete Then
            ClearFields()
        End If
    End Sub
    Private Function CheckId(Optional doit As Boolean = True) As Boolean
        If Not doit Then
            Return True
        End If
        Dim limit As Integer = 0
        Dim limit2 As Integer = 0
        Dim _r As Boolean = False
        Select Case tipoCliente.SelectedIndex
            Case 0
                _r = cmbIdentificacion.Text.Length = 9
            Case 1
                _r = cmbIdentificacion.Text.Length = 10
            Case 2
                _r = cmbIdentificacion.Text.Length >= 11 And cmbIdentificacion.Text.Length <= 12
            Case 3
                _r = cmbIdentificacion.Text.Length = 10
        End Select
        If Not _r Then
            MsgBox("Ha ingresado un número de " & cmbIdentificacion.Text & " incorrecto")
        End If
        Return _r
    End Function
    Sub Buscar(Optional lookByCedula As Boolean = True)
        If CheckId(lookByCedula) Then
            Dim sqlComando As New SqlClient.SqlCommand
            Dim clienteBusqueda As New DataTable
            Dim search As String = "Search"
            correos.Clear()
            Dim query = "SELECT Id, Cedula ,Nombre, ISNULL(Email,'') as Ema, ISNULL(Telefono1,'') as Tel, Id_tipo_Cliente, NumDocExoneracion,FechaDocExoneracion,IdTipoCliente FROM Cliente where "
            If lookByCedula Then
                query += "Cedula = @Search"
                search = cmbIdentificacion.Text
            Else
                query += "Nombre like @Search"
                search = txtNombre.Text & "%"
            End If
            sqlComando.CommandText = query
            sqlComando.Parameters.AddWithValue("@Search", search)
            conexionCliente.cargarDatosConParametros(sqlComando, clienteBusqueda)
            If clienteBusqueda.Rows.Count > 0 Then
                ID = clienteBusqueda.Rows(0).Item("Id")
                txTelefonoCliente.Text = clienteBusqueda.Rows(0).Item("Tel")
                txtNombre.Text = clienteBusqueda.Rows(0).Item("Nombre")
                cmbIdentificacion.Text = clienteBusqueda.Rows(0).Item("Cedula")
                cboTipoCliente.SelectedValue = clienteBusqueda.Rows(0).Item("IdTipoCliente")
                txtDocExoneracion.Text = clienteBusqueda.Rows(0).Item("NumDocExoneracion")
                dtpFechaVencimientoExoneracion.Value = clienteBusqueda.Rows(0).Item("FechaDocExoneracion")
                ObtenerCorreoTablaClientes(ID)
                BuscarCorreo(ID)



            Else
                ID = 0
                'txTelefonoCliente.Clear()
                'txNombreCliente.Clear()
                mostrarCorreos()
            End If
            txTelefonoCliente.Enabled = True
            txtNombre.Enabled = True
        End If

    End Sub
    Sub BuscarCorreo(ByVal codCliente As Integer)
        Dim sqlComando As New SqlClient.SqlCommand
        Dim correoBusqueda As New DataTable
        sqlComando.CommandText = "SELECT codCliente, idCorreo, correoCliente, correoSeleccionado FROM CorreosCliente where codCliente = @idCliente"
        sqlComando.Parameters.AddWithValue("@idCliente", codCliente)
        conexionCliente.cargarDatosConParametros(sqlComando, correoBusqueda)
        pnCorreos.Controls.Clear()
        pnCorreos.Refresh()
        correos.Clear()
        If correoBusqueda.Rows.Count > 0 Then

            For Each fila As DataRow In correoBusqueda.Rows
                Dim isCheck As Boolean = fila.Item("correoSeleccionado")
                Dim correo As New CorreoControl(isCheck, fila.Item("correoCliente"), fila.Item("idCorreo"), correos)
                correos.Add(correo)
            Next
            numCorreos = 0
            'Else
            'ID = 0
        End If
        mostrarCorreos()

    End Sub

    Sub ObtenerCorreoTablaClientes(ByVal ID As Integer)
        Try

            Dim sqlComando As New SqlClient.SqlCommand
            'Dim correoBusqueda As New DataTable
            sqlComando.CommandText = "SELECT Email FROM Cliente where Id = @ID and Email != '' and Email is not null"
            sqlComando.Parameters.AddWithValue("@ID", ID)
            conexionCliente.cargarDatosConParametros(sqlComando, correoTablaCliente)

        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub

    Sub mostrarCorreos()
        pnCorreos.Controls.Clear()
        If correos.Count > 0 Then
            For Each correo As CorreoControl In correos
                pnCorreos.Controls.Add(correo)
            Next
        ElseIf correoTablaCliente.Rows.Count > 0 Then
            AgregarCorreoTablaCliente()
        Else
            AgregarCorreo()

        End If

    End Sub
    Private Sub LimpiarCampos()
        cmbIdentificacion.Text = ""
        txtNombre.Text = ""
        txTelefonoCliente.Text = ""
        selCorreos.Clear()
        correos.Clear()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Guardar()
    End Sub
    Sub Guardar(Optional returning As Boolean = False)
        If CheckId() Then
            Dim _r As DialogResult = DialogResult.Cancel
            If cmbIdentificacion.Text.Equals("") Or txtNombre.Text.Equals("") Or txTelefonoCliente.Text.Equals("") Or (txTelefonoCliente.Text.Length < 8) Then
                If txTelefonoCliente.Text.Equals("") Or (txTelefonoCliente.Text.Length < 8) Then
                    MsgBox("Por favor digite un numero valido para el cliente", MsgBoxStyle.Exclamation)
                End If
                If cmbIdentificacion.Text.Equals("") Then
                    MsgBox("Por favor no deje sin identificación al cliente", MsgBoxStyle.Exclamation)
                End If
                If txtNombre.Text.Equals("") Then
                    MsgBox("Por favor digite el nombre del cliente", MsgBoxStyle.Exclamation)
                End If
                Return
            Else
                Try
                    If IfDo(returning) Then
                        Dim correoPrincipal As String = ""
                        Dim correoSele As Integer = 0
                        Dim correoSeleccionado As Boolean = False
                        Dim correoDigitado As Boolean = False
                        For Each correo As CorreoControl In correos
                            If correo.chSelCorreo.Checked Then


                                If Not correo.txCorreoCliente.Text.Equals("") Then
                                    correoDigitado = True
                                End If

                                correoPrincipal = correo.txCorreoCliente.Text
                                correoSele = CInt(correo.chSelCorreo.Checked)
                                correoSeleccionado = True
                                'correoSele = correoSele * -1
                            End If
                        Next

                        If correoSeleccionado = False Or correoDigitado = False Then
                            MsgBox("Por favor seleccione o digite un correo")
                            Return
                        End If

                        If ID = 0 Then
                            Dim Ejecutar As New SqlClient.SqlCommand
                            Ejecutar.CommandText = "INSERT INTO Salaberry.[dbo].[tb_Clientes]  ([Nombre]  ,[Observaciones]  ,[Cedula]  ,[Contacto]   ,[Exento]   ,[Credito]  ,[Telefono_1]  ,[Telefono_2]   ,[Email]  ,[Direccion_1]  ,[Limite_Credito]  ,[Plazo_Credito]  ,[Comicion] ,[TipoPrecio]  ,[Id_tipo_cliente], [NumDocExoneracion], [FechaDocExoneracion], [IdTipoCliente]) " &
                        " VALUES  ('" & txtNombre.Text & "'  ,'','" & cmbIdentificacion.Text & "' ,'',0,'' ,'" & txTelefonoCliente.Text & "' ,'','" & correoPrincipal & "' ,0 ,0 ,0   ,0  ,0 ,'" & txtDocExoneracion.Text & "'," & tipoCliente.SelectedValue & ",@FechaDocExoneracion," & cboTipoCliente.SelectedValue & ") "
                            Ejecutar.Parameters.AddWithValue("@FechaDocExoneracion", dtpFechaVencimientoExoneracion.Value)
                            conexionCliente.guardarDatosConParametros(Ejecutar)
                            Dim dtIDFactura As New DataTable
                            conexionCliente.cargarDatos("Select TOP 1 ID FROM Cliente ORDER BY ID DESC", dtIDFactura)
                            If dtIDFactura.Rows.Count > 0 Then
                                ID = dtIDFactura.Rows(0).Item("ID")
                                GuardarCorreos()
                            End If
                            _r = DialogResult.OK

                        Else
                            Dim ejecutar As New SqlClient.SqlCommand
                            ejecutar.CommandText = "UPDATE Cliente SET NumDocExoneracion='" & txtDocExoneracion.Text & "', IdTipoCliente=" & cboTipoCliente.SelectedValue & ",FechaDocExoneracion=@FechaDocExoneracion , Nombre = '" & txtNombre.Text & "' ,Telefono1 = '" & txTelefonoCliente.Text & "' ,Email = '" & correoPrincipal & "' WHERE Id = " & ID
                            ejecutar.Parameters.AddWithValue("@FechaDocExoneracion", dtpFechaVencimientoExoneracion.Value)
                            conexionCliente.guardarDatosConParametros(ejecutar)
                            GuardarCorreos()
                            _r = DialogResult.OK
                        End If
                    End If

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            End If
            If returning Then
                DialogResult = _r
            End If
        End If
    End Sub

    Private Function IfDo(Optional returning As Boolean = False) As Boolean
        Return If(returning, True, MsgBox("Desea guardar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes)
    End Function

    Sub GuardarCorreos()
        For Each correo As CorreoControl In correos
            Dim correoSele As Integer = 0
            correoSele = CInt(correo.chSelCorreo.Checked)
            'correoSele = correoSele * -1
            If correo.txCorreoCliente.Text = "" Then
                If correo.id > 0 Then
                    Dim eliminarCorreo As String = "DELETE [dbo].[CorreosCliente] WHERE [idCorreo] = '" & correo.id & "' "
                    conexionCliente.guardarDatos(eliminarCorreo)
                End If
            Else
                If correo.id = 0 Then
                    Dim insertarCorreosNuevos As String = "INSERT INTO [dbo].[CorreosCliente]  ([codCliente]  ,[correoCliente]  ,[correoSeleccionado]) " &
                        "VALUES  ('" & ID & "','" & correo.txCorreoCliente.Text & "'  ," & correoSele & ")"
                    conexionCliente.guardarDatos(insertarCorreosNuevos)
                Else
                    Dim actualizarCorreosNuevos As String = "UPDATE [dbo].[CorreosCliente]  SET [correoCliente] = '" & correo.txCorreoCliente.Text & "' , [correoSeleccionado] = " & correoSele & " WHERE [idCorreo] = " & correo.id
                    conexionCliente.guardarDatos(actualizarCorreosNuevos)
                End If
            End If
        Next
        BuscarCorreo(ID)
    End Sub

    Private Sub FlowLayoutPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnCorreos.Paint

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Guardar(True)

    End Sub

    Private Sub txIdentiCliente_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If enterPress Then
        '	If txIdentiCliente.Text = "" Then
        '		ClearFields()
        '	Else
        '		Buscar()
        '	End If
        'Else
        '	enterPress = False
        'End If

    End Sub

    Private Sub ClearFields()
        txTelefonoCliente.Text = ""
        txTelefonoCliente.Enabled = False
        txtNombre.Text = ""
        cmbIdentificacion.Text = ""
        correos.Clear()
        numCorreos = 0
        Dim sizePanelAncho = pnCorreos.Width
        pnCorreos.Size = New Size(pnCorreos.Width, sizePanelAncho)
        pnCorreos.Controls.Clear()

    End Sub

    Private Sub AgregarCorreo()
        Try
            If pnCorreos.Controls.Count > 0 Then
                Dim lastCC As CorreoControl = pnCorreos.Controls(pnCorreos.Controls.Count - 1)
                If lastCC.txCorreoCliente.Text <> "" And cmbIdentificacion.Text <> "" And txtNombre.Text <> "" And txTelefonoCliente.Text <> "" Then
                    Dim camposCorreo As New CorreoControl(pnCorreos, correos)
                    correos.Add(camposCorreo)
                    pnCorreos.Controls.Add(camposCorreo)
                    Dim heigth As Integer = Size.Height + 30
                    Dim nSize As New Size(Size.Width, heigth)
                    If correos.Count <> numCorreos Then
                        If nSize.Height < MaximumSize.Height Then
                            Size = nSize
                            numCorreos = correos.Count
                        End If
                    End If
                End If
            ElseIf cmbIdentificacion.Text <> "" And txtNombre.Text <> "" And txTelefonoCliente.Text <> "" Then
                Dim camposCorreo As New CorreoControl(pnCorreos, correos)
                correos.Add(camposCorreo)
                pnCorreos.Controls.Add(camposCorreo)
                Dim heigth As Integer = Size.Height + 30
                Dim nSize As New Size(Size.Width, heigth)
                If correos.Count <> numCorreos Then
                    If nSize.Height < MaximumSize.Height Then
                        Size = nSize
                        numCorreos = correos.Count
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub

    Private Sub AgregarCorreoTablaCliente()
        Try
            If pnCorreos.Controls.Count > 0 Then
                Dim lastCC As CorreoControl = pnCorreos.Controls(pnCorreos.Controls.Count - 1)
                If lastCC.txCorreoCliente.Text <> "" And cmbIdentificacion.Text <> "" And txtNombre.Text <> "" And txTelefonoCliente.Text <> "" Then
                    Dim camposCorreo As New CorreoControl(pnCorreos, correos)
                    camposCorreo.txCorreoCliente.Text = correoTablaCliente.Rows(0).Item("Email")
                    correos.Add(camposCorreo)
                    pnCorreos.Controls.Add(camposCorreo)
                    Dim heigth As Integer = Size.Height + 30
                    Dim nSize As New Size(Size.Width, heigth)
                    If correos.Count <> numCorreos Then
                        If nSize.Height < MaximumSize.Height Then
                            Size = nSize
                            numCorreos = correos.Count
                        End If
                    End If
                End If
            ElseIf cmbIdentificacion.Text <> "" And txtNombre.Text <> "" And txTelefonoCliente.Text <> "" Then
                Dim camposCorreo As New CorreoControl(pnCorreos, correos)
                camposCorreo.txCorreoCliente.Text = correoTablaCliente.Rows(0).Item("Email")
                correos.Add(camposCorreo)
                pnCorreos.Controls.Add(camposCorreo)
                Dim heigth As Integer = Size.Height + 30
                Dim nSize As New Size(Size.Width, heigth)
                If correos.Count <> numCorreos Then
                    If nSize.Height < MaximumSize.Height Then
                        Size = nSize
                        numCorreos = correos.Count
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub

    Private Sub AgregarCorreo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AgregarCorreo()
    End Sub


    Private Sub txTelefonoCliente_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txTelefonoCliente.KeyPress
        e.Handled = True
        If (txTelefonoCliente.Text.Length = 0) And (e.KeyChar = "0") Then
            e.Handled = True
        ElseIf Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar) Then
            e.Handled = False

        End If
    End Sub

    Private Sub frmCliente_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        'DialogResult = DialogResult.Cancel
    End Sub

    Private Sub txNombreCliente_KeyDown(sender As Object, e As KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            Buscar(False)
        End If
        If e.KeyCode = Keys.Delete Then
            ClearFields()
        End If
    End Sub

    Private Sub txNombreCliente_Leave(sender As Object, e As EventArgs)
        'If txNombreCliente.Text = "" Then
        '	ClearFields()
        'Else
        '	Buscar(False)
        'End If
    End Sub

    Private Sub cmbIdentificacion_KeyDown(sender As Object, e As KeyEventArgs) Handles cmbIdentificacion.KeyDown
        enterPress = True
        If e.KeyCode = Keys.Enter Then
            Buscar()
        End If
        If e.KeyCode = Keys.Delete Then
            ClearFields()
        End If
    End Sub

    Private Sub cmbIdentificacion_Leave(sender As Object, e As EventArgs) Handles cmbIdentificacion.Leave
        If enterPress Then
            If cmbIdentificacion.Text = "" Then
                ClearFields()
            Else
                Buscar()
            End If
        Else
            enterPress = False
        End If
    End Sub

    Private Sub cmbNombre_KeyDown(sender As Object, e As KeyEventArgs)
        Try
            ' Catch up and down arrows, and don't change text box if these keys are pressed.
            If e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down Then
                m_fOkToUpdateAutoComplete = False
            Else
                m_fOkToUpdateAutoComplete = True
            End If
        Catch theException As Exception
            ' Do something with the error
        End Try

        If e.KeyCode = Keys.Enter Then
            Buscar(False)
        End If
        If e.KeyCode = Keys.Delete Then
            ClearFields()
        End If
    End Sub

    Private Sub cmbNombre_Leave(sender As Object, e As EventArgs)

        If txtNombre.Text = "" Then
            ClearFields()
        Else
            Buscar(False)
        End If
    End Sub

    Private Sub cboTipoCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoCliente.SelectedIndexChanged
        Try
            If cboTipoCliente.SelectedValue = 1 Then
                gbExoneracion.Visible = False
            Else
                gbExoneracion.Visible = True
            End If
        Catch ex As Exception
            gbExoneracion.Visible = False
        End Try
    End Sub

    Private Sub listNombres_Click(sender As Object, e As EventArgs) Handles listNombres.Click
        RecorrerListEstados()
    End Sub

    Sub RecorrerListEstados()
        Try
            Dim Valor As String = ""
            Dim pos As Integer = listNombres.SelectedIndex
            Valor = listNombres.SelectedItem.ToString()

            txtNombre.Text = Valor
            listNombres.Visible = False
            listNombres.Items.Clear()
            Buscar(False)
        Catch ex As Exception
            MsgBox("Error al cargar opción de estados de cuenta. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Sub CargarListaEstados(ByVal Filtrar As String)
        Try
            Dim rows As DataRow()
            listNombres.Items.Clear()
            rows = clientes.Select("Nombre like '%" & Filtrar & "%'")

            For a As Integer = 0 To rows.Length - 1
                listNombres.Items.Add(rows(a).ItemArray(1))
            Next

            listNombres.Visible = True
            listNombres.Height = listNombres.Items.Count * 20
        Catch ex As Exception
            MsgBox("Error al cargar lista de estados de cuenta. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged

        If txtNombre.Text.Length() > 3 Then
            CargarListaEstados(txtNombre.Text)
        Else
            listNombres.Height = 0
            listNombres.Visible = False
        End If

    End Sub


    Private Sub txtNombre_KeyDown(sender As Object, e As KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            Buscar(False)
        End If
        If e.KeyCode = Keys.Delete Then
            ClearFields()
        End If
    End Sub
End Class
