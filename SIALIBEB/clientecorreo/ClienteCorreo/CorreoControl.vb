﻿Imports System.Text.RegularExpressions

Public Class CorreoControl
    Dim panel As FlowLayoutPanel
    Dim lista As List(Of CorreoControl)
    Public id As Integer
    Sub New(ByRef isCheck As Boolean, ByRef txtCorreo As String, ByRef _id As Integer, ByRef listCorreo As List(Of CorreoControl))

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        id = _id
        lista = listCorreo
        chSelCorreo.Checked = isCheck
        chSelCorreo.Refresh()
        txCorreoCliente.Text = txtCorreo
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Sub New(ByRef _panel As FlowLayoutPanel, ByRef listCorreo As List(Of CorreoControl))
        InitializeComponent()
        id = 0
        lista = listCorreo
        panel = _panel
    End Sub

    Private Sub txCorreoCliente_TextChanged(sender As Object, e As EventArgs) Handles txCorreoCliente.TextChanged
        For Each cc As CorreoControl In lista
            If cc.txCorreoCliente.Text = txCorreoCliente.Text And Not cc.Equals(Me) Then
                MsgBox("ha ingresado un correo existente")
                txCorreoCliente.Text = ""
                Exit For
            End If
        Next
    End Sub

    Private Sub chSelCorreo_CheckedChanged(sender As Object, e As EventArgs) Handles chSelCorreo.CheckedChanged
        For Each cc As CorreoControl In lista
            If chSelCorreo.Checked And Not cc.Equals(Me) Then
                cc.chSelCorreo.Checked = False
            End If
        Next
    End Sub

    Private Sub txCorreoCliente_Leave(sender As Object, e As EventArgs) Handles txCorreoCliente.Leave
        If Not Regex.IsMatch(txCorreoCliente.Text, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$") And txCorreoCliente.Text <> "" Then
            MsgBox("Ha ingresado un texto que no corresponde a un correo electronico, por favor vuelva a escribirlo", MsgBoxStyle.OkOnly)
            txCorreoCliente.Text = ""
        End If
    End Sub
End Class
