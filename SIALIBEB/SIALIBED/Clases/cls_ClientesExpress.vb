Public Class cls_ClientesExpress

    Dim f As New cls_Broker()

    Public Id As String
    Public Telefono As String
    Public Nombre As String
    Public Direccion As String
	Public Comentarios As String

	Public Cedula As String
	Public Correo As String
	Public Transporte As Double

	Dim dt As DataTable

    'Constructor..
    Public Sub New()

        Me.Id = ""
        Me.Telefono = ""
        Me.Nombre = ""
        Me.Direccion = ""
        Me.Comentarios = ""
		Me.Cedula = ""
		Me.Correo = ""
		Me.Transporte = 0
		Me.obtenerTodosLosClientes()

    End Sub


    'Creamos un nuevo cliente express..
    Public Sub nuevoCliente()
		Me.f.fireSQLNoReturn("Insert into ClienteExpress(Telefono, Nombre, Direccion, Comentarios,cedula,correo, Transporte)" &
							" values ( '" & Me.Telefono & "','" & Me.Nombre & "','" & Me.Direccion & "','" & Me.Comentarios & "','" & Me.Cedula & "','" & Me.Correo & "','" & Me.Transporte & "')")

	End Sub

    'Actualizamos un cliente..
    Public Sub actualizarCliente()
		Me.f.fireSQLNoReturn("Update ClienteExpress set " &
		" Telefono = '" & Me.Telefono & "'," &
		" Nombre = '" & Me.Nombre & "'," &
		" Direccion = '" & Me.Direccion & "'," &
		" Comentarios = '" & Me.Comentarios & "', " &
		  " cedula = '" & Me.Cedula & "' ," &
			" correo = '" & Me.Correo & "' ," &
			" Transporte = '" & Me.Transporte & "' " &
		" where Id = " & Me.Id)
	End Sub

    'Borrar un cliente de express
    Public Sub borraCliente()
        Me.f.fireSQLNoReturn("Delete ClienteExpress where Id = " & Me.Id)
    End Sub


    'Obtenemos todos los clientes para una busqueda
    Public Sub obtenerTodosLosClientes()
		Me.dt = Me.f.fireSQL("Select Telefono, Nombre from ClienteExpress")
	End Sub

    'Obtenemos una lista de los clientes..
    Public Function BuscarCliente(ByRef _codigo As String) As DataTable
        Dim dv As New DataView(Me.dt, "Telefono like '" & _codigo & "%' or Nombre like '%" & _codigo & "%'", "Telefono", DataViewRowState.CurrentRows)
        Return dv.ToTable()
    End Function

    Public Sub obtenerCliente(ByRef _codigo As String)
        Dim temp As DataTable
		temp = Me.f.fireSQL("Select id, telefono, nombre, direccion, comentarios, cedula, correo, Transporte from ClienteExpress where telefono = '" & _codigo & "'")

		If Not IsDBNull(temp.Rows(0)("id")) Then Me.Id = temp.Rows(0)("id")
        If Not IsDBNull(temp.Rows(0)("telefono")) Then Me.Telefono = temp.Rows(0)("telefono")
        If Not IsDBNull(temp.Rows(0)("nombre")) Then Me.Nombre = temp.Rows(0)("nombre")
        If Not IsDBNull(temp.Rows(0)("direccion")) Then Me.Direccion = temp.Rows(0)("direccion")
		If Not IsDBNull(temp.Rows(0)("comentarios")) Then Me.Comentarios = temp.Rows(0)("comentarios")
		If Not IsDBNull(temp.Rows(0)("cedula")) Then Me.Cedula = temp.Rows(0)("cedula")
		If Not IsDBNull(temp.Rows(0)("correo")) Then Me.Correo = temp.Rows(0)("correo")
		If Not IsDBNull(temp.Rows(0)("Transporte")) Then Me.Transporte = temp.Rows(0)("Transporte")

	End Sub


    Public Sub obtenerClienteById(ByRef _codigo As String)
        Dim temp As DataTable
		temp = Me.f.fireSQL("Select id, telefono, nombre, direccion, comentarios, cedula, correo, Transporte from ClienteExpress where Id = '" & _codigo & "'")

		If Not IsDBNull(temp.Rows(0)("id")) Then Me.Id = temp.Rows(0)("id")
        If Not IsDBNull(temp.Rows(0)("telefono")) Then Me.Telefono = temp.Rows(0)("telefono")
        If Not IsDBNull(temp.Rows(0)("nombre")) Then Me.Nombre = temp.Rows(0)("nombre")
        If Not IsDBNull(temp.Rows(0)("direccion")) Then Me.Direccion = temp.Rows(0)("direccion")
		If Not IsDBNull(temp.Rows(0)("comentarios")) Then Me.Comentarios = temp.Rows(0)("comentarios")
		If Not IsDBNull(temp.Rows(0)("cedula")) Then Me.Cedula = temp.Rows(0)("cedula")
		If Not IsDBNull(temp.Rows(0)("correo")) Then Me.Correo = temp.Rows(0)("correo")
		If Not IsDBNull(temp.Rows(0)("Transporte")) Then Me.Transporte = temp.Rows(0)("Transporte")

	End Sub



End Class
