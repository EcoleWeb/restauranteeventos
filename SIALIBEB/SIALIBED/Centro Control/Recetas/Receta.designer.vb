<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Receta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Receta))
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtnombre = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPUnitario = New System.Windows.Forms.TextBox
        Me.txtInsumo = New System.Windows.Forms.TextBox
        Me.dtgInsumos = New System.Windows.Forms.DataGridView
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Unid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Insumo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Costo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PUnit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Dism = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.idBodegaDescarga = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Articulo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Bodega = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtCPorcion = New System.Windows.Forms.TextBox
        Me.txtCTotal = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtIdInsumo = New System.Windows.Forms.TextBox
        Me.nmPorciones = New System.Windows.Forms.NumericUpDown
        Me.txtCedula = New System.Windows.Forms.TextBox
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.txtIdReceta = New System.Windows.Forms.TextBox
        Me.txtCantidad = New System.Windows.Forms.TextBox
        Me.txtpresentacion = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBar2 = New System.Windows.Forms.ToolBar
        Me.txtUsuario = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterConversion = New System.Data.SqlClient.SqlDataAdapter
        Me.DataSetReceta1 = New SIALIBEB.DataSetReceta
        Me.Label11 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.TablaConversionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.ckReceta = New System.Windows.Forms.CheckBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtEspecies = New System.Windows.Forms.TextBox
        Me.lblEspecies = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.lblDesechos = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtSubTotal = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.TxtSubTotalTemp = New System.Windows.Forms.Label
        Me.TxtDisminuyeTemp = New System.Windows.Forms.Label
        Me.cbxBodega = New System.Windows.Forms.ComboBox
        Me.BodegaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProveeduriaDataSet2 = New SIALIBEB.ProveeduriaDataSet2
        Me.lblbodega = New System.Windows.Forms.Label
        Me.BodegaTableAdapter = New SIALIBEB.ProveeduriaDataSet2TableAdapters.BodegaTableAdapter
        Me.AdapterConfiguraciones = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DataSetRecetaImp1 = New SIALIBEB.DataSetRecetaImp
        Me.FillByToolStrip = New System.Windows.Forms.ToolStrip
        Me.FillByToolStripButton = New System.Windows.Forms.ToolStripButton
        CType(Me.dtgInsumos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nmPorciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetReceta1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TablaConversionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BodegaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProveeduriaDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetRecetaImp1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FillByToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'txtnombre
        '
        Me.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtnombre, "txtnombre")
        Me.txtnombre.Name = "txtnombre"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'txtPUnitario
        '
        Me.txtPUnitario.BackColor = System.Drawing.Color.White
        Me.txtPUnitario.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtPUnitario, "txtPUnitario")
        Me.txtPUnitario.Name = "txtPUnitario"
        Me.txtPUnitario.ReadOnly = True
        '
        'txtInsumo
        '
        Me.txtInsumo.BackColor = System.Drawing.Color.White
        Me.txtInsumo.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtInsumo, "txtInsumo")
        Me.txtInsumo.Name = "txtInsumo"
        Me.txtInsumo.ReadOnly = True
        '
        'dtgInsumos
        '
        Me.dtgInsumos.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgInsumos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.NullValue = "0"
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgInsumos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dtgInsumos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgInsumos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.Cantidad, Me.Unid, Me.Insumo, Me.Precio, Me.Costo, Me.PUnit, Me.Dism, Me.idBodegaDescarga, Me.Articulo, Me.Bodega})
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgInsumos.DefaultCellStyle = DataGridViewCellStyle13
        resources.ApplyResources(Me.dtgInsumos, "dtgInsumos")
        Me.dtgInsumos.Name = "dtgInsumos"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgInsumos.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        '
        'id
        '
        resources.ApplyResources(Me.id, "id")
        Me.id.Name = "id"
        '
        'Cantidad
        '
        resources.ApplyResources(Me.Cantidad, "Cantidad")
        Me.Cantidad.Name = "Cantidad"
        '
        'Unid
        '
        resources.ApplyResources(Me.Unid, "Unid")
        Me.Unid.Name = "Unid"
        '
        'Insumo
        '
        resources.ApplyResources(Me.Insumo, "Insumo")
        Me.Insumo.Name = "Insumo"
        '
        'Precio
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Precio.DefaultCellStyle = DataGridViewCellStyle9
        resources.ApplyResources(Me.Precio, "Precio")
        Me.Precio.Name = "Precio"
        '
        'Costo
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Costo.DefaultCellStyle = DataGridViewCellStyle10
        resources.ApplyResources(Me.Costo, "Costo")
        Me.Costo.Name = "Costo"
        '
        'PUnit
        '
        DataGridViewCellStyle11.Format = "#,#0.00"
        Me.PUnit.DefaultCellStyle = DataGridViewCellStyle11
        resources.ApplyResources(Me.PUnit, "PUnit")
        Me.PUnit.Name = "PUnit"
        '
        'Dism
        '
        DataGridViewCellStyle12.Format = "#,#0.0000"
        Me.Dism.DefaultCellStyle = DataGridViewCellStyle12
        resources.ApplyResources(Me.Dism, "Dism")
        Me.Dism.Name = "Dism"
        '
        'idBodegaDescarga
        '
        resources.ApplyResources(Me.idBodegaDescarga, "idBodegaDescarga")
        Me.idBodegaDescarga.Name = "idBodegaDescarga"
        '
        'Articulo
        '
        resources.ApplyResources(Me.Articulo, "Articulo")
        Me.Articulo.Name = "Articulo"
        '
        'Bodega
        '
        resources.ApplyResources(Me.Bodega, "Bodega")
        Me.Bodega.Name = "Bodega"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'txtCPorcion
        '
        Me.txtCPorcion.BackColor = System.Drawing.Color.White
        Me.txtCPorcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtCPorcion, "txtCPorcion")
        Me.txtCPorcion.Name = "txtCPorcion"
        Me.txtCPorcion.ReadOnly = True
        '
        'txtCTotal
        '
        Me.txtCTotal.BackColor = System.Drawing.Color.White
        Me.txtCTotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtCTotal, "txtCTotal")
        Me.txtCTotal.Name = "txtCTotal"
        Me.txtCTotal.ReadOnly = True
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Name = "Label12"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.ForeColor = System.Drawing.Color.Yellow
        Me.Label13.Name = "Label13"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Name = "Label14"
        '
        'txtIdInsumo
        '
        Me.txtIdInsumo.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtIdInsumo.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtIdInsumo, "txtIdInsumo")
        Me.txtIdInsumo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtIdInsumo.Name = "txtIdInsumo"
        Me.txtIdInsumo.ReadOnly = True
        '
        'nmPorciones
        '
        resources.ApplyResources(Me.nmPorciones, "nmPorciones")
        Me.nmPorciones.Name = "nmPorciones"
        '
        'txtCedula
        '
        resources.ApplyResources(Me.txtCedula, "txtCedula")
        Me.txtCedula.Name = "txtCedula"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(10, "")
        '
        'txtIdReceta
        '
        resources.ApplyResources(Me.txtIdReceta, "txtIdReceta")
        Me.txtIdReceta.Name = "txtIdReceta"
        Me.txtIdReceta.ReadOnly = True
        '
        'txtCantidad
        '
        Me.txtCantidad.BackColor = System.Drawing.Color.White
        Me.txtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtCantidad, "txtCantidad")
        Me.txtCantidad.Name = "txtCantidad"
        '
        'txtpresentacion
        '
        Me.txtpresentacion.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.txtpresentacion.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtpresentacion, "txtpresentacion")
        Me.txtpresentacion.Name = "txtpresentacion"
        Me.txtpresentacion.ReadOnly = True
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'ToolBarButton1
        '
        resources.ApplyResources(Me.ToolBarButton1, "ToolBarButton1")
        Me.ToolBarButton1.Name = "ToolBarButton1"
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBar2
        '
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarButton1, Me.ToolBarCerrar})
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'txtUsuario
        '
        Me.txtUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.txtUsuario, "txtUsuario")
        Me.txtUsuario.ForeColor = System.Drawing.Color.White
        Me.txtUsuario.Name = "txtUsuario"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.Name = "Label10"
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtClave, "txtClave")
        Me.txtClave.Name = "txtClave"
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT     NombreUnidad, ConvertirA, Multiplo, Estado" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         TablaConversi" & _
            "ones"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=OSCAR;Initial Catalog=Restaurante;Integrated Security=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterConversion
        '
        Me.AdapterConversion.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterConversion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TablaConversiones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NombreUnidad", "NombreUnidad"), New System.Data.Common.DataColumnMapping("ConvertirA", "ConvertirA"), New System.Data.Common.DataColumnMapping("Multiplo", "Multiplo"), New System.Data.Common.DataColumnMapping("Estado", "Estado")})})
        '
        'DataSetReceta1
        '
        Me.DataSetReceta1.DataSetName = "DataSetReceta"
        Me.DataSetReceta1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.TablaConversionesBindingSource
        Me.ComboBox1.DisplayMember = "ConvertirA"
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        resources.ApplyResources(Me.ComboBox1, "ComboBox1")
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.TabStop = False
        '
        'TablaConversionesBindingSource
        '
        Me.TablaConversionesBindingSource.DataMember = "TablaConversiones"
        Me.TablaConversionesBindingSource.DataSource = Me.DataSetReceta1
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.TextBox2, "TextBox2")
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'ckReceta
        '
        resources.ApplyResources(Me.ckReceta, "ckReceta")
        Me.ckReceta.Name = "ckReceta"
        Me.ckReceta.UseVisualStyleBackColor = True
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'txtEspecies
        '
        resources.ApplyResources(Me.txtEspecies, "txtEspecies")
        Me.txtEspecies.Name = "txtEspecies"
        '
        'lblEspecies
        '
        resources.ApplyResources(Me.lblEspecies, "lblEspecies")
        Me.lblEspecies.Name = "lblEspecies"
        '
        'TextBox3
        '
        resources.ApplyResources(Me.TextBox3, "TextBox3")
        Me.TextBox3.Name = "TextBox3"
        '
        'lblDesechos
        '
        resources.ApplyResources(Me.lblDesechos, "lblDesechos")
        Me.lblDesechos.Name = "lblDesechos"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.White
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtSubTotal, "txtSubTotal")
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.ReadOnly = True
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'TxtSubTotalTemp
        '
        Me.TxtSubTotalTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TxtSubTotalTemp, "TxtSubTotalTemp")
        Me.TxtSubTotalTemp.Name = "TxtSubTotalTemp"
        '
        'TxtDisminuyeTemp
        '
        Me.TxtDisminuyeTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TxtDisminuyeTemp, "TxtDisminuyeTemp")
        Me.TxtDisminuyeTemp.Name = "TxtDisminuyeTemp"
        '
        'cbxBodega
        '
        Me.cbxBodega.DataSource = Me.BodegaBindingSource
        Me.cbxBodega.DisplayMember = "Nombre"
        Me.cbxBodega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxBodega.FormattingEnabled = True
        resources.ApplyResources(Me.cbxBodega, "cbxBodega")
        Me.cbxBodega.Name = "cbxBodega"
        Me.cbxBodega.TabStop = False
        Me.cbxBodega.ValueMember = "IdBodega"
        '
        'BodegaBindingSource
        '
        Me.BodegaBindingSource.DataMember = "Bodega"
        Me.BodegaBindingSource.DataSource = Me.ProveeduriaDataSet2
        '
        'ProveeduriaDataSet2
        '
        Me.ProveeduriaDataSet2.DataSetName = "ProveeduriaDataSet2"
        Me.ProveeduriaDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblbodega
        '
        resources.ApplyResources(Me.lblbodega, "lblbodega")
        Me.lblbodega.Name = "lblbodega"
        '
        'BodegaTableAdapter
        '
        Me.BodegaTableAdapter.ClearBeforeFill = True
        '
        'AdapterConfiguraciones
        '
        Me.AdapterConfiguraciones.SelectCommand = Me.SqlCommand1
        Me.AdapterConfiguraciones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("Tel_01", "Tel_01"), New System.Data.Common.DataColumnMapping("Tel_02", "Tel_02"), New System.Data.Common.DataColumnMapping("Fax_01", "Fax_01"), New System.Data.Common.DataColumnMapping("Fax_02", "Fax_02"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Logo", "Logo")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "SELECT     Cedula, Empresa, Tel_01, Tel_02, Fax_01, Fax_02, Direccion, Logo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM" & _
            "         Configuraciones"
        Me.SqlCommand1.Connection = Me.SqlConnection1
        '
        'DataSetRecetaImp1
        '
        Me.DataSetRecetaImp1.DataSetName = "DataSetRecetaImp"
        Me.DataSetRecetaImp1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FillByToolStrip
        '
        Me.FillByToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FillByToolStripButton})
        resources.ApplyResources(Me.FillByToolStrip, "FillByToolStrip")
        Me.FillByToolStrip.Name = "FillByToolStrip"
        '
        'FillByToolStripButton
        '
        Me.FillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FillByToolStripButton.Name = "FillByToolStripButton"
        resources.ApplyResources(Me.FillByToolStripButton, "FillByToolStripButton")
        '
        'Receta
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.FillByToolStrip)
        Me.Controls.Add(Me.lblbodega)
        Me.Controls.Add(Me.cbxBodega)
        Me.Controls.Add(Me.TxtDisminuyeTemp)
        Me.Controls.Add(Me.TxtSubTotalTemp)
        Me.Controls.Add(Me.txtInsumo)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.lblDesechos)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.lblEspecies)
        Me.Controls.Add(Me.txtEspecies)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ckReceta)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtpresentacion)
        Me.Controls.Add(Me.txtCantidad)
        Me.Controls.Add(Me.txtIdReceta)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.nmPorciones)
        Me.Controls.Add(Me.txtIdInsumo)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtCPorcion)
        Me.Controls.Add(Me.txtCTotal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtPUnitario)
        Me.Controls.Add(Me.dtgInsumos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtnombre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "Receta"
        CType(Me.dtgInsumos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nmPorciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetReceta1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TablaConversionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BodegaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProveeduriaDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetRecetaImp1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FillByToolStrip.ResumeLayout(False)
        Me.FillByToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPUnitario As System.Windows.Forms.TextBox
    Friend WithEvents txtInsumo As System.Windows.Forms.TextBox
    Friend WithEvents dtgInsumos As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCPorcion As System.Windows.Forms.TextBox
    Friend WithEvents txtCTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtIdInsumo As System.Windows.Forms.TextBox
    Friend WithEvents nmPorciones As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtCedula As System.Windows.Forms.TextBox
    Friend WithEvents txtIdReceta As System.Windows.Forms.TextBox
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents txtpresentacion As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Friend WithEvents txtUsuario As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterConversion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DataSetReceta1 As SIALIBEB.DataSetReceta
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TablaConversionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ckReceta As System.Windows.Forms.CheckBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEspecies As System.Windows.Forms.TextBox
    Friend WithEvents lblEspecies As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents lblDesechos As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TxtSubTotalTemp As System.Windows.Forms.Label
    Friend WithEvents TxtDisminuyeTemp As System.Windows.Forms.Label
    Friend WithEvents cbxBodega As System.Windows.Forms.ComboBox
    Friend WithEvents lblbodega As System.Windows.Forms.Label
    Friend WithEvents ProveeduriaDataSet2 As SIALIBEB.ProveeduriaDataSet2
    Friend WithEvents BodegaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BodegaTableAdapter As SIALIBEB.ProveeduriaDataSet2TableAdapters.BodegaTableAdapter
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Unid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Insumo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Costo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dism As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idBodegaDescarga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Articulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Bodega As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdapterConfiguraciones As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSetRecetaImp1 As SIALIBEB.DataSetRecetaImp
    Friend WithEvents FillByToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents FillByToolStripButton As System.Windows.Forms.ToolStripButton
End Class
