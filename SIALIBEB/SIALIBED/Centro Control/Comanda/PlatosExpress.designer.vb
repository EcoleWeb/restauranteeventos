<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PlatosExpress
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PlatosExpress))
        Me.PanelMesas = New System.Windows.Forms.Panel()
        Me.txtSubTotal = New System.Windows.Forms.TextBox()
        Me.txtIServicio = New System.Windows.Forms.TextBox()
        Me.txtIVentas = New System.Windows.Forms.TextBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblEncabezado = New System.Windows.Forms.Label()
        Me.lstProductos = New System.Windows.Forms.ListView()
        Me.Codigo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Cantidad = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Precio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Comanda = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cProducto = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grpModifica = New System.Windows.Forms.GroupBox()
        Me.GroupBoxNombre = New System.Windows.Forms.GroupBox()
        Me.ButtonSalirNombre = New System.Windows.Forms.Button()
        Me.ButtonGuardarNombre = New System.Windows.Forms.Button()
        Me.TextBoxNombreCliente = New System.Windows.Forms.TextBox()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.ToolButtonCerrar = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonEliminar = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonReducir = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonCambiarPrecio = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonNombre = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtComanda = New System.Windows.Forms.RichTextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtTransporte = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtFilas = New System.Windows.Forms.Label()
        Me.TxtColumnas = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBoxComensal = New System.Windows.Forms.ComboBox()
        Me.ComboBoxHabitaciones = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtNumeroComanda = New System.Windows.Forms.Label()
        Me.LabelUsuario = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolButtonRegresar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonVolver = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton9 = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonModificarComanda = New System.Windows.Forms.ToolStripButton()
        Me.tlsCantidad = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonNotas = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonFacturar = New System.Windows.Forms.ToolStripButton()
        Me.ToolButtonTerminar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolButtonExpress = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonAplicarDescuento = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonCredito = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonAcumular = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton12 = New System.Windows.Forms.ToolStripButton()
        Me.LabelMesa = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.ToolStripLabelComanda = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabelCuenta = New System.Windows.Forms.ToolStripLabel()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.cmbCuentas = New System.Windows.Forms.ComboBox()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.lblSalonero = New System.Windows.Forms.Label()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.ButtonCambiaSalonero = New System.Windows.Forms.Button()
        Me.cmbMesas = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNombreCliente = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.pnParaLlevar = New System.Windows.Forms.Panel()
        Me.ToolStripButtonRegresar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonTerminar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSepararCuenta = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.grpModifica.SuspendLayout()
        Me.GroupBoxNombre.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.pnParaLlevar.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        resources.ApplyResources(Me.PanelMesas, "PanelMesas")
        Me.PanelMesas.BackColor = System.Drawing.Color.White
        Me.PanelMesas.ForeColor = System.Drawing.Color.Black
        Me.PanelMesas.Name = "PanelMesas"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.White
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtSubTotal, "txtSubTotal")
        Me.txtSubTotal.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.ReadOnly = True
        '
        'txtIServicio
        '
        Me.txtIServicio.BackColor = System.Drawing.Color.White
        Me.txtIServicio.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtIServicio, "txtIServicio")
        Me.txtIServicio.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtIServicio.Name = "txtIServicio"
        Me.txtIServicio.ReadOnly = True
        '
        'txtIVentas
        '
        Me.txtIVentas.BackColor = System.Drawing.Color.White
        Me.txtIVentas.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtIVentas, "txtIVentas")
        Me.txtIVentas.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtIVentas.Name = "txtIVentas"
        Me.txtIVentas.ReadOnly = True
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.White
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtTotal, "txtTotal")
        Me.txtTotal.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'lblEncabezado
        '
        resources.ApplyResources(Me.lblEncabezado, "lblEncabezado")
        Me.lblEncabezado.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblEncabezado.ForeColor = System.Drawing.Color.White
        Me.lblEncabezado.Name = "lblEncabezado"
        '
        'lstProductos
        '
        resources.ApplyResources(Me.lstProductos, "lstProductos")
        Me.lstProductos.BackColor = System.Drawing.Color.White
        Me.lstProductos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lstProductos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Codigo, Me.Cantidad, Me.Descripcion, Me.Precio, Me.Comanda, Me.cProducto})
        Me.lstProductos.HideSelection = False
        Me.lstProductos.Name = "lstProductos"
        Me.ToolTip1.SetToolTip(Me.lstProductos, resources.GetString("lstProductos.ToolTip"))
        Me.lstProductos.UseCompatibleStateImageBehavior = False
        '
        'Codigo
        '
        resources.ApplyResources(Me.Codigo, "Codigo")
        '
        'Cantidad
        '
        resources.ApplyResources(Me.Cantidad, "Cantidad")
        '
        'Descripcion
        '
        resources.ApplyResources(Me.Descripcion, "Descripcion")
        '
        'Precio
        '
        resources.ApplyResources(Me.Precio, "Precio")
        '
        'Comanda
        '
        resources.ApplyResources(Me.Comanda, "Comanda")
        '
        'cProducto
        '
        resources.ApplyResources(Me.cProducto, "cProducto")
        '
        'grpModifica
        '
        resources.ApplyResources(Me.grpModifica, "grpModifica")
        Me.grpModifica.BackColor = System.Drawing.Color.White
        Me.grpModifica.Controls.Add(Me.GroupBoxNombre)
        Me.grpModifica.Controls.Add(Me.ToolStrip2)
        Me.grpModifica.Controls.Add(Me.Label5)
        Me.grpModifica.Controls.Add(Me.lstProductos)
        Me.grpModifica.Name = "grpModifica"
        Me.grpModifica.TabStop = False
        '
        'GroupBoxNombre
        '
        Me.GroupBoxNombre.BackColor = System.Drawing.Color.OldLace
        resources.ApplyResources(Me.GroupBoxNombre, "GroupBoxNombre")
        Me.GroupBoxNombre.Controls.Add(Me.ButtonSalirNombre)
        Me.GroupBoxNombre.Controls.Add(Me.ButtonGuardarNombre)
        Me.GroupBoxNombre.Controls.Add(Me.TextBoxNombreCliente)
        Me.GroupBoxNombre.Name = "GroupBoxNombre"
        Me.GroupBoxNombre.TabStop = False
        '
        'ButtonSalirNombre
        '
        resources.ApplyResources(Me.ButtonSalirNombre, "ButtonSalirNombre")
        Me.ButtonSalirNombre.Name = "ButtonSalirNombre"
        Me.ButtonSalirNombre.UseVisualStyleBackColor = True
        '
        'ButtonGuardarNombre
        '
        resources.ApplyResources(Me.ButtonGuardarNombre, "ButtonGuardarNombre")
        Me.ButtonGuardarNombre.Name = "ButtonGuardarNombre"
        Me.ButtonGuardarNombre.UseVisualStyleBackColor = True
        '
        'TextBoxNombreCliente
        '
        Me.TextBoxNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextBoxNombreCliente, "TextBoxNombreCliente")
        Me.TextBoxNombreCliente.Name = "TextBoxNombreCliente"
        '
        'ToolStrip2
        '
        resources.ApplyResources(Me.ToolStrip2, "ToolStrip2")
        Me.ToolStrip2.ImageScalingSize = New System.Drawing.Size(48, 48)
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolButtonCerrar, Me.ToolButtonEliminar, Me.ToolButtonReducir, Me.ToolButtonCambiarPrecio, Me.ToolStripButtonNombre, Me.ToolStripButton6})
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        '
        'ToolButtonCerrar
        '
        Me.ToolButtonCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        resources.ApplyResources(Me.ToolButtonCerrar, "ToolButtonCerrar")
        Me.ToolButtonCerrar.Name = "ToolButtonCerrar"
        '
        'ToolButtonEliminar
        '
        Me.ToolButtonEliminar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolButtonEliminar.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
        resources.ApplyResources(Me.ToolButtonEliminar, "ToolButtonEliminar")
        Me.ToolButtonEliminar.Name = "ToolButtonEliminar"
        '
        'ToolButtonReducir
        '
        Me.ToolButtonReducir.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolButtonReducir.Image = Global.SIALIBEB.My.Resources.Resources.TL_hist
        resources.ApplyResources(Me.ToolButtonReducir, "ToolButtonReducir")
        Me.ToolButtonReducir.Name = "ToolButtonReducir"
        '
        'ToolButtonCambiarPrecio
        '
        Me.ToolButtonCambiarPrecio.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolButtonCambiarPrecio.Image = Global.SIALIBEB.My.Resources.Resources.facj
        resources.ApplyResources(Me.ToolButtonCambiarPrecio, "ToolButtonCambiarPrecio")
        Me.ToolButtonCambiarPrecio.Name = "ToolButtonCambiarPrecio"
        '
        'ToolStripButtonNombre
        '
        Me.ToolStripButtonNombre.Image = Global.SIALIBEB.My.Resources.Resources.Users_Folder
        resources.ApplyResources(Me.ToolStripButtonNombre, "ToolStripButtonNombre")
        Me.ToolStripButtonNombre.Name = "ToolStripButtonNombre"
        '
        'ToolStripButton6
        '
        resources.ApplyResources(Me.ToolStripButton6, "ToolStripButton6")
        Me.ToolStripButton6.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripButton6.Name = "ToolStripButton6"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Name = "Label5"
        '
        'txtComanda
        '
        resources.ApplyResources(Me.txtComanda, "txtComanda")
        Me.txtComanda.BackColor = System.Drawing.Color.Gainsboro
        Me.txtComanda.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtComanda.Name = "txtComanda"
        Me.txtComanda.ReadOnly = True
        '
        'TextBox1
        '
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.txtTransporte)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtDescuento)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtSubTotal)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.txtIServicio)
        Me.Panel1.Controls.Add(Me.txtIVentas)
        Me.Panel1.Controls.Add(Me.txtTotal)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Name = "Panel1"
        '
        'txtTransporte
        '
        Me.txtTransporte.BackColor = System.Drawing.Color.White
        Me.txtTransporte.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtTransporte, "txtTransporte")
        Me.txtTransporte.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtTransporte.Name = "txtTransporte"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.White
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtDescuento, "txtDescuento")
        Me.txtDescuento.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtDescuento.Name = "txtDescuento"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'txtFilas
        '
        resources.ApplyResources(Me.txtFilas, "txtFilas")
        Me.txtFilas.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtFilas.Name = "txtFilas"
        '
        'TxtColumnas
        '
        resources.ApplyResources(Me.TxtColumnas, "TxtColumnas")
        Me.TxtColumnas.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TxtColumnas.Name = "TxtColumnas"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label6.Name = "Label6"
        '
        'ComboBoxComensal
        '
        resources.ApplyResources(Me.ComboBoxComensal, "ComboBoxComensal")
        Me.ComboBoxComensal.FormatString = "N0"
        Me.ComboBoxComensal.FormattingEnabled = True
        Me.ComboBoxComensal.Items.AddRange(New Object() {resources.GetString("ComboBoxComensal.Items"), resources.GetString("ComboBoxComensal.Items1"), resources.GetString("ComboBoxComensal.Items2"), resources.GetString("ComboBoxComensal.Items3"), resources.GetString("ComboBoxComensal.Items4"), resources.GetString("ComboBoxComensal.Items5"), resources.GetString("ComboBoxComensal.Items6"), resources.GetString("ComboBoxComensal.Items7"), resources.GetString("ComboBoxComensal.Items8"), resources.GetString("ComboBoxComensal.Items9"), resources.GetString("ComboBoxComensal.Items10"), resources.GetString("ComboBoxComensal.Items11"), resources.GetString("ComboBoxComensal.Items12"), resources.GetString("ComboBoxComensal.Items13"), resources.GetString("ComboBoxComensal.Items14"), resources.GetString("ComboBoxComensal.Items15"), resources.GetString("ComboBoxComensal.Items16"), resources.GetString("ComboBoxComensal.Items17"), resources.GetString("ComboBoxComensal.Items18"), resources.GetString("ComboBoxComensal.Items19"), resources.GetString("ComboBoxComensal.Items20"), resources.GetString("ComboBoxComensal.Items21"), resources.GetString("ComboBoxComensal.Items22"), resources.GetString("ComboBoxComensal.Items23"), resources.GetString("ComboBoxComensal.Items24"), resources.GetString("ComboBoxComensal.Items25"), resources.GetString("ComboBoxComensal.Items26"), resources.GetString("ComboBoxComensal.Items27"), resources.GetString("ComboBoxComensal.Items28"), resources.GetString("ComboBoxComensal.Items29"), resources.GetString("ComboBoxComensal.Items30"), resources.GetString("ComboBoxComensal.Items31"), resources.GetString("ComboBoxComensal.Items32"), resources.GetString("ComboBoxComensal.Items33"), resources.GetString("ComboBoxComensal.Items34"), resources.GetString("ComboBoxComensal.Items35"), resources.GetString("ComboBoxComensal.Items36"), resources.GetString("ComboBoxComensal.Items37"), resources.GetString("ComboBoxComensal.Items38"), resources.GetString("ComboBoxComensal.Items39"), resources.GetString("ComboBoxComensal.Items40"), resources.GetString("ComboBoxComensal.Items41"), resources.GetString("ComboBoxComensal.Items42"), resources.GetString("ComboBoxComensal.Items43"), resources.GetString("ComboBoxComensal.Items44"), resources.GetString("ComboBoxComensal.Items45"), resources.GetString("ComboBoxComensal.Items46"), resources.GetString("ComboBoxComensal.Items47"), resources.GetString("ComboBoxComensal.Items48"), resources.GetString("ComboBoxComensal.Items49")})
        Me.ComboBoxComensal.Name = "ComboBoxComensal"
        '
        'ComboBoxHabitaciones
        '
        resources.ApplyResources(Me.ComboBoxHabitaciones, "ComboBoxHabitaciones")
        Me.ComboBoxHabitaciones.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComboBoxHabitaciones.FormattingEnabled = True
        Me.ComboBoxHabitaciones.Items.AddRange(New Object() {resources.GetString("ComboBoxHabitaciones.Items"), resources.GetString("ComboBoxHabitaciones.Items1"), resources.GetString("ComboBoxHabitaciones.Items2"), resources.GetString("ComboBoxHabitaciones.Items3"), resources.GetString("ComboBoxHabitaciones.Items4"), resources.GetString("ComboBoxHabitaciones.Items5"), resources.GetString("ComboBoxHabitaciones.Items6"), resources.GetString("ComboBoxHabitaciones.Items7"), resources.GetString("ComboBoxHabitaciones.Items8"), resources.GetString("ComboBoxHabitaciones.Items9"), resources.GetString("ComboBoxHabitaciones.Items10"), resources.GetString("ComboBoxHabitaciones.Items11"), resources.GetString("ComboBoxHabitaciones.Items12"), resources.GetString("ComboBoxHabitaciones.Items13"), resources.GetString("ComboBoxHabitaciones.Items14"), resources.GetString("ComboBoxHabitaciones.Items15"), resources.GetString("ComboBoxHabitaciones.Items16"), resources.GetString("ComboBoxHabitaciones.Items17"), resources.GetString("ComboBoxHabitaciones.Items18"), resources.GetString("ComboBoxHabitaciones.Items19"), resources.GetString("ComboBoxHabitaciones.Items20"), resources.GetString("ComboBoxHabitaciones.Items21"), resources.GetString("ComboBoxHabitaciones.Items22"), resources.GetString("ComboBoxHabitaciones.Items23"), resources.GetString("ComboBoxHabitaciones.Items24"), resources.GetString("ComboBoxHabitaciones.Items25"), resources.GetString("ComboBoxHabitaciones.Items26"), resources.GetString("ComboBoxHabitaciones.Items27"), resources.GetString("ComboBoxHabitaciones.Items28"), resources.GetString("ComboBoxHabitaciones.Items29"), resources.GetString("ComboBoxHabitaciones.Items30"), resources.GetString("ComboBoxHabitaciones.Items31"), resources.GetString("ComboBoxHabitaciones.Items32"), resources.GetString("ComboBoxHabitaciones.Items33"), resources.GetString("ComboBoxHabitaciones.Items34"), resources.GetString("ComboBoxHabitaciones.Items35"), resources.GetString("ComboBoxHabitaciones.Items36"), resources.GetString("ComboBoxHabitaciones.Items37"), resources.GetString("ComboBoxHabitaciones.Items38"), resources.GetString("ComboBoxHabitaciones.Items39"), resources.GetString("ComboBoxHabitaciones.Items40"), resources.GetString("ComboBoxHabitaciones.Items41"), resources.GetString("ComboBoxHabitaciones.Items42"), resources.GetString("ComboBoxHabitaciones.Items43"), resources.GetString("ComboBoxHabitaciones.Items44"), resources.GetString("ComboBoxHabitaciones.Items45"), resources.GetString("ComboBoxHabitaciones.Items46"), resources.GetString("ComboBoxHabitaciones.Items47"), resources.GetString("ComboBoxHabitaciones.Items48"), resources.GetString("ComboBoxHabitaciones.Items49")})
        Me.ComboBoxHabitaciones.Name = "ComboBoxHabitaciones"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label7.Name = "Label7"
        '
        'TxtNumeroComanda
        '
        resources.ApplyResources(Me.TxtNumeroComanda, "TxtNumeroComanda")
        Me.TxtNumeroComanda.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TxtNumeroComanda.ForeColor = System.Drawing.Color.Yellow
        Me.TxtNumeroComanda.Name = "TxtNumeroComanda"
        '
        'LabelUsuario
        '
        resources.ApplyResources(Me.LabelUsuario, "LabelUsuario")
        Me.LabelUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelUsuario.ForeColor = System.Drawing.Color.White
        Me.LabelUsuario.Name = "LabelUsuario"
        '
        'ToolStrip1
        '
        resources.ApplyResources(Me.ToolStrip1, "ToolStrip1")
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolButtonRegresar, Me.ToolStripButtonVolver, Me.ToolStripButton9, Me.ToolButtonModificarComanda, Me.tlsCantidad, Me.ToolButtonNotas, Me.ToolButtonFacturar, Me.ToolButtonTerminar, Me.ToolStripButton8, Me.ToolStripSeparator2, Me.ToolButtonExpress, Me.ToolStripButtonAplicarDescuento, Me.ToolStripButtonCredito, Me.ToolStripButtonAcumular, Me.ToolStripButton12})
        Me.ToolStrip1.Name = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        resources.ApplyResources(Me.ToolStripLabel1, "ToolStripLabel1")
        Me.ToolStripLabel1.ForeColor = System.Drawing.Color.Blue
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        '
        'ToolButtonRegresar
        '
        resources.ApplyResources(Me.ToolButtonRegresar, "ToolButtonRegresar")
        Me.ToolButtonRegresar.Name = "ToolButtonRegresar"
        '
        'ToolStripButtonVolver
        '
        resources.ApplyResources(Me.ToolStripButtonVolver, "ToolStripButtonVolver")
        Me.ToolStripButtonVolver.Name = "ToolStripButtonVolver"
        '
        'ToolStripButton9
        '
        Me.ToolStripButton9.Image = Global.SIALIBEB.My.Resources.Resources.recetas
        resources.ApplyResources(Me.ToolStripButton9, "ToolStripButton9")
        Me.ToolStripButton9.Name = "ToolStripButton9"
        '
        'ToolButtonModificarComanda
        '
        resources.ApplyResources(Me.ToolButtonModificarComanda, "ToolButtonModificarComanda")
        Me.ToolButtonModificarComanda.Name = "ToolButtonModificarComanda"
        '
        'tlsCantidad
        '
        resources.ApplyResources(Me.tlsCantidad, "tlsCantidad")
        Me.tlsCantidad.Image = Global.SIALIBEB.My.Resources.Resources.facj
        Me.tlsCantidad.Name = "tlsCantidad"
        '
        'ToolButtonNotas
        '
        resources.ApplyResources(Me.ToolButtonNotas, "ToolButtonNotas")
        Me.ToolButtonNotas.Name = "ToolButtonNotas"
        '
        'ToolButtonFacturar
        '
        Me.ToolButtonFacturar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        resources.ApplyResources(Me.ToolButtonFacturar, "ToolButtonFacturar")
        Me.ToolButtonFacturar.Name = "ToolButtonFacturar"
        '
        'ToolButtonTerminar
        '
        resources.ApplyResources(Me.ToolButtonTerminar, "ToolButtonTerminar")
        Me.ToolButtonTerminar.Name = "ToolButtonTerminar"
        '
        'ToolStripButton8
        '
        resources.ApplyResources(Me.ToolStripButton8, "ToolStripButton8")
        Me.ToolStripButton8.Image = Global.SIALIBEB.My.Resources.Resources.modComanda
        Me.ToolStripButton8.Name = "ToolStripButton8"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'ToolButtonExpress
        '
        resources.ApplyResources(Me.ToolButtonExpress, "ToolButtonExpress")
        Me.ToolButtonExpress.Image = Global.SIALIBEB.My.Resources.Resources.TL_proj
        Me.ToolButtonExpress.Name = "ToolButtonExpress"
        '
        'ToolStripButtonAplicarDescuento
        '
        resources.ApplyResources(Me.ToolStripButtonAplicarDescuento, "ToolStripButtonAplicarDescuento")
        Me.ToolStripButtonAplicarDescuento.Image = Global.SIALIBEB.My.Resources.Resources.Private_Folder
        Me.ToolStripButtonAplicarDescuento.Name = "ToolStripButtonAplicarDescuento"
        '
        'ToolStripButtonCredito
        '
        resources.ApplyResources(Me.ToolStripButtonCredito, "ToolStripButtonCredito")
        Me.ToolStripButtonCredito.BackColor = System.Drawing.Color.DarkGray
        Me.ToolStripButtonCredito.Image = Global.SIALIBEB.My.Resources.Resources.Users_Folder
        Me.ToolStripButtonCredito.Name = "ToolStripButtonCredito"
        '
        'ToolStripButtonAcumular
        '
        Me.ToolStripButtonAcumular.Image = Global.SIALIBEB.My.Resources.Resources.reporte
        resources.ApplyResources(Me.ToolStripButtonAcumular, "ToolStripButtonAcumular")
        Me.ToolStripButtonAcumular.Name = "ToolStripButtonAcumular"
        '
        'ToolStripButton12
        '
        Me.ToolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        resources.ApplyResources(Me.ToolStripButton12, "ToolStripButton12")
        Me.ToolStripButton12.Name = "ToolStripButton12"
        '
        'LabelMesa
        '
        resources.ApplyResources(Me.LabelMesa, "LabelMesa")
        Me.LabelMesa.BackColor = System.Drawing.Color.White
        Me.LabelMesa.ForeColor = System.Drawing.Color.Blue
        Me.LabelMesa.Name = "LabelMesa"
        '
        'ToolStripLabelComanda
        '
        resources.ApplyResources(Me.ToolStripLabelComanda, "ToolStripLabelComanda")
        Me.ToolStripLabelComanda.ForeColor = System.Drawing.Color.Blue
        Me.ToolStripLabelComanda.Name = "ToolStripLabelComanda"
        Me.ToolStripLabelComanda.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'ToolStripLabelCuenta
        '
        Me.ToolStripLabelCuenta.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripLabelCuenta.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        resources.ApplyResources(Me.ToolStripLabelCuenta, "ToolStripLabelCuenta")
        Me.ToolStripLabelCuenta.ForeColor = System.Drawing.Color.Blue
        Me.ToolStripLabelCuenta.LinkColor = System.Drawing.Color.Blue
        Me.ToolStripLabelCuenta.Name = "ToolStripLabelCuenta"
        Me.ToolStripLabelCuenta.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'lblCuenta
        '
        resources.ApplyResources(Me.lblCuenta, "lblCuenta")
        Me.lblCuenta.BackColor = System.Drawing.Color.White
        Me.lblCuenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblCuenta.Name = "lblCuenta"
        '
        'cmbCuentas
        '
        resources.ApplyResources(Me.cmbCuentas, "cmbCuentas")
        Me.cmbCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCuentas.FormattingEnabled = True
        Me.cmbCuentas.Name = "cmbCuentas"
        '
        'lblCantidad
        '
        resources.ApplyResources(Me.lblCantidad, "lblCantidad")
        Me.lblCantidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblCantidad.Name = "lblCantidad"
        '
        'lblSalonero
        '
        resources.ApplyResources(Me.lblSalonero, "lblSalonero")
        Me.lblSalonero.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblSalonero.ForeColor = System.Drawing.Color.White
        Me.lblSalonero.Name = "lblSalonero"
        '
        'ButtonCambiaSalonero
        '
        resources.ApplyResources(Me.ButtonCambiaSalonero, "ButtonCambiaSalonero")
        Me.ButtonCambiaSalonero.Name = "ButtonCambiaSalonero"
        Me.ButtonCambiaSalonero.UseVisualStyleBackColor = True
        '
        'cmbMesas
        '
        Me.cmbMesas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMesas.FormattingEnabled = True
        resources.ApplyResources(Me.cmbMesas, "cmbMesas")
        Me.cmbMesas.Name = "cmbMesas"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'txtNombreCliente
        '
        resources.ApplyResources(Me.txtNombreCliente, "txtNombreCliente")
        Me.txtNombreCliente.Name = "txtNombreCliente"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'pnParaLlevar
        '
        resources.ApplyResources(Me.pnParaLlevar, "pnParaLlevar")
        Me.pnParaLlevar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnParaLlevar.Controls.Add(Me.cmbMesas)
        Me.pnParaLlevar.Controls.Add(Me.Label10)
        Me.pnParaLlevar.Controls.Add(Me.Label11)
        Me.pnParaLlevar.Controls.Add(Me.txtNombreCliente)
        Me.pnParaLlevar.Name = "pnParaLlevar"
        '
        'ToolStripButtonRegresar
        '
        Me.ToolStripButtonRegresar.Image = Global.SIALIBEB.My.Resources.Resources.regresar
        resources.ApplyResources(Me.ToolStripButtonRegresar, "ToolStripButtonRegresar")
        Me.ToolStripButtonRegresar.Name = "ToolStripButtonRegresar"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = Global.SIALIBEB.My.Resources.Resources.modComanda
        resources.ApplyResources(Me.ToolStripButton1, "ToolStripButton1")
        Me.ToolStripButton1.Name = "ToolStripButton1"
        '
        'ToolStripButtonTerminar
        '
        Me.ToolStripButtonTerminar.Image = Global.SIALIBEB.My.Resources.Resources.AgregarArticulo
        resources.ApplyResources(Me.ToolStripButtonTerminar, "ToolStripButtonTerminar")
        Me.ToolStripButtonTerminar.Name = "ToolStripButtonTerminar"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Image = Global.SIALIBEB.My.Resources.Resources.greenshd
        resources.ApplyResources(Me.ToolStripButton4, "ToolStripButton4")
        Me.ToolStripButton4.Name = "ToolStripButton4"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.SIALIBEB.My.Resources.Resources.SepararC
        resources.ApplyResources(Me.ToolStripButton2, "ToolStripButton2")
        Me.ToolStripButton2.Name = "ToolStripButton2"
        '
        'ToolStripButton3
        '
        resources.ApplyResources(Me.ToolStripButton3, "ToolStripButton3")
        Me.ToolStripButton3.Image = Global.SIALIBEB.My.Resources.Resources.SepararC
        Me.ToolStripButton3.Name = "ToolStripButton3"
        '
        'ToolStripButtonSepararCuenta
        '
        Me.ToolStripButtonSepararCuenta.Image = Global.SIALIBEB.My.Resources.Resources.Factura2
        resources.ApplyResources(Me.ToolStripButtonSepararCuenta, "ToolStripButtonSepararCuenta")
        Me.ToolStripButtonSepararCuenta.Name = "ToolStripButtonSepararCuenta"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton5, "ToolStripButton5")
        Me.ToolStripButton5.Name = "ToolStripButton5"
        '
        'PlatosExpress
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Controls.Add(Me.pnParaLlevar)
        Me.Controls.Add(Me.ButtonCambiaSalonero)
        Me.Controls.Add(Me.lblSalonero)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.cmbCuentas)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.LabelMesa)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.txtComanda)
        Me.Controls.Add(Me.grpModifica)
        Me.Controls.Add(Me.LabelUsuario)
        Me.Controls.Add(Me.TxtNumeroComanda)
        Me.Controls.Add(Me.ComboBoxHabitaciones)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ComboBoxComensal)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TxtColumnas)
        Me.Controls.Add(Me.txtFilas)
        Me.Controls.Add(Me.lblEncabezado)
        Me.Controls.Add(Me.PanelMesas)
        Me.Controls.Add(Me.Panel1)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "PlatosExpress"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grpModifica.ResumeLayout(False)
        Me.grpModifica.PerformLayout()
        Me.GroupBoxNombre.ResumeLayout(False)
        Me.GroupBoxNombre.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.pnParaLlevar.ResumeLayout(False)
        Me.pnParaLlevar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelMesas As System.Windows.Forms.Panel
    Friend WithEvents txtSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtIServicio As System.Windows.Forms.TextBox
    Friend WithEvents txtIVentas As System.Windows.Forms.TextBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblEncabezado As System.Windows.Forms.Label
    Friend WithEvents lstProductos As System.Windows.Forms.ListView
    Friend WithEvents Codigo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Cantidad As System.Windows.Forms.ColumnHeader
    Friend WithEvents Descripcion As System.Windows.Forms.ColumnHeader
    Friend WithEvents Precio As System.Windows.Forms.ColumnHeader
    Friend WithEvents Comanda As System.Windows.Forms.ColumnHeader
    Friend WithEvents cProducto As System.Windows.Forms.ColumnHeader
    Friend WithEvents grpModifica As System.Windows.Forms.GroupBox
    Friend WithEvents txtComanda As System.Windows.Forms.RichTextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFilas As System.Windows.Forms.Label
    Friend WithEvents TxtColumnas As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxComensal As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxHabitaciones As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtNumeroComanda As System.Windows.Forms.Label
    Friend WithEvents LabelUsuario As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButtonRegresar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonSepararCuenta As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabelComanda As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabelCuenta As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButtonTerminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolButtonNotas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonRegresar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonModificarComanda As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolButtonTerminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonFacturar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolButtonReducir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolButtonCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents LabelMesa As System.Windows.Forms.Label
    Friend WithEvents ToolButtonCambiarPrecio As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cmbCuentas As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tlsCantidad As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents ToolStripButton9 As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblSalonero As System.Windows.Forms.Label
    Friend WithEvents ToolButtonExpress As System.Windows.Forms.ToolStripButton
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents ToolStripButtonAplicarDescuento As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonCredito As System.Windows.Forms.ToolStripButton
    Friend WithEvents ButtonCambiaSalonero As System.Windows.Forms.Button
    Friend WithEvents ToolStripButtonNombre As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBoxNombre As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonSalirNombre As System.Windows.Forms.Button
    Friend WithEvents ButtonGuardarNombre As System.Windows.Forms.Button
    Friend WithEvents TextBoxNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton12 As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtTransporte As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbMesas As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtNombreCliente As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents pnParaLlevar As Panel
    Friend WithEvents ToolStripButtonVolver As ToolStripButton
    Friend WithEvents ToolStripButtonAcumular As ToolStripButton
End Class
