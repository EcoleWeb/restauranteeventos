Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports System.Xml
Imports System.Globalization
Imports System.Threading
Imports System.ServiceProcess


Public Class ComandaMenu

#Region "variables"
    Dim vuelto1 As Double = 0
    Dim banderaDevuelPlatos As Boolean = False
    Dim bandeTipo As Boolean = False
    Public Shared CSeparada As Boolean = False
    Public NombreUsuario As String
    Public NombreCliente As String = ""
    Dim nUsu As String = ""
    Public Shared usuario As String 'psv 26072007
    Public facturar, idMenuActivo As Integer
    Public Shared idMesa, comenzales, numeroComanda, nombreMesa As String
    Public Shared HabitacionAsignada As String = ""
    Public cCuentas As Integer
    Public cArrayCuentas As New ArrayList
    Public Shared conectadobd As New SqlClient.SqlConnection
    Dim descuento As Double = 0
    Public cConexion As New ConexionR
    Dim cConexion1 As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim rs1 As SqlClient.SqlDataReader
    Dim iForzado, idGrupoT As String
    Dim tieneAcompa As String
    Dim impresoras As String = ""
    Dim prueba As Integer = 0
    Dim contador As Integer = 0
    Dim IdReceta(200) As Integer
    Dim disminuye As Double
    Dim Cargando As Boolean = True
    Public vuelto As String = ""
    Dim FPago As String = ""
    Dim Referencia As String = ""
    Public idGrupo As String
    Public iCategoria As Integer
    Public todo_comandado As Boolean = True
    Dim NoTerminar As Boolean = False
    Dim p_Tipo As Integer = 0
    Dim Id_Categoria As Integer = 0
    Dim current_Cant As Integer = 0
    Dim PreFactura1 As Object
    Public mesa As New cls_Mesas()
    Dim IdComandaTemporal As Integer
    Dim OpcionesEspeciales As Boolean = False
    Dim RecargarReportes As Boolean = False
    Dim CCOFactura As String = ""
    Public _ParaLlevar As Integer = 0
    Public _SoyExpress As Integer = 0
    Dim img As Image
    Dim DondEstoy As Array
    Public SoyAcumulada As Integer = 0
    Public PorcDescuentoAcumulada As Double = 0

#End Region

    Public Shared estadoAnte As Integer = 0

    Private Sub ComandaMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If todo_comandado = False Then

                If NoTerminar = False Then

                    spProcesarComanda()

                End If

            Else

                Dim cx As New Conexion

                cx.Conectar("Restaurante")

                cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)

                cx.DesConectar(cx.sQlconexion)

            End If

            cConexion.DesConectar(conectadobd)

            '-----------------------------------------------------------------------

            Me.mesa.obtenerMesa(idMesa)


            Me.mesa.cambiarNombreMesa(Me.txt_NomMesa.Text)

            spCorrigeEstadoMesa()

            If fac Then
                If Not OpcionFacturacion Then
                    Dim cx As New Conexion
                    cx.Conectar("Restaurante")

                    cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 2 WHERE Id = " & idMesa)
                    cx.DesConectar(cx.sQlconexion)
                End If
            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "FormClosing", ex.Message)
        End Try
    End Sub


    Sub spForzarConfiguracionRegionl()
        Try
            Dim oldDecimalSeparator As String = _
       Application.CurrentCulture.NumberFormat.NumberDecimalSeparator

            Dim forceDotCulture As CultureInfo
            forceDotCulture = Application.CurrentCulture.Clone()
            forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
            forceDotCulture.NumberFormat.NumberGroupSeparator = ","

            forceDotCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
            forceDotCulture.DateTimeFormat.AMDesignator = "a.m."
            forceDotCulture.DateTimeFormat.PMDesignator = "p.m."
            forceDotCulture.DateTimeFormat.ShortTimePattern = "hh:mm tt"

            Application.CurrentCulture = forceDotCulture
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spForzarConfiguracionRegionl", ex.Message)
        End Try
    End Sub
    Dim conta As Integer = 0


    Private Sub ComandaMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If gloCargandodtsGlobal Then
                Me.Close()
                Exit Sub
            End If

            If _ParaLlevar = 1 Then
                ToolButtonSepararCuenta.Visible = False

            End If
            glodtsGlobal.DatosExpress.Clear()

            Dim Conexion As New ConexionR
            iCategoria = 0
            txtComanda.Text = ""
            txtComanda.Clear()
            img = ToolButtonTerminar.Image
            If GetSetting("SeeSoft", "Restaurante", "OpcionesEspeciales").Equals("1") Then OpcionesEspeciales = True Else SaveSetting("SeeSoft", "Restaurante", "OpcionesEspeciales", "0")
            If GetSetting("SeeSoft", "Restaurante", "RecargarReportes").Equals("1") Then RecargarReportes = True Else SaveSetting("SeeSoft", "Restaurante", "RecargarReportes", "0")

            If GetSetting("SeeSoft", "Restaurante", "CCOFactura").Equals("") Then
                CCOFactura = ""
                SaveSetting("SeeSoft", "Restaurante", "CCOFactura", "")
            Else
                CCOFactura = GetSetting("SeeSoft", "Restaurante", "CCOFactura")
            End If

            If OpcionesEspeciales Then ToolButtonTerminar.Text = "Enviar"
            conectadobd = cConexion.Conectar("Restaurante")
            Dim impresion As String = GetSetting("SeeSoft", "Restaurante", "Impresion")
            If impresion.Equals("") Then
                SaveSetting("SeeSoft", "Restaurante", "Impresion", "0")
            End If
            If GetSetting("SeeSoft", "Restaurante", "noComanda").Equals("1") Then
                ToolButtonNotas.Enabled = False
                ToolButtonNotas.Visible = False

            End If

            If cCuentas > 0 Then
                lblCuenta.Visible = True
                cmbCuentas.Visible = True

                spCargarCuentas()

            Else
                cmbCuentas.Items.Add(0)
            End If

            Dim RowConf = (From i As dtsGlobal.confRow In glodtsGlobal.conf)

            For Each r As dtsGlobal.confRow In RowConf
                p_Tipo = r.tipo
                ToolStripButton9.Visible = usaGrupo
            Next


            Dim RowMesa = (From i As dtsGlobal.MesasRow In glodtsGlobal.Mesas Where i.Id = idMesa)
            For Each r As dtsGlobal.MesasRow In RowMesa
                comenzales = r.Numero_Asientos
                If GetSetting("SeeSoft", "Restaurante", "MesaExpress").Equals("1") Then
                    ToolStripButton6.Visible = IIf(r.Express = True, False, True)
                End If
            Next

            cmbCuentas.SelectedIndex = 0

            '  Me.mesa.obtenerMesa(idMesa)
            Me.txt_NomMesa.Text = Me.mesa.obtenerNombreMesa()

            ObtenerDimension()
            CrearCategoriaMenu()

            Dim RowPerfil = (From i As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where i.Id_Usuario = usuario And i.Modulo_Nombre_Interno = Name)
            For Each r As dtsGlobal.PermisosSeguridadRow In RowPerfil
                ToolButtonCambiarPrecio.Enabled = r.Accion_Opcion  'VSMA(usuario, Name, Secure.Others)  PUEDE cambiar Precio
                ToolButtonPreFacturar.Enabled = r.Accion_Opcion  'VSMA(usuario, Name, Secure.Others) 'Puede Pre-Facturar
                ToolButtonReducir.Enabled = r.Accion_Eliminacion 'VSMA(usuario, Name, Secure.Delete) 'Puede Reducir cantidades
                ToolStripButtonDescuento.Enabled = r.Accion_Opcion  'VSMA(usuario, Name, Secure.Others)
                Exit For
            Next


            Dim RowUsuario = (From i As dtsGlobal.UsuariosRow In glodtsGlobal.Usuarios Where i.Cedula = usuario)
            For Each r As dtsGlobal.UsuariosRow In RowUsuario
                nUsu = r.Nombre
            Next
            'nUsu = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre from Usuarios WITH (NOLOCK) where Cedula='" & usuario & "'")
            LabelUsuario.Text = nUsu
            Dim DataSet As New DataSet
            cConexion.GetDataSet(cConexion.Conectar("Restaurante"), "SELECT * FROM Cuentas_Todas WITH (NOLOCK)", DataSet, "Cuentas")
            If BindingContext(DataSet, "Cuentas").Count = 0 Then
                ComboBoxHabitaciones.Visible = False
                Label7.Visible = False
            End If
            ComboBoxHabitaciones.DrawMode = DrawMode.OwnerDrawVariable
            ComboBoxHabitaciones.DisplayMember = "Habitacion"
            ComboBoxHabitaciones.DataSource = DataSet.Tables("Cuentas")

            Cargando = False
            If numeroComanda <> "" Then
                Dim dtC As New DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT  Habitacion, comenzales FROM ComandasActivas WITH (NOLOCK) WHERE cMesa = " & idMesa, dtC)
                If dtC.Rows.Count > 0 Then
                    ComboBoxHabitaciones.Text = dtC.Rows(0).Item(0)
                    comenzales = dtC.Rows(0).Item(1)
                    ComboBoxComensal.Text = comenzales
                End If

                ' ComboBoxHabitaciones.Text = cConexion.SlqExecuteScalar(cConexion.Conectar("Restaurante"), "SELECT  Habitacion FROM ComandasActivas WITH (NOLOCK) WHERE cMesa = " & idMesa)
                ' comenzales = cConexion.SlqExecuteScalar(conectadobd, "Select DISTINCT comenzales from ComandasActivas WITH (NOLOCK) WHERE  Estado='Activo'and cMesa = " & idMesa)
                ' ComboBoxComensal.Text = comenzales

                Dim sql As New SqlClient.SqlCommand
                sql.CommandText = "select * from DatosExpress where id_comanda = " & numeroComanda
                cFunciones.spCargarDatos(sql, glodtsGlobal.DatosExpress)

            End If

            'Me.mesa.obtenerMesa(idMesa)
            ' Me.txt_NomMesa.Text = Me.mesa.obtenerNombreMesa()

            If GetSetting("SeeSoft", "Restaurante", "Acumular") = "1" Then
                Me.ToolStripButton11.Visible = True
            Else
                Me.ToolStripButton11.Visible = False
            End If

            If GetSetting("Seesoft", "Restaurante", "1/2") = "1" Then
                ToolStripButton12.Visible = True
            Else
                ToolStripButton12.Visible = False
            End If
            spForzarConfiguracionRegionl()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Load", ex.Message)
        End Try
    End Sub

    Private Sub CrearCategoriaMenu()
        Try
            ' If General.Exist(idMesa) Then Me.txt_NomMesa.Text = General.GetNombre(idMesa)
            If mesa.tem <> "" Then Me.txt_NomMesa.Text = mesa.nombre_mesa

			Dim ancho As Double
			Dim largo As Double
			ancho = Double.Parse(GetSetting("SeeSoft", "Restaurante", "anchoComanda"))
			largo = Double.Parse(GetSetting("SeeSoft", "Restaurante", "largoComanda"))
			Dim aButton As Button
            Dim i, ii, X, Y, n As Integer
            PanelMesas.Visible = True
            grpModifica.Visible = False
            PanelMesas.Controls.Clear()
            For i = 1 To txtFilas.Text '14 'ORIGINAL 7
                For ii = 1 To TxtColumnas.Text '4
                    aButton = New Button
                    aButton.Top = Y
                    aButton.Left = X
					aButton.Width = largo
					aButton.Height = ancho
					aButton.Name = n
                    aButton.Visible = False
                    aButton.Enabled = False
                    AddHandler aButton.Click, AddressOf Clickbuttons
                    Me.PanelMesas.Controls.Add(aButton)
					X = X + (largo + 2)
					n = n + 1
                Next
				Y = Y + (ancho + 2)
				X = 0
            Next

            Dim CATMENUSelect() As DataRow

            CATMENUSelect = glodtsGlobal.Categorias_Menu.Select()
            If idGrupo = "0" Then
                ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo = 'CATMENU',Precio_VentaA=0 FROM Categorias_Menu WITH (NOLOCK) Order By posicion")
                CATMENUSelect = glodtsGlobal.Categorias_Menu.Select("", "Posicion ASC")
                UbicarPosiciones(CATMENUSelect)
            Else
                'UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo = 'CATMENU',Precio_VentaA=0 FROM Categorias_Menu WITH (NOLOCK) where idGrupo = " & idGrupo & "Order By posicion")
                CATMENUSelect = glodtsGlobal.Categorias_Menu.Select("idGrupo = " & idGrupo)
                UbicarPosiciones(CATMENUSelect)
            End If

            PanelMesas.Visible = True

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CrearCategoriaMenu", ex.Message)
        End Try
    End Sub

    Private Sub UbicarPosiciones(ByVal DataRow As DataRow())
        Try
            Dim ruta As String
            Dim botones As Button
            Dim valida As Boolean = False
            Dim ponePIC As String = GetSetting("SEESOFT", "Restaurante", "sinPIC")
            ' Dim dt As New DataTable
            Dim sql As New SqlClient.SqlCommand
            Dim MiPrecio As Double = 0
            ' sql.CommandText = consulta
            ' cConexion.GetRecorset(conectadobd, consulta, rs)

            ' cFunciones.spCargarDatos(sql, dt, "", False)

            '  If dt.Rows.Count > 0 Then
            'For i As Integer = 0 To dt.Rows.Count - 1
            For i As Integer = 0 To DataRow.GetUpperBound(0)
                botones = New Button
                botones = PanelMesas.Controls.Item(i) ' rs(2) posicion del control
                botones.Text = DataRow(i).Item(1) ' Nombre
                Try
                    If _ParaLlevar = 1 Then
                        MiPrecio = DataRow(i).Item("Precio_VentaExpressA")
                    Else
                        MiPrecio = DataRow(i).Item("Precio_VentaA")
                    End If
                Catch ex As Exception
                    MiPrecio = DataRow(i).Item("Precio_VentaA")
                End Try

                botones.Name = botones.Name & "-" & DataRow(i).Item(0) & "-" & DataRow(i).Item("Tipo") & "-" & MiPrecio 'Codigo
                botones.Enabled = True

                ruta = DataRow(i).Item(3)
                If ruta.Trim.Length > 0 Then
                    Try
                        If Not ponePIC.Equals("1") Then
                            If Not ruta.Equals("") Then
                                Dim SourceImage As Bitmap
                                SourceImage = New Bitmap(ruta)
                                'botones.Image = New Drawing.Bitmap(SourceImage, 100, 60) ' 'SourceImage
                                botones.Image = SourceImage
                                botones.Text = DataRow(i).Item(1).ToString.Substring(0, IIf(DataRow(i).Item(1).ToString.Length > 16, 16, DataRow(i).Item(1).ToString.Length))
                                Me.ToolTip1.SetToolTip(botones, DataRow(i).Item(1))
                            End If

                        End If

                        'End If
                    Catch ex As Exception
                        valida = True
                    End Try
                End If
                botones.TextImageRelation = TextImageRelation.ImageAboveText    '.TextAboveImage
                botones.Visible = True
            Next


            '  Next

            ' End If


        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "UbicarPosiciones", ex.Message)
        End Try
    End Sub

    Public Sub limpiaDatos()

        Try
            Dim i As Integer
            Dim botones As Button

            For i = 0 To ((txtFilas.Text * TxtColumnas.Text) - 1)
                botones = New Button
                botones = PanelMesas.Controls.Item(i)
                botones.Text = ""
                botones.Name = ""
                botones.Image = Nothing
                botones.Visible = False
                botones.Enabled = False
            Next
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "limpiarDatos", ex.Message)
        End Try

    End Sub

    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim arrayNombre As Array
            Dim tieneForzado As String
            arrayNombre = sender.name.ToString.Split("-")
            DondEstoy = arrayNombre
            If Trim(iForzado) <> vbNullString Then
                arrayNombre(2) = iForzado
            End If
            ObtenerDimension()
            Select Case arrayNombre(2)

                Case "CATMENU" 'SELECCIONA UNA CATEGORIA DEL MENÚ, CARGANDO LOS PLATOS O DATOS DE ESA CATEGORIA
                    ObtenerDimensionCategoria(arrayNombre(1))
                    CrearCategoriaMenu()
                    limpiaDatos()
                    idGrupoT = arrayNombre(1)
                    Dim MenuSelect() As DataRow
                    MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & arrayNombre(1))
                    For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                        MenuSelect(i).Item("tipo") = "MENUREST"
                    Next
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    UbicarPosiciones(MenuSelect)
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA, Impresora FROM Menu_Restaurante WITH (NOLOCK) where deshabilitado = 0 and Id_Categoria =" & arrayNombre(1) & " ORDER BY Posicion")
                    'impresoras = cConexion.SlqExecuteScalar(conectadobd, "Select Impresora from Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & arrayNombre(1))
                    lblEncabezado.Text = "Menú"

                Case "MENUREST"
                    ' Dim dt As New DataTable
                    Dim sql As New SqlClient.SqlCommand

                    ToolButtonNotas.Visible = False
                    ToolStripButton11.Visible = False

                    cmbCuentas.Enabled = False
                    prueba += 1
                    ' sql.CommandText = "Select Modificador_Forzado, SinPrecio, SinPrecioImp , ImpVenta, ImpServ , Plato_Principal , Impresora , Id_Categoria , moneda , Precio_VentaD from Menu_Restaurante WITH (NOLOCK) where id_menu=" & arrayNombre(1)
                    'cFunciones.spCargarDatos(sql, dt)

                    Dim MenuSelec() As DataRow

                    MenuSelec = glodtsGlobal.Menu_Restaurante.Select("id_menu=" & arrayNombre(1))


                    tieneForzado = MenuSelec(0)("Modificador_Forzado")
                    Dim pedirPrecio As Boolean = MenuSelec(0)("SinPrecio")
                    Dim pedirPrecioImp As Boolean = MenuSelec(0)("SinPrecioImp")
					Dim ImpVenta As Boolean
					Dim ImpServ As Boolean
					Dim Plato_Principal As Boolean = IIf(OpcionesEspeciales, MenuSelec(0)("Plato_Principal"), False)

					If glodtsGlobal.Configuraciones(0).Imp_Venta > 0 Then
						ImpVenta = MenuSelec(0)("ImpVenta")
					End If
					If glodtsGlobal.Configuraciones(0).Imp_Servicio > 0 Then
						ImpServ = MenuSelec(0)("ImpServ")
					End If

					idMenuActivo = arrayNombre(1)
                    If Trim(numeroComanda) = vbNullString Then
                        numeroComanda = 0
                    End If
                    impresoras = MenuSelec(0)("Impresora")

                    Id_Categoria = MenuSelec(0)("Id_Categoria")
                    If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                    ' rs.Close()
                    Dim iPrimero As Integer
                    Dim PedidoExpress As Boolean = fnMesaExpres()
                    iPrimero = IIf(ToolStripButton6.BackColor = Color.Yellow, 1, 0)

                    lblCantidad.Text = "1"
                    If tlsCantidad.BackColor = Color.Yellow Then
                        Dim canPedido As Double
                        Dim rEmpleado0 As New registro
                        rEmpleado0.Text = "Cantidad de Pedidos"
                        rEmpleado0.ShowDialog()
                        Try
                            canPedido = CDbl(rEmpleado0.txtCodigo.Text)
                        Catch ex As Exception
                            canPedido = 1
                        End Try
                        rEmpleado0.Dispose()
                        lblCantidad.Text = canPedido
                    End If
                    current_Cant = lblCantidad.Text
                    Dim valorCambio, dmoneda As Double
                    dmoneda = MenuSelec(0)("moneda")
                    valorCambio = cConexion.SlqExecuteScalar(conectadobd, "select  (select valorVenta from moneda where codMoneda=" & dmoneda & ") / (select m.valorVenta from moneda as m, hotel.dbo.configuraciones as hc where m.codMoneda = hc.monedaRestaurante)")

                    Dim precio As Double = arrayNombre(3)
                    If precio = 0 Then
                        If pedirPrecio Then
                            If VSMA(usuario, Name, Secure.Others) Then
                                Dim solicitoPrecio As New registro
                                solicitoPrecio.Text = "Digite el precio..."
                                solicitoPrecio.ShowDialog()
                                Try
                                    precio = (CDbl(solicitoPrecio.txtCodigo.Text))
                                Catch ex As Exception
                                    precio = 0
                                End Try
                                solicitoPrecio.Dispose()
                            End If
                        End If
                        If pedirPrecioImp Then
                            If VSMA(usuario, Name, Secure.Others) Then
                                Dim solicitoPrecio As New registro
                                solicitoPrecio.Text = "Digite el precio..."
                                solicitoPrecio.ShowDialog()
                                Try
                                    If ImpVenta And (Not ImpServ) Then
                                        precio = (CDbl(solicitoPrecio.txtCodigo.Text) / (1 + (13 / 100)))
                                    ElseIf ImpVenta And ImpServ Then
                                        precio = (CDbl(solicitoPrecio.txtCodigo.Text) / (1 + (23 / 100)))
                                    ElseIf ImpVenta = False And ImpServ Then
                                        precio = (CDbl(solicitoPrecio.txtCodigo.Text) / (1 + (10 / 100)))
                                    ElseIf ImpVenta = False And ImpServ = False Then
                                        precio = (CDbl(solicitoPrecio.txtCodigo.Text))
                                    End If

                                Catch ex As Exception
                                    precio = 0
                                End Try
                                solicitoPrecio.Dispose()
                            End If

                        End If
                    End If

                    Dim nombremenu As String = ""

                    If Me.BANDERA_MEDIO = True Then
                        If _ParaLlevar = 1 Then
                            precio = MenuSelec(0)("Precio_VentaExpressM")
                        Else
                            precio = MenuSelec(0)("Precio_VentaD")
                        End If

                        nombremenu = "1/2 " & sender.text
                    Else
                        nombremenu = sender.text
                    End If

                    AlmacenaTemporal(arrayNombre(1), nombremenu, precio * valorCambio, impresoras, comenzales, usuario, HabitacionAsignada, iPrimero, PedidoExpress)
                    If OpcionesEspeciales Then spIdComandaTemporal()

					'Try
					'    My.Computer.FileSystem.WriteAllText("C:\REPORTES\ClicsLog.txt", numeroComanda & " - " & LabelMesa.Text & " Menu:" & nombremenu & " FH:" & Now & " Usuario:" & usuario & vbCrLf, True)
					'Catch ex As Exception
					'    My.Computer.FileSystem.WriteAllText("C:\REPORTES\ClicsLog.txt", "", False)
					'End Try


					todo_comandado = False
                    Dim act As Integer = estadoAnte 'cConexion.SlqExecuteScalar(conectadobd, "Select activa from Mesas WITH (NOLOCK) where Id=" & idMesa)

                    If act = 0 Or act = 1 Then
                        estadoAnte = 1
                        'cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "id=" & idMesa)
                    End If

                    If p_Tipo = 1 Then 'Si especifica modificadores y acompañamientos
                        If tieneForzado = "True" Or Plato_Principal = True Then
                            limpiaDatos()
                            Bloquear(True)
                            cmbCuentas.Enabled = False
                            bandeTipo = True
                            ControlBox = True
                            ToolButtonModificarComanda.Visible = False
                            ToolButtonRegresar.Visible = False
                            ToolStripButton9.Visible = False
                            ToolStripButton6.Visible = IIf(PedidoExpress = True, False, IIf(OpcionesEspeciales, True, False))
                            ToolStripButton8.Visible = IIf(OpcionesEspeciales, True, False)
                            If OpcionesEspeciales Then
                                ToolStripButtonAtras.Visible = True
                                ToolStripButtonAtras.Text = "Regresar"
                            End If
                            If OpcionesEspeciales Then ToolButtonTerminar.Text = "Siguiente"
                            If OpcionesEspeciales Then ToolButtonTerminar.Image = My.Resources.Derecha
                            tlsCantidad.Visible = False
                            ToolButtonNotas.Visible = False
                            ToolButtonExpress.Visible = False
                            ToolButtonPreFacturar.Visible = False
                            ToolButtonFacturar.Visible = False
                            ToolButtonSepararCuenta.Visible = False
                            ToolStripButtonTerminar.Enabled = True
                            ToolButtonPlatoFuerte.Visible = IIf(PedidoExpress = True, False, IIf(OpcionesEspeciales, True, False))
                            ToolButtonAlergenico.Visible = IIf(OpcionesEspeciales, True, False)
                            ToolStripButton12.Visible = False

                            If tieneForzado = "True" Then
                                ' Dim dtt As New DataTable
                                Dim Modificadores() As DataRow
                                Modificadores = glodtsGlobal.ModificadoresPrecio.Select("IdCategoriaMenu = " & Id_Categoria)

                                ' cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dtt)
                                If Modificadores.Count > 0 Then
                                    For i As Integer = 0 To Modificadores.GetUpperBound(0)

                                        Modificadores(i).Item("Precio_VentaA") = Modificadores(i).Item("Precio_VentaA") * Me.current_Cant
                                    Next

                                    'UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
                                    ' dtt.Rows.Clear()
                                    UbicarPosiciones(Modificadores)
                                Else

                                    ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                                    Modificadores = glodtsGlobal.Modificadores_Forzados.Select()
                                    UbicarPosiciones(Modificadores)

                                End If
                            End If
                            lblEncabezado.Text = "Modificadores*"
                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)
                        End If

                    Else

                        If tieneForzado = "True" Then
                            limpiaDatos()
                            Bloquear(False)
                            cmbCuentas.Enabled = False
                            Dim Forzados() As DataRow
                            Forzados = glodtsGlobal.Modificadores_Forzados.Select()
                            For i As Integer = 0 To Forzados.GetUpperBound(0)
                                Forzados(i).Item("Precio_VentaA") = 0
                            Next
                            'UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                            UbicarPosiciones(Forzados)
                            lblEncabezado.Text = "Modificadores"
                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)
                        End If

                    End If
                    ComandasShow()
                Case "FORZADOS"
                    If Trim(iForzado) = vbNullString Then
                        Dim iPrimero As Integer
                        Dim PedidoExpress As Boolean = fnMesaExpres()
                        iPrimero = IIf(ToolStripButton6.BackColor = Color.Yellow, 1, 0)

                        If _ParaLlevar = 1 Then
                            PedidoExpress = 1
                        End If

                        If arrayNombre(3) > 0 Then
                            cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto,cPrecio,cCantidad,cDescripcion,cCodigo,Cmesa, Impreso, Impresora, Comenzales, Tipo_Plato,Cedula,primero,separada, Express", arrayNombre(1) & "," & arrayNombre(3) & ",-1,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Modificador'" & ",'" & usuario & "'," & iPrimero & "," & fnObtenerCuenta(cmbCuentas.Text) & ",'" & PedidoExpress & "'")
                        Else
                            cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto,cPrecio,cCantidad,cDescripcion,cCodigo,Cmesa, Impreso, Impresora, Comenzales, Tipo_Plato,Cedula,primero,separada, Express", arrayNombre(1) & "," & arrayNombre(3) & ",0,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Modificador'" & ",'" & usuario & "'," & iPrimero & "," & fnObtenerCuenta(cmbCuentas.Text) & ",'" & PedidoExpress & "'")
                        End If

                        If p_Tipo = 1 Then
                            limpiaDatos()
                            Bloquear(True)
                            cmbCuentas.Enabled = False
                            bandeTipo = True
                            ControlBox = True
                            ToolButtonModificarComanda.Visible = False
                            ToolButtonRegresar.Visible = False
                            ToolStripButton9.Visible = False
                            ToolStripButton6.Visible = IIf(PedidoExpress = True, False, IIf(OpcionesEspeciales, True, False))
                            ToolStripButton8.Visible = IIf(OpcionesEspeciales, True, False)
                            Me.ToolStripButtonAtras.Enabled = True
                            tlsCantidad.Visible = False
                            ToolButtonNotas.Visible = False
                            ToolButtonExpress.Visible = False
                            ToolButtonPreFacturar.Visible = False
                            ToolButtonFacturar.Visible = False
                            ToolButtonSepararCuenta.Visible = False
                            ToolStripButtonTerminar.Enabled = True
                            ToolStripButton12.Visible = False

                            ' Dim dt As New DataTable
                            ' cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dt)
                            ' If dt.Rows.Count > 0 Then
                            'UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
                            ' dt.Rows.Clear()
                            'Else
                            '    UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                            '  End If
                            Dim Modificadores() As DataRow
                            Modificadores = glodtsGlobal.ModificadoresPrecio.Select("IdCategoriaMenu = " & Id_Categoria)

                            ' cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dtt)
                            If Modificadores.Count > 0 Then
                                For i As Integer = 0 To Modificadores.GetUpperBound(0)
                                    Modificadores(i).Item("Precio_VentaA") = Modificadores(i).Item("Precio_VentaA") * Me.current_Cant
                                Next

                                'UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
                                ' dtt.Rows.Clear()
                                UbicarPosiciones(Modificadores)
                            Else

                                ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                                Modificadores = glodtsGlobal.Modificadores_Forzados.Select()
                                UbicarPosiciones(Modificadores)

                            End If
                            lblEncabezado.Text = "Modificadores*"
                            ComandasShow()
                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)
                        End If
                    Else
                        iForzado = ""
                    End If
                    Dim row = (From i As dtsGlobal.Menu_RestauranteRow In glodtsGlobal.Menu_Restaurante Where i.Id_Menu = idMenuActivo)
                    Dim TieneModificador_Forzado As Boolean = False
                    For Each r As dtsGlobal.Menu_RestauranteRow In row
                        tieneAcompa = r.Numero_Acompañamientos
                        TieneModificador_Forzado = r.Modificador_Forzado
                    Next
                    If (Not TieneModificador_Forzado = True) Or p_Tipo = 0 Then
                        limpiaDatos()
                        ' tieneAcompa = cConexion.SlqExecuteScalar(conectadobd, "Select Numero_Acompañamientos from Menu_Restaurante WITH (NOLOCK) where id_menu=" & idMenuActivo)

                        If CDbl(tieneAcompa) > 0 Then limpiaDatos()
                        If CDbl(tieneAcompa) > 0 Then
                            lblEncabezado.Text = "Acompañamientos"

                            If p_Tipo = 1 Then
                                bandeTipo = True
                                Bloquear(True)
                                cmbCuentas.Enabled = False
                                ToolButtonModificarComanda.Visible = False
                                ToolButtonRegresar.Visible = False
                                ToolStripButton9.Visible = False
                                ToolStripButton6.Visible = False
                                ToolStripButton8.Visible = False
                                tlsCantidad.Visible = False
                                ToolButtonNotas.Visible = False
                                ToolButtonExpress.Visible = False
                                ToolButtonPreFacturar.Visible = False
                                ToolButtonFacturar.Visible = False
                                ToolButtonSepararCuenta.Visible = False
                                ToolStripButtonAtras.Enabled = True
                                ToolStripButtonTerminar.Enabled = True
                                ToolStripButton12.Visible = IIf(GetSetting("Seesoft", "Restaurante", "1/2") = "1", True, False)
                                ' Dim dt As New DataTable
                                'cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio as Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion", dt)

                                Dim Acompanamiento() As DataRow
                                Acompanamiento = glodtsGlobal.Acompañamientos_MenuPrecio.Select("IdCategoriaMenu = " & Id_Categoria)
                                If Acompanamiento.Count > 0 Then
                                    ' UbicarPosiciones("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio AS Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion")
                                    UbicarPosiciones(Acompanamiento)
                                Else
                                    ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
                                    UbicarPosiciones(glodtsGlobal.Acompañamientos_Menu.Select())
                                End If
                            Else
                                'UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
                                UbicarPosiciones(glodtsGlobal.Acompañamientos_Menu.Select())
                                Bloquear(False)
                                cmbCuentas.Enabled = False
                                bandeTipo = False
                            End If
                        Else
                            Dim MenuSelect() As DataRow
                            MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & idGrupoT)
                            For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                                MenuSelect(i).Item("tipo") = "MENUREST"
                            Next
                            ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                            UbicarPosiciones(MenuSelect)
                            lblEncabezado.Text = "Menú"
                            Bloquear(True)
                            cmbCuentas.Enabled = True
                            bandeTipo = False
                        End If
                    End If
                    ' ComandasShow()
                Case "ACOMPA"
                    Dim iPrimero As Integer
                    Dim PedidoExpress As Boolean = fnMesaExpres()
                    Dim cPrecio As Decimal

                    iPrimero = IIf(ToolStripButton6.BackColor = Color.Yellow, 1, 0)
                    Dim RowAcompa = (From i As dtsGlobal.Acompañamientos_MenuPrecioRow In glodtsGlobal.Acompañamientos_MenuPrecio Where i.IdCategoriaMenu = Id_Categoria And i.Id = arrayNombre(1))
                    For Each r As dtsGlobal.Acompañamientos_MenuPrecioRow In RowAcompa
                        cPrecio = r.Precio_VentaA
                    Next
                    ' cPrecio = cConexion.SlqExecuteScalar(conectadobd, "SELECT dbo.Categoria_Agregado.precio FROM dbo.Acompañamientos_Menu INNER JOIN " & _
                    '                                "dbo.Categoria_Agregado ON dbo.Acompañamientos_Menu.Id = dbo.Categoria_Agregado.IdAgregado " & _
                    '                             "WHERE (dbo.Categoria_Agregado.TipoAgregado = 'ACO') AND (dbo.Categoria_Agregado.IdCategoriaMenu = '" & Id_Categoria & "') AND (dbo.Acompañamientos_Menu.Id = '" & arrayNombre(1) & "')")

                    cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto,cCantidad,cDescripcion,cCodigo,cMEsa, Impreso, Impresora, Comenzales, Tipo_Plato,cedula,primero,separada,Express,cPrecio", arrayNombre(1) & ",0,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Acompañamiento'" & ",'" & usuario & "'," & iPrimero & "," & fnObtenerCuenta(cmbCuentas.Text) & ",'" & PedidoExpress & "','" & cPrecio & "'")
                    If tieneAcompa = 1 Then 'Si especifica modificadores y acompañamientos
                        limpiaDatos()
                        idMenuActivo = 0
                        Dim MenuSelect() As DataRow
                        MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & idGrupoT)
                        For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                            MenuSelect(i).Item("tipo") = "MENUREST"
                        Next
                        ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                        UbicarPosiciones(MenuSelect)
                        ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                        lblEncabezado.Text = "Menú"
                        Bloquear(True)
                        cmbCuentas.Enabled = False
                        bandeTipo = False
                        ControlBox = True
                        ToolButtonModificarComanda.Visible = True
                        ToolButtonRegresar.Visible = True
                        ToolStripButton9.Visible = usaGrupo
                        ToolStripButton6.Visible = IIf(PedidoExpress = True, False, IIf(OpcionesEspeciales, True, False))
                        ToolStripButton8.Visible = True 'IIf(OpcionesEspeciales, True, False)
                        tlsCantidad.Visible = True
                        ' ToolButtonNotas.Visible = True
                        ToolButtonExpress.Visible = True
                        ToolButtonPreFacturar.Visible = True
                        ToolButtonFacturar.Visible = True
                        ToolButtonSepararCuenta.Visible = True
                        ToolStripButtonAtras.Enabled = True
                        ToolStripButtonTerminar.Enabled = True
                        ToolButtonPlatoFuerte.Visible = False
                        ToolButtonAlergenico.Visible = False
                        If OpcionesEspeciales Then
                            ToolButtonTerminar.Text = "Enviar"
                            ToolButtonTerminar.Image = img
                            ToolButtonRegresar.Visible = False
                        Else
                            ToolButtonTerminar.Text = "Terminar"
                        End If
                        cmbCuentas.Enabled = True
                    Else
                        tieneAcompa = tieneAcompa - 1
                    End If
                    ComandasShow()
            End Select

            arrayNombre = Nothing
            PanelMesas.Focus()
        Catch ex As Exception
            MessageBox.Show("Error:" & ex.ToString, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "Clickbuttons", ex.Message)
        End Try
    End Sub
    Private Sub UbicarPosicionesAgregados()

        Try
            Dim er As Boolean = False
            Dim txt As String = "SELECT ACO_AGREGADO.IdCategoriaAgregados As Id,TipoAgregado as Tipo,0 as Precio_VentaA,ACO_AGREGADO.IdAgregado, Acompañamientos_Menu.Nombre, ACO_AGREGADO.posicion, Acompañamientos_Menu.ImagenRuta " &
                            " FROM Acompañamientos_Menu INNER JOIN" &
                          " ACO_AGREGADO ON Acompañamientos_Menu.Id = ACO_AGREGADO.IdAgregado WHERE (ACO_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")"
            '  UbicarPosiciones(txt)

            txt = "SELECT MOD_AGREGADO.IdCategoriaAgregados As Id, TipoAgregado as Tipo, 0 as Precio_VentaA, Modificadores_Forzados.Nombre, MOD_AGREGADO.posicion, Modificadores_Forzados.ImagenRuta, MOD_AGREGADO.IdAgregado, MOD_AGREGADO.TipoAgregado " &
                                " FROM MOD_AGREGADO INNER JOIN " &
                          " Modificadores_Forzados ON MOD_AGREGADO.IdAgregado = Modificadores_Forzados.ID WHERE (MOD_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")"
            ' UbicarPosiciones(txt)
            If er = True Then
                MsgBox("Algunas imagenes fueron movidas de su ubicación original")
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "UbicarPosicionesAgregados", ex.Message)
        End Try

    End Sub
    Private Sub AlmacenaTemporal(ByVal cProducto As Integer, ByVal cDescripcion As String, ByVal cPrecio As Double, ByVal Impresora As String, ByVal cont As Integer, ByVal cedula As String, ByVal Hab As String, ByVal primero As Integer, ByVal Express As Boolean)
        Try
            Dim prec As Double = Format(cPrecio, "###,##0.0000")
            If Hab = Nothing Then Hab = ""

            Dim cx As New Conexion
            cx.Conectar()

            If Trim(numeroComanda) = vbNullString Then
                numeroComanda = 0

            End If

            cx.DesConectar(cx.sQlconexion)

            If _ParaLlevar = 1 Then
                Express = 1
            End If

            Dim cCodigo As String
            cCodigo = cConexion.SlqExecuteScalar(conectadobd, "ActualizaTablaTemporal " & cProducto & ",'" & cDescripcion & "'," & CDbl(prec) & "," & numeroComanda & "," & idMesa & ",0,'" & Impresora & "'," & cont & ",'Principal','" & cedula & "','" & Hab & "'," & primero & "," & fnObtenerCuenta(cmbCuentas.Text) & "," & IIf(Express, 1, 0) & "," & lblCantidad.Text)
            If numeroComanda = 0 Then
                numeroComanda = cCodigo

            End If

            lblCantidad.Text = "1"

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "AlmacenaTemporal", ex.Message)
        End Try
    End Sub


    Private Sub CargaPanel(ByVal consulta As String, ByVal encabezado As String)
        limpiaDatos()
        ' UbicarPosiciones(consulta)
        lblEncabezado.Text = encabezado
    End Sub

    Dim IVentas As Double
    Dim IServicios As Double = 0
    Private Sub ComandasShow()
        Try
            cargarDescuento()
            Dim leyo As Boolean = False
            Dim precio, cantidad, subTotal As Double
            Dim CodigoMesa As Integer
            Dim descripcion As String
            Dim subTotalTodo As Double
            Dim SelColor As Boolean = False
            Dim Sel As String = Nothing
            Dim ii, PosArray As Integer
            Dim arrayStyle As New ArrayList
            Dim arrayImp As New ArrayList
            Dim aGeneraStyle As Array
            Dim bandera As Boolean = True
            Dim bandera1 As Boolean = True
            Dim cedSalonero As String = ""
            Dim hora As String = ""
            Dim Linea As String = ""
            IVentas = 0
            IServicios = 0
            Dim BanderaExpressIni As Boolean = True
            Dim BanderaExpressFin As Boolean = True


            Me.ComboBoxComensal.Text = comenzales
            Me.TxtNumeroComanda.Text = numeroComanda
            Me.LabelMesa.Text = "Mesa:  " & nombreMesa
            txtComanda.Text = ""

            If numeroComanda = vbNullString Then
                Exit Sub ' si no se ha creado una comanda todavia.
            End If
            Dim dt As New DataTable
            Dim sql As New SqlClient.SqlCommand

            Dim Conexion As New Conexion
            Dim Registros As SqlClient.SqlDataReader
            ' Registros = Conexion.GetRecorset(conectadobd, "select * from DatosExpress where id_comanda = " & numeroComanda)
            '  sql.CommandText = "select * from DatosExpress where id_comanda = " & numeroComanda
            ' cFunciones.spCargarDatos(sql, dt)
            For i As Integer = 0 To glodtsGlobal.DatosExpress.Rows.Count - 1
                txtComanda.Text = txtComanda.Text & "******* DATOS DE PEDIDO EXPRESS *************" & vbCrLf
                txtComanda.Text = txtComanda.Text & "*Cliente  :" & glodtsGlobal.DatosExpress.Rows(i).Item("nombre") & "" & vbCrLf
                txtComanda.Text = txtComanda.Text & "*Teléfono :" & glodtsGlobal.DatosExpress.Rows(i).Item("Telefono") & "" & vbCrLf
                txtComanda.Text = txtComanda.Text & "*Dirección:" & glodtsGlobal.DatosExpress.Rows(i).Item("Direccion") & "" & vbCrLf
            Next

            txtComanda.Text = txtComanda.Text & "*********************************************" & vbCrLf
            txtComanda.Text = txtComanda.Text & "*            Detalle de la Orden            *" & vbCrLf
            txtComanda.Text = txtComanda.Text & "*********************************************" & vbCrLf
            txtComanda.Text = txtComanda.Text & "* Cant.    Precio/U     Descripción         *" & vbCrLf
            Try

                dt.Clear()
                If numeroComanda = "" Then Exit Sub
                'Cambio realizado por Diego Gamboa para que tome en cuenta todos los productos que tiene 0 y 1.
                If fnObtenerCuenta(cmbCuentas.Text) = 0 Or fnObtenerCuenta(cmbCuentas.Text) = 1 Then
                    sql.CommandText = "SELECT ComandasActivas.cCantidad,ComandasActivas.cCantidadNueva,Impreso, ComandasActivas.cProducto, ComandasActivas.cDescripcion, ComandasActivas.cPrecio, ComandasActivas.cCodigo,  ComandasActivas.primero, ComandasActivas.Cedula, ComandasActivas.hora, ComandasActivas.Express,  (CASE WHEN ComandasActivas.cProducto = 0  THEN ComandasActivas.cCantidadNueva * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) " &
                                      "  when ComandasActivas.cProducto > 0 and ComandasActivas.cCantidad > 0  then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100)  when ComandasActivas.cProducto > 0 and (ComandasActivas.cCantidad = -1 or ComandasActivas.cCantidad = 0)  then 1 * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100)  END) AS IVentas,  (CASE WHEN ComandasActivas.cProducto = 0 THEN ComandasActivas.cCantidadNueva * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) " &
                                      " when ComandasActivas.cProducto > 0 and ComandasActivas.cCantidad > 0 then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) when ComandasActivas.cProducto > 0 and (ComandasActivas.cCantidad = -1 or ComandasActivas.cCantidad = 0) then 1 * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) END) AS IServicios,ComandasActivas.cMesa FROM ComandasActivas WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cMesa =" & idMesa & " and (separada= 0 or separada = 1)AND ComandasActivas.Estado='Activo'  ORDER BY primero DESC,express ASC,hora ASC"
                    cFunciones.spCargarDatos(sql, dt)
                Else
                    sql.CommandText = "SELECT ComandasActivas.cCantidad,ComandasActivas.cCantidadNueva,Impreso, ComandasActivas.cProducto, ComandasActivas.cDescripcion, ComandasActivas.cPrecio, ComandasActivas.cCodigo,  ComandasActivas.primero, ComandasActivas.Cedula, ComandasActivas.hora, ComandasActivas.Express,  (CASE WHEN ComandasActivas.cProducto = 0  THEN ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) " &
                                      "  when ComandasActivas.cProducto > 0 and ComandasActivas.cCantidad > 0  then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) when ComandasActivas.cProducto > 0 and (ComandasActivas.cCantidad = -1 or ComandasActivas.cCantidad = 0)  then 1 * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100)  END) AS IVentas,  (CASE WHEN ComandasActivas.cProducto = 0 THEN ComandasActivas.cCantidadNueva * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) " &
                                      " when ComandasActivas.cProducto > 0 and ComandasActivas.cCantidad > 0 then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0))  when ComandasActivas.cProducto > 0 and (ComandasActivas.cCantidad = -1 or ComandasActivas.cCantidad = 0) then 1 * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) END) AS IServicios,ComandasActivas.cMesa FROM ComandasActivas WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cMesa=" & idMesa & " and separada= " & fnObtenerCuenta(cmbCuentas.Text) & " AND ComandasActivas.Estado='Activo' ORDER BY primero DESC,express ASC,hora ASC"
                    cFunciones.spCargarDatos(sql, dt)
                End If

                Dim todo_impreso As Boolean = True
                For i As Integer = 0 To dt.Rows.Count - 1
                    leyo = True
                    subTotal = 0
                    precio = dt.Rows(i).Item("cPrecio")

                    cantidad = dt.Rows(i).Item("cCantidadNueva")
                    descripcion = Trim(dt.Rows(i).Item("cDescripcion")).ToUpper
                    CodigoMesa = Convert.ToInt32(dt.Rows(i).Item("cMesa"))

                    If cantidad > 0 Then
                        subTotal = dt.Rows(i).Item("cCantidadNueva") * dt.Rows(i).Item("cPrecio")
                    End If
                    SelColor = dt.Rows(i).Item("Impreso")

                    If dt.Rows(i).Item("Primero") = 1 And bandera = True Then
                        txtComanda.Text = txtComanda.Text & "********* PREPARAR DE PRIMERO ***************" & vbCrLf
                        bandera = False
                    End If

                    If dt.Rows(i).Item("Primero") = 0 And bandera1 = True Then
                        txtComanda.Text = txtComanda.Text & "*********************************************" & vbCrLf
                        bandera1 = False
                    End If

                    If CodigoMesa <> 0 Then

                        If dt.Rows(i).Item("Express") = True And BanderaExpressIni = True Then
                            txtComanda.Text = txtComanda.Text & vbCrLf & "::::::::::> PREPARAR PARA EXPRESS <::::::::::" & vbCrLf
                            BanderaExpressIni = False
                        End If
                        Linea = ""
                        If CDbl(dt.Rows(i).Item("cCantidad")) = 0 Or CDbl(dt.Rows(i).Item("cCantidad")) = -1 Then
                            If precio = "0" Then
                                If descripcion.Length < (45 - Linea.Length) Then
                                    Linea = Space(15) & descripcion
                                Else
                                    Linea = Space(15) & descripcion.Substring(0, (25))
                                End If
                                txtComanda.Text = txtComanda.Text & Linea & vbCrLf
                                arrayStyle.Add(Linea)
                            Else
                                Dim desc As String = descripcion & " " & precio
                                If desc.Length < (45 - Linea.Length) Then
                                    Linea = Space(15) & desc
                                Else
                                    Linea = Space(15) & desc.Substring(0, (25))
                                End If
                                txtComanda.Text = txtComanda.Text & Linea & vbCrLf
                                arrayStyle.Add(Linea)
                            End If
                            subTotal = precio
                        Else
                            If cantidad.ToString.Length <= 4 Then
                                If (SelColor = True) Then
                                    Sel = " Imp"
                                Else
                                    todo_impreso = False
                                End If
                                Linea = Space(4 - cantidad.ToString.Length) & cantidad.ToString & Sel
                                Sel = Nothing
                            Else
                                Linea = cantidad.ToString
                            End If
                            If precio.ToString.Length <= 9 Then
                                Linea = Linea & " " & Space(9 - precio.ToString.Length) & precio.ToString & "    "
                            Else
                                Linea = Linea & " " & precio.ToString & "    "
                            End If
                            If descripcion.Length < (45 - Linea.Length) Then
                                Linea = Linea & descripcion
                            Else
                                Linea = Linea & descripcion.Substring(0, (45 - Linea.Length))
                            End If
                            txtComanda.Text = txtComanda.Text & Linea & vbCrLf
                        End If
                    End If
                    Try
                        cedSalonero = dt.Rows(i).Item("cedula")
                        hora = dt.Rows(i).Item("hora")
                    Catch ex As Exception
                    End Try
                    subTotalTodo += subTotal
                    If precio = "0" Then
                        IVentas += dt.Rows(i).Item("IVentas")
                        IServicios += dt.Rows(i).Item("IServicios")
                    Else
                        IVentas += dt.Rows(i).Item("IVentas")
                        IServicios += dt.Rows(i).Item("IServicios")
                    End If
                Next
                If todo_comandado = True Then
                    If todo_impreso = False Then
                        todo_comandado = False
                    End If
                End If



                For ii = 0 To arrayStyle.Count - 1
                    txtComanda.SelectionStart = txtComanda.Find(arrayStyle(ii))
d:
                    If txtComanda.SelectionBackColor = Color.LightYellow Then
                        PosArray = txtComanda.SelectionStart + txtComanda.SelectionLength
                        txtComanda.DeselectAll()
                        txtComanda.SelectionStart = txtComanda.Find(arrayStyle(ii), PosArray, RichTextBoxFinds.None)
                        GoTo d
                    End If
                    txtComanda.SelectionFont = New System.Drawing.Font("verdana", 7, CType((System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    txtComanda.SelectionBackColor = Color.LightYellow
                    txtComanda.DeselectAll()
                Next
                aGeneraStyle = Nothing
                arrayStyle = Nothing

                Dim totalDescuento As Double = 0
                Dim totaDescIServ As Double = 0
                Dim totaDescIVenta As Double = 0
                If descuento > 0 Then
                    totalDescuento = subTotalTodo * (descuento / 100)
                    totaDescIServ = IServicios * (descuento / 100)
                    totaDescIVenta = IVentas * (descuento / 100)

                End If
                txtSubTotal.Text = Format(subTotalTodo, "###,##0.00")
                txtDesc.Text = Format(totalDescuento, "###,##0.00")
                txtIServicio.Text = Format(IServicios - totaDescIServ, "###,##0.00")
                txtIVentas.Text = Format(IVentas - totaDescIVenta, "###,##0.00") 'Cambiar por los datos de configuracion
                txtTotal.Text = Format((subTotalTodo - totalDescuento) + txtIServicio.Text + txtIVentas.Text, "###,##0.00")



                Try
                    Dim RowUsuario = (From i As dtsGlobal.UsuariosRow In glodtsGlobal.Usuarios Where i.Cedula = cedSalonero)
                    For Each r As dtsGlobal.UsuariosRow In RowUsuario
                        nUsu = r.Nombre
                    Next
                    ' nUsu = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from usuarios where cedula ='" & cedSalonero & "'")
                    lblSalonero.Text = nUsu & " - " & hora
                Catch ex As Exception
                    cFunciones.spEnviar_Correo(Me.Name, "ComandasShow", ex.Message)
                End Try

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
                cFunciones.spEnviar_Correo(Me.Name, "ComandasShow", ex.Message)
            End Try
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ComandasShow", ex.Message)
        End Try
    End Sub

    Private Sub RefrescaDatos()
        Dim ConexionX As New ConexionR
        Dim ReaderSource As SqlDataReader
        lstProductos.View = View.Details
        lstProductos.FullRowSelect = True
        lstProductos.LabelEdit = False
        lstProductos.Items.Clear()
        Try
            If fnObtenerCuenta(cmbCuentas.Text) = 0 Or fnObtenerCuenta(cmbCuentas.Text) = 1 Then
                ConexionX.GetRecorset(ConexionX.Conectar(), "Select id,cProducto, cCantidad, cDescripcion, cPrecio, cCodigo from ComandasActivas WITH (NOLOCK) where  (separada=0 OR separada = 1) AND  cMesa = " & idMesa & " order by primero", ReaderSource)
            Else
                ConexionX.GetRecorset(ConexionX.Conectar(), "Select id,cProducto, cCantidad, cDescripcion, cPrecio, cCodigo from ComandasActivas WITH (NOLOCK) where  separada= " & fnObtenerCuenta(cmbCuentas.Text) & " AND cMesa = " & idMesa & " order by primero", ReaderSource)
            End If


            While ReaderSource.Read
                With lstProductos.Items.Add(ReaderSource("id"))
                    .SubItems.Add(ReaderSource("cCantidad"))
                    .SubItems.Add(ReaderSource("cDescripcion"))
                    .SubItems.Add(ReaderSource("cPrecio"))
                    .SubItems.Add(ReaderSource("cCodigo"))
                    .SubItems.Add(ReaderSource("cProducto"))
                End With
            End While
            ReaderSource.Close()
            ConexionX.DesConectar(ConexionX.SqlConexion)
            grpModifica.Visible = True

            If fnMesaExpres() Then
                ToolStripButton10.Visible = False
                ToolStripButton7.Visible = False
            End If


        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "RefrescarDatos", ex.Message)
        End Try
    End Sub

    Private Function Verificar_Impresora_Instalada(ByVal Impresora As String, ByVal NumeroComanda As Double) As String
        Dim PrinterInstalled As String
        Dim existe As Boolean = False
        Dim arrayImpresora1 As Array

        Try
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                arrayImpresora1 = PrinterInstalled.ToUpper.ToString.Split("\")

                If Trim(Impresora.ToUpper) = Trim(PrinterInstalled.ToUpper) Then
                    If Not GetSetting("SeeSoft", "Restaurante", "obligaImpresora").Equals("") Then
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda) '& " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                    Else
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                    End If

                    Return Trim(PrinterInstalled.ToUpper)
                    existe = True
                    Exit For
                Else
                    If arrayImpresora1.Length > 1 Then
                        If Trim(Impresora.ToUpper) = arrayImpresora1(3) Then
                            If GetSetting("SeeSoft", "Restaurante", "obligaImpresora").Equals("") Then
                                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                            Else
                                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda) '& " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                            End If

                            Return Trim(PrinterInstalled.ToUpper)
                            Exit For
                        End If
                    End If
                End If
            Next
22064020:
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            cFunciones.spEnviar_Correo(Me.Name, "Verificar_Impresora_Instalada", ex.Message)
        End Try

        MessageBox.Show("No se encontró la impresora correspondiente: [" & Impresora & "]", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        Dim Impr As String = busca_impresora()
        If Impr = "" Then
            MessageBox.Show("No se seleccionó ninguna impresora, la comanda será eliminada", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Close()
            Return ""
            Exit Function
        End If
        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Impr & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
        Return Impr
    End Function

    Private Function verifica_impresora(ByVal impresora As String) As Boolean
        Try
            Dim PrinterInstalled As String
            Dim existe As Boolean = False
            Dim arrayImpresora1 As Array
            Dim arrayImpresora2 As Array
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                arrayImpresora1 = PrinterInstalled.ToUpper.ToString.Split("\")
                arrayImpresora2 = impresora.ToUpper.ToString.Split("\")
                If Trim(impresora.ToUpper) = Trim(PrinterInstalled.ToUpper) Then
                    existe = True
                    Exit For
                Else
                    If arrayImpresora2.Length > 1 Then
                        If arrayImpresora2(3) = Trim(PrinterInstalled.ToUpper) Then
                            impresoras = Trim(PrinterInstalled.ToUpper)
                            existe = True
                            Exit For
                        End If
                    End If
                    If arrayImpresora1.Length > 1 Then
                        If arrayImpresora1(3) = Trim(PrinterInstalled.ToUpper) Then
                            impresoras = Trim(PrinterInstalled.ToUpper)
                            existe = True
                            Exit For
                        End If
                    End If
                End If
            Next
            Return existe
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "verifica_impresora", ex.Message)
        End Try
        Return False
    End Function

    Private Sub actualizar()

        ' spFinalizarComanda()

        spCorrigeEstadoMesa()
    End Sub

    Private Sub spFinalizarComanda()
        Try
            Dim Conexion1 As New SqlConnection
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal.comandado = 1 where ComandaTemporal.cMesa =" & idMesa)
            cConexion.DesConectar(Conexion1)
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "spFinalizarComanda", ex.Message)
        End Try
    End Sub

    Private Sub spReporteConConexionSql(ByVal Impresora As String, ByVal Reimpresion As Boolean)
        Try
            If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("1") Then Exit Sub

            Dim ImpresoraBar As String = ""
            If Impresora.IndexOf("BAR") > 0 Then
                ImpresoraBar = "BAR"
            End If
            If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Sub

            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                rptComanda.Refresh()
                rptComanda.PrintOptions.PrinterName = Impresora
                rptComanda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                rptComanda.SetParameterValue(0, numeroComanda)
                rptComanda.SetParameterValue(1, 0)
                rptComanda.SetParameterValue(2, Impresora)
                rptComanda.SetParameterValue(3, User_Log.NombrePunto)
                If Reimpresion = True Then
                    rptComanda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    rptComanda.SetParameterValue(4, "")
                End If

                rptComanda.PrintToPrinter(1, True, 0, 0)

                If Impresora Like "COCINA" Then
                    Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                    If TEM = "" Then Exit Sub
                    rptComanda.Refresh()
                    rptComanda.PrintOptions.PrinterName = TEM
                    rptComanda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComanda.SetParameterValue(0, numeroComanda)
                    rptComanda.SetParameterValue(1, 0)
                    rptComanda.SetParameterValue(2, Impresora)
                    rptComanda.SetParameterValue(3, User_Log.NombrePunto)
                    If Reimpresion = True Then
                        rptComanda.SetParameterValue(4, "REIMPRESIÓN")
                    Else
                        rptComanda.SetParameterValue(4, "")
                    End If

                    rptComanda.PrintToPrinter(1, True, 0, 0)
                End If
            Else
                rptComandaIzquierda.Refresh()
                rptComandaIzquierda.PrintOptions.PrinterName = Impresora
                rptComandaIzquierda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                rptComandaIzquierda.SetParameterValue(0, numeroComanda)
                rptComandaIzquierda.SetParameterValue(1, 0)
                rptComandaIzquierda.SetParameterValue(2, Impresora)
                rptComandaIzquierda.SetParameterValue(3, User_Log.NombrePunto)
                If Reimpresion = True Then
                    rptComandaIzquierda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    rptComandaIzquierda.SetParameterValue(4, "")
                End If

                rptComandaIzquierda.PrintToPrinter(1, True, 0, 0)

                If Impresora Like "COCINA" Then
                    Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                    If TEM = "" Then Exit Sub
                    rptComandaIzquierda.Refresh()
                    rptComandaIzquierda.PrintOptions.PrinterName = TEM
                    rptComandaIzquierda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComandaIzquierda.SetParameterValue(0, numeroComanda)
                    rptComandaIzquierda.SetParameterValue(1, 0)
                    rptComandaIzquierda.SetParameterValue(2, Impresora)
                    rptComandaIzquierda.SetParameterValue(3, User_Log.NombrePunto)
                    If Reimpresion = True Then
                        rptComandaIzquierda.SetParameterValue(4, "REIMPRESIÓN")
                    Else
                        rptComandaIzquierda.SetParameterValue(4, "")
                    End If

                    rptComandaIzquierda.PrintToPrinter(1, True, 0, 0)
                End If

            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spReporteConConexionSql", ex.Message)
        End Try
    End Sub
    Private Function FnCargarDatosImpresionComanda(ByVal _Impresora As String) As Boolean
        Dim conn As New SqlClient.SqlConnection
        Dim sql As New SqlClient.SqlCommand
        Dim adap As New SqlClient.SqlDataAdapter(sql)

        Try

            DtsImprimeComanda1.Tb_ComandaTemporal.Clear()
            DtsImprimeComanda1.Tb_Mesa.Clear()

            conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            conn.Open()
            sql.Connection = conn
            sql.CommandText = "SELECT dbo.ComandasActivas.cProducto ,dbo.ComandasActivas.Habitacion,dbo.ComandasActivas.Impreso,dbo.ComandasActivas.Estado,  dbo.ComandasActivas.cCantidad ,  dbo.ComandasActivas.cDescripcion ," &
                              "  dbo.ComandasActivas.primero, dbo.ComandasActivas.Express,dbo.ComandasActivas.Solicitud " &
                              " ,dbo.ComandasActivas.Id FROM dbo.ComandasActivas INNER JOIN " &
                              " dbo.Mesas ON dbo.ComandasActivas.cMesa = dbo.Mesas.Id " &
                              " where ComandasActivas.Estado = 'ACTIVO' and ComandasActivas.Impreso = 0 and (UPPER(ComandasActivas.Impresora) = UPPER(@PImpresora)) and (ComandasActivas.cMesa = @Mesa)  order by dbo.ComandasActivas.Id asc "
            sql.Parameters.AddWithValue("@PImpresora", _Impresora)
            sql.Parameters.AddWithValue("@Mesa", idMesa)
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_ComandaTemporal)

            sql.CommandText = "SELECT    distinct  top 1 dbo.Mesas.Nombre_Mesa , dbo.Grupos_Mesas.Nombre_Grupo,dbo.Usuarios.Nombre , dbo.ComandasActivas.Id" &
                              " FROM dbo.ComandasActivas INNER JOIN dbo.Usuarios ON dbo.ComandasActivas.Cedula = dbo.Usuarios.Cedula INNER JOIN " &
                              " dbo.Mesas ON dbo.ComandasActivas.cMesa = dbo.Mesas.Id INNER JOIN dbo.Grupos_Mesas ON dbo.Mesas.Id_GrupoMesa = dbo.Grupos_Mesas.id " &
                              " where (UPPER(ComandasActivas.Impresora) = UPPER(@PImpresora)) and (dbo.Mesas.Id = @Mesa) order by dbo.ComandasActivas.Id desc"
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_Mesa)
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.Message, True)
            cFunciones.spEnviar_Correo(Me.Name, "FnCargarDatosImpresionComanda", ex.Message)
            Return False
        End Try
    End Function



    Private Sub spReporteConDTS(ByVal Impresora As String, ByVal Reimpresion As Boolean)

        If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("1") Then Exit Sub
        Dim ImpresoraBar As String = ""
        If Impresora.IndexOf("BAR") > 0 Then
            ImpresoraBar = "BAR"
        End If
        Try


            Dim rpt As New rptComandadts
            'CrystalReportsConexion.LoadReportViewer(Nothing, rptComanda, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))

            If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Sub
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                If FnCargarDatosImpresionComanda(Impresora) Then
                    rpt.Refresh()
                    rpt.SetDataSource(DtsImprimeComanda1)
                    rpt.PrintOptions.PrinterName = Impresora
                    rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rpt.SetParameterValue(0, numeroComanda)
                    rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rpt.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rpt.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rpt.PrintToPrinter(1, True, 0, 0)
                    ' Dim Visor As New frmVisorReportes
                    ' Visor.rptViewer.ReportSource = rpt
                    ' Visor.rptViewer.Show()
                    ' Visor.ShowDialog()

                    If Impresora Like "COCINA" Then
                        Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                        If TEM = "" Then Exit Sub
                        rpt.Refresh()
                        rpt.SetDataSource(DtsImprimeComanda1)
                        rpt.PrintOptions.PrinterName = TEM
                        rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        rpt.SetParameterValue(0, numeroComanda)
                        rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                        If Reimpresion = True Then
                            rpt.SetParameterValue(2, "REIMPRESIÓN")
                        Else
                            rpt.SetParameterValue(2, "")
                        End If
                        Dim Peticiones1 As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                        rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                        rpt.PrintToPrinter(1, True, 0, 0)
                    End If
                End If
            Else
                If FnCargarDatosImpresionComanda(Impresora) Then
                    rpt.Refresh()
                    rpt.SetDataSource(DtsImprimeComanda1)
                    rpt.PrintOptions.PrinterName = Impresora
                    rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rpt.SetParameterValue(0, numeroComanda)
                    rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rpt.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rpt.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rpt.PrintToPrinter(1, True, 0, 0)

                    If Impresora Like "COCINA" Then
                        Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                        If TEM = "" Then Exit Sub
                        rpt.Refresh()
                        rpt.SetDataSource(DtsImprimeComanda1)
                        rpt.PrintOptions.PrinterName = TEM
                        rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        rpt.SetParameterValue(0, numeroComanda)
                        rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                        If Reimpresion = True Then
                            rpt.SetParameterValue(2, "REIMPRESIÓN")
                        Else
                            rpt.SetParameterValue(2, "")
                        End If
                        Dim Peticiones1 As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                        rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                        rpt.PrintToPrinter(1, True, 0, 0)
                    End If
                End If
            End If
        Catch ex As Exception
            'ESCRIBIR LOG DE ERRRORES

            cFunciones.spEnviar_Correo(Me.Name, "spReporteConDTS", ex.Message)
        End Try

    End Sub


    Private Sub Imprime_Comanda(ByVal Impresora As String, ByVal Reimpresion As Boolean)
        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("") Then SaveSetting("SeeSoft", "Restaurante", "TipoReporteComanda", "0")
        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("1") Then
            spReporteConDTS(Impresora, Reimpresion)
        Else
            spReporteConConexionSql(Impresora, Reimpresion)
        End If
    End Sub

    Private Function busca_impresora() As String
        Try
            Dim PrintDocument1 As New PrintDocument
            Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
            Dim PrinterInstalled As String

            'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
            Dim impresoraPredeterminada As String = GetSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura")
            If Not impresoraPredeterminada.Equals("") Then

                For Each PrinterInstalled In PrinterSettings.InstalledPrinters

                    Dim cedula As String
                    'cedula = cConexion.SlqExecuteScalar(conectadobd, "SELECT Cedula FROM CONFIGURACIONES")
                    cedula = glodtsGlobal.Configuraciones(0).Cedula

                    If cedula = "3-101-139559" Then
                        Select Case PrinterInstalled.ToUpper
                            Case impresoraPredeterminada
                                Return PrinterInstalled.ToString
                        End Select
                    Else
                        Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                            Case impresoraPredeterminada 'FACTURACION
                                Return PrinterInstalled.ToString
                                Exit Function
                        End Select
                    End If
                Next
            Else
                'CREA EL REGISTRO PARA QUE PODAMOS MODIFICARLO LUEGO
                SaveSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura", "")
            End If
            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return "" 'Ninguna Impresora
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "busca_impresora", ex.Message)
        End Try
        Return ""
    End Function


    Private Function DescargaInvRecursivo(ByVal Id_Receta As Integer, ByVal cantidad As Double) As Decimal
        Try
            'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
            Dim conexion As New SqlConnection
            Dim Conexion1 As New SqlConnection
            '
            Dim existencia As Double = 0
            Dim existencia_inv As Double = 0
            Dim existencia_bod As Double = 0
            Dim existencia_inv_act As Double = 0
            Dim existencia_bod_act As Double = 0
            Dim arrayBodegas(200) As Integer
            Dim dt As New DataTable
            'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
            Dim Costo_Movimiento As Double
            Dim saldo_final As Double
            Dim costo_promedio As Double
            Dim id_bodega As Integer

            Dim _Total_costo_real As Decimal = 0

            Dim reduce As Double = 0
            cFunciones.Llenar_Tabla_Generico("SELECT     Codigo, Articulo, Disminuye,cantidad,idBodegaDescarga as bodega        FROM Recetas_Detalles " &
                                        " WHERE     (IdReceta = " & Id_Receta & ")", dt)
            conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If conexion.State = ConnectionState.Closed Then conexion.Open()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("Articulo") Then
                    'EXISTENCIA EN BODEGA
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Codigo"))
                    existencia_inv = existencia

                    If Not GetSetting("SeeSoft", "Restaurante", "actualizado").Equals("") Then
                        reduce = (cantidad * dt.Rows(i).Item("disminuye")) 'Se usa en caso de que se haya metido los datos bien y actualizados
                    Else
                        reduce = dt.Rows(i).Item("disminuye")
                    End If

                    existencia = existencia - reduce
                    existencia_inv_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dt.Rows(i).Item("Codigo"))
                    'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
                    id_bodega = dt.Rows(i).Item("Bodega") 'rs("bodega")
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
                    saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
                    costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
                    'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
                    existencia_bod = existencia
                    If Not GetSetting("SeeSoft", "Restaurante", "actualizado").Equals("") Then
                        Dim disminuye As Double = dt.Rows(i).Item("Disminuye")
                        If disminuye <= 0 Then
                            disminuye = 1
                        End If
                        reduce = (cantidad * disminuye) 'Se usa en caso de que se haya metido los datos bien y actualizados
                    Else
                        reduce = dt.Rows(i).Item("disminuye")
                    End If
                    existencia = existencia - reduce
                    existencia = Math.Round(existencia, 2)
                    Costo_Movimiento = reduce * costo_promedio
                    Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                    saldo_final = saldo_final - Costo_Movimiento
                    existencia_bod_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dt.Rows(i).Item("Codigo") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
                    'INGRESA AL KARDEX
                    actualiza_kardex(conexion, dt.Rows(i).Item("Codigo"), reduce, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dt.Rows(i).Item("Bodega"), costo_promedio, saldo_final)
                    'En la tabla de conf de Restaurante se escoje si se da el último costo o costo promedio
                    Dim UltimoCosto = cConexion.SlqExecuteScalar(Conexion1, "Select UltimoPrecio from conf")
                    Dim costo_real As Decimal
                    If UltimoCosto = False Then
                        costo_real = dt.Rows(i).Item("disminuye") * costo_promedio
                    Else
                        Dim CostoUltimo As Decimal = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Costo from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Codigo"))
                        costo_real = dt.Rows(i).Item("disminuye") * CostoUltimo
                    End If
                    _Total_costo_real += costo_real
                Else
                    Dim dt_rec As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dt.Rows(i).Item("Codigo") & ") ", dt_rec)
                    _Total_costo_real += DescargaInvRecursivo(dt.Rows(i).Item("Codigo"), cantidad * (dt.Rows(i).Item("cantidad") / dt_rec.Rows(0).Item("Porciones")))

                End If

            Next
            Return _Total_costo_real
            '*******************************************FIN ARTICULOS*********************************************************************************************************

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "DescargaInvRecursivo", ex.Message)
        End Try
    End Function

    Private Sub DescargaInventario()
        Try
            'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
            Dim conexion As New SqlConnection
            Dim Conexion1 As New SqlConnection
            '
            Dim existencia As Double = 0
            Dim existencia_inv As Double = 0
            Dim existencia_bod As Double = 0
            Dim existencia_inv_act As Double = 0
            Dim existencia_bod_act As Double = 0
            Dim arrayBodegas(200) As Integer

            'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
            Dim Costo_Movimiento As Double
            Dim saldo_final As Double
            Dim costo_promedio As Double
            Dim id_bodega As Integer
            Dim costo_real As Double
            Dim UltimoCosto As Boolean : Dim CostoUltimo As Double
            Dim dt As New DataTable
            Dim sql As New SqlClient.SqlCommand

            sql.CommandText = "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandasActivas.cCantidad, Menu_Restaurante.bodega, ComandasActivas.id as id_detalle, Menu_Restaurante.Tipo from ComandasActivas inner join Menu_Restaurante on ComandasActivas.cProducto = Menu_Restaurante.Id_Menu where ComandasActivas.Estado='Activo'and ComandasActivas.comandado = 0 AND ComandasActivas.cMesa =" & idMesa & " And (cCantidad - Impreso <> 0)"
            conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            cFunciones.spCargarDatos(sql, dt)

            If conexion.State = ConnectionState.Closed Then conexion.Open()

            'En la tabla de conf de Restaurante se escoje si se da el último costo o costo promedio
            UltimoCosto = cConexion.SlqExecuteScalar(Conexion1, "Select UltimoPrecio from conf")

            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("Tipo") = 2 Then
                    'EXISTENCIA EN BODEGA
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Id_Receta"))
                    existencia_inv = existencia
                    existencia = existencia - (dt.Rows(i).Item("cCantidad") * dt.Rows(i).Item("disminuye"))
                    existencia_inv_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dt.Rows(i).Item("Id_Receta"))
                    'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
                    id_bodega = dt.Rows(i).Item("bodega")
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Id_Receta") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
                    saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Id_Receta") & "and idBodega = " & id_bodega)
                    costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Id_Receta") & "and idBodega = " & id_bodega)
                    'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
                    existencia_bod = existencia
                    existencia = existencia - (dt.Rows(i).Item("cCantidad") * dt.Rows(i).Item("disminuye"))
                    existencia = Math.Round(existencia, 2)
                    Costo_Movimiento = (dt.Rows(i).Item("cCantidad") * dt.Rows(i).Item("disminuye")) * costo_promedio
                    Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                    saldo_final = saldo_final - Costo_Movimiento
                    existencia_bod_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dt.Rows(i).Item("Id_Receta") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
                    'INGRESA AL KARDEX
                    actualiza_kardex(conexion, dt.Rows(i).Item("Id_Receta"), (dt.Rows(i).Item("cCantidad") * dt.Rows(i).Item("disminuye")), existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dt.Rows(i).Item("Bodega"), costo_promedio, saldo_final)

                    If UltimoCosto = False Then
                        costo_real = dt.Rows(i).Item("disminuye") * costo_promedio
                    Else
                        CostoUltimo = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Costo from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Id_Receta"))
                        costo_real = dt.Rows(i).Item("disminuye") * CostoUltimo
                    End If

                    If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
                    cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal.costo_real = " & costo_real & "  where ComandaTemporal.id = " & dt.Rows(i).Item("id_detalle"))
                Else
                    Dim dt_rec As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dt.Rows(i).Item("Id_Receta") & ") ", dt_rec)
                    Dim disminuye As Double = dt.Rows(i).Item("disminuye")
                    If disminuye <= 0 Then
                        disminuye = 1
                    End If
                    Dim CostoR As Decimal = DescargaInvRecursivo(dt.Rows(i).Item("Id_Receta"), (dt.Rows(i).Item("cCantidad") * disminuye) / dt_rec.Rows(0).Item("Porciones"))

                    If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
                    cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal.costo_real = " & CostoR & "  where ComandaTemporal.id = " & dt.Rows(i).Item("id_detalle"))

                End If
            Next

            '*******************************************FIN ARTICULOS*********************************************************************************************************

            'ya con los que son articulos rebajados ahora vamos con los que son recetas... aunq los hubiera rebajado todos de un solo pero me dan pere...
            'es mas hagamos un cambio... vamos a cambiarle el tipo de articulo a los acompañamientos y a los modificadores forzados para q aparescan aca y no estorben
            'x los momentos trabajemos x separado asi:
            'el tipo 2 son articulos
            'el tipo 1 recetas

            '*******************************************PRINCIPIO RECETAS*********************************************************************************************************
            '  sql.CommandText = "select menu_restaurante.id_receta, menu_restaurante.disminuye, comandatemporal.ccantidad, menu_restaurante.bodega, comandatemporal.id as id_detalle from comandatemporal inner join menu_restaurante on comandatemporal.cproducto = menu_restaurante.id_menu where comandatemporal.comandado = 0 and  comandatemporal.cmesa =" & idMesa & " and menu_restaurante.tipo=1 and (ccantidad - impreso <> 0)"
            ' cFunciones.spCargarDatos(sql, dt)
            '  For i As Integer = 0 To dt.Rows.Count - 1
            'Dim costoxreceta As Double
            ' ' Dim dism As Double = dt.Rows(i).Item("disminuye")
            ' If dism <= 0 Then
            '     dism = 1
            '  End If
            'costoxreceta = CalculaCostoReceta(dt.Rows(i).Item("id_receta")) * dism * dt.Rows(i).Item("ccantidad") ', rs("disminuye"), rs("ccantidad"), rs("bodega"), false)
            'y se manda a actualizar a la tabla temporal el costo real de la receta.
            'diego
            '     If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
            '    cConexion.SlqExecute(Conexion1, "update comandatemporal set comandatemporal.costo_real = " & costoxreceta & " where comandatemporal.id = " & dt.Rows(i).Item("id_detalle"))

            '  Next


            ' cConexion.DesConectar(Conexion1)
            '*******************************************FIN RECETAS*********************************************************************************************************

            '*******************************************PRINCIPIO ACOMPAÑAMIENTOS*********************************************************************************************************
            sql.CommandText = "SELECT dbo.ComandasActivas.id, dbo.ComandasActivas.cProducto, dbo.Acompañamientos_Menu.idBodegaDescarga,dbo.Acompañamientos_Menu.Tipo,Acompañamientos_Menu.ID_Receta FROM dbo.Acompañamientos_Menu INNER JOIN dbo.ComandasActivas ON dbo.Acompañamientos_Menu.Id = dbo.ComandasActivas.cProducto where ComandasActivas.cCodigo =" & numeroComanda
            cFunciones.spCargarDatos(sql, dt)
            Dim costo_acomp As Double
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("Tipo") = 1 Then
                    costo_acomp = CalculaCostoReceta(dt.Rows(i).Item("ID_Receta")) ', 1, 1, rs_Acompa("idBodegaDescarga"), True)
                Else
                    costo_acomp = CalculaCostoArticulo(dt.Rows(i).Item("cProducto"), dt.Rows(i).Item("idBodegaDescarga"), "", 0)

                End If
                If conexion.State = ConnectionState.Closed Then conexion.Open()
                cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal set  Restaurante.dbo.ComandaTemporal.costo_real = " & costo_acomp & "  where Restaurante.dbo.ComandaTemporal.id = " & dt.Rows(i).Item("id"))
            Next


        Catch ex As Exception
            MessageBox.Show("Error al actualizar el inventario: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "DescargaInventario", ex.Message)
        End Try
    End Sub

    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer) As Double

        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select * From Recetas WHere ID = " & codigo_receta, dt)
            Dim porc As Double = dt.Rows(0).Item("Porciones")

            Dim cnx As New SqlClient.SqlConnection

            Dim exe As New ConexionR

            Dim calculando_costo As Double

            Dim dsDetalle_RecetaAnidada As New DataSet

            Dim fila_receta As DataRow

            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            If cnx.State = ConnectionState.Closed Then cnx.Open()

            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")

            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows

                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta(fila_receta("Codigo")) * fila_receta("Cantidad")

                Else
                    calculando_costo += (CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("idreceta")) * fila_receta("Cantidad"))

                End If
            Next

            Dim especies As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("Especies")) / 100)

            Dim desaprovechamiento As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("aprovechamiento")) / 100)

            calculando_costo = (calculando_costo + especies + desaprovechamiento) '/ dt.Rows(0).Item("Porciones")

            Return calculando_costo / porc

        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" & ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
            cFunciones.spEnviar_Correo(Me.Name, "CalculaCostoReceta", ex.Message)
        End Try

    End Function


    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit FROM VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & Receta)
            If costoXarticulo = 0 Then
                'Cambio por Diego lo deshabilite para que no salga donde los clientes
                'MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizará el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
                costoXarticulo = 0
            End If
            Return costoXarticulo
            cnx.Close()
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
            cFunciones.spEnviar_Correo(Me.Name, "CalculaCostoArticulo", ex.Message)
        End Try
    End Function
    Private Sub actualiza_kardex(ByRef conexion As SqlClient.SqlConnection, ByVal art As Integer, ByVal cantidad As Decimal, ByVal existencia_inv As Decimal, ByVal existencia_bod As Decimal, ByVal existencia_inv_act As Decimal, ByVal existencia_bod_act As Decimal, ByVal Codigo_Bodega As Integer, ByVal CostoPromedio As Double, ByVal SaldoFinal As Double)
        Try
            Dim codigo_prov, cod_moneda As Integer
            Dim costo_unit As Double
            Dim data As New DataSet
            'BUSCA CODIGO DEL PROVEEDOR, COSTO DEL ARTICULO Y CODIGO DE MONEDA
            cConexion.GetDataSet(conexion, "Select * from [Articulos x Proveedor] where CodigoArticulo=" & art, data, "Articulos")
            Dim fil As DataRow
            For Each fil In data.Tables("Articulos").Rows
                codigo_prov = fil("CodigoProveedor")
                cod_moneda = fil("Moneda")
                costo_unit = fil("UltimoCosto")
            Next
            Dim tipo As String = "CO" & Mid(User_Log.NombrePunto, 1, 1)
            'ACTUALIZAR MOVIMIENTOS EN EL KARDEX
            Dim campos As String = "Codigo, Documento, Tipo, Fecha, Exist_Ant, Cantidad, Exist_Act, Costo_Unit, Costo_Mov, Cod_Moneda, IdBodegaOrigen, IdBodegaDestino, Exist_AntBod, Exist_ActBod, Cod_Proveedor, Observaciones, Costo_Promedio, Saldo_Final"
            Dim datos As String = art & "," & numeroComanda & ",'" & tipo & "', getdate()," & existencia_inv & "," & cantidad & "," & (existencia_inv_act) & "," & CostoPromedio & "," & (cantidad * CostoPromedio) & "," & cod_moneda & "," & Codigo_Bodega & "," & Codigo_Bodega & "," & existencia_bod & "," & (existencia_bod_act) & "," & codigo_prov & ",'Comanda " & User_Log.NombrePunto & " # " & numeroComanda & "' ," & CostoPromedio & "," & SaldoFinal
            cConexion.AddNewRecord(conexion, "Kardex", campos, datos)
        Catch ex As Exception
            MessageBox.Show("Error al actualizar el kardex: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "actualiza_kardex", ex.Message)
        End Try
    End Sub

    Public Sub imprimir(ByVal NFactura As Integer, ByVal Idioma As String)
        If TipoImpres = 3 Then
            inicializarFactura(Idioma)
        End If
        imprimirFactura(NFactura, Idioma, numeroComanda)
    End Sub

    Public Sub imprimir_SERVICIO_RESTAURANTE(ByVal NFactura As Integer, ByVal Idioma As String, ByVal idventa As Integer)
        Try
            Dim Impresora As String
            Impresora = busca_impresora()
            If Impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Impresora = busca_impresora()
                If Impresora = "" Then
                    Exit Sub
                End If
            End If

            '' Cargar consecutivo provisional hacienda
            Dim conce As String = ""
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo_Hacienda FROM [Hotel].[dbo].[TB_CE_ConsecutivoProvisionalHA] where  Id_Factura = " & idventa, dt)
            If dt.Rows.Count > 0 Then

                conce = dt.Rows(0).Item("Consecutivo_Hacienda")
            End If

            If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
            ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
            End If
            CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVESR, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

            FacturaPVESR.Refresh()
            FacturaPVESR.PrintOptions.PrinterName = Impresora
            FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            FacturaPVESR.SetParameterValue(0, NFactura)
            FacturaPVESR.SetParameterValue(1, User_Log.PuntoVenta)
            FacturaPVESR.SetParameterValue(2, numeroComanda)
            FacturaPVESR.SetParameterValue(3, False)

            cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
            Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

            If nc = "1" Or nc = "2" Then
                If dt.Rows.Count > 0 Then
                    FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                    '  FacturaPVESR.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                    '  FacturaPVESR.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                    '  FacturaPVESR.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))

                Else
                    FacturaPVESR.SetParameterValue(4, "0")
                    '  FacturaPVESR.SetParameterValue(5, "")
                    '   FacturaPVESR.SetParameterValue(6, "")
                    ' FacturaPVESR.SetParameterValue(7, Comenzales)
                End If
            Else
                If dt.Rows.Count > 0 Then
                    FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                Else
                    FacturaPVESR.SetParameterValue(4, 0)
                End If
            End If
            FacturaPVESR.SetParameterValue(5, conce)
            If GetSetting("SeeSoft", "Restaurante", "CCOFactura").Equals("") Then
                CCOFactura = ""
                SaveSetting("SeeSoft", "Restaurante", "CCOFactura", "")
            Else
                CCOFactura = GetSetting("SeeSoft", "Restaurante", "CCOFactura")
            End If

            If TipoImpres = 1 Or TipoImpres = 2 Then
                FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                If Not CCOFactura.Equals("") Then

                    FacturaPVESR.PrintOptions.PrinterName = CCOFactura
                    FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)

                End If
            ElseIf TipoImpres = 3 Then
                If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                    If Not CCOFactura.Equals("") Then

                        FacturaPVESR.PrintOptions.PrinterName = CCOFactura
                        FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        FacturaPVESR.PrintToPrinter(1, True, 0, 0)

                    End If
                Else
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                    If Not CCOFactura.Equals("") Then

                        FacturaPVESR.PrintOptions.PrinterName = CCOFactura
                        FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        FacturaPVESR.PrintToPrinter(1, True, 0, 0)

                    End If
                End If
            ElseIf TipoImpres = 4 Then
                Dim PreguntaImprimeFactura As String = GetSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura")
                Dim Imprime As Boolean = True
                If PreguntaImprimeFactura.Equals("") Then
                    SaveSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura", 1)
                End If
                If PreguntaImprimeFactura.Equals("1") Then
                    If Not MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        Imprime = False
                    End If
                End If
                If Imprime Then
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                    If Not CCOFactura.Equals("") Then

                        FacturaPVESR.PrintOptions.PrinterName = CCOFactura
                        FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        FacturaPVESR.PrintToPrinter(1, True, 0, 0)

                    End If
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "imprimir_SERVICIO_RESTAURANTE", ex.Message)
        End Try

    End Sub

    'Public Sub imprimir_SERVICIO_RESTAURANTE(ByVal NFactura As Integer, ByVal Idioma As String)
    '    Dim Impresora As String
    '    Impresora = busca_impresora()
    '    If Impresora = "" Then
    '        MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
    '        Impresora = busca_impresora()
    '        If Impresora = "" Then
    '            Exit Sub
    '        End If
    '    End If
    '    If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
    '        FacturaPVESR = New RptFacturaPVE_SR   'Generica
    '    ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
    '        FacturaPVESR = New RptFacturaPVE_SR_IZ

    '    End If

    '    '
    '    CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVESR, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

    '    FacturaPVESR.Refresh()
    '    FacturaPVESR.PrintOptions.PrinterName = Impresora
    '    FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
    '    FacturaPVESR.SetParameterValue(0, NFactura)
    '    FacturaPVESR.SetParameterValue(1, User_Log.PuntoVenta)
    '    FacturaPVESR.SetParameterValue(2, numeroComanda)
    '    FacturaPVESR.SetParameterValue(3, False)
    '    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
    '    'FacturaPVESR = Nothing


    'End Sub
    Dim FilasCateTam As Integer = -1
    Dim ColumnasCateTam As Integer = -1
    Private Sub ObtenerDimension() ' SAJ 080807. REDIMESIONAMIENTO DE LSA CATEGORIAS
        Dim Conexion As New Conexion
        Dim dt As New DataTable
        Dim sql As New SqlClient.SqlCommand
        Try
            If FilasCateTam = -1 Or ColumnasCateTam = -1 Then
                ' sql.CommandText = "SELECT TOP 1 Filas, Columnas FROM Categoria_Dimesion"
                ' cFunciones.spCargarDatos(sql, dt)
                FilasCateTam = glodtsGlobal.Categoria_Dimesion.Rows(0).Item("Filas")
                ColumnasCateTam = glodtsGlobal.Categoria_Dimesion.Rows(0).Item("Columnas")
                txtFilas.Text = FilasCateTam
                TxtColumnas.Text = ColumnasCateTam
            End If


        Catch ex As Exception
            MsgBox("Error al cargar la dimesión de las categorías..", MsgBoxStyle.Information, "Alerta...")
            cFunciones.spEnviar_Correo(Me.Name, "ObtenerDimension", ex.Message)
        End Try
    End Sub

    Private Sub ObtenerDimensionCategoria(ByVal Categoria As Integer) ' SAJ 080807. REDIMESIONAMIENTO DE LSA CATEGORIAS
        Dim Conexion As New Conexion
        Dim F, C As Integer
        Dim dt As New DataTable
        Dim sql As New SqlClient.SqlCommand
        Try
            'If Inicializando Then
            Dim Row = (From i As dtsGlobal.Categorias_MenuRow In glodtsGlobal.Categorias_Menu Where i.Id = Categoria)
            ' sql.CommandText = "SELECT  Filas , Columnas FROM Categorias_Menu WHERE Id = " & Categoria
            ' cFunciones.spCargarDatos(sql, dt
            For Each r As dtsGlobal.Categorias_MenuRow In Row
                F = r.Filas
                C = r.Columnas
            Next
            ' F = Row.Item("Filas")
            ' C = Row.Item("Columnas")
            txtFilas.Text = F
            TxtColumnas.Text = C
            'End If
        Catch ex As Exception
            MsgBox("Error al cargar la dimesión de las categorías..", MsgBoxStyle.Information, "Alerta...")
            cFunciones.spEnviar_Correo(Me.Name, "ObtenerDimensionCategoria", ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxComensal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxComensal.SelectedIndexChanged
        Dim Conexion As New ConexionR
        Dim Comensal As String
        If idMesa = "" Then Exit Sub
        Try
            If Trim(numeroComanda) = vbNullString Or Comensal = Nothing Then
            Else
                Comensal = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT DISTINCT Comenzales FROM ComandasActivas WITH (NOLOCK) WHERE Estado='Activo' and cMesa = " & idMesa)
            End If

            If Comensal = Nothing Then
                Dim RowMesa = (From i As dtsGlobal.MesasRow In glodtsGlobal.Mesas Where i.Id = idMesa)
                For Each r As dtsGlobal.MesasRow In RowMesa
                    Comensal = r.Numero_Asientos
                Next
                ' Comensal = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT Numero_Asientos FROM Mesas WITH (NOLOCK) WHERE Id = " & idMesa & "")
            End If

            If ComboBoxComensal.Text = Comensal Then Exit Sub

            If numeroComanda = Nothing Then
                MessageBox.Show("No se asignado ningun artículo a la mesa")
            Else
                Conexion.UpdateRecords(Conexion.Conectar("Restaurante"), "ComandaTemporal", "Comenzales = " & Me.ComboBoxComensal.Text, "Estado='Activo' and cMesa = " & idMesa)
                comenzales = Me.ComboBoxComensal.Text
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención..")
            cFunciones.spEnviar_Correo(Me.Name, "ComboBoxComensal_SelectedIndexChanged", ex.Message)
        Finally
            Conexion.DesConectar(Conexion.SqlConexion)
        End Try
    End Sub

    Private Sub ComboBoxHabitaciones_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles ComboBoxHabitaciones.DrawItem
        Try
            If ComboBoxHabitaciones.Items(e.Index).Item("Habitacion") = vbNullString Then
                Exit Sub
            End If
            If ComboBoxHabitaciones.Items(e.Index).Item("Abierta") Then
                e.Graphics.DrawString(ComboBoxHabitaciones.Items(e.Index).Item("Habitacion"), e.Font, New SolidBrush(Color.Blue), e.Bounds)
            Else
                e.Graphics.DrawString(ComboBoxHabitaciones.Items(e.Index).Item("Habitacion"), e.Font, New SolidBrush(Color.Red), e.Bounds)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "ComboBoxHabitaciones_DrawItem", ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxHabitaciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxHabitaciones.SelectedIndexChanged
        Dim Conexion As New ConexionR
        Dim HabitacionComanda As String
        If Cargando = True Then Exit Sub
        Try
            If numeroComanda <> Nothing Then
                HabitacionComanda = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT DISTINCT Habitacion FROM ComandasActivas WITH (NOLOCK) WHERE Estado='Activo' and cMesa = " & idMesa)
                If ComboBoxHabitaciones.Text = HabitacionComanda Then
                    HabitacionAsignada = HabitacionComanda
                    Exit Sub
                Else
                    HabitacionAsignada = ComboBoxHabitaciones.Text
                    Conexion.UpdateRecords(Conexion.Conectar("Restaurante"), "ComandaTemporal", "Habitacion = " & Me.ComboBoxHabitaciones.Text, "Estado='Activo' and cMesa = " & idMesa)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención..")
            cFunciones.spEnviar_Correo(Me.Name, "ComboBoxHabitaciones_SelectedIndexChanged", ex.Message)
        Finally
            Conexion.DesConectar(Conexion.SqlConexion)
        End Try
    End Sub

    Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonRegresar.Click
        limpiaDatos()
        ObtenerDimension()
        CrearCategoriaMenu()
        cmbCuentas.Enabled = True
    End Sub

    Private Sub ToolButtonModificarComanda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonModificarComanda.Click
        RefrescaDatos()
        PanelMesas.Visible = False
        ' ComandasShow()
    End Sub
    Private Function fnMesaExpres() As Boolean
        Try
            If GetSetting("SeeSoft", "Restaurante", "MesaExpress").Equals("1") Then
                Dim RowMesa = (From i As dtsGlobal.MesasRow In glodtsGlobal.Mesas Where i.Id = idMesa)
                For Each r As dtsGlobal.MesasRow In RowMesa
                    Return r.Express
                Next
                ' Return cConexion.SlqExecuteScalar(conectadobd, " SELECT Express FROM Mesas WITH (NOLOCK) where Id = " & idMesa)

            Else
                Return False
            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnMesaExpres", ex.Message)
            Return False
        End Try
        Return False
    End Function

    Private Sub ToolButtonTerminar_nClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonTerminar.Click
        Try
            If Me.bandeTipo Then
                ToolStripButton12.Visible = IIf(GetSetting("Seesoft", "Restaurante", "1/2") = "1", True, False)
                If lblEncabezado.Text.Equals("Acompañamientos") Then
                    limpiaDatos()
                    Dim MenuSelect() As DataRow
                    MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & idGrupoT)
                    For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                        MenuSelect(i).Item("tipo") = "MENUREST"
                    Next
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    UbicarPosiciones(MenuSelect)
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    lblEncabezado.Text = "Menú"
                    Bloquear(True)
                    cmbCuentas.Enabled = True
                    Me.bandeTipo = False
                    '--------- habilitar botones -----
                    Me.ToolButtonModificarComanda.Visible = True
                    Me.ToolButtonRegresar.Visible = True
                    Me.ToolStripButton9.Visible = usaGrupo
                    Me.ToolStripButton6.Visible = IIf(fnMesaExpres() = True, False, True)
                    Me.ToolStripButton8.Visible = True
                    Me.ToolStripButtonAtras.Enabled = True
                    Me.tlsCantidad.Visible = True
                    '  Me.ToolButtonNotas.Visible = True
                    Me.ToolButtonExpress.Visible = True
                    Me.ToolButtonPreFacturar.Visible = True
                    Me.ToolButtonFacturar.Visible = True
                    Me.ToolButtonSepararCuenta.Visible = True
                    Me.ToolStripButtonTerminar.Enabled = True
                    ToolButtonPlatoFuerte.Visible = False
                    ToolButtonAlergenico.Visible = False
                    If OpcionesEspeciales Then
                        ToolButtonTerminar.Text = "Enviar"
                        ToolButtonTerminar.Image = img
                    Else
                        ToolButtonTerminar.Text = "Terminar"
                    End If
                    If ToolStripButton12.BackColor = Color.Blue = True Then
                        ToolStripButton12.BackColor = Color.Transparent
                        Me.BANDERA_MEDIO = False
                    End If
                    Exit Sub
                End If

                limpiaDatos()
                Dim RowMenu = (From i As dtsGlobal.Menu_RestauranteRow In glodtsGlobal.Menu_Restaurante Where i.Id_Menu = idMenuActivo)
                For Each r As dtsGlobal.Menu_RestauranteRow In RowMenu
                    tieneAcompa = r.Numero_Acompañamientos
                Next
                ' tieneAcompa = cConexion.SlqExecuteScalar(conectadobd, "Select Numero_Acompañamientos from Menu_Restaurante WITH (NOLOCK) where id_menu=" & idMenuActivo)

                If CDbl(tieneAcompa) > 0 Then limpiaDatos()
                If CDbl(tieneAcompa) > 0 Then
                    lblEncabezado.Text = "Acompañamientos"
                    limpiaDatos()
                    Bloquear(True)

                    Me.bandeTipo = True
                    Me.ControlBox = True

                    Me.ToolButtonModificarComanda.Visible = False
                    Me.ToolButtonRegresar.Visible = False
                    Me.ToolStripButton9.Visible = False
                    Me.ToolStripButton6.Visible = False
                    Me.ToolStripButton8.Visible = False
                    Me.ToolStripButtonAtras.Enabled = True
                    Me.tlsCantidad.Visible = False
                    ' Me.ToolButtonNotas.Visible = False
                    Me.ToolButtonExpress.Visible = False
                    Me.ToolButtonPreFacturar.Visible = False
                    Me.ToolButtonFacturar.Visible = False
                    Me.ToolButtonSepararCuenta.Visible = False
                    Me.ToolStripButtonTerminar.Enabled = True
                    ToolStripButton12.Visible = False

                    If p_Tipo = 1 Then
                        Dim Acompanamiento() As DataRow
                        Acompanamiento = glodtsGlobal.Acompañamientos_MenuPrecio.Select("IdCategoriaMenu = " & Id_Categoria)
                        If Acompanamiento.Count > 0 Then
                            ' UbicarPosiciones("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio AS Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion")
                            UbicarPosiciones(Acompanamiento)
                        Else
                            ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
                            UbicarPosiciones(glodtsGlobal.Acompañamientos_Menu.Select())
                        End If

                        'Dim dt As New DataTable
                        ' cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio as Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion", dt)
                        ' If dt.Rows.Count > 0 Then
                        '    UbicarPosiciones("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio AS Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion")
                        ' Else
                        '    UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
                        ' End If

                    Else
                        ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
                        UbicarPosiciones(glodtsGlobal.Acompañamientos_Menu.Select())
                    End If
                    Dim MesaExpress As Boolean = fnMesaExpres()

                    ToolStripButton6.Visible = IIf(MesaExpress = True, False, IIf(OpcionesEspeciales, True, False))
                    ToolStripButton8.Visible = IIf(OpcionesEspeciales, True, False)
                    ToolButtonPlatoFuerte.Visible = IIf(MesaExpress = True, False, IIf(OpcionesEspeciales, True, False))
                    ToolButtonAlergenico.Visible = IIf(OpcionesEspeciales, True, False)
                    Exit Sub

                Else
                    Dim MenuSelect() As DataRow
                    MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & idGrupoT)
                    For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                        MenuSelect(i).Item("tipo") = "MENUREST"
                    Next
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    UbicarPosiciones(MenuSelect)
                    '   UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    lblEncabezado.Text = "Menú"
                    Bloquear(True)
                    cmbCuentas.Enabled = True
                End If
                Me.bandeTipo = False
                '--------- habilitar botones -----
                Me.ToolButtonModificarComanda.Visible = True
                Me.ToolButtonRegresar.Visible = True

                Me.ToolStripButton9.Visible = usaGrupo

                Me.ToolStripButton6.Visible = True
                Me.ToolStripButton8.Visible = True
                Me.ToolStripButtonAtras.Enabled = True
                Me.tlsCantidad.Visible = True
                '  Me.ToolButtonNotas.Visible = True
                Me.ToolButtonExpress.Visible = True
                Me.ToolButtonPreFacturar.Visible = True
                Me.ToolButtonFacturar.Visible = True
                Me.ToolButtonSepararCuenta.Visible = True
                Me.ToolStripButtonTerminar.Enabled = True
                ToolButtonPlatoFuerte.Visible = False
                ToolButtonAlergenico.Visible = False
                If OpcionesEspeciales Then
                    ToolButtonTerminar.Text = "Enviar"
                    ToolButtonTerminar.Image = img
                Else
                    ToolButtonTerminar.Text = "Terminar"
                End If
                If ToolStripButton12.BackColor = Color.Blue = True Then
                    ToolStripButton12.BackColor = Color.Transparent
                    Me.BANDERA_MEDIO = False
                End If
                Exit Sub
            End If

            If todo_comandado = False Then
                spProcesarComanda()
            Else
                ' spCorrigeEstadoMesa()
                DialogResult = DialogResult.OK
            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonTerminar_nClick", ex.Message)
        End Try
    End Sub

    Private Sub spCargarCuentas()
        Try
            cmbCuentas.Items.Clear()

            Dim sql As New SqlClient.SqlCommand
            Dim dt As New DataTable

            sql.CommandText = "select Cuenta, NombreCuenta from Tb_R_Cuentas where IdMesa = '" & idMesa & "' order by Cuenta"
            cFunciones.spCargarDatos(sql, dt)

            For i As Integer = 0 To dt.Rows.Count - 1
                cmbCuentas.Items.Add(dt.Rows(i).Item("Cuenta") & "-" & dt.Rows(i).Item("NombreCuenta"))
                cArrayCuentas.Add(dt.Rows(i).Item("Cuenta"))
            Next
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spCargarCuentas", ex.Message)
        End Try


    End Sub

    Private Sub ToolButtonSepararCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonSepararCuenta.Click
        'Es un nuevo módulo que Isaac programo para separar cuentas.
        Try
            cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Ccantidad = -1", "(cMesa = " & idMesa & " AND ccantidad = 0)  AND Comandado = 0") ' SAJ 09082007
            Me.Hide()
            Dim cf As New FormCrearCuentas
            cf.cantCuentas = cCuentas
            cf.Mesa = idMesa
            cf.NombreMesa = nombreMesa
            cf.frm = Me
            cf.currentComanda = numeroComanda
            cf.ShowDialog()
            cCuentas = cf.cantCuentas
            fac = cf.Facturada
            cmbCuentas.Items.Clear()


            If cCuentas > 0 Then
                lblCuenta.Visible = True
                cmbCuentas.Visible = True

                spCargarCuentas()

            Else
                cmbCuentas.Items.Add(0)
            End If
            cmbCuentas.SelectedIndex = 0
            '  RefrescaDatos()
            'PanelMesas.Visible = True
            '  ComandasShow()
            ' grpModifica.Visible = False


            'cConexion.GetRecorset(conectadobd, "Select * from ComandaTemporal where cCodigo=" & numeroComanda, rs)
            'If rs.Read = False Then
            '    MessageBox.Show("No tiene artículos pendientes", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            '    rs.Close()
            '    Exit Sub
            'End If
            'rs.Close()
            'Dim CCuentaS As New SeleccionaFact
            'Me.Hide()
            'CCuentaS.numerocomanda = numeroComanda
            'CCuentaS.idmesa = idMesa
            'CCuentaS.clave = clave
            'CCuentaS.ShowDialog()
            'CSeparada = CCuentaS.separada
            'If CSeparada = True And CCuentaS.hecho = True Then
            '    If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            '        imprimir(CCuentaS.NFactura, "espanol")
            '    Else
            '        imprimir(CCuentaS.NFactura, "Ingles")
            '    End If
            'End If
            'CCuentaS.Dispose()
        Catch ex As Exception
            MessageBox.Show("Error al cargar los datos", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonSepararCuenta_Click", ex.Message)
        End Try
    End Sub

    Private Sub ToolButtonPreFacturar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonPreFacturar.Click
        'prefacturar()
        If GetSetting("SeeSoft", "Restaurante", "Rapido") = "1" Then
            Dim impresora = Me.busca_impresora
            prefacturaRapida(impresora)
            Exit Sub
        End If

        prefacturar()
    End Sub

    'MWMWMWMWMWMWMWMWMWMWMWMWMWMMWMWMWMWMWMWMWMWMWMWMWMWMWMMWMWMWMWMWMWMWMWMWMWMWMWMWMMWMWMWMWMWMWMWMWMWMWMWMWMWMMWMWMWMWMWMWMWMWMWMWMWMWMWMMWMWMWMWMWMWMWMWMWMWMWMWMWM
    'Impresion rapida de una prefactura..
    Sub prefacturaRapida(ByVal Impresora As String)
        Try

            Dim Subtotal As Double = Me.txtSubTotal.Text
            Dim descuento As Double = Me.txtDesc.Text
            Dim total As Double = Me.txtTotal.Text

            Dim impV As Double = Me.txtIVentas.Text
            Dim impS As Double = Me.txtIServicio.Text

            If Me.banderaAplicaIV = False Then
                impV = 0
            End If
            If Me.banderaAplicaIS = False Then
                impS = 0
            End If




            Dim etiquetaTotal2 As String = ""
            Dim simbolo As String = ""
            Dim totalMoneda2 As Double = 0

            'CALCULAS TOTALES 2
            Dim moned As String
            Dim monedaRestaurante As Integer = CInt(cConexion.SlqExecuteScalar(conectadobd, "Select MonedaRestaurante from hotel.dbo.configuraciones"))
            Dim moneResta As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select valorCompra, MonedaNombre, Simbolo from moneda where CodMoneda ='" & monedaRestaurante & "'", moneResta, GetSetting("SeeSoft", "Seguridad", "Conexion"))
            Dim moneOtra As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select valorCompra, MonedaNombre, Simbolo from moneda where CodMoneda <> " & monedaRestaurante & "", moneOtra, GetSetting("SeeSoft", "Seguridad", "Conexion"))
            If moneOtra.Rows.Count > 0 And moneResta.Rows.Count > 0 Then

                etiquetaTotal2 = moneOtra.Rows(0).Item("MonedaNombre") & ": "
                simbolo = moneOtra.Rows(0).Item("Simbolo")
                totalMoneda2 = total * moneResta.Rows(0).Item("valorCompra") / moneOtra.Rows(0).Item("valorCompra")

            End If

            If Me.cCuentas <= 1 Then

                Dim f As New ClassImpresionTipoFac : f.nFac = numeroComanda : f.impresora = Impresora : f.nSalonero = nUsu : f.mesa = nombreMesa : f.nombrePuntoVenta = User_Log.NombrePunto
                f.preF = True
                If User_Log.NombrePunto = "STEAK HOUSE" Or User_Log.NombrePunto = "SEAFOOD" Or User_Log.NombrePunto = "CABALLO BLANCO" Then
                    f.cargoHabitacion = False
                End If

                f.etiquetaTotal2 = etiquetaTotal2
                f.Simbolo2 = simbolo
                f.totalMoneda2 = totalMoneda2
                f.comenzales = Me.ComboBoxComensal.Text

                If fnObtenerCuenta(Me.cmbCuentas.Text) = "" Then
                    f.cuenta = 0
                Else
                    f.cuenta = fnObtenerCuenta(Me.cmbCuentas.Text)
                End If

                f.Express = _SoyExpress
                    f.Subtotal = Subtotal
                    f.impV = impV
                    f.impS = impS
                    f.descuento = descuento
                    f.total = total
                f.Porcdescuento = PorcDescuentoAcumulada


                f.cargaDatos()

            Else
                If MsgBox("Hay mas de una cuenta, Desea imprimir todas cuentas?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then

                    For i As Integer = 1 To Me.cCuentas
                        Dim f As New ClassImpresionTipoFac : f.nFac = numeroComanda : f.impresora = Impresora : f.nSalonero = nUsu : f.mesa = nombreMesa : f.nombrePuntoVenta = User_Log.NombrePunto
                        f.preF = True
                        If User_Log.NombrePunto = "STEAK HOUSE" Or User_Log.NombrePunto = "SEAFOOD" Then
                            f.cargoHabitacion = False
                        End If

                        Me.cmbCuentas.SelectedItem = i

                        Subtotal = Me.txtSubTotal.Text
                        descuento = Me.txtDesc.Text
                        total = Me.txtTotal.Text
                        impV = Me.txtIVentas.Text
                        impS = Me.txtIServicio.Text

                        totalMoneda2 = total * moneResta.Rows(0).Item("valorCompra") / moneOtra.Rows(0).Item("valorCompra")

                        f.etiquetaTotal2 = etiquetaTotal2
                        f.totalMoneda2 = totalMoneda2
                        f.comenzales = Me.ComboBoxComensal.Text
                        f.cuenta = i
                        f.Subtotal = Subtotal
                        f.Simbolo2 = simbolo
                        f.impV = impV
                        f.impS = impS
                        f.descuento = descuento
                        f.Porcdescuento = PorcDescuentoAcumulada
                        f.total = total
                        f.cargaDatos()
                    Next

                Else
                    Dim f As New ClassImpresionTipoFac : f.nFac = numeroComanda : f.impresora = Impresora : f.nSalonero = nUsu : f.mesa = nombreMesa : f.nombrePuntoVenta = User_Log.NombrePunto
                    f.preF = True
                    If User_Log.NombrePunto = "STEAK HOUSE" Or User_Log.NombrePunto = "SEAFOOD" Then
                        f.cargoHabitacion = False
                    End If
                    f.comenzales = Me.ComboBoxComensal.Text
                    f.cuenta = fnObtenerCuenta(Me.cmbCuentas.Text)
                    f.etiquetaTotal2 = etiquetaTotal2
                    f.totalMoneda2 = totalMoneda2
                    f.Subtotal = Subtotal
                    f.Simbolo2 = simbolo
                    f.impV = impV
                    f.impS = impS
                    f.descuento = descuento
                    f.Porcdescuento = PorcDescuentoAcumulada
                    f.total = total
                    f.cargaDatos()
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "prefacturaRapida", ex.Message)
        End Try
    End Sub

    'MWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWMMWMWMWMWMWM
    'FUncion que pre-factura..
    Sub prefacturar()
        Dim monto As Double
        Dim bandera As Boolean = False
        Dim valorVenta As Double
        Dim _salonero() As String
        Dim monedaRestaurante As Integer
        Dim PreFactura1
        Try
            If RecargarReportes Then
                inicializarPreFactura()
            End If
            'Hacemos que se muestre la prefactura rapida pro defecto..
            'Establecemos el nombre de la mesa
            spProcesarComanda()
            Me.mesa.obtenerMesa(idMesa)
            Me.mesa.cambiarNombreMesa(Me.txt_NomMesa.Text)
            If GetSetting("SeeSoft", "Restaurante", "Rapido") = "1" Then
                prefacturaRapida(busca_impresora())

                Dim cxx As New Conexion
                cxx.Conectar("Restaurante")
                cxx.SlqExecute(cxx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)
                cxx.SlqExecute(cxx.sQlconexion, "INSERT INTO SolicitudPrefactura (Usuario, IdMesa, Comanda , NombreMesa,  Total) VALUES ('" & Me.usuario & "', " & Me.idMesa & ", " & Me.numeroComanda & ", '" & Me.nombreMesa & "', " & CDbl(Me.txtTotal.Text) & ")")
                cxx.DesConectar(cxx.sQlconexion)
                Exit Sub

            End If

            If Trim(lblSalonero.Text) = vbNullString Then Exit Sub

            ' bandera = cConexion.SlqExecuteScalar(conectadobd, "select monedaPrefactura from hotel.dbo.configuraciones")
            bandera = glodtsGlobal.Configuraciones(0).monedaPrefactura
            If bandera = True Then
                Dim frmBuscador As New Buscar
                frmBuscador.Text = "Elija Moneda a Prefacturar"
                frmBuscador.sqlstring = "select MonedaNombre, ValorVenta from moneda"
                frmBuscador.campo = "MonedaNombre"
                frmBuscador.ShowDialog()
                If Trim(frmBuscador.descrip) <> vbNullString Then
                    If frmBuscador.descrip = "CANCELAR" Then Exit Sub
                    valorVenta = CDbl(frmBuscador.descrip)
                    monto = CDbl(cConexion.SlqExecuteScalar(conectadobd, "select ValorVenta from moneda as m, hotel.dbo.configuraciones as h where m.codMoneda = h.OpcionFacturacion")) / valorVenta
                    cConexion.SlqExecute(conectadobd, "Update configuraciones set prefactura = " & monto)
                End If
            End If

            Dim moned As String

            ' monedaRestaurante = CInt(cConexion.SlqExecuteScalar(conectadobd, "Select MonedaRestaurante from hotel.dbo.configuraciones"))
            monedaRestaurante = glodtsGlobal.Configuraciones(0).MonedaRestaurante
            moned = CDbl(cConexion.SlqExecuteScalar(conectadobd, "Select valorCompra from moneda where CodMoneda='" & monedaRestaurante & "'"))

            'Dim cedula As String
            '' cedula = cConexion.SlqExecuteScalar(conectadobd, "SELECT Empresa FROM CONFIGURACIONES")
            'cedula = glodtsGlobal.Configuraciones(0).Empresa

            'If cedula.Contains("HOTEL") And Not (cedula.Contains("BRAMA")) Then
            '    PreFactura1 = New Pre_Factura_Hotel
            '    CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura1, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            'Else
            '    'If MsgBox("Ver en Ingles?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            '    '    PreFactura1 = PreFacturaIng
            '    'Else
            '    PreFactura1 = PreFactura
            '    'End If

            'End If
            PreFactura1 = PreFactura

            PreFactura1.PrintOptions.PrinterName = Me.busca_impresora
            If PreFactura1.PrintOptions.PrinterName = "" Then
                Exit Sub
            End If

            PreFactura1.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            If Me.cCuentas <= 1 Then

                PreFactura1.refresh()

                PreFactura1.SetParameterValue(0, idMesa)
                PreFactura1.SetParameterValue(1, User_Log.PuntoVenta)
                PreFactura1.SetParameterValue(2, CDbl(fnObtenerCuenta(cmbCuentas.Text)))

                _salonero = lblSalonero.Text.Split("-")

                Dim cuenta As Integer = 0
                cuenta = CInt(cmbCuentas.Text)

                PreFactura1.SetParameterValue(3, _salonero(0))
                PreFactura1.SetParameterValue(4, valorVenta)
                PreFactura1.SetParameterValue(5, monedaRestaurante)
                PreFactura1.SetParameterValue(6, cuenta)


                PreFactura1.PrintToPrinter(1, True, 0, 0)

            Else
                If MsgBox("Hay mas de una cuenta, Desea imprimir todas cuentas?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then

                    For i As Integer = 0 To cArrayCuentas.Count - 1
                        PreFactura1.refresh()
                        PreFactura1.SetParameterValue(0, idMesa)
                        PreFactura1.SetParameterValue(1, User_Log.PuntoVenta)
                        PreFactura1.SetParameterValue(2, cArrayCuentas.Item(i).ToString)

                        _salonero = lblSalonero.Text.Split("-")

                        PreFactura1.SetParameterValue(3, _salonero(0))
                        PreFactura1.SetParameterValue(4, valorVenta)
                        PreFactura1.SetParameterValue(5, monedaRestaurante)
                        PreFactura1.SetParameterValue(6, cmbCuentas.Items(i).ToString)

                        PreFactura1.PrintToPrinter(1, True, 0, 0)
                    Next

                Else
                    PreFactura1.refresh()
                    PreFactura1.SetParameterValue(0, idMesa)
                    PreFactura1.SetParameterValue(1, User_Log.PuntoVenta)
                    PreFactura1.SetParameterValue(2, CDbl(fnObtenerCuenta(cmbCuentas.Text)))

                    _salonero = lblSalonero.Text.Split("-")

                    PreFactura1.SetParameterValue(3, _salonero(0))
                    PreFactura1.SetParameterValue(4, valorVenta)
                    PreFactura1.SetParameterValue(5, monedaRestaurante)
                    PreFactura1.SetParameterValue(6, CDbl(fnObtenerCuenta(cmbCuentas.Text)))
                    ' CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura, True, conectadobd.ConnectionString)
                    PreFactura1.PrintToPrinter(1, True, 0, 0)

                End If
            End If
            '-------------------------
            Dim cx As New Conexion
            cx.Conectar("Restaurante")
            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)
            cx.SlqExecute(cx.sQlconexion, "INSERT INTO SolicitudPrefactura (Usuario, IdMesa, Comanda , NombreMesa,  Total) VALUES ('" & Me.usuario & "', " & Me.idMesa & ", " & Me.numeroComanda & ", '" & Me.nombreMesa & "', " & CDbl(Me.txtTotal.Text) & ")")
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "prefacturar", ex.Message)
        Finally
            'PreFactura1 = Nothing
            ' If rs.IsClosed = False Then
            'rs.Close()
            ' End If
            If bandera = True Then
                cConexion.SlqExecute(conectadobd, "Update configuraciones set prefactura = 1")
            End If
            Close()
        End Try
    End Sub

    Private Sub spFacturacion()
        Dim Print As New PrintDocument
        Print.PrinterSettings.PrinterName = "FACT"


        AddHandler Print.PrintPage, AddressOf print_PrintPage
        ' indicamos que queremos imprimir
        Dim prtPrev As New PrintPreviewDialog
        prtPrev.Document = Print


        prtPrev.Text = "Previsualizar documento"
        prtPrev.ShowDialog()


    End Sub

    Private Sub print_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        ' Este evento se producirá cada vez que se imprima una nueva página
        ' imprimir HOLA MUNDO en Arial tamaño 24 y negrita

        ' imprimimos la cadena en el margen izquierdo
        Dim xPos As Single = e.MarginBounds.Left
        ' La fuente a usar
        Dim prFont As New Font("Arial", 24, FontStyle.Bold)

        ' la posición superior
        Dim yPos As Single = prFont.GetHeight(e.Graphics)

        ' imprimimos la 
        Dim stringFormat As New StringFormat()
        stringFormat.Alignment = StringAlignment.Center
        stringFormat.LineAlignment = StringAlignment.Center

        e.Graphics.DrawString(glodtsGlobal.Configuraciones(0).Empresa, New Font("Arial", 24, FontStyle.Bold), Brushes.Black, xPos, yPos, stringFormat)
        e.Graphics.DrawString(glodtsGlobal.Configuraciones(0).PersonaJuridica, New Font("Arial", 16, FontStyle.Bold), Brushes.Black, xPos, 70, stringFormat)
        e.Graphics.DrawString("Ced.Jur", New Font("Arial", 20, FontStyle.Regular), Brushes.Black, xPos, 100, stringFormat)
        e.Graphics.DrawString(glodtsGlobal.Configuraciones(0).Cedula, New Font("Arial", 20, FontStyle.Bold), Brushes.Black, 180, 100, stringFormat)

        ' indicamos que ya no hay nada más que imprimir
        ' (el valor predeterminado de esta propiedad es False)
        e.HasMorePages = False

    End Sub

    Private Sub ToolButtonFacturar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonFacturar.Click
        facturame()
    End Sub

    Dim HiloCargaReportesFactura As Thread

    Sub spRecargarReportesFactura()
        Try
            If RecargarReportes Then
                If TipoImpres = 1 Then
                    inicializarFactura("espanol")
                ElseIf TipoImpres = 2 Then
                    inicializarFactura("Ingles")
                ElseIf TipoImpres = 3 Then
                    inicializarFactura("ambos")
                ElseIf TipoImpres = 4 Then
                    inicializarFactura("espanol")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Dim fac As Boolean = False
    Sub facturame(Optional ByVal rapido As Boolean = False)
        Dim cFacturaVenta As New Factura

        Try
            HiloCargaReportesFactura = New Thread(AddressOf spRecargarReportesFactura)
            HiloCargaReportesFactura.Start()


            Dim cx As New Conexion
            cx.Conectar("Restaurante")
            '   cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)
            cx.DesConectar(cx.sQlconexion)
            ToolButtonFacturar.Enabled = False
            If todo_comandado = False Then
                spProcesarComanda()
            Else
                ' spCorrigeEstadoMesa()
            End If

            CSeparada = False
            Dim dtComandaActivas As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select * from ComandasActivas where cMesa=" & idMesa, dtComandaActivas)
            ' cConexion.GetRecorset(cConexion.Conectar, "Select * from ComandasActivas where cMesa=" & idMesa, datareader)
            Try
                If Not dtComandaActivas.Rows.Count > 0 Then
                    MessageBox.Show("No tiene artículos pendientes", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                    Exit Sub
                Else
                End If
            Catch ex As Exception
                MessageBox.Show("No tiene artículos pendientes", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                cFunciones.spEnviar_Correo(Me.Name, "facturame", ex.Message)
                Exit Sub
            End Try
            cFacturaVenta.rapido = rapido
            cFacturaVenta.idmesa = idMesa
            cFacturaVenta.nombremesa = nombreMesa
            cFacturaVenta.numeroComanda = numeroComanda
            cFacturaVenta.Comenzales = comenzales
            If _ParaLlevar = 1 Then
                cFacturaVenta.CheckBoxIS.Checked = False
            End If
            If fnObtenerCuenta(cmbCuentas.Text) = "0" Then
                cFacturaVenta.separada = False
                cFacturaVenta.numeroSeparado = 0
            Else
                cFacturaVenta.separada = True
                cFacturaVenta.numeroSeparado = fnObtenerCuenta(cmbCuentas.Text)
            End If
            cFacturaVenta.CedulaS = dtComandaActivas.Rows(0).Item("CEDULA")
            Hide()
            vuelto = 0

            cFacturaVenta.ShowDialog()
            Dim NFactura As Int64 = cFacturaVenta.NumeroFactura
            If cFacturaVenta.hecho = True Then

                'Restablecemos el nombre de la mesa..
                txt_NomMesa.Text = ""
                mesa.cambiarNombreMesa("")
                fac = True
                If cFacturaVenta.tipo_pago.Equals("CON") Then

                    If OpcionFacturacion Then
                        OpcionesdePago()
                    Else
                        ' spCorrigeEstadoMesa()
                    End If
                Else
                    'spCorrigeEstadoMesa()
                End If
                If _ParaLlevar = 1 Then
                    cConexion.DeleteRecords(conectadobd, "ComandaTemporal_Expres", "cCodigo=" & numeroComanda)
                End If

                IMPRIMIR_MEJORADO(cFacturaVenta.CheckBoxSR.Checked, NFactura, cFacturaVenta.id_ventas)

            End If
            cFacturaVenta.Dispose()
            cFacturaVenta.Close()
            If GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("") Then
                SaveSetting("SeeSoft", "Restaurante", "MostrarVuelto", "1")
            End If

            If (vuelto > 0) And GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("1") Then
                Dim fvuelto As New Vuelto
                fvuelto.txtvuelto.Text = vuelto
                fvuelto.ShowDialog()
            End If

            Close()
        Catch ex As Exception
            MessageBox.Show("Error al realizar la factura, " & ex.ToString, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "facturame", ex.Message)
        End Try
    End Sub
    Sub IMPRIMIR_MEJORADO(ByVal ServiProf As Boolean, ByVal NFactura As String, ByVal idventa As Long)
        Try
            CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True, GetSetting("SeeSOFT", "Hotel", "Conexion"))

            Dim Impresora As String
            Impresora = busca_impresora()
            If Impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Impresora = busca_impresora()
                If Impresora = "" Then
                    Exit Sub
                End If
            End If

            '' Cargar consecutivo provisional hacienda
            Dim conce As String = ""
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo_Hacienda FROM [Hotel].[dbo].[TB_CE_ConsecutivoProvisionalHA] where  Id_Factura = " & idventa, dt)
            If dt.Rows.Count > 0 Then

                conce = dt.Rows(0).Item("Consecutivo_Hacienda")
            End If

            If ServiProf Then
                If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
                    Dim factura As New cls_Facturas()
                    factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, True)
                    Exit Sub
                End If
                imprimir_SERVICIO_RESTAURANTE(NFactura, "espanol", idventa)
            Else

                If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
                    Dim factura As New cls_Facturas()
                    factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, False)
                    Exit Sub
                End If

                FacturaPVE.Refresh()
                FacturaPVE.PrintOptions.PrinterName = Impresora
                FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                FacturaPVE.SetParameterValue(0, NFactura)
                FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
                FacturaPVE.SetParameterValue(2, numeroComanda)
                FacturaPVE.SetParameterValue(3, False)

                If TipoImpres = 3 Then
                    FacturaPVEIng.Refresh()
                    FacturaPVEIng.PrintOptions.PrinterName = Impresora
                    FacturaPVEIng.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    FacturaPVEIng.SetParameterValue(0, NFactura)
                    FacturaPVEIng.SetParameterValue(1, User_Log.PuntoVenta)
                    FacturaPVEIng.SetParameterValue(2, numeroComanda)
                    FacturaPVEIng.SetParameterValue(3, False)
                End If


                cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
                Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

                If nc = "1" Or nc = "2" Then
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        FacturaPVE.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                        FacturaPVE.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                        FacturaPVE.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
                        FacturaPVE.SetParameterValue(8, conce)
                        If TipoImpres = 3 Then
                            FacturaPVEIng.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                            FacturaPVEIng.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                            FacturaPVEIng.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                            FacturaPVEIng.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
                            FacturaPVE.SetParameterValue(8, conce)
                        End If
                    Else
                        FacturaPVE.SetParameterValue(4, "")
                        FacturaPVE.SetParameterValue(5, "")
                        FacturaPVE.SetParameterValue(6, "")
                        FacturaPVE.SetParameterValue(7, comenzales)
                        FacturaPVE.SetParameterValue(8, conce)
                        If TipoImpres = 3 Then
                            FacturaPVEIng.SetParameterValue(4, "")
                            FacturaPVEIng.SetParameterValue(5, "")
                            FacturaPVEIng.SetParameterValue(6, "")
                            FacturaPVEIng.SetParameterValue(7, comenzales)
                            FacturaPVE.SetParameterValue(8, conce)
                        End If
                    End If
                Else
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        If TipoImpres = 3 Then
                            FacturaPVEIng.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        End If
                    Else
                        FacturaPVE.SetParameterValue(4, 0)
                        If TipoImpres = 3 Then
                            FacturaPVEIng.SetParameterValue(4, 0)
                        End If
                    End If
                End If
                If GetSetting("SeeSoft", "Restaurante", "CCOFactura").Equals("") Then
                    CCOFactura = ""
                    SaveSetting("SeeSoft", "Restaurante", "CCOFactura", "")
                Else
                    CCOFactura = GetSetting("SeeSoft", "Restaurante", "CCOFactura")
                End If

                If TipoImpres = 1 Or TipoImpres = 2 Then
                    FacturaPVE.PrintToPrinter(1, True, 0, 0)
                    If Not CCOFactura.Equals("") Then

                        FacturaPVE.PrintOptions.PrinterName = CCOFactura
                        FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        FacturaPVE.PrintToPrinter(1, True, 0, 0)

                    End If
                ElseIf TipoImpres = 3 Then
                    If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        FacturaPVE.PrintToPrinter(1, True, 0, 0)
                        If Not CCOFactura.Equals("") Then

                            FacturaPVE.PrintOptions.PrinterName = CCOFactura
                            FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                            FacturaPVE.PrintToPrinter(1, True, 0, 0)

                        End If
                        spImprimirComision(NFactura, Impresora)
                    Else
                        FacturaPVEIng.PrintToPrinter(1, True, 0, 0)
                        If Not CCOFactura.Equals("") Then

                            FacturaPVE.PrintOptions.PrinterName = CCOFactura
                            FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                            FacturaPVE.PrintToPrinter(1, True, 0, 0)

                        End If
                        spImprimirComision(NFactura, Impresora)
                    End If
                ElseIf TipoImpres = 4 Then
                    Dim PreguntaImprimeFactura As String = GetSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura")
                    Dim Imprime As Boolean = True
                    If PreguntaImprimeFactura.Equals("") Then
                        SaveSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura", 1)
                    End If
                    If PreguntaImprimeFactura.Equals("1") Then
                        If Not MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                            Imprime = False
                        End If
                    End If
                    If Imprime Then
                        FacturaPVE.PrintToPrinter(1, True, 0, 0)
                        If Not CCOFactura.Equals("") Then

                            FacturaPVE.PrintOptions.PrinterName = CCOFactura
                            FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                            FacturaPVE.PrintToPrinter(1, True, 0, 0)

                        End If
                        spImprimirComision(NFactura, Impresora)
                    End If


                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "IMPRIMIR_MEJORADO", ex.Message)
        End Try
    End Sub

    Private Sub spImprimirComision(ByVal _NFactura As Integer, ByVal _Impresora As String)

        Try
            If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then

                Dim cx As New Conexion
                Dim Existe As Integer = 0
                Existe = cx.SlqExecuteScalar(cx.Conectar("Hotel"), "SELECT COUNT(*) FROM tb_FD_Comision where Num_Factura = '" & _NFactura & "'")
                If Existe > 0 Then
                    Dim rpt As New rptComision1
                    rpt.Refresh()
                    rpt.PrintOptions.PrinterName = _Impresora
                    rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rpt.SetParameterValue(0, _NFactura)
                    rpt.SetParameterValue(1, User_Log.PuntoVenta)
                    rpt.PrintToPrinter(1, True, 0, 0)
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "spImprimirComision", ex.Message)
        End Try

    End Sub
    Function numeroSolicitud(ByVal cNumero As Integer) As Integer
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(solicitud), 0) AS UltimaSolicitud FROM ComandasActivas WHERE (cMesa = " & idMesa & ")", dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(0) + 1
            Else
                Return 0
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "numeroSolicitud", ex.Message)
        End Try

    End Function

    Public Sub spProcesarComanda()
        Dim dt As New DataSet
        Dim fila As DataRow
        Dim deseaImprimir As Boolean
        Try

            If (txtComanda.Text <> "") Then
                If GetSetting("SeeSoft", "Restaurante", "obligaImpresora").Equals("1") Then
                Else
                    Dim impresoraObligada As String = GetSetting("SeeSoft", "Restaurante", "obligaImpresora")
                    Me.ToolButtonTerminar.Enabled = False
                    If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("0") Then
                        deseaImprimir = True
                        If GetSetting("SeeSoft", "Restaurante", "PreguntaComanda").Equals("1") Then
                            Dim frm As New frmQuestion

                            Dim SinImprimir As Integer = cConexion.SlqExecuteScalar(cConexion.Conectar, "select count(*) from ComandasActivas where Impresora <> '' and Impreso = 0 AND Comandado = 0 and cMesa = " & idMesa)
                            If SinImprimir > 0 Then
                                frm.LblMensaje.Text = "¿Desea Imprimir la Comanda?"
                                frm.ShowDialog()
                                deseaImprimir = frm.resultado
                            Else
                                deseaImprimir = False
                            End If
                        End If
                        If deseaImprimir Then
                            If Not GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles").Equals("1") Then
                                Dim str As String = "SELECT DISTINCT dbo.SplitImpresora(Impresora) as Impresora FROM ComandasActivas WHERE  Impreso = 0 AND cMesa=" & idMesa
                                cConexion.GetDataSet(conectadobd, str, dt, "ComandaTemporal")
                                If dt.Tables("ComandaTemporal").Rows.Count > 0 Then
                                    'QUITAR COMENTARIO PARA Q IMPRIMA
                                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Ccantidad = -1", "(cMesa = " & idMesa & " AND ccantidad = 0) AND Comandado = 0 ") ' SAJ 09082007
                                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "solicitud = " & numeroSolicitud(numeroComanda), "(cMesa = " & idMesa & ") AND Impreso = 0 AND Comandado = 0")
                                    For Each fila In dt.Tables("ComandaTemporal").Rows
                                        Application.DoEvents()
                                        Try
                                            General.DeleteNombre(idMesa) 'elimina el nombre de la mesa
                                            If Not fila("Impresora").Equals("") Then
                                                If impresoraObligada.Equals("") Then
                                                    Imprime_Comanda(Verificar_Impresora_Instalada(fila("Impresora"), numeroComanda), False)
                                                Else
                                                    Imprime_Comanda(Verificar_Impresora_Instalada(impresoraObligada, numeroComanda), False)
                                                End If
                                            End If
                                        Catch ex As Exception
                                            MsgBox("La impresora que indica el menú no ha sido encontrada: [" & fila("Impresora") & "]", MsgBoxStyle.Information, "Atención...")
                                            cFunciones.spEnviar_Correo(Me.Name, "spProcesarComanda linea 3122", ex.Message)
                                        End Try
                                    Next
                                End If
                            End If

                        End If
                    End If
                End If
                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Ccantidad = -1", "(cMesa = " & idMesa & " AND ccantidad = 0)  AND Comandado = 0") ' SAJ 09082007
                If GetSetting("SeeSOFT", "Restaurante", "Utiliza_Inventario") = 1 Then
                    DescargaInventario()
                End If
                If Not GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles").Equals("1") Then
                    If deseaImprimir Then
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "comandado = 1 , Impreso = cCantidad", "(cMesa = " & idMesa & ") AND (Impreso = 0) AND Comandado = 0")
                    Else
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "comandado = 1 , Impreso = cCantidad, solicitud = " & numeroSolicitud(numeroComanda), "(cMesa = " & idMesa & ") AND (Impreso = 0) AND Comandado = 0")
                    End If
                ElseIf Not deseaImprimir Then
                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "comandado = 1 , Impreso = cCantidad, solicitud = " & numeroSolicitud(numeroComanda), "(cMesa = " & idMesa & ") AND (Impreso = 0) AND Comandado = 0")
                Else
                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "comandado = 1  , solicitud = " & numeroSolicitud(numeroComanda), "(cMesa = " & idMesa & ") AND (Impreso = 0) AND Comandado = 0")
                End If

                'Try
                '	My.Computer.FileSystem.WriteAllText("C:\REPORTES\ImpLog.txt", "cCodigo: " & numeroComanda & " - " & LabelMesa.Text & " Imp:" & deseaImprimir & " FH:" & Now & " Usuario:" & usuario & vbCrLf, True)
                'Catch ex As Exception
                '	My.Computer.FileSystem.WriteAllText("C:\REPORTES\ImpLog.txt", "", False)
                'End Try
                ' actualizar()
                todo_comandado = True
                Close()
                Me.ToolButtonTerminar.Enabled = True
            Else
                todo_comandado = True
                Close()
                Me.ToolButtonTerminar.Enabled = True
            End If
        Catch ex As Exception
            Me.ToolButtonTerminar.Enabled = True
            If rs.IsClosed = False Then rs.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            cFunciones.spEnviar_Correo(Me.Name, "spProcesarComanda", ex.Message)
        Finally
        End Try
    End Sub
    Public Sub OpcionesdePago()
        Try
            Dim nFactura As Int64
            Dim dt As New DataTable

            Dim cFacturaVenta As New frmMovimientoCajaPagoAbono(User_Log.PuntoVenta)
            Dim strb As String = "SELECT top 1 * from BuscaFacturaConFiltro WITH (NOLOCK) where IdMesa = " & idMesa & " AND Proveniencia_Venta = " & User_Log.PuntoVenta & " Order by id desc"
            cFunciones.Llenar_Tabla_Generico(strb, dt)
            'cConexion.GetRecorset(cConexion.Conectar(), strb, rs)
            If dt.Rows.Count > 0 Then
                cFacturaVenta.Factura = CDbl(dt.Rows(0).Item("Num_Factura"))
                cFacturaVenta.fecha = CDate(dt.Rows(0).Item("Fecha"))
                cFacturaVenta.Total = Math.Round(CDbl(dt.Rows(0).Item("Total")), 2)
                cFacturaVenta.codmod = dt.Rows(0).Item("Cod_Moneda")
                cFacturaVenta.Tipo = "FV"
                nFactura = cFacturaVenta.Factura
            End If
            '  While rs.Read
            'cFacturaVenta.Factura = CDbl(rs("Num_Factura"))
            ' cFacturaVenta.fecha = CDate(rs("Fecha"))
            '  cFacturaVenta.Total = CDbl(rs("Total"))
            ' cFacturaVenta.codmod = rs("Cod_Moneda")
            ' cFacturaVenta.Tipo = "FV"
            ' nFactura = cFacturaVenta.Factura
            ' End While
            ' rs.Close()


            '   Dim x As String
            '  x = cConexion.SlqExecuteScalar(cConexion.Conectar(), strb)
            Try
                If dt.Rows.Count = 0 Then
                    Dim dt1 As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select * From ComandasActivas Where cMesa = " & idMesa, dt1)
                    If dt1.Rows.Count = 0 Then
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0", "id=" & idMesa)

                    End If


                    Exit Sub
                End If
            Catch ex As Exception
                cFunciones.spEnviar_Correo(Me.Name, "OpcionesdePago", ex.Message)
            End Try
            cFacturaVenta.cedu = usuario
            cFacturaVenta.nombre = NombreUsuario
            cFacturaVenta.ShowDialog()
            vuelto1 = cFacturaVenta.lblvuelto.Text
            If cFacturaVenta.Hecho = True Then
                ' spCorrigeEstadoMesa()
                Me.vuelto = cFacturaVenta.vuelto

            End If
            cFacturaVenta.Dispose()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "OpcionesdePago", ex.Message)
        End Try
    End Sub
    Sub spCorrigeEstadoMesa()
        Try
            Dim cancela As String = ""

            If OpcionFacturacion Then

                cConexion.UpdateRecords(cConexion.Conectar(), "Comanda", "Facturado=1", "idMesa=" & idMesa & " and Facturado=0")
                cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Idcomanda from Comanda WITH (NOLOCK) where Facturado=0 and IdMesa=" & idMesa)
                If cancela = "" Then
                    cancela = ""
                    cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Id from ComandasActivas WITH (NOLOCK) where cMesa=" & idMesa)
                    If cancela = "" Then
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & idMesa)
                    Else
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1", "id=" & idMesa)
                    End If
                Else
                    cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=3", "id=" & idMesa)
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spCorrigeEstadoMesa", ex.Message)
        End Try

    End Sub
    Private Sub ToolButtonNotas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonNotas.Click

        Try
            If Not GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles").Equals("1") Then
                cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set impreso=0 where Estado='Activo' and cMesa=" & idMesa)
            Else
                cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set impreso=-2 where Estado='Activo' and cMesa=" & idMesa)
            End If
            Dim dt As New DataSet
            Dim fila As DataRow
            Me.ToolButtonTerminar.Enabled = False

            If Not GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles").Equals("1") Then
                Dim str As String = "SELECT DISTINCT dbo.SplitImpresora(Impresora) as Impresora FROM ComandasActivas WHERE Impreso = 0 AND cMesa=" & idMesa  ' SAJ 24072007
                'Dim str As String = "SELECT DISTINCT dbo.SplitImpresora(Impresora) as Impresora FROM ComandaTemporal WHERE  Impresora <> '' AND cCodigo=" & numeroComanda ' DGN 140809
                cConexion.GetDataSet(conectadobd, str, dt, "ComandaTemporal")
                For Each fila In dt.Tables("ComandaTemporal").Rows
                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Ccantidad = -1", "(cMesa = " & idMesa & " AND ccantidad = 0)") ' SAJ 09082007
                    Application.DoEvents()
                    If Not fila("Impresora").Equals("") Then
                        If RecargarReportes Then
                            inicializarComandas()
                        End If
                        Imprime_Comanda(Verificar_Impresora_Instalada(fila("Impresora"), numeroComanda), True)
                    End If
                Next
                cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set impreso=cCantidad where Estado='Activo' and cMesa=" & idMesa)

                Dim cx As New Conexion
                cx.Conectar("Restaurante")
                cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)
                cx.DesConectar(cx.sQlconexion)
            End If


            'actualizar()
            Close()
            Me.ToolButtonTerminar.Enabled = True

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonNotas_Click", ex.Message)
            Me.ToolButtonTerminar.Enabled = True
            If rs.IsClosed = False Then rs.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Private Sub ToolButtonReducir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonReducir.Click
        Try
            Dim ConexionLocal As New ConexionR
            If lstProductos.SelectedItems.Count = 0 Then Exit Sub
            Dim theItem As New ListViewItem
            Dim cantidad, codigo As Integer
            theItem = lstProductos.SelectedItems(0)
            codigo = theItem.Text
            cantidad = CInt(theItem.SubItems(1).Text) - 1
            If CInt(theItem.SubItems(1).Text) <= 0 Then
                MessageBox.Show("No se puede reducir este producto", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            If cantidad = 0 Then
                MessageBox.Show("No se puede reducir mas este producto, Debe Eliminarlo", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            Else
                If RegistrarEliminacion(codigo, "", True) Then
                    cConexion.SlqExecute(conectadobd, " Update ComandaTemporal set cCantidad=" & cantidad & " where id=" & codigo)
                    If banderaDevuelPlatos = True Then
                        If GetSetting("SeeSOFT", "Restaurante", "Utiliza_Inventario") = 1 Then
                            RecargaInventario(codigo, False)
                        End If

                    End If
                End If
                ConexionLocal.SlqExecute(conectadobd, "Insert Into BitacoraComandaEliminada(Razon,IdReferencia,TipoReferencia,Usuario) Values ('Reduccion','" & codigo & "','Comanda','" & LabelUsuario.Text & "')")
            End If
            'cConexion.DesConectar(cConexion.SqlConexion)

            EstadoMesaComanda(False)
            RefrescaDatos()
            ComandasShow()
            If cantidad > 0 Then lstProductos.FindItemWithText(codigo).Selected = True
            theItem = Nothing
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonReducir_Click", ex.Message)
        End Try
    End Sub

    Private Sub ToolButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonEliminar.Click
        Try
            If lstProductos.SelectedItems.Count <= 0 Then Exit Sub
            Dim existencia As Boolean = False
            Dim theItem As New ListViewItem
            Dim codigos, acompa, MotivoElimina As String
            Dim i As Integer
            Dim ConexionLocal As New ConexionR
            For i = 0 To lstProductos.SelectedItems.Count - 1
                theItem = lstProductos.SelectedItems(i)
                codigos &= theItem.Text & ","
            Next
            codigos = codigos.Remove(codigos.Length - 1, 1)

            If lstProductos.SelectedItems.Count = 0 Then Exit Sub

            If MessageBox.Show("Desea eliminar los Items seleccionados..", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ConexionLocal.GetRecorset(ConexionLocal.Conectar(), "Select id, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Habitacion, Hora, Cantidad, Costo_Real, Impreso from ComandasActivas where  id in (" & codigos & ")", rs)
                Dim idmesa As String = ""
                Dim a, b As Integer

                While rs.Read
                    idmesa = rs("cMesa")
                    If rs("Impreso") > 0 Then
                        Dim RowPermiso = (From RP As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where RP.Id_Usuario = usuario And RP.Modulo_Nombre_Interno = Name)
                        For Each RPF As dtsGlobal.PermisosSeguridadRow In RowPermiso
                            If RPF.Accion_Eliminacion = False Then
                                MsgBox("No puede eliminar comandas, consulte con el administrador sobre sus privilegios", MsgBoxStyle.OkOnly)
                                Exit Sub
                            End If
                            Exit For
                        Next
                        '  If VSMA(usuario, Name, Secure.Delete) = False Then
                        'MsgBox("No puede eliminar comandas, consulte con el administrador sobre sus privilegios", MsgBoxStyle.OkOnly)
                        ' Exit Sub
                        'End If
                        If MotivoElimina = vbNullString Then
                            '------------------------------------------------------------------------------
                            'MOTIVO DE LA ELIMINACION - ORA
                            MotivoElimina = MotivoEliminacion()
                            If MotivoElimina = vbNullString Then
                                MsgBox("Tiene que espefificar el motivo para poder eliminar!!", MsgBoxStyle.Exclamation, "Atención...")
                                Exit Sub
                            End If
                            '------------------------------------------------------------------------------
                        End If

                        a = rs("cProducto")
                        b = rs("cCodigo")
                        RegistrarEliminacion(rs("id"), MotivoElimina)
                        If banderaDevuelPlatos = True Then
                            If GetSetting("SeeSOFT", "Restaurante", "Utiliza_Inventario") = 1 Then
                                RecargaInventario(rs("id"), False)
                            End If

                        End If
                    End If
                    'ELIMINA LOS MODIFICADORES Y ACOMPAÑAMIENTOS ASOCIADOS A UN PLATO PRINCIPAL
                    Dim dt As New DataSet
                    Dim fila As DataRow
                    cConexion.GetDataSet(conectadobd, "Select * from ComandasActivas where  cMesa=" & idmesa, dt, "ComandaT")
                    For Each fila In dt.Tables("ComandaT").Rows
                        If fila("Id") > CInt(theItem.Text) Then
                            If fila("cCantidad") <> 0 Or fila("cCantidad") < 0 Then
                                Exit For
                            Else
                                If rs("Impreso") > 0 Then
                                    RegistrarEliminacion(fila("id"), MotivoElimina)
                                    acompa &= "," & fila("id")
                                End If
                                Dim idfila As Integer
                                idfila = fila("Id")
                                cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal set Estado= 'Inactivo' where Estado ='Activo' and id=" & fila("Id"))
                            End If
                        End If
                    Next
                    EstadoMesaComanda(existencia)

                End While

                Dim CentroComandas As String = GetSetting("SeeSoft", "Restaurante", "CentroComanda")

                '                If CentroComandas.Equals("Si") Then
                '                    Dim Revisar() As String
                '                    Dim dtResivar As New DataTable
                '                    Dim EnOrden As Integer = 0

                '                    dtResivar.Clear()
                '                    Revisar = codigos.Split(",")
                '                    For c As Integer = 0 To Revisar.Count - 1
                '                        cFunciones.Llenar_Tabla_Generico("Select Id from tb_DetalleOrden where IdComandaTemporal=" & Revisar(c) & "", dtResivar)
                '                        If dtResivar.Rows.Count > 0 Then
                '                            EnOrden = 1
                '                            Exit For
                '                        End If
                '                    Next

                '                    If EnOrden = 1 Then
                '                        Try
                'ReintentarClave:

                '                            Dim rEmpleado0 As New registro
                '                            rEmpleado0.Text = "DIGITE CONTRASEÑA"
                '                            rEmpleado0.txtCodigo.PasswordChar = "*"

                '                            '---------------------------------------------------------------
                '                            'VERIFICA SI PIDE O NO EL USUARIO
                '                            If gloNoClave Then
                '                                clave = User_Log.Clave_Interna
                '                                rEmpleado0.iOpcion = 1
                '                            Else
                '                                rEmpleado0.ShowDialog()
                '                                clave = rEmpleado0.txtCodigo.Text
                '                            End If

                '                            '---------------------------------------------------------------
                '                            rEmpleado0.Dispose()
                '                            If rEmpleado0.iOpcion = 0 Then Exit Sub

                '                            If clave <> "" Then
                '                                'cedula = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
                '                                'If Trim(cedula) = "" Or Trim(cedula) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                '                                '    MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atención...")
                '                                '    GoTo ReintentarClave
                '                                '    Exit Sub
                '                                'End If
                '                                Dim RowPermiso = (From RP As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where RP.Id_Usuario = usuario And RP.Modulo_Nombre_Interno = Name)
                '                                For Each RPF As dtsGlobal.PermisosSeguridadRow In RowPermiso
                '                                    If RPF.Accion_Eliminacion = False Then
                '                                        MsgBox("No puede eliminar comandas, consulte con el administrador sobre sus privilegios", MsgBoxStyle.OkOnly)
                '                                        Exit Sub
                '                                    End If
                '                                    Exit For
                '                                Next
                '                            Else
                '                                GoTo ReintentarClave
                '                            End If
                '                            cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal set Estado= 'Inactivo' where Estado ='Activo' and (id in (" & codigos & "))")
                '                        Catch ex As Exception

                '                        End Try
                '                    Else
                '                        cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal set Estado= 'Inactivo' where Estado ='Activo' and (id in (" & codigos & "))")
                '                    End If
                '                Else
                '                    cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal set Estado= 'Inactivo' where Estado ='Activo' and (id in (" & codigos & "))")
                '                End If
                cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal set Estado= 'Inactivo' where Estado ='Activo' and (id in (" & codigos & "))")

                If MotivoElimina = vbNullString Then
                        ConexionLocal.SlqExecute(conectadobd, "Insert Into BitacoraComandaEliminada(Razon,IdReferencia,TipoReferencia,Usuario) Values ('Comanda Temporal','" & numeroComanda & "','Comanda','" & LabelUsuario.Text & "')")
                    Else

                        ConexionLocal.SlqExecute(conectadobd, "Insert Into BitacoraComandaEliminada(Razon,IdReferencia,TipoReferencia,Usuario) Values ('" & MotivoElimina & "','" & numeroComanda & "','Comanda','" & LabelUsuario.Text & "')")
                    End If


                    rs.Close()
                    ConexionLocal.DesConectar(ConexionLocal.SqlConexion)

                End If
                RefrescaDatos()
            ComandasShow()
            theItem = Nothing

            '------------------------------------------------------------------------------
            'IMPRIME COMPROBANTE DE ELIMINACION
            If MotivoElimina <> vbNullString Then
                Dim frm As New frmQuestion
                frm.LblMensaje.Text = "Desea Imprimir el Recibo ?"
                frm.ShowDialog()
                If frm.resultado = True Then
                    Imprime_Eliminacion(codigos & acompa)
                End If
            End If
            '------------------------------------------------------------------------------

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonEliminar_Click", ex.Message)
        End Try
    End Sub

    Private Sub EstadoMesaComanda(ByVal Existencia As Boolean)

        Try
            Dim ConexionY As New ConexionR
            Dim ExistDatos As String = ""

            ExistDatos = ConexionY.SlqExecuteScalar(ConexionY.Conectar, "Select cMesa FROM ComandasActivas GROUP BY cMesa HAVING  cMesa=" & idMesa)
            Existencia = IIf(ExistDatos = "", False, True)

            If Existencia = False Then ConexionY.UpdateRecords(ConexionY.Conectar, "Mesas", "activa=0", "id=" & idMesa)
            If Existencia = False Then
                ConexionY.DeleteRecords(ConexionY.Conectar, "DatosExpress", "id_comanda = " & numeroComanda)
                glodtsGlobal.DatosExpress.Clear()
            End If

            ConexionY.DesConectar(ConexionY.SqlConexion)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "EstadoMesaComanda", ex.Message)
        End Try

    End Sub

    Private Sub ToolButtonCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonCerrar.Click

        ToolButtonCerrar.Enabled = False
        limpiaDatos()
        Me.ObtenerDimension()
        CrearCategoriaMenu()
        ToolButtonCerrar.Enabled = True
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonCambiarPrecio.Click
        Try
            If lstProductos.SelectedItems.Count <= 0 Then
                Exit Sub
            End If

            Dim CambioPrecios As New ModificarPrecioComanda
            CambioPrecios.TxtItem.Text = lstProductos.SelectedItems(0).SubItems(0).Text
            CambioPrecios.TxtCantidad.Text = lstProductos.SelectedItems(0).SubItems(1).Text
            CambioPrecios.TxtDescripcion.Text = lstProductos.SelectedItems(0).SubItems(2).Text
            CambioPrecios.TxtPrecio.Text = lstProductos.SelectedItems(0).SubItems(3).Text
            CambioPrecios.txtComanda.Text = lstProductos.SelectedItems(0).SubItems(4).Text
            CambioPrecios.TxtProducto.Text = lstProductos.SelectedItems(0).SubItems(5).Text

            CambioPrecios.TxtImpuestoVentas.Text = Math.Round((CambioPrecios.TxtPrecio.Text) * cFunciones.ObtenerIvaProducto(lstProductos.SelectedItems(0).SubItems(5).Text) / 100, 2)
            CambioPrecios.TxtImpuestoServicio.Text = Math.Round((CambioPrecios.TxtPrecio.Text) * User_Log.ISS / 100, 2)
            CambioPrecios.TxtPrecioFinal.Text = Math.Round(CDbl(CambioPrecios.TxtPrecio.Text) + CDbl(CambioPrecios.TxtImpuestoVentas.Text) + CDbl(CambioPrecios.TxtImpuestoServicio.Text), 2)

            CambioPrecios.ShowDialog(Me)


            If CambioPrecios.Cancelado Then
                Exit Sub
            Else
                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Cprecio =" & CambioPrecios.TxtPrecio.Text, "id =" & CambioPrecios.TxtItem.Text)
            End If

            RefrescaDatos()
            ComandasShow()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton6_Click", ex.Message)
        End Try
    End Sub

    Private Sub ToolStripButton6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click

        Try
            If lblEncabezado.Text.Equals("Modificadores*") Or lblEncabezado.Text.Equals("Acompañamientos") Then

                cConexion.SlqExecute(conectadobd, "update ComandaTemporal set primero = 1 where id >= '" & IdComandaTemporal & "' and cMesa = '" & idMesa & "'")
                ComandasShow()
            Else
                If ToolStripButton6.BackColor = Color.Yellow Then
                    ToolStripButton6.BackColor = Color.Transparent
                Else
                    ToolStripButton6.BackColor = Color.Yellow
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton6_Click_1", ex.Message)
        End Try

    End Sub
    Private Function fnValidarEntrada(ByVal _Id As Integer, ByVal _descripcion As String) As Boolean
        Try
            Dim PedidoExpress As Boolean = False
            PedidoExpress = cConexion.SlqExecuteScalar(conectadobd, " SELECT Express FROM ComandasActivas WITH (NOLOCK) where Id = " & _Id)

            If PedidoExpress Then
                Dim frm As New frmQuestion2
                frm.LblMensaje.Text = "El artículo: " & _descripcion & " es un pedido Express, ¿Desea pasarlo a Entrada?"
                frm.ShowDialog()

                If frm.resultado Then
                    cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set Express=0 where id = " & _Id & "")
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnValidarEntrada", ex.Message)
        End Try
        Return True
    End Function

    Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton7.Click

        Dim theItem As New ListViewItem
        Dim codigos As String = ""
        Dim i As Integer
        Dim Descripcion As String
        Try
            For i = 0 To lstProductos.SelectedItems.Count - 1
                theItem = lstProductos.SelectedItems(i)
                Descripcion = lstProductos.SelectedItems(i).SubItems(1).Text & " " & lstProductos.SelectedItems(i).SubItems(2).Text
                If fnValidarEntrada(theItem.Text, Descripcion) Then
                    codigos &= theItem.Text & ","
                End If
            Next

            If Trim(codigos) = vbNullString Then
                Exit Sub
            End If

            codigos = codigos.Remove(codigos.Length - 1, 1)

            cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set primero=1 where id in(" & codigos & ")")
            cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set primero=0 where id not in(" & codigos & ")")
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton7_Click", ex.Message)
        End Try

        RefrescaDatos()
        ComandasShow()

        theItem = Nothing
    End Sub

    Private Sub cmbCuentas_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCuentas.SelectedValueChanged

        'RefrescaDatos()
        PanelMesas.Visible = True
        ComandasShow()
        grpModifica.Visible = False
    End Sub

    Private Sub ToolStripButton8_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
        Try

            Dim FrmAdicionales As New FrmModificadorAdicional
            FrmAdicionales.Comanda = numeroComanda
            FrmAdicionales.ShowDialog(Me)

            If FrmAdicionales.Aceptado = True Then

                'If FrmAdicionales.MontoNota > 0 Then
                '	cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & FrmAdicionales.Impresora & "'," & comenzales & "," & IIf(FrmAdicionales.MontoNota > 0, "'Principal'", "'Modificador'") & ",'" & usuario & "','" & HabitacionAsignada & "',0," & fnObtenerCuenta(cmbCuentas.Text) & "," & fnMesaExpres() & ",1")
                'Else
                '	cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & FrmAdicionales.Impresora & "'," & comenzales & "," & IIf(FrmAdicionales.MontoNota > 0, "'Principal'", "'Modificador'") & ",'" & usuario & "','" & HabitacionAsignada & "',0," & fnObtenerCuenta(cmbCuentas.Text) & "," & fnMesaExpres() & ",-1")
                'End If
                Dim Express As Boolean
                Express = fnMesaExpres()
                If _ParaLlevar = 1 Then
                    Express = True
                End If

                If FrmAdicionales.MontoNota > 0 Then
                    cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & FrmAdicionales.Impresora & "'," & comenzales & "," & "'Modificador'" & ",'" & usuario & "','" & HabitacionAsignada & "',0," & fnObtenerCuenta(cmbCuentas.Text) & "," & Express & ",-1")
                Else
                    cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & FrmAdicionales.Impresora & "'," & comenzales & "," & "'Modificador'" & ",'" & usuario & "','" & HabitacionAsignada & "',0," & fnObtenerCuenta(cmbCuentas.Text) & "," & Express & ",-1")
                End If

                'If FrmAdicionales.MontoNota > 0 Then
                '    Dim idultimoingresado As Integer = cConexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(max(id),0) FROM ComandaTemporal WITH (NOLOCK)")
                '    cConexion.SlqExecute(conectadobd, "update ComandaTemporal set cCantidad= 1 where id=" & idultimoingresado)
                'End If
                todo_comandado = False
                Me.ComandasShow()
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton8_Click_1", ex.Message)
        End Try
    End Sub

    Private Sub tlsCantidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlsCantidad.Click
        If tlsCantidad.BackColor = Color.Yellow = True Then
            tlsCantidad.BackColor = Color.Transparent
        Else
            tlsCantidad.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub ToolStripButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton9.Click
        Try
            iCategoria = 1
            NoTerminar = True
            Dim cx As New Conexion
            cx.Conectar("Restaurante")
            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & estadoAnte & " WHERE Id = " & idMesa)
            cx.DesConectar(cx.sQlconexion)

            Close()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton9_Click", ex.Message)
        End Try

    End Sub

    Private Sub ToolButtonExpress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonExpress.Click
        Dim nombred, codigod, impre, preC As String
        Dim busqueda As New Buscar
        busqueda.sqlstring = "Select * from menuBusqueda"
        busqueda.campo = "Nombre"
        busqueda.tipo = True
        busqueda.ShowDialog()

        Try
            codigod = busqueda.codigo
            nombred = busqueda.descrip
            preC = busqueda.monto


            Dim iPrimero As Object
            If (codigod.Equals(Nothing) = False) Then

                iPrimero = IIf(ToolStripButton6.BackColor = Color.Yellow, 1, 0)

                Dim valorCambio, dmoneda As Double
                preC = cConexion.SlqExecuteScalar(conectadobd, "select Precio_VentaExpressA as  Precio_VentaA from menu_restaurante where id_Menu =" & codigod)
                dmoneda = cConexion.SlqExecuteScalar(conectadobd, "select moneda from menu_restaurante where id_Menu =" & codigod)
                valorCambio = cConexion.SlqExecuteScalar(conectadobd, "select  (select valorVenta from moneda where codMoneda=" & dmoneda & ") / (select m.valorVenta from moneda as m, hotel.dbo.configuraciones as hc where m.codMoneda = hc.monedaRestaurante)")

                impre = cConexion.SlqExecuteScalar(conectadobd, "Select Impresora from Menu_Restaurante WITH (NOLOCK) where Id_MENU=" & codigod)

                AlmacenaTemporal(codigod, nombred, preC * valorCambio, impresoras, comenzales, usuario, HabitacionAsignada, iPrimero, False)

            End If
            Dim act As Integer = estadoAnte 'cConexion.SlqExecuteScalar(conectadobd, "Select activa from Mesas WITH (NOLOCK) where Id=" & idMesa)
            If act = 0 Or act = 1 Then
                estadoAnte = 1
                cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "id=" & idMesa)
            End If


            RefrescaDatos()
            ComandasShow()

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonExpress_Click", ex.Message)
            Exit Sub
        End Try
    End Sub
    Private Function fnValidarExpress(ByVal _Id As Integer, ByVal _descripcion As String) As Boolean
        Try
            Dim Primero As Boolean = False
            Dim PrecioExpress As Double = 0

            Primero = cConexion.SlqExecuteScalar(conectadobd, " SELECT primero FROM ComandasActivas WITH (NOLOCK) where Id = " & _Id)



            If Primero Then
                Dim frm As New frmQuestion2
                frm.LblMensaje.Text = "El artículo: " & _descripcion & " es un pedido de Entrada, ¿Desea pasarlo a Express?"
                frm.ShowDialog()
                If frm.resultado Then
                    cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set primero=0 where id = " & _Id & "")
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnValidarExpress", ex.Message)
        End Try
        Return True
    End Function

    Sub CambiarPrecioExpress(ByVal IdComandaTemporal As Integer, ByVal TipoPrecio As Integer, ByVal Descripcion As String) ' 1 para express y 0 para mesa
        Try
            Try
                Dim dtPrecioExpress As New DataTable
                Dim EsMedio As Boolean

                If Descripcion.Contains("1/2") Then
                    EsMedio = True
                Else
                    EsMedio = False
                End If

                If TipoPrecio = 1 Then
                    If EsMedio Then
                        cFunciones.Llenar_Tabla_Generico("Select Precio_VentaExpressM as Precio from Menu_Restaurante where Id_Menu = (Select cProducto from ComandaTemporal where id=" & IdComandaTemporal & ")", dtPrecioExpress)

                    Else
                        cFunciones.Llenar_Tabla_Generico("Select Precio_VentaExpressA as Precio from Menu_Restaurante where Id_Menu = (Select cProducto from ComandaTemporal where id=" & IdComandaTemporal & ")", dtPrecioExpress)

                    End If

                Else
                    If EsMedio Then
                        cFunciones.Llenar_Tabla_Generico("Select Precio_VentaD as Precio from Menu_Restaurante where Id_Menu = (Select cProducto from ComandaTemporal where id=" & IdComandaTemporal & ")", dtPrecioExpress)

                    Else
                        cFunciones.Llenar_Tabla_Generico("Select Precio_VentaA as Precio from Menu_Restaurante where Id_Menu = (Select cProducto from ComandaTemporal where id=" & IdComandaTemporal & ")", dtPrecioExpress)
                    End If

                End If
                If dtPrecioExpress.Rows.Count > 0 Then
                    cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set cPrecio=" & dtPrecioExpress.Rows(0).Item("Precio") & " where id = " & IdComandaTemporal & "")
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ToolStripButton10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton10.Click
        Dim theItem As New ListViewItem
        Dim codigos As String = ""
        Dim i As Integer
        Dim Descripcion As String

        Try
            If lstProductos.SelectedItems.Count = 0 Then
                For a As Integer = 0 To lstProductos.Items.Count - 1
                    theItem = lstProductos.Items(a)
                    CambiarPrecioExpress(theItem.Text, 0, lstProductos.Items(a).SubItems(2).Text)
                Next

                cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set Express=0 where Estado='Activo' and cMesa = " & idMesa)
                GoTo Fin
                Exit Sub
            End If


            For i = 0 To lstProductos.SelectedItems.Count - 1
                theItem = lstProductos.SelectedItems(i)
                Descripcion = lstProductos.SelectedItems(i).SubItems(1).Text & " " & lstProductos.SelectedItems(i).SubItems(2).Text
                If fnValidarExpress(theItem.Text, Descripcion) Then
                    CambiarPrecioExpress(theItem.Text, 1, lstProductos.SelectedItems(i).SubItems(2).Text)
                    codigos &= theItem.Text & ","
                End If
            Next
            codigos = codigos.Remove(codigos.Length - 1, 1)

            cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set Express=1 where id in(" & codigos & ")")
            cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set Express=0 where id not in(" & codigos & ")")
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton10_Click", ex.Message)
        End Try

Fin:
        RefrescaDatos()
        ComandasShow()
        theItem = Nothing
    End Sub

    Private Sub lstProductos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstProductos.KeyDown
        If e.KeyValue = Keys.Delete Then
            If ToolButtonEliminar.Enabled = True Then
                ToolButtonEliminar_Click(sender, e)
            Else
                MsgBox("Usted no tiene Permisos para eliminar comandas", MsgBoxStyle.Information, "Servicios Estructurales Seesoft")
            End If
        End If
    End Sub

    Function RegistrarEliminacion(ByVal CodigoComanda As Integer, ByVal Motivo As String, Optional ByVal Reduce As Boolean = False) As Boolean

        RegistrarEliminacion = False
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable

        Try
            Dim idmesa As String = ""

            sql.CommandText = "Select cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Habitacion, Hora, Cantidad, Costo_Real, Impreso from ComandasActivas where  id = " & CodigoComanda
            cFunciones.spCargarDatos(sql, dt)

            For i As Integer = 0 To dt.Rows.Count - 1
                If Reduce = False Then
                    Dim cmd As New SqlCommand

                    cmd.CommandText = "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo, devuelveInventario) VALUES (" & CodigoComanda & "," & dt.Rows(i).Item("cProducto") & ", " & dt.Rows(i).Item("cCantidad") & ", '" & dt.Rows(i).Item("cDescripcion") & "', " & dt.Rows(i).Item("cPrecio") & ", " & dt.Rows(i).Item("cCodigo") & ", " & dt.Rows(i).Item("cMesa") & ", '" & dt.Rows(i).Item("Tipo_Plato") & "', '" & usuario & "', '" & dt.Rows(i).Item("Habitacion") & "', @Fecha, GETDATE(), " & dt.Rows(i).Item("Costo_Real") & ", '" & Motivo & "', " & CInt(Me.banderaDevuelPlatos) * -1 & ")"
                    cmd.Parameters.AddWithValue("@Fecha", CDate(dt.Rows(i).Item("Hora")))
                    cFunciones.spEjecutar(cmd, GetSetting("Seesoft", "Restaurante", "Conexion"))


                    'cConexion.SlqExecute(conectadobd, "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo, devuelveInventario) " & _
                    '                                  "VALUES (" & CodigoComanda & "," & rsEli("cProducto") & ", " & rsEli("cCantidad") & ", '" & rsEli("cDescripcion") & "', " & rsEli("cPrecio") & ", " & rsEli("cCodigo") & ", " & rsEli("cMesa") & ", '" & rsEli("Tipo_Plato") & "', '" & usuario & "', '" & rsEli("Habitacion") & "', @Fecha, GETDATE(), " & rsEli("Costo_Real") & ", '" & Motivo & "', " & CInt(Me.banderaDevuelPlatos) * -1 & ")")
                    Return True
                Else
                    If dt.Rows(i).Item("Impreso") > 0 Then
                        Motivo = MotivoEliminacion()
                        If Motivo = vbNullString Then
                            MsgBox("Tiene que espefificar el motivo para poder reducir!!", MsgBoxStyle.Exclamation, "Atención...")
                            Return False
                        End If
                        Dim cmd As New SqlCommand

                        cmd.CommandText = "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo, devuelveInventario) VALUES (" & CodigoComanda & "," & dt.Rows(i).Item("cProducto") & ", " & dt.Rows(i).Item("cCantidad") & ", '" & dt.Rows(i).Item("cDescripcion") & "', " & dt.Rows(i).Item("cPrecio") & ", " & dt.Rows(i).Item("cCodigo") & ", " & dt.Rows(i).Item("cMesa") & ", '" & dt.Rows(i).Item("Tipo_Plato") & "', '" & usuario & "', '" & dt.Rows(i).Item("Habitacion") & "', @Fecha, GETDATE(), " & dt.Rows(i).Item("Costo_Real") & ", '" & Motivo & "', " & CInt(Me.banderaDevuelPlatos) * -1 & ")"
                        cmd.Parameters.AddWithValue("@Fecha", CDate(dt.Rows(i).Item("Hora")))
                        cFunciones.spEjecutar(cmd, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        'cConexion.SlqExecute(conectadobd, "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo, devuelveInventario) " & _
                        '                                                        "VALUES (" & CodigoComanda & "," & rsEli("cProducto") & ", " & 1 & ", '" & rsEli("cDescripcion") & "', " & rsEli("cPrecio") & ", " & rsEli("cCodigo") & ", " & rsEli("cMesa") & ", '" & rsEli("Tipo_Plato") & "', '" & usuario & "', '" & rsEli("Habitacion") & "', '" & CDate(rsEli("Hora")).ToUniversalTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) & "', GETDATE() , " & rsEli("Costo_Real") & ", '" & Motivo & "', " & CInt(Me.banderaDevuelPlatos) * -1 & ")")
                        '------------------------------------------------------------------------------
                        'IMPRIME COMPROBANTE DE ELIMINACION - ORA
                        Imprime_Eliminacion(CodigoComanda)
                        '------------------------------------------------------------------------------
                    End If
                    Return True
                End If
            Next


        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "RegistrarEliminacion", ex.Message)
            MsgBox(ex.Message)
            Return False
        End Try
    End Function


    Private Sub Imprime_Eliminacion(ByVal CodigosImpri As String)
        Dim rptElimina As New rptComandaEliminados
        Dim Impresora As String

        Try
            Impresora = busca_impresora()
            If Impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Impresora = busca_impresora()
                If Impresora = "" Then
                    Exit Sub
                End If
            End If

            CrystalReportsConexion.LoadReportViewer(Nothing, rptElimina, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
            rptElimina.PrintOptions.PrinterName = Impresora
            rptElimina.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            rptElimina.RecordSelectionFormula = "{Comanda_Eliminados.Id_Temporal} in [" & CodigosImpri & "]"
            rptElimina.SetParameterValue(0, User_Log.NombrePunto)
            rptElimina.PrintToPrinter(1, True, 0, 0)

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Imprime_Eliminacion", ex.Message)
            MsgBox(ex.Message)
        Finally
            rptElimina.Dispose()
            rptElimina.Close()
            rptElimina = Nothing
        End Try
    End Sub

    Function MotivoEliminacion() As String
        Dim Motivo As New FrmCortesia
        Try
            '------------------------------------------------------------------------------
            'MOTIVO DE LA ELIMINACION - ORA
            Motivo.Text = "Eliminar Articulos de Comanda"
            Motivo.TituloModulo.Text = "Elimina Comanda"
            Motivo.CheckBox1.Visible = True
            Motivo.Label13.Visible = False
            Motivo.txtautorizado.Visible = False
            Motivo.txtautorizado.Text = "-"
            Motivo.Label1.Text = "Motivo de la Eliminación : "
            Motivo.ShowDialog()
            banderaDevuelPlatos = Motivo.CheckBox1.Checked
            If Motivo.completa = False Then
                Return ""
            Else
                Return Motivo.observaciones
            End If

            '------------------------------------------------------------------------------
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "MotivoEliminacion", ex.Message)
            MsgBox(ex.Message)
            Return ""
        Finally
            Motivo.Dispose()
        End Try
    End Function


#Region "Anular Devolviendo a Inventario"

#Region "RecargaInventario"
    Private Sub RecargarInvRecursivo(ByVal Id_Receta As Integer, ByVal cantidad As Double)
        Try
            'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
            Dim conexion As New SqlConnection
            Dim Conexion1 As New SqlConnection
            '
            Dim existencia As Double = 0
            Dim existencia_inv As Double = 0
            Dim existencia_bod As Double = 0
            Dim existencia_inv_act As Double = 0
            Dim existencia_bod_act As Double = 0
            Dim arrayBodegas(200) As Integer
            Dim dt As New DataTable
            'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
            Dim Costo_Movimiento As Double
            Dim saldo_final As Double
            Dim costo_promedio As Double
            Dim id_bodega As Integer
            Dim aumentar As Double = 0
            cFunciones.Llenar_Tabla_Generico("SELECT     Codigo, Articulo, Disminuye,cantidad,idBodegaDescarga as bodega        FROM Recetas_Detalles " &
                                        " WHERE     (IdReceta = " & Id_Receta & ")", dt)
            conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If conexion.State = ConnectionState.Closed Then conexion.Open()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("Articulo") Then
                    'EXISTENCIA EN BODEGA
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Codigo"))
                    existencia_inv = existencia
                    aumentar = dt.Rows(i).Item("disminuye") * cantidad
                    existencia = existencia + aumentar
                    existencia_inv_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dt.Rows(i).Item("Codigo"))
                    'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
                    id_bodega = dt.Rows(i).Item("bodega")
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
                    saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
                    costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
                    'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
                    existencia_bod = existencia
                    existencia = existencia + aumentar
                    existencia = Math.Round(existencia, 2)
                    Costo_Movimiento = dt.Rows(i).Item("disminuye") * costo_promedio
                    Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                    saldo_final = saldo_final + Costo_Movimiento
                    existencia_bod_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dt.Rows(i).Item("Codigo") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
                    'INGRESA AL KARDEX
                    actualiza_kardex(conexion, dt.Rows(i).Item("Codigo"), aumentar, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dt.Rows(i).Item("Bodega"), costo_promedio, saldo_final)
                    'En la tabla de conf de Restaurante se escoje si se da el último costo o costo promedio
                    'Dim Ultimocosto As Boolean
                    'Dim costo_real, costoultimo As Double
                    'UltimoCosto = cConexion.SlqExecuteScalar(Conexion1, "Select UltimoPrecio from conf")
                    'If UltimoCosto = False Then
                    '    costo_real = dt.Rows(i).Item("disminuye") * costo_promedio
                    'Else
                    '    CostoUltimo = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Costo from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & rs("Id_Receta"))
                    '    costo_real = dt.Rows(i).Item("disminuye") * CostoUltimo
                    'End If

                    'If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
                    'cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal.costo_real = " & costo_real & ", ComandaTemporal.comandado = 1 where ComandaTemporal.id = " & rs("id_detalle"))
                Else
                    Dim dt_rec As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dt.Rows(i).Item("Codigo") & ") ", dt_rec)
                    RecargarInvRecursivo(dt.Rows(i).Item("Codigo"), cantidad * (dt.Rows(i).Item("cantidad") / dt_rec.Rows(0).Item("Porciones")))

                End If

            Next

            '*******************************************FIN ARTICULOS*********************************************************************************************************

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "RecargarInvRecursivo", ex.Message)
        End Try
    End Sub
    Private Sub RecargaInventario(ByVal numerofactura As Integer, ByVal venta As Boolean)
        Try
            'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
            Dim conexion As New SqlConnection
            Dim Conexion1 As New SqlConnection
            Dim existencia As Double = 0
            Dim existencia_inv As Double = 0
            Dim existencia_bod As Double = 0
            Dim existencia_inv_act As Double = 0
            Dim existencia_bod_act As Double = 0
            Dim arrayBodegas(200) As Integer
            Dim rs_recarga As SqlDataReader
            'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
            Dim Costo_Movimiento As Double
            Dim saldo_final As Double
            Dim costo_promedio As Double
            Dim id_bodega As Integer
            Dim costo_real As Double
            If venta Then
                cConexion.GetRecorset(conectadobd, "SELECT Menu_Restaurante.Id_Receta,Menu_Restaurante.Tipo, Menu_Restaurante.disminuye, Menu_Restaurante.Cantidad as cantRe, Menu_Restaurante.bodega, Ventas_Detalle.Codigo, Ventas_Detalle.Cantidad,Ventas_Detalle.Id_Factura  FROM Menu_Restaurante INNER JOIN Ventas_Detalle ON Menu_Restaurante.Id_Menu = Ventas_Detalle.Codigo  WHERE(Ventas_Detalle.Id_Factura = " & numerofactura & ")", rs_recarga) 'SAJ x aqui paso, el q escribe los rotulos!
            Else
                cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, Menu_Restaurante.Cantidad as cantRe, ComandasActivas.cCantidad as Cantidad, Menu_Restaurante.bodega, ComandasActivas.id as id_detalle, Menu_Restaurante.Tipo from ComandasActivas inner join Menu_Restaurante on ComandasActivas.cProducto = Menu_Restaurante.Id_Menu where ComandasActivas.comandado = 1 AND ComandasActivas.Id =" & numerofactura & " ", rs_recarga) 'SAJ x aqui paso, el q escribe los rotulos!
            End If

            conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If conexion.State = ConnectionState.Closed Then conexion.Open()
            While rs_recarga.Read
                If rs_recarga("Tipo") = 2 Then
                    'EXISTENCIA EN BODEGA
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & rs_recarga("Id_Receta"))
                    existencia_inv = existencia
                    'existencia = existencia + (rs_recarga("Cantidad") * rs_recarga("disminuye"))
                    existencia = existencia + rs_recarga("disminuye")
                    existencia_inv_act = existencia
                    'LE AUMENTA LA EXISTENCIA A LA BODEGA CENTRAL
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & rs_recarga("Id_Receta"))
                    'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
                    id_bodega = rs_recarga("bodega")
                    existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
                    saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & "and idBodega = " & id_bodega)
                    costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & "and idBodega = " & id_bodega)
                    'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
                    existencia_bod = existencia
                    existencia = existencia + rs_recarga("disminuye")
                    existencia = Math.Round(existencia, 2)
                    Costo_Movimiento = rs_recarga("disminuye") * costo_promedio
                    Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
                    saldo_final = saldo_final + Costo_Movimiento
                    existencia_bod_act = existencia
                    'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
                    cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & rs_recarga("Id_Receta") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
                    'INGRESA AL KARDEX
                    actualiza_kardex(conexion, rs_recarga("Id_Receta"), -1 * rs_recarga("disminuye"), existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, rs_recarga("Bodega"), costo_promedio, saldo_final)
                    costo_real = rs_recarga("disminuye") * costo_promedio
                    If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
                    cConexion.SlqExecute(Conexion1, "Update ComandaTemporal_Expres set ComandaTemporal_Expres.costo_real = " & costo_real & ", ComandaTemporal_Expres.comandado = 1 where ComandaTemporal_Expres.id = " & rs_recarga("id_detalle"))
                Else
                    Dim dt_rec As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & rs_recarga("Id_Receta") & ") ", dt_rec)
                    Dim disminuye As Double = rs_recarga("disminuye")
                    If disminuye > 0 Then
                        disminuye = rs_recarga("disminuye")
                    Else
                        disminuye = rs_recarga("cantRe")
                    End If
                    RecargarInvRecursivo(rs_recarga("Id_Receta"), (rs_recarga("Cantidad") * disminuye) / dt_rec.Rows(0).Item("Porciones"))

                End If

            End While
            rs_recarga.Close()
            '*******************************************FIN ARTICULOS*********************************************************************************************************

            'ya con los que son articulos rebajados ahora vamos con los que son recetas... aunq los hubiera rebajado todos de un solo pero me dan pere...
            'es mas hagamos un cambio... vamos a cambiarle el tipo de articulo a los acompañamientos y a los modificadores forzados para q aparescan aca y no estorben
            'x los momentos trabajemos x separado asi:
            'el tipo 2 son articulos
            'el tipo 1 recetas
            '*******************************************PRINCIPIO RECETAS**************************z*******************************************************************************
            Dim rs_rect As SqlDataReader
            cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND  ComandaTemporal_Expres.cMesa =" & idMesa & " And Menu_Restaurante.Tipo=1 And (cCantidad - Impreso <> 0)", rs_rect) 'SAJ 
            While rs_rect.Read
                Dim CostoXreceta As Double
                Dim dism As Double = rs_rect("disminuye")
                If dism <= 0 Then
                    dism = 1
                End If
                CostoXreceta = CalculaCostoReceta(rs_rect("Id_Receta")) * dism * rs_rect("cCantidad") ', rs_rect("bodega"), False)
                'y se manda a actualizar a la tabla temporal el costo real de la receta.
                'Diego
                If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
                cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal_Expres.costo_real = " & CostoXreceta & ",ComandaTemporal.comandado = 1 where ComandaTemporal.id = " & rs_rect("id_detalle"))
            End While
            cConexion.DesConectar(Conexion1)
            rs_rect.Close()
            '*******************************************FIN RECETAS*********************************************************************************************************

            '*******************************************PRINCIPIO ACOMPAÑAMIENTOS*********************************************************************************************************
            Dim rs_Acompa As SqlDataReader
            cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id, dbo.ComandaTemporal_Expres.cProducto, dbo.Acompañamientos_Menu.idBodegaDescarga,dbo.Acompañamientos_Menu.Tipo,Acompañamientos_Menu.ID_Receta FROM dbo.Acompañamientos_Menu INNER JOIN dbo.ComandaTemporal_Expres ON dbo.Acompañamientos_Menu.Id = dbo.ComandaTemporal_Expres.cProducto where ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Acompa)

            Dim costo_acomp As Double
            While rs_Acompa.Read
                If rs_Acompa("Tipo") = 1 Then
                    costo_acomp = Me.CalculaCostoRecetaanulado(rs_Acompa("ID_Receta"), 1, 1, rs_Acompa("idBodegaDescarga"), True)
                Else
                    costo_acomp = CalculaCostoArticuloanulado(0, rs_Acompa("cProducto"), 1, 1, rs_Acompa("idBodegaDescarga"), "", True)
                End If
                If conexion.State = ConnectionState.Closed Then conexion.Open()

                cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.costo_real = " & costo_acomp & ", Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Acompa("id"))
            End While
            rs_Acompa.Close()
            '*******************************************FIN ACOMPAÑAMIENTOS*********************************************************************************************************

            'Dim rs_Modifi As SqlDataReader
            'cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id FROM dbo.ComandaTemporal_Expres WHERE dbo.ComandaTemporal_Expres.Tipo_Plato= 'Modificador' and ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Modifi)

            'While rs_Modifi.Read
            '    cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Modifi("id"))
            'End While
            'rs_Modifi.Close()
        Catch ex As Exception
            MsgBox("Error al actualizar el inventario: " & ex.Message, MsgBoxStyle.OkOnly)
            cFunciones.spEnviar_Correo(Me.Name, "RecargaInventario", ex.Message)
        End Try
    End Sub

#End Region

#Region "CalculaCostoArticuloanulado"
    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticuloanulado(ByVal id_receta As Integer, ByVal codigo_articulo As Integer, ByVal Cantidad As Double, ByVal Disminuye As Double, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal acom As Boolean) As Double
        Try

            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & id_receta)
            'Esta es la verdadera cantidad q disminuye una porcion de este articulo en una receta
            Disminuye = exe.SlqExecuteScalar(cnx, "Select disminuye From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
            If costoXarticulo = 0 Then
                costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
            End If

            Dim Conexion As New SqlClient.SqlConnection
            Dim existencia, existencia_inv, existencia_inv_act, existencia_bod, existencia_bod_act As Double 'EXISTENCIAS DE INVENTARIO
            Dim saldo_final, costo_promedio, Costo_Movimiento, costo_real As Double
            Conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
            If Conexion.State = ConnectionState.Closed Then Conexion.Open()

            'EXISTENCIA EN BODEGA
            existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & codigo_articulo)
            existencia_inv = existencia
            existencia = existencia + (Cantidad * Disminuye)
            existencia_inv_act = existencia

            'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
            If acom Then
                cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & codigo_articulo)
            End If


            'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
            'ESTO RELACIONADO CON EL COSTO PROMEDIO Y SU DESCARGA DEL INVENTARIO DE LA BODEGA A LA QUE ESTA ASOCIADO ESTE ARTICULO DE MENU
            existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & idbodega_descarga)
            saldo_final = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
            costo_promedio = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
            'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
            existencia_bod = existencia
            existencia = existencia + (Cantidad * Disminuye)
            existencia = Math.Round(existencia, 2)
            Costo_Movimiento = (Cantidad * Disminuye) * costo_promedio
            Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
            saldo_final = saldo_final - Costo_Movimiento
            existencia_bod_act = existencia
            If acom Then
                'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
                cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & codigo_articulo & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & idbodega_descarga)
                'INGRESA AL KARDEX
                actualiza_kardex(Conexion, codigo_articulo, Cantidad * Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, idbodega_descarga, costo_promedio, saldo_final)
            End If

            costoXarticulo = costoXarticulo * Cantidad

            Return costoXarticulo
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CalculaCostoArticuloanulado", ex.Message)
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function
#End Region

#Region "CalculacostoRecetaanulado"
    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    Private Function CalculaCostoRecetaanulado(ByVal codigo_receta As Integer, ByVal disminuye As Double, ByVal cantidad As Double, ByVal idBodegaDescarga As Double, ByVal acom As Boolean) As Double
        Try
            Dim cnx As New SqlClient.SqlConnection
            Dim exe As New ConexionR
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            Dim fila_receta As DataRow
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Articulo,idBodegaDescarga, Disminuye FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoRecetaanulado(fila_receta("Codigo"), fila_receta("Disminuye"), cantidad, idBodegaDescarga, acom)
                Else
                    calculando_costo += CalculaCostoArticuloanulado(codigo_receta, fila_receta("Codigo"), cantidad, disminuye, fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), acom)
                End If
            Next
            Return calculando_costo
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CalculaCostoRecetaanulado", ex.Message)
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function
#End Region

#End Region



    'Public Sub ActualizaMesa()

    '    Dim ActivoMesa As Integer
    '    Dim Conectadobdtem As New SqlClient.SqlConnection
    '    Dim cConexion2 As New Conexion
    '    Dim mesa As New cls_Mesas()

    '    Try

    '        'ACTUALIZA EL ESTADO DE LA MESA, EN CASO DE QUE QUEDE VACIA - ORA
    '        cConexion.GetRecorset(conectadobd, "Select Distinct(cMesa) from ComandaTemporal WITH (NOLOCK) where Estado='Activo'and cCodigo=" & numeroComanda, rs)

    '        mesa.obtenerMesa(idMesa)

    '        ActivoMesa = mesa.activa

    '        'If Me.txt_NomMesa.Text.Length <> 0 Then mesa.tem = Me.txt_NomMesa.Text
    '        'mesa.actualizar()
    '        'Conectadobdtem = cConexion2.Conectar("Restaurante")
    '        'ActivoMesa = cConexion2.SlqExecuteScalar(Conectadobdtem, "Select activa from mesas where id = " & idMesa)

    '        'cConexion2.DesConectar(Conectadobdtem)


    '        'Si la mesa está desactivada no tiene que realizar nada DGN 180809
    '        If ActivoMesa = 0 Then Exit Sub
    '        If rs.IsClosed = True Then
    '            Exit Sub
    '        End If
    '        If rs.Read = False Then
    '            rs.Close()
    '            If ActivoMesa = 1 Or ActivoMesa = 4 Then
    '                mesa.activa = "0"

    '                'cConexion.UpdateRecords(conectadobd, "Mesas", "activa=0", "Id=" & idMesa)
    '            End If
    '        End If

    '        mesa.actualizar()

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        If rs.IsClosed = False Then rs.Close()
    '    End Try
    'End Sub

    Private Sub Bloquear(ByVal estado As Boolean)
        Try
            ToolStrip1.Enabled = estado
            Me.ControlBox = estado

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Bloquear", ex.Message)
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub ToolStripButtonDescuento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonDescuento.Click

        Try
            If MessageBox.Show("Desea Escoger el Cliente..", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim credito As New ClienteCredito
                credito.desdedescuento = True
                credito.ShowDialog()
                descuento = credito.montodescuento
                ' cod_cliente = credito.id
                guardarNombre(credito.nombre)
                ToolStripButtonDescuento.BackColor = Color.Yellow
                ToolStripButtonDescuento.Text = "Desc(" & descuento & ")"
                AplicarDescuentoComandas()
            Else

                Dim descuennto As Double
                Dim rDesc As New registro
                rDesc.Text = "Digite el descuento a aplicar"
                If descuento > 0 Then
                    rDesc.txtCodigo.Text = descuento
                End If
                rDesc.ShowDialog()
                Try
                    descuennto = CDbl(rDesc.txtCodigo.Text)
                Catch ex As Exception
                    descuennto = 0
                End Try
                rDesc.Dispose()

                descuento = descuennto

                If descuento = 0 Or descuento > 100 Then
                    ToolStripButtonDescuento.BackColor = Color.Transparent
                    ToolStripButtonDescuento.Text = "Descuento"
                    descuento = 0
                Else
                    ToolStripButtonDescuento.BackColor = Color.Yellow
                    ToolStripButtonDescuento.Text = "Desc(" & descuento & ")"

                End If
                AplicarDescuentoComandas()
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButtonDescuento_Click", ex.Message)
        End Try
        ComandasShow()

    End Sub
    Sub AplicarDescuentoComandas()
        Try
            Dim secuencia As String = "Update ComandaTemporal " &
                                               " SET descuento = " & descuento & "" &
                                               " WHERE Estado='Activo'and cMesa=" & idMesa & " and separada= " & fnObtenerCuenta(cmbCuentas.Text)
            Dim cx As New Conexion
            cx.SlqExecute(cx.Conectar(), secuencia)
            cx.DesConectar(cx.sQlconexion)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "AplicarDescuentoComandas", ex.Message)
        End Try


    End Sub

    Private Sub ToolStripButtonNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonNombre.Click

        ToolStripButtonNombre.BackColor = Color.Yellow
        Me.GroupBoxNombre.Visible = True
        Me.TextBoxNombreCliente.Text = Me.NombreCliente
        TextBoxNombreCliente.Focus()

    End Sub

    Private Sub ButtonGuardarNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGuardarNombre.Click
        guardarNombre(TextBoxNombreCliente.Text)
    End Sub
    Sub guardarNombre(ByVal Nombre As String)
        Try
            Me.NombreCliente = Nombre
            If Not Me.NombreCliente.Equals("") Then

                Dim secuencia As String = "Update ComandaTemporal " &
                                                            " SET Nombre = '" & Me.NombreCliente & "'" &
                                                            " WHERE Estado='Activo' and cMesa=" & idMesa & " and ( separada = " & fnObtenerCuenta(cmbCuentas.Text) & " or separada = 0 )"
                Dim cx As New Conexion
                cx.SlqExecute(cx.Conectar(), secuencia)
                cx.DesConectar(cx.sQlconexion)
                ToolStripButtonNombre.BackColor = Color.Yellow

            Else
                ToolStripButtonNombre.BackColor = Color.Transparent

            End If

            Me.GroupBoxNombre.Visible = False
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "guardarNombre", ex.Message)
        End Try

    End Sub

    Private Sub ButtonSalirNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalirNombre.Click
        GroupBoxNombre.Visible = False
        If Not NombreCliente.Equals("") Then
            ToolStripButtonNombre.BackColor = Color.Yellow

        Else
            ToolStripButtonNombre.BackColor = Color.Transparent
        End If
    End Sub
    Sub cargarDescuento()
        Try
            If numeroComanda = "" Then
                numeroComanda = Nothing
            End If
            If Not numeroComanda > 0 Then
                Exit Sub
            End If
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(descuento),0) AS DES, Nombre AS NOM, cCodigo, separada " &
                                                " FROM ComandasActivas " &
                                                " GROUP BY Estado,cCodigo, separada, Nombre ,cMesa" &
                                                " HAVING (Estado='Activo') AND (cMesa = " & idMesa & ") AND (separada = 0 OR separada = " & fnObtenerCuenta(Me.cmbCuentas.Text) & ")", dt)
            If dt.Rows.Count > 0 Then

                Me.NombreCliente = dt.Rows(0).Item("NOM")
                Me.descuento = dt.Rows(0).Item("DES")
                If Me.descuento > 0 Then
                    Me.ToolStripButtonDescuento.BackColor = Color.Yellow
                    Me.ToolStripButtonDescuento.Text = "Desc(" & Me.descuento & ")"
                Else
                    Me.ToolStripButtonDescuento.BackColor = Color.Transparent
                    Me.ToolStripButtonDescuento.Text = "Descuento"

                End If

                If Not Me.NombreCliente.Equals("") Then
                    Me.ToolStripButtonNombre.BackColor = Color.Yellow

                Else
                    Me.ToolStripButtonNombre.BackColor = Color.Transparent

                End If
            Else
                ToolStripButtonDescuento.BackColor = Color.Transparent
                ToolStripButtonNombre.BackColor = Color.Transparent
                Me.ToolStripButtonDescuento.Text = "Descuento"

            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "cargarDescuento", ex.Message)
        End Try
    End Sub

    Private Sub FacturarToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturarToolStripMenuItem1.Click
        facturame(True)
    End Sub


    '---------------------------------------------------------------------------------------------
    '---------------------------------------------------------------------------------------------

    Dim banderaAplicaIV As Boolean = True
    Dim banderaAplicaIS As Boolean = True
    '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Esta funcion guarda lo actualmente comandado en una tabla de totales para luego ser facturado
    ' individualmente, hacer un total.. o bien eliminarlo..
    Public Sub guardarComandaEnAcumulado_2(ByRef e As Boolean, ByRef s As String, ByRef iv As String, ByRef iss As String, ByRef d As String, ByRef t As String, ByRef m As String, ByRef _cuenta As String, Optional SoyExpress As Integer = 0, Optional NumComanda As Integer = 0, Optional _Transporte As Double = 0, Optional Telefono As String = "")

        Try
            Dim fx As New cls_Broker()
            Dim myAcum As New cls_facturasAcumuladas()
            Dim myAcumDetalle As New cls_facturasAcumuladasDetalle()
            Dim _IdMesa As Integer = 0

            If SoyExpress = 1 Then
                _SoyExpress = SoyExpress
                conectadobd = cConexion.Conectar("Restaurante")
                idMesa = m
                numeroComanda = NumComanda
            Else
                NumComanda = numeroComanda
            End If

            If _ParaLlevar = 1 Then
                _IdMesa = 0
            Else
                _IdMesa = idMesa
            End If

            myAcumDetalle.NombreUsuario = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre from Usuarios WITH (NOLOCK) where Cedula='" & usuario & "'")
            Me.txtSubTotal.Text = s
            Me.txtDesc.Text = d
            Me.txtTotal.Text = t
            Me.txtIVentas.Text = iv
            Me.txtIServicio.Text = iss
            Me.cmbCuentas.Items.Add(_cuenta)
            Me.cmbCuentas.Text = _cuenta

            If iv = 0 Then Me.banderaAplicaIV = False
            If iss = 0 Then Me.banderaAplicaIS = False

            'Preguntamos si desea imprimir la prefactura...
            If e = True Then
                If SoyAcumulada = 1 Then
                    Me.txtSubTotal.Text = s
                    Me.txtDesc.Text = d
                    Me.txtTotal.Text = t
                    Me.txtIVentas.Text = iv
                    Me.txtIServicio.Text = iss
                    SoyAcumulada = 0
                End If
                Me.prefacturaRapida(busca_impresora())
            End If
            myAcum = New cls_facturasAcumuladas()
            'Guardar en tabla de acumulados..
            myAcum.subtotal = Convert.ToDouble(s)
            myAcum.descuento = Convert.ToDouble(d)
            myAcum.imp_venta = Convert.ToDouble(iv)
            myAcum.monto_saloero = Convert.ToDouble(iss)
            myAcum.total = Convert.ToDouble(t)
            myAcum.transporte = Convert.ToDouble(_Transporte)
            myAcum.proveniencia_venta = User_Log.PuntoVenta
            myAcum.descripcion = User_Log.NombrePunto
            myAcum.subtotalgravada = Convert.ToDouble(s)
            myAcum.mesa = _IdMesa
            myAcum.telefono = Telefono
            'Guardamos el encabezado de la factura..
            myAcum.nuevaFacturaAcumulada()


            myAcumDetalle.Id_Factura = myAcum.Obtener_ID_FacturaAcumulada(myAcum.num_factura)
            'Guardamos el detalle de la nueva factura acumulada..

            If _cuenta = "" Then
                _cuenta = "0"
            End If

            If SoyExpress = 1 Or _ParaLlevar = 1 Then
                Dim dtmoneda As New DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT m.MonedaNombre, m.CodMoneda, m.ValorCompra" &
            " FROM dbo.Moneda m INNER JOIN dbo.Configuraciones r ON m.CodMoneda = r.MonedaRestaurante", dtmoneda)
                cConexion.AddNewRecord(conectadobd, "Comanda", "NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura, Cod_Moneda", NumComanda & ", GetDate()," & CDbl(txtSubTotal.Text) & "," & CDbl(Me.txtIVentas.Text) & "," & CDbl(txtTotal.Text) & ",'" & Me.usuario & "','0 '," & idMesa & ",1,0," & myAcumDetalle.Id_Factura & "," & dtmoneda.Rows(0).Item("CodMoneda"))
                myAcumDetalle.nuevoDetalle(idMesa, _cuenta, NumComanda)
                myAcumDetalle.borrarComandaTemporal(idMesa, _cuenta, 1, NumComanda)
            Else
                myAcumDetalle.nuevoDetalle(idMesa, _cuenta)
                myAcumDetalle.borrarComandaTemporal(idMesa, _cuenta)
            End If



            Me.mesa.obtenerMesa(idMesa)
            Me.mesa.restablecerNombreMesa()

            Me.Close()
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "guardarComandaEnAcumulado_2", ex.Message)
        End Try

    End Sub
    '----------------------------------------------------------------------------------------------------------

    Private Sub spIdComandaTemporal()
        Try
            IdComandaTemporal = cConexion.SlqExecuteScalar(conectadobd, "select max(id)as ID from ComandasActivas where cMesa = '" & idMesa & "'")

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spIdComandaTemporal", ex.Message)
        End Try
    End Sub
    Private Sub ToolStripButton11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton11.Click
        Try
            Dim confirmacion As New frmQuestion
            Dim _PaLlevar As Integer = _ParaLlevar
            With confirmacion
                .LblMensaje.Text = "Desea Generar la Factura Acomulada ?"
                .ShowDialog()
                If .resultado = True Then
                    ''guardarComandaEnAcumulado()
                    Dim f As New frmAcumular
                    Dim c As New cls_facturasAcumuladas
                    Dim nom_mesa = c.nombreMesa(idMesa)
                    f.PaLlevar = _PaLlevar
                    Dim _IServicio As Double = 0
                    Dim _IVenta As Double = 0
                    Try
                        Dim dt As New DataTable
                        Dim sql As New SqlClient.SqlCommand


                        sql.CommandText = "Select top 1 Configuraciones.Imp_Venta, Configuraciones.Imp_Servicio from Configuraciones"
                        cFunciones.spCargarDatos(sql, dt)
                        If dt.Rows.Count > 0 Then
                            _IServicio = Convert.ToDouble(dt.Rows(0).Item("Imp_Servicio"))
                            _IVenta = Convert.ToDouble(dt.Rows(0).Item("Imp_Venta"))
                        Else
                            _IServicio = 10
                            _IVenta = 13
                        End If

                        dt.Clear()
                        sql.CommandText = "Select Express from Mesas where Id=" & idMesa & ""
                        cFunciones.spCargarDatos(sql, dt)
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item("Express").ToString().Equals("True") Then
                                f.MesaExpress = 1
                            End If
                        End If

                    Catch ex As Exception
                        _IServicio = 10
                        _IVenta = 13
                    End Try
                    SoyAcumulada = 1
                    f.ReciveDatos(txtSubTotal.Text, txtIVentas.Text, txtIServicio.Text, txtDesc.Text, txtTotal.Text, nom_mesa, fnObtenerCuenta(Me.cmbCuentas.Text), _IServicio, _IVenta, SoyAcumulada)
                    f.ShowDialog()

                    Me.Close()
                End If
            End With
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton11_Click", ex.Message)
        End Try
    End Sub

    Private Sub ToolStripButtonAtras_Click(sender As Object, e As EventArgs) Handles ToolStripButtonAtras.Click
        Try
            Dim Titulo As String = lblEncabezado.Text
            Select Case Titulo

                Case "Menú" 'SELECCIONA UNA CATEGORIA DEL MENÚ, CARGANDO LOS PLATOS O DATOS DE ESA CATEGORIA
                    limpiaDatos()
                    ObtenerDimension()
                    CrearCategoriaMenu()
                    cmbCuentas.Enabled = True
                    If OpcionesEspeciales Then
                        ToolButtonTerminar.Text = "Enviar"
                        ToolButtonTerminar.Image = img
                        ToolButtonRegresar.Visible = False
                    Else
                        ToolButtonTerminar.Text = "Terminar"
                    End If
                Case "Modificadores*"
                    limpiaDatos()
                    Dim MenuSelect() As DataRow
                    MenuSelect = glodtsGlobal.Menu_Restaurante.Select("Id_Categoria =" & idGrupoT)
                    For i As Integer = 0 To MenuSelect.GetUpperBound(0)
                        MenuSelect(i).Item("tipo") = "MENUREST"
                    Next
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    UbicarPosiciones(MenuSelect)
                    ' UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT)
                    lblEncabezado.Text = "Menú"
                    ComandasShow()
                Case "Acompañamientos"
                    limpiaDatos()
                    Dim Modificadores() As DataRow
                    Modificadores = glodtsGlobal.ModificadoresPrecio.Select("IdCategoriaMenu = " & Id_Categoria)

                    ' cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dtt)
                    If Modificadores.Count > 0 Then
                        For i As Integer = 0 To Modificadores.GetUpperBound(0)
                            Modificadores(i).Item("Precio_VentaA") = Modificadores(i).Item("Precio_VentaA") * Me.current_Cant
                        Next

                        'UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
                        ' dtt.Rows.Clear()
                        UbicarPosiciones(Modificadores)
                    Else

                        ' UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                        Modificadores = glodtsGlobal.Modificadores_Forzados.Select()
                        UbicarPosiciones(Modificadores)

                    End If
                    lblEncabezado.Text = "Modificadores*"
                    ComandasShow()
            End Select

            PanelMesas.Focus()

        Catch ex As Exception
            MessageBox.Show("Error:" & ex.ToString, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Dim BANDERA_MEDIO As Boolean
    Private Sub ToolStripButton12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton12.Click
        If ToolStripButton12.BackColor = Color.Blue = True Then
            ToolStripButton12.BackColor = Color.Transparent
            Me.BANDERA_MEDIO = False
        Else
            ToolStripButton12.BackColor = Color.Blue
            Me.BANDERA_MEDIO = True
        End If
    End Sub

    Private Sub ToolButtonPlatoFuerte_Click(sender As Object, e As EventArgs) Handles ToolButtonPlatoFuerte.Click
        Try
            If lblEncabezado.Text.Equals("Modificadores*") Or lblEncabezado.Text.Equals("Acompañamientos") Then
                cConexion.SlqExecute(conectadobd, "update ComandaTemporal set primero = 0 where id >= '" & IdComandaTemporal & "' and cMesa = '" & idMesa & "'")
                ComandasShow()
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonPlatoFuerte_Click", ex.Message)
        End Try

    End Sub

    Private Sub ToolButtonAlergenico_Click(sender As Object, e As EventArgs) Handles ToolButtonAlergenico.Click
        Try
            Dim FrmAdicionales As New FrmModificadorAdicional
            FrmAdicionales.Comanda = numeroComanda
            FrmAdicionales.TituloModulo.Text = "Nota Alergénica"
            FrmAdicionales.Label2.Visible = False
            FrmAdicionales.TextBox1.Visible = False
            FrmAdicionales.ShowDialog(Me)

            If FrmAdicionales.Aceptado = True Then

                cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal " & 0 & ",'! " & FrmAdicionales.Nota & "'," & 0 & "," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ", 'Modificador' ,'" & usuario & "','" & HabitacionAsignada & "',0," & fnObtenerCuenta(cmbCuentas.Text) & "," & fnMesaExpres() & ",-1")

                Me.ComandasShow()
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ToolButtonAlergenico_Click", ex.Message)
        End Try
    End Sub
    Private Function fnObtenerCuenta(ByVal _Cuenta As String) As String
        Try
            Dim vector() As String
            vector = _Cuenta.Split("-")
            Return vector(0)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnObtenerCuenta", ex.Message)
        End Try
    End Function


End Class

