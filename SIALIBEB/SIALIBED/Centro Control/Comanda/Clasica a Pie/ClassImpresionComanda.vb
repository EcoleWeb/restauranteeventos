Public Class ClassImpresionComanda
    Public Id_Comanda As Integer = 0
    Public imprimeNuevos As Boolean = True
    Public nombrePuntoVenta As String = "RESTAURANTE"
    Public usuario As String = "Usuario"
    Public Impresora As String = "PUNTOVENTA"
    Public Mesa As String = "MESA"
    Public Habitacion As String = ""
    Private tkt As New ComandaRapida
    Private dt As New DataTable
    Private dt1 As New DataTable

    Sub cargaComanda()
        If Me.imprimeNuevos = False Then
            cFunciones.Llenar_Tabla_Generico("SELECT  cProducto, SUM(cCantidad) AS cCantidad, cDescripcion, cCodigo, Impreso, Impresora, Tipo_Plato, CantImp, Express, primero FROM ComandaTemporal GROUP BY cDescripcion, cCodigo, Impreso, Impresora, Tipo_Plato, CantImp, Express, primero, cProducto " & _
            " HAVING (cCodigo = " & Id_Comanda & ") AND (Impresora = '" & Me.Impresora & "')", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            '*******************************************
            cFunciones.Llenar_Tabla_Generico("SELECT  0 As Agregado, Id, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, Impreso, Tipo_Plato, Impresora, CantImp, primero, Express " & _
            " FROM         ComandaTemporal WHERE (cCodigo = " & Id_Comanda & ") AND (Impresora = '" & Me.Impresora & "')", dt1, GetSetting("SeeSoft", "Restaurante", "Conexion"))


        Else
            cFunciones.Llenar_Tabla_Generico("SELECT  cProducto, SUM(cCantidad) AS cCantidad, cDescripcion, cCodigo, Impreso, Impresora, Tipo_Plato, CantImp, Express FROM ComandaTemporal GROUP BY cDescripcion, cCodigo, Impreso, Impresora, Tipo_Plato, CantImp, Express, primero,cProducto " & _
            " HAVING (cCodigo = " & Id_Comanda & ") AND (Impresora = '" & Me.Impresora & "') AND (Impreso = 0)", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            '*******************************************
            cFunciones.Llenar_Tabla_Generico("SELECT  0 As Agregado, Id, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, Impreso, Tipo_Plato, Impresora, CantImp, primero, Express " & _
                   " FROM         ComandaTemporal WHERE (cCodigo = " & Id_Comanda & ") AND (Impresora = '" & Me.Impresora & "') AND (Impreso = 0)", dt1, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        End If

        If dt.Rows.Count > 0 Then
            generaImpresionComanda()
        End If
    End Sub
    Public Sub cort()
        Dim intFileNo As Integer = FreeFile()
        FileOpen(1, "c:\corte.txt", OpenMode.Output)
        PrintLine(1, Chr(29) & Chr(86) & Chr(0))

        FileClose(1)
        Shell("print FACTURACION c:\corte.txt", AppWinStyle.NormalFocus)
    End Sub
    
    Sub generaImpresionComanda()
        Dim tkt As ComandaRapida = New ComandaRapida
        tkt.TamanoDeLaFuente = "12"
        tkt.MargenIzquierdo = 1
        tkt.MargenSuperior = 0.2

        Dim ImpresoraBar As String = ""

        If Me.Impresora.IndexOf("BAR") > 0 Then
            ImpresoraBar = "BAR"
        End If

        'Si la impresora es BEBIDAS y el registro "noComanda" es igual a 2, entonces que no imprima la comanda..
        If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Sub

        'a.HeaderImage = "C:\Documents and Settings\Administrador\Mis documentos\COMPU.jpg" 
        Dim dtVouch As New DataTable
        tkt.AnadirLineaCabeza(nombrePuntoVenta)
        If Me.imprimeNuevos = False Then tkt.AnadirLineaSubcabeza("**REIMPRESION**")
        tkt.AnadirLineaCabeza("Comanda No " & Id_Comanda)
        tkt.AnadirLineaCabeza("" & Format(DateTime.Now, "dd/MM/yy") + " " + Format(DateTime.Now, "hh:mm:ss tt"))
        tkt.AnadirLineaCabeza("Salonero: " & usuario)
        tkt.AnadirLineaCabeza("Mesa: " & Mesa)
        If Not Habitacion.Equals("") Then tkt.AnadirLineaSubcabeza("Habitacion: " & Me.Habitacion)
        tkt.EncabezadoElementos = "CANT  DESCRIPCION"
        'AGREGAR LOS DE PREPARAR PARA EXPRESS
        Dim hay As Boolean = True
        For i As Integer = 0 To Me.dt1.Rows.Count - 1
            If dt1.Rows(i).Item("Express") Then
                If hay Then
                    tkt.AnadirElemento("-", ">--PARA LLEVAR--<", "") : hay = False
                End If
                If dt1.Rows(i).Item("cCantidad") > 0 Then
                    procesaLinea(tkt, dt1.Rows(i))
                End If
            End If
        Next
        'AGREGAR LOS DE PREPARAR PRIMERO
        hay = True
        For i As Integer = 0 To Me.dt1.Rows.Count - 1
            If dt1.Rows(i).Item("primero") = 1 And Not dt1.Rows(i).Item("Express") Then
                If hay Then
                    tkt.AnadirElemento("-", ">-PREPARAR PRIMERO-<", "") : hay = False
                End If
                If dt1.Rows(i).Item("cCantidad") > 0 Then
                    procesaLinea(tkt, dt1.Rows(i))
                End If
            End If
        Next
        'AGREGAR AHORA LOS DEMAS
        hay = True

        For i As Integer = 0 To Me.dt1.Rows.Count - 1
            If dt1.Rows(i).Item("primero") = 0 And Not dt1.Rows(i).Item("Express") Then
                If hay Then
                    tkt.AnadirElemento("-", ">---FUERTES---<", "") : hay = False
                End If
                If dt1.Rows(i).Item("cCantidad") > 0 Then
                    procesaLinea(tkt, dt1.Rows(i))
                End If
            End If
        Next

        If Me.imprimeNuevos = False Then tkt.AnadeLineaAlPie("**REIMPRESION**")

        tkt.AnadeLineaAlPie("    ")
        tkt.AnadeLineaAlPie("    ")
        tkt.AnadeLineaAlPie("    ")
        tkt.AnadeLineaAlPie("    ")
        tkt.AnadeLineaAlPie("    ")
        tkt.AnadeLineaAlPie(Chr(29) & Chr(86) & Chr(0))
        tkt.ImprimeTicket(Me.Impresora)




    End Sub

#Region "   PROCESA LINEA"
    Sub procesaLinea(ByRef tkt As ComandaRapida, ByRef rw As DataRow)
        If rw("Agregado") = 0 Then
            Dim cuantos As Integer = Me.cuantosMasDeEste(rw("cProducto"))
            If cuantos > 1 Then
                If tieneSubs(rw("Id")) Then
                    tkt.AnadirElemento(rw("cCantidad"), rw("cDescripcion"), "")
                    agregaSubs(tkt, rw("Id"))
                Else
                    tkt.AnadirElemento(cuantos, rw("cDescripcion"), "")
                    marqueLosMismos(rw("cProducto"))
                End If
            Else
                If tieneSubs(rw("Id")) Then
                    tkt.AnadirElemento(rw("cCantidad"), rw("cDescripcion"), "")
                    agregaSubs(tkt, rw("Id"))

                Else
                    tkt.AnadirElemento(rw("cCantidad"), rw("cDescripcion"), "")
                End If

            End If
            rw("Agregado") = 1


        End If

    End Sub

    Sub agregaSubs(ByRef tkt As ComandaRapida, ByVal linea As Integer)
        Dim busco As Boolean = False
        For i As Integer = 0 To Me.dt1.Rows.Count - 1

            If busco Then
                If dt1.Rows(i).Item("Tipo_Plato").ToString.Trim(" ").Equals("Acompaņamiento") Or dt1.Rows(i).Item("Tipo_Plato").ToString.Trim(" ").Equals("Modificador") Then
                    tkt.AnadirElemento("   ", "   " & dt1.Rows(i).Item("cDescripcion"), "", "Lucida Console", "10")
                Else
                    Exit Sub
                End If
            End If
            If dt1.Rows(i).Item("Id") = linea Then
                busco = True
            End If


        Next
    End Sub

    Function cuantosMasDeEste(ByVal cPro As Integer) As Integer
        For i As Integer = 0 To Me.dt.Rows.Count - 1
            If dt.Rows(i).Item("cProducto") = cPro Then
                Return dt.Rows(i).Item("cCantidad")
            End If

        Next
    End Function

    Sub marqueLosMismos(ByVal cPro As Integer)
        For i As Integer = 0 To Me.dt1.Rows.Count - 1
            If dt1.Rows(i).Item("cProducto") = cPro Then
                dt1.Rows(i).Item("Agregado") = 1
            End If
        Next
    End Sub

    Function tieneSubs(ByVal linea As Integer) As Boolean
        Dim busco As Boolean = False
        For i As Integer = 0 To Me.dt1.Rows.Count - 1
            If busco Then
                If dt1.Rows(i).Item("Tipo_Plato").ToString.Trim(" ").Equals("Acompaņamiento") Or dt1.Rows(i).Item("Tipo_Plato").ToString.Trim(" ").Equals("Modificador") Then
                    Return True
                Else
                    Return False
                End If
            End If
            If dt1.Rows(i).Item("Id") = linea Then
                busco = True
            End If
        Next
        Return False
    End Function
#End Region

End Class
