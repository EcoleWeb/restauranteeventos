Option Explicit On
Imports System.IO

Public Class Comanda

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim idGrupo As String = "1"
    Dim botones As Button
    Dim idseccion As String
    Dim cambio As Boolean = False
    Dim idnuevo As String = ""
    Public Cedula As String
    Dim cedula1 As String
    Dim TiempoInicial As Date
    Dim TiempoEspera As Double
    Public cuentas As Integer = 0
    Dim usaGrupo As Boolean = False
    Dim dime As Boolean = False
    Dim UnirMesa As Boolean = False
    Dim cArrayUnirMesa As New ArrayList
    Dim UnionSeparada As Boolean = False
    Dim rutaMesaUnida As String = ""
    Dim SepararMesa As Boolean = False
    Public EsParaLlevar As Integer = 0
#End Region

    Private Sub Comanda_KeyDown(ByVal sennder As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.P Then
            cort()
        End If
    End Sub
    Public Sub cort()
        Try
            Dim intFileNo As Integer = FreeFile()
            FileOpen(1, "c:\corte.txt", OpenMode.Output)
            PrintLine(1, Chr(29) & Chr(86) & Chr(0))
            FileClose(1)
            Shell("print LPT1 c:\corte.txt", AppWinStyle.NormalFocus)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "cort", ex.Message)
        End Try
    End Sub
    Private Sub rComanda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            'Me.BackColor = Color.White
            'Me.PanelMesas.BackColor = Color.White
            'Me.PanelGrupos.BackColor = Color.White
            'Me.PanelSecciones.BackColor = Color.White


            If GetSetting("SeeSOFT", "Restaurante", "TiempoEspera") = "" Then SaveSetting("SeeSOFT", "Restaurante", "TiempoEspera", 5)
            If GetSetting("SeeSOFT", "Restaurante", "RutaMesaUnida") = "" Then SaveSetting("SeeSOFT", "Restaurante", "RutaMesaUnida", "") Else rutaMesaUnida = GetSetting("SeeSOFT", "Restaurante", "RutaMesaUnida")
            If GetSetting("SeeSoft", "Restaurante", "OpcionesEspeciales").Equals("1") Then
                btParallevar.Text = "To Go"
                btParallevar.ToolTipText = "To Go"
            Else
                SaveSetting("SeeSOFT", "Restaurante", "OpcionesEspeciales", "0")
            End If
            TiempoEspera = GetSetting("SeeSOFT", "Restaurante", "TiempoEspera")
            Label8.Text = "Prefacturado"
            Cedula = User_Log.Cedula
            cedula1 = User_Log.Cedula
			If GetSetting("SeeSoft", "Restaurante", "VigilaImpresora").Equals("") Then
				Me.botonVerComandas.Visible = False
			End If
            If GetSetting("SeeSoft", "Restaurante", "ParaLlevar").Equals("1") Then
                Me.btParallevar.Visible = False
            Else
                SaveSetting("SeeSoft", "Restaurante", "ParaLlevar", "0")
            End If

			CargarGrupoMesas()
            crearMesas(False)
            CargarSecciones()
            PanelMesas.Focus()

            Dim RowPermiso = (From i As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where i.Id_Usuario = User_Log.Cedula And i.Modulo_Nombre_Interno = Name)
            For Each r As dtsGlobal.PermisosSeguridadRow In RowPermiso
                PanelMesas.Enabled = r.Accion_Ejecucion
                Exit For
            Next
            '  PanelMesas.Enabled = VSMA(User_Log.Cedula, Name, Seguridad.Secure.Execute)

            If Me.PanelMesas.Enabled Then
                TextBox1.Text = ""
                Me.PanelMesas.Enabled = True
            Else
                Me.Enabled = True
            End If

            usaGrupo = glodtsGlobal.conf(0).UtilizaGrupo
            ' usaGrupo = cConexion.SlqExecuteScalar(cConexion.Conectar, "Select utilizaGrupo from conf")
            Text = "SEESOFT A&B PUNTO VENTA : " & User_Log.NombrePunto & " - " & "USUARIO: " & User_Log.Nombre & " v. " & Globales.GetVersionPublicacion
            TiempoInicial = Now
            Timer1.Enabled = True
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Load", ex.Message)
        End Try
    End Sub

    Private Sub CargarGrupoMesas()
        Try
            Dim aButton As Button
            Dim bBandera As Boolean = False
            Dim ruta As String
            Dim primero As Boolean = False
            Dim y As Integer
            Dim sql As New SqlClient.SqlCommand
            Dim dt As New DataTable


            sql.CommandText = "Select id,nombre_grupo,ImagenRuta from grupos_mesas WITH (NOLOCK)"
            cFunciones.spCargarDatos(sql, dt)
            For i As Integer = 0 To dt.Rows.Count - 1
                If primero = False Then
                    idGrupo = dt.Rows(i).Item("id")
                    primero = True
                End If
                aButton = New Button

                aButton.FlatStyle = FlatStyle.Flat
                aButton.FlatAppearance.BorderSize = 0


                'aButton.BackColor = Color.White

                aButton.Top = y
                aButton.Width = 184
                aButton.Height = 56
                aButton.Name = dt.Rows(i).Item("id") & "-GRUPO"
                aButton.Text = dt.Rows(i).Item("Nombre_grupo")
                If bBandera = False Then
                    Label3.Text = dt.Rows(i).Item("Nombre_grupo")
                    bBandera = True
                End If

                If Not IsDBNull(dt.Rows(i).Item("ImagenRuta")) Then
                    ruta = dt.Rows(i).Item("ImagenRuta")
                    If ruta.Length > 0 Then
                        Try
                            Dim SourceImage As Bitmap
                            SourceImage = New Bitmap(ruta)
                            'SourceImage = New Drawing.Bitmap(New Bitmap(ruta), aButton.Width, aButton.Height)
                            'aButton.Image = SourceImage
                            aButton.BackgroundImage = SourceImage
                            aButton.BackgroundImageLayout = ImageLayout.Stretch

                        Catch ex As Exception
                        End Try
                    End If
                End If

                aButton.TextImageRelation = TextImageRelation.ImageBeforeText
                AddHandler aButton.Click, AddressOf Clickbuttons
                Me.PanelGrupos.Controls.Add(aButton)
                y += 58
            Next

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CargarGrupoMesas", ex.Message)
        End Try

    End Sub

    Private Sub crearMesas(ByVal valida As Boolean)
		Try

			Dim ancho As Double
			Dim largo As Double
			ancho = Double.Parse(GetSetting("SeeSoft", "Restaurante", "anchoMesas"))
			largo = Double.Parse(GetSetting("SeeSoft", "Restaurante", "largoMesas"))
			Dim aButton As Button
			Dim i, ii, X, Y, n As Integer
			If Me.LabelEstado.Text = "0" Then
				Me.LabelEstado.Text = "1"

				PanelMesas.Visible = False
				PanelMesas.Controls.Clear()
				For i = 1 To 8
					For ii = 1 To 8
						aButton = New Button
						aButton.Top = Y
						aButton.Left = X
						aButton.Width = ancho
						aButton.Height = largo
						aButton.Name = n
						aButton.Visible = False
						AddHandler aButton.Click, AddressOf Clickbuttons
						Me.PanelMesas.Controls.Add(aButton)
						X = X + (largo + 2)
						n = n + 1
					Next
					Y = Y + (ancho + 2)
					X = 0
				Next
				TiempoInicial = Now
				UbicarPosiciones(valida)
				PanelMesas.Visible = True
				Me.LabelEstado.Text = "0"
			End If
		Catch ex As Exception
			cFunciones.spEnviar_Correo(Me.Name, "crearMesas", ex.Message)
        End Try
       
    End Sub

    Private Sub CargarSecciones() ' Por el momento no se va a utilizar
        Try
            Dim aCheck As RadioButton 'CheckBox
            Dim y As Integer
            Dim sql As New SqlClient.SqlCommand
            Dim dt As New DataTable

            sql.CommandText = "Select id,nombre,descripcion from Secciones_Restaurante WITH (NOLOCK)"
            cFunciones.spCargarDatos(sql, dt)
            For i As Integer = 0 To dt.Rows.Count - 1
                aCheck = New RadioButton 'CheckBox
                aCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 12, CType((System.Drawing.FontStyle.Bold), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                aCheck.Top = y
                aCheck.Width = 184
                aCheck.Height = 26
                aCheck.Name = dt.Rows(i).Item("id") & "-SECCION"
                aCheck.Text = dt.Rows(i).Item("nombre")
                'aCheck.ToolTip = rs("descripcion")
                'aCheck.Properties.CheckStyle = aCheck.Properties.CheckStyle.Style16
                AddHandler aCheck.Click, AddressOf Clickbuttons
                Me.PanelSecciones.Controls.Add(aCheck)
                y += 28
            Next

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "CargarSecciones", ex.Message)
        End Try
    End Sub
    Dim dt_Respaldo As New DataTable
    Private Sub UbicarPosiciones(ByVal valida As Boolean)
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable
        Dim dtMesaUnidas As New DataTable

        Try
            sql.CommandText = "Select m.id,m.nombre_mesa,m.posicion,m.imagenRuta,m.activa, m.id_Seccion,  cast(m.HoraComanda as varchar(1024)) AS Tiempo, m.separada, " & _
                              " ( select count(CantidadComanda) from vs_R_SolicitudPrefactura  WHERE  (IdMesa = m.Id) ) as Prefactura  " & _
                              " ,(select COUNT(*) from Tb_R_MesaUnidas where Tb_R_MesaUnidas.idMesaPrincipal = m.id) as MesaPrincipal " & _
                              ",(select COUNT(*) from Tb_R_MesaUnidas where Tb_R_MesaUnidas.idMesa = m.id) as MesaUnida from mesas as m WITH (NOLOCK) where m.id_Grupomesa= " & idGrupo & " ORDER BY m.Posicion"
            cFunciones.spCargarDatos(sql, dt, "", False)

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    SeccionesMesas(dt.Rows(i), valida, False, dtMesaUnidas)
                Next

                dt_Respaldo = dt.Copy()
            Else
                If dt_Respaldo.Rows.Count > 0 Then
                    For i As Integer = 0 To dt_Respaldo.Rows.Count - 1
                        SeccionesMesas(dt_Respaldo.Rows(i), valida, True, dtMesaUnidas)
                    Next
                End If
            End If

        Catch ex As Exception
            If dt_Respaldo.Rows.Count > 0 Then
                For i As Integer = 0 To dt_Respaldo.Rows.Count - 1
                    SeccionesMesas(dt_Respaldo.Rows(i), valida, True, dtMesaUnidas)
                Next
            End If
            cFunciones.spEnviar_Correo(Me.Name, "UbicarPosiciones", ex.Message)
        End Try
   
    End Sub
    Function procesarTiempo(ByVal tiempo As String) As String
        If tiempo.Equals("") Then
            Return ""
        End If

        Return tiempo.Substring(tiempo.Length - 7, 7)

    End Function



    Private Sub SeccionesMesas(ByVal rs As DataRow, ByVal valida As Boolean, ByVal _Error As Boolean, ByVal dtMesaUnidas As DataTable)
        Try
            Dim ruta As String
            Dim numeroboton() As String
            Dim tempControl As New Control
            botones = New Button
            botones = PanelMesas.Controls.Item(CInt(rs("posicion")))
            botones.Text = rs("nombre_mesa")
            numeroboton = botones.Name.Split("-")
            botones.Name = numeroboton(0)

            botones.Name = botones.Name & "-" & rs("id") & "-" & rs("activa") 'Codigo de la mesa
            botones.Visible = True
            'botones.FlatStyle = FlatStyle.Standard
            'botones.BackColor = Color.Beige
            botones.BackColor = Color.White
            botones.FlatStyle = FlatStyle.Flat
            botones.FlatAppearance.BorderSize = 0

            If _Error Then PanelMesas.Enabled = False Else PanelMesas.Enabled = True



            If rs("activa") = 1 Then 'si es uno esta ocupada

                botones.BackColor = Color.PaleGreen


                If rs("Prefactura") > 0 Then
                    botones.BackColor = Color.RoyalBlue
                End If

                botones.Text = rs("nombre_mesa") & "* " & procesarTiempo(rs("Tiempo")) ' Nombre de la mesa

            ElseIf rs("activa") = 2 Then ' si es 2 esta facturada en azul
                botones.BackColor = Color.Red

            ElseIf rs("activa") = 3 Then ' si es 3 esta facturada separada en celeste
                botones.BackColor = Color.LightSteelBlue

            ElseIf rs("activa") = 4 Then ' si es 3 esta facturada separada en celeste
                botones.BackColor = Color.Orange

            End If
            If Trim(rs("id_Seccion")) = Trim(idseccion) Then
                If rs("activa") = 0 Then
                    botones.BackColor = Color.LightGray
                ElseIf rs("activa") = 2 Then
                    botones.BackColor = Color.RoyalBlue
                ElseIf rs("activa") = 3 Then
                    botones.BackColor = Color.LightSteelBlue
                End If
            End If
            If UnirMesa Then

                For i As Integer = 0 To cArrayUnirMesa.Count - 1
                    If cArrayUnirMesa.Item(i) = rs("id") Then
                        botones.BackColor = Color.Yellow
                        If Int(rs("activa")) > 0 Then
                            UnionSeparada = True
                        End If
                    End If
                Next

            End If

            ruta = rs("imagenRuta")
            If ruta.Trim.Length > 0 Then
                Try
                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    'SourceImage = New Drawing.Bitmap(New Bitmap(ruta), botones.Width, botones.Height)
                    botones.Image = SourceImage

                Catch ex As Exception

                End Try
            End If
            If Not _Error Then


                ' If dtMesaUnidas.Rows.Count > 0 Then

                ' If dtMesaUnidas.Select("idMesaPrincipal = " & rs("id")).Count > 0 Then
                If rs("MesaPrincipal") > 0 Then
                    Try
                        Dim SourceImage As Bitmap
                        SourceImage = New Bitmap(rutaMesaUnida)
                        botones.Image = SourceImage

                    Catch ex As Exception

                    End Try
                End If
                'If dtMesaUnidas.Select("idMesa = " & rs("id")).Count > 0 Then
                If rs("MesaUnida") > 0 Then
                    botones.Visible = False
                End If
                ' End If
            End If


            ToolTip1.SetToolTip(botones, "Mesa " & botones.Text)
            botones.TextImageRelation = TextImageRelation.ImageAboveText

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "SeccionesMesas", ex.Message)
        End Try
    End Sub


    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs) 'todos los eventos de botones
        Try
            Dim arrayB As Array
            Dim idMesa As String = ""
            Dim comenzales As String = ""
            Dim numeroComanda As String = ""
            Dim idGrupoConta As String = ""
            'Dim tipo As Integer
            Dim bandera As Boolean = False
            Dim dtComandaActiva As New DataTable
            Dim sql As New SqlClient.SqlCommand

            Dim dtMesaParaLlevar As New DataTable
            'Instancia de la clase cls_Mesas
            Dim mesas As New cls_Mesas()


            idMesa = ""
            arrayB = sender.name.ToString.Split("-")
            idMesa = arrayB(1)
            If IsNumeric(idMesa) Then


                cFunciones.Llenar_Tabla_Generico("Select ParaLlevar from Grupos_Mesas left outer join Mesas on Grupos_Mesas.id=Mesas.Id_GrupoMesa  where Mesas.Id=" & idMesa & "", dtMesaParaLlevar)
                If dtMesaParaLlevar.Rows.Count > 0 Then
                    If dtMesaParaLlevar.Rows(0).Item("ParaLlevar").ToString().Equals("True") Then
                        EsParaLlevar = 1
                    Else
                        EsParaLlevar = 0
                    End If
                End If
            End If
            Dim bloqueoMesaOcupada As String
            bloqueoMesaOcupada = GetSetting("SeeSoft", "Restaurante", "BloqueoMesaOcupada")
            If bloqueoMesaOcupada = "" Then
                SaveSetting("SeeSoft", "Restaurante", "BloqueoMesaOcupada", "0")
                bloqueoMesaOcupada = "0"
            End If

            If UnirMesa Then


                If Not arrayB(1) = "GRUPO" And Not arrayB(1) = "SECCION" Then
                    For i As Integer = 0 To cArrayUnirMesa.Count - 1
                        If cArrayUnirMesa.Item(i) = idMesa Then
                            Exit Sub
                        End If
                    Next
                    cArrayUnirMesa.Add(idMesa)
                    crearMesas(False)
                End If

                Exit Sub

            End If

            If SepararMesa Then
                If Not arrayB(1) = "GRUPO" And Not arrayB(1) = "SECCION" Then
                    spSepararMesas(idMesa)
                    crearMesas(False)
                End If
                Exit Sub
            End If



            If arrayB(1) = "GRUPO" Then ' si se elige un grupo 
                idGrupo = arrayB(0)
                crearMesas(False)
                Label3.Text = sender.text
            ElseIf arrayB(1) = "SECCION" Then ' si se elige una seccion
                idseccion = arrayB(0)
                crearMesas(True)

            Else ' cualquier evento sobre los botones de las mesas
                '****************************************************************************

                'OBtenemos el estado mas actual de la mesa..
                'spRefrescarMesas()
                mesas.obtenerMesa(idMesa)
                arrayB(2) = mesas.activa                


                If cambio = False Then ' si no se va a cambiar de mesa, solo ingresar a la misma
                    sql.CommandText = "Select * from ComandasActivas WITH (NOLOCK) where cMesa=" & arrayB(1)
                    cFunciones.spCargarDatos(sql, dtComandaActiva)
                    If arrayB(2) = 1 Then
                        If Not dtComandaActiva.Rows.Count > 0 Then
                            'cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & arrayB(1))
                            'MessageBox.Show("Actualizando datos...", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                            'crearMesas(False)
                            cuentas = 0
                            ' Exit Sub
                        End If

                    End If
                    If CBool(arrayB(2)) = False Then 'si la mesa no esta ocupada
ReintentarClave:

                        cuentas = 0
                        '----------------------------------------------------------------------------------------------------------------------------

                        Dim rEmpleado0 As New registro
                        rEmpleado0.Text = "DIGITE CONTRASE�A"
                        rEmpleado0.txtCodigo.PasswordChar = "*"

                        '---------------------------------------------------------------
                        'VERIFICA SI PIDE O NO EL USUARIO

                        If gloNoClave Then
                            clave = User_Log.Clave_Interna
                            rEmpleado0.iOpcion = 1
                        Else
                            rEmpleado0.ShowDialog()
                            clave = rEmpleado0.txtCodigo.Text
                        End If

                        'ojo cambiar esto
                        'validar admin
                        mesas.obtenerMesa(idMesa)
                        arrayB(2) = mesas.activa

                        Dim dtt2 As New DataTable
                        cFunciones.Llenar_Tabla_Generico("SELECT     Perfil_x_Usuario.*, Perfil.Nombre_Perfil" & _
                                                            " FROM         Perfil_x_Usuario INNER JOIN " & _
                                                            " Perfil ON Perfil_x_Usuario.Id_Perfil = Perfil.Id_Perfil" & _
                                                            " WHERE     (Perfil_x_Usuario.Id_Usuario = '" & cedula1 & "')", dtt2, GetSetting("SeeSoft", "Seguridad", "Conexion"))
                        If dtt2.Rows.Count > 0 Then
                            If Not dtt2.Rows(0).Item("Nombre_Perfil").Equals("ADMINISTRATIVO") Or Not dtt2.Rows(0).Item("Nombre_Perfil").Equals("SISTEMAS") Then
                                If mesas.activa = "4" Then Exit Sub
                            End If
                        Else
                            If mesas.activa = "4" Then Exit Sub
                        End If


                        rEmpleado0.Dispose()
                        If rEmpleado0.iOpcion = 0 Then
                            Exit Sub
                        End If

                        If clave <> "" Then
                            Dim RowUsuario = (From i As dtsGlobal.UsuariosRow In glodtsGlobal.Usuarios Where i.Clave_Interna.ToUpper = clave.ToUpper)
                            cedula1 = ""
                            For Each r As dtsGlobal.UsuariosRow In RowUsuario
                                cedula1 = r.Cedula
                            Next
                            ' cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
                            If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                                MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                                GoTo ReintentarClave
                                Exit Sub
                            End If
                        Else
                            GoTo ReintentarClave
                        End If
                        If ToolStripButton1.BackColor = Color.Yellow Then
ReintentarClaves:
                            rEmpleado0 = New registro
                            rEmpleado0.Text = "CANTIDAD DE CUENTAS"
                            rEmpleado0.ShowDialog()
                            If rEmpleado0.iOpcion = 0 Then Exit Sub
                            If Not IsNumeric(rEmpleado0.txtCodigo.Text) Then
                                cuentas = 0
                                MsgBox("Cantidad de Cuentas debe ser mayor a 1..", MsgBoxStyle.Information, "Atenci�n...")
                                rEmpleado0.Dispose()
                                GoTo ReintentarClaves
                            Else
                                If CInt(rEmpleado0.txtCodigo.Text) <= 1 Then
                                    cuentas = 0
                                    MsgBox("Cantidad de Cuentas debe ser mayor a 1..", MsgBoxStyle.Information, "Atenci�n...")
                                    rEmpleado0.Dispose()
                                    GoTo ReintentarClaves
                                Else
                                    cuentas = CInt(rEmpleado0.txtCodigo.Text)
                                    dime = True
                                End If

                            End If
                            rEmpleado0.Dispose()
                        End If
                        idMesa = arrayB(1)

                        If (dime = True) Then
                            cConexion.SlqExecute(cConexion.Conectar(), "Update mesas set separada=" & cuentas & " where id=" & idMesa)
                            dime = False
                        Else
                            cuentas = 0
                            ' cConexion.SlqExecute(cConexion.Conectar(), "Update mesas set separada=" & cuentas & " where id=" & idMesa)
                        End If

                    Else ' si la mesa esta ocupada
                        Dim NumeroApertura As Double

                        '  NumeroApertura = VerificarAperturaCaja(User_Log.Cedula)
TryAgain:
                        'Lo desactive para que pregunte siempre la clave, tambi�n se desativo despu�s del else DGN 080709
                        Dim rEmpleado0 As New registro
                        rEmpleado0.Text = "DIGITE CONTRASE�A"
                        rEmpleado0.txtCodigo.PasswordChar = "*"

                        '---------------------------------------------------------------
                        'VERIFICA SI PIDE O NO EL USUARIO
                        If gloNoClave Then
                            clave = User_Log.Clave_Interna
                            rEmpleado0.iOpcion = 1
                        Else
                            rEmpleado0.ShowDialog()
                            clave = rEmpleado0.txtCodigo.Text
                        End If
                        '---------------------------------------------------------------

                        rEmpleado0.Dispose()
                        If rEmpleado0.iOpcion = 0 Then Exit Sub

                        If clave <> "" Then
                            cedula1 = ""
                            Dim RowUsuario = (From i As dtsGlobal.UsuariosRow In glodtsGlobal.Usuarios Where i.Clave_Interna = clave)
                            For Each r As dtsGlobal.UsuariosRow In RowUsuario
                                cedula1 = r.Cedula
                            Next
                            ' cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK)where Clave_Interna='" & clave & "'")
                            If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                                MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                                GoTo TryAgain
                                Exit Sub
                            Else
                                NumeroApertura = VerificarAperturaCaja(Trim(cedula1))
                                Cedula = cedula1
                            End If
                        Else
                            GoTo TryAgain
                        End If

                        ' numeroComanda = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandaTemporal WITH (NOLOCK) where Estado='Activo' and cMesa=" & arrayB(1))
                        ' comenzales = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select comenzales from ComandaTemporal WITH (NOLOCK) where Estado='Activo' and cMesa =" & arrayB(1))
                        ' Dim dtComanda As New DataTable
                        ' cFunciones.Llenar_Tabla_Generico("Select cCodigo, comenzales from ComandasActivas WITH (NOLOCK) where  cMesa=" & arrayB(1), dtComanda)
                        If dtComandaActiva.Rows.Count > 0 Then
                            numeroComanda = dtComandaActiva.Rows(0).Item("cCodigo")
                            comenzales = dtComandaActiva.Rows(0).Item("comenzales")
                        End If

                        idMesa = arrayB(1)
                        ' cuentas = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from mesas where id =" & arrayB(1))
                        cuentas = mesas.separada
                        If User_Log.SolicitarClaveMesa = True And NumeroApertura = 0 Then
                            Dim QuienAbrioLaMesa As String = ""
                            ' QuienAbrioLaMesa = cConexion.SlqExecuteScalar(cConexion.Conectar(), "SELECT Cedula FROM ComandasActivas WITH (NOLOCK) WHERE (cCodigo = " & numeroComanda & ") GROUP BY Cedula")
                            If dtComandaActiva.Rows.Count > 0 Then
                                QuienAbrioLaMesa = dtComandaActiva.Rows(0).Item("Cedula")
                            End If
                            If QuienAbrioLaMesa <> cedula1 Then
                                MsgBox("Solo el usuario " & QuienAbrioLaMesa & " puede abrir la mesa..")
                                Exit Sub
                            End If
                        End If
                    End If

                    'Antes de comparar el estado de las mesas, obtenemos el estado desde la base de datos..
                    '----------------------------------------------------------------
                    ' mesas.obtenerMesa(idMesa)
                    arrayB(2) = mesas.activa
                    '----------------------------------------------------------------


                    'VERIFICA SI ES ADMISTRADOR
                    '------------------------------------------------------------------------------------
                    If arrayB(2) = 4 Then

                        Dim dtt2 As New DataTable
                        cFunciones.Llenar_Tabla_Generico("SELECT     Perfil_x_Usuario.*, Perfil.Nombre_Perfil" & _
                                                            " FROM         Perfil_x_Usuario INNER JOIN " & _
                                                            " Perfil ON Perfil_x_Usuario.Id_Perfil = Perfil.Id_Perfil" & _
                                                            " WHERE     (Perfil_x_Usuario.Id_Usuario = '" & cedula1 & "')", dtt2, GetSetting("SeeSoft", "Seguridad", "Conexion"))

                        If dtt2.Rows.Count > 0 Then
                          
                            If bloqueoMesaOcupada = "0" Then
                                If dtt2.Rows(0).Item("Nombre_Perfil").Equals("ADMINISTRATIVO") Or dtt2.Rows(0).Item("Nombre_Perfil").Equals("SISTEMAS") Then
                                    If MessageBox.Show("La Mesa Esta Siendo Utilizada En Este Momento, Desea Ingresar de Todas Formas", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                                        arrayB(2) = 1

                                        mesas.obtenerMesa(idMesa)
                                        mesas.activa = "1"
                                        mesas.actualizar()

                                    End If
                                End If
                            End If
                        
                        End If
                    End If

                    '------------------------------------------------------------------------------------

                    'si la mesa esta desocupada o (ocupada y en color beige)
                    If arrayB(2) = 0 Or arrayB(2) = 1 Then                'llamar al menu de comanda
                        Dim iCategoria As Integer = 1
                        While iCategoria = 1
                            Dim actPasado As Integer = arrayB(2)
                            Dim cx As New Conexion
                            cx.Conectar("Restaurante")
                            cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 4 WHERE Id = " & idMesa)
                            cx.DesConectar(cx.sQlconexion)


                            Dim CMenu As New ComandaMenu
                            Hide()
                            Timer1.Stop()
                            If usaGrupo = True Then
                                Dim cGrupo As New GrupoPrincipal
                                cGrupo.ShowDialog()
                                idGrupoConta = cGrupo.idGrupo
                                If cGrupo.idGrupo = -1 Then
                                    cx.Conectar("Restaurante")
                                    cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = " & actPasado & " WHERE Id = " & idMesa)
                                    cx.DesConectar(cx.sQlconexion)
                                    Exit Sub
                                End If
                                cGrupo.Dispose()
                                ' numeroComanda = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandasActivas WITH (NOLOCK) where  cMesa=" & arrayB(1))
                                If dtComandaActiva.Rows.Count > 0 Then
                                    numeroComanda = dtComandaActiva.Rows(0).Item("cCodigo")
                                End If
                                cuentas = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from Mesas WITH (NOLOCK) where Id=" & arrayB(1))
                            Else
                                idGrupoConta = 0
                            End If
                            CMenu.estadoAnte = actPasado
                            CMenu.idMesa = idMesa
                            CMenu.comenzales = comenzales
                            CMenu.numeroComanda = numeroComanda
                            CMenu.nombreMesa = sacarnombreMesa(sender.text)
                            CMenu.idGrupo = idGrupoConta
                            CMenu.usuario = cedula1
                            CMenu.cCuentas = cuentas
                            CMenu._ParaLlevar = EsParaLlevar
                            CMenu.mesa = mesas
                            CMenu.ShowDialog()

                            iCategoria = CMenu.iCategoria

                            Dim estadoAnte As Integer = 0

                            Dim dtt As New DataTable
                            cFunciones.Llenar_Tabla_Generico("Select Activa From Mesas Where Id = " & idMesa, dtt)
                            If dtt.Rows.Count > 0 Then
                                estadoAnte = dtt.Rows(0).Item("Activa")

                            End If
                            If estadoAnte = 4 Then
                                cx.Conectar("Restaurante")
                                cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 1 WHERE Id = " & idMesa)
                                cx.DesConectar(cx.sQlconexion)
                            End If
                            If estadoAnte = 0 Then
                                cx.Conectar("Restaurante")
                                cx.SlqExecute(cx.sQlconexion, "Delete from Tb_R_Cuentas where  IdMesa = " & idMesa)
                                cx.DesConectar(cx.sQlconexion)
                            End If

                            CMenu.Dispose()
                        End While
                        'si la mesa esta en color azul para emitir factura
                        Me.Timer1.Start()
                    ElseIf arrayB(2) = 2 Then
                        If bloqueoMesaOcupada = "0" Then
                            Dim nFactura As Int64
                            Dim dt As New DataTable
                            Dim cFacturaVenta As New frmMovimientoCajaPagoAbono(User_Log.PuntoVenta) 'FacturaVenta 'Venta
                            sql.CommandText = "SELECT * from BuscaFactura WITH (NOLOCK) where Facturado=0 and anulado=0 AND IdMesa = " & idMesa & " AND Proveniencia_Venta = " & User_Log.PuntoVenta & " Order by NumeroComanda ASC"
                            cFunciones.spCargarDatos(sql, dt)
                            For i As Integer = 0 To dt.Rows.Count - 1
                                cFacturaVenta.Factura = CDbl(dt.Rows(i).Item("Num_Factura"))
                                cFacturaVenta.fecha = CDate(dt.Rows(i).Item("Fecha"))
								cFacturaVenta.Total = Math.Round(CDbl(dt.Rows(i).Item("Total")), 2)
								cFacturaVenta.codmod = dt.Rows(i).Item("Cod_Moneda")
                                cFacturaVenta.Tipo = "FV"
                                nFactura = cFacturaVenta.Factura
                            Next

                            cFacturaVenta.cedu = Cedula
                            cFacturaVenta.ShowDialog()
                            If cFacturaVenta.Hecho = True Then
                                Dim cancela As String = ""
                                cConexion.UpdateRecords(cConexion.Conectar(), "Comanda", "Facturado=1", "idMesa=" & idMesa & " and Facturado=0 and Numerofactura=" & nFactura)
                                cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Idcomanda from Comanda WITH (NOLOCK) where Fecha = '" & Now.Date & "' AND (Facturado = 0) AND (CuentaContable = '') AND (Numerofactura = 0) and IdMesa=" & idMesa) ' SAJ 
                                If cancela = "" Then
                                    cancela = ""
                                    cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Id from ComandasActivas WITH (NOLOCK) where  cMesa=" & idMesa)
                                    If cancela = "" Then
                                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & idMesa)
                                        cConexion.SlqExecute(cConexion.SqlConexion, "Delete from Tb_R_Cuentas where  IdMesa = " & idMesa)
                                    Else
                                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1", "id=" & idMesa)
                                    End If
                                Else
                                    cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=3", "id=" & idMesa)
                                End If
                            End If
                            cFacturaVenta.Dispose()
                            Dim Vuelto As Double = cFacturaVenta.vuelto
                            If GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("") Then
                                SaveSetting("SeeSoft", "Restaurante", "MostrarVuelto", "1")
                            End If

                            If (Vuelto > 0) And GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("1") Then
                                Dim fvuelto As New Vuelto
                                fvuelto.txtvuelto.Text = Vuelto
                                fvuelto.ShowDialog()
                            End If
                        End If
                       

                        'CUENTAS SEPARADAS
                    ElseIf arrayB(2) = 3 Then
                        If bloqueoMesaOcupada = "0" Then
                            If MessageBox.Show("�Desea cancelar la cuenta separada?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                                Dim nFactura As Int64
                                Dim bfactura, bFecha, bTotal, bCodMoneda As String
                                Dim fBuscadorFactura As New BusquedaFact
                                fBuscadorFactura.sqlstring = "SELECT num_factura,Fecha,Total,cod_Moneda from BuscaFactura WITH (NOLOCK) where Facturado=0 AND anulado=0 and IdMesa = " & idMesa & " AND Proveniencia_Venta = " & User_Log.PuntoVenta
                                fBuscadorFactura.ShowDialog()
                                If fBuscadorFactura.Cancelado = True Then Exit Sub
                                Try
                                    bfactura = fBuscadorFactura.factura
                                    bFecha = fBuscadorFactura.fecha
                                    bTotal = fBuscadorFactura.total
                                    bCodMoneda = fBuscadorFactura.codigoMoneda
                                Catch ex As Exception
                                    fBuscadorFactura.Dispose()
                                    Exit Sub
                                End Try
                                Dim dt As New DataTable
                                Dim cFacturaVenta As New frmMovimientoCajaPagoAbono(User_Log.PuntoVenta) 'FacturaVenta 'Venta
                                Dim strb As String = "SELECT * from BuscaFactura WITH (NOLOCK) where Facturado=0 AND anulado=0 and IdMesa = " & idMesa & " AND Proveniencia_Venta = " & User_Log.PuntoVenta & " ORDER BY NumeroComanda ASC"
                                sql.CommandText = strb
                                cFunciones.spCargarDatos(sql, dt)
                                For i As Integer = 0 To dt.Rows.Count - 1
                                    cFacturaVenta.Factura = bfactura
                                    cFacturaVenta.fecha = bFecha
									cFacturaVenta.Total = Math.Round(CDbl(bTotal), 2)
									cFacturaVenta.codmod = bCodMoneda
                                    cFacturaVenta.Tipo = "FV"
                                    nFactura = cFacturaVenta.Factura
                                Next

                                Dim x As String
                                x = cConexion.SlqExecuteScalar(cConexion.Conectar(), strb)
                                Try
                                    If x = vbNullString Or x = Nothing Then
                                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0", "id=" & idMesa)
                                        Exit Sub
                                    End If
                                Catch ex As Exception
                                End Try
                                cFacturaVenta.cedu = User_Log.Cedula
                                cFacturaVenta.ShowDialog()
                                'Me.Timer1.Enabled = True
                                If cFacturaVenta.Hecho = True Then
                                    Dim cancela As String = ""
                                    cConexion.UpdateRecords(cConexion.Conectar(), "Comanda", "Facturado=1", "idMesa=" & idMesa & " and Facturado=0 and Numerofactura=" & nFactura)
                                    cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Idcomanda from Comanda WITH (NOLOCK) where Facturado=0 and IdMesa=" & idMesa)
                                    If cancela = "" Then
                                        cancela = ""
                                        cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Id from ComandasActivas WITH (NOLOCK) where  cMesa=" & idMesa)
                                        If cancela = "" Then
                                            cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & idMesa)
                                        Else
                                            cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1", "id=" & idMesa)
                                        End If
                                    Else
                                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=3", "id=" & idMesa)
                                    End If
                                End If
                                cFacturaVenta.Dispose()
                            Else
                                If numeroComanda = "" Then
                                    MsgBox("La mesa actual esta a cobro...", MsgBoxStyle.Information, "Atenci�n...")
                                    'Me.Close()
                                    Exit Sub
                                End If
                                Dim cx As New Conexion
                                cx.Conectar("Restaurante")
                                cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 4 WHERE Id = " & idMesa)
                                cx.DesConectar(cx.sQlconexion)

                                Dim CMenu As New ComandaMenu
                                Hide()
                                Timer1.Stop()
                                If usaGrupo = True Then
                                    Dim cGrupo As New GrupoPrincipal
                                    cGrupo.ShowDialog()
                                    idGrupoConta = cGrupo.idGrupo
                                    cGrupo.Dispose()
                                Else
                                    idGrupoConta = 0
                                End If
                                CMenu.idMesa = idMesa
                                CMenu.usuario = cedula1 'psv
                                CMenu.comenzales = comenzales
                                CMenu.idGrupo = idGrupoConta
                                CMenu.numeroComanda = numeroComanda
                                CMenu.nombreMesa = sacarnombreMesa(sender.text)
                                CMenu._ParaLlevar = EsParaLlevar
                                CMenu.cCuentas = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from mesas where id =" & arrayB(1))
                                CMenu.mesa = mesas
                                CMenu.ShowDialog()
                                Dim estadoAnte As Integer = 0
                                Dim dtt As New DataTable
                                cFunciones.Llenar_Tabla_Generico("Select Activa From Mesas Where Id = " & idMesa, dtt)
                                If dtt.Rows.Count > 0 Then
                                    estadoAnte = dtt.Rows(0).Item("Activa")

                                End If
                                If estadoAnte = 4 Then
                                    cx.Conectar("Restaurante")
                                    cx.SlqExecute(cx.sQlconexion, "UPDATE Mesas Set Activa = 1 WHERE Id = " & idMesa)
                                    cx.DesConectar(cx.sQlconexion)

                                End If

                                CMenu.Dispose()
                            End If
                        End If
                     
                        End If
                    Show()
                    crearMesas(False)

                Else 'si se va a cambiar de mesa
                    Cam_Mesa(arrayB, idMesa)
                End If
            End If
        
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Clickbuttons", ex.Message)
        End Try
    End Sub
    Function sacarnombreMesa(ByVal nombreMesa As String) As String

        Try
            Dim indx As Integer = nombreMesa.IndexOf("*")
            If indx < 0 Then
                Return nombreMesa
            End If
            Return nombreMesa.Substring(0, indx)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "sacarnombreMesa", ex.Message)
        End Try

    End Function

    Private Sub spRefrescarMesas()
        Try
            If DateDiff(DateInterval.Second, TiempoInicial, Now) <= TiempoEspera Then
                LabelTiempo.Text = TiempoEspera - DateDiff(DateInterval.Second, TiempoInicial, Now) + 1
            Else
                If LabelTiempo.Text <> "Actualizando..." Then
                    TiempoInicial = Now
                    LabelTiempo.Text = "Actualizando..."

                    PanelMesas.Visible = False

                    UbicarPosiciones(False)

                    PanelMesas.Visible = True
                ElseIf DateDiff(DateInterval.Second, TiempoInicial, Now) >= TiempoEspera Then
                    TiempoInicial = Now
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Timer1_Tick", ex.Message)
        End Try
    End Sub


    Private Sub Cam_Mesa(ByVal arrayB As Array, ByVal idMesa As Integer)
        Try
            If idnuevo = "" Then 'si elige la mesa de origen
                If CBool(arrayB(2)) = True Then 'si la mesa esta ocupada
                    If arrayB(2) = 2 Then
                        MessageBox.Show("No se puede cambiar esta mesa", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        cambio = False
                        lbestado.Text = ""
                        Exit Sub
                    End If
                    idMesa = arrayB(1)
                    idnuevo = idMesa
                    Show()
                    crearMesas(False)
                    lbestado.Text = "Elija la mesa a utilizar"
                Else 'si la mesa esta libre
                    MessageBox.Show("La mesa seleccionada no est� ocupada actualmente", "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    cambio = False
                    lbestado.Text = ""
                End If
            Else ' si elige la mesa final
                If CBool(arrayB(2)) = False Then 'si la mesa esta libre
                    idMesa = arrayB(1)
                    Dim sep, comenza As String
                    Try
                        cConexion.UpdateRecords(cConexion.Conectar(), "ComandaTemporal", "cMesa=" & idMesa, "cMesa=" & idnuevo)
                        sep = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from mesas where id=" & idnuevo)
                        comenza = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select numero_asientos from mesas where id=" & idnuevo)
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1,separada=" & sep & ", numero_asientos = " & comenza, "id=" & idMesa)
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & idnuevo)
                        cConexion.UpdateRecords(cConexion.Conectar(), "Tb_R_Cuentas", "IdMesa = " & idMesa, "IdMesa =" & idnuevo)
                        idnuevo = ""
                        cambio = False
                        Show()
                        crearMesas(False)
                    Catch ex As Exception
                        MessageBox.Show("No se puede realizar el cambio de mesa", "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
                        cFunciones.spEnviar_Correo(Me.Name, "Cam_Mesa", ex.Message)
                    End Try
                Else ' si la mesa esta ocupada
                    If MessageBox.Show("La mesa seleccionada est� ocupada actualmente, Desea unir las mesas?", "Atencion...", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.OK Then

                        Dim mesaOrigen As Integer = idnuevo
                        Dim mesaDestino As Integer = idMesa
                        If mesaOrigen = mesaDestino Then
                            MsgBox("No se puede seleccionar la misma mesa", MsgBoxStyle.Critical, "SeeSoft")
                            idnuevo = ""
                            cambio = False
                            Show()
                            crearMesas(False)
                            Exit Sub
                        End If
                        Dim IdComanda As Integer = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select MAX(cCodigo) AS CodigoComanda from ComandasActivas where  cMesa=" & mesaOrigen)

                        Dim sep As Integer = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from mesas where id=" & mesaDestino)

                        If sep > 0 Then
                            cConexion.UpdateRecords(cConexion.Conectar(), "ComandaTemporal", "separada = 1", "cMesa=" & mesaOrigen)

                        End If

                        cConexion.UpdateRecords(cConexion.Conectar(), "ComandaTemporal", "cCodigo=" & IdComanda, "cMesa=" & mesaDestino)

                        cConexion.UpdateRecords(cConexion.Conectar(), "ComandaTemporal", "cMesa=" & mesaDestino, "cMesa=" & mesaOrigen)

                        'Cantidad de Gente
                        Dim comenza As Integer = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select numero_asientos from mesas where id=" & mesaOrigen)

                        'Ocupa Mesa Origen con el numero de asientos
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1,numero_asientos = " & comenza, "id=" & mesaDestino)
                        '******************************
                        'Desocupa Mesa Origen***********
                        cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & mesaOrigen)
                        '**************
                        idnuevo = ""
                        cambio = False
                        Show()
                        crearMesas(False)

                    End If

                    cambio = False
                End If
                lbestado.Text = ""
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Cam_Mesa", ex.Message)
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        spRefrescarMesas()
    End Sub

    Private Function VerificarAperturaCaja(ByVal Usuario As String) As String
        Try
            Dim Cconexion As New ConexionR
            Dim SqlCon As New SqlClient.SqlConnection
            Dim AperturaCaja As String
            Cconexion.Conectar()
            AperturaCaja = Cconexion.SlqExecuteScalar(Cconexion.SqlConexion, "SELECT NApertura FROM aperturacaja WITH (NOLOCK) WHERE (Estado = 'A') AND (Anulado = 0) AND (Cedula = '" & Usuario & "')")
            Cconexion.DesConectar(Cconexion.SqlConexion)
            If AperturaCaja <> "" Then Return AperturaCaja Else Return "0"
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "VerificarAperturaCaja", ex.Message)
        End Try
    End Function

    Private Sub ToolButtonBuscarFactura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonBuscarFactura.Click
        Dim BuscarFacturas As New BuscarFactura
        Hide()
        BuscarFacturas.cedula = User_Log.Cedula
        BuscarFacturas.ShowDialog()
        BuscarFacturas.Dispose()
        Show()
    End Sub

    Private Sub ToolButtonCambiarMesa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonCambiarMesa.Click
        If MessageBox.Show("Desea cambiar la mesa", "Cambio de Mesa", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            lbestado.Text = "Elija la mesa a cambiar"
            cambio = True
        Else
            cambio = False
            lbestado.Text = ""
        End If
    End Sub

    Private Sub ToolButtonTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonTerminar.Click
        Me.Dispose()
        Close()
    End Sub


    'Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If ToolStripButton1.BackColor = Color.Yellow Then
    '        ToolStripButton1.BackColor = Color.Transparent
    '    Else
    '        ToolStripButton1.BackColor = Color.Yellow
    '    End If
    'End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim busqueda As New Buscar
        busqueda.sqlstring = "Select * from menuBusqueda"
        busqueda.campo = "Nombre"
        busqueda.ShowDialog()
    End Sub

    Private Sub ButtonCuentasExpress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        verListadoLlevar()

    End Sub
    Sub verListadoLlevar()
        Dim frm_Express As New FormCuentasALlevar
        Hide()
        'frm_Express.cedula = User_Log.Cedula
        frm_Express.ShowDialog()
        frm_Express.Dispose()
        Show()

    End Sub

    Sub crearPedido()
        Try
            Dim idMesa As String = "0"
            Dim comenzales As String = "1"
            Dim numeroComanda As String = "0"
            Dim idGrupoConta As String = "0"

ReintentarClave:

            Dim rEmpleado0 As New registro
            rEmpleado0.Text = "DIGITE CONTRASE�A"
            rEmpleado0.txtCodigo.PasswordChar = "*"

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
            If gloNoClave Then
                clave = User_Log.Clave_Interna
                rEmpleado0.iOpcion = 1
            Else
                rEmpleado0.ShowDialog()
                clave = rEmpleado0.txtCodigo.Text
            End If

            '---------------------------------------------------------------

            rEmpleado0.Dispose()
            If rEmpleado0.iOpcion = 0 Then Exit Sub

            If clave <> "" Then
                cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
                If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                    MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                    GoTo ReintentarClave
                    Exit Sub
                End If
            Else
                GoTo ReintentarClave
            End If
            Dim iCategoria As Integer = 1
            While iCategoria = 1
                Dim platos As New PlatosExpress

                Hide()

                If usaGrupo = True Then
                    Dim cGrupo As New GrupoPrincipal
                    cGrupo.ShowDialog()
                    idGrupoConta = cGrupo.idGrupo
                    If cGrupo.idGrupo = -1 Then
                        Exit Sub
                    End If
                    cGrupo.Dispose()
                    numeroComanda = 0 '= cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandaTemporal WITH (NOLOCK) where cMesa=" & arrayB(1))
                    ' cuentas = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from Mesas WITH (NOLOCK) where Id=" & arrayB(1))
                Else
                    idGrupoConta = 0
                End If
                platos.idMesa = idMesa
                platos.comenzales = comenzales
                platos.numeroComanda = numeroComanda
                platos.nombreMesa = "Pedido Express"
                platos.idGrupo = idGrupoConta
                platos.usuario = cedula1
                platos.cCuentas = 0
                platos.ShowDialog()
                iCategoria = platos.iCategoria

                cargarPedido(platos.numeroComanda)
                'buscarCliente(Me.TextBoxTelefono.Text, True)
                ' guardarPedidoExpress()
                platos.Dispose()
                Show()
                'listadoPedidos()

            End While

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "crearPedido", ex.Message)
        End Try
    End Sub
    Sub cargarPedido(ByVal IdComanda As Integer)
        Try
            Dim SubTotal As Double = 0
            Dim SubTotalG As Double = 0
            Dim pedidoExpress As DataTable = Nothing

            cFunciones.Llenar_Tabla_Generico("SELECT dbo.ComandaTemporal_Expres.Id, dbo.ComandaTemporal_Expres.cProducto, dbo.ComandaTemporal_Expres.cCantidad, " & _
                          "dbo.ComandaTemporal_Expres.cDescripcion, dbo.ComandaTemporal_Expres.cPrecio, dbo.ComandaTemporal_Expres.cCodigo, " & _
                          "dbo.ComandaTemporal_Expres.cMesa, dbo.ComandaTemporal_Expres.Impreso, dbo.ComandaTemporal_Expres.Impresora, " & _
                          "dbo.ComandaTemporal_Expres.Comenzales, dbo.ComandaTemporal_Expres.Tipo_Plato, dbo.ComandaTemporal_Expres.CantImp, " & _
                          "dbo.ComandaTemporal_Expres.Cedula, dbo.ComandaTemporal_Expres.Habitacion, dbo.ComandaTemporal_Expres.primero, " & _
                          "dbo.ComandaTemporal_Expres.separada, dbo.ComandaTemporal_Expres.hora, dbo.ComandaTemporal_Expres.Express, " & _
                          "dbo.ComandaTemporal_Expres.cantidad, dbo.ComandaTemporal_Expres.costo_real, dbo.ComandaTemporal_Expres.comandado, " & _
                          "dbo.ComandaTemporal_Expres.cPrecio * dbo.ComandaTemporal_Expres.cCantidad AS SubTotal, dbo.Menu_Restaurante.ImpVenta as iVenta, " & _
                        "dbo.Menu_Restaurante.ImpServ as iServicio" & _
                        " FROM dbo.ComandaTemporal_Expres INNER JOIN " & _
                          "dbo.Menu_Restaurante ON dbo.ComandaTemporal_Expres.cProducto = dbo.Menu_Restaurante.Id_Menu Where cCodigo = " & IdComanda, pedidoExpress)
            For i As Integer = 0 To pedidoExpress.Rows.Count - 1

                If pedidoExpress.Rows(i).Item("iVenta") Then
                    SubTotalG += pedidoExpress.Rows(i).Item("SubTotal")

                End If
                SubTotal += pedidoExpress.Rows(i).Item("SubTotal")

            Next
            'Me.TextBoxSubTotal.Text = Format(SubTotal, "##,#0.00")
            'Dim iVenta As Double = SubTotalG * (0.13)
            'Me.TextBoxImpVenta.Text = Format(iVenta, "##,#0.00")
            'Me.TextBoxTotal.Text = Format(SubTotal + iVenta, "#,#0.00")

            'Me.Panel1.Visible = True

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "cargarPedido", ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        crearFactura()
    End Sub
    Sub crearFactura(Optional ByVal ParaLlevar As Integer = 0)
        Try
            Dim cedula1 As String = ""
            Dim idMesa As String = "0"
            Dim comenzales As String = "1"
            Dim numeroComanda As String = "0"
            Dim idGrupoConta As String = "0"

ReintentarClave:

            Dim rEmpleado0 As New registro
            rEmpleado0.Text = "DIGITE CONTRASE�A"
            rEmpleado0.txtCodigo.PasswordChar = "*"

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
            cConexion.DesConectar(cConexion.SqlConexion)
            Dim NoClave As Boolean = True 'cConexion.SlqExecuteScalar(cConexion.Conectar("Hotel"), "SELECT NoClave FROM Configuraciones")
            If NoClave Then
                clave = User_Log.Clave_Interna
                rEmpleado0.iOpcion = 1
            Else
                rEmpleado0.ShowDialog()
                clave = rEmpleado0.txtCodigo.Text
            End If
            cConexion.DesConectar(cConexion.SqlConexion)
            '---------------------------------------------------------------

            rEmpleado0.Dispose()
            If rEmpleado0.iOpcion = 0 Then Exit Sub

            If clave <> "" Then
                cedula1 = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
                If Trim(cedula1) = "" Or Trim(cedula1) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                    MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atenci�n...")
                    GoTo ReintentarClave
                    Exit Sub
                End If
            Else
                GoTo ReintentarClave
            End If
            Dim iCategoria As Integer = 1
            While iCategoria = 1
                Dim platos As New PlatosExpress(0, True)

                Hide()

                If usaGrupo = True Then
                    Dim cGrupo As New GrupoPrincipal
                    cGrupo.ShowDialog()
                    idGrupoConta = cGrupo.idGrupo
                    If cGrupo.idGrupo = -1 Then
                        Show()
                        Exit Sub
                    End If
                    cGrupo.Dispose()
                    numeroComanda = 0 '= cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandaTemporal WITH (NOLOCK) where cMesa=" & arrayB(1))
                    ' cuentas = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select separada from Mesas WITH (NOLOCK) where Id=" & arrayB(1))
                Else
                    idGrupoConta = 0
                End If
                platos.TipoPedido = 0 '1
                platos.ParaLlevar = ParaLlevar
                platos.idMesa = idMesa
                platos.comenzales = comenzales
                platos.numeroComanda = numeroComanda
                platos.nombreMesa = "Pedido Express"
                platos.idGrupo = idGrupoConta
                platos.usuario = cedula1
                platos.cCuentas = 0
                platos.ShowDialog()
                iCategoria = platos.iCategoria
                platos.esParaLlevar = True 'hh

                platos.Dispose()
                Show()


            End While
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "crearFactura", ex.Message)
        End Try
    End Sub

    Private Sub botonVerComandas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles botonVerComandas.Click
        Dim frmV As New FormVerComandas
        Dim frmCentroComandas As New frmCentroComandas
        Dim CentroComandas As String = GetSetting("SeeSoft", "Restaurante", "CentroComanda")

        If CentroComandas.Equals("Si") Then
            frmCentroComandas.ShowDialog()
        Else
            frmV.ShowDialog()
        End If
    End Sub

    Public Sub New()

        ' Llamada necesaria para el Dise�ador de Windows Forms.
        InitializeComponent()

        ' Agregue cualquier inicializaci�n despu�s de la llamada a InitializeComponent().

    End Sub



    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        If ToolStripButton1.BackColor = Color.Yellow Then
            ToolStripButton1.BackColor = Color.Transparent
        Else
            ToolStripButton1.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub btParallevar_Click(sender As Object, e As EventArgs) Handles btParallevar.Click
        crearFactura(1)
    End Sub

    Private Sub btExpress_Click(sender As Object, e As EventArgs) Handles btExpress.Click
        verListadoLlevar()
    End Sub

    Private Sub ToolStripUnirMesa_Click(sender As Object, e As EventArgs) Handles ToolStripUnirMesa.Click
        If Not SepararMesa Then
            If ToolStripUnirMesa.BackColor = Color.Yellow Then
                ToolStripUnirMesa.BackColor = Color.Transparent
                ToolStripUnirMesa.Text = "Unir Mesa"
                spUnirMesa()
                UnirMesa = False
            Else
                ToolStripUnirMesa.BackColor = Color.Yellow
                ToolStripUnirMesa.Text = "Terminar"
                UnirMesa = True
            End If
        End If

    End Sub
    Private Sub spUnirMesa()
        Try
            Dim MesaSecundaria As Integer = 0
            Dim MesaPrimaria As Integer = 0
            Dim cuentas As Integer = 0
            Dim NumeroComanda As String = 0

            If cArrayUnirMesa.Count > 1 Then
                MesaPrimaria = cConexion.SlqExecuteScalar(cConexion.Conectar(), "SELECT isnull(MAX(Cuenta),0) as Cuenta from Tb_R_Cuentas where IdMesa = " & cArrayUnirMesa.Item(0))
                NumeroComanda = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandasActivas WITH (NOLOCK) where  cMesa=" & cArrayUnirMesa.Item(0))
                For i As Integer = 1 To cArrayUnirMesa.Count - 1
                    MesaSecundaria = cConexion.SlqExecuteScalar(cConexion.SqlConexion, "Select separada from mesas where id=" & cArrayUnirMesa.Item(i))
                    If IsNothing(NumeroComanda) Then
                        NumeroComanda = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select cCodigo from ComandasActivas WITH (NOLOCK) where  cMesa=" & cArrayUnirMesa.Item(i))
                    End If
                    If UnionSeparada Then
                        If MesaPrimaria = 0 Then
                            cConexion.SlqExecute(cConexion.SqlConexion, "UPDATE Mesas SET separada = " & 1 & ",Activa = 1 Where Id = " & cArrayUnirMesa.Item(0))
                            cConexion.SlqExecute(cConexion.SqlConexion, "UPDATE ComandaTemporal Set separada = " & 1 & " WHERE Estado = 'Activo' and cMesa = " & cArrayUnirMesa.Item(0))
                            cConexion.SlqExecute(cConexion.SqlConexion, "INSERT INTO Tb_R_Cuentas ([IdMesa],[Cuenta],[NombreCuenta]) VALUES ('" & cArrayUnirMesa.Item(0) & "','" & 1 & "','" & cConexion.SlqExecuteScalar(cConexion.Conectar(), "select nombre_mesa from Mesas where Id =" & cArrayUnirMesa.Item(0)) & "')")
                            MesaPrimaria = 1
                        End If
                        If MesaSecundaria = 0 Then
                            MesaPrimaria = fnIngresarCuenta(cArrayUnirMesa.Item(0), cArrayUnirMesa.Item(i), MesaPrimaria, False)
                        Else
                            MesaPrimaria = fnIngresarCuenta(cArrayUnirMesa.Item(0), cArrayUnirMesa.Item(i), MesaPrimaria, True)
                        End If
                    End If

                Next
                If IsNothing(NumeroComanda) Then NumeroComanda = 0
                spGuardarUnion(NumeroComanda)
            Else
                MessageBox.Show("Debe seleccionar mas de una mesa.", "Atenci�n...", MessageBoxButtons.OK)
            End If
            ToolStripUnirMesa.BackColor = Color.Transparent
            ToolStripUnirMesa.Text = "Unir Mesa"
            UnirMesa = False
            cArrayUnirMesa.Clear()
            crearMesas(False)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spUnirMesa", ex.Message)
        End Try
    End Sub

    Private Function fnIngresarCuenta(ByVal _IdNuevo As String, ByVal _IdOld As String, ByVal _cuenta As String, ByVal _TieneCuenta As Boolean) As Integer
        Try
            Dim dt As New DataTable
            Dim Fx As New cFunciones
            Dim sql As New SqlClient.SqlCommand
            Dim NombreCuenta As String = ""
            Dim conexion As String = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            If _TieneCuenta Then
                sql.CommandText = "Select *  from Tb_R_Cuentas where IdMesa=" & _IdOld
                Fx.spCargarDatos(sql, dt, conexion)

                For i As Integer = 0 To dt.Rows.Count - 1
                    _cuenta += 1

                    If dt.Rows(i).Item("NombreCuenta").Equals("") Then NombreCuenta = "Cuenta " & dt.Rows(i).Item("Cuenta") & " " & cConexion.SlqExecuteScalar(cConexion.Conectar(), "select nombre_mesa from Mesas where Id =" & _IdOld) Else NombreCuenta = dt.Rows(i).Item("NombreCuenta")
                    sql.CommandText = "UPDATE Tb_R_Cuentas set IdMesa ='" & _IdNuevo & "' ,Cuenta = '" & _cuenta & "',NombreCuenta = '" & NombreCuenta & "' where IdMesa = " & _IdOld & " and Cuenta = " & dt.Rows(i).Item("Cuenta")
                    Fx.spEjecutar(sql, conexion)
                    sql.CommandText = "UPDATE ComandaTemporal Set separada = " & _cuenta & ", cMesa = " & _IdNuevo & " WHERE Estado = 'Activo' and separada = " & dt.Rows(i).Item("Cuenta") & " and cMesa = " & _IdOld
                    Fx.spEjecutar(sql, conexion)
                Next
                sql.CommandText = "UPDATE Mesas SET separada = 0, Activa = 0 Where Id = " & _IdOld
                Fx.spEjecutar(sql, conexion)
                sql.CommandText = "UPDATE Mesas SET separada = (Select count(*)  from Tb_R_Cuentas where IdMesa = " & _IdNuevo & "), Activa = 1 Where Id = " & _IdNuevo
                Fx.spEjecutar(sql, conexion)
            Else
                _cuenta += 1
                NombreCuenta = cConexion.SlqExecuteScalar(cConexion.Conectar(), "select nombre_mesa from Mesas where Id =" & _IdOld)
                sql.CommandText = "INSERT INTO Tb_R_Cuentas ([IdMesa],[Cuenta],[NombreCuenta]) VALUES ('" & _IdNuevo & "','" & _cuenta & "','" & NombreCuenta & "')"
                Fx.spEjecutar(sql, conexion)
                sql.CommandText = "UPDATE ComandaTemporal Set separada = " & _cuenta & ", cMesa = " & _IdNuevo & " WHERE Estado = 'Activo' and cMesa = " & _IdOld
                Fx.spEjecutar(sql, conexion)
                sql.CommandText = "UPDATE Mesas SET separada = 0, Activa = 0 Where Id = " & _IdOld
                Fx.spEjecutar(sql, conexion)
                sql.CommandText = "UPDATE Mesas SET separada = (Select count(*)  from Tb_R_Cuentas where IdMesa = " & _IdNuevo & "), Activa = 1 Where Id = " & _IdNuevo
                Fx.spEjecutar(sql, conexion)
            End If

            Return _cuenta

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnIngresarCuenta", ex.Message)
        End Try
    End Function
    Private Sub spGuardarUnion(ByVal _numerocomanda As String)
        Try
            Dim cx As New ConexionR
            For i As Integer = 1 To cArrayUnirMesa.Count - 1
                cx.SlqExecute(cx.Conectar, "INSERT INTO [dbo].[Tb_R_MesaUnidas] ([idMesaPrincipal],[idMesa]) VALUES ('" & cArrayUnirMesa.Item(0) & "','" & cArrayUnirMesa.Item(i) & "')")
            Next
            cConexion.SlqExecute(cConexion.SqlConexion, "UPDATE ComandaTemporal Set cCodigo = " & _numerocomanda & " WHERE Estado = 'Activo' and cMesa = " & cArrayUnirMesa.Item(0))

            cx.DesConectar(cx.SqlConexion)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spGuardarUnion", ex.Message)
        End Try
    End Sub

    Private Sub spSepararMesas(ByVal _IdMesa As String)
        Try
            Dim dtMesaUnidas As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT  distinct idMesaPrincipal, idMesa FROM Tb_R_MesaUnidas", dtMesaUnidas)
            Dim sql As New SqlClient.SqlCommand
            If dtMesaUnidas.Rows.Count > 0 Then

                If dtMesaUnidas.Select("idMesaPrincipal = " & _IdMesa).Count > 0 Then
                    dtMesaUnidas.Clear()
                    cFunciones.Llenar_Tabla_Generico("SELECT  * FROM ComandasActivas where  cMesa = " & _IdMesa, dtMesaUnidas)
                    If dtMesaUnidas.Rows.Count > 0 Then
                        sql.CommandText = "  UPDATE Mesas SET Activa = 1 Where Id = " & _IdMesa
                    Else
                        sql.CommandText = "  UPDATE Mesas SET separada = 0, Activa = 0 Where Id = " & _IdMesa
                    End If
                    sql.CommandText += "delete from Tb_R_MesaUnidas where idMesaPrincipal = " & _IdMesa
                    cFunciones.spEjecutar(sql, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                    ToolStripSepararMesa.BackColor = Color.Transparent
                    ToolStripSepararMesa.Text = "Separar Mesa"
                    SepararMesa = False

                End If
            End If
        Catch ex As Exception
            MessageBox.Show("No se realiz� la operaci�n de Separar Mesas.")
            cFunciones.spEnviar_Correo(Me.Name, "spSepararMesas", ex.Message)
        End Try

    End Sub


    Private Sub ToolStripSepararMesa_Click(sender As Object, e As EventArgs) Handles ToolStripSepararMesa.Click
        If Not UnirMesa Then
            If ToolStripSepararMesa.BackColor = Color.Yellow Then
                ToolStripSepararMesa.BackColor = Color.Transparent
                ToolStripSepararMesa.Text = "Separar Mesa"
                SepararMesa = False
            Else
                ToolStripSepararMesa.BackColor = Color.Yellow
                ToolStripSepararMesa.Text = "Cancelar"
                SepararMesa = True
            End If
        End If

    End Sub
End Class