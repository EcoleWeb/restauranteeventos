<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCuentasALlevar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBoxListado = New System.Windows.Forms.GroupBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkLabel6 = New System.Windows.Forms.LinkLabel()
        Me.CheckBoxLlevar = New System.Windows.Forms.CheckBox()
        Me.TextBoxNombreCliente = New System.Windows.Forms.TextBox()
        Me.TextBoxDireccion = New System.Windows.Forms.TextBox()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.TextBoxTelefono = New System.Windows.Forms.TextBox()
        Me.TextBoxComentario = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgCliente = New System.Windows.Forms.DataGridView()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel7 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.txtCorreo = New System.Windows.Forms.TextBox()
        Me.txtTransporte = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxImpVenta = New System.Windows.Forms.TextBox()
        Me.TextBoxSubTotal = New System.Windows.Forms.TextBox()
        Me.TextBoxTotal = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.CheckBoxEntregado = New System.Windows.Forms.CheckBox()
        Me.Labeltotal = New System.Windows.Forms.Label()
        Me.LabelTiempoPrep = New System.Windows.Forms.Label()
        Me.LabelTiempo = New System.Windows.Forms.Label()
        Me.textdescuento = New System.Windows.Forms.TextBox()
        Me.CheckBoxPreparando = New System.Windows.Forms.CheckBox()
        Me.LabelIv = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.textImpuestoServicio = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTotalTransporte = New System.Windows.Forms.TextBox()
        Me.lbMarcarEntregado = New System.Windows.Forms.LinkLabel()
        Me.GroupBoxListado.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBoxListado
        '
        Me.GroupBoxListado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxListado.Controls.Add(Me.DataGridView2)
        Me.GroupBoxListado.Location = New System.Drawing.Point(9, 33)
        Me.GroupBoxListado.Name = "GroupBoxListado"
        Me.GroupBoxListado.Size = New System.Drawing.Size(184, 598)
        Me.GroupBoxListado.TabIndex = 1
        Me.GroupBoxListado.TabStop = False
        Me.GroupBoxListado.Text = "Pedidos Pendientes"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToResizeColumns = False
        Me.DataGridView2.AllowUserToResizeRows = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DataGridView2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DeepSkyBlue
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cliente})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DeepSkyBlue
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.GridColor = System.Drawing.Color.White
        Me.DataGridView2.Location = New System.Drawing.Point(3, 16)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.RowHeadersWidth = 50
        Me.DataGridView2.RowTemplate.Height = 35
        Me.DataGridView2.Size = New System.Drawing.Size(178, 579)
        Me.DataGridView2.TabIndex = 0
        '
        'Cliente
        '
        Me.Cliente.DataPropertyName = "Cliente"
        Me.Cliente.HeaderText = "Cliente"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        Me.Cliente.Width = 150
        '
        'LinkLabel6
        '
        Me.LinkLabel6.AutoSize = True
        Me.LinkLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel6.Location = New System.Drawing.Point(271, 201)
        Me.LinkLabel6.Name = "LinkLabel6"
        Me.LinkLabel6.Size = New System.Drawing.Size(142, 20)
        Me.LinkLabel6.TabIndex = 35
        Me.LinkLabel6.TabStop = True
        Me.LinkLabel6.Text = "Asignar Repartidor"
        Me.LinkLabel6.Visible = False
        '
        'CheckBoxLlevar
        '
        Me.CheckBoxLlevar.AutoSize = True
        Me.CheckBoxLlevar.BackColor = System.Drawing.Color.White
        Me.CheckBoxLlevar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxLlevar.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxLlevar.Location = New System.Drawing.Point(273, 185)
        Me.CheckBoxLlevar.Name = "CheckBoxLlevar"
        Me.CheckBoxLlevar.Size = New System.Drawing.Size(127, 20)
        Me.CheckBoxLlevar.TabIndex = 24
        Me.CheckBoxLlevar.Text = "Vienen a Traerlo"
        Me.CheckBoxLlevar.UseVisualStyleBackColor = False
        Me.CheckBoxLlevar.Visible = False
        '
        'TextBoxNombreCliente
        '
        Me.TextBoxNombreCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxNombreCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNombreCliente.Location = New System.Drawing.Point(273, 65)
        Me.TextBoxNombreCliente.Name = "TextBoxNombreCliente"
        Me.TextBoxNombreCliente.Size = New System.Drawing.Size(429, 21)
        Me.TextBoxNombreCliente.TabIndex = 18
        Me.TextBoxNombreCliente.Text = "CONTADO"
        '
        'TextBoxDireccion
        '
        Me.TextBoxDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDireccion.Location = New System.Drawing.Point(273, 87)
        Me.TextBoxDireccion.Multiline = True
        Me.TextBoxDireccion.Name = "TextBoxDireccion"
        Me.TextBoxDireccion.Size = New System.Drawing.Size(429, 23)
        Me.TextBoxDireccion.TabIndex = 19
        '
        'LabelNombre
        '
        Me.LabelNombre.AutoSize = True
        Me.LabelNombre.ForeColor = System.Drawing.Color.DimGray
        Me.LabelNombre.Location = New System.Drawing.Point(220, 67)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(47, 13)
        Me.LabelNombre.TabIndex = 20
        Me.LabelNombre.Text = "Nombre:"
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelTelefono
        '
        Me.LabelTelefono.AutoSize = True
        Me.LabelTelefono.Location = New System.Drawing.Point(215, 9)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(52, 13)
        Me.LabelTelefono.TabIndex = 17
        Me.LabelTelefono.Text = "Telefono:"
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBoxTelefono
        '
        Me.TextBoxTelefono.BackColor = System.Drawing.Color.Moccasin
        Me.TextBoxTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTelefono.Location = New System.Drawing.Point(273, 6)
        Me.TextBoxTelefono.Name = "TextBoxTelefono"
        Me.TextBoxTelefono.Size = New System.Drawing.Size(119, 26)
        Me.TextBoxTelefono.TabIndex = 16
        Me.TextBoxTelefono.Text = "0"
        '
        'TextBoxComentario
        '
        Me.TextBoxComentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxComentario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxComentario.Location = New System.Drawing.Point(273, 133)
        Me.TextBoxComentario.Multiline = True
        Me.TextBoxComentario.Name = "TextBoxComentario"
        Me.TextBoxComentario.Size = New System.Drawing.Size(429, 23)
        Me.TextBoxComentario.TabIndex = 26
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgCliente)
        Me.Panel2.Location = New System.Drawing.Point(203, 35)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(517, 854)
        Me.Panel2.TabIndex = 27
        Me.Panel2.Visible = False
        '
        'dgCliente
        '
        Me.dgCliente.AllowUserToAddRows = False
        Me.dgCliente.AllowUserToDeleteRows = False
        Me.dgCliente.AllowUserToResizeColumns = False
        Me.dgCliente.AllowUserToResizeRows = False
        Me.dgCliente.BackgroundColor = System.Drawing.Color.White
        Me.dgCliente.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgCliente.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgCliente.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DeepSkyBlue
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgCliente.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgCliente.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgCliente.GridColor = System.Drawing.Color.White
        Me.dgCliente.Location = New System.Drawing.Point(70, 3)
        Me.dgCliente.MultiSelect = False
        Me.dgCliente.Name = "dgCliente"
        Me.dgCliente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgCliente.RowHeadersVisible = False
        Me.dgCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgCliente.Size = New System.Drawing.Size(364, 217)
        Me.dgCliente.TabIndex = 0
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(424, 9)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(117, 20)
        Me.LinkLabel1.TabIndex = 28
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Guardar Nuevo"
        Me.LinkLabel1.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.DimGray
        Me.Label3.Location = New System.Drawing.Point(212, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Direccion:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.DimGray
        Me.Label4.Location = New System.Drawing.Point(199, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Comentarios:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel2.Location = New System.Drawing.Point(424, 9)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(68, 20)
        Me.LinkLabel2.TabIndex = 31
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Guardar"
        Me.LinkLabel2.Visible = False
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel3.Location = New System.Drawing.Point(269, 202)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(228, 20)
        Me.LinkLabel3.TabIndex = 32
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Crear Pedido Para Este Cliente"
        Me.LinkLabel3.Visible = False
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel4.Location = New System.Drawing.Point(269, 201)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(163, 20)
        Me.LinkLabel4.TabIndex = 33
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Modificar Este Pedido"
        Me.LinkLabel4.Visible = False
        '
        'LinkLabel5
        '
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel5.Location = New System.Drawing.Point(553, 201)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(155, 20)
        Me.LinkLabel5.TabIndex = 34
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "Eliminar Este Pedido"
        Me.LinkLabel5.Visible = False
        '
        'LinkLabel7
        '
        Me.LinkLabel7.AutoSize = True
        Me.LinkLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel7.Location = New System.Drawing.Point(662, 8)
        Me.LinkLabel7.Name = "LinkLabel7"
        Me.LinkLabel7.Size = New System.Drawing.Size(40, 20)
        Me.LinkLabel7.TabIndex = 36
        Me.LinkLabel7.TabStop = True
        Me.LinkLabel7.Text = "Salir"
        '
        'LinkLabel8
        '
        Me.LinkLabel8.AutoSize = True
        Me.LinkLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel8.Location = New System.Drawing.Point(569, 9)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(65, 20)
        Me.LinkLabel8.TabIndex = 37
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "Eliminar"
        Me.LinkLabel8.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.DimGray
        Me.Label6.Location = New System.Drawing.Point(212, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 39
        Me.Label6.Text = "Cedula:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.DimGray
        Me.Label7.Location = New System.Drawing.Point(212, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "Correo:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCedula
        '
        Me.txtCedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCedula.Location = New System.Drawing.Point(273, 43)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(429, 21)
        Me.txtCedula.TabIndex = 42
        '
        'txtCorreo
        '
        Me.txtCorreo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCorreo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCorreo.Location = New System.Drawing.Point(273, 111)
        Me.txtCorreo.Name = "txtCorreo"
        Me.txtCorreo.Size = New System.Drawing.Size(429, 21)
        Me.txtCorreo.TabIndex = 43
        '
        'txtTransporte
        '
        Me.txtTransporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTransporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTransporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransporte.Location = New System.Drawing.Point(273, 158)
        Me.txtTransporte.Multiline = True
        Me.txtTransporte.Name = "txtTransporte"
        Me.txtTransporte.Size = New System.Drawing.Size(429, 23)
        Me.txtTransporte.TabIndex = 44
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.DimGray
        Me.Label8.Location = New System.Drawing.Point(199, 163)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "Transporte:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DimGray
        Me.Label1.Location = New System.Drawing.Point(268, 275)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "SubTotal"
        '
        'TextBoxImpVenta
        '
        Me.TextBoxImpVenta.BackColor = System.Drawing.Color.White
        Me.TextBoxImpVenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxImpVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxImpVenta.ForeColor = System.Drawing.Color.DimGray
        Me.TextBoxImpVenta.Location = New System.Drawing.Point(332, 293)
        Me.TextBoxImpVenta.Name = "TextBoxImpVenta"
        Me.TextBoxImpVenta.ReadOnly = True
        Me.TextBoxImpVenta.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxImpVenta.TabIndex = 2
        Me.TextBoxImpVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxSubTotal
        '
        Me.TextBoxSubTotal.BackColor = System.Drawing.Color.White
        Me.TextBoxSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSubTotal.ForeColor = System.Drawing.Color.DimGray
        Me.TextBoxSubTotal.Location = New System.Drawing.Point(332, 271)
        Me.TextBoxSubTotal.Name = "TextBoxSubTotal"
        Me.TextBoxSubTotal.ReadOnly = True
        Me.TextBoxSubTotal.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxSubTotal.TabIndex = 4
        Me.TextBoxSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxTotal
        '
        Me.TextBoxTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTotal.Location = New System.Drawing.Point(125, 362)
        Me.TextBoxTotal.Name = "TextBoxTotal"
        Me.TextBoxTotal.Size = New System.Drawing.Size(100, 29)
        Me.TextBoxTotal.TabIndex = 0
        Me.TextBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeColumns = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.DeepSkyBlue
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridView1.GridColor = System.Drawing.Color.White
        Me.DataGridView1.Location = New System.Drawing.Point(2, 3)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(430, 227)
        Me.DataGridView1.TabIndex = 6
        '
        'CheckBoxEntregado
        '
        Me.CheckBoxEntregado.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.CheckBoxEntregado.AutoSize = True
        Me.CheckBoxEntregado.Location = New System.Drawing.Point(3, 319)
        Me.CheckBoxEntregado.Name = "CheckBoxEntregado"
        Me.CheckBoxEntregado.Size = New System.Drawing.Size(82, 17)
        Me.CheckBoxEntregado.TabIndex = 9
        Me.CheckBoxEntregado.Text = "En Entrega:"
        Me.CheckBoxEntregado.UseVisualStyleBackColor = True
        Me.CheckBoxEntregado.Visible = False
        '
        'Labeltotal
        '
        Me.Labeltotal.AutoSize = True
        Me.Labeltotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Labeltotal.Location = New System.Drawing.Point(70, 368)
        Me.Labeltotal.Name = "Labeltotal"
        Me.Labeltotal.Size = New System.Drawing.Size(49, 20)
        Me.Labeltotal.TabIndex = 1
        Me.Labeltotal.Text = "Total"
        '
        'LabelTiempoPrep
        '
        Me.LabelTiempoPrep.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.LabelTiempoPrep.Location = New System.Drawing.Point(140, 297)
        Me.LabelTiempoPrep.Name = "LabelTiempoPrep"
        Me.LabelTiempoPrep.Size = New System.Drawing.Size(87, 19)
        Me.LabelTiempoPrep.TabIndex = 12
        Me.LabelTiempoPrep.Text = "Tiempo en Prep"
        Me.LabelTiempoPrep.Visible = False
        '
        'LabelTiempo
        '
        Me.LabelTiempo.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.LabelTiempo.Location = New System.Drawing.Point(82, 320)
        Me.LabelTiempo.Name = "LabelTiempo"
        Me.LabelTiempo.Size = New System.Drawing.Size(117, 18)
        Me.LabelTiempo.TabIndex = 10
        Me.LabelTiempo.Text = "Tiempo en Espera"
        Me.LabelTiempo.Visible = False
        '
        'textdescuento
        '
        Me.textdescuento.BackColor = System.Drawing.Color.White
        Me.textdescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.textdescuento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textdescuento.ForeColor = System.Drawing.Color.DimGray
        Me.textdescuento.Location = New System.Drawing.Point(332, 337)
        Me.textdescuento.Name = "textdescuento"
        Me.textdescuento.ReadOnly = True
        Me.textdescuento.Size = New System.Drawing.Size(100, 21)
        Me.textdescuento.TabIndex = 6
        Me.textdescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CheckBoxPreparando
        '
        Me.CheckBoxPreparando.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.CheckBoxPreparando.Location = New System.Drawing.Point(3, 295)
        Me.CheckBoxPreparando.Name = "CheckBoxPreparando"
        Me.CheckBoxPreparando.Size = New System.Drawing.Size(138, 18)
        Me.CheckBoxPreparando.TabIndex = 11
        Me.CheckBoxPreparando.Text = "En Preparacion desde:"
        Me.CheckBoxPreparando.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPreparando.UseVisualStyleBackColor = True
        Me.CheckBoxPreparando.Visible = False
        '
        'LabelIv
        '
        Me.LabelIv.AutoSize = True
        Me.LabelIv.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIv.ForeColor = System.Drawing.Color.DimGray
        Me.LabelIv.Location = New System.Drawing.Point(252, 296)
        Me.LabelIv.Name = "LabelIv"
        Me.LabelIv.Size = New System.Drawing.Size(74, 13)
        Me.LabelIv.TabIndex = 3
        Me.LabelIv.Text = "Imp. Ventas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DimGray
        Me.Label2.Location = New System.Drawing.Point(258, 340)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Descuento"
        '
        'textImpuestoServicio
        '
        Me.textImpuestoServicio.BackColor = System.Drawing.Color.White
        Me.textImpuestoServicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.textImpuestoServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textImpuestoServicio.ForeColor = System.Drawing.Color.DimGray
        Me.textImpuestoServicio.Location = New System.Drawing.Point(332, 315)
        Me.textImpuestoServicio.Name = "textImpuestoServicio"
        Me.textImpuestoServicio.ReadOnly = True
        Me.textImpuestoServicio.Size = New System.Drawing.Size(100, 21)
        Me.textImpuestoServicio.TabIndex = 13
        Me.textImpuestoServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DimGray
        Me.Label5.Location = New System.Drawing.Point(245, 319)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Imp. Servicio"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtTotalTransporte)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.textImpuestoServicio)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.LabelIv)
        Me.Panel1.Controls.Add(Me.CheckBoxPreparando)
        Me.Panel1.Controls.Add(Me.textdescuento)
        Me.Panel1.Controls.Add(Me.LabelTiempo)
        Me.Panel1.Controls.Add(Me.LabelTiempoPrep)
        Me.Panel1.Controls.Add(Me.Labeltotal)
        Me.Panel1.Controls.Add(Me.CheckBoxEntregado)
        Me.Panel1.Controls.Add(Me.DataGridView1)
        Me.Panel1.Controls.Add(Me.TextBoxTotal)
        Me.Panel1.Controls.Add(Me.TextBoxSubTotal)
        Me.Panel1.Controls.Add(Me.TextBoxImpVenta)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(270, 222)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(440, 389)
        Me.Panel1.TabIndex = 14
        Me.Panel1.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DimGray
        Me.Label9.Location = New System.Drawing.Point(258, 364)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Transporte"
        '
        'txtTotalTransporte
        '
        Me.txtTotalTransporte.BackColor = System.Drawing.Color.White
        Me.txtTotalTransporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalTransporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalTransporte.ForeColor = System.Drawing.Color.DimGray
        Me.txtTotalTransporte.Location = New System.Drawing.Point(332, 361)
        Me.txtTotalTransporte.Name = "txtTotalTransporte"
        Me.txtTotalTransporte.ReadOnly = True
        Me.txtTotalTransporte.Size = New System.Drawing.Size(100, 21)
        Me.txtTotalTransporte.TabIndex = 15
        Me.txtTotalTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbMarcarEntregado
        '
        Me.lbMarcarEntregado.AutoSize = True
        Me.lbMarcarEntregado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMarcarEntregado.Location = New System.Drawing.Point(500, 201)
        Me.lbMarcarEntregado.Name = "lbMarcarEntregado"
        Me.lbMarcarEntregado.Size = New System.Drawing.Size(84, 20)
        Me.lbMarcarEntregado.TabIndex = 46
        Me.lbMarcarEntregado.TabStop = True
        Me.lbMarcarEntregado.Text = "Entregado"
        Me.lbMarcarEntregado.Visible = False
        '
        'FormCuentasALlevar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(783, 657)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.txtTransporte)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtCorreo)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.LinkLabel8)
        Me.Controls.Add(Me.LinkLabel7)
        Me.Controls.Add(Me.LinkLabel2)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.TextBoxComentario)
        Me.Controls.Add(Me.CheckBoxLlevar)
        Me.Controls.Add(Me.TextBoxNombreCliente)
        Me.Controls.Add(Me.TextBoxDireccion)
        Me.Controls.Add(Me.LabelTelefono)
        Me.Controls.Add(Me.TextBoxTelefono)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBoxListado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.LabelNombre)
        Me.Controls.Add(Me.LinkLabel5)
        Me.Controls.Add(Me.LinkLabel4)
        Me.Controls.Add(Me.LinkLabel3)
        Me.Controls.Add(Me.LinkLabel6)
        Me.Controls.Add(Me.lbMarcarEntregado)
        Me.MinimumSize = New System.Drawing.Size(739, 472)
        Me.Name = "FormCuentasALlevar"
        Me.Text = "Llevar"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBoxListado.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBoxListado As System.Windows.Forms.GroupBox
	Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
	Friend WithEvents CheckBoxLlevar As System.Windows.Forms.CheckBox
	Friend WithEvents TextBoxNombreCliente As System.Windows.Forms.TextBox
	Friend WithEvents TextBoxDireccion As System.Windows.Forms.TextBox
	Friend WithEvents LabelNombre As System.Windows.Forms.Label
	Friend WithEvents LabelTelefono As System.Windows.Forms.Label
	Friend WithEvents TextBoxTelefono As System.Windows.Forms.TextBox
	Friend WithEvents TextBoxComentario As System.Windows.Forms.TextBox
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents dgCliente As System.Windows.Forms.DataGridView
	Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
	Friend WithEvents Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
	Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel
	Friend WithEvents LinkLabel5 As System.Windows.Forms.LinkLabel
	Friend WithEvents LinkLabel6 As System.Windows.Forms.LinkLabel
	Friend WithEvents LinkLabel7 As System.Windows.Forms.LinkLabel
	Friend WithEvents LinkLabel8 As System.Windows.Forms.LinkLabel
	Friend WithEvents Label6 As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents txtCedula As TextBox
	Friend WithEvents txtCorreo As TextBox
	Friend WithEvents txtTransporte As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents TextBoxImpVenta As TextBox
	Friend WithEvents TextBoxSubTotal As TextBox
	Friend WithEvents TextBoxTotal As TextBox
	Friend WithEvents DataGridView1 As DataGridView
	Friend WithEvents CheckBoxEntregado As CheckBox
	Friend WithEvents Labeltotal As Label
	Friend WithEvents LabelTiempoPrep As Label
	Friend WithEvents LabelTiempo As Label
	Friend WithEvents textdescuento As TextBox
	Friend WithEvents CheckBoxPreparando As CheckBox
	Friend WithEvents LabelIv As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents textImpuestoServicio As TextBox
	Friend WithEvents Label5 As Label
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Label9 As Label
	Friend WithEvents txtTotalTransporte As TextBox
	Friend WithEvents lbMarcarEntregado As LinkLabel
End Class
