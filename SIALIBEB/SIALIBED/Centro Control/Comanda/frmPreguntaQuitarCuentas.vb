﻿Public Class frmPreguntaQuitarCuentas

    Public QuitarTodas As Boolean = False

    Private Sub btnSeleccionadas_Click(sender As Object, e As EventArgs) Handles btnSeleccionadas.Click
        Me.QuitarTodas = False
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnTodas_Click(sender As Object, e As EventArgs) Handles btnTodas.Click
        Me.QuitarTodas = True
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

End Class