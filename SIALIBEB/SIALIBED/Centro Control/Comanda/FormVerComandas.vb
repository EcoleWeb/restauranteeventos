Public Class FormVerComandas

    Dim solicitudes As New cls_SolicitudesEnMesas()


    Private Sub FormVerComandas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.TextBox1.Text = GetSetting("SeeSoft", "Restaurante", "VigilaImpresora")
        Me.Cursor = Cursors.Default
        buscaComandas()
    End Sub
    Sub guardaImpresora()
        SaveSetting("SeeSoft", "Restaurante", "VigilaImpresora", Me.TextBox1.Text)
    End Sub

    'Buscamos las comandas activas en la vista y le damos formato..
    ' este procedimiento se carga cuando carga el formulario..
    Sub buscaComandas()

        Me.DataGridView1.DataSource = Me.solicitudes.obtenerDatos(Me.TextBox1.Text)
        If Me.DataGridView1.Rows.Count > 1 Then
            Me.DataGridView1.Columns("Id").Visible = False
            Me.DataGridView1.Columns("T.Espera").Width = 90
            Me.DataGridView1.Columns("Mesa").Width = 90
            Me.DataGridView1.Columns("Comanda").Width = 90
            Me.DataGridView1.Columns("Solicitud").Width = 90
            Me.DataGridView1.Columns("T.Espera").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            Me.DataGridView1.Columns("Solicitud").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            Me.DataGridView1.Columns("Mesa").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            
            'recorremos todas las filas del grid y verificamos cuales comandas ya han
            ' sido despachadas a los clientes.. 
            Dim r As DataGridViewRow
            For Each r In Me.DataGridView1.Rows
                If r.Cells("Entregado").Value = True Then
                    r.DefaultCellStyle.BackColor = Color.LightGreen
                End If
            Next
        End If
    End Sub

    

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.guardaImpresora()
        Me.buscaComandas()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then

                Dim comanda As String = Me.DataGridView1.Rows(e.RowIndex).Cells("Comanda").Value.ToString()
                Dim solicitud As String = Me.DataGridView1.Rows(e.RowIndex).Cells("Solicitud").Value.ToString()


                If Me.DataGridView1.Rows(e.RowIndex).Cells("Entregado").Value = True Then
                    Me.solicitudes.restablecerEstadoDespachado(comanda, solicitud)
                    Me.DataGridView1.Rows(e.RowIndex).Cells("Entregado").Value = False
                    Me.DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                Else
                    Me.DataGridView1.BeginEdit(True)
                    Me.solicitudes.actualizarEstadoDespachado(comanda, solicitud)
                    Me.DataGridView1.Rows(e.RowIndex).Cells("Entregado").Value = True
                    Me.DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGreen
                    Me.DataGridView1.EndEdit()
                End If
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If e.RowIndex >= 0 Then
            Dim u As String

            Dim comanda As String = Me.DataGridView1.Rows(e.RowIndex).Cells("Comanda").Value.ToString()
            Dim solicitud As String = Me.DataGridView1.Rows(e.RowIndex).Cells("Solicitud").Value.ToString()

            Dim dt As New DataTable

            dt = Me.solicitudes.obtenerInformacionPlatillo(comanda, solicitud)
            u = Me.solicitudes.obtenerInformacionUsuarioComanda(comanda, solicitud)

            Dim frm As New frmInfoPlatillo(dt)

            frm.Label4.Text = u
            frm.ShowDialog()
        End If
    End Sub
End Class