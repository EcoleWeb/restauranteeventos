Imports System.data.SqlClient
Imports System.Drawing.Printing
Imports System.Xml


Public Class PlatosExpress

#Region "variables"
    Public facturando As Boolean = False
    Dim bandeTipo As Boolean = False
    Dim cedSalonero As String = ""
    Public CSeparada As Boolean = False
    Public NombreUsuario As String
    Public usuario As String 'psv 26072007
    Public facturar, idMenuActivo As Integer
    Public idMesa, comenzales, HabitacionAsignada, numeroComanda, nombreMesa As String
    Public cCuentas As Integer
    Public TipoPedido As Integer = 0
    Public TipoPago As String = "CON"
    Public cod_cliente As Integer = 0
	Public nombreCliente As String = ""
	Public cod_Clienteexpress As String = ""
	Public cedulaexpress As String = ""
	Public nombreClienteexpress As String = ""
	Public telefonocliente As String = ""
	Public Direccion As String = ""
	Public Descuento As Double = 0
    Public Num_apertura As Integer = 0
    Public ParaLlevar As Integer = 0
    Dim telefono As String
	Dim conectadobd As New SqlClient.SqlConnection
	Dim cConexion As New ConexionR
	Dim cConexion1 As New ConexionR
	Dim rs As SqlClient.SqlDataReader
	Dim rs1 As SqlClient.SqlDataReader
	Dim iForzado, idGrupoT As String
	Dim tieneAcompa As String
	Dim impresoras As String = ""
	Dim prueba As Integer = 0
	Dim contador As Integer = 0
	Dim IdReceta(200) As Integer
	Dim disminuye As Double
	Dim Cargando As Boolean = True
	Dim vuelto As String = ""
	Dim FPago As String = ""
	Dim Referencia As String = ""
	Public idGrupo As String
	Public iCategoria As Integer
	Dim todo_comandado As Boolean = False
	Dim NoTerminar As Boolean = False
	Dim p_Tipo As Integer = 0
	Dim Id_Categoria As Integer = 0
	Dim current_Cant As Integer = 0
	Public numFactura As String = ""
	Dim NFactura As Int64 = 0
	Dim CCOFactura As String = ""
	Dim Transporte As Double
    Dim PMU As New PerfilModulo_Class
    Public mesa As New cls_Mesas()
    Public EsParaLlevar As Boolean = False
    Public Acumular As Integer = 0
	Dim PrimeraVez As Integer = 0
	Dim ListaColores As New ArrayList
#End Region

	Public Sub New()
		MyBase.New()

		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()

		'Agregar cualquier inicialización después de la llamada a InitializeComponent()

	End Sub

    Public Sub New(ByVal transporte, ByVal paraLLevar)
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

        Me.Transporte = Double.Parse(transporte)
        Me.EsParaLlevar = paraLLevar
        Me.txtTransporte.Text = transporte
    End Sub



    Private Sub ComandaMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'If todo_comandado = False Then
        '    If NoTerminar = False Then
        '        Terminar_Comanda()
        '    End If
        'Else
        '    ActualizaMesa()

        'End If

        '---------------------------------------------------------------------
        'jor..
        'If Not Me.facturando Then
        'Dim cx As New Conexion
        'cx.Conectar()
        'cx.SlqExecute(cx.sQlconexion, "DELETE FROM ComandaTemporal_Expres")
        'cx.DesConectar(cx.sQlconexion)
        'End If
        Me.mesa.obtenerMesa(idMesa)
        Me.mesa.cambiarNombreMesa(Me.txtNombreCliente.Text)

        cConexion.DesConectar(conectadobd)
	End Sub

	'Cargamos el Formulario..
	Private Sub ComandaMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Conexion As New ConexionR
        If GetSetting("SeeSoft", "Restaurante", "Acumular") = "1" Then
            ToolStripButtonAcumular.Visible = True
        Else
            ToolStripButtonAcumular.Visible = False
        End If
        iCategoria = 0
		txtComanda.Text = ""
		txtComanda.Clear()
		conectadobd = cConexion.Conectar("Restaurante")
		CargarColores()
		If ParaLlevar = 1 Then
            CargarMesasParaLlevar()
            pnParaLlevar.Visible = True
        End If
        Dim xContador As Integer
		If cCuentas > 0 Then
			lblCuenta.Visible = True
			cmbCuentas.Visible = True
			For xContador = 1 To cCuentas
				cmbCuentas.Items.Add(xContador)
			Next
			cmbCuentas.Text = 1
		Else
			cmbCuentas.Items.Add(0)
		End If
		Dim dt As New DataTable
		cFunciones.Llenar_Tabla_Generico("Select * from conf ", dt)
		If dt.Rows.Count > 0 Then
			p_Tipo = dt.Rows(0).Item(0)
		Else
			p_Tipo = 0

		End If
		comenzales = cConexion.SlqExecuteScalar(conectadobd, "Select numero_asientos from mesas WITH (NOLOCK) where id = " & idMesa)
		cmbCuentas.SelectedIndex = 0
		ObtenerDimension()
		CrearCategoriaMenu()
		ComandasShow()
		ToolButtonCambiarPrecio.Enabled = VSMA(usuario, Name, Secure.Others) ' PUEDE cambiar Precio
		ToolButtonReducir.Enabled = True 'VSMA(usuario, Name, Secure.Delete) 'REDUCIR
		ToolButtonEliminar.Enabled = True 'VSMA(usuario, Name, Secure.Delete) 'ELIMINAR
		PMU = VSM(usuario, Name)

		If Me.TipoPedido = 1 Then 'Facturacion Directa
			Me.ToolButtonTerminar.Visible = False
		End If
		LabelUsuario.Text = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre from Usuarios WITH (NOLOCK) where Cedula='" & usuario & "'")

		Dim DataSet As New DataSet
		cConexion.GetDataSet(cConexion.Conectar("Restaurante"), "SELECT * FROM Cuentas_Todas WITH (NOLOCK)", DataSet, "Cuentas")
		If BindingContext(DataSet, "Cuentas").Count = 0 Then
			ComboBoxHabitaciones.Visible = False
			Label7.Visible = False
		End If
		ComboBoxHabitaciones.DrawMode = DrawMode.OwnerDrawVariable
		ComboBoxHabitaciones.DisplayMember = "Habitacion"
		ComboBoxHabitaciones.DataSource = DataSet.Tables("Cuentas")

		If CBool(cConexion.SlqExecuteScalar(conectadobd, "Select utilizaGrupo from conf")) = True Then
			ToolStripButton9.Visible = True
		End If
		If Me.nombreCliente <> "CLIENTE CONTADO" And nombreCliente <> "" Then
			Me.Text = "Pedido Express - " & nombreCliente & " "
		Else
			Me.Text = "Pedido Express - CLIENTE CONTADO "
		End If
		Cargando = False
		If numeroComanda <> "" Then
			ComboBoxHabitaciones.Text = cConexion.SlqExecuteScalar(cConexion.Conectar("Restaurante"), "SELECT DISTINCT Habitacion FROM ComandaTemporal_Expres WITH (NOLOCK) WHERE cMesa = " & idMesa & " AND cCodigo = " & numeroComanda)
			comenzales = cConexion.SlqExecuteScalar(conectadobd, "Select DISTINCT comenzales from ComandaTemporal_Expres WITH (NOLOCK) WHERE cMesa = " & idMesa & " AND cCodigo = " & numeroComanda)
			ComboBoxComensal.Text = comenzales
		End If

		If GetSetting("Seesoft", "Restaurante", "1/2") = 1 Then
			ToolStripButton12.Visible = True
		Else
			ToolStripButton12.Visible = False
		End If
        PrimeraVez = 1
    End Sub

    Sub CargarMesasParaLlevar()
        Try
            Dim dtMesas As New DataTable
            Dim IdMesaParallevar As Integer = 0
            Dim dt As New DataTable
            Dim Seleccionar As Integer = 0

            cFunciones.Llenar_Tabla_Generico("Select id from Grupos_Mesas where ParaLlevar=1", dtMesas)

            If dtMesas.Rows.Count > 0 Then
                IdMesaParallevar = dtMesas.Rows(0).Item("id")

                dtMesas.Clear()
                cFunciones.Llenar_Tabla_Generico("Select Id,Nombre_Mesa from Mesas where Id_GrupoMesa=" & IdMesaParallevar & " order by Id asc", dtMesas)
                cmbMesas.DataSource = dtMesas
                cmbMesas.DisplayMember = "Nombre_Mesa"
                cmbMesas.ValueMember = "Id"
Again:
                cmbMesas.SelectedIndex = Seleccionar

                cFunciones.Llenar_Tabla_Generico("Select activa from Mesas where Mesas.Id=" & Convert.ToInt32(cmbMesas.SelectedValue) & "", dt)
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0).Item("Activa") = 1 Then
                        Seleccionar = Seleccionar + 1
                        GoTo Again
                    End If
                End If
                idMesa = cmbMesas.SelectedValue
            End If

        Catch ex As Exception
            MsgBox("Error al cargar mesas para llevar.", MsgBoxStyle.Information)
        End Try
    End Sub

	Private Sub CrearCategoriaMenu()

		Dim ancho As Double
		Dim largo As Double
		ancho = Double.Parse(GetSetting("SeeSoft", "Restaurante", "anchoComanda"))
		largo = Double.Parse(GetSetting("SeeSoft", "Restaurante", "largoComanda"))
		Dim aButton As Button
		Dim i, ii, X, Y, n As Integer
		PanelMesas.Visible = True
		grpModifica.Visible = False
		PanelMesas.Controls.Clear()
		For i = 1 To txtFilas.Text '14 'ORIGINAL 7
			For ii = 1 To TxtColumnas.Text '4
				aButton = New Button
				aButton.Top = Y
				aButton.Left = X
				aButton.Width = largo
				aButton.Height = ancho
				aButton.Name = n
				aButton.Visible = False
				aButton.Enabled = False
				AddHandler aButton.Click, AddressOf Clickbuttons
				Me.PanelMesas.Controls.Add(aButton)
				X = X + (largo + 2)
				n = n + 1
			Next
			Y = Y + (ancho + 2)
			X = 0
		Next


		If idGrupo = "0" Then
			UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo = 'CATMENU',Precio_VentaExpressA=0 FROM Categorias_Menu WITH (NOLOCK) Order By posicion")
		Else
			UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo = 'CATMENU',Precio_VentaExpressA=0 FROM Categorias_Menu WITH (NOLOCK) where idGrupo = " & idGrupo & "Order By posicion")
		End If

		PanelMesas.Visible = True
	End Sub

	Private Sub UbicarPosiciones(ByVal consulta As String)
		Dim ruta As String
		Dim botones As Button
		Dim valida As Boolean = False
		Dim ponePIC As String = GetSetting("SEESOFT", "Restaurante", "sinPIC")
        Dim o As Integer = 0
        cConexion.GetRecorset(conectadobd, consulta, rs)

		While rs.Read
			botones = New Button
            botones = PanelMesas.Controls.Item(o) ' rs(2) posicion del control
            botones.Text = rs(1) ' Nombre
			botones.Name = botones.Name & "-" & rs(0) & "-" & rs("Tipo") & "-" & rs("Precio_VentaExpressA") 'Codigo

			botones.Enabled = True

			ruta = rs(3)
			If ruta.Trim.Length > 0 Then
				Try
					If Not ponePIC.Equals("1") Then
						If Not ruta.Equals("") Then
							Dim SourceImage As Bitmap
							SourceImage = New Bitmap(ruta)
							'botones.Image = New Drawing.Bitmap(SourceImage, 100, 60) ' 'SourceImage
							botones.Image = SourceImage
						End If

					End If

				Catch ex As Exception
					valida = True
				End Try
			End If
			botones.TextImageRelation = TextImageRelation.ImageAboveText    '.TextAboveImage
            botones.Visible = True
            o = o + 1
        End While

		rs.Close()

	End Sub

	Public Sub limpiaDatos()
		Dim i As Integer
		Dim botones As Button

		For i = 0 To ((txtFilas.Text * TxtColumnas.Text) - 1)
			botones = New Button
			botones = PanelMesas.Controls.Item(i)
			botones.Text = ""
			botones.Name = ""
			botones.Image = Nothing
			botones.Visible = False
			botones.Enabled = False
		Next

	End Sub

	Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Me.comenzales = "0"
            Dim arrayNombre As Array
            Dim tieneForzado As String
            arrayNombre = sender.name.ToString.Split("-")

            If Trim(iForzado) <> vbNullString Then
                arrayNombre(2) = iForzado
            End If
            ObtenerDimension()
            Select Case arrayNombre(2)

                Case "CATMENU" 'SELECCIONA UNA CATEGORIA DEL MENÚ, CARGANDO LOS PLATOS O DATOS DE ESA CATEGORIA
                    ObtenerDimensionCategoria(arrayNombre(1))
                    CrearCategoriaMenu()
                    limpiaDatos()

                    idGrupoT = arrayNombre(1)
                    UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA, Impresora FROM Menu_Restaurante WITH (NOLOCK) where deshabilitado = 0 and Id_Categoria =" & arrayNombre(1) & " ORDER BY Posicion")
                    impresoras = cConexion.SlqExecuteScalar(conectadobd, "Select Impresora from Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & arrayNombre(1))
                    lblEncabezado.Text = "Menú"

                Case "MENUREST"

                    prueba += 1
                    tieneForzado = cConexion.SlqExecuteScalar(conectadobd, "Select Modificador_Forzado from Menu_Restaurante WITH (NOLOCK) where id_menu=" & arrayNombre(1))
                    Dim pedirPrecio As Boolean = cConexion.SlqExecuteScalar(conectadobd, "Select SinPrecio from Menu_Restaurante WITH (NOLOCK) where id_menu=" & arrayNombre(1))
                    Dim pedirPrecioImp As Boolean = cConexion.SlqExecuteScalar(conectadobd, "Select SinPrecioImp from Menu_Restaurante WITH (NOLOCK) where id_menu=" & arrayNombre(1))


                    idMenuActivo = arrayNombre(1)
                    If Trim(numeroComanda) = vbNullString Then
                        numeroComanda = 0
                    End If
                    impresoras = cConexion.SlqExecuteScalar(conectadobd, "Select Impresora from Menu_Restaurante WITH (NOLOCK) where Id_MENU=" & arrayNombre(1))

                    Id_Categoria = cConexion.SlqExecuteScalar(conectadobd, "Select Id_Categoria from Menu_Restaurante WITH (NOLOCK) where Id_MENU=" & arrayNombre(1))
                    If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                    rs.Close()
                    Dim iPrimero As Integer
                    Dim PedidoExpress As Boolean = False
                    iPrimero = 0
                    PedidoExpress = IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0)

                    lblCantidad.Text = "1"
                    If tlsCantidad.BackColor = Color.Yellow Then
                        Dim canPedido As Double
                        Dim rEmpleado0 As New registro
                        rEmpleado0.Text = "Cantidad de Pedidos"
                        rEmpleado0.ShowDialog()
                        Try
                            canPedido = CDbl(rEmpleado0.txtCodigo.Text)
                        Catch ex As Exception
                            canPedido = 1
                        End Try
                        rEmpleado0.Dispose()
                        lblCantidad.Text = canPedido
                    End If
                    current_Cant = lblCantidad.Text
                    Dim valorCambio, dmoneda As Double
                    dmoneda = cConexion.SlqExecuteScalar(conectadobd, "select moneda from menu_restaurante where id_Menu =" & arrayNombre(1))
                    valorCambio = cConexion.SlqExecuteScalar(conectadobd, "select  (select valorVenta from moneda where codMoneda=" & dmoneda & ") / (select m.valorVenta from moneda as m, hotel.dbo.configuraciones as hc where m.codMoneda = hc.monedaRestaurante)")

                    Dim precio As Double = arrayNombre(3)
                    If precio = 0 Then
                        If pedirPrecio Then
                            If VSMA(usuario, Name, Secure.Others) Then
                                Dim solicitoPrecio As New registro
                                solicitoPrecio.Text = "Digite el precio..."
                                solicitoPrecio.ShowDialog()
                                Try
                                    precio = (CDbl(solicitoPrecio.txtCodigo.Text))
                                Catch ex As Exception
                                    precio = 0
                                End Try
                                solicitoPrecio.Dispose()
                            End If
                        End If
                        If pedirPrecioImp Then
                            If VSMA(usuario, Name, Secure.Others) Then
                                Dim solicitoPrecio As New registro
                                solicitoPrecio.Text = "Digite el precio..."
                                solicitoPrecio.ShowDialog()
                                Try
                                    precio = (CDbl(solicitoPrecio.txtCodigo.Text) / (1 + (13 / 100)))
                                Catch ex As Exception
                                    precio = 0
                                End Try
                                solicitoPrecio.Dispose()
                            End If

                        End If
                    End If

                    Dim nombremenu As String = ""
                    If Me.BANDERA_MEDIO = True Then
						precio = cConexion.SlqExecuteScalar(conectadobd, "Select Precio_VentaExpressM from Menu_Restaurante WITH (NOLOCK) where id_menu=" & arrayNombre(1))
						nombremenu = "1/2 " & sender.text
                    Else
                        nombremenu = sender.text
                    End If

                    AlmacenaTemporal(arrayNombre(1), nombremenu, precio * valorCambio, impresoras, comenzales, usuario, HabitacionAsignada, iPrimero, PedidoExpress)

                    todo_comandado = False
                    Dim act As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select activa from Mesas WITH (NOLOCK) where Id=" & idMesa)

                    If act = 0 Or act = 1 Then
                        cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "id=" & idMesa)
                    End If

                    If p_Tipo = 1 Then 'Si especifica modificadores y acompañamientos
                        If tieneForzado = "True" Then
                            limpiaDatos()
                            Bloquear(True)

                            Me.bandeTipo = True
                            Me.ControlBox = True

                            Me.ToolButtonModificarComanda.Visible = False
                            Me.ToolButtonRegresar.Visible = False
                            Me.ToolStripButton9.Visible = False
                            Me.ToolStripButton8.Visible = False
                            Me.tlsCantidad.Visible = False
                            Me.ToolButtonNotas.Visible = False
                            Me.ToolButtonExpress.Visible = False
                            ToolButtonFacturar.Visible = False
                            ToolStripButtonAcumular.Visible = False
                            ToolStripButtonTerminar.Enabled = True
                            ToolButtonTerminar.Visible = True


                            Dim dt As New DataTable
							cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaExpressA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dt)
							If dt.Rows.Count > 0 Then
								UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaExpressA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
								dt.Rows.Clear()
                            Else
								UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 as Precio_VentaExpressA FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
							End If
                            lblEncabezado.Text = "Modificadores*"
                            ToolStripButtonTerminar.Visible = True
                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)

                        End If

                    Else

                        If tieneForzado = "True" Then
                            limpiaDatos()
                            Bloquear(False)
							'limpiaDatos()
							'Bloquear(True)

							'Me.bandeTipo = True
							'Me.ControlBox = True

							'Me.ToolButtonModificarComanda.Visible = False
							'Me.ToolButtonRegresar.Visible = False
							'Me.ToolStripButton9.Visible = False
							'Me.ToolStripButton6.Visible = False
							'Me.ToolStripButton8.Visible = False
							'Me.tlsCantidad.Visible = False
							'Me.ToolButtonNotas.Visible = False
							'Me.ToolButtonExpress.Visible = False
							'Me.ToolButtonPreFacturar.Visible = False
							'Me.ToolButtonFacturar.Visible = False
							'Me.ToolButtonSepararCuenta.Visible = False
							'Me.ToolStripButtonTerminar.Enabled = True

							UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 as Precio_VentaExpressA FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
							lblEncabezado.Text = "Modificadores"


                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)
                        End If

                    End If

                Case "FORZADOS"

                    If Trim(iForzado) = vbNullString Then

                        Dim iPrimero As Integer
                        iPrimero = 0

                        If arrayNombre(3) > 0 Then
                            cConexion.AddNewRecord(conectadobd, "ComandaTemporal_Expres", "cProducto,cPrecio,cCantidad,cDescripcion,cCodigo,Cmesa, Impreso, Impresora, Comenzales, Tipo_Plato,Cedula,primero,separada, Express", arrayNombre(1) & "," & arrayNombre(3) & ",-1,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Modificador'" & ",'" & usuario & "'," & iPrimero & "," & cmbCuentas.Text & "," & IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0))
                        Else
                            cConexion.AddNewRecord(conectadobd, "ComandaTemporal_Expres", "cProducto,cPrecio,cCantidad,cDescripcion,cCodigo,Cmesa, Impreso, Impresora, Comenzales, Tipo_Plato,Cedula,primero,separada, Express", arrayNombre(1) & "," & arrayNombre(3) & ",0,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Modificador'" & ",'" & usuario & "'," & iPrimero & "," & cmbCuentas.Text & "," & IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0))
                        End If

                        If p_Tipo = 1 Then

                            limpiaDatos()
                            Bloquear(True)

                            Me.bandeTipo = True
                            Me.ControlBox = True

                            Me.ToolButtonModificarComanda.Visible = False
                            Me.ToolButtonRegresar.Visible = False
                            Me.ToolStripButton9.Visible = False
                            Me.ToolStripButton8.Visible = False
                            Me.tlsCantidad.Visible = False
                            Me.ToolButtonNotas.Visible = False
                            Me.ToolButtonExpress.Visible = False
                            Me.ToolButtonFacturar.Visible = False
                            Me.ToolStripButtonAcumular.Visible = False
                            Me.ToolStripButtonTerminar.Enabled = True
                            Me.ToolButtonTerminar.Visible = True

                            Dim dt As New DataTable
							cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaExpressA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dt)
							If dt.Rows.Count > 0 Then
								UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaExpressA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
								dt.Rows.Clear()
                            Else
								UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 as Precio_VentaExpressA FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
							End If
                            lblEncabezado.Text = "Modificadores*"
                        Else
                            iForzado = "FORZADOS"
                            Clickbuttons(sender, e)
                        End If
                    Else
                        iForzado = ""
                    End If
                    If (Not cConexion.SlqExecuteScalar(conectadobd, "Select Modificador_Forzado from Menu_Restaurante WITH (NOLOCK) where id_menu=" & idMenuActivo) = "True") Or p_Tipo = 0 Then
                        limpiaDatos()
                        tieneAcompa = cConexion.SlqExecuteScalar(conectadobd, "Select Numero_Acompañamientos from Menu_Restaurante WITH (NOLOCK) where id_menu=" & idMenuActivo)

                        If CDbl(tieneAcompa) > 0 Then limpiaDatos()
                        If CDbl(tieneAcompa) > 0 Then
                            lblEncabezado.Text = "Acompañamientos"

                            If p_Tipo = 1 Then

                                Me.bandeTipo = True
                                Bloquear(True)

                                Me.ToolButtonModificarComanda.Visible = False
                                Me.ToolButtonRegresar.Visible = False
                                Me.ToolStripButton9.Visible = False
                                Me.ToolStripButton8.Visible = False
                                Me.tlsCantidad.Visible = False
                                Me.ToolButtonNotas.Visible = False
                                Me.ToolButtonExpress.Visible = False
                                Me.ToolButtonFacturar.Visible = False
                                ToolStripButtonAcumular.Visible = False
                                Me.ToolStripButtonTerminar.Enabled = True
                                ToolButtonTerminar.Visible = True

                                Dim dt As New DataTable
                                cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio as Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion", dt)
                                If dt.Rows.Count > 0 Then
                                    UbicarPosiciones("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio AS Precio_VentaA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion")
                                Else
									UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 as Precio_VentaExpressA FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
								End If

                            Else
								UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 as Precio_VentaExpressA FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
								Bloquear(False)
                                bandeTipo = False
                            End If



                        Else
							UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA FROM Menu_Restaurante WITH (NOLOCK) where  deshabilitado = 0 and Id_Categoria =" & idGrupoT & " ORDER BY Posicion")
							lblEncabezado.Text = "Menú"
                            Bloquear(True)
                            bandeTipo = False
                        End If

                    End If



                Case "ACOMPA"

                    Dim iPrimero As Integer
                    iPrimero = 0
                    cConexion.AddNewRecord(conectadobd, "ComandaTemporal_Expres", "cProducto,cCantidad,cDescripcion,cCodigo,cMEsa, Impreso, Impresora, Comenzales, Tipo_Plato,cedula,primero,separada,Express", arrayNombre(1) & ",0,'" & sender.text & "'," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & ",'Acompañamiento'" & ",'" & usuario & "'," & iPrimero & "," & cmbCuentas.Text & "," & IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0))
                    If tieneAcompa = 1 Then 'Si especifica modificadores y acompañamientos
                        limpiaDatos()
                        idMenuActivo = 0
						UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA FROM Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & idGrupoT & " ORDER BY Posicion")
						lblEncabezado.Text = "Menú"
                        Bloquear(True)

                        Me.bandeTipo = False
                        Me.ControlBox = True

                        Me.ToolButtonModificarComanda.Visible = True
                        Me.ToolButtonRegresar.Visible = True
                        Me.ToolStripButton9.Visible = usaGrupo
                        Me.ToolStripButton8.Visible = True
                        Me.tlsCantidad.Visible = True
                        Me.ToolButtonNotas.Visible = True
                        Me.ToolButtonExpress.Visible = True
                        Me.ToolButtonFacturar.Visible = True
                        ' Me.ToolStripButtonAcumular.Visible = True
                        Me.ToolStripButtonTerminar.Enabled = True
                        ToolButtonTerminar.Visible = True
                    Else
                        tieneAcompa = tieneAcompa - 1

                    End If

            End Select
            ToolStripButtonTerminar.Visible = True
            ComandasShow()
            arrayNombre = Nothing
            PanelMesas.Focus()
        Catch ex As Exception
            MessageBox.Show("Error:" & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
		End Try
	End Sub

	Private Sub UbicarPosicionesAgregados()
		Dim er As Boolean = False
		Dim txt As String = "SELECT ACO_AGREGADO.IdCategoriaAgregados As Id,TipoAgregado as Tipo,0 as Precio_VentaExpressA,ACO_AGREGADO.IdAgregado, Acompañamientos_Menu.Nombre, ACO_AGREGADO.posicion, Acompañamientos_Menu.ImagenRuta " &
						" FROM Acompañamientos_Menu INNER JOIN" &
					  " ACO_AGREGADO ON Acompañamientos_Menu.Id = ACO_AGREGADO.IdAgregado WHERE (ACO_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")"
		UbicarPosiciones(txt)

		txt = "SELECT MOD_AGREGADO.IdCategoriaAgregados As Id, TipoAgregado as Tipo, 0 as Precio_VentaExpressA, Modificadores_Forzados.Nombre, MOD_AGREGADO.posicion, Modificadores_Forzados.ImagenRuta, MOD_AGREGADO.IdAgregado, MOD_AGREGADO.TipoAgregado " &
							" FROM MOD_AGREGADO INNER JOIN " &
					  " Modificadores_Forzados ON MOD_AGREGADO.IdAgregado = Modificadores_Forzados.ID WHERE (MOD_AGREGADO.IdCategoriaMenu = " & Me.Id_Categoria & ")"
		UbicarPosiciones(txt)
		If er = True Then
			MsgBox("Algunas imagenes fueron movidas de su ubicación original")
		End If
	End Sub

	Private Sub AlmacenaTemporal(ByVal cProducto As Integer, ByVal cDescripcion As String, ByVal cPrecio As Double, ByVal Impresora As String, ByVal cont As Integer, ByVal cedula As String, ByVal Hab As String, ByVal primero As Integer, ByVal Express As Boolean)
		Dim prec As Double = Format(cPrecio, "###,##0.0000")
		If Hab = Nothing Then Hab = ""
		Dim maxComanda, maxComandaTemp As Double
		Dim idultimoingresado As Integer

		Dim iPrimero As Integer = 0
		Dim cx As New Conexion
		cx.Conectar()

		'If Trim(numeroComanda) = vbNullString Or numeroComanda = 0 Then
		If Trim(numeroComanda) = vbNullString Or numeroComanda = "0" Then
			numeroComanda = 0
			Dim dt As New DataTable
			cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(NumeroComanda), 0) AS NumComanda FROM ConsecutivoComandas", dt)
			If dt.Rows.Count > 0 Then
				numeroComanda = dt.Rows(0).Item("NumComanda") + 1

			End If
			cx.SlqExecute(cx.sQlconexion, "INSERT INTO ConsecutivoComandas (Fecha,NumeroComanda)  VALUES (GETDATE(), " & numeroComanda & ")")

			maxComanda = CDbl(cConexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(MAX(NumeroComanda),0) FROM Comanda WITH (NOLOCK)"))
			maxComandaTemp = cConexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(max(cCodigo),0) FROM ComandaTemporal_Expres WITH (NOLOCK)")

			If maxComanda >= maxComandaTemp Then
				numeroComanda = maxComanda + 1
			Else
				numeroComanda = maxComandaTemp + 1
			End If

		End If

		If lblCantidad.Text = "0" Then Me.lblCantidad.Text = "1"

		cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal_Expres " & cProducto & ",'" & cDescripcion & "'," & CDbl(prec) & "," & numeroComanda & "," & idMesa & ",0,'" & Impresora & "'," & cont & ",'Principal','" & cedula & "','" & Hab & "'," & primero & "," & cmbCuentas.Text & "," & IIf(Express, 1, 0) & ", " & Me.lblCantidad.Text)
		'cConexion.AddNewRecord(conectadobd, "ComandaTemporal_Expres", "cProducto, cCantidad,cDescripcion,cCodigo,Cmesa, Impreso, Impresora, Comenzales, Tipo_Plato,Cedula,primero,separada", cProducto & "," & lblCantidad.Text & ",'" & cDescripcion & "'," & numeroComanda & "," & idMesa & ",0,'" & Impresora & "'," & comenzales & ",'Modificador'" & ",'" & usuario & "'," & iPrimero & ",0")
		idultimoingresado = cConexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(max(id),0) FROM ComandaTemporal_Expres WITH (NOLOCK)")

		cConexion.SlqExecute(conectadobd, "update ComandaTemporal_Expres set cCantidad=" & lblCantidad.Text & " where id=" & idultimoingresado)
		txtComanda.Clear()
		lblCantidad.Text = "1"
	End Sub

	Private Sub CargaPanel(ByVal consulta As String, ByVal encabezado As String)
		limpiaDatos()
		UbicarPosiciones(consulta)
		lblEncabezado.Text = encabezado
	End Sub

	Private Sub ComandasShow()

		cargarDescuento()
		Dim leyo As Boolean = False
		Dim precio, cantidad, subTotal As Double
		Dim descripcion As String
		Dim subTotalTodo As Double
		Dim SelColor As Boolean = False
		Dim Sel As String = Nothing
		Dim ii, PosArray As Integer
		Dim arrayStyle As New ArrayList
		Dim arrayImp As New ArrayList
		Dim aGeneraStyle As Array
		Dim bandera As Boolean = True
		Dim bandera1 As Boolean = True
		Dim hora As String = ""
		Dim Linea As String = ""
		Dim IVentas As Double
		Dim IServicios As Double = 0
		Dim BanderaExpressIni As Boolean = True
		Dim BanderaExpressFin As Boolean = True


		Me.ComboBoxComensal.Text = comenzales
		Me.TxtNumeroComanda.Text = numeroComanda
		Me.LabelMesa.Text = "Mesa:  " & nombreMesa
		txtComanda.Text = ""

		If numeroComanda = vbNullString Then Exit Sub ' si no se ha creado una comanda todavia.

		Dim Conexion As New Conexion
		Dim Registros As SqlClient.SqlDataReader
		Registros = Conexion.GetRecorset(conectadobd, "select * from DatosExpress where id_comanda = " & numeroComanda)
		If Registros.Read() Then

			txtComanda.Text = txtComanda.Text & "***** DATOS DE PEDIDO EXPRESS **********" & vbCrLf
			txtComanda.Text = txtComanda.Text & "*Cliente  :" & Registros("nombre") & "" & vbCrLf
			txtComanda.Text = txtComanda.Text & "*Teléfono :" & Registros("Telefono") & "" & vbCrLf
			txtComanda.Text = txtComanda.Text & "*Dirección:" & Registros("Direccion") & "" & vbCrLf

		End If
		Registros.Close()
		Conexion.DesConectar(Conexion.sQlconexion)
		txtComanda.Text = txtComanda.Text & "****************************************" & vbCrLf
		txtComanda.Text = txtComanda.Text & "*          Detalle de la Orden         *" & vbCrLf
		txtComanda.Text = txtComanda.Text & "****************************************" & vbCrLf
		txtComanda.Text = txtComanda.Text & "* Cant.    Precio/U     Descripción    *" & vbCrLf
		Try
			If Registros.IsClosed Then

			End If
            If numeroComanda = "" Then Exit Sub
            Registros.Close()
			cConexion.GetRecorset(conectadobd, "SELECT ComandaTemporal_Expres.cCantidad,Impreso, ComandaTemporal_Expres.cProducto, ComandaTemporal_Expres.cDescripcion, ComandaTemporal_Expres.cPrecio, ComandaTemporal_Expres.cCodigo,  ComandaTemporal_Expres.primero, ComandaTemporal_Expres.Cedula, ComandaTemporal_Expres.hora, ComandaTemporal_Expres.Express, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ABS(ComandaTemporal_Expres.cCantidad) * ComandaTemporal_Expres.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ABS(ComandaTemporal_Expres.cCantidad) * ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ABS(ComandaTemporal_Expres.cCantidad) * ComandaTemporal_Expres.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) ELSE ABS(ComandaTemporal_Expres.cCantidad) * ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) END) AS IServicios FROM ComandaTemporal_Expres WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cCodigo=" & numeroComanda & " and separada= " & cmbCuentas.Text & " ORDER BY primero DESC,express ASC", Registros)
			Dim todo_impreso As Boolean = True
			While Registros.Read
				leyo = True
				precio = Registros("cPrecio")

                cantidad = Registros("cCantidad")
                descripcion = Trim(Registros("cDescripcion")).ToUpper
                subTotal = Math.Abs(Registros("cCantidad")) * Registros("cPrecio")
                SelColor = Registros("Impreso")

				If Registros("Primero") = 1 And bandera = True Then
					txtComanda.Text = txtComanda.Text & "******* PREPARAR DE PRIMERO ************" & vbCrLf
					bandera = False
				End If

				If Registros("Primero") = 0 And bandera1 = True Then
					txtComanda.Text = txtComanda.Text & "****************************************" & vbCrLf
					bandera1 = False
				End If

				If Registros("Express") = True And BanderaExpressIni = True Then
					txtComanda.Text = txtComanda.Text & vbCrLf & "::::::::> PREPARAR PARA EXPRESS <:::::::" & vbCrLf
					BanderaExpressIni = False
				End If
				Linea = ""
				If cantidad = 0 Or cantidad = -1 Then
                    If precio = "0" Then
                        If descripcion.Length < (40 - Linea.Length) Then
                            Linea = Space(15) & descripcion
                        Else
                            Linea = Space(15) & descripcion.Substring(0, (25))
                        End If
                        txtComanda.Text = txtComanda.Text & Linea & vbCrLf
                        arrayStyle.Add(Linea)
                    Else
                        Dim desc As String = descripcion & " " & precio
                        If desc.Length < (40 - Linea.Length) Then
                            Linea = Space(15) & desc
                        Else
                            Linea = Space(15) & desc.Substring(0, (25))
                        End If
                        txtComanda.Text = txtComanda.Text & Linea & vbCrLf
                        arrayStyle.Add(Linea)
                    End If
                    If precio <= 0 Then
                        subTotal += precio
                    End If
                Else
					If cantidad.ToString.Length <= 4 Then
						If (SelColor = True) Then
							Sel = " Imp"
						Else
							todo_impreso = False
						End If
						Linea = Space(4 - cantidad.ToString.Length) & cantidad.ToString & Sel
						Sel = Nothing
					Else
						Linea = cantidad.ToString
					End If
					If precio.ToString.Length <= 9 Then
						Linea = Linea & " " & Space(9 - precio.ToString.Length) & precio.ToString & " "
					Else
						Linea = Linea & " " & precio.ToString & " "
					End If
					If descripcion.Length < (40 - Linea.Length) Then
						Linea = Linea & descripcion
					Else
						Linea = Linea & descripcion.Substring(0, (40 - Linea.Length))
					End If
					txtComanda.Text = txtComanda.Text & Linea & vbCrLf
				End If
				Try
					' cedSalonero = Registros("cedula")
					hora = Registros("hora")
				Catch ex As Exception
				End Try
				subTotalTodo += subTotal
				If precio = "0" Then
					IVentas += Registros("IVentas")
					If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress", "0").Equals("0") Then
						IServicios += 0 'Registros("IServicios")
					Else
						IServicios += Registros("IServicios")
					End If

				Else
					IVentas += Registros("IVentas")
					If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress", "0").Equals("0") Then
						IServicios += 0 'Registros("IServicios")
					Else
						IServicios += Registros("IServicios")
					End If
				End If
			End While
			If todo_impreso = False Then
				todo_comandado = False
			Else
				todo_comandado = True
			End If
			Registros.Close()

            For ii = 0 To arrayStyle.Count - 1
                txtComanda.SelectionStart = txtComanda.Find(arrayStyle(ii))
D:
                If txtComanda.SelectionBackColor = Color.LightYellow Then
                    PosArray = txtComanda.SelectionStart + txtComanda.SelectionLength
                    txtComanda.DeselectAll()
                    txtComanda.SelectionStart = txtComanda.Find(arrayStyle(ii), PosArray, RichTextBoxFinds.None)
                    GoTo D
                End If
                txtComanda.SelectionFont = New System.Drawing.Font("Verdana", 7, CType((System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                txtComanda.SelectionBackColor = Color.LightYellow
                txtComanda.DeselectAll()
            Next

            If Not leyo Then
				If cmbCuentas.Visible = False Or cmbCuentas.Items.Count = 1 Then
					Dim dt As New DataTable
					cFunciones.Llenar_Tabla_Generico("SELECT ComandaTemporal_Expres.cCantidad,Impreso, ComandaTemporal_Expres.cProducto, ComandaTemporal_Expres.cDescripcion, ComandaTemporal_Expres.cPrecio, ComandaTemporal_Expres.cCodigo,  ComandaTemporal_Expres.primero, ComandaTemporal_Expres.Cedula, ComandaTemporal_Expres.hora, ComandaTemporal_Expres.Express, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ComandaTemporal_Expres.cCantidad * ComandaTemporal_Expres.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandaTemporal_Expres.cCantidad * ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ComandaTemporal_Expres.cCantidad * ComandaTemporal_Expres.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) ELSE ComandaTemporal_Expres.cCantidad * ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) END) AS IServicios FROM ComandaTemporal_Expres WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cCodigo=" & numeroComanda & "  ORDER BY primero DESC,express ASC", dt)
					If dt.Rows.Count > 0 Then
						Dim cx As New Conexion
						cx.SlqExecute(cx.Conectar, "UPDATE ComandaTemporal_Expres Set separada = 1 WHERE cMesa = " & idMesa)
						cx.DesConectar(cx.sQlconexion)
						ComandasShow()
					End If
				End If
			End If

			aGeneraStyle = Nothing
			arrayStyle = Nothing

			Dim totalDescuento As Double = 0
			Dim totaDescIServ As Double = 0
			Dim totaDescIVenta As Double = 0
			If Descuento > 0 Then
				totalDescuento = subTotalTodo * (Descuento / 100)
				totaDescIServ = IServicios * (Descuento / 100)
				totaDescIVenta = IVentas * (Descuento / 100)
			End If
			txtSubTotal.Text = Format(subTotalTodo, "###,##0.00")
			txtDescuento.Text = Format(totalDescuento, "###,##0.00")
			'    Me.txtTransporte.Text = Format(Me.txtTransporte.Text, "###,##0.00")

			If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress", "0").Equals("0") Then
				txtIServicio.Text = 0 'Format(IServicios - totaDescIServ, "###,##0.00")
			Else
				txtIServicio.Text = Format(IServicios - totaDescIServ, "###,##0.00")
			End If
			txtIVentas.Text = Format(IVentas - totaDescIVenta, "###,##0.00") 'Cambiar por los datos de configuracion
			txtTotal.Text = Format((subTotalTodo - totalDescuento) + IIf(IsNumeric(txtTransporte.Text), Me.txtTransporte.Text, 0) + txtIServicio.Text + txtIVentas.Text, "###,##0.00")
			Try
				lblSalonero.Text = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from usuarios where cedula ='" & cedSalonero & "'") & " - " & hora
			Catch ex As Exception
			End Try

		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
		End Try
	End Sub

#Region "Descuento"
	Sub cargarDescuento()
		If Not Me.numeroComanda > 0 Then
			Exit Sub
		End If
		Dim dt As New DataTable
		cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(descuento),0) AS DES, Nombre AS NOM, cCodigo, separada " &
											" FROM ComandaTemporal_Expres " &
											" GROUP BY cCodigo, separada, Nombre " &
											" HAVING (cCodigo = " & Me.numeroComanda & ") AND (separada = 0 OR separada = " & Me.cmbCuentas.Text & ")", dt)
		If dt.Rows.Count > 0 Then

			'Me.NombreCliente = dt.Rows(0).Item("NOM")
			Me.Descuento = dt.Rows(0).Item("DES")
			If Me.Descuento > 0 Then
				Me.ToolStripButtonAplicarDescuento.BackColor = Color.Yellow
				Me.ToolStripButtonAplicarDescuento.Text = "Desc(" & Me.Descuento & ")"
			Else
				Me.ToolStripButtonAplicarDescuento.BackColor = Color.Transparent
				Me.ToolStripButtonAplicarDescuento.Text = "Descuento"

			End If

			'If Not Me.NombreCliente.Equals("") Then
			'    Me.ToolStripButtonNombre.BackColor = Color.Yellow

			'Else
			'    Me.ToolStripButtonNombre.BackColor = Color.Transparent

			'End If
		Else
			ToolStripButtonAplicarDescuento.BackColor = Color.Transparent
			''  ToolStripButtonNombre.BackColor = Color.Transparent
			Me.ToolStripButtonAplicarDescuento.Text = "Descuento"

		End If
	End Sub
#End Region

	Private Sub RefrescaDatos()
		Dim ConexionX As New ConexionR
		Dim ReaderSource As SqlDataReader
		lstProductos.View = View.Details
		lstProductos.FullRowSelect = True
		lstProductos.LabelEdit = False
		lstProductos.Items.Clear()
		ConexionX.GetRecorset(ConexionX.Conectar(), "Select id,cProducto, cCantidad, cDescripcion, cPrecio, cCodigo from ComandaTemporal_Expres WITH (NOLOCK) where cCodigo=" & numeroComanda & " and separada= " & cmbCuentas.Text & " AND cMesa = " & idMesa & " order by primero", ReaderSource)
		Try
			While ReaderSource.Read
				With lstProductos.Items.Add(ReaderSource("id"))
					.SubItems.Add(ReaderSource("cCantidad"))
					.SubItems.Add(ReaderSource("cDescripcion"))
					.SubItems.Add(ReaderSource("cPrecio"))
					.SubItems.Add(ReaderSource("cCodigo"))
					.SubItems.Add(ReaderSource("cProducto"))
				End With
			End While
			ReaderSource.Close()
			ConexionX.DesConectar(ConexionX.SqlConexion)
			grpModifica.Visible = True
		Catch ex As Exception

		End Try
	End Sub

	Private Function Verificar_Impresora_Instalada(ByVal Impresora As String, ByVal NumeroComanda As Double) As String
		Dim PrinterInstalled As String
		Dim existe As Boolean = False
		Dim arrayImpresora1 As Array

		Try
			For Each PrinterInstalled In PrinterSettings.InstalledPrinters
				arrayImpresora1 = PrinterInstalled.ToUpper.ToString.Split("\")

				If Trim(Impresora.ToUpper) = Trim(PrinterInstalled.ToUpper) Then
					cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
					Return Trim(PrinterInstalled.ToUpper)
					existe = True
					Exit For
				Else
					If arrayImpresora1.Length > 1 Then
						If Trim(Impresora.ToUpper) = arrayImpresora1(3) Then
							cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
							Return Trim(PrinterInstalled.ToUpper)
							Exit For
						End If
					End If
				End If
			Next

		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
		End Try

		MessageBox.Show("No se encontró la impresora correspondiente", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		Dim Impr As String = busca_impresora()
		If Impr = "" Then
			MessageBox.Show("No se seleccionó ninguna impresora, la comanda será eliminada", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
			Close()
			Return ""
			Exit Function
		End If
		cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Impresora='" & Impr & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
		Return Impr
	End Function

	Private Function verifica_impresora(ByVal impresora As String) As Boolean
		Dim PrinterInstalled As String
		Dim existe As Boolean = False
		Dim arrayImpresora1 As Array
		Dim arrayImpresora2 As Array
		For Each PrinterInstalled In PrinterSettings.InstalledPrinters
			arrayImpresora1 = PrinterInstalled.ToUpper.ToString.Split("\")
			arrayImpresora2 = impresora.ToUpper.ToString.Split("\")
			If Trim(impresora.ToUpper) = Trim(PrinterInstalled.ToUpper) Then
				existe = True
				Exit For
			Else
				If arrayImpresora2.Length > 1 Then
					If arrayImpresora2(3) = Trim(PrinterInstalled.ToUpper) Then
						impresoras = Trim(PrinterInstalled.ToUpper)
						existe = True
						Exit For
					End If
				End If
				If arrayImpresora1.Length > 1 Then
					If arrayImpresora1(3) = Trim(PrinterInstalled.ToUpper) Then
						impresoras = Trim(PrinterInstalled.ToUpper)
						existe = True
						Exit For
					End If
				End If
			End If
		Next
		Return existe
	End Function

	Private Sub actualizar()
		DescargaInventario()
		ActualizaMesa()
	End Sub

	Private Sub Imprime_Comanda(ByVal Impresora As String, ByVal Reimpresion As Boolean)
		'  Dim rptComanda1 As New rptComandasexpress

		Dim tipo As String
		If Me.nombreClienteexpress = "" Then
			tipo = "PARA LLEVAR"
		Else
			tipo = "PEDIDO EXPRESS"
		End If
		rptComandaExpres.Refresh()

		rptComandaExpres.PrintOptions.PrinterName = Impresora 'Automatic_Printer_Dialog(3) 'FACTURACION
		rptComandaExpres.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
		rptComandaExpres.SetParameterValue(0, numeroComanda)
		rptComandaExpres.SetParameterValue(1, 0)
		rptComandaExpres.SetParameterValue(2, Impresora)
		rptComandaExpres.SetParameterValue(3, User_Log.NombrePunto)

		If Reimpresion = True Then
			rptComandaExpres.SetParameterValue(4, "REIMPRESIÓN")
		Else
			rptComandaExpres.SetParameterValue(4, "")
		End If
		rptComandaExpres.SetParameterValue(5, tipo)

		'CrystalReportsConexion.LoadReportViewer(Nothing, rptComanda1, True, conectadobd.ConnectionString)
		rptComandaExpres.PrintToPrinter(1, True, 0, 0)
		'rptComandaExpres.Dispose()
		'rptComandaExpres.Close()
		'rptComandaExpres = Nothing

	End Sub

	Private Function busca_impresora() As String
		Try
			Dim PrintDocument1 As New PrintDocument
			Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
			Dim PrinterInstalled As String
			'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
			For Each PrinterInstalled In PrinterSettings.InstalledPrinters
				Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
					Case "FACTURACION" 'FACTURACION
						Return PrinterInstalled.ToString
						Exit Function
				End Select
			Next

			Dim PrinterDialog As New PrintDialog
			Dim DocPrint As New PrintDocument
			PrinterDialog.Document = DocPrint
			If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
				Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
			Else
				Return "" 'Ninguna Impresora
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Function

	Private Sub DescargaInvRecursivo(ByVal Id_Receta As Integer, ByVal cantidad As Double)
		'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
		Dim conexion As New SqlConnection
		Dim Conexion1 As New SqlConnection
		'
		Dim existencia As Double = 0
		Dim existencia_inv As Double = 0
		Dim existencia_bod As Double = 0
		Dim existencia_inv_act As Double = 0
		Dim existencia_bod_act As Double = 0
		Dim arrayBodegas(200) As Integer
		Dim dt As New DataTable
		'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
		Dim Costo_Movimiento As Double
		Dim saldo_final As Double
		Dim costo_promedio As Double
		Dim id_bodega As Integer
		Dim ultimocosto As Boolean
		Dim costo_real, CostoUltimo As Double
		Dim reduce As Double = 0
		cFunciones.Llenar_Tabla_Generico("SELECT     Codigo, Articulo, Disminuye,cantidad,idBodegaDescarga as bodega        FROM Recetas_Detalles " &
									" WHERE     (IdReceta = " & Id_Receta & ")", dt)
		conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
		Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
		If conexion.State = ConnectionState.Closed Then conexion.Open()
		For i As Integer = 0 To dt.Rows.Count - 1
			If dt.Rows(i).Item("Articulo") Then
				'EXISTENCIA EN BODEGA
				existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Codigo"))
				existencia_inv = existencia

				If Not GetSetting("SeeSoft", "Restaurante", "actualizado").Equals("") Then
					reduce = (cantidad * dt.Rows(i).Item("disminuye")) 'Se usa en caso de que se haya metido los datos bien y actualizados
				Else
					reduce = dt.Rows(i).Item("disminuye")
				End If

				existencia = existencia - reduce
				existencia_inv_act = existencia
				'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
				cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dt.Rows(i).Item("Codigo"))
				'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
				id_bodega = dt.Rows(i).Item("Bodega") 'rs("bodega")
				existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
				saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
				costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
				'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
				existencia_bod = existencia
				If Not GetSetting("SeeSoft", "Restaurante", "actualizado").Equals("") Then
					reduce = (cantidad * dt.Rows(i).Item("disminuye")) 'Se usa en caso de que se haya metido los datos bien y actualizados
				Else
					reduce = dt.Rows(i).Item("disminuye")
				End If
				existencia = existencia - reduce
				existencia = Math.Round(existencia, 2)
				Costo_Movimiento = reduce * costo_promedio
				Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
				saldo_final = saldo_final - Costo_Movimiento
				existencia_bod_act = existencia
				'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
				cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dt.Rows(i).Item("Codigo") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
				'INGRESA AL KARDEX
				actualiza_kardex(conexion, dt.Rows(i).Item("Codigo"), reduce, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dt.Rows(i).Item("Bodega"), costo_promedio, saldo_final)
			Else
				Dim dt_rec As New DataTable
				cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dt.Rows(i).Item("Codigo") & ") ", dt_rec)
				DescargaInvRecursivo(dt.Rows(i).Item("Codigo"), cantidad * (dt.Rows(i).Item("cantidad") / dt_rec.Rows(0).Item("Porciones")))

			End If

		Next

		'*******************************************FIN ARTICULOS*********************************************************************************************************

	End Sub

	Private Sub DescargaInventario()
		Try
			'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
			Dim conexion As New SqlConnection
			Dim Conexion1 As New SqlConnection
			'
			Dim existencia As Double = 0
			Dim existencia_inv As Double = 0
			Dim existencia_bod As Double = 0
			Dim existencia_inv_act As Double = 0
			Dim existencia_bod_act As Double = 0
			Dim arrayBodegas(200) As Integer

			'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
			Dim Costo_Movimiento As Double
			Dim saldo_final As Double
			Dim costo_promedio As Double
			Dim id_bodega As Integer
			Dim costo_real As Double
			Dim UltimoCosto As Boolean : Dim CostoUltimo As Double

			If conectadobd.State = ConnectionState.Closed Then conectadobd.Open()
			Dim dtLinea As New DataTable
			cFunciones.Llenar_Tabla_Generico("Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle, Menu_Restaurante.Tipo from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND ComandaTemporal_Expres.cCodigo =" & numeroComanda & " And (cCantidad - Impreso <> 0)", dtLinea)

			'cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle, Menu_Restaurante.Tipo from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND ComandaTemporal_Expres.cCodigo =" & numeroComanda & " And (cCantidad - Impreso <> 0)", rs) 'SAJ x aqui paso, el q escribe los rotulos!
			conectadobd.Close()
			conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
			Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If conexion.State = ConnectionState.Closed Then conexion.Open()
			'While rs.Read
			For i As Integer = 0 To dtLinea.Rows.Count - 1

				If dtLinea.Rows(i).Item("Tipo") = 2 Then
					'EXISTENCIA EN BODEGA
					existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dtLinea.Rows(i).Item("Id_Receta"))
					existencia_inv = existencia
					Dim dism As Double = dtLinea.Rows(i).Item("disminuye")
					If dism <= 0 Then
						dism = 1
					End If
					existencia = existencia - (dtLinea.Rows(i).Item("cCantidad") * dism)
					existencia_inv_act = existencia
					'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
					cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dtLinea.Rows(i).Item("Id_Receta"))
					'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
					id_bodega = dtLinea.Rows(i).Item("bodega")
					existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dtLinea.Rows(i).Item("Id_Receta") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
					saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dtLinea.Rows(i).Item("Id_Receta") & "and idBodega = " & id_bodega)
					costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dtLinea.Rows(i).Item("Id_Receta") & "and idBodega = " & id_bodega)
					'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
					existencia_bod = existencia
					existencia = existencia - (dtLinea.Rows(i).Item("cCantidad") * dism)
					existencia = Math.Round(existencia, 2)
					Costo_Movimiento = (dtLinea.Rows(i).Item("cCantidad") * dism) * costo_promedio
					Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
					saldo_final = saldo_final - Costo_Movimiento
					existencia_bod_act = existencia
					'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
					cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dtLinea.Rows(i).Item("Id_Receta") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
					'INGRESA AL KARDEX
					actualiza_kardex(conexion, dtLinea.Rows(i).Item("Id_Receta"), (dtLinea.Rows(i).Item("cCantidad") * dism), existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dtLinea.Rows(i).Item("Bodega"), costo_promedio, saldo_final)
					'En la tabla de conf de Restaurante se escoje si se da el último costo o costo promedio
					UltimoCosto = cConexion.SlqExecuteScalar(Conexion1, "Select UltimoPrecio from conf")
					If UltimoCosto = False Then
						costo_real = dtLinea.Rows(i).Item("disminuye") * costo_promedio
					Else
						CostoUltimo = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Costo from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dtLinea.Rows(i).Item("Id_Receta"))
						costo_real = dtLinea.Rows(i).Item("disminuye") * CostoUltimo
					End If

					If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
					cConexion.SlqExecute(Conexion1, "Update ComandaTemporal_Expres set ComandaTemporal_Expres.costo_real = " & costo_real & ", ComandaTemporal_Expres.comandado = 1 where ComandaTemporal_Expres.id = " & dtLinea.Rows(i).Item("id_detalle"))
				Else
					Dim dt_rec As New DataTable
					cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dtLinea.Rows(i).Item("Id_Receta") & ") ", dt_rec)
					Dim dism As Double = dtLinea.Rows(i).Item("disminuye")
					If dism <= 0 Then
						dism = 1
					End If
					DescargaInvRecursivo(dtLinea.Rows(i).Item("Id_Receta"), (dtLinea.Rows(i).Item("cCantidad") * dism) / dt_rec.Rows(0).Item("Porciones"))

				End If
				'End While

			Next

			'rs.Close()
			'*******************************************FIN ARTICULOS*********************************************************************************************************

			'ya con los que son articulos rebajados ahora vamos con los que son recetas... aunq los hubiera rebajado todos de un solo pero me dan pere...
			'es mas hagamos un cambio... vamos a cambiarle el tipo de articulo a los acompañamientos y a los modificadores forzados para q aparescan aca y no estorben
			'x los momentos trabajemos x separado asi:
			'el tipo 2 son articulos
			'el tipo 1 recetas

			'*******************************************PRINCIPIO RECETAS*********************************************************************************************************
			'cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND  ComandaTemporal_Expres.cCodigo =" & numeroComanda & " And Menu_Restaurante.Tipo=1 And (cCantidad - Impreso <> 0)", rs) 'SAJ 
			cFunciones.Llenar_Tabla_Generico("Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND  ComandaTemporal_Expres.cCodigo =" & numeroComanda & " And Menu_Restaurante.Tipo=1 And (cCantidad - Impreso <> 0)", dtLinea)
			'While rs.Read
			For i As Integer = 0 To dtLinea.Rows.Count - 1


				Dim CostoXreceta As Double
				Dim dism As Double = dtLinea.Rows(i).Item("disminuye")
				If dism <= 0 Then
					dism = 1
				End If
				CostoXreceta = CalculaCostoReceta_Mejorado(dtLinea.Rows(i).Item("Id_Receta")) * dism * dtLinea.Rows(i).Item("cCantidad")
				'y se manda a actualizar a la tabla temporal el costo real de la receta.
				'Diego
				If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
				cConexion.SlqExecute(Conexion1, "Update ComandaTemporal_Expres set ComandaTemporal_Expres.costo_real = " & CostoXreceta & ",ComandaTemporal_Expres.comandado = 1 where ComandaTemporal_Expres.id = " & dtLinea.Rows(i).Item("id_detalle"))
			Next
			'rs.Close()
			cConexion.DesConectar(Conexion1)
			'*******************************************FIN RECETAS*********************************************************************************************************

			'*******************************************PRINCIPIO ACOMPAÑAMIENTOS*********************************************************************************************************
			Dim rs_Acompa As SqlDataReader
			cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id, dbo.ComandaTemporal_Expres.cProducto, dbo.Acompañamientos_Menu.idBodegaDescarga,dbo.Acompañamientos_Menu.Tipo,Acompañamientos_Menu.ID_Receta FROM dbo.Acompañamientos_Menu INNER JOIN dbo.ComandaTemporal_Expres ON dbo.Acompañamientos_Menu.Id = dbo.ComandaTemporal_Expres.cProducto where ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Acompa)

			Dim costo_acomp As Double
			While rs_Acompa.Read
				If rs_Acompa("Tipo") = 1 Then
					costo_acomp = CalculaCostoReceta(rs_Acompa("ID_Receta"), 1, 1, rs_Acompa("idBodegaDescarga"), True)
				Else
					costo_acomp = CalculaCostoArticulo(0, rs_Acompa("cProducto"), 1, 1, rs_Acompa("idBodegaDescarga"), "", True)
				End If
				If conexion.State = ConnectionState.Closed Then conexion.Open()
				cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.costo_real = " & costo_acomp & ", Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Acompa("id"))
			End While
			rs_Acompa.Close()
			'*******************************************FIN ACOMPAÑAMIENTOS*********************************************************************************************************

			Dim rs_Modifi As SqlDataReader
			cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id FROM dbo.ComandaTemporal_Expres WHERE dbo.ComandaTemporal_Expres.Tipo_Plato= 'Modificador' and ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Modifi)

			While rs_Modifi.Read
				cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Modifi("id"))
			End While
			rs_Modifi.Close()

		Catch ex As Exception
			MessageBox.Show("Error al actualizar el inventario: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		End Try
	End Sub

#Region "Anular"

#Region "RecargaInventario"
	Private Sub RecargarInvRecursivo(ByVal Id_Receta As Integer, ByVal cantidad As Double)
		'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
		Dim conexion As New SqlConnection
		Dim Conexion1 As New SqlConnection
		'
		Dim existencia As Double = 0
		Dim existencia_inv As Double = 0
		Dim existencia_bod As Double = 0
		Dim existencia_inv_act As Double = 0
		Dim existencia_bod_act As Double = 0
		Dim arrayBodegas(200) As Integer
		Dim dt As New DataTable
		'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
		Dim Costo_Movimiento As Double
		Dim saldo_final As Double
		Dim costo_promedio As Double
		Dim id_bodega As Integer
		Dim aumenta As Double = 0

		cFunciones.Llenar_Tabla_Generico("SELECT     Codigo, Articulo, Disminuye,cantidad,idBodegaDescarga as bodega        FROM Recetas_Detalles " &
									" WHERE     (IdReceta = " & Id_Receta & ")", dt)
		conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
		Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
		If conexion.State = ConnectionState.Closed Then conexion.Open()
		For i As Integer = 0 To dt.Rows.Count - 1
			If dt.Rows(i).Item("Articulo") Then
				'EXISTENCIA EN BODEGA
				existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & dt.Rows(i).Item("Codigo"))
				existencia_inv = existencia
				existencia = existencia + (cantidad * dt.Rows(i).Item("disminuye"))
				existencia_inv_act = existencia
				'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
				cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & dt.Rows(i).Item("Codigo"))
				'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
				id_bodega = rs("bodega")
				existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
				saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
				costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & dt.Rows(i).Item("Codigo") & "and idBodega = " & id_bodega)
				'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
				existencia_bod = existencia
				existencia = existencia + (cantidad * dt.Rows(i).Item("disminuye"))
				existencia = Math.Round(existencia, 2)
				Costo_Movimiento = (cantidad * dt.Rows(i).Item("disminuye")) * costo_promedio
				Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
				saldo_final = saldo_final + Costo_Movimiento
				existencia_bod_act = existencia
				'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
				cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & dt.Rows(i).Item("Codigo") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
				'INGRESA AL KARDEX
				actualiza_kardex(conexion, dt.Rows(i).Item("Codigo"), (cantidad * dt.Rows(i).Item("disminuye")), existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, dt.Rows(i).Item("Bodega"), costo_promedio, saldo_final)
				'En la tabla de conf de Restaurante se escoje si se da el último costo o costo promedio
				'UltimoCosto = cConexion.SlqExecuteScalar(Conexion1, "Select UltimoPrecio from conf")
				'If UltimoCosto = False Then
				'    costo_real = dt.Rows(i).Item("disminuye") * costo_promedio
				'Else
				'    CostoUltimo = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Costo from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & rs("Id_Receta"))
				'    costo_real = dt.Rows(i).Item("disminuye") * CostoUltimo
				'End If

				'If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
				'cConexion.SlqExecute(Conexion1, "Update ComandaTemporal set ComandaTemporal.costo_real = " & costo_real & ", ComandaTemporal.comandado = 1 where ComandaTemporal.id = " & rs("id_detalle"))
			Else
				Dim dt_rec As New DataTable
				cFunciones.Llenar_Tabla_Generico("SELECT     ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & dt.Rows(i).Item("Codigo") & ") ", dt_rec)
				DescargaInvRecursivo(dt.Rows(i).Item("Codigo"), cantidad * (dt.Rows(i).Item("cantidad") / dt_rec.Rows(0).Item("Porciones")))

			End If

		Next

		'*******************************************FIN ARTICULOS*********************************************************************************************************

	End Sub
	Private Function CalculaCostoRecetaanulado(ByVal codigo_receta As Integer, ByVal disminuye As Double, ByVal cantidad As Double, ByVal idBodegaDescarga As Double, ByVal acom As Boolean) As Double
		Try
			Dim cnx As New SqlClient.SqlConnection
			Dim exe As New ConexionR
			Dim calculando_costo As Double
			Dim dsDetalle_RecetaAnidada As New DataSet
			Dim fila_receta As DataRow
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()
			exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Articulo,idBodegaDescarga, Disminuye FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
			For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
				If fila_receta("Articulo") = 0 Then
					calculando_costo += CalculaCostoRecetaanulado(fila_receta("Codigo"), fila_receta("Disminuye"), cantidad, idBodegaDescarga, acom)
				Else
					calculando_costo += CalculaCostoArticuloanulado(codigo_receta, fila_receta("Codigo"), cantidad, disminuye, fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), acom)
				End If
			Next
			Return calculando_costo
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function

	Private Sub RecargaInventario(ByVal numerofactura As Integer, ByVal venta As Boolean)
		Try
			'Obtiene dataset con los articulos que no requieren receta para luego ser rebajados del inventario general de proveeduria
			Dim conexion As New SqlConnection
			Dim Conexion1 As New SqlConnection
			Dim existencia As Double = 0
			Dim existencia_inv As Double = 0
			Dim existencia_bod As Double = 0
			Dim existencia_inv_act As Double = 0
			Dim existencia_bod_act As Double = 0
			Dim arrayBodegas(200) As Integer
			Dim rs_recarga As SqlDataReader
			'RELACIONADOS CON EL COSTO PROMEDIO Y EL SALDO FINAL
			Dim Costo_Movimiento As Double
			Dim saldo_final As Double
			Dim costo_promedio As Double
			Dim id_bodega As Integer
			Dim costo_real As Double
			Dim aumenta As Double = 0
			If venta Then
				cConexion.GetRecorset(conectadobd, "SELECT Menu_Restaurante.Id_Receta,Menu_Restaurante.Tipo, Menu_Restaurante.disminuye, Menu_Restaurante.bodega, Ventas_Detalle.Codigo, Ventas_Detalle.Cantidad,Ventas_Detalle.Id_Factura  FROM Menu_Restaurante INNER JOIN Ventas_Detalle ON Menu_Restaurante.Id_Menu = Ventas_Detalle.Codigo  WHERE(Ventas_Detalle.Id_Factura = " & numerofactura & ")", rs_recarga) 'SAJ x aqui paso, el q escribe los rotulos!
			Else
				cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad as Cantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle, Menu_Restaurante.Tipo from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 1 AND ComandaTemporal_Expres.Id =" & numerofactura & " ", rs_recarga) 'SAJ x aqui paso, el q escribe los rotulos!
			End If

			conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
			Conexion1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If conexion.State = ConnectionState.Closed Then conexion.Open()
			While rs_recarga.Read
				If rs_recarga("Tipo") = 2 Then
					'EXISTENCIA EN BODEGA
					existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & rs_recarga("Id_Receta"))
					existencia_inv = existencia
					If Not GetSetting("SeeSoft", "Restaurante", "Actualizado").Equals("") Then
						aumenta = (rs_recarga("Cantidad") * rs_recarga("disminuye"))
					Else
						aumenta = rs_recarga("disminuye")
					End If
					existencia = existencia + aumenta
					existencia_inv_act = existencia
					'LE AUMENTA LA EXISTENCIA A LA BODEGA CENTRAL
					cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & rs_recarga("Id_Receta"))
					'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
					id_bodega = rs_recarga("bodega")
					existencia = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & id_bodega)
					saldo_final = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & "and idBodega = " & id_bodega)
					costo_promedio = cConexion.SlqExecuteScalar(conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & rs_recarga("Id_Receta") & "and idBodega = " & id_bodega)
					'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
					existencia_bod = existencia
					If Not GetSetting("SeeSoft", "Restaurante", "Actualizado").Equals("") Then
						aumenta = (rs_recarga("Cantidad") * rs_recarga("disminuye"))
					Else
						aumenta = rs_recarga("disminuye")
					End If
					existencia = existencia + aumenta

					existencia = Math.Round(existencia, 2)
					Costo_Movimiento = aumenta * costo_promedio
					Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
					saldo_final = saldo_final - Costo_Movimiento
					existencia_bod_act = existencia
					'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
					cConexion.SlqExecute(conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & rs_recarga("Id_Receta") & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & id_bodega)
					'INGRESA AL KARDEX
					actualiza_kardex(conexion, rs_recarga("Id_Receta"), aumenta, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, rs_recarga("Bodega"), costo_promedio, saldo_final)
					costo_real = aumenta * costo_promedio
					If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
					cConexion.SlqExecute(Conexion1, "Update ComandaTemporal_Expres set ComandaTemporal_Expres.costo_real = " & costo_real & ", ComandaTemporal_Expres.comandado = 1 where ComandaTemporal_Expres.id = " & rs_recarga("id_detalle"))
				Else

					Dim dt_rec As New DataTable
					cFunciones.Llenar_Tabla_Generico("SELECT ID, Nombre, Porciones FROM Recetas WHERE     (ID = " & rs_recarga("Id_Receta") & ") ", dt_rec)

					RecargarInvRecursivo(rs_recarga("Id_Receta"), (rs_recarga("Cantidad") * rs_recarga("disminuye")) / dt_rec.Rows(0).Item("Porciones"))

				End If

			End While
			rs_recarga.Close()
			'*******************************************FIN ARTICULOS*********************************************************************************************************

			'ya con los que son articulos rebajados ahora vamos con los que son recetas... aunq los hubiera rebajado todos de un solo pero me dan pere...
			'es mas hagamos un cambio... vamos a cambiarle el tipo de articulo a los acompañamientos y a los modificadores forzados para q aparescan aca y no estorben
			'x los momentos trabajemos x separado asi:
			'el tipo 2 son articulos
			'el tipo 1 recetas
			'*******************************************PRINCIPIO RECETAS**************************z*******************************************************************************
			Dim rs_rect As SqlDataReader
			cConexion.GetRecorset(conectadobd, "Select Menu_Restaurante.Id_Receta, Menu_Restaurante.disminuye, ComandaTemporal_Expres.cCantidad, Menu_Restaurante.bodega, ComandaTemporal_Expres.id as id_detalle from ComandaTemporal_Expres inner join Menu_Restaurante on ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu where ComandaTemporal_Expres.comandado = 0 AND  ComandaTemporal_Expres.cCodigo =" & numeroComanda & " And Menu_Restaurante.Tipo=1 And (cCantidad - Impreso <> 0)", rs_rect) 'SAJ 
			While rs_rect.Read
				Dim CostoXreceta As Double
				CostoXreceta = CalculaCostoReceta(rs_rect("Id_Receta"), rs_rect("disminuye"), rs_rect("cCantidad"), rs_rect("bodega"), False)
				'y se manda a actualizar a la tabla temporal el costo real de la receta.
				'Diego
				If Conexion1.State <> ConnectionState.Open Then Conexion1.Open()
				cConexion.SlqExecute(Conexion1, "Update ComandaTemporal_Expres set ComandaTemporal_Expres.costo_real = " & CostoXreceta & ",ComandaTemporal_Expres.comandado = 1 where ComandaTemporal_Expres.id = " & rs_rect("id_detalle"))
			End While
			cConexion.DesConectar(Conexion1)
			rs_rect.Close()
			'*******************************************FIN RECETAS*********************************************************************************************************

			'*******************************************PRINCIPIO ACOMPAÑAMIENTOS*********************************************************************************************************
			Dim rs_Acompa As SqlDataReader
			cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id, dbo.ComandaTemporal_Expres.cProducto, dbo.Acompañamientos_Menu.idBodegaDescarga,dbo.Acompañamientos_Menu.Tipo,Acompañamientos_Menu.ID_Receta FROM dbo.Acompañamientos_Menu INNER JOIN dbo.ComandaTemporal_Expres ON dbo.Acompañamientos_Menu.Id = dbo.ComandaTemporal_Expres.cProducto where ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Acompa)

			Dim costo_acomp As Double
			While rs_Acompa.Read
				If rs_Acompa("Tipo") = 1 Then
					costo_acomp = Me.CalculaCostoRecetaanulado(rs_Acompa("ID_Receta"), 1, 1, rs_Acompa("idBodegaDescarga"), True)
				Else
					costo_acomp = CalculaCostoArticuloanulado(0, rs_Acompa("cProducto"), 1, 1, rs_Acompa("idBodegaDescarga"), "", True)
				End If
				If conexion.State = ConnectionState.Closed Then conexion.Open()

				cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.costo_real = " & costo_acomp & ", Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Acompa("id"))
			End While
			rs_Acompa.Close()
			'*******************************************FIN ACOMPAÑAMIENTOS*********************************************************************************************************

			'Dim rs_Modifi As SqlDataReader
			'cConexion.GetRecorset(conectadobd, "SELECT dbo.ComandaTemporal_Expres.id FROM dbo.ComandaTemporal_Expres WHERE dbo.ComandaTemporal_Expres.Tipo_Plato= 'Modificador' and ComandaTemporal_Expres.cCodigo =" & numeroComanda, rs_Modifi)

			'While rs_Modifi.Read
			'    cConexion.SlqExecute(conexion, "Update Restaurante.dbo.ComandaTemporal_Expres set  Restaurante.dbo.ComandaTemporal_Expres.comandado = 1 where Restaurante.dbo.ComandaTemporal_Expres.id = " & rs_Modifi("id"))
			'End While
			'rs_Modifi.Close()
		Catch ex As Exception
			MsgBox("Error al actualizar el inventario: " & ex.Message, MsgBoxStyle.OkOnly)
		End Try
	End Sub

#End Region

#Region "CalculaCostoArticuloanulado"
	'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
	Private Function CalculaCostoArticuloanulado(ByVal id_receta As Integer, ByVal codigo_articulo As Integer, ByVal Cantidad As Double, ByVal Disminuye As Double, ByVal idbodega_descarga As Integer, ByVal nombre As String) As Double
		Try

			Dim costoXarticulo As Double
			Dim exe As New ConexionR
			Dim cnx As New SqlClient.SqlConnection
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()
			costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & id_receta)
			'Esta es la verdadera cantidad q disminuye una porcion de este articulo en una receta
			Disminuye = exe.SlqExecuteScalar(cnx, "Select disminuye From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			If costoXarticulo = 0 Then
				costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			End If

			Dim Conexion As New SqlClient.SqlConnection
			Dim existencia, existencia_inv, existencia_inv_act, existencia_bod, existencia_bod_act As Double 'EXISTENCIAS DE INVENTARIO
			Dim saldo_final, costo_promedio, Costo_Movimiento, costo_real As Double
			Conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
			If Conexion.State = ConnectionState.Closed Then Conexion.Open()

			'EXISTENCIA EN BODEGA
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & codigo_articulo)
			existencia_inv = existencia
			existencia = existencia + (Cantidad * Disminuye)
			existencia_inv_act = existencia

			'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
			' cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & codigo_articulo)

			'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
			'ESTO RELACIONADO CON EL COSTO PROMEDIO Y SU DESCARGA DEL INVENTARIO DE LA BODEGA A LA QUE ESTA ASOCIADO ESTE ARTICULO DE MENU
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & idbodega_descarga)
			saldo_final = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			costo_promedio = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
			existencia_bod = existencia
			existencia = existencia + (Cantidad * Disminuye)
			existencia = Math.Round(existencia, 2)
			Costo_Movimiento = (Cantidad * Disminuye) * costo_promedio
			Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
			saldo_final = saldo_final - Costo_Movimiento
			existencia_bod_act = existencia
			'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
			'cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & codigo_articulo & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & idbodega_descarga)
			'INGRESA AL KARDEX
			' actualiza_kardex(Conexion, codigo_articulo, Cantidad * Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, idbodega_descarga, costo_promedio, saldo_final)
			costoXarticulo = costoXarticulo * Cantidad

			Return costoXarticulo
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function
#End Region

#Region "CalculacostoRecetaanulado"
	'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
	Private Function CalculaCostoArticuloanulado(ByVal id_receta As Integer, ByVal codigo_articulo As Integer, ByVal Cantidad As Double, ByVal Disminuye As Double, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal acom As Boolean) As Double
		Try

			Dim costoXarticulo As Double
			Dim exe As New ConexionR
			Dim cnx As New SqlClient.SqlConnection
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()
			costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & id_receta)
			'Esta es la verdadera cantidad q disminuye una porcion de este articulo en una receta
			Disminuye = exe.SlqExecuteScalar(cnx, "Select disminuye From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			If costoXarticulo = 0 Then
				costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			End If

			Dim Conexion As New SqlClient.SqlConnection
			Dim existencia, existencia_inv, existencia_inv_act, existencia_bod, existencia_bod_act As Double 'EXISTENCIAS DE INVENTARIO
			Dim saldo_final, costo_promedio, Costo_Movimiento, costo_real As Double
			Conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
			If Conexion.State = ConnectionState.Closed Then Conexion.Open()

			'EXISTENCIA EN BODEGA
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & codigo_articulo)
			existencia_inv = existencia
			existencia = existencia + (Cantidad * Disminuye)
			existencia_inv_act = existencia

			'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
			If acom Then
				cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & codigo_articulo)
			End If


			'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
			'ESTO RELACIONADO CON EL COSTO PROMEDIO Y SU DESCARGA DEL INVENTARIO DE LA BODEGA A LA QUE ESTA ASOCIADO ESTE ARTICULO DE MENU
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & idbodega_descarga)
			saldo_final = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			costo_promedio = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
			existencia_bod = existencia
			existencia = existencia + (Cantidad * Disminuye)
			existencia = Math.Round(existencia, 2)
			Costo_Movimiento = (Cantidad * Disminuye) * costo_promedio
			Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
			saldo_final = saldo_final - Costo_Movimiento
			existencia_bod_act = existencia
			If acom Then
				'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA
				cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & codigo_articulo & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & idbodega_descarga)
				'INGRESA AL KARDEX
				actualiza_kardex(Conexion, codigo_articulo, Cantidad * Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, idbodega_descarga, costo_promedio, saldo_final)
			End If

			costoXarticulo = costoXarticulo * Cantidad

			Return costoXarticulo
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function
#End Region

#End Region
	Private Function CalculaCostoReceta_Mejorado(ByVal codigo_receta As Integer) As Double

		Try
			Dim dt As New DataTable
			cFunciones.Llenar_Tabla_Generico("Select * From Recetas WHere ID = " & codigo_receta, dt)
			Dim porc As Double = dt.Rows(0).Item("Porciones")

			Dim cnx As New SqlClient.SqlConnection

			Dim exe As New ConexionR

			Dim calculando_costo As Double

			Dim dsDetalle_RecetaAnidada As New DataSet

			Dim fila_receta As DataRow

			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

			If cnx.State = ConnectionState.Closed Then cnx.Open()

			exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Cantidad, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")

			For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows

				If fila_receta("Articulo") = 0 Then
					calculando_costo += CalculaCostoReceta_Mejorado(fila_receta("Codigo")) * fila_receta("Cantidad")

				Else
					calculando_costo += (CalculaCostoArticulo_Mejorado(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), fila_receta("idreceta")) * fila_receta("Cantidad"))

				End If
			Next

			Dim especies As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("Especies")) / 100)

			Dim desaprovechamiento As Double = CDbl(calculando_costo) * (CDbl(dt.Rows(0).Item("aprovechamiento")) / 100)

			calculando_costo = (calculando_costo + especies + desaprovechamiento) '/ dt.Rows(0).Item("Porciones")

			Return calculando_costo / porc

		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")

		End Try

	End Function
	'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
	Private Function CalculaCostoArticulo_Mejorado(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal Receta As Integer) As Double
		Try
			Dim costoXarticulo As Double
			Dim exe As New ConexionR
			Dim cnx As New SqlClient.SqlConnection
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()

			costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoUnit FROM VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & Receta)
			If costoXarticulo = 0 Then
				'Cambio por Diego lo deshabilite para que no salga donde los clientes
				'MsgBox("El Articulo " + nombre + " no se ha registrado en la bodega que se indico para descarga, se utilizará el precio base para el calculo de la receta", MsgBoxStyle.OkOnly, "Servicios Estructurales SeeSoft")
				costoXarticulo = 0
			End If
			Return costoXarticulo
			cnx.Close()
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function
	'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
	Private Function CalculaCostoReceta(ByVal codigo_receta As Integer, ByVal disminuye As Double, ByVal cantidad As Double, ByVal idBodegaDescarga As Double, ByVal acom As Boolean, Optional ByVal nivel As Integer = 0) As Double
		Try
			Dim cnx As New SqlClient.SqlConnection
			Dim exe As New ConexionR
			Dim calculando_costo As Double
			Dim dsDetalle_RecetaAnidada As New DataSet
			Dim fila_receta As DataRow
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()
			exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Articulo,idBodegaDescarga, Disminuye, Recetas.Porciones, Cantidad FROM Recetas_Detalles, Recetas WHERE Recetas.ID = " & codigo_receta & " AND IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
			For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
				If fila_receta("Articulo") = 0 Then
					calculando_costo += CalculaCostoReceta(fila_receta("Codigo"), fila_receta("cantidad") / fila_receta("Porciones"), cantidad, idBodegaDescarga, 1) / cantidad * (fila_receta("cantidad") / fila_receta("Porciones"))
				Else
					If nivel = 0 Then
						calculando_costo += CalculaCostoArticulo(codigo_receta, fila_receta("Codigo"), cantidad, disminuye / fila_receta("Porciones"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), False)
					Else
						calculando_costo += CalculaCostoArticulo(codigo_receta, fila_receta("Codigo"), cantidad, disminuye, fila_receta("idBodegaDescarga"), fila_receta("Descripcion"), acom)
					End If

				End If
			Next
			Return calculando_costo
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function
	'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
	Private Function CalculaCostoArticulo(ByVal id_receta As Integer, ByVal codigo_articulo As Integer, ByVal Cantidad As Double, ByVal Disminuye As Double, ByVal idbodega_descarga As Integer, ByVal nombre As String, ByVal acom As Boolean) As Double
		Try

			Dim costoXarticulo As Double
			Dim exe As New ConexionR
			Dim cnx As New SqlClient.SqlConnection
			cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
			If cnx.State = ConnectionState.Closed Then cnx.Open()
			costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo & " and IdReceta = " & id_receta)
			'Esta es la verdadera cantidad q disminuye una porcion de este articulo en una receta
			Dim dismAnte As Double = Disminuye
			Disminuye = exe.SlqExecuteScalar(cnx, "Select disminuye From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			If costoXarticulo = 0 Then
				costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From Recetas_Detalles Where idReceta = " & id_receta & " and idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
			End If
			Disminuye = Disminuye * dismAnte
			Dim Conexion As New SqlClient.SqlConnection
			Dim existencia, existencia_inv, existencia_inv_act, existencia_bod, existencia_bod_act As Double 'EXISTENCIAS DE INVENTARIO
			Dim saldo_final, costo_promedio, Costo_Movimiento, costo_real As Double
			Conexion.ConnectionString = GetSetting("SeeSOFT", "Proveeduria", "Conexion")
			If Conexion.State = ConnectionState.Closed Then Conexion.Open()
			'Disminuye1 = Disminuye1 * Disminuye
			'EXISTENCIA EN BODEGA
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.Inventario.Existencia from Proveeduria.dbo.Inventario where Proveeduria.dbo.Inventario.Codigo=" & codigo_articulo)
			existencia_inv = existencia
			existencia = existencia - (Cantidad * Disminuye)
			existencia_inv_act = existencia

			'LE REBAJA LA EXISTENCIA A LA BODEGA CENTRAL
			If acom Then
				cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.Inventario set Proveeduria.dbo.Inventario.Existencia=" & existencia & " where codigo = " & codigo_articulo)
			End If


			'EXISTENCIA EN BODEGA ASOCIADA AL ARTICULO DE MENU
			'ESTO RELACIONADO CON EL COSTO PROMEDIO Y SU DESCARGA DEL INVENTARIO DE LA BODEGA A LA QUE ESTA ASOCIADO ESTE ARTICULO DE MENU
			existencia = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Existencia from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & " And Proveeduria.dbo.ArticulosXBodega.IdBodega = " & idbodega_descarga)
			saldo_final = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Saldo_Final from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			costo_promedio = cConexion.SlqExecuteScalar(Conexion, "Select Proveeduria.dbo.ArticulosXBodega.Costo_Promedio from Proveeduria.dbo.ArticulosXBodega where Proveeduria.dbo.ArticulosXBodega.Codigo=" & codigo_articulo & "and idBodega = " & idbodega_descarga)
			'SE CALCULA CUANTO ES LO QUE SE VA A DISMINUIR MONETAREAMENTE DE LA BODEGA DEL INVENTARIO Y SE LE RESTA AL INVENTARIO
			existencia_bod = existencia
			existencia = existencia - (Cantidad * Disminuye)
			existencia = Math.Round(existencia, 2)
			Costo_Movimiento = (Cantidad * Disminuye) * costo_promedio
			Costo_Movimiento = Math.Round(Costo_Movimiento, 2)
			saldo_final = saldo_final - Costo_Movimiento
			existencia_bod_act = existencia
			If acom Then
				'LE REBAJA LA EXISTENCIA A LA BODEGA ASOCIADA AL PUNTO DE VENTA YA LO HACE EN OTRO LADO
				cConexion.SlqExecute(Conexion, "Update Proveeduria.dbo.ArticulosXBodega set Proveeduria.dbo.ArticulosXBodega.Existencia= " & existencia & ", Saldo_Final = " & saldo_final & " where Proveeduria.dbo.ArticulosXBodega.codigo = " & codigo_articulo & "and Proveeduria.dbo.ArticulosXBodega.idBodega = " & idbodega_descarga)
				'INGRESA AL KARDEX YA LO HACE EN OTRO LADO
				actualiza_kardex(Conexion, codigo_articulo, Cantidad * Disminuye, existencia_inv, existencia_bod, existencia_inv_act, existencia_bod_act, idbodega_descarga, costo_promedio, saldo_final)
			End If

			costoXarticulo = costoXarticulo * Cantidad

			Return costoXarticulo
		Catch ex As Exception
			MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
		End Try
	End Function

	Private Sub actualiza_kardex(ByRef conexion As SqlClient.SqlConnection, ByVal art As Integer, ByVal cantidad As Decimal, ByVal existencia_inv As Decimal, ByVal existencia_bod As Decimal, ByVal existencia_inv_act As Decimal, ByVal existencia_bod_act As Decimal, ByVal Codigo_Bodega As Integer, ByVal CostoPromedio As Double, ByVal SaldoFinal As Double)
		Try
			Dim codigo_prov, cod_moneda As Integer
			Dim costo_unit As Double
			Dim data As New DataSet
			'BUSCA CODIGO DEL PROVEEDOR, COSTO DEL ARTICULO Y CODIGO DE MONEDA
			cConexion.GetDataSet(conexion, "Select * from [Articulos x Proveedor] where CodigoArticulo=" & art, data, "Articulos")
			Dim fil As DataRow
			For Each fil In data.Tables("Articulos").Rows
				codigo_prov = fil("CodigoProveedor")
				cod_moneda = fil("Moneda")
				costo_unit = fil("UltimoCosto")
			Next
			Dim tipo As String = "CO" & Mid(User_Log.NombrePunto, 1, 1)
			'ACTUALIZAR MOVIMIENTOS EN EL KARDEX
			Dim campos As String = "Codigo, Documento, Tipo, Fecha, Exist_Ant, Cantidad, Exist_Act, Costo_Unit, Costo_Mov, Cod_Moneda, IdBodegaOrigen, IdBodegaDestino, Exist_AntBod, Exist_ActBod, Cod_Proveedor, Observaciones, Costo_Promedio, Saldo_Final"
			Dim datos As String = art & "," & NFactura & ",'FV', getdate()," & existencia_inv & "," & cantidad & "," & (existencia_inv_act) & "," & CostoPromedio & "," & (cantidad * CostoPromedio) & "," & cod_moneda & "," & Codigo_Bodega & "," & Codigo_Bodega & "," & existencia_bod & "," & (existencia_bod_act) & "," & codigo_prov & ",'Comanda " & User_Log.NombrePunto & " # " & numeroComanda & "' ," & CostoPromedio & "," & SaldoFinal
			cConexion.AddNewRecord(conexion, "Kardex", campos, datos)
		Catch ex As Exception
			MessageBox.Show("Error al actualizar el kardex: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		End Try
	End Sub

	Private Sub imprimir(ByVal NFactura As Integer, ByVal Idioma As String)
		If TipoImpres = 3 Then Globales.inicializarFactura(Idioma)
		imprimirFactura(NFactura, Idioma, Me.numeroComanda)
	End Sub
	Public Sub cort()
		Dim intFileNo As Integer = FreeFile()
		FileOpen(1, "c:\corte.txt", OpenMode.Output)
		PrintLine(1, Chr(29) & Chr(86) & Chr(0))
		FileClose(1)
		Shell("print LPT1 c:\corte.txt", AppWinStyle.NormalFocus)
	End Sub
	Private Sub ObtenerDimension() ' SAJ 080807. REDIMESIONAMIENTO DE LSA CATEGORIAS
		Dim Conexion As New Conexion
		Dim F, C As Integer
		Try

			F = Conexion.SlqExecuteScalar(conectadobd, "SELECT TOP 1 Filas FROM Categoria_Dimesion")
			C = Conexion.SlqExecuteScalar(conectadobd, "SELECT TOP 1 Columnas FROM Categoria_Dimesion")
			txtFilas.Text = F
			TxtColumnas.Text = C

		Catch ex As Exception
			MsgBox("Error al cargar la dimesión de las categorías..", MsgBoxStyle.Information, "Alerta...")
		End Try
	End Sub

	Private Sub ObtenerDimensionCategoria(ByVal Categoria As Integer) ' SAJ 080807. REDIMESIONAMIENTO DE LSA CATEGORIAS
		Dim Conexion As New Conexion
		Dim F, C As Integer
		Try
			'If Inicializando Then
			F = Conexion.SlqExecuteScalar(Conexion.Conectar(), "SELECT  Filas FROM Categorias_Menu WHERE Id = " & Categoria)
			C = Conexion.SlqExecuteScalar(Conexion.Conectar(), "SELECT  Columnas FROM Categorias_Menu WHERE Id = " & Categoria)
			txtFilas.Text = F
			TxtColumnas.Text = C
			'End If
		Catch ex As Exception
			MsgBox("Error al cargar la dimesión de las categorías..", MsgBoxStyle.Information, "Alerta...")
		End Try
	End Sub

	Private Sub ComboBoxComensal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxComensal.SelectedIndexChanged
		Dim Conexion As New ConexionR
		Dim Comensal As String
        If idMesa = "" Then Exit Sub
        If ParaLlevar = 1 Then ComboBoxComensal.Text = "1"

        If Trim(numeroComanda) = vbNullString Or Comensal = Nothing Then
		Else
			Comensal = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT DISTINCT Comenzales FROM ComandaTemporal_Expres WITH (NOLOCK) WHERE cMesa = " & idMesa & " AND cCodigo = " & numeroComanda)
		End If

		If Comensal = Nothing Then
			Comensal = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT Numero_Asientos FROM Mesas WITH (NOLOCK) WHERE Id = " & idMesa & "")
		End If

        If ComboBoxComensal.Text = Comensal Then Exit Sub
        Try
			If numeroComanda = Nothing Then
				MessageBox.Show("No se asignado ningun artículo a la mesa")
			Else
				Conexion.UpdateRecords(Conexion.Conectar("Restaurante"), "ComandaTemporal_Expres", "Comenzales = " & Me.ComboBoxComensal.Text, "cCodigo = " & numeroComanda)
				comenzales = Me.ComboBoxComensal.Text
			End If

		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención..")
		Finally
			Conexion.DesConectar(Conexion.SqlConexion)
		End Try
	End Sub

	Private Sub ComboBoxHabitaciones_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles ComboBoxHabitaciones.DrawItem
		Try
			If ComboBoxHabitaciones.Items(e.Index).Item("Habitacion") = vbNullString Then
				Exit Sub
			End If
			If ComboBoxHabitaciones.Items(e.Index).Item("Abierta") Then
				e.Graphics.DrawString(ComboBoxHabitaciones.Items(e.Index).Item("Habitacion"), e.Font, New SolidBrush(Color.Blue), e.Bounds)
			Else
				e.Graphics.DrawString(ComboBoxHabitaciones.Items(e.Index).Item("Habitacion"), e.Font, New SolidBrush(Color.Red), e.Bounds)
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub ComboBoxHabitaciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxHabitaciones.SelectedIndexChanged
		Dim Conexion As New ConexionR
		Dim HabitacionComanda As String
		If Cargando = True Then Exit Sub
		Try
			If numeroComanda <> Nothing Then
				HabitacionComanda = Conexion.SlqExecuteScalar(Conexion.Conectar("Restaurante"), "SELECT DISTINCT Habitacion FROM ComandaTemporal_Expres WITH (NOLOCK) WHERE cMesa = " & idMesa & " AND cCodigo = " & numeroComanda)
				If ComboBoxHabitaciones.Text = HabitacionComanda Then
					HabitacionAsignada = HabitacionComanda
					Exit Sub
				Else
					HabitacionAsignada = ComboBoxHabitaciones.Text
					Conexion.UpdateRecords(Conexion.Conectar("Restaurante"), "ComandaTemporal_Expres", "Habitacion = " & Me.ComboBoxHabitaciones.Text, "cCodigo = " & numeroComanda)
				End If
			End If
		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención..")
		Finally
			Conexion.DesConectar(Conexion.SqlConexion)
		End Try
	End Sub

	Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonRegresar.Click
		limpiaDatos()
		ObtenerDimension()
		CrearCategoriaMenu()
	End Sub
	Private Sub ToolButtonModificarComanda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonModificarComanda.Click
		RefrescaDatos()
		PanelMesas.Visible = False
		ComandasShow()
	End Sub

	Private Sub ToolButtonTerminar_nClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonTerminar.Click
        If Me.bandeTipo Then
            If lblEncabezado.Text.Equals("Acompañamientos") Then
                limpiaDatos()
				UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA FROM Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & idGrupoT)
				lblEncabezado.Text = "Menú"
                Bloquear(True)
                Me.bandeTipo = False
                '--------- habilitar botones -----
                Me.ToolButtonModificarComanda.Visible = True
                Me.ToolButtonRegresar.Visible = True
                Me.ToolStripButton9.Visible = usaGrupo
                Me.ToolStripButton8.Visible = True
                Me.tlsCantidad.Visible = True
                Me.ToolButtonNotas.Visible = True
                Me.ToolButtonExpress.Visible = True
                Me.ToolButtonFacturar.Visible = True
                ' Me.ToolStripButtonAcumular.Visible = True
                Me.ToolStripButtonTerminar.Enabled = True
                ToolButtonTerminar.Visible = True
                Exit Sub
            End If


            limpiaDatos()
            tieneAcompa = cConexion.SlqExecuteScalar(conectadobd, "Select Numero_Acompañamientos from Menu_Restaurante WITH (NOLOCK) where id_menu=" & idMenuActivo)

            If CDbl(tieneAcompa) > 0 Then limpiaDatos()
            If CDbl(tieneAcompa) > 0 Then
                lblEncabezado.Text = "Acompañamientos"
                limpiaDatos()
                Bloquear(True)

                Me.bandeTipo = True
                Me.ControlBox = True

                Me.ToolButtonModificarComanda.Visible = False
                Me.ToolButtonRegresar.Visible = False
                Me.ToolStripButton9.Visible = False
                Me.ToolStripButton8.Visible = False
                Me.tlsCantidad.Visible = False
                Me.ToolButtonNotas.Visible = False
                Me.ToolButtonExpress.Visible = False
                Me.ToolButtonFacturar.Visible = False
                Me.ToolStripButtonAcumular.Visible = False
                Me.ToolStripButtonTerminar.Enabled = True
                ToolButtonTerminar.Visible = True
                If p_Tipo = 1 Then

                    Dim dt As New DataTable
					cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio as Precio_VentaExpressA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion", dt)
					If dt.Rows.Count > 0 Then
						UbicarPosiciones("SELECT id,nombre,dbo.ACO_AGREGADO.posicion,imagenRuta,tipo='ACOMPA',dbo.ACO_AGREGADO.precio AS Precio_VentaExpressA FROM  Acompañamientos_Menu,dbo.ACO_AGREGADO WHERE dbo.ACO_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND ACO_AGREGADO.IdAgregado = Acompañamientos_Menu.Id order by dbo.ACO_AGREGADO.posicion")
					Else
						UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 as Precio_VentaExpressA FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")
					End If

                Else
					UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='ACOMPA',Precio_VentaA=0 as Precio_VentaExpressA FROM  Acompañamientos_Menu WITH (NOLOCK) order by posicion")

				End If

                Exit Sub

            Else
				UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA FROM Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & idGrupoT)
				lblEncabezado.Text = "Menú"
                Bloquear(True)

            End If
            Me.bandeTipo = False
            '--------- habilitar botones -----
            Me.ToolButtonModificarComanda.Visible = True
            Me.ToolButtonRegresar.Visible = True
            Me.ToolStripButton9.Visible = usaGrupo
            Me.ToolStripButton8.Visible = True
            Me.tlsCantidad.Visible = True
            Me.ToolButtonNotas.Visible = True
            Me.ToolButtonExpress.Visible = True
            Me.ToolButtonFacturar.Visible = True
            ' Me.ToolStripButtonAcumular.Visible = True
            Me.ToolStripButtonTerminar.Enabled = True
            ToolButtonTerminar.Visible = True
            Exit Sub
        End If


        If Me.TipoPedido = 1 Then
                    hacerLaFactura()
                Else
                    If todo_comandado = False Then
                        Terminar_Comanda()

                    Else
                        ActualizaMesa()
                        Close()
                    End If
                End If


    End Sub

	Private Sub ToolButtonSepararCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		'Es un nuevo módulo que Isaac programo para separar cuentas.
		Try
			Dim cf As New FormCrearCuentas
			cf.cantCuentas = cCuentas
			cf.Mesa = idMesa
			cf.NombreMesa = nombreMesa
			cf.ShowDialog()
			cCuentas = cf.cantCuentas
			cmbCuentas.Items.Clear()

			Dim xContador As Integer
			If cCuentas > 0 Then
				lblCuenta.Visible = True
				cmbCuentas.Visible = True
				For xContador = 1 To cCuentas
					cmbCuentas.Items.Add(xContador)
				Next
				cmbCuentas.Text = 0
			Else
				cmbCuentas.Items.Add(0)
			End If
			cmbCuentas.SelectedIndex = 0
			RefrescaDatos()
			PanelMesas.Visible = True
			ComandasShow()
			grpModifica.Visible = False


			'cConexion.GetRecorset(conectadobd, "Select * from ComandaTemporal_Expres where cCodigo=" & numeroComanda, rs)
			'If rs.Read = False Then
			'    MessageBox.Show("No tiene artículos pendientes", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
			'    rs.Close()
			'    Exit Sub
			'End If
			'rs.Close()
			'Dim CCuentaS As New SeleccionaFact
			'Me.Hide()
			'CCuentaS.numerocomanda = numeroComanda
			'CCuentaS.idmesa = idMesa
			'CCuentaS.clave = clave
			'CCuentaS.ShowDialog()
			'CSeparada = CCuentaS.separada
			'If CSeparada = True And CCuentaS.hecho = True Then
			'    If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
			'        imprimir(CCuentaS.NFactura, "espanol")
			'    Else
			'        imprimir(CCuentaS.NFactura, "Ingles")
			'    End If
			'End If
			'CCuentaS.Dispose()
		Catch ex As Exception
			MessageBox.Show("Error al cargar los datos", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		End Try
	End Sub

	Private Sub ToolButtonPreFacturar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Dim monto As Double
		Dim bandera As Boolean = False
		Dim valorVenta As Double
		Dim _salonero() As String
		Dim monedaRestaurante As Integer
		Dim PreFactura1
		If Trim(lblSalonero.Text) = vbNullString Then Exit Sub

		Try

			bandera = cConexion.SlqExecuteScalar(conectadobd, "select monedaPrefactura from hotel.dbo.configuraciones")
			If bandera = True Then
				Dim frmBuscador As New Buscar
				frmBuscador.Text = "Elija Moneda a Prefacturar"
				frmBuscador.sqlstring = "select MonedaNombre, ValorVenta from moneda"
				frmBuscador.campo = "MonedaNombre"
				frmBuscador.ShowDialog()
				If Trim(frmBuscador.descrip) <> vbNullString Then
					If frmBuscador.descrip = "CANCELAR" Then Exit Sub
					valorVenta = CDbl(frmBuscador.descrip)
					monto = CDbl(cConexion.SlqExecuteScalar(conectadobd, "select ValorVenta from moneda as m, hotel.dbo.configuraciones as h where m.codMoneda = h.OpcionFacturacion")) / valorVenta
					cConexion.SlqExecute(conectadobd, "Update configuraciones set prefactura = " & monto)
				End If
			End If

			Dim moned As String

			monedaRestaurante = CInt(cConexion.SlqExecuteScalar(conectadobd, "Select MonedaRestaurante from hotel.dbo.configuraciones"))
			moned = CDbl(cConexion.SlqExecuteScalar(conectadobd, "Select valorCompra from moneda where CodMoneda='" & monedaRestaurante & "'"))

			Dim cedula As String
			cedula = cConexion.SlqExecuteScalar(conectadobd, "SELECT Cedula FROM CONFIGURACIONES")

			If cedula = "3-101-139559" Then
				PreFactura1 = New RptPre_Factura___Finisterra
			Else
				If MsgBox("Ver en Ingles?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
					PreFactura1 = New RptPre_FacturaIngles
				Else

					PreFactura1 = New RptPre_Factura
				End If

			End If

			PreFactura1.PrintOptions.PrinterName = Me.busca_impresora
			If PreFactura1.PrintOptions.PrinterName = "" Then
				Exit Sub
			End If

			PreFactura1.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
			PreFactura1.SetParameterValue(0, numeroComanda)
			PreFactura1.SetParameterValue(1, User_Log.PuntoVenta)
			PreFactura1.SetParameterValue(2, CDbl(cmbCuentas.Text))

			_salonero = lblSalonero.Text.Split("-")

			PreFactura1.SetParameterValue(3, _salonero(0))
			'PreFactura1.SetParameterValue(4, moned)
			PreFactura1.SetParameterValue(4, valorVenta)
			PreFactura1.SetParameterValue(5, monedaRestaurante)

			'CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura, True, conectadobd.ConnectionString)
			CrystalReportsConexion.LoadReportViewer(Nothing, PreFactura1, True, conectadobd.ConnectionString)
			PreFactura1.PrintToPrinter(1, True, 0, 0)

		Catch ex As Exception
			MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		Finally
			PreFactura1 = Nothing
			If rs.IsClosed = False Then
				rs.Close()
			End If
			If bandera = True Then
				cConexion.SlqExecute(conectadobd, "Update configuraciones set prefactura = 1")
			End If
			Close()
		End Try
	End Sub

	Public Sub CargarColores()

		ListaColores.Add("IndianRed")
		ListaColores.Add("LightCoral")
		ListaColores.Add("Tomato")
		ListaColores.Add("OrangeRed")
		ListaColores.Add("Chocolate")
		ListaColores.Add("SandyBrown")
		ListaColores.Add("Goldenrod")
		ListaColores.Add("Orange")
		ListaColores.Add("Gold")
		ListaColores.Add("Yellow")
		ListaColores.Add("YellowGreen")
		ListaColores.Add("GreenYellow")
		ListaColores.Add("RosyBrown")
		ListaColores.Add("LimeGreen")
		ListaColores.Add("SpringGreen")
		ListaColores.Add("Turquoise")
		ListaColores.Add("Aqua")
		ListaColores.Add("HotPink")
		ListaColores.Add("DarkKhaki")
		ListaColores.Add("Tan")
		ListaColores.Add("NavajoWhite")
		ListaColores.Add("Lime")
		ListaColores.Add("DeepPink")
		ListaColores.Add("Crimson")
		ListaColores.Add("Magenta")
		ListaColores.Add("DarkSeaGreen")
		ListaColores.Add("PaleTurquoise")
		ListaColores.Add("CadetBlue")
		ListaColores.Add("DodgerBlue")
		ListaColores.Add("MediumVioletRed")
		ListaColores.Add("Violet")
		ListaColores.Add("Olive")
		ListaColores.Add("Coral")
		ListaColores.Add("Peru")
		ListaColores.Add("DarkOrange")
	End Sub
	Public Function RandomColor() As Color
		Dim random As New Random
		Dim ColorDevuelto As Color
		Dim MiColor As String = ""
		Dim dt As New DataTable

		Try

OtraVez:
			ColorDevuelto = Color.FromName(ListaColores(random.Next(0, 35)))
			MiColor = ColorDevuelto.ToString()
			MiColor = MiColor.Replace("Color [", "").Replace("]", "")

			If IsNumeric(MiColor) Then
				GoTo OtraVez
			End If
			Dim sql As New SqlCommand("Select IdOrden from tb_Orden with (Nolock) where Color='" & ColorDevuelto.ToString() & "' and Estado=1 ")

			cFunciones.spCargarDatos(sql, dt)

			If dt.Rows.Count > 0 Then
				GoTo OtraVez
			End If

			Return ColorDevuelto

		Catch ex As Exception
			GoTo OtraVez
		End Try

	End Function

	Private Sub ToolButtonFacturar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonFacturar.Click
		'If cedSalonero = Nothing Or cedSalonero = "" Then
		'    MessageBox.Show("Debe Seleccionar un Salonero", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information)
		'Else
		Me.cedSalonero = Me.usuario
		hacerLaFactura()
		'  End If
	End Sub

	Sub hacerLaFactura()
		Try
			If PMU.Execute = False Then
				MsgBox("No tiene permiso para realizar una factura...", MsgBoxStyle.Information, "Atención...") : Exit Sub
			End If
			facturando = True
			Dim datareader As SqlClient.SqlDataReader

			cConexion.GetRecorset(cConexion.Conectar, "Select * from ComandaTemporal_Expres where cCodigo=" & numeroComanda, datareader)

			If datareader.Read = False Then
				If MessageBox.Show("No tiene artículos pendientes, Desea cerrar?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then
					datareader.Close()
					Close()
					Exit Sub
				End If
				datareader.Close()
				Exit Sub
			Else
				Dim CentroComanda As String = GetSetting("SeeSoft", "Restaurante", "CentroComanda")

				If CentroComanda.Equals("Si") Then

					Try 'CAMBIO PARA 4 MARES, LUEGO DE FACTURAR UNA COMANDA EXPRESS SE INSERTA EN EL CENTRO DE PRODUCCION, ESTO DEBIDO A QUE LOS CENTROS SE ALIMENTAN DE TABLA COMANDATEMPORAL Y ESTAS ESTAN EN COMANDATEMPORALEXPRESS
						Dim cx As New Conexion
						Dim Sql As String = ""
						Dim dtComandasExpress As New DataTable
						cFunciones.Llenar_Tabla_Generico("Select cproducto,ccantidad,cDescripcion,cPrecio,cCodigo,cMesa,Tipo_Plato,CantImp
           ,Cedula,Comandado,Descuento from comandatemporal_expres where  ccodigo=" & numeroComanda & "", dtComandasExpress)

						If dtComandasExpress.Rows.Count > 0 Then
							For b As Integer = 0 To dtComandasExpress.Rows.Count - 1
								Sql = "INSERT INTO [dbo].[ComandaTemporal]
           ([cProducto]
           ,[cCantidad]
           ,[cDescripcion]
           ,[cPrecio]
           ,[cCodigo]
           ,[cMesa]
           ,[Impreso]
           ,[Impresora]
           ,[Comenzales]
           ,[Tipo_Plato]
           ,[CantImp]
           ,[Cedula]
           ,[Habitacion]
           ,[primero]
           ,[separada]
           ,[hora]
           ,[Express]
           ,[cantidad]
           ,[costo_real]
           ,[comandado]
           ,[descuento]
           ,[Nombre]
           ,[solicitud]
           ,[despachado]
           ,[horaDespachado]
           ,[Estado]) Values 
			(" & dtComandasExpress.Rows(b).Item("cProducto") & ", 
			  " & dtComandasExpress.Rows(b).Item("cCantidad") & ",
'" & dtComandasExpress.Rows(b).Item("cDescripcion") & "',
" & dtComandasExpress.Rows(b).Item("cPrecio") & ",
" & dtComandasExpress.Rows(b).Item("cCodigo") & ",
" & dtComandasExpress.Rows(b).Item("cMesa") & ",0,'',0,
'" & dtComandasExpress.Rows(b).Item("Tipo_Plato") & "',
" & dtComandasExpress.Rows(b).Item("CantImp") & ",
'" & dtComandasExpress.Rows(b).Item("Cedula") & "','',0,0,GETDATE(),1,0,0,1,
" & dtComandasExpress.Rows(b).Item("Descuento") & ",'',0,0,GETDATE(),'Activo')"
								cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
								cx.DesConectar(cx.sQlconexion)
							Next

						End If


					Catch ex As Exception
						MsgBox("No se pudo agregar la comanda al centro de produccion")
					End Try
				End If
			End If

				datareader.Close()
			vuelto = 0
			If MsgBox("Desea proceder a generar la factura....?", MsgBoxStyle.YesNo, "Atención..") = MsgBoxResult.Yes Then

				ToolButtonFacturar.Enabled = False

                If conectadobd.State = ConnectionState.Open Then
					conectadobd.Close()
				End If
				' conectadobd.ConnectionString = GetSetting("SeeSoft", "Hotel", "Conexion")
				Dim dt_2 As New DataTable
				cFunciones.Llenar_Tabla_Generico("Select isnull(MAX(Num_Factura),0) as NumeroFactura from Ventas where Proveniencia_Venta= " & User_Log.PuntoVenta, dt_2)
				If dt_2.Rows.Count > 0 Then
					Dim NumeroFactura As String = dt_2.Rows(0).Item("NumeroFactura") 'cConexion.SlqExecuteScalar(conectadobd, ")
					NFactura = CInt(NumeroFactura) + 1
				End If

				conectadobd.Close()

				If todo_comandado = False Then
					Terminar_Comanda()
				Else
					ActualizaMesa()
				End If

				Me.Hide()

				CrearFactura()

				conectadobd.ConnectionString = GetSetting("SeeSoft", "Hotel", "Conexion")

				If cConexion.SlqExecuteScalar(conectadobd, "Select OpcionFacturacion from hotel.dbo.configuraciones") = "True" Then
					If TipoPago.Equals("CON") Then
						'	If TipoPedido <> 2 Then
						OpcionesdePago(NFactura)
						'	End If

					End If

				End If
				conectadobd.Close()
				conectadobd.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")

				Dim id_ventas As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Id) as Id from Ventas where Num_Factura=" & NFactura & " and Proveniencia_Venta =" & User_Log.PuntoVenta)

				Dim generarconsec As New GenerarConsecutivoHA
				'------------------------------------------------------------------
				generarconsec.crearNumeroConsecutivo(cod_cliente, id_ventas, NFactura, Now)
				'------------------------------------------------------------------

				IMPRIMIR_MEJORADO(SR, NFactura, id_ventas)

				'If SR = True Then
				'	Me.imprimir_SERVICIO_RESTAURANTE(NFactura, "espanol")
				'	'Globales.imprimir_SERVICIO_RESTAURANTE(NFactura, numeroComanda)
				'Else
				'	'Globales.ImprimirReportesInternos(NFactura)
				'	Dim TipoImpresion As Integer = TipoImpres  'cConexion.SlqExecuteScalar(conectadobd, "Select ImpFactura from conf")

				'	If TipoImpresion = 1 Then
				'		imprimir(NFactura, "espanol")
				'	ElseIf TipoImpresion = 2 Then
				'		imprimir(NFactura, "Ingles")
				'	ElseIf TipoImpresion = 3 Then
				'		If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
				'			imprimir(NFactura, "espanol")
				'		Else
				'			imprimir(NFactura, "Ingles")
				'		End If
				'	ElseIf TipoImpresion = 4 Then
				'		If MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
				'			imprimir(NFactura, "espanol")
				'		End If
				'	End If
				'End If

				numFactura = NFactura
				If (vuelto > 0) And GetSetting("SeeSoft", "Restaurante", "MostrarVuelto").Equals("1") Then
					Dim fvuelto As New Vuelto
					fvuelto.txtvuelto.Text = vuelto
					fvuelto.ShowDialog()
				End If

				'Close()
			End If

		Catch ex As Exception
			MessageBox.Show("Error al realizar la factura, " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
		End Try
	End Sub
	Private Sub spImprimirComision(ByVal _NFactura As Integer, ByVal _Impresora As String)

		Try
			If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then

				Dim cx As New Conexion
				Dim Existe As Integer = 0
				Existe = cx.SlqExecuteScalar(cx.Conectar("Hotel"), "SELECT COUNT(*) FROM tb_FD_Comision where Num_Factura = '" & _NFactura & "'")
				If Existe > 0 Then
					Dim rpt As New rptComision1
					rpt.Refresh()
					rpt.PrintOptions.PrinterName = _Impresora
					rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
					rpt.SetParameterValue(0, _NFactura)
					rpt.SetParameterValue(1, User_Log.PuntoVenta)
					rpt.PrintToPrinter(1, True, 0, 0)
				End If

			End If
		Catch ex As Exception
			MsgBox(ex.Message)
			cFunciones.spEnviar_Correo(Me.Name, "spImprimirComision", ex.Message)
		End Try

	End Sub
	Sub IMPRIMIR_MEJORADO(ByVal ServiProf As Boolean, ByVal NFactura As String, ByVal idventa As Long)
		Try
			Dim Impresora As String
			Impresora = busca_impresora()
			If Impresora = "" Then
				MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
				Impresora = busca_impresora()
				If Impresora = "" Then
					Exit Sub
				End If
			End If

			'' Cargar consecutivo provisional hacienda
			Dim conce As String = ""
			Dim dt As New DataTable
			cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo_Hacienda FROM [Hotel].[dbo].[TB_CE_ConsecutivoProvisionalHA] where  Id_Factura = " & idventa, dt)
			If dt.Rows.Count > 0 Then

				conce = dt.Rows(0).Item("Consecutivo_Hacienda")
			End If

			If ServiProf Then
				If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
					Dim factura As New cls_Facturas()
					factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, True)
					Exit Sub
				End If
				imprimir_SERVICIO_RESTAURANTE(NFactura, "espanol", idventa)
			Else

				If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
					Dim factura As New cls_Facturas()
					factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, False)
					Exit Sub
				End If

				FacturaPVE.Refresh()
				FacturaPVE.PrintOptions.PrinterName = Impresora
				FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
				FacturaPVE.SetParameterValue(0, NFactura)
				FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
				FacturaPVE.SetParameterValue(2, numeroComanda)
				FacturaPVE.SetParameterValue(3, False)

				If TipoImpres = 3 Then
					FacturaPVEIng.Refresh()
					FacturaPVEIng.PrintOptions.PrinterName = Impresora
					FacturaPVEIng.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
					FacturaPVEIng.SetParameterValue(0, NFactura)
					FacturaPVEIng.SetParameterValue(1, User_Log.PuntoVenta)
					FacturaPVEIng.SetParameterValue(2, numeroComanda)
					FacturaPVEIng.SetParameterValue(3, False)
				End If


				cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

				'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
				Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

				If nc = "1" Or nc = "2" Then
					If dt.Rows.Count > 0 Then
						FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
						FacturaPVE.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
						FacturaPVE.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
						FacturaPVE.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
						FacturaPVE.SetParameterValue(8, conce)
						If TipoImpres = 3 Then
							FacturaPVEIng.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
							FacturaPVEIng.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
							FacturaPVEIng.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
							FacturaPVEIng.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
							FacturaPVE.SetParameterValue(8, conce)
						End If
					Else
						FacturaPVE.SetParameterValue(4, "")
						FacturaPVE.SetParameterValue(5, "")
						FacturaPVE.SetParameterValue(6, "")
						FacturaPVE.SetParameterValue(7, comenzales)
						FacturaPVE.SetParameterValue(8, conce)
						If TipoImpres = 3 Then
							FacturaPVEIng.SetParameterValue(4, "")
							FacturaPVEIng.SetParameterValue(5, "")
							FacturaPVEIng.SetParameterValue(6, "")
							FacturaPVEIng.SetParameterValue(7, comenzales)
							FacturaPVE.SetParameterValue(8, conce)
						End If
					End If
				Else
					If dt.Rows.Count > 0 Then
						FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
						If TipoImpres = 3 Then
							FacturaPVEIng.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
						End If
					Else
						FacturaPVE.SetParameterValue(4, 0)
						If TipoImpres = 3 Then
							FacturaPVEIng.SetParameterValue(4, 0)
						End If
					End If
				End If
				If GetSetting("SeeSoft", "Restaurante", "CCOFactura").Equals("") Then
					CCOFactura = ""
					SaveSetting("SeeSoft", "Restaurante", "CCOFactura", "")
				Else
					CCOFactura = GetSetting("SeeSoft", "Restaurante", "CCOFactura")
				End If

				If TipoImpres = 1 Or TipoImpres = 2 Then
					FacturaPVE.PrintToPrinter(1, True, 0, 0)
					If Not CCOFactura.Equals("") Then

						FacturaPVE.PrintOptions.PrinterName = CCOFactura
						FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						FacturaPVE.PrintToPrinter(1, True, 0, 0)

					End If
				ElseIf TipoImpres = 3 Then
					If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
						FacturaPVE.PrintToPrinter(1, True, 0, 0)
						If Not CCOFactura.Equals("") Then

							FacturaPVE.PrintOptions.PrinterName = CCOFactura
							FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
							FacturaPVE.PrintToPrinter(1, True, 0, 0)

						End If
						spImprimirComision(NFactura, Impresora)
					Else
						FacturaPVEIng.PrintToPrinter(1, True, 0, 0)
						If Not CCOFactura.Equals("") Then

							FacturaPVE.PrintOptions.PrinterName = CCOFactura
							FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
							FacturaPVE.PrintToPrinter(1, True, 0, 0)

						End If
						spImprimirComision(NFactura, Impresora)
					End If
				ElseIf TipoImpres = 4 Then
					Dim PreguntaImprimeFactura As String = GetSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura")
					Dim Imprime As Boolean = True
					If PreguntaImprimeFactura.Equals("") Then
						SaveSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura", 1)
					End If
					If PreguntaImprimeFactura.Equals("1") Then
						If Not MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
							Imprime = False
						End If
					End If
					If Imprime Then
						FacturaPVE.PrintToPrinter(1, True, 0, 0)
						If Not CCOFactura.Equals("") Then

							FacturaPVE.PrintOptions.PrinterName = CCOFactura
							FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
							FacturaPVE.PrintToPrinter(1, True, 0, 0)

						End If
						spImprimirComision(NFactura, Impresora)
					End If


				End If
			End If
		Catch ex As Exception
			cFunciones.spEnviar_Correo(Me.Name, "IMPRIMIR_MEJORADO", ex.Message)
		End Try
	End Sub

	Public Sub imprimir_SERVICIO_RESTAURANTE(ByVal NFactura As Integer, ByVal Idioma As String, ByVal idventa As Integer)
		Try
			Dim Impresora As String
			Impresora = busca_impresora()
			If Impresora = "" Then
				MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
				Impresora = busca_impresora()
				If Impresora = "" Then
					Exit Sub
				End If
			End If

			'' Cargar consecutivo provisional hacienda
			Dim conce As String = ""
			Dim dt As New DataTable
			cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo_Hacienda FROM [Hotel].[dbo].[TB_CE_ConsecutivoProvisionalHA] where  Id_Factura = " & idventa, dt)
			If dt.Rows.Count > 0 Then

				conce = dt.Rows(0).Item("Consecutivo_Hacienda")
			End If

			If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
				FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
			ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
				FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
			End If
			CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVESR, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

			FacturaPVESR.Refresh()
			FacturaPVESR.PrintOptions.PrinterName = Impresora
			FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
			FacturaPVESR.SetParameterValue(0, NFactura)
			FacturaPVESR.SetParameterValue(1, User_Log.PuntoVenta)
			FacturaPVESR.SetParameterValue(2, numeroComanda)
			FacturaPVESR.SetParameterValue(3, False)

			cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

			'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
			Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

			If nc = "1" Or nc = "2" Then
				If dt.Rows.Count > 0 Then
					FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
					'  FacturaPVESR.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
					'  FacturaPVESR.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
					'  FacturaPVESR.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))

				Else
					FacturaPVESR.SetParameterValue(4, "0")
					'  FacturaPVESR.SetParameterValue(5, "")
					'   FacturaPVESR.SetParameterValue(6, "")
					' FacturaPVESR.SetParameterValue(7, Comenzales)
				End If
			Else
				If dt.Rows.Count > 0 Then
					FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
				Else
					FacturaPVESR.SetParameterValue(4, 0)
				End If
			End If
			FacturaPVESR.SetParameterValue(5, conce)
			If GetSetting("SeeSoft", "Restaurante", "CCOFactura").Equals("") Then
				CCOFactura = ""
				SaveSetting("SeeSoft", "Restaurante", "CCOFactura", "")
			Else
				CCOFactura = GetSetting("SeeSoft", "Restaurante", "CCOFactura")
			End If

			If TipoImpres = 1 Or TipoImpres = 2 Then
				FacturaPVESR.PrintToPrinter(1, True, 0, 0)
				If Not CCOFactura.Equals("") Then

					FacturaPVESR.PrintOptions.PrinterName = CCOFactura
					FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
					FacturaPVESR.PrintToPrinter(1, True, 0, 0)

				End If
			ElseIf TipoImpres = 3 Then
				If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
					FacturaPVESR.PrintToPrinter(1, True, 0, 0)
					If Not CCOFactura.Equals("") Then

						FacturaPVESR.PrintOptions.PrinterName = CCOFactura
						FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						FacturaPVESR.PrintToPrinter(1, True, 0, 0)

					End If
				Else
					FacturaPVESR.PrintToPrinter(1, True, 0, 0)
					If Not CCOFactura.Equals("") Then

						FacturaPVESR.PrintOptions.PrinterName = CCOFactura
						FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						FacturaPVESR.PrintToPrinter(1, True, 0, 0)

					End If
				End If
			ElseIf TipoImpres = 4 Then
				Dim PreguntaImprimeFactura As String = GetSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura")
				Dim Imprime As Boolean = True
				If PreguntaImprimeFactura.Equals("") Then
					SaveSetting("SeeSoft", "Restaurante", "PreguntaImprimeFactura", 1)
				End If
				If PreguntaImprimeFactura.Equals("1") Then
					If Not MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
						Imprime = False
					End If
				End If
				If Imprime Then
					FacturaPVESR.PrintToPrinter(1, True, 0, 0)
					If Not CCOFactura.Equals("") Then

						FacturaPVESR.PrintOptions.PrinterName = CCOFactura
						FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						FacturaPVESR.PrintToPrinter(1, True, 0, 0)

					End If
				End If
			End If
		Catch ex As Exception
			cFunciones.spEnviar_Correo(Me.Name, "imprimir_SERVICIO_RESTAURANTE", ex.Message)
		End Try

	End Sub

	Private Sub CrearFactura()
		Try

			Dim dtmoneda As New DataTable
			cFunciones.Llenar_Tabla_Generico("SELECT m.MonedaNombre, m.CodMoneda, m.ValorCompra" &
			" FROM dbo.Moneda m INNER JOIN dbo.Configuraciones r ON m.CodMoneda = r.MonedaRestaurante", dtmoneda)

			If (ToolStripButtonCredito.BackColor = Color.Yellow) Then
				TipoPago = "CRE"
			Else
				If Descuento = 0 Then
					Me.cod_cliente = 0
					'Me.nombreCliente = "CLIENTE CONTADO"
				End If
				TipoPago = "CON"
			End If

			Factura("SELECT ComandaTemporal_Expres.*, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ComandaTemporal_Expres.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandaTemporal_Expres.cProducto = 0 THEN ComandaTemporal_Expres.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandaTemporal_Expres.Express + 1,  2, 0) ELSE ComandaTemporal_Expres.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandaTemporal_Expres.Express + 1,  2, 0) END) AS IServicios  FROM Configuraciones CROSS JOIN  ComandaTemporal_Expres WITH (NOLOCK)  LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal_Expres.cProducto = Menu_Restaurante.Id_Menu left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE (ComandaTemporal_Expres.cPrecio>0) and (ComandaTemporal_Expres.cCodigo = " & numeroComanda & ")", "ComandaTemporal_Expres")
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub
    Private Sub Factura(ByVal str As String, ByVal tabla As String)
        Try
            Dim Mesa As String
            Dim cero As Integer = 0
            'SE PASA LA COMANDA TEMPORAL A LA TABLA COMANDA Y SUS DETALLES
            If conectadobd.State <> ConnectionState.Closed Then conectadobd.Close()

            conectadobd.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            Dim dtmoneda As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT m.MonedaNombre, m.CodMoneda, m.ValorCompra" &
            " FROM dbo.Moneda m INNER JOIN dbo.Configuraciones r ON m.CodMoneda = r.MonedaRestaurante", dtmoneda)

            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            cConexion.AddNewRecord(conectadobd, "Comanda", "NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura, Cod_Moneda", numeroComanda & ", GetDate()," & CDbl(txtSubTotal.Text) & "," & CDbl(Me.txtIVentas.Text) & "," & CDbl(txtTotal.Text) & ",'" & Me.usuario & "','" & Me.cedSalonero & "'," & idMesa & "," & comenzales & ",0,0," & dtmoneda.Rows(0).Item("CodMoneda"))
            Mesa = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Mesa From Mesas Where Id=" & idMesa & "")
			Dim idc As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Idcomanda) from Comanda")
			Dim subt, iv, iss As Double
			Dim IdDetalle As Integer = 0
            Dim Dt As New DataSet
            Dim row As DataRow
            Dim Hora As Date
            Dim preciou As Decimal
            conectadobd = cConexion.Conectar()
            cConexion.GetDataSet(conectadobd, str, Dt, tabla)
            Dim ISH_IS As Double = User_Log.ISH + User_Log.ISS

            Dim ValorMonedaC As Double = dtmoneda.Rows(0).Item("ValorCompra")


            For Each row In Dt.Tables(tabla).Rows
                If Trim(row("Tipo_Plato")) = "Principal" Then
                    preciou = ((row("cPrecio")))
                    'If separada = False Then
                    subt = row("cCantidad") * preciou
                    'Else
                    '    subt = row("CantSep") * preciou
                    'End If
                    iv = 0
                    iss = 0
                    Hora = row("Hora")
                    iv = ((row("IVentas")))
                    iss = ((row("IServicios")))
                    Dim CedulaS As String = row("Cedula")
                    If (CedulaS = Nothing) Then CedulaS = 1 'Para las comandas que fueron creadas con la version anterior a esta.
                    cConexion.SlqExecute(conectadobd, "Insert Into DetalleMenuComanda (IdComanda, Idmenu,Ced_Salonero, Cantidad, PrecioUnitario, Subtotal, Impventa, Impservicio, costo_real,Hora) values(" & idc & "," & row("cProducto") & ",'" & CedulaS & "'," & row("cCantidad") & "," & preciou & "," & subt & "," & iv & "," & iss & "," & row("costo_real") & ",'" & row("hora") & "')")
                    IdDetalle = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(IdDetalle) AS IdDetalle from DetalleMenuComanda where IdComanda =" & idc)

                ElseIf Trim(row("Tipo_Plato")) = "Modificador" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into Detalle_ModificadoresComanda (IdDetalle, IdModificador) values (" & IdDetalle & "," & row("cProducto") & ")")
                ElseIf Trim(row("Tipo_Plato")) = "Acompañamiento" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into DetalleAcompañamiento_Comanda (IdDetalle, IdAcompañamiento) values (" & IdDetalle & "," & row("cProducto") & ")")
                End If

            Next

            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")

            'INGRESO LOS DATOS DE LA FACTURA A LA TABLA VENTAS
            'HACK VENTA_DETALLE AGREGAR PRECIO DE COSTO
            'EN VENTAS AGREGAR SUBTOTALGRAVADO, SUBTOTALEXCENTO

            conectadobd = cConexion.Conectar()
            '   Dim NumeroFactura As String = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(MAX(Num_Factura),0) as NumeroFactura from Ventas where Proveniencia_Venta= " & User_Log.PuntoVenta)
            'numerofactura es el max numero de factura correspondiente al punto de venta seleccionado
            'campos es el string que contiene los campos de la tabla ventas de hotel
            Dim campos As String = "Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento, Transporte, Tipo_Cambio, Monto_ICT, Id_Reservacion, Monto_Saloero, Proveniencia_Venta, Huesped, Descripcion, TipoCambioDolar,IdCliente,ExtraPropina,Mesa"
            'datos es el string que contiene los datos de la tabla ventas de hotel
            ' NumeroFactura += 1
            'psv
            Dim datos As String

            Dim Gravado As Double
            Dim Excento As Double
            Dim totalDescuento As Double = CDbl(txtSubTotal.Text) * Me.Descuento / 100
            Gravado = Math.Round(IIf(Me.txtIVentas.Text <> 0, CDbl(txtSubTotal.Text), 0), 2)
            Excento = Math.Round(IIf(Me.txtIVentas.Text = 0, CDbl(txtSubTotal.Text), 0), 2)
            'En caso de que alguno de los 2 montos sea negativo lo cual nunca deberia pasar, pero pasa cuando el desc es 100%
            If Excento < 0 Then
                Excento = 0
            End If
            If Gravado < 0 Then
                Gravado = 0
            End If
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()

			If Not TextBoxNombreCliente.Text.Equals("") Then
				EsParaLlevar = False
			End If

			If Not Me.nombreClienteexpress.Equals("") Then
				Me.nombreCliente = Me.nombreClienteexpress
				If cedulaexpress.Equals("0") Then
					cod_cliente = 0
				Else
					cod_cliente = Me.cod_Clienteexpress
				End If
			End If

			If MessageBox.Show("¿Desea la factura con nombre?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

				Dim cs As New ClienteCorreo.ClienteSeleccionado
				If MessageBox.Show("¿Desea guardar el nombre?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
					cs = ClienteCorreo.ClientesHacienda.AbrirForm
				Else
					Dim frmPideNombre As New frmPedirNombreFactura
					AddOwnedForm(frmPideNombre)
					frmPideNombre.txtNombreFactura.Text = ""
					frmPideNombre.ShowDialog()
				End If
				If Not cs.Id = 0 Then
						cod_cliente = cs.Id
						nombreCliente = cs.Nombre
					End If
					'Dim buscar2 As New frmBuscarCliente()
					'buscar2.ShowDialog()
					'If buscar2.resultado = True Then
					'	cod_cliente = buscar2.pos
					'	nombreCliente = buscar2.nombre
					'End If
				End If


				If Me.nombreCliente.Equals("") Then
                cod_cliente = 0
                nombreCliente = " CLIENTE CONTADO "
            End If

            Dim NumeroFactura As String = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(MAX(Num_Factura),0) as NumeroFactura from Ventas where Proveniencia_Venta= " & User_Log.PuntoVenta)
            NumeroFactura += 1

            If NFactura <> NumeroFactura Then
                NFactura = NumeroFactura
            End If
			Dim Impuesto As Integer = cargarImpuestoIVA(cod_cliente)
			If Impuesto < 13 Then
				txtIVentas.Text = CDbl(txtSubTotal.Text) * (Impuesto / 100)
				txtTotal.Text = CDbl(txtSubTotal.Text) + CDbl(txtIVentas.Text) + CDbl(Me.txtTransporte.Text) - CDbl(txtDescuento.Text)
			End If

			datos = NumeroFactura & ",'" & TipoPago & "'," & Me.cod_cliente & ",'" & Me.nombreCliente & "',0,'" & usuario & "',0," & CDbl(txtSubTotal.Text) & "," & CDbl(totalDescuento) & "," & CDbl(Me.txtIVentas.Text) & "," & CDbl(txtTotal.Text) & ", GetDate(), GetDate(),'NINGUNO',0,0,0,0,0,0,0," & Num_apertura & ",0," & dtmoneda.Rows(0).Item("CodMoneda") & ",'" & dtmoneda.Rows(0).Item("MonedaNombre") & "','" & Me.Direccion & "','" & Me.telefonocliente & "'," & Gravado & "," & Excento & "," & Me.txtTransporte.Text & "," & ValorMonedaC & ",0," & 0 & "," & CDbl(Me.txtIServicio.Text) & "," & User_Log.PuntoVenta & ",'','" & User_Log.NombrePunto & "'," & ValorMonedaC & "," & Me.cod_cliente & "," & 0 & ",'" & Mesa & "'"    'psv"

			cConexion.AddNewRecord(conectadobd, "Ventas", campos, datos)
            Dim id_ventas As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Id) as Id from Ventas where Num_Factura=" & NumeroFactura & " and Proveniencia_Venta =" & User_Log.PuntoVenta)

            'HACER QUE INGRESE EL DETALLE DE LAS VENTAS
            Dim row2 As DataRow
            Dim precio As Double
            campos = "Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo, Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado, SubToTalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta, Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina"

            Dim guardamodificador As Boolean = Me.GuardaModificadores
            For Each row2 In Dt.Tables(tabla).Rows
                precio = (row2("cPrecio"))
                Dim can As Decimal
				can = Math.Abs(row2("cCantidad"))
				Impuesto = cargarImpuestoIVA(cod_cliente, row2("cProducto"))
				If can <> 0 Or guardamodificador = True Then
					datos = id_ventas & "," & row2("cProducto") & ",'" & row2("cDescripcion") & "'," & can & "," & row2("costo_real") & ",0,0,0," & precio & "," & Descuento & "," & ((Descuento / 100) * row2("cPrecio")) & "," & Impuesto & "," & (can * (Impuesto / 100) * precio) & "," & (precio * can) - ((Descuento / 100) * row2("cPrecio")) & ",0," & (precio * can) & ",0,0,0,1,1,0,0," & ISH_IS & "," & (can * (ISH_IS / 100) * precio)
					cConexion.AddNewRecord(conectadobd, "Ventas_Detalle", campos, datos)
                End If
            Next

            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar()

			If TipoPago = "CON" Then
				'INDICA QUE NO SE HA COBRADO
				cConexion.UpdateRecords(conectadobd, "Comanda", "Facturado=0, Numerofactura=" & NumeroFactura, "Idcomanda=" & idc)
				'cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Estado='Facturada'", "cCodigo=" & numeroComanda)
			Else
				cConexion.UpdateRecords(conectadobd, "Comanda", "Facturado=1, Numerofactura=" & NumeroFactura, "Idcomanda=" & idc)
			End If

			'cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Comenzales=1", "cCodigo =" & numeroComanda)
			cConexion.DeleteRecords(conectadobd, "ComandaTemporal_Expres", "cCodigo=" & numeroComanda)
			If Not EsParaLlevar Then
				CrearFacturaExpress(NumeroFactura)
			End If


			''
			'Dim FacturasPendientes As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select COUNT(*) from Comanda where NumeroComanda= " & numeroComanda & " And idMesa = " & idMesa & " And (Facturado = 0)")
			'Dim ComandasPendientes As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select COUNT(*) from ComandaTemporal_Expres where cCodigo=" & numeroComanda & " And cMesa=" & idMesa)
			'Dato = cConexion.SlqExecuteScalar(conectadobd, "Select separada From ComandaTemporal Where cCodigo=" & numeroComanda & "")
			'If (Dato = Nothing) Then
			'    Dim dem As New Comanda
			'    dem.cuentas = 0
			'    cConexion.UpdateRecords(conectadobd, "Mesas", "separada=" & cero, "Id=" & idMesa)
			'Else
			'    ComandasPendientes = 1
			'End If

			'Select Case TipoPago
			'    Case "COn"
			'        cConexion.UpdateRecords(conectadobd, "Mesas", "activa=2", "Id=" & idMesa) 'cuando las pone en cobro!

			'    Case "CRE"
			'        If FacturasPendientes = 0 Then
			'            If ComandasPendientes = 0 Then
			'                cConexion.UpdateRecords(conectadobd, "Mesas", "activa=0", "Id=" & idMesa)
			'            Else
			'                cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "Id=" & idMesa)
			'            End If
			'        Else
			'            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=3", "Id=" & idMesa)
			'        End If

			'End Select

			'----------------------------------------------------------------------------
			'ASIENTO CONTABLE
			'Dim Configuracion As Boolean
			'cConexion.DesConectar(conectadobd)
			'conectadobd = cConexion.Conectar("Hotel")
			'Configuracion = cConexion.SlqExecuteScalar(conectadobd, "Select Contabilidad FROM Configuraciones")
			'If Configuracion Then
			'    If rbContado.Checked = False Then
			'        GuardaAsiento(id_ventas)
			'        If TransAsiento() = False Then
			'            MsgBox("Error Guardando el Asiento Contable", MsgBoxStyle.Exclamation, "Asientos Contables")
			'        End If
			'    End If
			'End If
			'cConexion.DesConectar(conectadobd)
			'conectadobd = cConexion.Conectar("Restaurante")
			'cConexion.DesConectar(conectadobd)
			'----------------------------------------------------------------------------
			' cedSalonero = ""
			Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

	Sub CrearFacturaExpress(ByVal NumFactura As Integer)
        Try
            Dim sql As New SqlCommand("Insert into tb_RepartidorAsignado (IdRepartidor,IdFactura,FechaAsignacion) values (0,@IdFactura,GETDATE())")

            sql.Parameters.AddWithValue("@IdFactura", NumFactura)

            cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
        Catch ex As Exception

        End Try
    End Sub

    Private Function GuardaModificadores() As Boolean
		Return General.Ejecuta("Select GuardaModificadores from conf").Rows(0).Item(0)
	End Function
	Private Sub Terminar_Comanda()
		Dim dt As New DataSet
		Dim fila As DataRow
		Try
			If (txtComanda.Text <> "") Then

				Me.ToolButtonTerminar.Enabled = False
				If GetSetting("SeeSoft", "Restaurante", "NoComanda") = "1" Then

				Else

					If MessageBox.Show("¿Desea imprimir la comanda?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then

						Dim str As String = "Select DISTINCT dbo.SplitImpresora(Impresora) As Impresora FROM ComandaTemporal_Expres WHERE (CCantidad - Impreso <> 0) And cCodigo=" & numeroComanda ' SAJ 24072007
						' cConexion.GetDataSet(conectadobd, str, dt, "ComandaTemporal_Expres")
						Dim dt_1 As New DataTable

						cFunciones.Llenar_Tabla_Generico(str, dt_1)
						For Each fila In dt_1.Rows
							'QUITAR COMENTARIO PARA Q IMPRIMA
							Dim cx As New Conexion
							cx.Conectar("Restaurante")
							cConexion.UpdateRecords(cx.sQlconexion, "ComandaTemporal_Expres", "Ccantidad = -1", "(cCodigo = " & numeroComanda & " And ccantidad = 0)") ' SAJ 09082007
							cx.DesConectar(cx.sQlconexion)

							Application.DoEvents()
							Try
								If Not fila("Impresora").Equals("") Then
									Imprime_Comanda(Verificar_Impresora_Instalada(fila("Impresora"), numeroComanda), False)
								End If

							Catch ex As Exception
								MsgBox("Problemas al imprimir la comanda, la impresora que indica el menú no ha sido encontrada", MsgBoxStyle.Information, "Atención...")
							End Try
						Next
					End If
				End If

				actualizar()
				numeroSolicitud(numeroComanda)

				cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Impreso = cCantidad", "(cCodigo = " & numeroComanda & ") And (cCantidad - Impreso <> 0)")
				todo_comandado = True
				Close()
				Me.ToolButtonTerminar.Enabled = True
			Else
				todo_comandado = True
				Close()
				Me.ToolButtonTerminar.Enabled = True
			End If
		Catch ex As Exception
			Me.ToolButtonTerminar.Enabled = True
			If rs.IsClosed = False Then rs.Close()
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
		Finally
		End Try
	End Sub
	Function numeroSolicitud(ByVal cNumero As Integer) As Integer
		Dim dt As New DataTable
		cFunciones.Llenar_Tabla_Generico("Select     ISNULL(MAX(solicitud), 0) As UltimaSolicitud FROM ComandaTemporal WHERE  (cCodigo = " & cNumero & ")", dt)
		If dt.Rows.Count > 0 Then
			Return dt.Rows(0).Item(0) + 1
		Else
			Return 0
		End If

	End Function

	Private Sub OpcionesdePago(ByVal NumFac As Integer)
		Dim nFactura As Int64

		Dim cFacturaVenta As New frmMovimientoCajaPagoAbono(User_Log.PuntoVenta)
		Dim strb As String = "Select * from BuscaFactura With (NOLOCK) where Facturado=0 And anulado=0 And Num_Factura = " & NumFac & " And Proveniencia_Venta = " & User_Log.PuntoVenta & " Order by NumeroComanda ASC"
		cConexion.GetRecorset(cConexion.Conectar(), strb, rs)
		While rs.Read
			cFacturaVenta.Factura = CDbl(rs("Num_Factura"))
			cFacturaVenta.fecha = CDate(rs("Fecha"))
			cFacturaVenta.Total = Math.Round(CDbl(rs("Total")), 2)
			cFacturaVenta.codmod = rs("Cod_Moneda")
			cFacturaVenta.Tipo = "FV"
			nFactura = cFacturaVenta.Factura
		End While
		rs.Close()


		Dim x As String
		x = cConexion.SlqExecuteScalar(cConexion.Conectar(), strb)
		Try
			If x = vbNullString Or x = Nothing Then
				Dim dt As New DataTable
				cFunciones.Llenar_Tabla_Generico("Select * From ComandaTemporal_Expres Where cMesa = " & idMesa, dt)
				If dt.Rows.Count = 0 Then
					cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0", "id=" & idMesa)
				Else

				End If


				Exit Sub
			End If
		Catch ex As Exception
		End Try
		cFacturaVenta.cedu = usuario
		cFacturaVenta.nombre = NombreUsuario
		cFacturaVenta.ShowDialog()

		If cFacturaVenta.Hecho = True Then
			Dim cancela As String = ""
			cConexion.UpdateRecords(cConexion.Conectar(), "Comanda", "Facturado=1", "idMesa=" & idMesa & " And Facturado=0 And Numerofactura=" & nFactura)
			cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Idcomanda from Comanda With (NOLOCK) where Facturado=0 And IdMesa=" & idMesa)
			If cancela = "" Then
				cancela = ""
				cancela = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Id from ComandaTemporal_Expres With (NOLOCK) where cMesa=" & idMesa)
				If cancela = "" Then
					cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=0,separada=0", "id=" & idMesa)
				Else
					cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=1", "id=" & idMesa)
				End If
			Else
				cConexion.UpdateRecords(cConexion.Conectar(), "Mesas", "activa=3", "id=" & idMesa)
			End If
			Me.vuelto = cFacturaVenta.vuelto
		End If
		cFacturaVenta.Dispose()
	End Sub

	Private Sub ToolButtonNotas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonNotas.Click
		cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres Set impreso=0 where cCodigo=" & numeroComanda)
		Dim dt As New DataSet
		Dim fila As DataRow
		Me.ToolButtonTerminar.Enabled = False
		Try
			'Dim str As String = "Select DISTINCT dbo.SplitImpresora(Impresora) As Impresora FROM ComandaTemporal_Expres WHERE (CCantidad - Impreso <> 0) And cCodigo=" & numeroComanda ' SAJ 24072007
			Dim str As String = "Select DISTINCT dbo.SplitImpresora(Impresora) As Impresora FROM ComandaTemporal_Expres WHERE  cCodigo=" & numeroComanda ' DGN 140809
			cConexion.GetDataSet(conectadobd, str, dt, "ComandaTemporal_Expres")
			For Each fila In dt.Tables("ComandaTemporal_Expres").Rows
				cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Ccantidad = -1", "(cCodigo = " & numeroComanda & " And ccantidad = 0)") ' SAJ 09082007
				Application.DoEvents()
				Imprime_Comanda(Verificar_Impresora_Instalada(fila("Impresora"), numeroComanda), True)
			Next
			cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres Set impreso=cCantidad where cCodigo=" & numeroComanda)
			actualizar()
			Close()
			Me.ToolButtonTerminar.Enabled = True

		Catch ex As Exception
			Me.ToolButtonTerminar.Enabled = True
			If rs.IsClosed = False Then rs.Close()
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
		End Try
	End Sub

	Private Sub ToolButtonReducir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonReducir.Click
		If lstProductos.SelectedItems.Count = 0 Then Exit Sub
		Dim theItem As New ListViewItem
		Dim cantidad, codigo As Integer
		theItem = lstProductos.SelectedItems(0)
		codigo = theItem.Text
		cantidad = CInt(theItem.SubItems(1).Text) - 1
		If CInt(theItem.SubItems(1).Text) <= 0 Then
			MessageBox.Show("No se puede reducir este producto", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		If cantidad = 0 Then
			MessageBox.Show("No se puede reducir mas este producto, Debe Eliminarlo", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
			Exit Sub
		Else
			If RegistrarEliminacion(codigo, "", True) Then
				cConexion.SlqExecute(conectadobd, " Update ComandaTemporal_Expres Set cCantidad=" & cantidad & " where id=" & codigo)
			End If
		End If
		'cConexion.DesConectar(cConexion.SqlConexion)

		EstadoMesaComanda(False)
		RefrescaDatos()
		ComandasShow()
		If cantidad > 0 Then lstProductos.FindItemWithText(codigo).Selected = True
		theItem = Nothing
	End Sub

	Private Sub ToolButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonEliminar.Click
		If lstProductos.SelectedItems.Count <= 0 Then Exit Sub
		Dim existencia As Boolean = False
		Dim theItem As New ListViewItem
		Dim codigos, acompa, MotivoElimina As String
		Dim i As Integer
		Dim ConexionLocal As New ConexionR
		For i = 0 To lstProductos.SelectedItems.Count - 1
			theItem = lstProductos.SelectedItems(i)
			codigos &= theItem.Text & ","
		Next
		codigos = codigos.Remove(codigos.Length - 1, 1)

		If lstProductos.SelectedItems.Count = 0 Then Exit Sub

		If MessageBox.Show("Desea eliminar los Items seleccionados..", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
			ConexionLocal.GetRecorset(ConexionLocal.Conectar(), "Select id, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Habitacion, Hora, Cantidad, Costo_Real, Impreso from ComandaTemporal_Expres where id In (" & codigos & ")", rs)
			Dim idmesa As String = ""

			While rs.Read
				idmesa = rs("cMesa")
				If rs("Impreso") > 0 Then
					Dim RowPermiso = (From RP As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where RP.Id_Usuario = usuario And RP.Modulo_Nombre_Interno = Name)
					For Each RPF As dtsGlobal.PermisosSeguridadRow In RowPermiso
						If RPF.Accion_Eliminacion = False Then
							MsgBox("No puede eliminar comandas, consulte con el administrador sobre sus privilegios", MsgBoxStyle.OkOnly)
							Exit Sub
						End If
						Exit For
					Next
					If MotivoElimina = vbNullString Then
						'------------------------------------------------------------------------------
						'MOTIVO DE LA ELIMINACION - ORA
						MotivoElimina = MotivoEliminacion()
						If MotivoElimina = vbNullString Then
							MsgBox("Tiene que espefificar el motivo para poder eliminar!!", MsgBoxStyle.Exclamation, "Atención...")
							Exit Sub
						End If
						'------------------------------------------------------------------------------
					End If
					RegistrarEliminacion(rs("id"), MotivoElimina)
				End If
				'ELIMINA LOS MODIFICADORES Y ACOMPAÑAMIENTOS ASOCIADOS A UN PLATO PRINCIPAL
				Dim dt As New DataSet
				Dim fila As DataRow
				cConexion.GetDataSet(conectadobd, "Select * from ComandaTemporal_Expres where cCodigo=" & numeroComanda, dt, "ComandaT")
				For Each fila In dt.Tables("ComandaT").Rows
					If fila("Id") > CInt(theItem.Text) Then
						If fila("cCantidad") <> 0 Or fila("cCantidad") < 0 Then
							Exit For
						Else
							If rs("Impreso") > 0 Then
								RegistrarEliminacion(fila("id"), MotivoElimina)
								acompa &= "," & fila("id")
							End If
							cConexion.SlqExecute(conectadobd, "Delete from ComandaTemporal_Expres where id=" & fila("Id"))
						End If
					End If
				Next
				EstadoMesaComanda(existencia)
            End While
            Dim CentroComandas As String = GetSetting("SeeSoft", "Restaurante", "CentroComanda")

            If CentroComandas.Equals("Si") Then
                Dim Revisar() As String
                Dim dtResivar As New DataTable
                Dim dtNumComanda As New DataTable
                Dim EnOrden As Integer = 0

                dtResivar.Clear()

                Revisar = codigos.Split(",")
                For c As Integer = 0 To Revisar.Count - 1
                    dtNumComanda.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select cCodigo from ComandaTemporal_Expres where Id=" & Revisar(c) & "", dtNumComanda)
                    cFunciones.Llenar_Tabla_Generico("Select Id from tb_DetalleOrden where NumComanda=" & dtNumComanda.Rows(0).Item("cCodigo") & "", dtResivar)
                    If dtResivar.Rows.Count > 0 Then
                        EnOrden = 1
                        Exit For
                    End If
                Next

                If EnOrden = 1 Then
                    Try
ReintentarClave:

                        Dim rEmpleado0 As New registro
                        rEmpleado0.Text = "DIGITE CONTRASEÑA"
                        rEmpleado0.txtCodigo.PasswordChar = "*"

                        '---------------------------------------------------------------
                        'VERIFICA SI PIDE O NO EL USUARIO
                        If gloNoClave Then
                            clave = User_Log.Clave_Interna
                            rEmpleado0.iOpcion = 1
                        Else
                            rEmpleado0.ShowDialog()
                            clave = rEmpleado0.txtCodigo.Text
                        End If

                        '---------------------------------------------------------------
                        rEmpleado0.Dispose()
                        If rEmpleado0.iOpcion = 0 Then Exit Sub

                        If clave <> "" Then
                            'cedula = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios With (NOLOCK) where Clave_Interna='" & clave & "'")
                            'If Trim(cedula) = "" Or Trim(cedula) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                            '    MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atención...")
                            '    GoTo ReintentarClave
                            '    Exit Sub
                            'End If
                            Dim RowPermiso = (From RP As dtsGlobal.PermisosSeguridadRow In glodtsGlobal.PermisosSeguridad Where RP.Id_Usuario = usuario And RP.Modulo_Nombre_Interno = Name)
                            For Each RPF As dtsGlobal.PermisosSeguridadRow In RowPermiso
                                If RPF.Accion_Eliminacion = False Then
                                    MsgBox("No puede eliminar comandas, consulte con el administrador sobre sus privilegios", MsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                Exit For
                            Next
                        Else
                            GoTo ReintentarClave
                        End If
                        cConexion.SlqExecute(conectadobd, "Delete from ComandaTemporal_Expres where (id in (" & codigos & "))")
                    Catch ex As Exception

                    End Try
                Else
                    cConexion.SlqExecute(conectadobd, "Delete from ComandaTemporal_Expres where (id in (" & codigos & "))")
                End If
            Else
                cConexion.SlqExecute(conectadobd, "Delete from ComandaTemporal_Expres where (id in (" & codigos & "))")
            End If


            rs.Close()
			ConexionLocal.DesConectar(ConexionLocal.SqlConexion)
		End If
		RefrescaDatos()
		ComandasShow()
		theItem = Nothing

		'------------------------------------------------------------------------------
		'IMPRIME COMPROBANTE DE ELIMINACION - ORA
		If MotivoElimina <> vbNullString Then
			Imprime_Eliminacion(codigos & acompa)
		End If
		'------------------------------------------------------------------------------

	End Sub

	Private Sub EstadoMesaComanda(ByVal Existencia As Boolean)
		Dim ConexionY As New ConexionR
		Dim ExistDatos As String = ""

		ExistDatos = ConexionY.SlqExecuteScalar(ConexionY.Conectar, "Select cMesa FROM ComandaTemporal_Expres GROUP BY cMesa HAVING cMesa=" & idMesa)
		Existencia = IIf(ExistDatos = "", False, True)

		If Existencia = False Then ConexionY.UpdateRecords(ConexionY.Conectar, "Mesas", "activa=0", "id=" & idMesa)
		If Existencia = False Then ConexionY.DeleteRecords(ConexionY.Conectar, "DatosExpress", "id_comanda = " & numeroComanda)
		ConexionY.DesConectar(ConexionY.SqlConexion)

	End Sub

	Private Sub ToolButtonCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonCerrar.Click

		ToolButtonCerrar.Enabled = False
		limpiaDatos()
		Me.ObtenerDimension()
		CrearCategoriaMenu()
		ToolButtonCerrar.Enabled = True
	End Sub

	Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonCambiarPrecio.Click
		If lstProductos.SelectedItems.Count <= 0 Then
			Exit Sub
		End If

		Dim CambioPrecios As New ModificarPrecioComanda
		CambioPrecios.TxtItem.Text = lstProductos.SelectedItems(0).SubItems(0).Text
		CambioPrecios.TxtCantidad.Text = lstProductos.SelectedItems(0).SubItems(1).Text
		CambioPrecios.TxtDescripcion.Text = lstProductos.SelectedItems(0).SubItems(2).Text
		CambioPrecios.TxtPrecio.Text = lstProductos.SelectedItems(0).SubItems(3).Text
		CambioPrecios.txtComanda.Text = lstProductos.SelectedItems(0).SubItems(4).Text
		CambioPrecios.TxtProducto.Text = lstProductos.SelectedItems(0).SubItems(5).Text

		CambioPrecios.TxtImpuestoVentas.Text = Math.Round((CambioPrecios.TxtPrecio.Text) * cFunciones.ObtenerIvaProducto(lstProductos.SelectedItems(0).SubItems(5).Text) / 100, 2)
		CambioPrecios.TxtImpuestoServicio.Text = Math.Round((CambioPrecios.TxtPrecio.Text) * User_Log.ISS / 100, 2)
		CambioPrecios.TxtPrecioFinal.Text = Math.Round(CDbl(CambioPrecios.TxtPrecio.Text) + CDbl(CambioPrecios.TxtImpuestoVentas.Text) + CDbl(CambioPrecios.TxtImpuestoServicio.Text), 2)

		CambioPrecios.ShowDialog(Me)


		If CambioPrecios.Cancelado Then
			Exit Sub
		Else
			cConexion.UpdateRecords(conectadobd, "ComandaTemporal_Expres", "Cprecio =" & CambioPrecios.TxtPrecio.Text, "id =" & CambioPrecios.TxtItem.Text)
		End If

		RefrescaDatos()
		ComandasShow()
	End Sub

	Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

		Dim theItem As New ListViewItem
		Dim codigos As String = ""
		Dim i As Integer

		For i = 0 To lstProductos.SelectedItems.Count - 1
			theItem = lstProductos.SelectedItems(i)
			codigos &= theItem.Text & ","
		Next

		If Trim(codigos) = vbNullString Then
			Exit Sub
		End If

		codigos = codigos.Remove(codigos.Length - 1, 1)
		Try
			cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres set primero=1 where id in(" & codigos & ")")
			cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres set primero=0 where id not in(" & codigos & ")")
		Catch ex As Exception
		End Try

		RefrescaDatos()
		ComandasShow()

		theItem = Nothing
	End Sub

	Private Sub cmbCuentas_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCuentas.SelectedValueChanged

		RefrescaDatos()
		PanelMesas.Visible = True
		ComandasShow()
		grpModifica.Visible = False
	End Sub

	Private Sub ToolStripButton8_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
		Dim FrmAdicionales As New FrmModificadorAdicional
		FrmAdicionales.Comanda = numeroComanda
		FrmAdicionales.ShowDialog(Me)

		If FrmAdicionales.Aceptado = True Then
			If FrmAdicionales.MontoNota > 0 Then
                cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal_Expres " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & "," & IIf(FrmAdicionales.MontoNota > 0, "'Modificador'", "'Modificador'") & ",'" & usuario & "','" & HabitacionAsignada & "',0," & cmbCuentas.Text & "," & IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0) & ", -1 ")
            Else
                cConexion.SlqExecute(conectadobd, "ActualizaTablaTemporal_Expres " & 0 & ",'**" & FrmAdicionales.Nota & "'," & FrmAdicionales.MontoNota & "," & numeroComanda & "," & idMesa & ",0,'" & impresoras & "'," & comenzales & "," & IIf(FrmAdicionales.MontoNota > 0, "'Principal'", "'Modificador'") & ",'" & usuario & "','" & HabitacionAsignada & "',0," & cmbCuentas.Text & "," & IIf(ToolButtonExpress.BackColor = Color.Yellow, 1, 0) & ", -1 ")
            End If

			'If FrmAdicionales.MontoNota > 0 Then
			'    Dim idultimoingresado As Integer = cConexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(max(id),0) FROM ComandaTemporal_Expres WITH (NOLOCK)")
			'    cConexion.SlqExecute(conectadobd, "update ComandaTemporal_Expres set cCantidad= 1 where id=" & idultimoingresado)
			'End If
			Me.ComandasShow()
		End If
	End Sub

	Private Sub tlsCantidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlsCantidad.Click
		If tlsCantidad.BackColor = Color.Yellow = True Then
			tlsCantidad.BackColor = Color.Transparent
		Else
			tlsCantidad.BackColor = Color.Yellow
		End If
	End Sub

	Private Sub ToolStripButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton9.Click
		iCategoria = 1
		NoTerminar = True
		Close()
	End Sub

	Private Sub ToolButtonExpress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolButtonExpress.Click
		Dim nombred, codigod, impre, preC As String
		Dim busqueda As New Buscar
		busqueda.sqlstring = "Select * from menuBusqueda_2"
		busqueda.campo = "Nombre"
		busqueda.tipo = True
		busqueda.ShowDialog()

		Try
			codigod = busqueda.codigo
			nombred = busqueda.descrip
			preC = busqueda.monto


			Dim iPrimero As Object
			If (codigod.Equals(Nothing) = False) Then

				iPrimero = 0

				Dim valorCambio, dmoneda As Double
				dmoneda = cConexion.SlqExecuteScalar(conectadobd, "select moneda from menu_restaurante where id_Menu =" & codigod)
				valorCambio = cConexion.SlqExecuteScalar(conectadobd, "select  (select valorVenta from moneda where codMoneda=" & dmoneda & ") / (select m.valorVenta from moneda as m, hotel.dbo.configuraciones as hc where m.codMoneda = hc.monedaRestaurante)")

				impre = cConexion.SlqExecuteScalar(conectadobd, "Select Impresora from Menu_Restaurante WITH (NOLOCK) where Id_MENU=" & codigod)

				AlmacenaTemporal(codigod, nombred, preC * valorCambio, impresoras, comenzales, usuario, HabitacionAsignada, iPrimero, False)

			End If
			Dim act As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select activa from Mesas WITH (NOLOCK) where Id=" & idMesa)
			If act = 0 Or act = 1 Then
				cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "id=" & idMesa)
			End If


			RefrescaDatos()
			ComandasShow()

		Catch ex As Exception
			Exit Sub
		End Try
	End Sub

	Private Sub ToolStripButton10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Dim theItem As New ListViewItem
		Dim codigos As String = ""
		Dim i As Integer
		Try
			If lstProductos.SelectedItems.Count = 0 Then
				cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres set Express=0 where ccodigo = " & numeroComanda)
				GoTo Fin
				Exit Sub
			End If


			For i = 0 To lstProductos.SelectedItems.Count - 1
				theItem = lstProductos.SelectedItems(i)
				codigos &= theItem.Text & ","
			Next
			codigos = codigos.Remove(codigos.Length - 1, 1)

			cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres set Express=1 where id in(" & codigos & ")")
			cConexion.SlqExecute(conectadobd, "Update ComandaTemporal_Expres set Express=0 where id not in(" & codigos & ")")
		Catch ex As Exception

		End Try

Fin:
		RefrescaDatos()
		ComandasShow()
		theItem = Nothing
	End Sub

	Private Sub lstProductos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstProductos.KeyDown
		If e.KeyValue = Keys.Delete Then
			If ToolButtonEliminar.Enabled = True Then
				ToolButtonEliminar_Click(sender, e)
			Else
				MsgBox("Usted no tiene Permisos para eliminar comandas", MsgBoxStyle.Information, "Servicios Estructurales Seesoft")
			End If
		End If
	End Sub

	Function RegistrarEliminacion(ByVal CodigoComanda As Integer, ByVal Motivo As String, Optional ByVal Reduce As Boolean = False) As Boolean
		Dim ConexionLocal As New ConexionR
		Dim rsEli As SqlClient.SqlDataReader
		RegistrarEliminacion = False

		Try
			ConexionLocal.GetRecorset(ConexionLocal.Conectar(), "Select cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Habitacion, Hora, Cantidad, Costo_Real, Impreso from ComandaTemporal_Expres where id = " & CodigoComanda, rsEli)
			Dim idmesa As String = ""

			While rsEli.Read
				If Reduce = False Then
					cConexion.SlqExecute(conectadobd, "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo) " &
														"VALUES (" & CodigoComanda & "," & rsEli("cProducto") & ", " & rsEli("cCantidad") & ", '" & rsEli("cDescripcion") & "', " & rsEli("cPrecio") & ", " & rsEli("cCodigo") & ", " & rsEli("cMesa") & ", '" & rsEli("Tipo_Plato") & "', '" & usuario & "', '" & rsEli("Habitacion") & "', '" & CDate(rsEli("Hora")).ToUniversalTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) & " " & CDate(rsEli("Hora")).ToUniversalTime.ToString("H:mm:ss") & "', GETDATE(), " & rsEli("Costo_Real") & ", '" & Motivo & "')")
					Return True
				Else
					If rsEli("Impreso") > 0 Then
						Motivo = MotivoEliminacion()
						If Motivo = vbNullString Then
							MsgBox("Tiene que espefificar el motivo para poder reducir!!", MsgBoxStyle.Exclamation, "Atención...")
							Return False
						End If
						cConexion.SlqExecute(conectadobd, "Insert Into Comanda_Eliminados (Id_Temporal, cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Tipo_Plato, Cedula, Habitacion, Hora, HoraEliminacion, Costo_Real, Motivo) " &
																				"VALUES (" & CodigoComanda & "," & rsEli("cProducto") & ", " & 1 & ", '" & rsEli("cDescripcion") & "', " & rsEli("cPrecio") & ", " & rsEli("cCodigo") & ", " & rsEli("cMesa") & ", '" & rsEli("Tipo_Plato") & "', '" & usuario & "', '" & rsEli("Habitacion") & "', '" & CDate(rsEli("Hora")).ToUniversalTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) & "', GETDATE() , " & rsEli("Costo_Real") & ", '" & Motivo & "')")
						'------------------------------------------------------------------------------
						'IMPRIME COMPROBANTE DE ELIMINACION - ORA
						Imprime_Eliminacion(CodigoComanda)
						'------------------------------------------------------------------------------
					End If
					Return True
				End If
			End While

		Catch ex As Exception
			MsgBox(ex.Message)
			Return False
		Finally
			If rsEli.IsClosed = False Then rsEli.Close()
			ConexionLocal.DesConectar(ConexionLocal.SqlConexion)
		End Try
	End Function

	Private Sub Imprime_Eliminacion(ByVal CodigosImpri As String)
		Dim rptElimina As New rptComandaEliminados
		Dim Impresora As String

		Try
			Impresora = busca_impresora()
			If Impresora = "" Then
				MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
				Impresora = busca_impresora()
				If Impresora = "" Then
					Exit Sub
				End If
			End If

			CrystalReportsConexion.LoadReportViewer(Nothing, rptElimina, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
			rptElimina.PrintOptions.PrinterName = Impresora
			rptElimina.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
			rptElimina.RecordSelectionFormula = "{Comanda_Eliminados.Id_Temporal} in [" & CodigosImpri & "]"
			rptElimina.SetParameterValue(0, User_Log.NombrePunto)
			rptElimina.PrintToPrinter(1, True, 0, 0)

		Catch ex As Exception
			MsgBox(ex.Message)
		Finally
			rptElimina.Dispose()
			rptElimina.Close()
			rptElimina = Nothing
		End Try
	End Sub

	Function MotivoEliminacion() As String
		Dim Motivo As New FrmCortesia
		Try
			'------------------------------------------------------------------------------
			'MOTIVO DE LA ELIMINACION - ORA
			Motivo.Text = "Eliminar Articulos de Comanda"
			Motivo.TituloModulo.Text = "Elimina Comanda"
			Motivo.Label13.Visible = False
			Motivo.txtautorizado.Visible = False
			Motivo.txtautorizado.Text = "-"
			Motivo.Label1.Text = "Motivo de la Eliminación : "
			Motivo.ShowDialog()

			If Motivo.completa = False Then
				Return ""
			Else
				Return Motivo.observaciones
			End If

			'------------------------------------------------------------------------------
		Catch ex As Exception
			MsgBox(ex.Message)
			Return ""
		Finally
			Motivo.Dispose()
		End Try
	End Function

	Private Sub ActualizaMesa()
		Dim ActivoMesa As Integer
		Dim Conectadobdtem As New SqlClient.SqlConnection
		Dim cConexion2 As New Conexion

		Try
			'ACTUALIZA EL ESTADO DE LA MESA, EN CASO DE QUE QUEDE VACIA - ORA
			cConexion.GetRecorset(conectadobd, "Select Distinct(cMesa) from ComandaTemporal_Expres WITH (NOLOCK) where cCodigo=" & numeroComanda, rs)
			Conectadobdtem = cConexion2.Conectar("Restaurante")
			ActivoMesa = cConexion2.SlqExecuteScalar(Conectadobdtem, "Select activa from mesas where id = " & idMesa)
			cConexion2.DesConectar(Conectadobdtem)
			'Si la mesa está desactivada no tiene que realizar nada DGN 180809
			If ActivoMesa = 0 Then Exit Sub

			If rs.Read = False Then
				rs.Close()
				If ActivoMesa = 1 Then
					cConexion.UpdateRecords(conectadobd, "Mesas", "activa=0", "Id=" & idMesa)
				End If
			End If
		Catch ex As Exception
			MsgBox(ex.Message)
		Finally
			If rs.IsClosed = False Then rs.Close()
		End Try
	End Sub

	Private Sub Bloquear(ByVal estado As Boolean)
		Try
			ToolStrip1.Enabled = estado
			cmbCuentas.Enabled = estado
			Me.ControlBox = estado

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub ToolStripButtonCredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonCredito.Click
		If ToolStripButtonCredito.BackColor = Color.Yellow = True Then
			ToolStripButtonCredito.BackColor = Color.DarkGray
			cod_cliente = 0
			nombreCliente = "CLIENTE CONTADO"
			TipoPago = "CON"
		Else
			Dim credito As New ClienteCredito
			credito.ShowDialog()

			If credito.id <> "" Or credito.nombre <> "" Then
				cod_cliente = credito.id
				nombreCliente = credito.nombre
				TipoPago = "CRE"
			Else
				cod_cliente = 0
				nombreCliente = "CLIENTE CONTADO"
				TipoPago = "CON"
				ToolStripButtonCredito.BackColor = Color.DarkGray
				Exit Sub
			End If
			credito.Dispose()
			Text = "Pedido Express - " & nombreCliente
			ToolStripButtonCredito.BackColor = Color.Yellow
		End If
	End Sub

	Private Sub ToolStripButtonAplicarDescuento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonAplicarDescuento.Click

		If MessageBox.Show("Desea Escoger el Cliente..", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
			Dim credito As New ClienteCredito
			credito.desdedescuento = True
			credito.ShowDialog()
			Descuento = credito.montodescuento
			cod_cliente = credito.id
			nombreCliente = credito.nombre
			Me.Text = "Pedido Express - " & nombreCliente & " "
			ToolStripButtonAplicarDescuento.BackColor = Color.Yellow
			ToolStripButtonAplicarDescuento.Text = "Desc(" & Descuento & ")"
			AplicarDescuentoComandas()
		Else
			Dim descuennto As Double
			Dim rDesc As New registro
			rDesc.Text = "Digite el descuento a aplicar"
			If Descuento > 0 Then
				rDesc.txtCodigo.Text = Descuento
			End If
			rDesc.ShowDialog()
			Try
				descuennto = CDbl(rDesc.txtCodigo.Text)
			Catch ex As Exception
				descuennto = 0
			End Try
			rDesc.Dispose()

			Descuento = descuennto

			If Descuento = 0 Then
				ToolStripButtonAplicarDescuento.BackColor = Color.Transparent
				ToolStripButtonAplicarDescuento.Text = "Descuento"
				AplicarDescuentoComandas()
			Else
				Dim maximo As Double
				maximo = maximodescuentosalonero()
				If Descuento > maximo Then
					MsgBox("Ud no puede hacer un descuento mayor a " & maximo & "%", MsgBoxStyle.Exclamation, "Atención...")
				Else
					ToolStripButtonAplicarDescuento.BackColor = Color.Yellow
					ToolStripButtonAplicarDescuento.Text = "Desc(" & Descuento & ")"
					AplicarDescuentoComandas()
				End If
			End If
		End If

		ComandasShow()

	End Sub

#Region "Aplicar Descuento"
	Sub AplicarDescuentoComandas()
		Dim secuencia As String = "Update ComandaTemporal_Expres " &
									" SET descuento = " & Descuento & "" &
									" WHERE cCodigo=" & numeroComanda & " and separada= " & cmbCuentas.Text
		Dim cx As New Conexion
		cx.SlqExecute(cx.Conectar(), secuencia)
		cx.DesConectar(cx.sQlconexion)
	End Sub

#End Region

#Region "Maximo descuento salonero"
	Function maximodescuentosalonero() As Double
		Try
			Dim Reader As System.Data.SqlClient.SqlDataReader
			Dim Cx As New Conexion
			Dim montodescuento As Double = 0
			Reader = Cx.GetRecorset(Cx.Conectar("Seguridad"), "Select Porc_Desc from Usuarios WITH (NOLOCK) where Id_Usuario='" & usuario & "'")
			If Reader.Read() Then
				montodescuento = Reader("Porc_Desc")
				Return montodescuento
			Else
				Return montodescuento
			End If

		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
		End Try


	End Function
#End Region

	Private Sub ButtonCambiaSalonero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCambiaSalonero.Click
		asignarSalonero()
	End Sub

	Sub asignarSalonero()
		Dim funcion As New cFunciones
		Dim Id As String
		cedSalonero = funcion.BuscarDatos("SELECT Cedula,Nombre FROM Saloneros", "Nombre", "Buscar Usuario ...", GetSetting("SeeSoft", "´Seguridad", "Conexion"))
		If Not Id Is Nothing Then
			Dim cx As New Conexion
			cx.Conectar("Restaurante")
			cx.SlqExecute(cx.sQlconexion, "UPDATE ComandaTemporal_Expres set Cedula = '" & Id & "' Where cCodigo = " & Me.numeroComanda)
			cx.DesConectar(cx.sQlconexion)
			ComandasShow()
		End If


	End Sub

	Private Sub ToolStripButtonNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonNombre.Click
		ToolStripButtonNombre.BackColor = Color.Yellow
		Me.GroupBoxNombre.Visible = True
		Me.TextBoxNombreCliente.Text = Me.nombreCliente
		TextBoxNombreCliente.Focus()

	End Sub

	Sub guardarNombre(ByVal Nombre As String)
		Me.nombreCliente = Nombre
		If Not Me.nombreCliente.Equals("") Then

			Dim secuencia As String = "Update ComandaTemporal_Expres " &
														" SET Nombre = '" & Me.nombreCliente & "'" &
														" WHERE cCodigo=" & numeroComanda & " and ( separada = " & cmbCuentas.Text & " or separada = 0 )"
			Dim cx As New Conexion
			cx.SlqExecute(cx.Conectar(), secuencia)
			cx.DesConectar(cx.sQlconexion)
			ToolStripButtonNombre.BackColor = Color.Yellow

		Else
			ToolStripButtonNombre.BackColor = Color.Transparent

		End If

		Me.GroupBoxNombre.Visible = False

	End Sub

	Private Sub ButtonGuardarNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGuardarNombre.Click
		Me.guardarNombre(Me.TextBoxNombreCliente.Text)
	End Sub

	Private Sub ButtonSalirNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalirNombre.Click
		Me.GroupBoxNombre.Visible = False

		If Not Me.nombreCliente.Equals("") Then
			Me.ToolStripButtonNombre.BackColor = Color.Yellow

		Else
			Me.ToolStripButtonNombre.BackColor = Color.Transparent

		End If

	End Sub

	Private Sub ToolStripButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Try
			Dim datareader As SqlClient.SqlDataReader

			cConexion.GetRecorset(cConexion.Conectar, "Select * from ComandaTemporal_Expres where cCodigo=" & numeroComanda, datareader)

			If datareader.Read = False Then
				If MessageBox.Show("No tiene artículos pendientes, Desea cerrar?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then
					datareader.Close()
					Close()
					Exit Sub
				End If
				datareader.Close()
				Exit Sub
			Else
			End If
		Catch ex As Exception

		End Try

	End Sub

	Private BANDERA_MEDIO As Boolean

    Private Sub cmbMesas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMesas.SelectedIndexChanged
        Try
            If PrimeraVez <> 0 Then
                idMesa = cmbMesas.SelectedValue
            End If
        Catch ex As Exception
            idMesa = 0
        End Try
    End Sub

    Private Sub ToolStripButtonVolver_Click(sender As Object, e As EventArgs) Handles ToolStripButtonVolver.Click
        Try
            Dim Titulo As String = lblEncabezado.Text
            Select Case Titulo

                Case "Menú" 'SELECCIONA UNA CATEGORIA DEL MENÚ, CARGANDO LOS PLATOS O DATOS DE ESA CATEGORIA
                    limpiaDatos()
                    ObtenerDimension()
                    CrearCategoriaMenu()
                    cmbCuentas.Enabled = True
                Case "Modificadores*"
                    Dim iPrimero As Integer
                    iPrimero = 0
					'      If tieneAcompa = 1 Then 'Si especifica modificadores y acompañamientos
					limpiaDatos()
                        idMenuActivo = 0
					UbicarPosiciones("SELECT Id_Menu, Nombre_Menu,posicion,ImagenRuta,tipo='MENUREST',Precio_VentaExpressA FROM Menu_Restaurante WITH (NOLOCK) where Id_Categoria =" & idGrupoT)
					lblEncabezado.Text = "Menú"
                        Bloquear(True)

                        Me.bandeTipo = False
                        Me.ControlBox = True

                        Me.ToolButtonModificarComanda.Visible = True
                        Me.ToolButtonRegresar.Visible = True
                        Me.ToolStripButton9.Visible = usaGrupo
                        Me.ToolStripButton8.Visible = True
                        Me.tlsCantidad.Visible = True
                        Me.ToolButtonNotas.Visible = True
                        Me.ToolButtonExpress.Visible = True
                        Me.ToolButtonFacturar.Visible = True
                        Me.ToolStripButtonAcumular.Visible = True
                        Me.ToolStripButtonTerminar.Enabled = True
                        ToolButtonTerminar.Visible = True
					'Else
				'	tieneAcompa = tieneAcompa - 1

				   ' End If

				Case "Acompañamientos"
                    limpiaDatos()

                    Dim dt As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " AS Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion", dt)
                    If dt.Rows.Count > 0 Then
                        UbicarPosiciones("SELECT id,nombre,MOD_AGREGADO.posicion,imagenRuta,tipo='FORZADOS',MOD_AGREGADO.precio * " & Me.current_Cant & " as Precio_VentaA FROM  Modificadores_Forzados, dbo.MOD_AGREGADO WHERE dbo.MOD_AGREGADO.IdCategoriaMenu = " & Id_Categoria & " AND MOD_AGREGADO.IdAgregado = Modificadores_Forzados.Id  Order by dbo.MOD_AGREGADO.posicion")
                        dt.Rows.Clear()
                    Else
                        UbicarPosiciones("SELECT id,nombre,posicion,imagenRuta,tipo='FORZADOS',Precio_VentaA=0 FROM  Modificadores_Forzados WITH (NOLOCK) Order by posicion")
                    End If
                    lblEncabezado.Text = "Modificadores*"
                    ToolStripButtonTerminar.Visible = True
            End Select

            PanelMesas.Focus()

        Catch ex As Exception
            MessageBox.Show("Error:" & ex.ToString, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub ToolStripButtonAcumular_Click(sender As Object, e As EventArgs) Handles ToolStripButtonAcumular.Click
        Try
            Dim confirmacion As New frmQuestion
            With confirmacion
                .LblMensaje.Text = "Desea Generar la Factura Acomulada ?"
                .ShowDialog()
                If .resultado = True Then
                    ''guardarComandaEnAcumulado()
                    Dim f As New frmAcumular
                    Dim c As New cls_facturasAcumuladas
                    Dim nom_mesa As String
                    Dim _IServicio As Double = 0
                    Dim _IVenta As Double = 0

                    If idMesa <> 0 Then
                        nom_mesa = c.nombreMesa(idMesa)
                    Else
                        nom_mesa = "0"
                    End If
                    Try
                        Dim dt As New DataTable
                        Dim sql As New SqlClient.SqlCommand


						sql.CommandText = "Select top 1 Configuraciones.Imp_Venta, Configuraciones.Imp_Servicio from Configuraciones"
						cFunciones.spCargarDatos(sql, dt)
                        If dt.Rows.Count > 0 Then
                            _IServicio = Convert.ToDouble(dt.Rows(0).Item("Imp_Servicio"))
                            _IVenta = Convert.ToDouble(dt.Rows(0).Item("Imp_Venta"))
                        Else
                            _IServicio = 10
                            _IVenta = 13
                        End If
                    Catch ex As Exception
                        _IServicio = 10
                        _IVenta = 13
                    End Try
                    Acumular = 1
                    f.Transporte = Convert.ToDouble(txtTransporte.Text)
                    f.SoyExpress = 1
                    f.NumComanda = numeroComanda
                    f.Telefono = telefonocliente
                    f.ReciveDatos(txtSubTotal.Text, txtIVentas.Text, txtIServicio.Text, txtDescuento.Text, txtTotal.Text, nom_mesa, fnObtenerCuenta(Me.cmbCuentas.Text), 0, _IVenta, 1)
                    f.ShowDialog()
                    Dim frm As Form = Application.OpenForms.Cast(Of Form)().FirstOrDefault(Function(x) TypeOf x Is FormCuentasALlevar)
                    Dim _frm As New FormCuentasALlevar
                    _frm = frm
                    _frm.Acumular = 1
                    Me.Close()
                End If
            End With
        Catch ex As Exception
            'cFunciones.spEnviar_Correo(Me.Name, "ToolStripButton11_Click", ex.Message)
        End Try
    End Sub

    Private Function fnObtenerCuenta(ByVal _Cuenta As String) As String
        Try
            Dim vector() As String
            vector = _Cuenta.Split("-")
            Return vector(0)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "fnObtenerCuenta", ex.Message)
        End Try
    End Function

    Private Sub ToolStripButton12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton12.Click
        If ToolStripButton12.BackColor = Color.Blue = True Then
            ToolStripButton12.BackColor = Color.Transparent
            Me.BANDERA_MEDIO = False
        Else
            ToolStripButton12.BackColor = Color.Blue
            Me.BANDERA_MEDIO = True
        End If
    End Sub


	Private Sub txtTransporte_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransporte.TextChanged
		spCalcular()
	End Sub

	Private Sub spCalcular()
		Try
			'Dim IVentas As Double = Me.txtIVentas.Text
			'Dim IServicios As Double = Me.txtIServicio.Text
			Dim subTotalTodo As Double = Me.txtSubTotal.Text
			Dim MontoDescuento As Double = IIf(IsNumeric(txtDescuento.Text), Me.txtDescuento.Text, 0)
			Dim ImpVenta As Double = 0
			Dim ImpServicio As Double = 0
			Dim dtConfig As New DataTable

			cFunciones.Llenar_Tabla_Generico("Select top 1 Imp_Venta, Imp_Servicio from Configuraciones", dtConfig, GetSetting("SeeSOFT", "Hotel", "Conexion"))

			If dtConfig.Rows.Count > 0 Then
				ImpVenta = dtConfig.Rows(0).Item("Imp_Venta")
				ImpServicio = dtConfig.Rows(0).Item("Imp_Servicio")
			End If
			'Dim totalDescuento As Double = 0
			'Dim totaDescIServ As Double = 0
			'Dim totaDescIVenta As Double = 0
			'If Descuento > 0 Then
			'    totalDescuento = subTotalTodo * (Descuento / 100)
			'    totaDescIServ = IServicios * (Descuento / 100)
			'    totaDescIVenta = IVentas * (Descuento / 100)
			'End If
			' txtSubTotal.Text = Format(subTotalTodo, "###,##0.00")
			' txtDescuento.Text = Format(totalDescuento, "###,##0.00")

			If GetSetting("SeeSOFT", "Restaurante", "IServicioExpress", "0").Equals("0") Then
				txtIServicio.Text = 0 'Format(IServicios - totaDescIServ, "###,##0.00")
				'Else
				'    txtIServicio.Text = Format(IServicios - totaDescIServ, "###,##0.00")
			End If
			txtIVentas.Text = Format((subTotalTodo - MontoDescuento) * (ImpVenta / 100), "###,##0.00") 'Cambiar por los datos de configuracion
			' txtIServicio.Text = Format((subTotalTodo - MontoDescuento) * 0.1, "###,##0.00") 'Cambiar por los datos de configuracion
			txtTotal.Text = Format((subTotalTodo - MontoDescuento) + IIf(IsNumeric(txtTransporte.Text), Me.txtTransporte.Text, 0) + txtIServicio.Text + txtIVentas.Text, "###,##0.00")
		Catch ex As Exception

		End Try
	End Sub

	Public SR As Boolean
	Private Sub ToolStripButton6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
		If Me.ToolStripButton6.BackColor = Color.Transparent Then
			Me.ToolStripButton6.BackColor = Color.Blue
			SR = True
		Else
			Me.ToolStripButton6.BackColor = Color.Transparent
			SR = False
		End If
	End Sub

	Private Sub txtDescuento_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescuento.TextChanged
		spCalcular()
	End Sub

	Private Function cargarImpuestoIVA(ByVal CodigoCliente As Integer, Optional CodigoArticulo As Integer = 0) As Double
		Return ImpuestoProducto.Obtener(CodigoCliente, CodigoArticulo)
	End Function
End Class

