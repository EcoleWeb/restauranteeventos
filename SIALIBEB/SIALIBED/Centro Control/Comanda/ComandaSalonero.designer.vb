<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ComandaSalonero
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ComandaSalonero))
        Me.txtcedulaU = New System.Windows.Forms.TextBox
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.GridRelacion = New System.Windows.Forms.DataGridView
        Me.Label3 = New System.Windows.Forms.Label
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar2 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarTeclado = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.GridRelacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtcedulaU
        '
        resources.ApplyResources(Me.txtcedulaU, "txtcedulaU")
        Me.txtcedulaU.Name = "txtcedulaU"
        '
        'lbUsuario
        '
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.Name = "lbUsuario"
        '
        'GridRelacion
        '
        Me.GridRelacion.BackgroundColor = System.Drawing.Color.White
        Me.GridRelacion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GridRelacion.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridRelacion.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.GridRelacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridRelacion.ColumnHeadersVisible = False
        resources.ApplyResources(Me.GridRelacion, "GridRelacion")
        Me.GridRelacion.Name = "GridRelacion"
        Me.GridRelacion.RowHeadersVisible = False
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label3.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(10, "")
        '
        'ToolBar2
        '
        resources.ApplyResources(Me.ToolBar2, "ToolBar2")
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarTeclado, Me.ToolBarCerrar})
        Me.ToolBar2.ImageList = Me.ImageList
        Me.ToolBar2.Name = "ToolBar2"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        '
        'ToolBarTeclado
        '
        resources.ApplyResources(Me.ToolBarTeclado, "ToolBarTeclado")
        Me.ToolBarTeclado.Name = "ToolBarTeclado"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Name = "Label5"
        '
        'ComandaSalonero
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ControlBox = False
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtcedulaU)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ToolBar2)
        Me.Controls.Add(Me.GridRelacion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ComandaSalonero"
        Me.Opacity = 0.99
        CType(Me.GridRelacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridRelacion As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents txtcedulaU As System.Windows.Forms.TextBox
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Public WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarTeclado As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
