Imports System.Data.SqlClient
Imports System.IO

Public Class SeccionesR
    Inherits System.Windows.Forms.Form

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim ds As DataSet
    Dim dtv As DataView
    Dim ruta, cedula As String
    Dim PMU As New PerfilModulo_Class

#End Region

#Region "Load"
    Private Sub SeccionesR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtNombre.Focus()
        ToolBar2.Buttons(3).Enabled = False
        conectadobd = cConexion.Conectar("Restaurante")
        cargar()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
       
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If
       
        '---------------------------------------------------------------
    End Sub

    Private Sub cargar()
        cboxseccion.Items.Clear()
        txtDescripcion.Text = ""
        txtId.Text = ""
        cboxseccion.SelectedItem = ""
        cboxseccion.Text = ""
        BtImagen.Image = Nothing
        Dim myCommand As SqlDataAdapter = New SqlDataAdapter("SELECT ID, Nombre,descripcion, ImagenRuta FROM Secciones_Restaurante", conectadobd)
        ds = New DataSet
        myCommand.Fill(ds, "Secciones_Restaurante")
        dtv = New DataView(ds.Tables("Secciones_Restaurante"))
        Dim fila As DataRow
        For Each fila In ds.Tables("Secciones_Restaurante").Rows
            cboxseccion.Items.Add(fila("Nombre"))
        Next
    End Sub
#End Region

#Region "Cerrar"
    Private Sub SeccionesR_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub
#End Region

#Region "Agregar"
    Private Sub agregar()
        cboxseccion.Enabled = False
        GroupBox1.Text = "Agregar Secci�n"
        GroupBox1.Visible = True
        txtNombre.Clear()
        txtIdN.Clear()
        txtDescripcionN.Clear()
        btImagenN.Image = Nothing
        txtNombre.Focus()
    End Sub
#End Region

#Region "Editar"
    Private Sub editar()
        If cboxseccion.SelectedItem = "" Then
            Exit Sub
        End If
        cboxseccion.Enabled = False
        GroupBox1.Text = "Editar Secci�n"
        GroupBox1.Visible = True
        txtNombre.Text = cboxseccion.SelectedItem
        txtIdN.Text = txtId.Text
        txtDescripcionN.Text = txtDescripcion.Text
        btImagenN.Image = BtImagen.Image
        txtNombre.Focus()
    End Sub
#End Region

#Region "Buscar"
    Private Sub buscar()
        Dim Fbuscador As New buscador
        Fbuscador.TituloModulo.Text = "Restaurante"
        Fbuscador.bd = "Restaurante"
        Fbuscador.busca = "Nombre"
        Fbuscador.lblEncabezado.Text = "          Secciones de Restaurante"
        Fbuscador.consulta = "SELECT ID, Nombre,descripcion FROM  Secciones_Restaurante"
        Fbuscador.tabla = "Secciones_Restaurante"
        Fbuscador.ToolBarImprimir.Enabled = False
        Fbuscador.cantidad = 3
        Fbuscador.ShowDialog()
        Me.Text = "Editando Secciones del Restaurante"
        GroupBox1.Text = "Editar Secci�n"
        GroupBox1.Visible = True
        txtNombre.Text = Fbuscador.descripcion
        txtIdN.Text = Fbuscador.Icodigo
        txtDescripcionN.Text = Fbuscador.detalle
        txtNombre.Focus()
    End Sub
#End Region

#Region "Borrar"
    Private Sub borrar()
        If cboxseccion.SelectedItem = "" Then
            Exit Sub
        End If
        GroupBox1.Visible = True
        GroupBox1.Text = "Eliminar Secci�n"
        GroupBox1.Visible = True
        txtNombre.Text = cboxseccion.SelectedItem
        txtIdN.Text = txtId.Text
        txtDescripcionN.Text = txtDescripcion.Text
        btImagenN.Image = BtImagen.Image
        txtNombre.Focus()
    End Sub
#End Region

#Region "Imprimir"
    Private Sub imprimir()
        Try
            Dim rptSecciones As New Secciones
            Dim visor As New frmVisorReportes
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptSecciones, False, Me.conectadobd.ConnectionString)
            Me.Hide()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Seccion"
    Private Sub cboxseccion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxseccion.SelectedIndexChanged
        Dim er As Boolean = False
        Dim ruta As String
        txtDescripcion.Text = ""
        txtId.Text = ""
        dtv.RowFilter = "nombre = '" & cboxseccion.SelectedItem & "'"
        Dim fila As DataRowView
        Dim i As Integer = 0
        For Each fila In dtv
            i += 1
        Next
        txtDescripcion.Text = fila("Descripcion")
        txtId.Text = fila("id")
        ruta = fila("imagenRuta")
        If ruta.Trim.Length > 0 Then
            Try
                Dim SourceImage As Bitmap
                SourceImage = New Bitmap(ruta)
                BtImagen.Image = SourceImage
            Catch ex As Exception
                er = True
            End Try
        End If
        BtImagen.TextImageRelation = TextImageRelation.ImageAboveText
        BtImagen.ImageAlign = ContentAlignment.MiddleCenter
        If er = True Then
            MsgBox("Algunas imagenes fueron movidas de su ubicaci�n original")
        End If
    End Sub
#End Region

#Region "Aceptar"
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If PMU.Update Then
            If txtNombre.Text.Trim.Length > 0 And txtDescripcionN.Text.Trim.Length > 0 Then
                GroupBox1.Visible = False
                If GroupBox1.Text = "Agregar Secci�n" Then
                    cConexion.AddNewRecord(conectadobd, "Secciones_Restaurante", "nombre, Descripcion, ImagenRuta", "'" & txtNombre.Text & "','" & txtDescripcionN.Text & "','" & ruta & "'")
                ElseIf GroupBox1.Text = "Editar Secci�n" Then
                    cConexion.UpdateRecords(conectadobd, "Secciones_Restaurante", "nombre='" & txtNombre.Text & "',Descripcion='" & txtDescripcionN.Text & "', ImagenRuta='" & ruta & "'", "id=" & txtIdN.Text)
                ElseIf GroupBox1.Text = "Eliminar Secci�n" Then
                    If MessageBox.Show("Desea Eliminar esta secci�n del Restaurante", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        cConexion.DeleteRecords(conectadobd, "Secciones_Restaurante", " id=" & txtIdN.Text)
                    End If
                End If
                cargar()
            Else
                MessageBox.Show("Debe llenar todos los campos", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End If
            cboxseccion.Enabled = True
        Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
        End If
    End Sub
#End Region

#Region "Cancelar"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        GroupBox1.Visible = False
        cboxseccion.Enabled = True
    End Sub
#End Region

#Region "Imagen"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImagenN.Click
        Dim fImagen As New Imagen
        fImagen.PictureBox1.Image = BtImagen.Image
        fImagen.ShowDialog()
        btImagenN.Image = fImagen.PictureBox1.Image
        If fImagen.ruta <> "" Then
            ruta = fImagen.ruta
        End If
        fImagen.Dispose()
    End Sub
#End Region

#Region "KeyDown"
    Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtDescripcionN.Focus()
        End If
    End Sub

    Private Sub txtDescripcionN_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcionN.KeyDown
        If e.KeyCode = Keys.Enter Then
            btImagenN.Focus()
        End If
    End Sub
#End Region

#Region "Toolbar"
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Try
            PMU = VSM(cedula, Me.Name)
            Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
                Case 1 : agregar()
                Case 2 : editar()
                Case 3 : If PMU.Find Then Me.buscar() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 5 : If PMU.Delete Then borrar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 6 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 7 : teclado()
                Case 8 : Close()
            End Select
        Catch ex As Exception
            MessageBox.Show("Error al inicializar la seguridad: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Teclado"
    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                txtClave.Text = ""
                Me.ToolBar2.Buttons(0).Enabled = True
                Me.ToolBar2.Buttons(1).Enabled = True
                Me.ToolBar2.Buttons(2).Enabled = True
                Me.ToolBar2.Buttons(3).Enabled = False
                Me.ToolBar2.Buttons(4).Enabled = True
                Me.ToolBar2.Buttons(5).Enabled = True
                Me.ToolBar2.Buttons(6).Enabled = True
                Me.ToolBar2.Buttons(7).Enabled = True
                Me.cboxseccion.Enabled = True
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                Me.ToolBar2.Buttons(0).Enabled = True
                Me.ToolBar2.Buttons(1).Enabled = True
                Me.ToolBar2.Buttons(2).Enabled = True
                Me.ToolBar2.Buttons(3).Enabled = False
                Me.ToolBar2.Buttons(4).Enabled = True
                Me.ToolBar2.Buttons(5).Enabled = True
                Me.ToolBar2.Buttons(6).Enabled = True
                Me.ToolBar2.Buttons(7).Enabled = True
                Me.cboxseccion.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class