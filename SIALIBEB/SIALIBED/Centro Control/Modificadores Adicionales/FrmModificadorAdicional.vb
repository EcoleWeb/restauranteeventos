Public Class FrmModificadorAdicional
    Public Comanda As String
    Public Nota As String
    Public MontoNota As Double
    Public Impresora As String
    Public Aceptado As Boolean = False

    Private Function Validacion() As Boolean
        Dim Status As Boolean = False
        If Me.TextBox1.Text = "" Then MsgBox("No se ha definido ninguna nota!!!", MsgBoxStyle.Information, "Atenci�n...") : Return False
        If Not IsNumeric(Me.TextBox1.Text) Then MsgBox("No se ha definido un valor num�rico!!!", MsgBoxStyle.Information, "Atenci�n...") : Return False
        If CDbl(Me.TextBox1.Text) < 0 Then MsgBox("El valor definido no puede ser menor que 0 !!!", MsgBoxStyle.Information, "Atenci�n...") : Return False
        Return True
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Not Validacion() Then Exit Sub
        Nota = Me.TextBox2.Text
        MontoNota = Me.TextBox1.Text
        Impresora = Me.ComboBoxImpresora.Text
        Aceptado = True
        Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Nota = ""
        MontoNota = 0
        Aceptado = False
        Close()
    End Sub

    Private Sub FrmModificadorAdicional_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Conexion As New ConexionR
        Dim DataSet As New DataSet
        Conexion.Conectar("Restaurante")
        Conexion.GetDataSet(Conexion.SqlConexion, "SELECT Impresora FROM ComandaTemporal WHERE (cCodigo = " & Comanda & ") GROUP BY Impresora", DataSet, "Impresoras")
        Dim x As Integer = 0
        For x = 0 To DataSet.Tables("Impresoras").Rows.Count - 1
            If x > DataSet.Tables("Impresoras").Rows.Count - 1 Then
                Exit For
            End If
            If DataSet.Tables("Impresoras").Rows(x).Item(0) = "" Then
                DataSet.Tables("Impresoras").Rows.RemoveAt(x)
                x = x - 1
            End If
        Next
        Me.ComboBoxImpresora.DataSource = DataSet.Tables("Impresoras")
        Me.ComboBoxImpresora.DisplayMember = "Impresora"

    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Not Validacion() Then Exit Sub
            Nota = Me.TextBox2.Text
            MontoNota = Me.TextBox1.Text
            Impresora = Me.ComboBoxImpresora.Text
            Aceptado = True
            Close()
        End If
    End Sub


End Class