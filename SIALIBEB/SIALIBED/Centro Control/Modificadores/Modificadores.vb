
Public Class GCategorias

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Public escogiendo As Boolean = False
    Public escogi As Integer = 0
    Public nombreEscogi As String = ""
#End Region

#Region "Cerrar"
    Private Sub Modificadores_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
             cConexion.DesConectar(conectadobd)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
#End Region

#Region "Load"
    Private Sub Modificadores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            conectadobd = cConexion.Conectar("Restaurante")
            CargaModificadores()

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
          
            If gloNoClave Or escogiendo Then
                Loggin_Usuario()
            Else
                TextBox1.Focus()
            End If
           
            '---------------------------------------------------------------
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Cargar Modificadores"
    Private Sub CargaModificadores()
        Try
            cargaDatos()
            UbicarPosiciones()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cargaDatos()
        Dim aButton As Button
        Dim i, ii, X, Y, n As Integer

        Panel1.Controls.Clear()

        For i = 1 To 7
            For ii = 1 To 4
                aButton = New Button
                aButton.Top = Y
                aButton.Left = X
                aButton.Width = 120
                aButton.Height = 80
                aButton.Name = n
                'aButton.ShowToolTips = False
                AddHandler aButton.Click, AddressOf Clickbuttons
                Me.Panel1.Controls.Add(aButton)
                X = X + 122
                n = n + 1
            Next
            Y = Y + 82
            X = 0
        Next
    End Sub

    Private Sub UbicarPosiciones()
        Dim er As String = False
        Dim ruta As String
        Dim botones As Button

        cConexion.GetRecorset(conectadobd, "select ID,Nombre,posicion,ImagenRuta from modificadores_forzados where posicion<> -1", rs) '& ListItems.Text)
        While rs.Read
            botones = New Button
            botones = Panel1.Controls.Item(CInt(rs("posicion")))
            botones.Text = rs("Nombre") ' Nombre de la mesa
            botones.Name = botones.Name & "-" & rs("ID") 'Codigo de la mesa
            ruta = rs("ImagenRuta")
            If ruta.Trim.Length > 0 Then
                Try

                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    botones.Image = SourceImage
                Catch ex As Exception
                    er = True
                End Try
            End If
            botones.TextImageRelation = TextImageRelation.ImageAboveText
            botones.ImageAlign = ContentAlignment.MiddleCenter
        End While
        rs.Close()
        If er = True Then
            MsgBox("Algunas imagenes fueron movidas de su ubicaci�n original")
        End If
    End Sub
#End Region

#Region "Botones"
    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ruta As String = ""
            Dim g_prompt As New Prompt
            Dim datos As Array
            g_prompt.Text = "Modificador"
            g_prompt.txtDato.Text = sender.text
            g_prompt.btnBuscar.Visible = True
            g_prompt.btnBorrar.Visible = True
            g_prompt.GroupBox1.Visible = False

            If sender.name = "" Then
                cConexion.GetRecorset(conectadobd, "Select ImagenRuta from Modificadores_forzados where id=" & datos(1), rs)
                While rs.Read
                    ruta = rs("ImagenRuta")
                End While
                rs.Close()


            End If

            g_prompt.btnBuscar.Image = sender.image
            g_prompt.ruta = ruta
            g_prompt.Label2.Text = "NOMBRE"
            datos = CStr(sender.name).Split("-")
            If Me.escogiendo And (Not sender.text.Equals("")) Then
                Me.escogi = datos(1)
                Me.nombreEscogi = sender.text
                DialogResult = Windows.Forms.DialogResult.OK
                Exit Sub

            End If

            g_prompt.ShowDialog()


            ruta = g_prompt.ruta
            PMU = VSM(cedula, Me.Name)

            If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                If PMU.Update Then
                    If datos.Length > 1 Then ' update
                        cConexion.UpdateRecords(conectadobd, "Modificadores_forzados", "Nombre='" & g_prompt.txtDato.Text & "',ImagenRuta='" & ruta & "'", " id=" & datos(1))
                    Else 'insert
                        cConexion.AddNewRecord(conectadobd, "Modificadores_forzados", "Nombre,posicion,ImagenRuta", "'" & g_prompt.txtDato.Text & "'," & datos(0) & ",'" & ruta & "'")
                    End If
                Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                End If
            ElseIf g_prompt.tipo = 3 Then
                If datos.Length > 1 Then
                    If PMU.Delete Then
                        If MessageBox.Show("Realmente desea eliminar este acompa�amiento", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            cConexion.DeleteRecords(conectadobd, "Modificadores_forzados", " id=" & datos(1))
                        End If
                    Else : MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                    End If
                End If
            End If

            g_prompt.Dispose()
            CargaModificadores()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                TextBox1.Text = ""
                Me.Panel1.Enabled = True
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                Me.Panel1.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class