Public Class GruposMenu

#Region "variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Dim Inicializando As Boolean = False
#End Region

#Region "Cerrar"
    Private Sub GrupoCategoria_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
#End Region

#Region "Load"
    Private Sub GrupoCategoria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            conectadobd = cConexion.Conectar("Restaurante")
            Inicializando = True
            ObtenerDimension()
            CargarGrupos()
            LabelTotal.Text = "Max. Categor�as " & (Me.NumericColumnas.Value * Me.NumericFilas.Value)

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
           
            If gloNoClave Then
                Loggin_Usuario()
            Else
                TextBox1.Focus()
            End If
          
            '---------------------------------------------------------------

            If cmbGrupos.Items.Count > 0 Then CrearCategoriaMenu()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CargarGrupos()
        Dim DS As New DataSet
        cConexion.GetDataSet(conectadobd, "SELECT id,Nombre_Grupo FROM Grupos_Menu", DS, "Grupos")
        cmbGrupos.DataSource = DS.Tables("Grupos")
        cmbGrupos.DisplayMember = "Nombre_Grupo"
        cmbGrupos.ValueMember = "id"
    End Sub
#End Region

#Region "CrearCategoria"
    Private Sub CrearCategoriaMenu()
        Dim aButton As Button
        Dim i, ii, X, Y, n As Integer
        PanelMesas.Controls.Clear()
        For i = 1 To Me.NumericFilas.Value    'ORIGINAL 7
            For ii = 1 To Me.NumericColumnas.Value  'ORIGINAL 4
                aButton = New Button
                aButton.Top = Y
                aButton.Left = X
                aButton.Width = 120
                aButton.Height = 80
                aButton.Name = n
                'aButton.ShowToolTips = False
                AddHandler aButton.Click, AddressOf Clickbuttons
                Me.PanelMesas.Controls.Add(aButton)
                X = X + 122
                n = n + 1
            Next
            Y = Y + 82
            X = 0
        Next
        UbicarPosiciones()
    End Sub

    Private Sub UbicarPosiciones()
        Dim ruta As String
        Dim Er As Boolean = False
        Dim botones As Button
        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
        cConexion.GetRecorset(conectadobd, "SELECT id,nombre,imagenRuta,posicion,idgrupo FROM Categorias_Menu where IdGrupo =" & cmbGrupos.SelectedValue, rs)
        Dim cont As Integer = 0
        While rs.Read
            Try

            
                botones = New Button
                botones = PanelMesas.Controls.Item(CInt(rs("Posicion")))
                botones.Text = rs("nombre") ' Nombre de la mesa
                botones.Name = botones.Name & "-" & rs("id") & "-" & rs("idGrupo") 'Codigo de la mesa
                ruta = rs("imagenRuta")
                If ruta.Trim.Length > 0 Then
                    Try
                        Dim SourceImage As Bitmap
                        SourceImage = New Bitmap(ruta)
                        botones.Image = SourceImage
                    Catch ex As Exception
                        Er = True
                    End Try
                End If
                botones.TextImageRelation = TextImageRelation.ImageAboveText
                cont += 1
            Catch ex As Exception
                MsgBox("Categoria supera dimension de categoria ", MsgBoxStyle.Information)
            End Try
        End While
        rs.Close()
        '        If Er = True Then
        ' MsgBox("Algunas imagenes fueron movidas de su ubicaci�n original")
        ' End If
    End Sub
#End Region

#Region "Botones"
    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select Tipo From conf", dt)
        Dim tipo As Integer = 0
        If dt.Rows.Count > 0 Then
            tipo = dt.Rows(0).Item(0)

        End If
        If tipo = 0 Then

            Dim g_prompt As New Prompt
            Dim datos As Array
            Dim ruta As String
            PMU = VSM(cedula, Me.Name)


            g_prompt.txtDato.Text = sender.text
            ' g_prompt.txtMEsas.Visible = True
            g_prompt.btnBuscar.Visible = True
            g_prompt.btnBuscar.Image = sender.Image
            g_prompt.Label1.Visible = True
            g_prompt.btnBorrar.Visible = True
            g_prompt.Label2.Text = "NOMBRE"
            g_prompt.Label1.Text = "IMAGEN"
            g_prompt.GroupBox1.Visible = True
            datos = CStr(sender.name).Split("-")
            If datos.Length = 3 Then
                g_prompt.grupo = datos(2)
            End If
            g_prompt.ShowDialog()
            ruta = g_prompt.ruta
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                'If PMU.Update Then
                If datos.Length = 3 Then ' update
                    If Not ruta Is Nothing Then
                        If Not ruta.Equals("") Then
                            cConexion.UpdateRecords(conectadobd, "Categorias_Menu", "Nombre='" & g_prompt.txtDato.Text & "', IdGrupo=" & g_prompt.ListItems.Text & ", ImagenRuta='" & ruta & "'", " id=" & datos(1))
                        Else
                            cConexion.UpdateRecords(conectadobd, "Categorias_Menu", "Nombre='" & g_prompt.txtDato.Text & "', IdGrupo=" & g_prompt.ListItems.Text & "", " id=" & datos(1))
                        End If
                    Else
                        cConexion.UpdateRecords(conectadobd, "Categorias_Menu", "Nombre='" & g_prompt.txtDato.Text & "', IdGrupo=" & g_prompt.ListItems.Text & "", " id=" & datos(1))
                    End If
                    

                Else 'insert
                    Dim pos As Integer
                    Dim SQLS As SqlClient.SqlDataReader
                    Dim Cx As New Conexion
                    Dim ds As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select UtilizaGrupo From conf", ds)
                    Dim grupo As Boolean = False
                    If ds.Rows.Count > 0 Then
                        grupo = dt.Rows(0).Item(0)

                    End If
                    If grupo = False Then
                        SQLS = Cx.GetRecorset(Cx.Conectar("Restaurante"), "SELECT posicion  FROM Categorias_menu where Posicion=" & datos(0))
                        'Cx.DesConectar(Cx.sQlconexion)

                        Dim CONT As Integer = 0
                        While SQLS.Read
                            CONT += 1
                        End While
                        If CONT <> 0 Then
                            MsgBox("Ya se encuentra una categoria en esta posicion", MsgBoxStyle.Information)
                            Exit Sub
                        Else
                            pos = datos(0)
                        End If
                    Else
                        pos = datos(0)
                    End If
                 
                    cConexion.AddNewRecord(conectadobd, "Categorias_Menu", "Nombre,posicion,idGrupo,ImagenRuta", "'" & g_prompt.txtDato.Text & "'," & pos & "," & g_prompt.ListItems.Text & ",'" & ruta & "'")
                End If
                'Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                'End If
            ElseIf g_prompt.tipo = 3 Then
                If PMU.Delete Then
                    cConexion.DeleteRecords(conectadobd, "Categorias_Menu", " id=" & datos(1))
                Else : MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                End If
            End If

            g_prompt.Dispose()
            CrearCategoriaMenu()
        Else
            Dim g_prompt As New Prompt_Categoria
            Dim datos As Array
            Dim ruta As String
            PMU = VSM(cedula, Me.Name)

            g_prompt.txtDato.Text = sender.text
            ' g_prompt.txtMEsas.Visible = True
            g_prompt.btnBuscar.Visible = True
            g_prompt.btnBuscar.Image = sender.Image

            g_prompt.btnBorrar.Visible = True
            g_prompt.Label2.Text = "NOMBRE"
            g_prompt.txtDato.ReadOnly = False

            g_prompt.GroupBox1.Visible = True
            g_prompt.GbCabys.Visible = True
            g_prompt.PanelCategoria.Visible = True
            g_prompt.PanelAgregado.Visible = False
            datos = CStr(sender.name).Split("-")
            If datos.Length = 3 Then
                g_prompt.Id_Categoria = datos(1)
                g_prompt.grupo = datos(2)
            End If
            g_prompt.ShowDialog()
            ruta = g_prompt.ruta
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            If Trim(g_prompt.txtDato.Text) <> vbNullString And g_prompt.tipo = 1 Then
                'If PMU.Update Then
                If datos.Length = 3 Then ' update
                    If Not ruta Is Nothing Then
                        cConexion.UpdateRecords(conectadobd, "Categorias_Menu", "CuentaIngreso='" & g_prompt.txtCuentaIngresos.Text & "', DCuentaIngreso='" & g_prompt.lbDescripcionIngresos.Text & "', CuentaCosto='" & g_prompt.txtCuentaCostos.Text & "', DCuentaCosto='" & g_prompt.lbDescripcionCostos.Text & "', IdActividadEconomica=" & g_prompt.lbIdActividad.Text & ", IdCabys=" & g_prompt.lbIdCabys.Text & ", Nombre='" & g_prompt.txtDato.Text & "', IdGrupo=" & g_prompt.ListItems.Text & ", ImagenRuta='" & ruta & "'", " id=" & datos(1))
                    Else
                        cConexion.UpdateRecords(conectadobd, "Categorias_Menu", "CuentaIngreso='" & g_prompt.txtCuentaIngresos.Text & "', DCuentaIngreso='" & g_prompt.lbDescripcionIngresos.Text & "', CuentaCosto='" & g_prompt.txtCuentaCostos.Text & "', DCuentaCosto='" & g_prompt.lbDescripcionCostos.Text & "', IdActividadEconomica=" & g_prompt.lbIdActividad.Text & " , IdCabys=" & g_prompt.lbIdCabys.Text & " , Nombre='" & g_prompt.txtDato.Text & "', IdGrupo=" & g_prompt.ListItems.Text & "", " id=" & datos(1))
                    End If

                Else 'insert
                    Dim pos As Integer
                    Dim SQLS As SqlClient.SqlDataReader
                    Dim Cx As New Conexion
                    Dim ds As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select UtilizaGrupo From conf", ds)
                    Dim grupo As Boolean = False
                    If ds.Rows.Count > 0 Then
                        grupo = dt.Rows(0).Item(0)

                    End If
                    If grupo = False Then
                        SQLS = Cx.GetRecorset(Cx.Conectar("Restaurante"), "SELECT posicion  FROM Categorias_menu where Posicion=" & datos(0))
                        'Cx.DesConectar(Cx.sQlconexion)

                        Dim CONT As Integer = 0
                        While SQLS.Read
                            CONT += 1
                        End While
                        If CONT <> 0 Then
                            MsgBox("Ya se encuentra una categoria en esta posicion", MsgBoxStyle.Information)
                            Exit Sub
                        Else
                            pos = datos(0)
                        End If
                    Else
                        pos = datos(0)
                    End If
                    Dim posApp As Integer
                    posApp = fnUltimaPosicionApp()
                    posApp = posApp + 1
                    cConexion.AddNewRecord(conectadobd, "Categorias_Menu", "Nombre,posicion,idGrupo,ImagenRuta,PosicionApp, IdCabys, IdActividadEconomica,CuentaIngreso, DCuentaIngreso, CuentaCosto, DCuentaCosto", "'" & g_prompt.txtDato.Text & "'," & pos & "," & g_prompt.ListItems.Text & ",'" & ruta & "'," & posApp & ", " & g_prompt.lbIdCabys.Text & ", " & g_prompt.lbIdActividad.Text & ",'" & g_prompt.txtCuentaIngresos.Text & "','" & g_prompt.lbDescripcionIngresos.Text & "' ,'" & g_prompt.txtCuentaCostos.Text & "','" & g_prompt.lbDescripcionCostos.Text & "' ")
                End If
                'Else : MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                'End If
            ElseIf g_prompt.tipo = 3 Then
                If PMU.Delete Then
                    cConexion.DeleteRecords(conectadobd, "Categorias_Menu", " id=" & datos(1))
                Else : MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                End If
            End If

            g_prompt.Dispose()
            CrearCategoriaMenu()
        End If
    End Sub
    Function fnUltimaPosicionApp()
        Dim dt As New DataTable
        Dim posicion As Integer = 0
        Dim cmd As New SqlClient.SqlCommand
        cmd.CommandText = "Select Isnull(MAX(PosicionApp),0) as PosicionApp  from Categorias_Menu"
        cFunciones.spCargarDatos(cmd, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
        If dt.Rows.Count > 0 Then
            posicion = dt.Rows(0).Item("PosicionApp")
            Return posicion
        Else
            Return 0
        End If
    End Function
#End Region

#Region "Verificar Longuitud"
    Private Sub VerificarLongitud() ' SAJ 080807 Verifica la dimesion de los datos registrados antes de realizar las operaciones de actualizaci�n para que ningun elemento registrado quede por fuera de la matriz
        Dim Conexion As New Conexion
        Dim F, C, MaxPosicion As Integer
        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
        Try
            MaxPosicion = Conexion.SlqExecuteScalar(conectadobd, "SELECT ISNULL(MAX(posicion),1) FROM Categorias_Menu")
            F = Me.NumericFilas.Value
            C = Me.NumericColumnas.Value

            If (F * C) < MaxPosicion Then
                MsgBox("No se puede redimensionar el tama�o de las categoria.. quedar�a un elemento fuera de este..", MsgBoxStyle.Information, "Atenci�n..")
                ObtenerDimension()
                LabelTotal.Text = "Max. Categor�as " & (F * C)
            Else
                Conexion.UpdateRecords("Categoria_Dimesion", "Filas =" & F & ",Columnas = " & C, "", "Restaurante") ' actualiza la nueva dimesion en la bd.
                'CrearCategoriaMenu()
                LabelTotal.Text = "Max. Categor�as " & (F * C)
            End If

        Catch ex As Exception
            MsgBox("Error al cargar la dimesi�n de las categorias..", MsgBoxStyle.Information, "Alerta...")
        End Try
    End Sub

    Private Sub NumericFilas_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericFilas.ValueChanged
        If Inicializando Then VerificarLongitud()
    End Sub

    Private Sub NumericColumnas_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericColumnas.ValueChanged
        If Inicializando Then VerificarLongitud()
    End Sub
#End Region

#Region "Obtener Dimension"
    Private Sub ObtenerDimension() ' SAJ 0809807. REDIMESIONAMIENTO DE LSA CATEGORIAS
        Dim Conexion As New Conexion
        Dim F, C As Integer
        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
        Try
            If Inicializando Then
                F = Conexion.SlqExecuteScalar(conectadobd, "SELECT TOP 1 Filas FROM Categoria_Dimesion")
                C = Conexion.SlqExecuteScalar(conectadobd, "SELECT TOP 1 Columnas FROM Categoria_Dimesion")
                NumericFilas.Value = F
                NumericColumnas.Value = C
            End If
        Catch ex As Exception
            MsgBox("Error al cargar la dimesion de las categorias..", MsgBoxStyle.Information, "Alerta...")
        End Try
    End Sub
#End Region

#Region "Grupos"
    Private Sub cmbGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupos.SelectedIndexChanged
        Try
            CrearCategoriaMenu()
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox1.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario <> "" Then
                TextBox1.Text = ""
                Me.PanelMesas.Enabled = True
                Me.lbUsuario.Text = usuario
                Me.NumericColumnas.Enabled = True
                Me.NumericFilas.Enabled = True
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                TextBox1.Text = ""
                Me.PanelMesas.Enabled = True
                Me.NumericColumnas.Enabled = True
                Me.NumericFilas.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub PanelMesas_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelMesas.Paint

    End Sub
End Class