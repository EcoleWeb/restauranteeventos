<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuI
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuI))
        Me.PanelMesas = New System.Windows.Forms.Panel()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.lbUsuario = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnMover = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListItems = New System.Windows.Forms.ListBox()
        Me.cboGrupos = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LabelDimension = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericFilas = New System.Windows.Forms.NumericUpDown()
        Me.NumericColumnas = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericFilas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericColumnas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        resources.ApplyResources(Me.PanelMesas, "PanelMesas")
        Me.PanelMesas.Name = "PanelMesas"
        '
        'txtEstado
        '
        Me.txtEstado.BackColor = System.Drawing.SystemColors.Control
        Me.txtEstado.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtEstado, "txtEstado")
        Me.txtEstado.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtEstado.Name = "txtEstado"
        '
        'lbUsuario
        '
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.lbUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbUsuario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Name = "Label11"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        '
        'btnMover
        '
        Me.btnMover.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.btnMover, "btnMover")
        Me.btnMover.Image = Global.SIALIBEB.My.Resources.Resources.TL_hist
        Me.btnMover.Name = "btnMover"
        Me.btnMover.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog1
        Me.Button4.Name = "Button4"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.ListItems)
        Me.GroupBox1.Controls.Add(Me.cboGrupos)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Button1
        '
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ListItems
        '
        Me.ListItems.FormattingEnabled = True
        resources.ApplyResources(Me.ListItems, "ListItems")
        Me.ListItems.Name = "ListItems"
        '
        'cboGrupos
        '
        Me.cboGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cboGrupos, "cboGrupos")
        Me.cboGrupos.FormattingEnabled = True
        Me.cboGrupos.Name = "cboGrupos"
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.btnMover)
        Me.Panel1.Controls.Add(Me.txtEstado)
        Me.Panel1.Name = "Panel1"
        '
        'CheckBox1
        '
        resources.ApplyResources(Me.CheckBox1, "CheckBox1")
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.LabelDimension)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.NumericFilas)
        Me.GroupBox2.Controls.Add(Me.NumericColumnas)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'LabelDimension
        '
        resources.ApplyResources(Me.LabelDimension, "LabelDimension")
        Me.LabelDimension.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LabelDimension.Name = "LabelDimension"
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Name = "Label1"
        '
        'NumericFilas
        '
        resources.ApplyResources(Me.NumericFilas, "NumericFilas")
        Me.NumericFilas.Name = "NumericFilas"
        Me.NumericFilas.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'NumericColumnas
        '
        resources.ApplyResources(Me.NumericColumnas, "NumericColumnas")
        Me.NumericColumnas.Name = "NumericColumnas"
        Me.NumericColumnas.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'MenuI
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.PanelMesas)
        Me.Name = "MenuI"
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.NumericFilas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericColumnas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelMesas As System.Windows.Forms.Panel
    Friend WithEvents txtEstado As System.Windows.Forms.TextBox
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnMover As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListItems As System.Windows.Forms.ListBox
    Friend WithEvents cboGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelDimension As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents NumericColumnas As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericFilas As System.Windows.Forms.NumericUpDown
End Class
