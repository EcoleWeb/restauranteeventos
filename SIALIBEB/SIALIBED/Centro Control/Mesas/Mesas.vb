Public Class Mesas

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PMU As New PerfilModulo_Class
    Dim ParaLlevar As Integer = 0
#End Region

#Region "Cerrar"
    Private Sub Mesas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Close()
    End Sub
#End Region

#Region "Load"
    Private Sub Mesas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        CargarGrupos()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If

        '---------------------------------------------------------------
    End Sub

    Private Sub CargarGrupos()
        cboGrupo.Items.Clear()
        ListItems.Items.Clear()
        cConexion.GetRecorset(conectadobd, "Select id,nombre_grupo from grupos_mesas", rs)
        While rs.Read
            cboGrupo.Items.Add(rs("nombre_grupo"))
            ListItems.Items.Add(rs("id"))
        End While
        rs.Close()
        If cboGrupo.Items.Count > 0 Then
            cboGrupo.SelectedIndex = 0
            ListItems.SelectedIndex = 0
        End If
    End Sub
#End Region

#Region "Mesas"
    Private Sub UbicarPosiciones()
        Dim er As String = False
        Dim ruta As String

        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()

        cConexion.GetRecorset(conectadobd, "Select id,nombre_mesa,posicion,ISNULL(imagenRuta,'') AS imagenRuta from mesas where id_GrupoMesa=" & ListItems.Text, rs) '& ListItems.Text)
        While rs.Read

            PanelMesas.Controls.Item(CInt(rs("posicion"))).Text = rs("nombre_mesa") ' Nombre de la mesa
            PanelMesas.Controls.Item(CInt(rs("posicion"))).Name &= "-" & rs("id") 'Codigo de la mesa
            ruta = rs("imagenRuta")
            If ruta.Trim.Length > 0 Then
                Try
                    Dim SourceImage As Bitmap
                    SourceImage = New Bitmap(ruta)
                    CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).Image = SourceImage
                Catch ex As Exception
                    er = True
                End Try
            End If
            CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).TextImageRelation = TextImageRelation.ImageAboveText
            CType(PanelMesas.Controls.Item(CInt(rs("posicion"))), Button).ImageAlign = ContentAlignment.MiddleCenter
        End While
        rs.Close()
        If er = True Then
            MsgBox("Algunas imagenes fueron movidas de su ubicación original")
        End If
    End Sub

    Private Sub CrearMesas()
        Dim aButton As Button
        Dim i, ii, X, Y, n As Integer

        PanelMesas.Controls.Clear()

        For i = 1 To 8
            For ii = 1 To 8
                aButton = New Button
                aButton.Top = Y
                aButton.Left = X
                aButton.Width = 75
                aButton.Height = 75
                aButton.Name = n
                'aButton.ShowToolTips = False
                AddHandler aButton.Click, AddressOf Clickbuttons
                PanelMesas.Controls.Add(aButton)
                X = X + 77
                n = n + 1
            Next
            Y = Y + 77
            X = 0
        Next
        UbicarPosiciones()
    End Sub

    Public Sub Clickbuttons(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim rCarMesa As New MesaF
        Dim datos As Array

        datos = CStr(sender.name).Split("-")

        If datos.Length = 2 Then
            rCarMesa.idMEsa = datos(1)
        End If
        rCarMesa.cedula = cedula
        If ParaLlevar = 1 Then
            rCarMesa.ChMesaExpress.Checked = True
        End If
        rCarMesa.idGrupo = ListItems.SelectedItem ' Codigo de Grupo
        rCarMesa.posicion = datos(0) 'El nombre del boton tiene la posicion
        rCarMesa.ShowDialog()
        rCarMesa.Dispose()
        datos = Nothing
        CrearMesas()
    End Sub
#End Region

#Region "Grupos"
    Private Sub cboGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrupo.SelectedIndexChanged
        ListItems.SelectedIndex = cboGrupo.SelectedIndex
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select ParaLlevar from Grupos_Mesas where id=" & ListItems.SelectedItem & "", dt)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("ParaLlevar").ToString().Equals("True") Then
                    ParaLlevar = 1
                Else
                    ParaLlevar = 0
                End If
            End If

        Catch ex As Exception

        End Try

        CrearMesas()
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                txtClave.Text = ""
                lbUsuario.Text = usuario
                PanelMesas.Enabled = True
                cboGrupo.Enabled = True
            Else
                Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                PanelMesas.Enabled = True
                cboGrupo.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class
