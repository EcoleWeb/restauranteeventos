Imports System.IO
Public Class MesaF

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Public posicion As Integer
    Public idGrupo As Integer
    Public idMEsa As Integer
    Public ruta, cedula As String
    Dim PMU As New PerfilModulo_Class
#End Region

    Private Sub MesaF_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub MesaF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim MesaExpress As String = GetSetting("SeeSoft", "Restaurante", "MesaExpress")
        If MesaExpress.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "MesaExpress", "0")
            ChMesaExpress.Visible = False
        ElseIf MesaExpress.Equals("1") Then
            ChMesaExpress.Visible = True
        End If

        conectadobd = cConexion.Conectar("Restaurante")
        CargarSecciones()
        ToolBar2.Buttons(4).Enabled = False
        If idMEsa <> 0 Then
            CargarPredeterminado()
            ToolBar2.Buttons(4).Enabled = True
        End If
        ToolBar2.Buttons(0).Enabled = False
        ToolBar2.Buttons(1).Enabled = False
        ToolBar2.Buttons(2).Enabled = False
        'Me.Enabled = VerificandoAcceso_a_Modulos(Me, cedula)
        PMU = VSM(cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...")
        'Me.Close()
        Exit Sub
    End Sub

    Private Sub CargarPredeterminado()
        cConexion.GetRecorset(conectadobd, "Select nombre_mesa,numero_asientos,id_seccion,ISNULL(imagenRuta,'') AS imagenRuta, Express from mesas where id=" & idMEsa, rs)
        While rs.Read
            txtNombre.Text = rs("nombre_mesa")
            nAsiento.Text = rs("numero_asientos")
            cmdCombo.SelectedIndex = lstItems.FindString(rs("id_seccion"))
            ruta = rs("imagenRuta")
            ChMesaExpress.Checked = rs("Express")
            If ruta.Trim.Length > 0 Then
                Dim SourceImage As Bitmap

                If File.Exists(ruta) Then
                    SourceImage = New Bitmap(ruta)
                    BtImagen.Image = SourceImage
                End If

                BtImagen.Text = ""
            End If
        End While
        rs.Close()
    End Sub

    Private Sub CargarSecciones()
        cmdCombo.Items.Clear()
        lstItems.Items.Clear()

        cConexion.GetRecorset(conectadobd, "Select id,nombre from secciones_restaurante", rs)
        While rs.Read
            cmdCombo.Items.Add(rs("nombre"))
            lstItems.Items.Add(rs("id"))
        End While
        rs.Close()
    End Sub

    Private Sub cmdCombo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmdCombo.KeyDown
        If e.KeyCode = Keys.Enter Then
            BtImagen.Focus()
        End If
    End Sub

    Private Sub cmdCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCombo.SelectedIndexChanged
        lstItems.SelectedIndex = Me.cmdCombo.SelectedIndex
    End Sub


    Private Sub BtImagen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtImagen.Click
        Dim fImagen As New Imagen
        fImagen.PictureBox1.Image = BtImagen.Image
        fImagen.ShowDialog()
        BtImagen.Image = fImagen.PictureBox1.Image
        ruta = fImagen.ruta
        fImagen.Dispose()
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : If PMU.Update Then Me.agregar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Delete Then Me.borrar() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : teclado()
            Case 7 : Close()
        End Select
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicaci�n", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub agregar()
        If Trim(txtNombre.Text) <> vbNullString And nAsiento.Text > 0 And cmdCombo.Text <> vbNullString Then
            If idMEsa > 0 Then
                cConexion.SlqExecute(conectadobd, "Update mesas set Nombre_Mesa='" & txtNombre.Text & "',Id_GrupoMesa=" & idGrupo & ",Id_Seccion=" & lstItems.Text & ",Numero_Asientos=" & nAsiento.Text & ",Posicion=" & posicion & ",imagenRuta='" & ruta & "',Express='" & ChMesaExpress.Checked & "' Where Id=" & idMEsa)
            Else
                cConexion.SlqExecute(conectadobd, "Insert into mesas (nombre_mesa,id_grupoMesa,id_Seccion,Numero_Asientos,posicion,imagenRuta,Express) values('" & txtNombre.Text & "'," & idGrupo & "," & lstItems.Text & "," & nAsiento.Text & "," & posicion & ",'" & ruta & "','" & ChMesaExpress.Checked & "')")
            End If
            Close()
        Else
            MessageBox.Show("Verifica hayas completado todos los campos")
        End If
    End Sub

    Private Sub borrar()
        If MessageBox.Show("Esta seguro(a) que desea eliminar la Mesa?", "Mesas", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            If Trim(txtNombre.Text) <> vbNullString And nAsiento.Text > 0 And cmdCombo.Text <> vbNullString Then
                If idMEsa > 0 Then
                    cConexion.SlqExecute(conectadobd, "DELETE FROM mesas Where Id=" & idMEsa)
                Else
                    MsgBox("No permitido")
                End If
            Else
                MessageBox.Show("Datos Erroneos")
            End If
            Close()
        End If
    End Sub

    Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            nAsiento.Focus()
        End If
    End Sub


    Private Sub nAsiento_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles nAsiento.KeyDown
        If e.KeyCode = Keys.Enter Then
            cmdCombo.Focus()
        End If
    End Sub

End Class