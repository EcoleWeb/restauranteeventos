Public Class frmQuestion

    Public resultado As Boolean

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click, Label1.Click

        Me.Label1.BackColor = Color.LightSkyBlue
        Me.Label2.BackColor = Color.White
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro2

        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        resultado = True
        Me.Timer1.Start()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click, Label2.Click

        Me.Label1.BackColor = Color.White
        Me.Label2.BackColor = Color.LightSkyBlue
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro2
        resultado = False
        Me.Timer1.Start()
    End Sub

    Dim contador As Integer = 0
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If contador = 2 Then
            contador = 0
            Me.Timer1.Stop()
            Me.Close()
        Else
            contador = contador + 1
        End If

    End Sub

    Private Sub frmQuestion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.S
                Me.Label1.BackColor = Color.LightSkyBlue
                Me.Label2.BackColor = Color.White
                Me.PictureBox1.BackgroundImage = My.Resources.cuadro2

                Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
                resultado = True
                Me.Timer1.Start()
            Case Keys.N

                Me.Label1.BackColor = Color.White
                Me.Label2.BackColor = Color.LightSkyBlue
                Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
                Me.PictureBox2.BackgroundImage = My.Resources.cuadro2
                resultado = False
                Me.Timer1.Start()
        End Select
    End Sub
End Class