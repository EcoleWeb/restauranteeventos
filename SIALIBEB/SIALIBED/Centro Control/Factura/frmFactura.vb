Public Class frmFactura

    Dim miFacturas As New cls_Facturas()
    Dim miFacturasDetalle As New cls_FacturasDetalle()
    Dim f As New cls_Broker()

    Dim conf As New cls_Configuraciones()

    Public idComanda As String
    Public numeroSeparada As String
    Public mesa As String

    Public datos As DataTable

    'Variables para calcular el total de la factura..
    Dim subtotal, descuento, impServ, impVent, extraProp, total, tipoCambio, cantidad As Double
    'Variables para guardar la factura en diferentes configuraciones..
    Dim tipo_pago As String
    Dim id_reservacion As String
    Dim id_habitacion As String
    Dim id_cliente As String = "1"
    Dim Id_Centro_Cortesia As String
    Dim id_comisionista As String
    Dim cuenta_contable_cortesia As String
    Dim descripcion_cortesia As String
    Dim observacion_cortesia As String
    Dim nombre_cortesia As String
    Public result As Boolean = False

    'Impuesto Servicio Habitacion..
    Dim Banderaish As Boolean = False
    'Moneda..
    Dim Banderamoneda As String = "COLON"
    'Bandera para cargo a habitacion..
    Dim BaderaCargoHabitacion As Boolean = False
    'Bandera para impuesto de ventas
    Dim BanderaImpuestoVentas As Boolean = False
    'Bandera par el impuesto a servicio..
    Dim BanderaImpuestoServicio As Boolean = False
    'Bandera para el descuento
    Dim BanderaDescuento As Boolean = False
    'Bandera para las cortesias
    Dim BanderaCortesia As Boolean = False
    Dim BanderaServicioRestaurante As Boolean = False


    Public Sub New(ByRef _idComanda As String, ByRef _numeroSeparada As String, ByRef _mesa As String)

        ' Llamada necesaria para el Dise�ador de Windows Forms.
        InitializeComponent()

        'Agregamos las inicializaciones de los numeros de comandas..
        Me.idComanda = _idComanda
        Me.numeroSeparada = _numeroSeparada
        Me.mesa = _mesa

    End Sub


    'Calculamos los totales de la factura..
    Public Sub calcularTotales()

        Me.subtotal = 0
        Me.descuento = 0
        Me.impServ = 0
        Me.impVent = 0
        Me.extraProp = 0
        Me.total = 0
        Me.cantidad = 0

        If datos.Rows.Count > 0 Then

            'Consultamos la moneda del restaurante.. si es dolar, entonces seleccionamos los calculos como dolares..
            If Me.conf.MonedaRestaurante = "1" Then
                Me.tipoCambio = IIf(Me.Banderamoneda = "COLON", 1, Double.Parse(Me.conf.obtenerTipoCambio()))
            End If
            If Me.conf.MonedaRestaurante = "2" Then
                Me.tipoCambio = IIf(Me.Banderamoneda = "DOLAR", (1 / Double.Parse(Me.conf.obtenerTipoCambio())), 1)
            End If

            Dim r As DataRow

            'Calculamos el total de los articulos listados en el grid.. o en el datatable Datos..
            For Each r In Me.datos.Rows

                '
                'VARIABLES QUE MANEJAN LOS TOTALES GLOBALES DEL DETALLE..
                '
                Me.subtotal += ((r("Cant") * r("Precio"))) / Me.tipoCambio
                Me.impVent += IIf(Me.BanderaImpuestoVentas = True, r("IVentas") / Me.tipoCambio, 0)

                'Si aplica impuesto servicio habitacion.. se lo agregamos al impuesto de servicio..
                Me.impServ += IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")) / Me.tipoCambio, 0)

                'Me.impServ += r("IServicios")/ me.tipoCambio 
                Me.descuento += IIf(Me.BanderaDescuento = True, ((r("Cant") * r("Precio")) * (r("descuento") / 100)) / Me.tipoCambio, 0)
                Me.total += (((r("Cant") * r("Precio")) - IIf(Me.BanderaDescuento = True, ((r("Cant") * r("Precio")) * (r("descuento") / 100)), 0) + IIf(Me.BanderaImpuestoVentas = True, r("IVentas"), 0) + IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")), 0))) / Me.tipoCambio

            Next

            Me.txtSubtotal.Text = Format(Int(Me.subtotal), "###,###,###.#0")
            Me.txtDescuento.Text = Format(Int(Me.descuento), "###,###,###.#0")
            Me.txtImpServ.Text = Format(Int(Me.impServ), "###,###,###.#0")
            Me.txtImpVentas.Text = Format(Int(Me.impVent), "###,###,###.#0")
            Me.txtTotal.Text = Format(Int(Me.total), "###,###,###.#0")

            If Me.subtotal = 0 Then Me.txtSubtotal.Text = "0.00"
            If Me.descuento = 0 Then Me.txtDescuento.Text = "0.00"
            If Me.impServ = 0 Then Me.txtImpServ.Text = "0.00"
            If Me.impVent = 0 Then Me.txtImpVentas.Text = "0.00"
            If Me.total = 0 Then Me.txtTotal.Text = "0.00"

        End If
    End Sub

    'Registramos la nueva factura..
    Public Sub guardarNuevaFactura()
        '------------------------------
        Dim miComanda As New cls_Comandas()

        'Llenamos lso datos de la factura..
        Me.miFacturas.tipo = Me.tipo_pago
        Me.miFacturas.cod_cliente = Me.id_cliente
        Me.miFacturas.nombre_cliente = Me.txtCliente.Text
        Me.miFacturas.cedula_usuario = User_Log.Cedula
        Me.miFacturas.subtotal = Me.subtotal.ToString()
        Me.miFacturas.descuento = Me.descuento.ToString()
        Me.miFacturas.imp_venta = Me.impVent.ToString()
        Me.miFacturas.total = Me.total.ToString()

        'Me.miFacturas.cod_moneda = Me.conf.MonedaRestaurante

        Me.miFacturas.cod_moneda = IIf(Me.Banderamoneda = "COLON", "1", "2")
        Me.miFacturas.moneda_nombre = Banderamoneda

        Me.miFacturas.subtotalgravada = Me.subtotal.ToString()

        Me.miFacturas.tipo_cambio = IIf(Me.tipoCambio = 1, 1, Me.conf.obtenerTipoCambio())

        Me.miFacturas.id_reservacion = Me.id_reservacion
        Me.miFacturas.monto_saloero = Me.impServ.ToString()
        Me.miFacturas.proveniencia_venta = Me.conf.obtenerIdPuntoVenta()
        Me.miFacturas.descripcion = GetSetting("Seesoft", "Restaurante", "LastPoint")
        Me.miFacturas.tipocambiodolar = IIf(Me.tipoCambio = 1, 1, Me.conf.obtenerTipoCambio())
        Me.miFacturas.extrapropina = Me.txtExtraProp.Text
        Me.miFacturas.mesa = Me.mesa


        'guardamos el encabezado de las facturas..
        Me.miFacturas.guardarNuevaFactura()


        'Creamos la comanda..
        miComanda.numeroComanda = Me.idComanda
        miComanda.Subtotal = Me.subtotal.ToString()
        miComanda.impuesto_venta = Me.impVent.ToString()
        miComanda.total = Me.miFacturas.total
        miComanda.cedusuario = User_Log.Cedula
        miComanda.cedsalonero = miComanda.cedusuario
        miComanda.idMesa = Me.mesa
        miComanda.comenzales = "1"
        miComanda.facturado = "1"
        miComanda.numerofactura = Me.miFacturas.num_factura
        miComanda.cuentacontable = Me.cuenta_contable_cortesia
        miComanda.cortesia = "1"
        miComanda.numeroCortesia = "0"
        miComanda.cod_moneda = Me.conf.MonedaRestaurante
        miComanda.nuevoComanda()

        Dim idComandaGenerado As Integer = Integer.Parse(miComanda.obtenerUltimoIdComandaGenerado())

        ' Creamos el detalle de las facturas..  en ventasdetalle
        If datos.Rows.Count > 0 Then

            'Consultamos la moneda del restaurante.. si es dolar, entonces seleccionamos los calculos como dolares..
            If Me.conf.MonedaRestaurante = "1" Then
                Me.tipoCambio = IIf(Me.Banderamoneda = "COLON", Double.Parse(Me.conf.obtenerTipoCambio()), 1)
            End If
            If Me.conf.MonedaRestaurante = "2" Then
                Me.tipoCambio = IIf(Me.Banderamoneda = "DOLAR", 1, (1 / Double.Parse(Me.conf.obtenerTipoCambio())))
            End If

            Dim r As DataRow

            'Calculamos el total de los articulos listados en el grid.. o en el datatable Datos..
            For Each r In Me.datos.Rows
                Me.miFacturasDetalle.Id_Factura = Me.miFacturas.Id
                Me.miFacturasDetalle.Codigo = r("cProducto").ToString()
                Me.miFacturasDetalle.Descripcion = r("Desc").ToString()

                Me.miFacturasDetalle.Cantidad = r("Cant").ToString()
                Me.miFacturasDetalle.Precio_Costo = (((r("Cant") * r("Precio"))) / Me.tipoCambio).ToString()
                Me.miFacturasDetalle.TipoCambioCosto = "1"
                Me.miFacturasDetalle.Precio_Base = (((r("Cant") * r("Precio"))) / Me.tipoCambio).ToString()
                Me.miFacturasDetalle.Precio_Flete = "0"
                Me.miFacturasDetalle.Precio_Otros = "0"
                Me.miFacturasDetalle.Precio_Unit = (((r("Cant") * r("Precio"))) / Me.tipoCambio).ToString()
                Me.miFacturasDetalle.Descuento = r("descuento").ToString()
                Me.miFacturasDetalle.Monto_Descuento = IIf(Me.BanderaDescuento = True, ((r("Cant") * r("Precio")) * (r("descuento") / 100)) / Me.tipoCambio, 0).ToString()
                Me.miFacturasDetalle.Impuesto = IIf(Me.BanderaImpuestoVentas = True, "13", "0")
                Me.miFacturasDetalle.Monto_Impuesto = IIf(Me.BanderaImpuestoVentas = True, r("IVentas") / Me.tipoCambio, 0)
                Me.miFacturasDetalle.SubtotalGravado = IIf(r("IVentas") <> 0, (((r("Cant") * r("Precio"))) + IIf(Me.BanderaImpuestoVentas = True, r("IVentas") / Me.tipoCambio, 0) + IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")) / Me.tipoCambio, 0)), 0).ToString()
                Me.miFacturasDetalle.SubTotalExcento = IIf(r("IVentas") = 0, ((r("Cant") * r("Precio")) + IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")) / Me.tipoCambio, 0)), 0).ToString()
                Me.miFacturasDetalle.SubTotal = (r("Cant") * r("Precio"))
                'Me.miFacturasDetalle.SubTotal = (((r("Cant") * r("Precio"))) + IIf(Me.BanderaImpuestoVentas = True, r("IVentas") / Me.tipoCambio, 0) + IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")) / Me.tipoCambio, 0)).ToString()
                Me.miFacturasDetalle.Devoluciones = "0"
                Me.miFacturasDetalle.Numero_Entrega = "0"
                Me.miFacturasDetalle.Max_Descuento = "0"
                Me.miFacturasDetalle.Tipo_Cambio_ValorCompra = Me.conf.obtenerTipoCambioValorCompra()
                Me.miFacturasDetalle.Cod_MonedaVenta = Me.conf.MonedaRestaurante
                Me.miFacturasDetalle.Impuesto_Ict = "0"
                Me.miFacturasDetalle.Monto_Impuesto_Ict = "0"
                Me.miFacturasDetalle.Propina = IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish = True, (User_Log.ISH) + 10, 10), 0).ToString()
                Me.miFacturasDetalle.Monto_Propina = IIf(Me.BanderaImpuestoServicio = True, IIf(Me.Banderaish, r("IServicios") + ((r("Cant") * r("Precio")) * (User_Log.ISH / 100)), r("IServicios")) / Me.tipoCambio, 0).ToString()
                Me.miFacturasDetalle.IdOperador = "0"
                Me.miFacturasDetalle.nuevoDetalle()

                'Agregamos datos a comandadetalle...
                miComanda.nuevoDetalleMenuComanda(idComandaGenerado.ToString(), r("cProducto").ToString(), r("Cant").ToString(), Me.miFacturasDetalle.Precio_Unit, Me.miFacturasDetalle.SubTotal, Me.miFacturasDetalle.Monto_Impuesto, Me.miFacturasDetalle.Monto_Propina, Me.miFacturas.cedula_usuario, Me.miFacturasDetalle.Precio_Base)
            Next
        End If

    End Sub

    'obtenemos el maximo numero de cortesia..
    Public Function obtenerNumeroCortesia() As Integer
        Dim dt As New DataTable
        dt = Me.f.fireSQL("Select isnull(MAX(NumeroCortesia)+1,0) as NumeroCortesia from Comanda")
        Return dt.Rows(0)(0)
    End Function

    'Registramos una cortesia..
    Private Function guardarNuevaCortesia() As Integer
        Try
            Dim miCortesia As New cls_Cortesias()
            Dim miComanda As New cls_Comandas()
            Dim miMesa As New cls_Mesas()

            Dim codigo(100) As Integer
            Dim autorizadox, Observacionesx As String
            Dim ee As Integer = 0
            Dim observaciones As String
            Dim numero_cortesia As Int64
            Dim dt As DataTable
            Dim Global_subtotal As Double

            Dim detalle_cortesia As New FrmCortesia
            detalle_cortesia.rtcObservaciones.Text = Me.nombre_cortesia + "  " + Me.observacion_cortesia
            detalle_cortesia.ShowDialog()
            autorizadox = detalle_cortesia.autorizado
            observaciones = detalle_cortesia.observaciones
            Observacionesx = observaciones
            detalle_cortesia.Dispose()

            If detalle_cortesia.completa = False Then
                Return 0
                Exit Function
            End If

            numero_cortesia = Me.miFacturas.obtenerNumeroCortesia()

            dt = Me.miFacturas.obtenerDatosCortesias(Me.idComanda, Me.numeroSeparada)

            Dim contador As Integer = 0
            Dim i As Integer = 1
            Global_subtotal = 0
            Dim idComandaGenerado As Integer

            Dim d As DataRow
            Dim oo As Integer
            Dim totalCortesia As Double = 0
            Dim preciocosto As Double

            For oo = 0 To contador - 1
                preciocosto += Me.miFacturas.obtenerPrecioCostoArticulo(codigo(oo))
                totalCortesia = totalCortesia + preciocosto
            Next

            miCortesia.numero_cortesia = numero_cortesia.ToString()
            miCortesia.autorizado = autorizadox
            miCortesia.observaciones = Observacionesx
            miCortesia.cuenta_contable = Me.cuenta_contable_cortesia
            miCortesia.descripcion_cuenta = Me.descripcion_cortesia
            miCortesia.id_centro = Me.Id_Centro_Cortesia
            miCortesia.nuevaCortesia()

            miComanda.numeroComanda = Me.idComanda
            miComanda.Subtotal = Me.txtSubtotal.Text
            miComanda.impuesto_venta = Me.txtImpVentas.Text
            miComanda.total = totalCortesia.ToString()
            miComanda.cedusuario = User_Log.Cedula
            miComanda.cedsalonero = miComanda.cedusuario
            miComanda.idMesa = Me.mesa
            miComanda.comenzales = "1"
            miComanda.facturado = "1"
            miComanda.numerofactura = "0"
            miComanda.cuentacontable = Me.cuenta_contable_cortesia
            miComanda.cortesia = "1"
            miComanda.numeroCortesia = numero_cortesia.ToString()
            miComanda.cod_moneda = Me.conf.MonedaRestaurante
            miComanda.nuevoComanda()

            idComandaGenerado = Integer.Parse(miComanda.obtenerUltimoIdComandaGenerado())

            For Each d In dt.Rows
                codigo(contador) = d("cProducto")
                Global_subtotal = Global_subtotal + (d("cCantidad") * d("costo_real"))
                contador = contador + 1
                'Creamos el registro del detalle de la comanda...
                miComanda.nuevoDetalleMenuComanda(idComandaGenerado, d("cProducto").ToString(), d("cCantidad").ToString(), d("cPrecio").ToString(), (d("cCantidad") * d("cPrecio")).ToString(), "13", "10", User_Log.Cedula, d("costo_real"))
            Next
            '-------------------------------------------------------------------
            Me.txtSubtotal.Text = Format(Global_subtotal, "#,##0.00")
            Me.txtImpServ.Text = "0.00"
            Me.txtImpVentas.Text = "0.00"
            txtTotal.Text = Format(Global_subtotal, "#,##0.00")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    'Cargamos el detalle de la factura en el grid..
    Public Sub cargarDetalle()

        Me.datos = Me.miFacturas.obtenerDetalleFactura(Me.numeroSeparada, Me.idComanda)
        Me.dgDetalle.DataSource = Me.datos
        Me.dgDetalle.Columns("tipo_plato").Visible = False
        Me.dgDetalle.Columns("cProducto").Visible = False
        Me.dgDetalle.Columns("cCodigo").Visible = False
        Me.dgDetalle.Columns("primero").Visible = False
        Me.dgDetalle.Columns("cedula").Visible = False
        Me.dgDetalle.Columns("hora").Visible = False
        Me.dgDetalle.Columns("Express").Visible = False
        Me.dgDetalle.Columns("IVentas").Visible = False
        Me.dgDetalle.Columns("IServicios").Visible = False
        Me.dgDetalle.Columns("descuento").Visible = False

        '300 .. - .. - .. - .. - .. - .. - .. - ..
        Me.dgDetalle.Columns("Cant").Width = 40
        Me.dgDetalle.Columns("Desc").Width = 180
        Me.dgDetalle.Columns("Precio").Width = 80
        Me.dgDetalle.Columns("Precio").DefaultCellStyle.Format = "#,##0.00"

        'Si la moneda es dolar.. entonces por defecto seleccionamso esa moneda en la factura..
        If Me.conf.MonedaRestaurante = "2" Then
            Me.seleccionarDolares()
        End If
        If Me.conf.MonedaRestaurante = "1" Then
            Me.seleccionarColones()
        End If

        'por defecto cargamos contado por defecto..
        Me.seleccionarContado()
        'Seleccionamos el impuesto de ventas
        Me.seleccionarImpuestoVentas()
        'seleccionamos el impuesto de servicio..
        Me.seleccionarImpuestoServicio()
        'prpeguntamos si la factura tiene aplicado un descuento..'
        If Me.miFacturas.saberDescuentoFactura(Me.numeroSeparada, Me.idComanda) = True Then
            Me.BanderaDescuento = True
            Me.pbDescuento.BackgroundImage = My.Resources.cuadro2
            Me.Label14.BackColor = Color.LightSkyBlue
        End If
    End Sub

    'nos damos cuenta si la factura tiene ya aplicado un descuento
    Public Function saberDescuentoFactura(ByRef _numeroSeparado As String, ByRef _numeroComanda As String) As Boolean
        Dim str As String
        Dim dt As DataTable

        str = "select distinct  descuento from ComandaTemporal WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado
        dt = Me.f.fireSQL(str)

        'si devuelve datos..
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) = 0 Then
                Return False
            Else
                Return True
            End If
        End If

        Return False
    End Function


    'Cargamos los datos iniciales..
    Public Sub cargarDatos()

        Me.conf.obtenerConfiguraciones()
        'Cargamos el detalle de los articulos de la factura..
        Me.cargarDetalle()
        'Mostramos los calculos de la factura
        Me.calcularTotales()

    End Sub

    'Buscamos una cortesia..
    Private Sub buscacuentaCortesia()

        Dim fbuscador As New frmBuscarCortesia()
        Dim dt As DataTable

        fbuscador.Text = "Buscando Centro de Cortesia"
        fbuscador.cargarDatos()

        'Si solo hay una cuenta de cortesia, entonces no muestra la pantalla
        'y simplemente la asigna como valor por defecto.. si no hay nada,
        ' entonces se devuelve al valor por defecto de la factura de contado..
        If fbuscador.DataGridView1.Rows.Count = 1 Then

            Id_Centro_Cortesia = fbuscador.datos.Rows(0)("Id_Centro")
            dt = Me.miFacturas.obtenerCuentaContableCortesia(Id_Centro_Cortesia)

            cuenta_contable_cortesia = dt.Rows(0)("CuentaContable")
            descripcion_cortesia = dt.Rows(0)("DescripcionCuenta")
            observacion_cortesia = dt.Rows(0)("Observaciones")
            nombre_cortesia = dt.Rows(0)("Nombre")
            Me.txtCliente.Text = fbuscador.nombreCentro

            Exit Sub
        Else
            fbuscador.ShowDialog()
        End If

        If fbuscador.idCentro = "" Then
            Me.seleccionarContado()
            Exit Sub
        End If

        Id_Centro_Cortesia = fbuscador.idCentro
        dt = Me.miFacturas.obtenerCuentaContableCortesia(Id_Centro_Cortesia)

        cuenta_contable_cortesia = dt.Rows(0)("CuentaContable")
        descripcion_cortesia = dt.Rows(0)("DescripcionCuenta")
        observacion_cortesia = dt.Rows(0)("Observaciones")
        nombre_cortesia = dt.Rows(0)("Nombre")
        Me.txtCliente.Text = fbuscador.nombreCentro


    End Sub

    'Buscamos un cliente para credito..
    Public Sub seleccionarClienteCredito()
        Dim frmBuscaCliente As New frmBuscarCliente()
        frmBuscaCliente.ShowDialog()

        Me.txtCliente.Text = frmBuscaCliente.nombre
        Me.id_cliente = frmBuscaCliente.pos

    End Sub

    'Buscamos un cliente para contado..
    Public Sub seleccionarClienteContado()
        Dim frmBuscaCliente As New frmBuscarCliente()
        frmBuscaCliente.ShowDialog()

        If frmBuscaCliente.nombre = "" Then
            Me.txtCliente.Text = "Cliente de Contado"
            Me.id_cliente = "1"
        Else
            Me.txtCliente.Text = frmBuscaCliente.nombre
            Me.id_cliente = frmBuscaCliente.pos
        End If


    End Sub

    'Buscamos comisionista..
    Public Sub seleccionarComisionista()
        Dim frmBuscaCliente As New frmBuscarCliente()
        frmBuscaCliente.ShowDialog()

        Me.txtComisionista.Text = frmBuscaCliente.nombre
        Me.id_comisionista = frmBuscaCliente.pos
    End Sub

    'Seleccionamos que la factura sea de contado..
    Public Sub seleccionarContado()
        tipo_pago = "CON"
        Me.pbContado.BackgroundImage = My.Resources.cuadro2
        Me.Label5.BackColor = Color.LightSkyBlue

        Me.pbCredito.BackgroundImage = My.Resources.cuadro1
        Me.Label1.BackColor = Color.White

        Me.pbCargoHab.BackgroundImage = My.Resources.cuadro1
        Me.Label2.BackColor = Color.White

        Me.pbCortesia.BackgroundImage = My.Resources.cuadro1
        Me.Label3.BackColor = Color.White

        Me.pbServH.BackgroundImage = My.Resources.cuadro1
        Me.Label4.BackColor = Color.White

    End Sub

    'Seleccionamos una factura de credito..
    Public Sub seleccionarCredito()
        Me.BanderaCortesia = False
        tipo_pago = "CRE"
        Me.pbContado.BackgroundImage = My.Resources.cuadro1
        Me.Label5.BackColor = Color.White

        Me.pbCredito.BackgroundImage = My.Resources.cuadro2
        Me.Label1.BackColor = Color.LightSkyBlue

        Me.pbCargoHab.BackgroundImage = My.Resources.cuadro1
        Me.Label2.BackColor = Color.White

        Me.pbCortesia.BackgroundImage = My.Resources.cuadro1
        Me.Label3.BackColor = Color.White

        Me.pbServH.BackgroundImage = My.Resources.cuadro1
        Me.Label4.BackColor = Color.White
    End Sub

    'Seleccionamos hacer un cargo a la habitacion..
    Public Sub seleccionarCargoHabitacion()

        Me.BaderaCargoHabitacion = True
        Me.BanderaCortesia = False

        Me.pbContado.BackgroundImage = My.Resources.cuadro1
        Me.Label5.BackColor = Color.White

        Me.pbCredito.BackgroundImage = My.Resources.cuadro1
        Me.Label1.BackColor = Color.White

        Me.pbCargoHab.BackgroundImage = My.Resources.cuadro2
        Me.Label2.BackColor = Color.LightSkyBlue

        Me.pbCortesia.BackgroundImage = My.Resources.cuadro1
        Me.Label3.BackColor = Color.White

        Me.pbServH.BackgroundImage = My.Resources.cuadro1
        Me.Label4.BackColor = Color.White

        '------------------------------------------------

        tipo_pago = "CAR"

        Dim fBuscadorHuesped As New BuscadorHuesped
        fBuscadorHuesped.ShowDialog()

        txtCliente.Text = fBuscadorHuesped.nombre
        Me.id_cliente = fBuscadorHuesped.idCliente
        Me.id_reservacion = fBuscadorHuesped.idReservacion
        fBuscadorHuesped.Dispose()

        'Si no se selecciona un nombre de cliente.. entonces se selecciona la opcion por defecto
        ' que es factura de contado..
        If Me.id_cliente = "" Or txtCliente.Text = "" Then
            Me.seleccionarContado()
            Me.tipo_pago = "CON"
            txtCliente.Text = "CLIENTE CONTADO"
            Me.id_cliente = "0"
        End If

    End Sub

    'Seleccionamos hacer una cortesia..
    Public Sub seleccionarCortesia()
        BanderaCortesia = True

        Me.pbContado.BackgroundImage = My.Resources.cuadro1
        Me.Label5.BackColor = Color.White

        Me.pbCredito.BackgroundImage = My.Resources.cuadro1
        Me.Label1.BackColor = Color.White

        Me.pbCargoHab.BackgroundImage = My.Resources.cuadro1
        Me.Label2.BackColor = Color.White

        Me.pbCortesia.BackgroundImage = My.Resources.cuadro2
        Me.Label3.BackColor = Color.LightSkyBlue

        Me.pbServH.BackgroundImage = My.Resources.cuadro1
        Me.Label4.BackColor = Color.White


    End Sub

    'Seleccionamos hacer un servicio a la habitacion..
    Public Sub seleccionarServicioHabitacion()

        If Me.Label4.BackColor = Color.White Then
            Me.Banderaish = True
            Me.pbServH.BackgroundImage = My.Resources.cuadro2
            Me.Label4.BackColor = Color.LightSkyBlue
        Else
            Me.Banderaish = False
            Me.pbServH.BackgroundImage = My.Resources.cuadro1
            Me.Label4.BackColor = Color.White
        End If
    End Sub

    'Seleccionamos el impuesto de ventas..
    Public Sub seleccionarImpuestoVentas()
        If Me.Label16.BackColor = Color.White Then
            Me.BanderaImpuestoVentas = True
            Me.pbImpVentas.BackgroundImage = My.Resources.cuadro2
            Me.Label16.BackColor = Color.LightSkyBlue
        Else
            Me.BanderaImpuestoVentas = False
            Me.pbImpVentas.BackgroundImage = My.Resources.cuadro1
            Me.Label16.BackColor = Color.White
        End If
    End Sub

    'Seleccionamos el impuesto de servicio..
    Public Sub seleccionarImpuestoServicio()

        If Me.Label15.BackColor = Color.White Then
            Me.BanderaImpuestoServicio = True
            Me.pbImpServ.BackgroundImage = My.Resources.cuadro2
            Me.Label15.BackColor = Color.LightSkyBlue
        Else
            Me.BanderaImpuestoServicio = False
            Me.pbImpServ.BackgroundImage = My.Resources.cuadro1
            Me.Label15.BackColor = Color.White
        End If
    End Sub

    'Aplicamos un descuento a las comandas..
    Public Sub aplicarDescuentoComandas(ByRef _descuento As String, ByRef _numeroComanda As String, ByRef _numeroSeparado As String)

        Dim str As String = "Update ComandaTemporal " & _
                                  " SET descuento = " & _descuento & "" & _
                                  " WHERE cCodigo=" & _numeroComanda & " and separada= " & _numeroSeparado

        Me.f.fireSQLNoReturn(str)

    End Sub


    'Seleccionamos el descuento.
    Public Sub seleccionarDescuento()

        If Me.Label14.BackColor = Color.White Then

            'Aplica el descuento..
            Dim descuento As Double = Me.miFacturas.obtenerDescuentoFactura(Me.numeroSeparada, Me.idComanda)

            Dim rDesc As New registro
            rDesc.Text = "Digite el descuento a aplicar"
            If descuento > 0 Then
                rDesc.txtCodigo.Text = descuento
            End If
            rDesc.ShowDialog()
            Try
                descuento = CDbl(rDesc.txtCodigo.Text)
            Catch ex As Exception
                descuento = 0
            End Try
            rDesc.Dispose()
            rDesc.Text = ""

            'Aplicamos el descuento a las comandas..
            Me.miFacturas.aplicarDescuentoComandas(descuento.ToString(), Me.idComanda, Me.numeroSeparada)
            'actualizamos los datos de los articulos..
            Me.datos = Me.miFacturas.obtenerDetalleFactura(Me.numeroSeparada, Me.idComanda)

            Me.BanderaDescuento = True
            Me.pbDescuento.BackgroundImage = My.Resources.cuadro2
            Me.Label14.BackColor = Color.LightSkyBlue

        Else

            Me.BanderaDescuento = False
            Me.pbDescuento.BackgroundImage = My.Resources.cuadro1
            Me.Label14.BackColor = Color.White
            Me.miFacturas.aplicarDescuentoComandas("0", Me.idComanda, Me.numeroSeparada)

        End If
    End Sub


    'Obtenemos el monto del descuento de la factura..
    Public Function obtenerDescuentoFactura(ByRef _numeroSeparado As String, ByRef _numeroComanda As String) As Double
        Dim str As String
        Dim dt As DataTable

        str = "select distinct  descuento from ComandaTemporal WHERE cCodigo= " & _numeroComanda & " and separada= " & _numeroSeparado
        dt = Me.f.fireSQL(str)
        Return dt.Rows(0)(0)
    End Function
    'Seleccionamos la moneda..
    Public Sub seleccionarColones()

        Me.Banderamoneda = "COLON"

        Me.pbColones.BackgroundImage = My.Resources.cuadro2
        Me.Label12.BackColor = Color.LightSkyBlue

        Me.pbDolares.BackgroundImage = My.Resources.cuadro1
        Me.Label11.BackColor = Color.White

        Me.calcularTotales()

    End Sub

    'Seleccionamos la moneda..
    Public Sub seleccionarDolares()

        Me.Banderamoneda = "DOLAR"
        Me.pbColones.BackgroundImage = My.Resources.cuadro1
        Me.Label12.BackColor = Color.White

        Me.pbDolares.BackgroundImage = My.Resources.cuadro2
        Me.Label11.BackColor = Color.LightSkyBlue

        Me.calcularTotales()

    End Sub

    'limpiamos todos los campos..
    Public Sub limpiarCampos()


        Me.txtCliente.Text = ""
        Me.txtComisionista.Text = ""
        Me.txtSubtotal.Text = ""
        Me.txtDescuento.Text = ""
        Me.txtImpVentas.Text = ""
        Me.txtImpServ.Text = ""
        Me.txtExtraProp.Text = ""
        Me.txtTotal.Text = ""

    End Sub

    Function OpciondePago(ByVal Total As Double) As Boolean
        Dim Movimiento_Pago_Abonos As New frmMovimientoCajaPagoAbono

        Dim dtDatosFac As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT Cedula_Usuario, Total, Fecha, Num_Factura, Proveniencia_Venta, Num_Apertura FROM Ventas WHERE  (Id = " & Me.miFacturas.Id & ")", dtDatosFac)

        Movimiento_Pago_Abonos.Factura = dtDatosFac.Rows(0).Item("Num_Factura")
        Movimiento_Pago_Abonos.fecha = Now.Date
		Movimiento_Pago_Abonos.Total = Math.Round(Total, 2)
		Movimiento_Pago_Abonos.Tipo = "FVC" 'columna!tipo
        Movimiento_Pago_Abonos.codmod = Me.miFacturas.cod_moneda

        Movimiento_Pago_Abonos.cedu = dtDatosFac.Rows(0).Item("Cedula_usuario")
        Movimiento_Pago_Abonos.cambioFP = dtDatosFac.Rows(0).Item("Num_Apertura")

        Dim dtusua As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select Nombre From Usuarios Where Cedula = '" & dtDatosFac.Rows(0).Item("Cedula_usuario") & "'", dtusua)
        Movimiento_Pago_Abonos.nombre = dtusua.Rows(0).Item(0)

        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT IdPuntoVenta, Nombre, Tipo, BaseDatos FROM PuntoVenta WHERE     (IdPuntoVenta = " & dtDatosFac.Rows(0).Item("Proveniencia_Venta") & ")", dt, GetSetting("SeeSoft", "Hotel", "Conexion"))

        Dim cx As New Conexion
        cx.Conectar(dt.Rows(0).Item("BaseDatos"))
        cx.SlqExecute(cx.sQlconexion, "DELETE OpcionesDePago WHERE Documento = " & dtDatosFac.Rows(0).Item("Num_Factura") & " AND Numapertura = " & dtDatosFac.Rows(0).Item("Num_Apertura"))

        cx.DesConectar(cx.sQlconexion)
        Movimiento_Pago_Abonos.conexion1 = GetSetting("SeeSoft", dt.Rows(0).Item("BaseDatos"), "CONEXION")
        Movimiento_Pago_Abonos.ShowDialog()

    End Function


    'imprimimos una factura de forma manual..
    Public Sub imprimir()

        Dim impresora As String
        impresora = Busca_Impresora()
        If impresora = "" Then
            MessageBox.Show("No se seleccion� ninguna impresora", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            impresora = Busca_Impresora()
            If impresora = "" Then
                Exit Sub
            End If
        End If

        Me.miFacturas.ImprimirFacturaEsp(Me.miFacturas.num_factura, Me.miFacturas.numeroComanda, impresora, BanderaServicioRestaurante)

    End Sub


    'Imprimimos la factura..
    Private Sub Imprimir(ByVal NFactura As Integer)

        Try


            Dim facturaPVE
            Dim NFactura2 As Integer = Integer.Parse(Me.miFacturas.num_factura)
            Dim dt As New DataTable
            Dim cedula As String
            Dim salon As String = ""
            Dim cnx As New Conexion
            Dim impresion As String = GetSetting("SeeSoft", "Restaurante", "Impresion")
            If impresion.Equals("") Then
                SaveSetting("SeeSoft", "Restaurante", "Impresion", "0")
            End If

            dt = Me.miFacturas.obtenerCedulaSalonero(NFactura2)
            cedula = dt.Rows(0)(0)

            If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                facturaPVE = New RptFacturaPVE1
            ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                facturaPVE = New RptFacturaPVECB
            Else
                facturaPVE = New RptFacturaPVE  'Generica
            End If

            If Me.BanderaServicioRestaurante Then
                facturaPVE = New RptFacturaPVE_SR
            End If

            Dim impresora As String
            impresora = Busca_Impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccion� ninguna impresora", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = Busca_Impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If

            Dim Conexion As String = GetSetting("SeeSOFT", "Restaurante", "Conexion")

            salon = dt.Rows(0)(0).ToString()

            CrystalReportsConexion.LoadReportViewer(Nothing, facturaPVE, True, Conexion)

            facturaPVE.PrintOptions.PrinterName = impresora
            facturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            facturaPVE.SetParameterValue(0, NFactura2)
            facturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
            facturaPVE.SetParameterValue(2, 0)
            facturaPVE.SetParameterValue(3, False)

            If Me.BanderaServicioRestaurante Then
            Else
                facturaPVE.SetParameterValue(4, salon)
            End If
            facturaPVE.PrintToPrinter(1, True, 0, 0)
            facturaPVE = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    'Imprimimos la factura usando la forma rapida de impresion..
    Public Sub imprimirmyFactura()


        'Buscamos la impresora predeterminada..
        Dim impresoraPredeterminada As String = GetSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura")
        If impresoraPredeterminada <> "" Then
            Me.miFacturas.ImprimirFacturaEsp(Me.miFacturas.num_factura, Me.idComanda, impresoraPredeterminada, BanderaServicioRestaurante)
        Else
            Me.miFacturas.ImprimirFacturaEsp(Me.miFacturas.num_factura, Me.idComanda, Busca_Impresora(), BanderaServicioRestaurante)
        End If



    End Sub

    'limpiamos todo lo de las comandas..
    Public Sub limpiarComandaTemporal()
        Dim miComanda As New cls_Comandas()
        miComanda.borrarComandaTemporal(Me.numeroSeparada, Me.idComanda)
    End Sub

    'guardamos una nueva factura 
    Public Sub registrarFactura()

        Dim miMesa As New cls_Mesas()
        miMesa.obtenerMesa(Me.mesa)

        'preguntamos si el usuario tiene asociada una apertura.. si no es asi, entonces
        'no lo dejamos realizar una factura...
        If Me.miFacturas.obtenerNumeroApertura(User_Log.Cedula) <> "" Then
            Me.guardarNuevaFactura()

            If Me.miFacturas.tipo = "CON" Then
                Me.OpciondePago(Double.Parse(Me.miFacturas.total))
            End If


            If GetSetting("SeeSoft", "Restaurante", "Rapido") = 1 Then
                Me.imprimir()
            Else
                Me.imprimir(Integer.Parse(Me.miFacturas.Id))
            End If

            'Limpiamos todos los campos y salimos..
            Me.limpiarComandaTemporal()

            Me.limpiarCampos()

            miMesa.restablecerNombreMesa()
            miMesa.activa = "0"
            miMesa.actualizar()
            Me.Close()
            Me.result = True
        Else
            'Si el usuario no tiene asociada una apertura.. entonces le consultamos si desea
            'crear una...
            Dim frm As New frmQuestion()
            frm.LblMensaje.Text = "El usuario actual no tiene asociado una apertura, desea abrir una nueva caja ?"
            frm.ShowDialog()
            If frm.resultado Then
                Dim frmApertura As New frmAperturaCaja()
                frmApertura.ShowDialog()
            Else
                Me.Close()
            End If

        End If

    End Sub






    '------------------------------------------------------------------------------------------------------

    'Carga del formulario..
    Private Sub frmFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.cargarDatos()

    End Sub

    Private Sub pbContado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbContado.Click, Label5.Click

        Me.seleccionarContado()

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub pbServH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbServH.Click, Label4.Click
        Me.seleccionarServicioHabitacion()
        Me.calcularTotales()
    End Sub

    Private Sub pbCredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbCredito.Click, Label1.Click
        Me.seleccionarCredito()
        Me.seleccionarClienteCredito()
    End Sub

    Private Sub pbCargoHab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbCargoHab.Click, Label2.Click
        Me.seleccionarCargoHabitacion()
    End Sub

    Private Sub pbCortesia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbCortesia.Click, Label3.Click
        Me.seleccionarCortesia()
        Me.buscacuentaCortesia()
    End Sub

    Private Sub txtTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotal.TextChanged

    End Sub

    Private Sub txtExtraProp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExtraProp.TextChanged

    End Sub

    Private Sub txtImpVentas_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImpVentas.TextChanged

    End Sub

    Private Sub txtImpServ_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImpServ.TextChanged

    End Sub

    Private Sub txtDescuento_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescuento.TextChanged

    End Sub

    Private Sub txtSubtotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSubtotal.TextChanged

    End Sub

    Private Sub pbColones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbColones.Click, Label12.Click
        Me.seleccionarColones()
    End Sub

    Private Sub pbDolares_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbDolares.Click, Label11.Click
        Me.seleccionarDolares()
    End Sub

    Private Sub pbCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbCliente.Click, Label7.Click
        Me.seleccionarClienteContado()
        Me.seleccionarContado()
    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub pbComisionista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbComisionista.Click, Label8.Click
        Me.seleccionarComisionista()
    End Sub

    Private Sub pbImpVentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbImpVentas.Click, Label16.Click
        Me.seleccionarImpuestoVentas()
        Me.calcularTotales()
    End Sub

    Private Sub pbImpServ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbImpServ.Click, Label15.Click
        Me.seleccionarImpuestoServicio()
        Me.calcularTotales()
    End Sub

    Private Sub pbDescuento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbDescuento.Click, Label14.Click
        Me.seleccionarDescuento()
        Me.calcularTotales()
    End Sub

   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim miMesa As New cls_Mesas()
        miMesa.obtenerMesa(Me.mesa)

        If Me.BanderaCortesia Then
            Me.guardarNuevaCortesia()
            'Limpiamos todos los campos y salimos..
            Me.limpiarCampos()
            Me.limpiarComandaTemporal()
            miMesa.restablecerNombreMesa()
            miMesa.activa = "0"
            miMesa.actualizar()
            Me.Close()
        Else
            Me.registrarFactura()
        End If


    End Sub


    Private Sub pbSR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbSR.Click, Label19.Click

        If Label19.BackColor = Color.White Then
            Me.Label19.BackColor = Color.LightSkyBlue
            Me.pbSR.BackgroundImage = My.Resources.cuadro2
            Me.BanderaServicioRestaurante = True
        Else
            Me.Label19.BackColor = Color.White
            Me.pbSR.BackgroundImage = My.Resources.cuadro1
            Me.BanderaServicioRestaurante = False
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class