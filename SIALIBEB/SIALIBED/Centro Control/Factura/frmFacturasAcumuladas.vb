

Public Class frmFacturasAcumuladas

    Dim fa As New cls_facturasAcumuladas()
    Dim fd As New cls_facturasAcumuladasDetalle

    'FUNCIONES 
    Public Sub cargarDatos()

        Me.DataGridView1.DataSource = Me.fa.obtenerTodasFacturasAcumuladas()

        Me.DataGridView1.Columns(0).Width = 150
        Me.DataGridView1.Columns(1).Width = 300
        Me.DataGridView1.Columns(2).DefaultCellStyle.Format = "c"
        Me.DataGridView1.Columns(2).Width = 150

        'Mostramos los totales.. en la pantalla
        Dim dt As New DataTable()
        dt = Me.fa.obtenerTotalesFacturasAcumuladas()
        Me.Label2.Text = FormatNumber(dt.Rows(0)("Total"), 2, , , TriState.True)
        Me.Label8.Text = FormatNumber(dt.Rows(0)("Descuento"), 2, , , TriState.True)
        Me.Label3.Text = FormatNumber(dt.Rows(0)("Imp. Venta"), 2, , , TriState.True)
        Me.Label6.Text = FormatNumber(dt.Rows(0)("Imp. Servicio"), 2, , , TriState.True)


    End Sub
    Function OpciondePago(ByVal Total As Double, ByVal _idfactura As Integer, ByVal _moneda As Integer, ByVal numero As Integer) As Boolean
        Dim Movimiento_Pago_Abonos As New frmMovimientoCajaPagoAbono
        Dim dtDatosFac As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT Cedula_Usuario, Total, Fecha, Num_Factura, Proveniencia_Venta, Num_Apertura FROM Ventas WHERE  (Id = " & _idfactura & ")", dtDatosFac)

        Movimiento_Pago_Abonos.Factura = dtDatosFac.Rows(0).Item("Num_Factura")
        Movimiento_Pago_Abonos.fecha = Now.Date
		Movimiento_Pago_Abonos.Total = Math.Round(Total, 2)
		Movimiento_Pago_Abonos.Tipo = "FVC" 'columna!tipo
        Movimiento_Pago_Abonos.codmod = _moneda

        Movimiento_Pago_Abonos.cedu = dtDatosFac.Rows(0).Item("Cedula_usuario")
        Movimiento_Pago_Abonos.cambioFP = dtDatosFac.Rows(0).Item("Num_Apertura")
        Dim dtusua As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select Nombre From Usuarios Where Cedula = '" & dtDatosFac.Rows(0).Item("Cedula_usuario") & "'", dtusua)
        Movimiento_Pago_Abonos.nombre = dtusua.Rows(0).Item(0)
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT IdPuntoVenta, Nombre, Tipo, BaseDatos FROM PuntoVenta WHERE     (IdPuntoVenta = " & dtDatosFac.Rows(0).Item("Proveniencia_Venta") & ")", dt, GetSetting("SeeSoft", "Hotel", "Conexion"))
        Dim cx As New Conexion
        cx.Conectar(dt.Rows(0).Item("BaseDatos"))
        cx.SlqExecute(cx.sQlconexion, "DELETE OpcionesDePago WHERE Documento = " & dtDatosFac.Rows(0).Item("Num_Factura") & " AND Numapertura = " & dtDatosFac.Rows(0).Item("Num_Apertura"))

        cx.DesConectar(cx.sQlconexion)
        Movimiento_Pago_Abonos.conexion1 = GetSetting("SeeSoft", dt.Rows(0).Item("BaseDatos"), "CONEXION")

        If Total <= 0 Then
            Return True
        Else
            Movimiento_Pago_Abonos.ShowDialog()
            If Movimiento_Pago_Abonos.Registra = True Then
                Me.fa.Imprimir()
                Return True
            Else
                Return False
            End If
        End If
    End Function

    '-------------------------------------------
    Public Sub facturar()
        Try
            Dim dt As DataTable
            Dim total As Double
            Dim idfactura As Integer
            Dim codmoneda As Integer
            Dim numero As Integer = Convert.ToInt32(DataGridView1.SelectedCells(0).Value.ToString())
            Me.fa.seleccionaFactura(numero)
            Me.fd.seleccionaDetalles(numero)

            Me.cargarDatos()

            dt = Me.fd.datos_factura()

            Dim d As DataRow
            For Each d In dt.Rows
                total = (Convert.ToDouble(d("total").ToString()))
                idfactura = (Convert.ToInt32(d("id").ToString()))
                codmoneda = (Convert.ToInt32(d("cod_moneda").ToString()))
            Next
            OpciondePago(total, idfactura, codmoneda, numero)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub borrarTodo()
        Dim frm As New frmQuestion()
        frm.LblMensaje.Text = "Esta Seguro de Borrar Todo ?"
        frm.ShowDialog()
        If frm.resultado = True Then
            Me.fa.borrarTodasLasFacturasAcumuladas()
        Else
            'nothing..
        End If
        Me.cargarDatos()
    End Sub
    Public Sub reporte()
        Me.fa.imprimirReporteFacturasAcumuladas()
    End Sub
    Public Sub efecto1()
        Me.lblimprimir.BackColor = Color.LightSkyBlue
        Me.lblfacturar.BackColor = Color.White
        Me.lblborrar.BackColor = Color.White
        Me.PictureBox11.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox22.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox33.BackgroundImage = My.Resources.cuadro1
    End Sub
    Public Sub efecto2()
        Me.lblfacturar.BackColor = Color.LightSkyBlue
        Me.lblimprimir.BackColor = Color.White
        Me.lblborrar.BackColor = Color.White
        Me.PictureBox22.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox11.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox33.BackgroundImage = My.Resources.cuadro1
    End Sub
    Public Sub efecto3()
        Me.lblborrar.BackColor = Color.LightSkyBlue
        Me.lblimprimir.BackColor = Color.White
        Me.lblfacturar.BackColor = Color.White
        Me.PictureBox33.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox22.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox11.BackgroundImage = My.Resources.cuadro1
    End Sub


    Private Sub frmFacturasAcumuladas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cargarDatos()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.facturar()
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Me.Close()
    End Sub
    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        Me.Close()
    End Sub
    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblfacturar.Click
        Me.efecto2()
        Me.facturar()
    End Sub
    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox22.Click
        Me.efecto2()
        Me.facturar()
    End Sub
    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblborrar.Click
        Me.efecto3()
        Me.borrarTodo()
    End Sub
    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox33.Click
        Me.efecto3()
        Me.borrarTodo()
    End Sub
    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblimprimir.Click
        Me.efecto1()
        Me.reporte()
    End Sub
    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox11.Click
        Me.efecto1()
        Me.reporte()
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If MsgBox("Desea Imprimir la Factura Acumulada", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmar Accion") = MsgBoxResult.Yes Then
            Dim dt As New DataTable
            Dim moneda As New DataTable
            cFunciones.Llenar_Tabla_Generico("select * from moneda where codmoneda = 2", dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            Dim cambiodolar As Decimal = dt.Rows(0).Item(2)
            Dim id As Long
            id = Me.DataGridView1.Item(0, e.RowIndex).Value
            cFunciones.Llenar_Tabla_Generico("select * from facturasAcumuladas where num_factura = " & id, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))

            Dim nc As Decimal
            nc = Math.Round(CDec(dt.Rows(0).Item("total")) / cambiodolar, 2)

            Dim mesa As String
            mesa = dt.Rows(0).Item("Mesa").ToString()

            Dim printer As String
            printer = GetSetting("SeeSoft", "Restaurante", "ImpresoraPrefactura")

            Dim factura As New FacturaAcomulada
            '123=numero, boleano, cadena
            factura.SetParameterValue(0, id) 'nfactura
            factura.SetParameterValue(1, 0) 'comanda
            factura.SetParameterValue(2, False) 'estado
            factura.SetParameterValue(3, "") 'salonero
            factura.SetParameterValue(4, nc) 'dolar 
            factura.SetParameterValue(5, mesa) 'dolar 

            factura.PrintOptions.PrinterName = printer
            CrystalReportsConexion.LoadReportViewer(Nothing, factura, True, GetSetting("SeeSoft", "Restaurante", "Conexion"))
            factura.PrintToPrinter(1, True, 0, 0)
        End If
    End Sub

    Private Sub PictureBox1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class