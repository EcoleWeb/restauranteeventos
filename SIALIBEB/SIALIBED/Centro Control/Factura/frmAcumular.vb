Public Class frmAcumular

#Region "variables"
    Dim iv As Integer = 0
    Dim iser As Integer = 0
    Dim des As Integer = 0
    Dim total As Double
    Dim impv As Double
    Dim impser As Double
    Dim descu As Double
    Dim descuent As Double
    Dim subtotal As Double
    Dim resultado As Boolean
    Dim PorcServicio As Double = 0
    Dim PorcVenta As Double = 0
    Dim mesa As String
    Dim cuenta As String
    Public SoyExpress As Integer = 0
    Public NumComanda As Integer = 0
    Public Transporte As Double = 0
    Public Telefono As String = ""
    Public PaLlevar As Integer = 0
    Dim _SoyAcumulada As Integer = 0
    Public MesaExpress As Integer = 0
#End Region

    Dim c As New ComandaMenu
    Private Sub frmAcumular_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Inabilitar()
    End Sub
    Public Sub Inabilitar()
        txtsubtotal.Enabled = False
        txtiv.Enabled = False
        txtis.Enabled = False
        txtdesc.Enabled = False
        txttotal.Enabled = False
    End Sub
    Public Sub ReciveDatos(ByRef subt As String, ByRef iv As String, ByRef ise As String, ByRef desc As String, ByRef total As String, ByRef mes As String, ByRef _cuenta As String, Optional IServicio As Double = 0, Optional Iventa As Double = 0, Optional SoyAcumulada As Integer = 0)
        txtsubtotal.Text = subt
        txtiv.Text = iv
        If MesaExpress <> 1 Then
            txtis.Text = ise
        Else
            txtis.Text = "0.00"
        End If

        txtdesc.Text = desc
        txttotal.Text = total
        mesa = mes
        impv = iv
        impser = ise
        PorcServicio = IServicio
        PorcVenta = Iventa
        subtotal = subt
        _SoyAcumulada = SoyAcumulada

        Me.cuenta = _cuenta
    End Sub
    Private Sub btncancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'resultado = False
        'Me.c.guardarComandaEnAcumulado_2(resultado, txtsubtotal.Text, txtiv.Text, txtis.Text, txtdesc.Text, txttotal.Text, mesa)
        'Me.Close()
    End Sub
    Private Sub btniv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        imp_ser()

    End Sub
    Private Sub btnis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        imp_ser()
    End Sub
    Private Sub btndesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        desc()
    End Sub
    Private Sub btnaceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'resultado = True
        'Me.c.guardarComandaEnAcumulado_2(resultado, txtsubtotal.Text, txtiv.Text, txtis.Text, txtdesc.Text, txttotal.Text, mesa)
        'Me.Close()
    End Sub
    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.si()
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Me.no()
    End Sub
    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.efecto1()
        imp_vent()
    End Sub
    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.Click
        Me.efecto3()
        Me.desc()
    End Sub
    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        Me.efecto2()
        Me.imp_ser()
    End Sub
    Private Sub lbliv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbliv.Click
        Me.efecto1()
        Me.imp_vent()
    End Sub
    Private Sub lblis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblis.Click
        Me.efecto2()
        Me.imp_ser()
    End Sub
    Private Sub lbldesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbldesc.Click
        Me.efecto3()
        Me.desc()
    End Sub
    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        Me.no()
    End Sub
    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click
        Me.si()
    End Sub

    'FUNCIONES ..
    Public Sub no()
        Me.Label4.BackColor = Color.White
        Me.Label5.BackColor = Color.LightSkyBlue
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro2
        resultado = False
        Me.c.guardarComandaEnAcumulado_2(resultado, txtsubtotal.Text, txtiv.Text, txtis.Text, txtdesc.Text, txttotal.Text, mesa, cuenta)
        Me.Close()
    End Sub
    Public Sub si()
        Me.Label4.BackColor = Color.LightSkyBlue
        Me.Label5.BackColor = Color.White
        Me.PictureBox1.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox2.BackgroundImage = My.Resources.cuadro1
        resultado = True
        Me.c._ParaLlevar = PaLlevar
        Me.c.SoyAcumulada = _SoyAcumulada
        Me.c.guardarComandaEnAcumulado_2(resultado, txtsubtotal.Text, txtiv.Text, txtis.Text, txtdesc.Text, txttotal.Text, mesa, cuenta, SoyExpress, NumComanda, Transporte, Telefono)
        Me.Close()
    End Sub
    Public Sub efecto1()
        Me.lbliv.BackColor = Color.LightSkyBlue
        Me.lblis.BackColor = Color.White
        Me.lbldesc.BackColor = Color.White
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox5.BackgroundImage = My.Resources.cuadro1
    End Sub
    Public Sub efecto2()
        Me.lbliv.BackColor = Color.White
        Me.lblis.BackColor = Color.LightSkyBlue
        Me.lbldesc.BackColor = Color.White
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro2
        Me.PictureBox5.BackgroundImage = My.Resources.cuadro1
    End Sub

    Public Sub efecto3()
        Me.lbliv.BackColor = Color.White
        Me.lblis.BackColor = Color.White
        Me.lbldesc.BackColor = Color.LightSkyBlue
        Me.PictureBox3.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox4.BackgroundImage = My.Resources.cuadro1
        Me.PictureBox5.BackgroundImage = My.Resources.cuadro2
    End Sub

    Public Sub desc()

        txttotal.Text = (Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(txtis.Text))
        Dim reg As New registro
        reg.txtCodigo.Text = Me.txttotal.Text
        reg.ShowDialog()
        If reg.iOpcion = 1 Then
            descuent = Convert.ToDouble(reg.txtCodigo.Text)
            If descuent = (Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtis.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(Transporte)) Then
                txtis.Text = "0.00"
                txtiv.Text = "0.00"
                txttotal.Text = "0.00"
            Else
                If descuent >= 100 Then
                    txtdesc.Text = Format(descuent, "#,#0.00")
                    txtsubtotal.Text = Format(Convert.ToDouble(subtotal) - Convert.ToDouble(descuent), "#,#0.00")
                    If MesaExpress <> 1 Then
                        If iser = 1 Then
                            txtis.Text = "0.00"
                        Else
                            txtis.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcServicio / 100))
                        End If
                    End If

                    If iv = 1 Then
                        txtiv.Text = "0.00"
                    Else
                        txtiv.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcVenta / 100))
                    End If
                    txttotal.Text = Format(Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtis.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(Transporte), "#,#0.00")

                Else
                    Me.c.PorcDescuentoAcumulada = Convert.ToDouble(descuent)
                    descuent = (descuent / 100)
                    descuent = Math.Round(descuent * Convert.ToDouble(subtotal))
                    txtdesc.Text = Format(descuent, "#,#0.00")
                    'descuent = (Convert.ToDouble(subtotal) - descuent)
                    txtsubtotal.Text = Format(Convert.ToDouble(subtotal) - Convert.ToDouble(descuent), "#,#0.00")
                    If MesaExpress <> 1 Then
                        If iser = 1 Then
                            txtis.Text = "0.00"
                        Else
                            txtis.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcServicio / 100))
                        End If
                    End If

                    If iv = 1 Then
                        txtiv.Text = "0.00"
                    Else
                        txtiv.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcVenta / 100))
                    End If
                    txttotal.Text = Format(Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtis.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(Transporte), "#,#0.00")

                End If
                End If
        End If
            reg.Dispose()
    End Sub

    Public Sub imp_ser()
        If MesaExpress <> 1 Then
            If iser = 0 Then
                txtis.Text = "0.00"
                txttotal.Text = Format((Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(txtis.Text) + Convert.ToDouble(Transporte)), "#,#0.00")
                iser = 1
            Else
                txtis.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcServicio / 100), "#,#0.00")
                total = (Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(txtis.Text) + Convert.ToDouble(Transporte))
                txttotal.Text = Format(total, "#,#0.00")
                iser = 0
            End If
        Else
            txtis.Text = "0.00"
        End If
    End Sub

    Public Sub imp_vent()
        If iv = 0 Then
            txtiv.Text = "0.00"
            total = (Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(txtis.Text))
            txttotal.Text = Format(total, "#,#0.00")
            iv = 1
        Else
            txtiv.Text = Format(Convert.ToDouble(txtsubtotal.Text) * (PorcVenta / 100), "#,#0.00")
            total = (Convert.ToDouble(txtsubtotal.Text) + Convert.ToDouble(txtiv.Text) + Convert.ToDouble(txtis.Text))
            txttotal.Text = Format(total, "#,#0.00")
            iv = 0
        End If
    End Sub

End Class