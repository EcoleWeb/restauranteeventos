﻿Public Class frmNombreCuenta

    Public NombreCuenta As String
    Public Aceptado As Boolean = False

    Private Sub btAceptar_Click(sender As Object, e As EventArgs) Handles btAceptar.Click
        spAceptar()
    End Sub
    Private Function Validacion() As Boolean
        If Me.txtNombreCuenta.Text = "" Then MsgBox("No se ha definido ningun nombre!!!", MsgBoxStyle.Information, "Atención...") : Return False
       Return True
    End Function

    Private Sub frmNombreCuenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.txtNombreCuenta.Text = NombreCuenta
        Me.txtNombreCuenta.SelectAll()
    End Sub

    Private Sub btCancelar_Click(sender As Object, e As EventArgs) Handles btCancelar.Click
        Aceptado = False
        Close()
    End Sub

    Private Sub txtNombreCuenta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtNombreCuenta.KeyDown
        If e.KeyCode = Keys.Enter Then
            spAceptar()
        End If
    End Sub
    Private Sub spAceptar()
        If Not Validacion() Then Exit Sub
        NombreCuenta = Me.txtNombreCuenta.Text
        Aceptado = True
        Close()
    End Sub
End Class