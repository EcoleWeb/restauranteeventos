Imports System.Drawing.Printing
Imports System.Windows
Imports System.Globalization


Public Class Factura

#Region "variables"

#Region "Globales"
    Public Id_Centro_Cortesia As Integer
    Public crear As Boolean = False
    Public hecho As Boolean = False
    Public separada As Boolean = False
    Public reimprimir As Boolean = False
    Public Anul, anular As Boolean
    Public numeroComanda, Comenzales, NFactura, idmesa, ID_MESA, int, numeroSeparado As Integer
    Public idfactura As Long
    Public CedulaU, CedulaS, nombremesa, NumeroFactura As String
    Public tipo_pago As String = "CON"
    Public Observacion As String = ""
    Public nombre As String = ""
    Public tipo_factura As Boolean = True
    Public rapido As Boolean = False
	Public vCortesia As Boolean = False
    Public PorcExonerado As Double = 13
#End Region

#Region "Locales"
    Dim PMU As New PerfilModulo_Class
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim DataA, DataM, DataS As New DataSet
    Dim DataV As DataView
    Dim CodMonedaC, IdReservacion, IdCliente, id As Integer
    Dim ACTIVA As Integer  'PARA VERIFICAR SI LA MESA ESTA ACTIVA
    Dim valor As Decimal = 1
    Dim valor1 As Decimal = 1
    Dim ValorMonedaC As Decimal = 1
    Dim TipoCambioDolar As Decimal = 0
    Dim codMoneda, ValorCompra As Decimal
    Dim Descuento, Global_subtotal, iss, iv, total, guardasubtotal, ExtraPropina As Double
    Dim nombremesa1, cedula, mmoneda, CuentaCortesia, Num_Apertura, NombreMoneda, autorizadox, Observacionesx As String
    Dim generarConsec As New GenerarConsecutivoHA
#End Region

#End Region

#Region "Close"
    Private Sub Factura_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If separada = True Then
                Dim dt As New DataSet
                Dim roww As DataRow
                conectadobd = cConexion.Conectar("Restaurante")
                If int <> 1 Then
                    cConexion.GetDataSet(conectadobd, "Select Cuentas.id FROM ComandasActivas INNER JOIN Cuentas ON ComandasActivas.id = Cuentas.idcTemporal where ComandasActivas.Estado='Activo'and Cuentas.IdMesa = ComandasActivas.cMesa and ComandasActivas.cMesa=" & idmesa & " and (ComandasActivas.ccantidad - ComandasActivas.Impreso) = 0 and ComandasActivas.separada=" & numeroSeparado, DataS, "Cuentas")
                End If 'psv

                For Each roww In DataS.Tables("Cuentas").Rows
                    cConexion.DeleteRecords(conectadobd, "Cuentas", "id=" & roww("id"))
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "Factura_FormClosing", ex.Message)
        Finally
            cConexion.DesConectar(conectadobd)
        End Try
    End Sub
#End Region

#Region "Load"
    Sub spForzarConfiguracionRegionl()
		Try
			If GetSetting("SeeSoft", "Restaurante", "Observacion").Equals("1") Then
				Me.txtObservacion1.Visible = False
				Me.lblObservacion.Visible = False
			Else SaveSetting("SeeSoft", "Restaurante", "Observacion", "")
			End If

            Dim oldDecimalSeparator As String =
           Application.CurrentCulture.NumberFormat.NumberDecimalSeparator

            Dim forceDotCulture As CultureInfo
			forceDotCulture = Application.CurrentCulture.Clone()
			forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
			forceDotCulture.NumberFormat.NumberGroupSeparator = ","

			forceDotCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
			forceDotCulture.DateTimeFormat.AMDesignator = "a.m."
			forceDotCulture.DateTimeFormat.PMDesignator = "p.m."
			forceDotCulture.DateTimeFormat.ShortTimePattern = "hh:mm tt"

            Application.CurrentCulture = forceDotCulture
		Catch ex As Exception
			cFunciones.spEnviar_Correo(Me.Name, "spForzarConfiguracionRegionl", ex.Message)
        End Try
    End Sub
    Private Sub Factura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim dtMoneda As New DataTable

            lbAnulada.Visible = Anul
            ToolBar2.Buttons(0).Enabled = False
            ToolBar2.Buttons(3).Enabled = reimprimir
            conectadobd = cConexion.Conectar("Restaurante")
            SqlConnection1.ConnectionString = conectadobd.ConnectionString
            SqlConnection2.ConnectionString = GetSetting("SeeSoft", "Contabilidad", "Conexion")
            SqlConnection3.ConnectionString = GetSetting("SeeSoft", "Salaberry", "Conexion")

            adClientes.Fill(Me.DatasetCliente1, "Cliente")
            cFunciones.Llenar_Tabla_Generico("SELECT m.monedaNombre,m.ValorCompra,m.CodMoneda FROM Moneda as m,dbo.configuraciones as r where codMoneda= r.monedaRestaurante", dtMoneda)
            If Not dtMoneda.Rows.Count > 0 Then
                Exit Sub
            End If
            ' mmoneda = cConexion.SlqExecuteScalar(conectadobd, "SELECT m.monedaNombre FROM Moneda as m,dbo.configuraciones as r where codMoneda= r.monedaRestaurante")
            mmoneda = dtMoneda.Rows(0).Item("monedaNombre")
            'ValorMonedaC = cConexion.SlqExecuteScalar(conectadobd, "SELECT m.ValorCompra FROM Moneda as m,dbo.configuraciones as r where codMoneda= r.monedaRestaurante")
            ValorMonedaC = dtMoneda.Rows(0).Item("ValorCompra")
            valor1 = cConexion.SlqExecuteScalar(conectadobd, "SELECT m.ValorVenta FROM Moneda as m,dbo.configuraciones as r where codMoneda= 2")
            ' CodMonedaC = cConexion.SlqExecuteScalar(conectadobd, "SELECT m.CodMoneda FROM Moneda as m,dbo.configuraciones as r where codMoneda= r.monedaRestaurante")
            CodMonedaC = dtMoneda.Rows(0).Item("CodMoneda")
            If separada Then
                Text = "Factura ( Cuenta: " & Me.numeroSeparado & " )"
            End If

            evalua_usuario()
            CargaMoneda()
            cboxMoneda.SelectedIndex = cboxMoneda.FindString(mmoneda)
            cargarDesc()
            CargarDatos_Comanda_Factura()
            'ValoresDefecto() Diego lo quito porque eran para asientos contables.

            '-----------------------------------------------------------------------------------
            'VALIDA SI PUEDE EXONERAR O NO
            If anular = False Then
                PMU = VSM(cedula, Name)
                If PMU.Others Then
					'  CheckExonerar.Enabled = True
					CheckBoxIS.Enabled = True
                Else
					'   CheckExonerar.Enabled = False
					CheckBoxIS.Enabled = False
                End If
            End If
            '-----------------------------------------------------------------------------------
            If Me.Num_Apertura Is Nothing Then
                Me.ToolBarRegistrar.Enabled = False
                Me.rbContado.Enabled = False

            End If
            If rapido Then

                CrearFactura()

            End If
            CambiaNombre()
            spForzarConfiguracionRegionl()
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "Factura_Load", ex.Message)
        End Try
    End Sub
    Sub cargarDesc()
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT     ISNULL(MAX(descuento),0) AS DES, Nombre AS NOM, cCodigo, separada " & _
                                                " FROM ComandasActivas " & _
                                                " GROUP BY Estado,cCodigo, separada, Nombre ,cMesa " & _
                                                " HAVING ( Estado= 'Activo' AND cMesa = " & idmesa & ") AND (separada = 0 OR separada = " & Me.numeroSeparado & ")", dt)


            If dt.Rows.Count > 0 Then

                If dt.Rows(0).Item("DES") > 0 Then
                    Me.ckbdescuento.Checked = True
                    Me.txtdescuento.Value = dt.Rows(0).Item("DES")
                End If

                If dt.Rows(0).Item("NOM").Equals("") Then
                    Me.txtcliente.Text = "CLIENTE CONTADO"
                Else
                    Me.txtcliente.Text = dt.Rows(0).Item("NOM")
                End If

            Else
                Me.txtdescuento.Value = 0
                Me.txtcliente.Text = "CLIENTE CONTADO"
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "cargarDesc", ex.Message)
        End Try
    End Sub
    Private Sub evalua_usuario()
        Try
            If clave = "" Then clave = User_Log.Clave_Interna
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & clave & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")
            If usuario = "" Then
                Enabled = True
                Exit Sub
            End If

            If anular = True Then ' si se va a anular la factura
                ToolBarEliminar.Enabled = True
                ToolBarRegistrar.Enabled = False
                Exit Sub
            End If
            lbUsuario.Text = ""
            cConexion.GetRecorset(conectadobd, "Select aperturacaja.NApertura, aperturacaja.Nombre, aperturacaja.Cedula from aperturacaja inner join Usuarios on aperturacaja.cedula = Usuarios.Cedula where aperturacaja.Anulado = 0 AND Usuarios.Clave_Interna = '" & clave & "' and aperturacaja.Estado = 'A'", rs)
            While rs.Read
                Num_Apertura = rs("NApertura")
                lbUsuario.Text = rs("Nombre")
                CedulaU = rs("Cedula")
            End While
            rs.Close()

            conectadobd = cConexion.Conectar("Restaurante")
            cConexion.GetRecorset(conectadobd, "Select Cedula from Saloneros Where Nombre = '" & lbUsuario.Text & "'", rs)
            While rs.Read
                CedulaS = rs("Cedula")
            End While
            rs.Close()

            If lbUsuario.Text = "" Then
                If MessageBox.Show("Clave incorrecta ó el usuario no tiene ninguna apertura de caja asociada, ¿Desea realizar una apertura de caja?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    Dim form As New AperturaCaja
                    Hide()
                    form.ShowDialog()

                    cConexion.GetRecorset(conectadobd, "Select aperturacaja.NApertura, aperturacaja.Nombre, aperturacaja.Cedula from aperturacaja inner join Usuarios on aperturacaja.cedula = Usuarios.Cedula where Usuarios.Clave_Interna = '" & clave & "' and aperturacaja.Estado = 'A'", rs)
                    While rs.Read
                        Num_Apertura = rs("NApertura")
                        lbUsuario.Text = rs("Nombre")
                        CedulaU = rs("Cedula")
                    End While
                    rs.Close()

                Else
                    hecho = False
                    Exit Sub
                End If
            End If
            ToolBar2.Buttons(0).Enabled = True

            Me.CambiaNombre()

        Catch ex As Exception
            MessageBox.Show("Error al realizar la factura: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "evalua_usuario", ex.Message)
        End Try
    End Sub

    Private Sub CargaMoneda()
        Try
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Seguridad")
            cConexion.GetDataSet(conectadobd, "Select * from Moneda", DataS, "Moneda")
            DataV = New DataView(DataS.Tables("Moneda"))
            Dim fila As DataRow

            For Each fila In DataS.Tables("Moneda").Rows
                If fila("CodMoneda") = CodMonedaC Then
                    codMoneda = fila("CodMoneda")
                    NombreMoneda = fila("MonedaNombre")
                    cboxMoneda.SelectedItem = fila("MonedaNombre")
                    valor = fila("ValorCompra")
                End If
                If fila("CodMoneda") = 2 Then
                    TipoCambioDolar = fila("ValorCompra")
                End If

                cboxMoneda.Items.Add(fila("MonedaNombre"))
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "CargaMoneda", ex.Message)
        Finally
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Restaurante")
        End Try
    End Sub

    Private Sub ValoresDefecto()
        Try
            'VALORES POR DEFECTO PARA LA TABLA ASIENTOS
            DatasetCliente1.AsientosContables.FechaColumn.DefaultValue = Now.Date
            DatasetCliente1.AsientosContables.NumDocColumn.DefaultValue = "0"
            DatasetCliente1.AsientosContables.IdNumDocColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.BeneficiarioColumn.DefaultValue = ""
            DatasetCliente1.AsientosContables.TipoDocColumn.DefaultValue = 15
            DatasetCliente1.AsientosContables.AccionColumn.DefaultValue = "AUT"
            DatasetCliente1.AsientosContables.AnuladoColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.FechaEntradaColumn.DefaultValue = Now.Date
            DatasetCliente1.AsientosContables.MayorizadoColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.PeriodoColumn.DefaultValue = Now.Month & "/" & Now.Year
            DatasetCliente1.AsientosContables.NumMayorizadoColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.ModuloColumn.DefaultValue = "Facturacion Restaurante"
            DatasetCliente1.AsientosContables.ObservacionesColumn.DefaultValue = ""
            DatasetCliente1.AsientosContables.NombreUsuarioColumn.DefaultValue = ""
            DatasetCliente1.AsientosContables.TotalDebeColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.TotalHaberColumn.DefaultValue = 0
            DatasetCliente1.AsientosContables.CodMonedaColumn.DefaultValue = 1
            DatasetCliente1.AsientosContables.TipoCambioColumn.DefaultValue = 1

            'VALORES POR DEFECTO PARA LA TABLA DETALLES ASIENTOS
            DatasetCliente1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrement = True
            DatasetCliente1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementSeed = -1
            DatasetCliente1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementStep = -1
            DatasetCliente1.DetallesAsientosContable.NumAsientoColumn.DefaultValue = ""
            DatasetCliente1.DetallesAsientosContable.DescripcionAsientoColumn.DefaultValue = ""
            DatasetCliente1.DetallesAsientosContable.CuentaColumn.DefaultValue = ""
            DatasetCliente1.DetallesAsientosContable.NombreCuentaColumn.DefaultValue = ""
            DatasetCliente1.DetallesAsientosContable.MontoColumn.DefaultValue = 0
            DatasetCliente1.DetallesAsientosContable.DebeColumn.DefaultValue = 0
            DatasetCliente1.DetallesAsientosContable.HaberColumn.DefaultValue = 0
            DatasetCliente1.DetallesAsientosContable.TipocambioColumn.DefaultValue = 1
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "ValoresDefecto", ex.Message)
        End Try
    End Sub
#End Region

#Region "ToolBar"

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        If cedula = "" Then cedula = User_Log.Cedula
        PMU = VSM(cedula, Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : If PMU.Update Then CrearFactura() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 2 : If PMU.Delete Then anulando() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 3 : If cbkCortesia.Checked = True Then imprimir1(NFactura, 0) Else Imprimir(NFactura)
            Case 4 : teclado()
            Case 5 : Me.Close()
        End Select
    End Sub
#End Region

#Region "Guardar"

    Dim IdCortesia As Integer
    Private Sub CrearFactura()
        Try
            If MsgBox("Desea proceder a generar la factura....?", MsgBoxStyle.YesNo, "Atención..") = MsgBoxResult.Yes Then

               If cbkCortesia.Checked = True Then
                    If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                    Dim numero_cortesia As Int64
                    numero_cortesia = RealizaCortesia()
                    IdCortesia = numero_cortesia
                    If numero_cortesia = 0 Then Exit Sub
                    If MessageBox.Show("¿Desea imprimir el comprobante de cortesía?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        If int = 1 Then 'psv

                            If numeroSeparado = 0 Or numeroSeparado = 1 Then
                                PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' AND cMesa= " & idmesa & " and (separada=0 or separada = 1) ", "ComandaTemporal")
                            Else
                                PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' AND cMesa= " & idmesa & " and separada=" & numeroSeparado, "ComandaTemporal")
                            End If
                            imprimir2(numeroComanda, numero_cortesia)
                        Else
                            If numeroSeparado = 0 Or numeroSeparado = 1 Then
                                PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' AND cMesa= " & idmesa & " and (separada=0 or separada =1)", "ComandaTemporal")
                            Else
                                PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' and cMesa= " & idmesa & " and separada=" & numeroSeparado, "ComandaTemporal")
                            End If
                            imprimir1(numeroComanda, numero_cortesia)
                        End If 'psv
                    Else
                        If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                        If numeroSeparado = 0 Or numeroSeparado = 1 Then
                            PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' AND cMesa= " & idmesa & " and (separada=0 or separada =1)", "ComandaTemporal")
                        Else
                            PasarComanda_Quitar_Temporal("Select * from ComandasActivas where Estado='Activo' AND cMesa= " & idmesa & " and separada=" & numeroSeparado, "ComandaTemporal")
                        End If
                    End If
                    Close()
                    Exit Sub
                End If

                If cbkCortesia.Checked = True Then Close()

                If cboxMoneda.SelectedItem = "" Then
                    MessageBox.Show("Debe seleccionar un tipo de moneda", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                If rbCredito.Checked = True And txtcliente.Text = "CLIENTE CONTADO" Or txtcliente.Text = "" Then
                    MessageBox.Show("Debe seleccionar el cliente de crédito", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                ElseIf rbcarga.Checked = True And txtcliente.Text = "CLIENTE CONTADO" Or txtcliente.Text = "" Then
                    MessageBox.Show("Debe seleccionar el cliente y la habitación a cargar", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                ElseIf rbContado.Checked = True And txtcliente.Text = "" Then
                    txtcliente.Text = "CLIENTE CONTADO"
                    Exit Sub
                End If

                If separada = False Then
                    If int = 1 Then
                        Factura("SELECT ComandaTemporal1.*, (CASE WHEN ComandaTemporal1.cProducto = 0 or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandaTemporal1.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandaTemporal1.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandaTemporal1.cProducto = 0 or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandaTemporal1.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandaTemporal1.Express + 1,  2, 0) ELSE ComandaTemporal1.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandaTemporal1.Express + 1,  2, 0) END) AS IServicios, Mesas.Nombre_Mesa FROM Configuraciones CROSS JOIN  ComandaTemporal1 WITH (NOLOCK) INNER JOIN  Mesas ON ComandaTemporal1.cMesa = Mesas.Id LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal1.cProducto = Menu_Restaurante.Id_Menu left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE  ( ComandaTemporal1.Estado = 'Activo' and ComandaTemporal1.cMesa = " & idmesa & ") AND (ComandaTemporal1.separada = 0 OR ComandaTemporal1.separada = 1) ", "ComandaTemporal1")
                    Else
                        Factura("SELECT ComandasActivas.*, (CASE WHEN ComandasActivas.cProducto = 0  or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandasActivas.cProducto = 0 or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0) ELSE ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0) END) AS IServicios, Mesas.Nombre_Mesa  FROM Configuraciones CROSS JOIN  ComandasActivas WITH (NOLOCK) INNER JOIN  Mesas ON ComandasActivas.cMesa = Mesas.Id LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE  (ComandasActivas.Estado = 'Activo') AND (ComandasActivas.cMesa = " & idmesa & ") AND (ComandasActivas.separada = 0 OR ComandasActivas.separada = 1)", "ComandasActivas")
                    End If
                Else
                    Factura("SELECT ComandasActivas.*,Cuentas.id, Cuentas.idMesa, Cuentas.idcTemporal, Cuentas.Cantidad as CantSep, (CASE WHEN ComandasActivas.cProducto = 0 or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * Configuraciones.Imp_Venta / 100) END) AS IVentas, (CASE WHEN ComandasActivas.cProducto = 0 or ComandasActivas.Tipo_Plato = 'Acompañamiento' or ComandasActivas.Tipo_Plato = 'Modificador' THEN ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0) ELSE ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0) END) AS IServicios, Mesas.Nombre_Mesa  FROM cuentas, Configuraciones CROSS JOIN  ComandasActivas WITH (NOLOCK) INNER JOIN  Mesas ON ComandasActivas.cMesa = Mesas.Id LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE  (ComandasActivas.Estado='Activo') AND (ComandasActivas.cMesa = " & idmesa & ") AND ComandasActivas.separada =" & numeroSeparado & " and ComandasActivas.id = cuentas.idcTemporal and ComandasActivas.cMesa = cuentas.idMesa", "ComandaTemporal")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "CrearFactura", ex.Message)
        End Try
    End Sub
    Dim totalComision As Decimal = 0
    Private Sub spCargarComisionPuntoVenta(ByVal cProducto As String, ByVal SubTotal As Decimal)
        Try

            If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then
                Dim cx As New Conexion
                Dim Porcentaje As Decimal
                Porcentaje = cx.SlqExecuteScalar(cx.Conectar, "Select PorcentajeComision from Menu_Restaurante where Id_Menu = '" & cProducto & "'")
                If Porcentaje > 0 Then
                    totalComision += SubTotal * (Porcentaje / 100)
                End If

                cx.DesConectar(cx.sQlconexion)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "spCargarComisionPuntoVenta", ex.Message)
        End Try
   
    End Sub
    Private Sub spGuardarComisionPuntoVenta(ByVal _Num_Factura As String)
        Try
            If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then
                Dim cx As New ConexionR
                cx.AddNewRecord(cx.Conectar("Hotel"), "tb_FD_Comision", "IdCliente,NombreCliente,Num_Factura,NombreGuia,Monto,CodMoneda,CedUsuario,NombreUsuario,TotalFactura,Fecha,Proveniencia_Venta,SubTotalFactura,ImpuestoFactura,DescuentoFactura", "" & _
                                "'" & ComisionCedula & "','" & ComisionNombre & "','" & _Num_Factura & "','" & ComisionNombreGuia & "','" & totalComision & "','" & CodMonedaC & "','" & User_Log.Cedula & "','" & User_Log.Nombre & "','" & CDbl(txtTotal.Text) & "','" & Now.Date & "','" & User_Log.PuntoVenta & "','" & txtSubtotal.Text & "','" & txtIV.Text & "','" & TextBoxDescuento.Text & "'")
                cx.DesConectar(cx.SqlConexion)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "spGuardarComisionPuntoVenta", ex.Message)
        End Try
    End Sub
    Public id_ventas As Long
    Private Sub Factura(ByVal str As String, ByVal tabla As String)
        Try
            Dim Mesa, Dato As String
            Dim cero As Integer = 0
            'SE PASA LA COMANDA TEMPORAL A LA TABLA COMANDA Y SUS DETALLES
            If conectadobd.State <> ConnectionState.Closed Then conectadobd.Close()

            conectadobd.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()

            'If int = 1 Then
            cConexion.AddNewRecord(conectadobd, "Comanda", "NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura, Cod_Moneda", numeroComanda & ", GetDate()," & CDbl(txtSubtotal.Text) & "," & CDbl(txtIV.Text) & "," & CDbl(txtTotal.Text) & ",'" & CedulaU & "','" & CedulaS & "'," & idmesa & "," & Comenzales & ",0,0," & codMoneda)
            'Else
            'cConexion.AddNewRecord(conectadobd, "Comanda", "NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura, Cod_Moneda", numeroComanda & ", GetDate()," & CDbl(txtSubtotal.Text) & "," & CDbl(txtIV.Text) & "," & CDbl(txtTotal.Text) & ",'" & CedulaU & "','" & CedulaS & "'," & idmesa & "," & Comenzales & ",0,0," & codMoneda)
            'End If

            Mesa = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Mesa From Mesas Where Id=" & idmesa & "")
            Dim idc As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Idcomanda) from Comanda")
            Dim subt, iv, iss As Double
            Dim IdDetalle As Integer = 0
            Dim Dt As New DataSet
            Dim row As DataRow
            Dim Hora As Date
            Dim preciou As Decimal
            conectadobd = cConexion.Conectar()
            cConexion.GetDataSet(conectadobd, str, Dt, tabla)
            Dim ISH_IS As Double = IIf(CheckBoxISH.Checked, User_Log.ISH, 0) + IIf(CheckBoxIS.Checked, User_Log.ISS, 0)

            For Each row In Dt.Tables(tabla).Rows
                If Trim(row("Tipo_Plato")) = "Principal" Then
                    preciou = ((row("cPrecio") * ValorMonedaC) / valor)
                    'If separada = False Then
                    subt = row("cCantidad") * preciou
                    'Else
                    '    subt = row("CantSep") * preciou
                    'End If
                    iv = 0
                    iss = 0
                    Hora = row("Hora")
                    iv = ((row("IVentas") * ValorMonedaC) / valor)
                    iss = ((row("IServicios") * ValorMonedaC) / valor)
                    CedulaS = row("Cedula")
                    If tipo_pago = "CON" And chkComisionista.Checked = True Then
                        spCargarComisionPuntoVenta(row("cProducto"), subt)
                    End If
                  If (CedulaS = Nothing) Then CedulaS = 1 'Para las comandas que fueron creadas con la version anterior a esta.

                    cConexion.SlqExecute(conectadobd, "Insert Into DetalleMenuComanda (IdComanda, Idmenu,Ced_Salonero,Cantidad, PrecioUnitario, Subtotal, Impventa, Impservicio, costo_real,Hora) values(" & idc & "," & row("cProducto") & ",'" & CedulaS & "'," & row("cCantidad") & "," & preciou & "," & subt & "," & iv & "," & iss & "," & row("costo_real") & ",'" & row("hora") & "')")

                    IdDetalle = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(IdDetalle) AS IdDetalle from DetalleMenuComanda where IdComanda =" & idc)

                ElseIf Trim(row("Tipo_Plato")) = "Modificador" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into Detalle_ModificadoresComanda (IdDetalle, IdModificador) values (" & IdDetalle & "," & row("cProducto") & ")")
                ElseIf Trim(row("Tipo_Plato")) = "Acompañamiento" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into DetalleAcompañamiento_Comanda (IdDetalle, IdAcompañamiento) values (" & IdDetalle & "," & row("cProducto") & ")")
                End If

            Next

            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")

            'INGRESO LOS DATOS DE LA FACTURA A LA TABLA VENTAS
            'HACK VENTA_DETALLE AGREGAR PRECIO DE COSTO
            'EN VENTAS AGREGAR SUBTOTALGRAVADO, SUBTOTALEXCENTO

            conectadobd = cConexion.Conectar()
            NumeroFactura = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(MAX(Num_Factura),0) as NumeroFactura from Ventas where Num_Factura < 1000000") 'And Proveniencia_Venta= " & User_Log.PuntoVenta)
            'numerofactura es el max numero de factura correspondiente al punto de venta seleccionado
            'campos es el string que contiene los campos de la tabla ventas de hotel
            Dim campos As String = "Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento, Transporte, Tipo_Cambio, Monto_ICT, Id_Reservacion, Monto_Saloero, Proveniencia_Venta, Huesped, Descripcion, TipoCambioDolar,IdCliente,ExtraPropina,Mesa,Observacion"
			'datos es el string que contiene los datos de la tabla ventas de hotel
			NumeroFactura += 1
			'psv
			Dim datos As String
			Dim Gravado As Double
			Dim Excento As Double

			Gravado = Math.Round(IIf(txtIV.Text <> 0, txtSubtotal.Text - TextBoxDescuento.Text, 0), 2)
			Excento = Math.Round(IIf(txtIV.Text = 0, txtSubtotal.Text - TextBoxDescuento.Text, 0), 2)
			'En caso de que alguno de los 2 montos sea negativo lo cual nunca deberia pasar, pero pasa cuando el desc es 100%
			If Excento < 0 Then
				Excento = 0
			End If
			If Gravado < 0 Then
				Gravado = 0
			End If
			If Me.Num_Apertura Is Nothing Then
				Me.Num_Apertura = "0"
			End If
			If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            'valor = 0
            datos = NumeroFactura & ",'" & tipo_pago & "'," & txtcodcliente.Text & ",'" & txtcliente.Text & "',0,'" & CedulaU & "',0," & CDbl(txtSubtotal.Text) & "," & CDbl(TextBoxDescuento.Text) & "," & CDbl(txtIV.Text) & "," & CDbl(txtTotal.Text) & ", getdate(), getdate(),'NINGUNO',0,0,0,0,0,0,0," & Num_Apertura & ",0," & codMoneda & ",'" & NombreMoneda & "','',''," & Gravado & "," & Excento & ",0," & valor & ",0," & id & "," & CDbl(txtIS.Text) & "," & User_Log.PuntoVenta & ",'','FACTURA RESTAURANTE'," & valor1 & "," & IdCliente & "," & NumericUpDownExtraPropina.Value & ",'" & Mesa & "','" & txtObservacion1.Text & "'"   'psv"   
            cConexion.AddNewRecord(conectadobd, "Ventas", campos, datos)
            id_ventas = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Id) as Id from Ventas where Num_Factura=" & NumeroFactura & " and Proveniencia_Venta =" & User_Log.PuntoVenta)

            '------------------------------------------------------------------
            generarConsec.crearNumeroConsecutivo(txtcodcliente.Text, id_ventas, NumeroFactura, Now)
            '------------------------------------------------------------------

            If tipo_pago = "CON" And chkComisionista.Checked = True Then
                spGuardarComisionPuntoVenta(NumeroFactura)
            End If
          
            'HACER QUE INGRESE EL DETALLE DE LAS VENTAS
            Dim row2 As DataRow
            Dim precio As Double
            campos = "Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo, Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado, SubToTalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta, Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina"

            'bandera
            Dim guardamodificador As Boolean = Me.GuardaModificadores
            If int = 1 Then
                For Each row2 In Dt.Tables("ComandaTemporal1").Rows
                    precio = (row2("cPrecio") * ValorMonedaC) / valor
                    Dim can As Decimal
                    If can <> -1 Or guardamodificador = True Then
                        If separada = False Then can = Math.Abs(row2("cCantidad")) Else can = Math.Abs(row2("Cantidad"))
                        If row2("Tipo_Plato").ToString().Contains("Acompañamiento") Then
                            can = 1
                        End If
                        Dim impuesto = cargarImpuestoIVA(IdCliente, row2("cProducto"))
                        If PorcExonerado < 13 Then

                            datos = id_ventas & "," & row2("cProducto") & ",'" & row2("cDescripcion") & "'," & can & "," & row2("costo_real") & ",0,0,0," & precio & "," & txtdescuento.Value & "," & ((txtdescuento.Value / 100) * row2("cPrecio")) & "," & impuesto & "," & (can * (impuesto / 100) * precio) & "," & (precio * can) - ((txtdescuento.Value / 100) * row2("cPrecio")) & ",0," & (precio * can) & ",0,0,0,1,1,0,0," & ISH_IS & "," & (can * (ISH_IS / 100) * precio) ' SAJ 140907 
                        Else
                            datos = id_ventas & "," & row2("cProducto") & ",'" & row2("cDescripcion") & "'," & can & "," & row2("costo_real") & ",0,0,0," & precio & "," & txtdescuento.Value & "," & ((txtdescuento.Value / 100) * row2("cPrecio")) & "," & IIf(impuesto <= 0, "0", impuesto) & "," & IIf(impuesto <= 0, "0", (can * (impuesto / 100) * precio)) & "," & (precio * can) - ((txtdescuento.Value / 100) * row2("cPrecio")) & ",0," & (precio * can) & ",0,0,0,1,1,0,0," & ISH_IS & "," & (can * (ISH_IS / 100) * precio) ' SAJ 140907 
                        End If

						cConexion.AddNewRecord(conectadobd, "Ventas_Detalle", campos, datos)
                    End If
                Next
            Else
                For Each row2 In Dt.Tables(tabla).Rows
                    precio = (row2("cPrecio") * ValorMonedaC) / valor

                    Dim can As Decimal


                    If separada = False Then
                        can = Math.Abs(row2("cCantidad"))
                    Else
                        can = Math.Abs(row2("CantSep"))
                    End If
                    If can <> -1 Or guardamodificador = True Then
                        If row2("Tipo_Plato").ToString().Contains("Acompañamiento") Then
                            can = 1
                        End If
                        Dim impuesto = cargarImpuestoIVA(IdCliente, row2("cProducto"))
                        If PorcExonerado < 13 Then

                            datos = id_ventas & "," & row2("cProducto") & ",'" & row2("cDescripcion") & "'," & can & "," & row2("costo_real") & ",0,0,0," & precio & "," & txtdescuento.Value & "," & ((txtdescuento.Value / 100) * row2("cPrecio")) & "," & impuesto & "," & (can * (impuesto / 100) * precio) & "," & (precio * can) - ((txtdescuento.Value / 100) * row2("cPrecio")) & ",0," & (precio * can) & ",0,0,0,1,1,0,0," & ISH_IS & "," & (can * (ISH_IS / 100) * precio)
                        Else
                            datos = id_ventas & "," & row2("cProducto") & ",'" & row2("cDescripcion") & "'," & can & "," & row2("costo_real") & ",0,0,0," & precio & "," & txtdescuento.Value & "," & ((txtdescuento.Value / 100) * row2("cPrecio")) & "," & IIf(impuesto <= 0, "0", impuesto) & "," & IIf(impuesto <= 0, "0", (can * (impuesto / 100) * precio)) & "," & (precio * can) - ((txtdescuento.Value / 100) * row2("cPrecio")) & ",0," & (precio * can) & ",0,0,0,1,1,0,0," & ISH_IS & "," & (can * (ISH_IS / 100) * precio)
                        End If

                        cConexion.AddNewRecord(conectadobd, "Ventas_Detalle", campos, datos)
                    End If
                Next
            End If

            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar()

            If tipo_pago = "CON" Then
                'INDICA QUE NO SE HA COBRADO
                cConexion.UpdateRecords(conectadobd, "Comanda", "Facturado=0, Numerofactura=" & NumeroFactura, "Idcomanda=" & idc)
            Else
                cConexion.UpdateRecords(conectadobd, "Comanda", "Facturado=1, Numerofactura=" & NumeroFactura, "Idcomanda=" & idc)
            End If

            If separada = False Then
                If numeroSeparado = 0 Or numeroSeparado = 1 Then
                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Estado='Facturada'", " Estado= 'Activo' and cMesa=" & idmesa & " and (separada=0 or separada =1)")
                Else
                    cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Estado='Facturada'", " Estado= 'Activo' and cMesa=" & idmesa & " and separada=" & numeroSeparado)
                End If
            Else
                Dim Dat As New DataSet
                Dim fil As DataRow
                cConexion.GetDataSet(conectadobd, "Select C.idcTemporal, C.Cantidad, CT.cCantidad, CT.Id from Cuentas as C inner join ComandasActivas as CT on C.IdcTemporal = CT.Id where CT.Estado='Activo' and CT.cMesa=" & idmesa & "and ct.separada=" & numeroSeparado, Dat, "ComandaTemporal")
                For Each fil In Dat.Tables("ComandaTemporal").Rows
                    If fil("cCantidad") = fil("Cantidad") Then
                        cConexion.SlqExecute(conectadobd, "UPDATE ComandaTemporal Set Estado='Facturada' where Estado= 'Activo' and Id=" & fil("Id") & " and separada=" & numeroSeparado)
                    Else
                        cConexion.SlqExecute(conectadobd, "Update ComandaTemporal set cCantidad=" & (fil("cCantidad") - fil("Cantidad")) & " where Estado= 'Activo' and Id=" & fil("Id") & " and separada=" & numeroSeparado)
                        cConexion.SlqExecute(conectadobd, "Delete From Cuentas where idcTemporal=" & fil("Id"))
                    End If
                Next
            End If
            '
            Dim FacturasPendientes As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select COUNT(*) from Comanda where  idMesa=" & idmesa & " AND (Facturado = 0)")
            Dim ComandasPendientes As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select COUNT(*) from ComandasActivas where Estado='Activo' and cMesa=" & idmesa & " and separada=" & numeroSeparado)
            Dato = cConexion.SlqExecuteScalar(conectadobd, "Select separada From ComandasActivas Where Estado='Activo'and cMesa=" & idmesa & "")
            If (Dato = Nothing) Then
                Dim dem As New Comanda
                dem.cuentas = 0
                cConexion.UpdateRecords(conectadobd, "Mesas", "separada=" & cero, "Id=" & idmesa)
            Else
                ComandasPendientes = 1
            End If

            Select Case tipo_pago
                Case "CON"
                    If FacturasPendientes = 1 Then
                        If separada = False Then
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=2", "Id=" & idmesa) 'cuando las pone en cobro!
                        Else
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=3", "Id=" & idmesa) 'las pone pendientes de facturar las separadas, 
                        End If                                                                        'el error q pasa es xq estan quedando facturadas en 0
                    Else
                        If FacturasPendientes > 1 Then
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=3", "Id=" & idmesa)
                        Else
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=2", "Id=" & idmesa)
                        End If
                    End If

                Case "CRE"
                    If FacturasPendientes = 0 Then
                        If ComandasPendientes = 0 Then
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=0", "Id=" & idmesa)
                        Else
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "Id=" & idmesa)
                        End If
                    Else
                        cConexion.UpdateRecords(conectadobd, "Mesas", "activa=3", "Id=" & idmesa)
                    End If

                Case "CAR"
                    If FacturasPendientes = 0 Then
                        If ComandasPendientes = 0 Then
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=0", "Id=" & idmesa)
                        Else
                            cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1", "Id=" & idmesa)
                        End If
                    Else
                        cConexion.UpdateRecords(conectadobd, "Mesas", "activa=3", "Id=" & idmesa)
                    End If
            End Select

            hecho = True

            cConexion.SlqExecute(conectadobd, "Insert Into BitacoraComandaEliminada (Razon,IdReferencia,TipoReferencia,Usuario) Values ('Facturacion','" & Me.NumeroFactura & "','Factura','" & lbUsuario.Text & "')")




            '----------------------------------------------------------------------------
            'ASIENTO CONTABLE
            'Dim Configuracion As Boolean
            'cConexion.DesConectar(conectadobd)
            'conectadobd = cConexion.Conectar("Hotel")
            'If Configuracion Then
            '    If rbContado.Checked = False Then
            '        GuardaAsiento(id_ventas)
            '        If TransAsiento() = False Then
            '            MsgBox("Error Guardando el Asiento Contable", MsgBoxStyle.Exclamation, "Asientos Contables")
            '        End If
            '    End If
            'End If
            'cConexion.DesConectar(conectadobd)
            'conectadobd = cConexion.Conectar("Restaurante")
            'cConexion.DesConectar(conectadobd)
            '----------------------------------------------------------------------------
            Close()

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "Factura", ex.Message)
        End Try
    End Sub

    Private Function GuardaModificadores() As Boolean
        Try
            Return General.Ejecuta("select GuardaModificadores from conf").Rows(0).Item(0)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "GuardaModificadores", ex.Message)
        End Try

    End Function

    Private Sub PasarComanda_Quitar_Temporal(ByVal str As String, ByVal tabla As String)
        Try
            'SE PASA LA COMANDA TEMPORAL A LA TABLA COMANDA Y SUS DETALLES
            Dim idc As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Idcomanda) from Comanda")
            Dim subt, iv, iss As Double
            Dim IdDetalle As Integer = 0
            Dim Dt As New DataSet
            Dim row As DataRow
            Dim preciou As Decimal
            cConexion.GetDataSet(conectadobd, str, Dt, tabla)
            For Each row In Dt.Tables(tabla).Rows
                If Trim(row("Tipo_Plato")) = "Principal" Then
                    preciou = ((row("cPrecio") * ValorMonedaC) / valor)
                    If separada = False Then
                        subt = row("cCantidad") * preciou
                    Else
                        subt = row("Cantidad") * preciou
                    End If
                    iv = (subt * (PorcExonerado / 100))
                    iss = (subt * (User_Log.ISS / 100))
                    If separada = False Then
                        cConexion.SlqExecute(conectadobd, "Insert Into DetalleMenuComanda (IdComanda, Idmenu, Cantidad, PrecioUnitario, Subtotal, Impventa, Impservicio,Hora, costo_real) values(" & idc & "," & row("cProducto") & "," & row("cCantidad") & "," & preciou & "," & subt & "," & iv & "," & iss & ",'" & row("hora") & "', " & row("costo_real") & ")")
                    Else
                        cConexion.SlqExecute(conectadobd, "Insert Into DetalleMenuComanda (IdComanda, Idmenu, Cantidad, PrecioUnitario, Subtotal, Impventa, Impservicio,Hora, costo_real) values(" & idc & "," & row("cProducto") & "," & row("Cantidad") & "," & preciou & "," & subt & "," & iv & "," & iss & ",'" & row("hora") & "', " & row("costo_real") & ")")
                    End If

                    IdDetalle = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(IdDetalle) AS IdDetalle from DetalleMenuComanda where IdComanda =" & idc)
                ElseIf Trim(row("Tipo_Plato")) = "Modificador" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into Detalle_ModificadoresComanda (IdDetalle, IdModificador) values (" & IdDetalle & "," & row("cProducto") & ")")
                ElseIf Trim(row("Tipo_Plato")) = "Acompañamiento" Then
                    cConexion.SlqExecute(conectadobd, "Insert Into DetalleAcompañamiento_Comanda (IdDetalle, IdAcompañamiento) values (" & IdDetalle & "," & row("cProducto") & ")")
                End If
            Next
            conectadobd = cConexion.Conectar("Restaurante")
            cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Estado='Facturada'", "Estado= 'Activo'  and cMesa=" & idmesa & " and separada=" & numeroSeparado)
            cConexion.SlqExecute(conectadobd, "Insert Into BitacoraComandaEliminada (Razon,IdReferencia,TipoReferencia,Usuario) Values ('Cortesia','" & IdCortesia & "','Cortesia','" & lbUsuario.Text & "')")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            cFunciones.spEnviar_Correo(Me.Name, "PasarComanda_Quitar_Temporal", ex.Message)
        End Try
    End Sub
#End Region

#Region "Imprimir"
    Public Sub imprimir_SERVICIO_RESTAURANTE(ByVal NFactura As Integer, ByVal Idioma As String, ByVal conce As String)
        Try
            Dim Impresora As String
            Impresora = busca_impresora()
            If Impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Impresora = busca_impresora()
                If Impresora = "" Then
                    Exit Sub
                End If
            End If

            If GetSetting("SeeSoft", "Restaurante", "Impresion") = 1 Then
                FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
            ElseIf GetSetting("SeeSoft", "Restaurante", "Impresion") = 2 Then
                FacturaPVESR.Load(GetSetting("SeeSOFT", "A&B Reports", "RptFacturaPVE_SR"))
            End If
            CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVESR, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))
           
            FacturaPVESR.Refresh()
            FacturaPVESR.PrintOptions.PrinterName = Impresora
            FacturaPVESR.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            FacturaPVESR.SetParameterValue(0, NFactura)
            FacturaPVESR.SetParameterValue(1, User_Log.PuntoVenta)
            FacturaPVESR.SetParameterValue(2, numeroComanda)
            FacturaPVESR.SetParameterValue(3, False)
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
            Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

            If nc = "1" Or nc = "2" Then
                If dt.Rows.Count > 0 Then
                    FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                    ' FacturaPVESR.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                    '  FacturaPVESR.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                    '  FacturaPVESR.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))

                Else
                    FacturaPVESR.SetParameterValue(4, "")
                    ' FacturaPVESR.SetParameterValue(5, "")
                    ' FacturaPVESR.SetParameterValue(6, "")
                    ' FacturaPVESR.SetParameterValue(7, Comenzales)
                End If
            Else
                If dt.Rows.Count > 0 Then
                    FacturaPVESR.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                Else
                    FacturaPVESR.SetParameterValue(4, 0)
                End If
            End If
            FacturaPVESR.SetParameterValue(5, conce)
            If TipoImpres = 1 Or TipoImpres = 2 Then
                FacturaPVESR.PrintToPrinter(1, True, 0, 0)
            ElseIf TipoImpres = 3 Then
                If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                Else
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                End If
            ElseIf TipoImpres = 4 Then
                If MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    FacturaPVESR.PrintToPrinter(1, True, 0, 0)
                End If
            End If


        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "imprimir_SERVICIO_RESTAURANTE", ex.Message)
        End Try
    End Sub
    Sub IMPRIMIR_MEJORADO(ByVal ServiProf As Boolean, ByVal NFactura As String)
        Try

            id_ventas = cConexion.SlqExecuteScalar(cConexion.Conectar, "Select MAX(Id) as Id from Ventas where Num_Factura=" & NFactura & " and Proveniencia_Venta =" & User_Log.PuntoVenta)
            Dim conce As String = ""
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo_Hacienda FROM [Hotel].[dbo].[TB_CE_ConsecutivoProvisionalHA] where  Id_Factura = " & id_ventas, dt)
            If dt.Rows.Count > 0 Then

                conce = dt.Rows(0).Item("Consecutivo_Hacienda")
            End If
            If ServiProf Then
                If (GetSetting("SeeSOFT", "Restaurante", "Rapido") = 1) Then
                    Dim Impre As String
                    Impre = busca_impresora()
                    If Impre = "" Then
                        MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        Impre = busca_impresora()
                        If Impre = "" Then
                            Exit Sub
                        End If
                    End If

                    Dim factura As New cls_Facturas
                    factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impre, True)
                    Exit Sub
                End If
                Me.imprimir_SERVICIO_RESTAURANTE(NFactura, "espanol", conce)
            Else
                Dim Impresora As String
                Impresora = busca_impresora()
                If Impresora = "" Then
                    MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Impresora = busca_impresora()
                    If Impresora = "" Then
                        Exit Sub
                    End If
                End If

                If (GetSetting("SeeSOFT", "Restaurante", "Rapido") = 1) Then
                    Dim factura As New cls_Facturas()
                    factura.ImprimirFacturaEsp(NFactura, numeroComanda, Impresora, False)
                    Exit Sub
                End If

                FacturaPVE.Refresh()
                FacturaPVE.PrintOptions.PrinterName = Impresora
                FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                FacturaPVE.SetParameterValue(0, NFactura)
                FacturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
                FacturaPVE.SetParameterValue(2, numeroComanda)
                FacturaPVE.SetParameterValue(3, False)

                cFunciones.Llenar_Tabla_Generico("SELECT  comanda.CedSalonero, Usuarios_1.Nombre AS NCajero, Usuarios_2.Nombre AS NSalonero, comanda.Comenzales FROM Comanda INNER JOIN Usuarios AS Usuarios_1 ON Comanda.Cedusuario = Usuarios_1.Cedula INNER JOIN Usuarios AS Usuarios_2 on  Usuarios_2.Cedula = Comanda.CedSalonero  WHERE (Comanda.Numerofactura =  '" & NFactura & "')", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                'EL TIPO 0 Sera para factura muy simple con margen pegado a la izq
                Dim nc As String = GetSetting("SeeSofT", "Restaurante", "Impresion")

                If nc = "1" Or nc = "2" Then
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                        FacturaPVE.SetParameterValue(5, dt.Rows(0).Item("NSalonero"))
                        FacturaPVE.SetParameterValue(6, dt.Rows(0).Item("NCajero"))
                        FacturaPVE.SetParameterValue(7, dt.Rows(0).Item("Comenzales"))
                        FacturaPVE.SetParameterValue(8, conce)

                    Else
                        FacturaPVE.SetParameterValue(4, "")
                        FacturaPVE.SetParameterValue(5, "")
                        FacturaPVE.SetParameterValue(6, "")
                        FacturaPVE.SetParameterValue(7, Comenzales)
                        FacturaPVE.SetParameterValue(8, conce)
                    End If
                Else
                    If dt.Rows.Count > 0 Then
                        FacturaPVE.SetParameterValue(4, dt.Rows(0).Item("CedSalonero"))
                    Else
                        FacturaPVE.SetParameterValue(4, 0)
                    End If
                End If

                If TipoImpres = 1 Or TipoImpres = 2 Then
                    FacturaPVE.PrintToPrinter(1, True, 0, 0)
                ElseIf TipoImpres = 3 Then
                    If MessageBox.Show("¿Desea imprimir la factura en Español?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        FacturaPVE.PrintToPrinter(1, True, 0, 0)
                        spImprimirComision(NFactura, Impresora)
                    Else
                        FacturaPVEIng.PrintToPrinter(1, True, 0, 0)
                        spImprimirComision(NFactura, Impresora)
                    End If
                ElseIf TipoImpres = 4 Then
                    If MessageBox.Show("Desea imprimir la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        FacturaPVE.PrintToPrinter(1, True, 0, 0)
                        spImprimirComision(NFactura, Impresora)
                    End If
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "IMPRIMIR_MEJORADO", ex.Message)
        End Try
    End Sub
    Private Sub spImprimirComision(ByVal _NFactura As Integer, ByVal _Impresora As String)

        Try
         
            If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then

                Dim cx As New Conexion
                Dim Existe As Integer = 0
                Existe = cx.SlqExecuteScalar(cx.Conectar("Hotel"), "SELECT COUNT(*) FROM tb_FD_Comision where Num_Factura = '" & _NFactura & "'")
                If Existe > 0 Then
                    If MessageBox.Show("Desea imprimir el comprobante de Comisión?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        Dim rpt As New rptComision1
                        rpt.Refresh()
                        rpt.PrintOptions.PrinterName = _Impresora
                        rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        rpt.SetParameterValue("Num_Factura", _NFactura)
                        rpt.SetParameterValue("PuntoVenta", User_Log.PuntoVenta)
                        CrystalReportsConexion.LoadReportViewer(Nothing, rpt, True, GetSetting("SeeSOFT", "Hotel", "Conexion"))
                        rpt.PrintToPrinter(1, True, 0, 0)
                    End If
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "spImprimirComision", ex.Message)
        End Try

    End Sub
    Private Sub Imprimir(ByVal NFactura As Integer)
        IMPRIMIR_MEJORADO(CheckBoxSR.Checked, NFactura)
    End Sub
    Public Sub cort()
        Try
            Dim intFileNo As Integer = FreeFile()
            FileOpen(1, "c:\corte.txt", OpenMode.Output)
            PrintLine(1, Chr(29) & Chr(86) & Chr(0))
            FileClose(1)
            Shell("print LPT1 c:\corte.txt", AppWinStyle.NormalFocus)
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "cort", ex.Message)
        End Try
    End Sub

    Private Sub imprimir1(ByVal NFactura As Integer, ByVal numero_cortesia As Integer)
        Try
            Dim facturaPVE As New rptCortesias
            Dim impresora As String
            impresora = busca_impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = busca_impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If
            facturaPVE.PrintOptions.PrinterName = impresora
            facturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            facturaPVE.SetParameterValue(0, NFactura)
            facturaPVE.SetParameterValue(1, numero_cortesia)
            facturaPVE.SetParameterValue(2, autorizadox)
            'Falso para no mostrar los costos de la cortesía, True para mostrarlos.
            If User_Log.CostoCortesia = True Then
                facturaPVE.SetParameterValue(3, True)
            Else
                facturaPVE.SetParameterValue(3, False)
            End If
            facturaPVE.SetParameterValue(4, Observacionesx)
            CrystalReportsConexion.LoadReportViewer(Nothing, facturaPVE, True, conectadobd.ConnectionString)
            facturaPVE.PrintToPrinter(1, True, 0, 0)
            facturaPVE.Dispose()
            facturaPVE.Close()
            facturaPVE = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "imprimir1", ex.Message)
        End Try
    End Sub

    Private Sub imprimir2(ByVal NFactura As Integer, ByVal numero_cortesia As Integer)
        Try
            Dim facturaPVE As New rptCortesias
            Dim impresora As String
            impresora = busca_impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = busca_impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If
            facturaPVE.PrintOptions.PrinterName = impresora
            facturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            facturaPVE.SetParameterValue(0, NFactura)
            facturaPVE.SetParameterValue(1, numero_cortesia)
            facturaPVE.SetParameterValue(2, autorizadox)
            facturaPVE.SetParameterValue(3, False)
            facturaPVE.SetParameterValue(4, Observacionesx)
            CrystalReportsConexion.LoadReportViewer(Nothing, facturaPVE, True, conectadobd.ConnectionString)
            facturaPVE.PrintToPrinter(1, True, 0, 0)
            facturaPVE.Dispose()
            facturaPVE.Close()
            facturaPVE = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "imprimir2", ex.Message)
        End Try
    End Sub

    Private Function busca_impresora() As String
        Try
            Dim PrintDocument1 As New PrintDocument
            Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
            Dim PrinterInstalled As String
            'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                    Case "FACTURACION" 'FACTURACION
                        Return PrinterInstalled.ToString
                        Exit Function
                End Select
            Next

            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            If PrinterDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return "" 'Ninguna Impresora
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "busca_impresora", ex.Message)
        End Try
    End Function
#End Region

#Region "Anular"
    Private Sub anulando()
        Try
            Dim Mayorizado, Configuracion, Facturado As Boolean
            Dim Cod_Cliente As String
            Dim Conexion As New ConexionR
            Configuracion = cConexion.SlqExecuteScalar(conectadobd, "Select Contabilidad FROM Configuraciones")

            If MessageBox.Show("¿Esta seguro(a) que desea anular la factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then

                If cFunciones.fnValidaIntegracionFdNuevo() Then
                    Dim dt As New DataTable
                    Dim tipoFactura As String
                    tipoFactura = cConexion.SlqExecuteScalar(cConexion.SqlConexion, "SELECT tipo FROM Hotel.dbo.VENTAS WHERE ID =" & idfactura)
                    If tipoFactura = "CAR" Then
                        cFunciones.Llenar_Tabla_Generico("SELECT Facturada  FROM vs_FD_FacturaFdNuevo where IdPuntoVenta = '" & GetSetting("TourCo", "Conf", "RE-IdPuntoVentaAsignado") & "' and Numero = '" & NFactura & "'", dt)
                        If dt.Rows.Count Then
                            If dt.Rows(0).Item(0) Then
                                MessageBox.Show("No se puede anular la factura, la cuenta en casa esta facturada.", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                                Exit Sub
                            End If
                        Else
                            MessageBox.Show("No se puede anular la factura, la cuenta en casa esta facturada.", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                            Exit Sub
                        End If
                    End If

                End If

                '----------------------------------------------------------------------------
                'VERIFICA SI LA FACTURA FUE COBRADA - ORA
                Facturado = Conexion.SlqExecuteScalar(Conexion.Conectar(), "SELECT ISNULL(Facturado,0) FROM Comanda WHERE Numerofactura= " & NFactura)
                If Facturado = False Then
                    MessageBox.Show("La factura NO se puede anular, porque todavia se encuentra a cobro!", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
                '----------------------------------------------------------------------------

                '----------------------------------------------------------------------------

                '------------------------------------------------------------------------
                'VERIFICA SI EL ASIENTO ESTA HECHO
                Mayorizado = Conexion.SlqExecuteScalar(Conexion.Conectar("Hotel"), "SELECT AsientoVenta FROM Ventas WHERE ID =" & idfactura)
                cConexion.DesConectar(conectadobd)
                If Mayorizado Then
                    MessageBox.Show("La factura NO se puede anular, porque ya se realizo el asiento!", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
                '------------------------------------------------------------------------
                conectadobd = cConexion.Conectar()

                '----------------------------------------------------------------------------

                If ACTIVA <> 0 Then
                    If MessageBox.Show("La mesa " & nombremesa1 & " esta ocupada, desea continuar con la anulación ?" & vbCrLf & "NOTA: Los productos de la factura a anular se cargaran nuevamente en la mesa.!!", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                        Close()
                    End If
                End If

                Dim fAnulacion As New anulacion
                Dim aCedula, aCuentaContable, aNota As String
                fAnulacion.ShowDialog()
                aCedula = fAnulacion.cedula
                aCuentaContable = fAnulacion.CuentaContable
                Dim Devolver As Boolean = fAnulacion.cbkCortesia.Checked
                aNota = fAnulacion.Notas
                fAnulacion.Dispose()
                Dim Express As Integer = 0

                If Trim(aCedula) = vbNullString Then
                    MessageBox.Show("No se realizo la anulacion")
                    Exit Sub
                End If
                If Devolver = False Then
                    Dim costo As String
                    costo = cConexion.SlqExecuteScalar(conectadobd, "SELECT SUM(Precio_Costo) AS costo FROM Hotel.dbo.Ventas_Detalle WHERE Id_Factura = " & idfactura)
                    cConexion.UpdateRecords(conectadobd, "Comanda", "Subtotal=0,Impuesto_Venta=0,CuentaContable='" & aCuentaContable & "',Total=" & costo, "Numerofactura=" & NFactura)
                    cConexion.AddNewRecord(conectadobd, "BitacoraRestaurante", "HechoPor,identificador,fecha,Proveniencia,TipoEvento,Nota", "'" & aCedula & "','" & NFactura & "','" & Date.Now & "','RESTAURANTE','ANULACION','" & aNota & "'")
                    cConexion.UpdateRecords(conectadobd, "Hotel.dbo.Ventas", "Anulado=1", "Id=" & idfactura)
                Else
                    cConexion.AddNewRecord(conectadobd, "BitacoraRestaurante", "HechoPor,identificador,fecha,Proveniencia,TipoEvento,Nota", "'" & aCedula & "','" & NFactura & "','" & Date.Now & "','RESTAURANTE','ANULACION','" & aNota & "'")
                    Dim cProducto, cCodigo, cMesa, personas, idc As Integer
                    Dim cCantidad, cPrecio, costo_real As Double
                    Dim cDescripcion, impresora, nombre_reserva As String
                    Dim fila As DataRow
                    Dim fila_acom As DataRow ' Recorre los acompañamientos asociados a la comanda anulada
                    Dim datos As String
                    nombre_reserva = ""
                    Dim ds_acompañamientos As New DataSet
                    Dim separada As Integer = 0
                    Dim cCodigoActual As Integer

                    cConexion.GetDataSet(conectadobd, "SELECT dbo.DetalleMenuComanda.IdDetalle,Comanda.Idcomanda AS Idcomanda,DetalleMenuComanda.Hora, DetalleMenuComanda.Idmenu AS cProducto, DetalleMenuComanda.Cantidad AS cCantidad, Menu_Restaurante.Nombre_Menu, DetalleMenuComanda.PrecioUnitario, Comanda.NumeroComanda, Comanda.IdMesa AS Id_Mesa, DetalleMenuComanda.Cantidad AS Impreso, Menu_Restaurante.Impresora AS Impresora, Comanda.Comenzales AS Comenzales, Menu_Restaurante.Tipo AS Tipo_Plato, Comanda.CedSalonero AS Cedula, Comanda.Numerofactura, dbo.Menu_Restaurante.Numero_Acompañamientos, dbo.DetalleMenuComanda.costo_real FROM DetalleMenuComanda INNER JOIN Comanda ON DetalleMenuComanda.IdComanda = Comanda.Idcomanda INNER JOIN Menu_Restaurante ON DetalleMenuComanda.Idmenu = Menu_Restaurante.Id_Menu WHERE Comanda.Numerofactura = " & NFactura, DataS, "ComandaT")

                    If ACTIVA <> 0 Then
                        separada = cConexion.SlqExecuteScalar(conectadobd, "Select separada from Mesas where Id = " & ID_MESA)
                        cCodigoActual = cConexion.SlqExecuteScalar(conectadobd, "Select top 1 cCodigo from ComandasActivas where Estado = 'Activo' AND cMesa = " & ID_MESA)
                        If separada > 0 Then
                            separada += 1
                            cConexion.SlqExecute(conectadobd, "INSERT INTO Tb_R_Cuentas (IdMesa,Cuenta,NombreCuenta) VALUES ('" & ID_MESA & "','" & separada & "','FACT ANULADA " & NFactura & "')")
                        Else
                            separada = 2
                            cConexion.UpdateRecords(conectadobd, "Comandatemporal", "separada = 1 ", "Estado  = 'Activo' AND cMesa=" & ID_MESA)
                            cConexion.SlqExecute(conectadobd, "INSERT INTO Tb_R_Cuentas (IdMesa,Cuenta) VALUES ('" & ID_MESA & "','" & 1 & "')")
                            cConexion.SlqExecute(conectadobd, "INSERT INTO Tb_R_Cuentas (IdMesa,Cuenta,NombreCuenta) VALUES ('" & ID_MESA & "','" & separada & "','FACT ANULADA " & NFactura & "')")
                        End If
                    Else
                        separada = 1
                        cConexion.UpdateRecords(conectadobd, "Comandatemporal", "separada = 1 ", "Estado  = 'Activo' AND cMesa=" & ID_MESA)
                        cConexion.SlqExecute(conectadobd, "INSERT INTO Tb_R_Cuentas (IdMesa,Cuenta,NombreCuenta) VALUES ('" & ID_MESA & "','" & separada & "','FACT ANULADA " & NFactura & "')")

                    End If
                    Dim dtExpress As New DataTable()

                    For Each fila In DataS.Tables("ComandaT").Rows
                        idc = fila("idcomanda")
                        cProducto = fila("CPRODUCTO")
                        cFunciones.Llenar_Tabla_Generico("Select Top 1 ct.Express from Comanda c left outer join ComandaTemporal ct On c.NumeroComanda=ct.cCodigo where c.Idcomanda=" & idc & "", dtExpress)
                        If dtExpress.Rows.Count > 0 Then
                            Express = Convert.ToInt32(dtExpress.Rows(0).Item("Express"))
                        End If
                        cCantidad = fila("CCantidad")
                        cDescripcion = fila("Nombre_Menu")
                        cPrecio = fila("PrecioUnitario")
                        cCodigo = fila("NumeroComanda")
                        cMesa = fila("Id_Mesa")
                        impresora = fila("Impresora")
                        personas = fila("Comenzales")
                        costo_real = fila("costo_real")

                        If codMoneda <> CodMonedaC Then
                            If codMoneda = 2 Then
                                cPrecio = (cPrecio * valor)
                                costo_real = (costo_real * valor)
                            ElseIf valor < ValorMonedaC Then
                                cPrecio = (cPrecio / ValorMonedaC)
                                costo_real = (costo_real / ValorMonedaC)
                            End If
                        End If

                        cPrecio = Format(cPrecio, "##,##0.00")
                        costo_real = Format(costo_real, "##,##0.00")
                        If Not ACTIVA <> 0 Then
                            cCodigoActual = cCodigo
                        End If

                        datos = cProducto & "," & cCantidad & ",'" & cDescripcion & "'," & cPrecio & "," & cCodigoActual & "," & cMesa & ",1,'" & impresora & "'," & personas & ",'Principal'" & ", 1 , " & costo_real & ",'" & separada & "'"
                        cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, comandado, costo_real, separada", datos)

                        If fila("Numero_Acompañamientos") > 0 Then
                            cConexion.GetDataSet(conectadobd, "SELECT dbo.Comanda.Idcomanda, DetalleAcompañamiento_Comanda.IdAcompañamiento as IdMenu , dbo.DetalleMenuComanda.Cantidad , dbo.Acompañamientos_Menu.Nombre, dbo.Comanda.NumeroComanda, dbo.Comanda.IdMesa, dbo.Comanda.Comenzales, dbo.Acompañamientos_Menu.Tipo, dbo.DetalleMenuComanda.costo_real, dbo.Comanda.CedSalonero, dbo.Comanda.Numerofactura FROM dbo.DetalleMenuComanda INNER JOIN dbo.Comanda ON dbo.DetalleMenuComanda.IdComanda = dbo.Comanda.Idcomanda INNER JOIN dbo.DetalleAcompañamiento_Comanda ON dbo.DetalleMenuComanda.IdDetalle = dbo.DetalleAcompañamiento_Comanda.IdDetalle INNER JOIN dbo.Acompañamientos_Menu ON dbo.DetalleAcompañamiento_Comanda.IdAcompañamiento = dbo.Acompañamientos_Menu.Id WHERE dbo.DetalleMenuComanda.IdDetalle = " & fila("IdDetalle"), ds_acompañamientos, "Acompañamientos")
                            For Each fila_acom In ds_acompañamientos.Tables("Acompañamientos").Rows
                                'datos = fila_acom("Idmenu") & "," & fila_acom("Cantidad") & ",'" & fila_acom("Nombre") & "'," & cPrecio & "," & fila_acom("NumeroComanda") & "," & fila_acom("IdMesa") & ", 1, '', " & fila_acom("Comenzales") & ", 'Acompañamiento', 1," & costo_real
                                datos = fila_acom("Idmenu") & "," & 0 & ",'" & fila_acom("Nombre") & "'," & 0 & "," & cCodigoActual & "," & fila_acom("IdMesa") & ", 1, '', " & fila_acom("Comenzales") & ", 'Acompañamiento', 1," & costo_real & ",'" & separada & "'"
                                cConexion.AddNewRecord(conectadobd, "ComandaTemporal", "cProducto, cCantidad, cDescripcion, cPrecio, cCodigo, cMesa, Impreso, Impresora, Comenzales, Tipo_Plato, comandado, costo_real, separada", datos)
                            Next
                            ds_acompañamientos.Tables("Acompañamientos").Clear()
                        End If
                    Next

                    cConexion.UpdateRecords(conectadobd, "Comanda", "anulado = 1 ", "idcomanda=" & idc)
                    cConexion.UpdateRecords(conectadobd, "Mesas", "activa=1 , separada = " & separada, "Id=" & cMesa)
                    cConexion.UpdateRecords(conectadobd, "Hotel.dbo.Ventas", "Anulado=1", "Id=" & idfactura)

                    '------------------------------------------------------------------
                    Cod_Cliente = Mayorizado = Conexion.SlqExecuteScalar(Conexion.Conectar("Hotel"), "SELECT Cod_Cliente FROM Ventas WHERE ID =" & idfactura)
                    cConexion.DesConectar(conectadobd)

                    generarConsec.crearNumeroConsecutivo(Cod_Cliente, idfactura, NFactura, Now, True)
                    '------------------------------------------------------------------


                    cConexion.DesConectar(conectadobd)
                    Close()
                End If

                'Modificacion!!!!
                'Se puso esta linea aqui para que lo ejecutara siempre no importa si devuelve o no
                'los platos a la mesa.
                AplicarDevolucionCaja_x_Anulacion_Factura()
                '----------------------------------------------------------------------------
                'ASIENTO CONTABLE
                cConexion.DesConectar(conectadobd)
                If Configuracion Then
                    cConexion.UpdateRecords(cConexion.Conectar("Contabilidad"), "AsientosContables", "Anulado=1", "Modulo = 'Facturacion Restaurante' And IdNumDoc = " & idfactura)
                End If
                cConexion.DesConectar(conectadobd)
                conectadobd = cConexion.Conectar()
                '----------------------------------------------------------------------------
                MessageBox.Show("La factura ha sido anulada correctamente", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                If Express <> 1 Then
                    MessageBox.Show("Los platos fueron cargados de nuevo a la mesa en una cuenta separada Factura Anulada #" & NFactura, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                End If
            End If
            Close()
        Catch ex As Exception
            MessageBox.Show("Error al anular la factura: " & ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "anulando", ex.Message)
        End Try
    End Sub

    Private Sub AplicarDevolucionCaja_x_Anulacion_Factura() '(ByVal Factura As Double, ByVal Apertura As Double)
        Try
            If conectadobd.State <> ConnectionState.Closed Then conectadobd.Close()
            If conectadobd.State <> ConnectionState.Open Then
                conectadobd.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
                conectadobd.Open()
            End If
            Dim Campos As String = "Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario, Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha,  Numapertura", Datos As String = ""
            Dim DataSet As New DataSet
            Dim Tupla As DataRow

            Dim AperturaActual As Double = cConexion.SlqExecuteScalar(conectadobd, "SELECT NApertura FROM aperturacaja WHERE (Estado = 'A') AND (Anulado = 0) AND (Cedula = '" & User_Log.Cedula & "')")
            Dim AperturaFactura As Double = cConexion.SlqExecuteScalar(conectadobd, "SELECT Num_Apertura FROM Ventas WHERE Id =" & idfactura)

            If AperturaActual = 0 Then
                If AperturaActual <> AperturaFactura Then MsgBox("ADVERTENCIA: Las aperturas de cajas de la factura y la actual no es la misma !!!" & vbCrLf & "Se procedera a anular la caja con la apertura en que se realizó la venta..", MsgBoxStyle.Question, "Atención...")
                cConexion.GetDataSet(conectadobd, " SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario, Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha,  Numapertura FROM OpcionesDePago WHERE (Documento = " & NFactura & ") AND (TipoDocumento = 'FV')", DataSet, "OpcionesPago")

                For Each Tupla In DataSet.Tables("OpcionesPago").Rows
                    Datos = Tupla!documento & ",'FVA'," & (Tupla!MontoPago * -1) & ",'" & Tupla!FormaPago & "'," & (Tupla!Denominacion * -1) & ",'" & User_Log.Cedula & "','" & User_Log.Nombre & "'," & Tupla!CodMoneda & ",'" & Tupla!Nombremoneda & "'," & Tupla!TipoCambio & ", GETDATE()," & AperturaFactura
                    cConexion.AddNewRecord(cConexion.SqlConexion, "OpcionesDePago", Campos, Datos)
                Next
            End If

            If AperturaActual <> 0 Then
                If AperturaActual <> AperturaFactura Then MsgBox("ADVERTENCIA: Las aperturas de cajas de la factura y la actual no es la misma !!!" & vbCrLf & "Se procedera a anular la caja con la apertura que tiene asignada el usuario actual..", MsgBoxStyle.Question, "Atención...")
                cConexion.GetDataSet(conectadobd, " SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario, Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha,  Numapertura FROM OpcionesDePago WHERE (Documento = " & NFactura & ") AND (TipoDocumento = 'FV')", DataSet, "OpcionesPago")

                For Each Tupla In DataSet.Tables("OpcionesPago").Rows
                    Datos = Tupla!documento & ",'FVA'," & (Tupla!MontoPago * -1) & ",'" & Tupla!FormaPago & "'," & (Tupla!Denominacion * -1) & ",'" & User_Log.Cedula & "','" & User_Log.Nombre & "'," & Tupla!CodMoneda & ",'" & Tupla!Nombremoneda & "'," & Tupla!TipoCambio & ", GETDATE()," & AperturaActual
                    cConexion.AddNewRecord(cConexion.SqlConexion, "OpcionesDePago", Campos, Datos)
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "AplicarDevolucionCaja_x_Anulacion_Factura", ex.Message)
        End Try
    End Sub
#End Region

#Region "Teclado"
    Private Sub teclado()
        Try
            Call Shell("osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            cFunciones.spEnviar_Correo(Me.Name, "teclado", ex.Message)
        End Try
    End Sub
#End Region

#Region "Cortesia"
    Private Function RealizaCortesia() As Integer
        Try
            Dim codigo(100) As Integer
            DataGridView1.Rows.Clear()
            Dim str As String
            Dim ee As Integer = 0
            Dim observaciones As String

            Dim detalle_cortesia As New FrmCortesia
            detalle_cortesia.rtcObservaciones.Text = Me.nombre + "  " + Observacion
            detalle_cortesia.ShowDialog()
            autorizadox = detalle_cortesia.autorizado
            observaciones = detalle_cortesia.observaciones
            Observacionesx = observaciones
            detalle_cortesia.Dispose()

            If detalle_cortesia.completa = False Then
                Return 0
                Exit Function
            End If

            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            'Aqui se busca la ultima cortesia que se realizo
            Dim numero_cortesia As Int64

            numero_cortesia = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(MAX(NumeroCortesia),0) as NumeroCortesia from Comanda")
            numero_cortesia = numero_cortesia + 1

            If separada = False Then
                If int = 1 Then 'psv
                    str = "SELECT ComandaTemporal1.*, 'BUFFET' as Nombre_Mesa FROM ComandaTemporal1  WHERE ComandaTemporal1.cPrecio>0 and Estado='Activo' and cMesa = " & idmesa & " and ComandaTemporal1.separada=" & numeroSeparado
                Else 'psv
                    str = "SELECT ComandasActivas.*, Mesas.Nombre_Mesa FROM ComandasActivas inner join Mesas on ComandasActivas.cMesa = Mesas.Id WHERE ComandasActivas.cPrecio>0 and Estado='Activo'and cMesa = " & idmesa & " and Impreso=1 and ComandasActivas.separada=" & numeroSeparado
                End If 'psv
            Else
                str = "SELECT ComandasActivas.cProducto,ComandasActivas.Tipo_Plato, ComandasActivas.cCodigo, Mesas.Nombre_Mesa, ComandasActivas.Comenzales, Cuentas.Cantidad as cCantidad, ComandasActivas.Costo_Real, ComandasActivas.cPrecio, ComandasActivas.cDescripcion FROM ComandasActivas INNER JOIN Cuentas ON ComandasActivas.id = Cuentas.idcTemporal inner join Mesas on ComandasActivas.cMesa = Mesas.Id where ComandasActivas.cPrecio>0 and Cuentas.IdMesa= ComandasActivas.cMesa and ComandasActivas.Estado='Activo' and ComandasActivas.cMesa= " & idmesa & " and ComandasActivas.Impreso=1 and ComandasActivas.separada=" & numeroSeparado
            End If

            cConexion.GetRecorset(conectadobd, str, rs)
            Dim contador As Integer = 0
            Dim i As Integer = 1
            Global_subtotal = 0
            While rs.Read
                codigo(contador) = rs("cProducto")
                TxtComanda.Text = numeroComanda
                TxtComensales.Text = ""
                TxtMesa.Text = ""

                With DataGridView1.Rows
                    If i = 1 Then
                        .Add("Cant.", "Descripción", "P/Unitario", "Subtotal")
                        i = 0
                    End If

                    If Trim(rs("Tipo_Plato")) <> "Modificador" Then
                        .Add(rs("cCantidad"), rs("cDescripcion"), Format(rs("costo_real"), "#####0.00"), Format((rs("costo_real") * rs("cCantidad")), "#####0.00"))
                    End If
                End With
                Global_subtotal = Global_subtotal + (rs("cCantidad") * rs("costo_real"))
                contador = contador + 1
            End While

            rs.Close()
            txtSubtotal.Text = Format(Global_subtotal, "#,##0.00")
            txtIS.Text = "0.00"
            txtIV.Text = "0.00"
            txtTotal.Text = Format(Global_subtotal, "#,##0.00")

            Dim oo As Integer
            Dim totalCortesia As Double = 0
            For oo = 0 To contador - 1
                Dim preciocosto As Double
                cConexion.GetRecorset(conectadobd, "Select CostoTotal from Menu_Restaurante where Id_Menu =" & codigo(oo), rs)
                While rs.Read
                    preciocosto += rs("CostoTotal")
                End While
                totalCortesia = totalCortesia + preciocosto
                rs.Close()
            Next
            Dim imp As Integer
            If Me.CheckBoxIS.Checked = False Then
                imp = 1
            Else
                imp = 0
            End If
            Dim venta As Integer = 0
            If Me.CheckExonerar.Checked = False Then
                venta = 1
            Else
                venta = 0
            End If
            cConexion.AddNewRecord(conectadobd, "Comanda", "NumeroComanda, Fecha, Subtotal, Impuesto_venta, Total, Cedusuario, CedSalonero, IdMesa, Comenzales, Facturado, Numerofactura,CuentaContable,cortesia ,NumeroCortesia, Cod_Moneda", numeroComanda & ", GetDate()," & CDbl(txtSubtotal.Text) & "," & CDbl(txtIV.Text) & "," & totalCortesia & ",'" & CedulaU & "','" & CedulaS & "'," & idmesa & "," & Comenzales & ",1,0,'" & TextBox1.Text & "', 1, " & numero_cortesia & "," & codMoneda)
            cConexion.AddNewRecord(conectadobd, "Cortesia", "numero_cortesia, autorizado,observaciones, cuenta_contable, descripcion_cuenta,id_centro,Num_Apertura", numero_cortesia & " , '" & autorizadox & "', '" & observaciones & "', '" & TextBox1.Text & "','" & CuentaCortesia & "','" & Me.Id_Centro_Cortesia & "','" & Num_Apertura & "'")

            conectadobd.Close()
            Dim uu As String
            If separada = False Then
                uu = "activa=0"
            Else
                Dim dt As New DataTable
                cFunciones.Llenar_Tabla_Generico("Select * From ComandasActivas Where Estado='Activo' and cMesa = " & Me.idmesa & " AND separada <> " & Me.numeroSeparado, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                If dt.Rows.Count = 0 Then

                    uu = "activa=0"

                Else
                    uu = "activa=1"
                End If

            End If


            Dim condicion As String
            condicion = "Id=" & idmesa

            cConexion.UpdateRecords(conectadobd, "Mesas", uu, condicion)
            conectadobd.Close()
            Return numero_cortesia

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "RealizarCortesia", ex.Message)
        End Try
    End Function

    Private Sub buscacuenta()
        Try
            Dim rs As SqlClient.SqlDataReader
            Dim fbuscador As New buscador

            fbuscador.check = 1
            fbuscador.ToolBarImprimir.Enabled = False
            fbuscador.TituloModulo.Text = "Buscando Centro de Cortesia"
            fbuscador.bd = "Restaurante" : fbuscador.tabla = "Cortesia_Centro"
            fbuscador.busca = "Nombre" : fbuscador.cantidad = 3 : fbuscador.lblEncabezado.Text = "Cortesías"
            fbuscador.consulta = "SELECT Id_Centro, Nombre, Observaciones FROM Cortesia_Centros where inhabilitado =0"
            fbuscador.ShowDialog()

            If fbuscador.Icodigo = 0 Then
                cbkCortesia.Checked = False
                Exit Sub
            End If

            Id_Centro_Cortesia = fbuscador.Icodigo

            cConexion.GetRecorset(conectadobd, "Select Nombre, ISNULL(CuentaContable,'') AS CuentaContable, ISNULL(DescripcionCuenta,'') AS DescripcionCuenta, Observaciones FROM Cortesia_Centros WHERE id_Centro =" & fbuscador.Icodigo, rs)
            While rs.Read

                TextBox1.Text = rs("CuentaContable")
                CuentaCortesia = rs("DescripcionCuenta")
                Observacion = rs("Observaciones")
                nombre = rs("Nombre")
            End While
            rs.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "buscacuenta", ex.Message)
        End Try
    End Sub
#End Region

#Region "Funciones Controles"
    Private Sub Factura_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 And ToolBar2.Buttons(0).Enabled = True Then
            CrearFactura()
        End If
    End Sub
    Private Sub checkCliente()
        ButtonBuscarCliente.Enabled = ckbEspecifico.Checked
        txtcliente.Enabled = ckbEspecifico.Checked
        If ckbEspecifico.Checked = False Then
            txtcliente.Enabled = True
            txtcliente.Text = "CLIENTE CONTADO"
            txtcodcliente.Text = "0"
            txtdescuento.Text = 0
			calculo_descuento()
			CheckExonerar.Checked = True
            PorcExonerado = 13
            CargarDatos_Comanda_Factura()
			MuestraTotales()
		Else
            txtcliente.Enabled = False
        End If
    End Sub
    Private Sub ckbEspecifico_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbEspecifico.CheckedChanged
        checkCliente()
    End Sub

    Private Sub rbcarga_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbcarga.CheckedChanged
        Try
            If rbcarga.Checked = True Then
                tipo_pago = "CAR"
                Dim detalleVentas As New ArrayList

                Dim fBuscadorHuesped As New BuscadorHuesped
                fBuscadorHuesped.ShowDialog()
                txtcliente.Text = fBuscadorHuesped.nombre
                txtIdHab.Text = fBuscadorHuesped.idReservacion
                txtcodcliente.Text = fBuscadorHuesped.idCliente
                id = fBuscadorHuesped.idReservacion
                fBuscadorHuesped.Dispose()
                If txtcodcliente.Text = "" Or txtcliente.Text = "" Then
                    rbContado.Checked = True
                    tipo_pago = "CON"
                    ckbEspecifico.Enabled = True
                    txtcliente.Text = "CLIENTE CONTADO"
                    txtcodcliente.Text = "0"
                Else
                    ckbEspecifico.Enabled = False
                End If
                If Me.Num_Apertura Is Nothing Then
                    Me.ToolBarRegistrar.Enabled = True

                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "rbcarga_CheckedChanged", ex.Message)
        End Try
    End Sub

    Private Sub rbCredito_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCredito.CheckedChanged
        Try
            If rbCredito.Checked = True And anular = False Then
                tipo_pago = "CRE"
                Dim credito As New ClienteCredito
                credito.ShowDialog()
                If credito.id <> "" Or credito.nombre <> "" Then
                    txtcodcliente.Text = credito.id
                    txtcliente.Text = credito.nombre
                    credito.Dispose()
                End If
                Dim precio As String
                conectadobd = cConexion.Conectar("Restaurante")
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                precio = cConexion.SlqExecuteScalar(conectadobd, "Select TipoPrecio as Precio From Cliente where id =" & txtcodcliente.Text)
                conectadobd.Close()
                If ((precio <> "TIPO A") And (precio <> Nothing)) Then
                    If MsgBox("El cliente tiene definido un tipo de precio diferente al Publico General (" & precio & ") desea aplicar este tipo de Precio.", MsgBoxStyle.YesNo, "Atención...") Then
                        modificarPrecios(precio)
                    End If
                End If

                If txtcodcliente.Text = "" Or txtcliente.Text = "" Then
                    rbContado.Checked = True
                    tipo_pago = "CON"
                    ckbEspecifico.Enabled = True
                    txtcliente.Text = "CLIENTE CONTADO"
                    txtcodcliente.Text = "0"
                Else
                    ckbEspecifico.Enabled = False
                End If
                If Num_Apertura Is Nothing Then
                    Me.ToolBarRegistrar.Enabled = True
                End If
            ElseIf Me.rbContado.Checked Then 'EN CADO DE MARCAR COMO CONTADO
                tipo_pago = "CON"
                rbContado.Checked = True
                tipo_pago = "CON"
                ckbEspecifico.Enabled = True
                txtcliente.Text = "CLIENTE CONTADO"
                txtcodcliente.Text = "0"
            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "rbCredito_CheckedChanged", ex.Message)
        End Try
    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownExtraPropina.ValueChanged
        If NumericUpDownExtraPropina.Value >= 0 Then
            MuestraTotales()
        End If
    End Sub

    Private Sub cboxMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxMoneda.SelectedIndexChanged
        Try
            DataV.RowFilter = "MonedaNombre ='" & cboxMoneda.SelectedItem & "'"
            Dim fil As DataRowView
            For Each fil In DataV
                codMoneda = fil("CodMoneda")
                NombreMoneda = fil("MonedaNombre")
                valor = fil("ValorVenta")
                If fil("CodMoneda") = 2 Then
                    valor1 = fil("ValorVenta")
                End If
            Next

            DataV = New DataView(DataS.Tables("Moneda"))
            If reimprimir = False Then
                MuestraTotales()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "cboxMoneda_SelectedIndexChanged", ex.Message)
        End Try
    End Sub

    Private Sub txtdescuento_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdescuento.ValueChanged
        If txtdescuento.Value >= 0 Or txtdescuento.Value <= 100 Then
            MuestraTotales()
        Else
            txtdescuento.Value = 0
            MuestraTotales()
            MessageBox.Show("Debe ingresar un valor entre 1 y 100 %", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End If
    End Sub

    Private Sub txtcliente_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtcliente.KeyDown
        If e.KeyCode = Keys.F2 And ToolBar2.Buttons(0).Enabled = True Then
            CrearFactura()
        End If
    End Sub

    Private Sub txtObservaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservaciones.KeyDown
        If e.KeyCode = Keys.F2 And ToolBar2.Buttons(0).Enabled = True Then
            CrearFactura()
        End If
    End Sub

    Private Sub chkComisionista_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComisionista.CheckedChanged
        Try
            If chkComisionista.Checked = True Then
                If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then
                    If rbContado.Checked Then
                        spCargarComision()
                    Else
                        MsgBox("La factura debe ser de CONTADO para aplicar comisión.")
                        txtComisionista.Text = ""
                        chkComisionista.Checked = False
                    End If

                Else
                    Dim Buscar1 As New BuscarCliente(Me.DatasetCliente1.Cliente)
                    If Buscar1.ShowDialog() = DialogResult.OK Then
                        BindingContext(Me.DatasetCliente1, "Cliente").Position = Buscar1.Pos
                        IdCliente = BindingContext(Me.DatasetCliente1, "Cliente").Current("Id")
                        txtComisionista.Text = BindingContext(Me.DatasetCliente1, "Cliente").Current("Nombre")
                        Buscar1.Dispose()
                    Else
                    End If
                End If
            Else
                txtComisionista.Text = ""
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "chkComisionista_CheckedChanged", ex.Message)
        End Try
    End Sub
    Dim ComisionCedula As String
    Dim ComisionNombre As String
    Dim ComisionNombreGuia As String
    Private Sub spCargarComision()
        Dim frm As New frmComision
        frm.Nuevo = True
        If frm.ShowDialog = Forms.DialogResult.OK Then

            txtComisionista.Text = frm.NombreCliente
            ComisionCedula = frm.CedulaCliente
            ComisionNombre = frm.NombreCliente
            ComisionNombreGuia = frm.NombreGuia
        Else
            txtComisionista.Text = ""
            chkComisionista.Checked = False
        End If
    End Sub

    Private Sub cbkCortesia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbkCortesia.CheckedChanged
        If cbkCortesia.Checked = True Then
            If MessageBox.Show("¿Desea aplicar cortesía a esta factura?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                buscacuenta()
                ' cbkCortesia.Enabled = False
                TextBox1.Enabled = False
            Else
                cbkCortesia.Checked = False
                Exit Sub
            End If
        End If
    End Sub

    Private Sub ckbdescuento_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbdescuento.CheckedChanged
        If ckbdescuento.Checked = True Then
            txtdescuento.Enabled = True
            txtdescuento.Value = 0
            MuestraTotales()
        Else
            Global_subtotal = guardasubtotal
            txtdescuento.Enabled = False
            txtdescuento.Value = 0
            MuestraTotales()
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxIS.CheckedChanged, CheckBoxISH.CheckedChanged
        Me.CargarDatos_Comanda_Factura()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscarCliente.Click
		Try
			Dim Exento As Boolean = False
			If ckbEspecifico.Checked = True Then
				'PSV 
				'Dim Buscar1 As New BuscarCliente(Me.DatasetCliente1.Cliente)

				'Dim buscar2 As New frmBuscarCliente()
				'buscar2.ShowDialog()

				Dim cs As New ClienteCorreo.ClienteSeleccionado
				cs = ClienteCorreo.ClientesHacienda.AbrirForm
				If cs.Id = 0 Then
					cs.Nombre = "CLIENTE CONTADO"
					txtcliente.Enabled = True
					ckbEspecifico.Checked = False
					checkCliente()
					Exit Sub

				End If

                adClientes.Fill(Me.DatasetCliente1, "Cliente")
                'If buscar2.resultado = True Then
                For i As Integer = 0 To Me.DatasetCliente1.Cliente.Rows.Count - 1

					If Me.DatasetCliente1.Cliente(i).Id = cs.Id Then
						BindingContext(Me.DatasetCliente1, "Cliente").Position = i
					End If
				Next

                Try
                    Dim dtCliente As New DataTable
                    dtCliente.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select PorcentajeIVA from TipoCliente left outer join Cliente on TipoCliente.Id=Cliente.IdTipoCliente where Cliente.Id=" & BindingContext(DatasetCliente1, "Cliente").Current("Id") & "", dtCliente, GetSetting("SeeSoft", "Hotel", "Conexion"))

                    If dtCliente.Rows.Count > 0 Then
                        PorcExonerado = dtCliente.Rows(0).Item("PorcentajeIVA")
                    Else
                        PorcExonerado = BindingContext(DatasetCliente1, "Cliente").Current("ImpustoVentas")
                    End If

                Catch ex As Exception
                    PorcExonerado = BindingContext(DatasetCliente1, "Cliente").Current("ImpustoVentas")
                End Try


                IdCliente = cs.Id
				txtcodcliente.Text = IdCliente

				txtcliente.Text = cs.Nombre
				Exento = Not BindingContext(DatasetCliente1, "Cliente").Current("Exento")
				If BindingContext(DatasetCliente1, "Cliente").Current("Comicion") > 0 Then
					ckbdescuento.Checked = True
					txtdescuento.Text = cs.Comision
					calculo_descuento()
				Else
					ckbdescuento.Checked = False
					txtdescuento.Text = cs.Comision
					calculo_descuento()
				End If

				'buscar2.Dispose()
			Else
				GoTo f
				ckbEspecifico.Checked = False
				txtcliente.Enabled = True
			End If
			' txtcliente.Enabled = False
			Dim precio As String

			If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
			precio = cConexion.SlqExecuteScalar(conectadobd, "Select TipoPrecio as Precio From Cliente where Nombre like '%" & txtcliente.Text & "%'")
			conectadobd.Close()
			calculo_descuento()
			modificarPrecios(precio)

			'Else
			'txtcliente.Enabled = False
			'txtcliente.Text = "CLIENTE CONTADO"
			'txtdescuento.Text = 0
			'txtcodcliente.Text = "0"S
			'End If
f:


			CheckExonerar.Checked = Exento

			MuestraTotales()
			txtcliente.Focus()

		Catch ex As Exception
			MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "Button1_Click", ex.Message)
        End Try
    End Sub

    Private Sub CheckExonerar_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckExonerar.CheckedChanged
        MuestraTotales()
    End Sub
#End Region

#Region "Funciones"

    Private Sub CambiaNombre()
        If idmesa = 0 Then Exit Sub
        If General.Exist(Me.idmesa) Then Me.txtcliente.Text = General.GetNombre(Me.idmesa)
    End Sub

    Private Sub CargarDatos_Comanda_Factura()
        Try
            If numeroComanda = 0 Then Exit Sub

            Dim str As String
            Global_subtotal = 0    'Se resetea el subtotal de la factura y todos los demas valores globales declarados en la region Variables
            iv = 0
            iss = 0
            Descuento = 0
            guardasubtotal = 0

            conectadobd = cConexion.Conectar("Restaurante")
            If anular = False Then
                If separada = False Then
                    If int = 1 Then 'psv
                        str = "SELECT ComandaTemporal1.*, 'BUFFET' as Nombre_Mesa FROM ComandaTemporal1  WHERE Estado='Activo'and cMesa = " & idmesa & ""
                    Else 'psv
                        str = "SELECT ComandasActivas.*, (CASE WHEN ComandasActivas.cProducto = 0 Then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) Else ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100)end) AS IVentas, " &
                        "(CASE WHEN ComandasActivas.cProducto = 0 Then ComandasActivas.cCantidad * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0) ELSE ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1,  2, 0)END) AS IServicios, " &
                        "Mesas.Nombre_Mesa AS Expr1 FROM Configuraciones CROSS JOIN  ComandasActivas WITH (NOLOCK) INNER JOIN  Mesas ON ComandasActivas.cMesa = Mesas.Id LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id  WHERE  Estado='Activo' and cMesa = " & idmesa
                    End If 'psv

                Else
                    If numeroSeparado > 0 Then
                        If cConexion.SlqExecuteScalar(cConexion.Conectar, "SELECT Cuentas.id FROM Cuentas INNER JOIN  ComandasActivas ON Cuentas.idcTemporal = ComandasActivas.id WHERE Estado='Activo' and cMesa = " & idmesa & " AND separada =" & numeroSeparado) <> "" Then
                            'la cuenta ya existe.. NO SE HACE NADA.
                        Else
                            cConexion.SlqExecute(conectadobd, "Insert into cuentas SELECT cMesa, id, cCantidad FROM ComandasActivas WHERE Estado='Activo' and cMesa = " & idmesa & " AND separada =" & numeroSeparado)
                        End If
                    End If
                    If (numeroSeparado = 0) Then
                        str = "SELECT ComandasActivas.cProducto, ComandasActivas.Tipo_Plato, ComandasActivas.cCodigo, Mesas.Nombre_Mesa, ComandasActivas.Comenzales, Cuentas.Cantidad AS cCantidad,  ComandasActivas.cPrecio, ComandasActivas.cDescripcion, (CASE WHEN ComandasActivas.cProducto = 0 THEN Cuentas.Cantidad * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE Cuentas.Cantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0)  * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) END) AS IVentas, (CASE WHEN ComandasActivas.cProducto = 0 THEN Cuentas.Cantidad * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1, 2, 0) ELSE Cuentas.Cantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0)  * Configuraciones.Imp_Servicio / 100) * REPLACE(ComandasActivas.Express + 1, 2, 0) END) AS IServicios, Mesas.Nombre_Mesa AS Expr1 FROM Menu_Restaurante RIGHT OUTER JOIN " &
                                "ComandasActivas WITH (NOLOCK) INNER JOIN  Mesas ON ComandasActivas.cMesa = Mesas.Id INNER JOIN  Cuentas ON ComandasActivas.id = Cuentas.idcTemporal AND ComandasActivas.cMesa = Cuentas.idMesa ON  Menu_Restaurante.Id_Menu = ComandasActivas.cProducto CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cPrecio>0 and Estado='Activo' and cMesa =" & idmesa
                    Else
                        str = "SELECT ComandasActivas.Tipo_Plato, ComandasActivas.cCantidad, ComandasActivas.cProducto, ComandasActivas.cDescripcion, ComandasActivas.cPrecio, ComandasActivas.cCodigo, ComandasActivas.primero, ComandasActivas.Cedula, ComandasActivas.hora, ComandasActivas.Express, (CASE WHEN ComandasActivas.cProducto = 0 THEN ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100) ELSE ComandasActivas.cCantidad * ComandasActivas.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * ISNULL( Hotel.dbo.Cabys.porcentajeIVA,13) / 100)END) AS IVentas, (CASE WHEN ComandasActivas.cProducto = 0 THEN ComandasActivas.cCantidad * ComandasActivas.cPrecio * (Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) ELSE (ComandasActivas.cCantidad * ComandasActivas.cPrecio) * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0))END) AS IServicios " &
                            "FROM ComandasActivas WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandasActivas.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id WHERE cPrecio>0 and Estado='Activo' and cMesa=" & idmesa & " and separada= " & numeroSeparado & " ORDER BY primero DESC,express ASC"
                        'str = "SELECT ComandaTemporal.Tipo_Plato,ComandaTemporal.cCantidad, ComandaTemporal.cProducto, ComandaTemporal.cDescripcion, ComandaTemporal.cPrecio, ComandaTemporal.cCodigo,  ComandaTemporal.primero, ComandaTemporal.Cedula, ComandaTemporal.hora, ComandaTemporal.Express,  ComandaTemporal.cCantidad * ComandaTemporal.cPrecio * (ISNULL(Menu_Restaurante.ImpVenta, 0) * Configuraciones.Imp_Venta / 100) AS IVentas, ComandaTemporal.cCantidad *  ComandaTemporal.cPrecio * (ISNULL(Menu_Restaurante.ImpServ, 0) * Configuraciones.Imp_Servicio / 100)  * (replace(express+ 1,2,0)) AS IServicios FROM ComandaTemporal WITH (NOLOCK) LEFT OUTER JOIN  Menu_Restaurante ON ComandaTemporal.cProducto = Menu_Restaurante.Id_Menu CROSS JOIN  Configuraciones WHERE cCodigo=" & numeroComanda & " and separada= " & numeroSeparado & " ORDER BY primero DESC,express ASC"
                    End If
                End If
                cConexion.GetRecorset(conectadobd, str, rs)
                TxtMesa.Text = nombremesa '& "(" & idmesa & ")"
                TxtComanda.Text = numeroComanda
                TxtComensales.Text = Comenzales

                DataGridView1.Rows.Clear()
                Dim i As Integer = 1
                While rs.Read
                    With DataGridView1.Rows
                        i = 0
                        If Trim(rs("Tipo_Plato")) <> "Modificador" Or rs("cPrecio") > 0 Then
                            If rs("cPrecio") > 0 Then
                                .Add(Math.Abs(rs("cCantidad")), rs("cDescripcion"), Format(Decimal.ToSingle(rs("cPrecio")), "#####0.00"), Format(Decimal.ToSingle(rs("cPrecio") * Math.Abs(rs("cCantidad"))), "#####0.00"))
                            Else
                                .Add(Math.Abs(rs("cCantidad")), rs("cDescripcion"), Format(Decimal.ToSingle(rs("cPrecio")), "#####0.00"), Format(Decimal.ToSingle(rs("cPrecio")), "#####0.00"))
                            End If

                        Else
                            .Add(Math.Abs(rs("cCantidad")), rs("cDescripcion"), Format(Decimal.ToSingle(rs("cPrecio")), "#####0.00"), Format(Decimal.ToSingle(rs("cPrecio")), "#####0.00"))
                        End If

                    End With
                    'En el Global se acumula dependiendo de los valores que tienen los platos del menu
                    If rs("cCantidad") > 0 Then
                        Global_subtotal = Global_subtotal + (Math.Abs(rs("cCantidad")) * rs("cPrecio"))

                        '----
                        If ckbEspecifico.Checked = True And PorcExonerado < 13  Then
                            Dim montoImp As Double
                            Dim PorcentajeImpuesto = cargarImpuestoIVA(IdCliente, rs("cProducto"))
                            montoImp = (rs("cPrecio") * Math.Abs(rs("cCantidad"))) * (PorcentajeImpuesto / 100)
                            iv += montoImp
                        Else
                            iv += rs("IVentas")
                        End If
                        '----


                        iss += rs("IServicios")

                    Else
                        Global_subtotal = Global_subtotal + (rs("cPrecio"))

                        iv += (rs("cPrecio")) * (glodtsGlobal.Configuraciones(0).Imp_Venta / 100)
						iss += (rs("cPrecio")) * (glodtsGlobal.Configuraciones(0).Imp_Servicio / 100)

					End If


                End While
                rs.Close()

                guardasubtotal = Global_subtotal
                MuestraTotales()

            ElseIf anular = True Then 'si se busco la factura para anularla
                Dim tipoF As String

                tipoF = cConexion.SlqExecuteScalar(conectadobd, "select anulado from ventas where id =" & idfactura)
                If UCase(tipoF) = "FALSE" Then
                    ToolBarEliminar.Enabled = True
                Else
                    ToolBarEliminar.Enabled = False
                End If

                CheckBoxIS.Enabled = False
                CheckBoxISH.Enabled = False
                CheckExonerar.Enabled = False
                chkComisionista.Enabled = False
                ckbdescuento.Enabled = False
                cbkCortesia.Enabled = False
                NumericUpDownExtraPropina.Enabled = False

                If vCortesia <> True Then
                    If tipo_factura = True Then
                        str = "SELECT VistaPrueba.*, Id AS Expr1 FROM VistaPrueba WHERE IDMESA = " & idmesa & " AND Id = " & idfactura & " ORDER BY IdDetalle"
                    Else
                        str = "SELECT VistaPrueba2.*, Id AS Expr1 FROM VistaPrueba2 WHERE IDMESA = " & idmesa & " AND Id = " & idfactura & " ORDER BY IdDetalle"

                    End If
                Else
                    If tipo_factura = True Then
                        str = "SELECT VistaPrueba.*, Id AS Expr1 FROM VistaPrueba WHERE IDMESA = " & idmesa & " AND id_cortesia = " & idfactura & " ORDER BY IdDetalle"
                    Else
                        str = "SELECT VistaPrueba2.*, Id AS Expr1 FROM VistaPrueba2 WHERE IDMESA = " & idmesa & " AND id_cortesia = " & idfactura & " ORDER BY IdDetalle"
                    End If
                End If

                'str = "Select VistaPrueba.*, Hotel.dbo.Ventas.Id from VistaPrueba inner join Hotel.dbo.Ventas on VistaPrueba.Num_Factura = Hotel.dbo.Ventas.Num_Factura where VistaPrueba.NumeroComanda= " & numeroComanda & " and Hotel.dbo.Ventas.Id=" & idfactura & " Order By VistaPrueba.IdDetalle"
                cConexion.GetRecorset(conectadobd, str, rs)
                Dim i As Integer = 1
                Dim rr As Integer = 0 '***************** PSV *************'
                Dim campos(500) As String '***************** PSV *************'
                Dim nombre_reserva As String = ""
                While rs.Read
                    With DataGridView1.Rows
                        If tipo_factura = True Then
                            nombremesa1 = rs("Nombre_Mesa")
                        End If

                        If i = 1 Then
                            If tipo_factura = True Then
                                ID_MESA = rs("IDMESA")
                            End If

                            .Add("**************", "************************************************************", "**********************", "**********************")
                            .Add("N. Factura", rs("Num_Factura"), "", "")
                            .Add("Moneda: ", rs("Moneda_Nombre"), "", "")
                            .Add("Fecha: ", CDate(rs("Fecha")), "", "")
                            If tipo_factura = True Then
                                .Add("Mesa: ", rs("Nombre_Mesa"), "", "")
                            End If

                            .Add("Cliente: ", rs("Nombre_Cliente"), "", "")
                            .Add("**************", "************************************************************", "**********************", "**********************")
                            .Add("Cant.", "Descripción", "P/Unitario", "Subtotal")
                            i = 0
                            'BUSCA LA MONEDA EN QUE SE REALIZO LA FACTURA
                            DataV.RowFilter = "MonedaNombre ='" & rs("Moneda_Nombre") & "'"
                            Dim fil As DataRowView
                            For Each fil In DataV
                                codMoneda = fil("CodMoneda")
                                NombreMoneda = fil("MonedaNombre")
                                valor = fil("ValorCompra")
                            Next
                            DataV = New DataView(DataS.Tables("Moneda"))
                            cboxMoneda.SelectedItem = rs("Moneda_Nombre")
                        End If
                        ' ***** Diego ****
                        Dim Detalle As Integer
                        If rs("IdDetalle") <> Detalle Then
                            .Add(rs("Cantidad"), rs("Nombre_Menu"), Format(Decimal.ToSingle(rs("PrecioUnitario")), "#####0.00"), Format(Decimal.ToSingle(rs("PrecioUnitario") * rs("Cantidad")), "#####0.00"))
                            '***************** PSV *************'
                            campos(rr) = ("0," & rs("Cantidad") & ",'" & rs("Nombre_Menu") & "', " & Format(Decimal.ToSingle(rs("PrecioUnitario") * rs("Cantidad")), "#####0.00") & "," & numeroComanda & "," & ID_MESA & ",0,'EPSON TM-U300A Partial cut',0,''")
                            '******************************'

                            If IsDBNull(rs("Nombre")) = False Then
                                .Add(0, rs("Nombre"), 0, 0)
                            End If
                            nombre_reserva = rs("Nombre_Menu")
                            Detalle = rs("IdDetalle")
                        Else
                            If IsDBNull(rs("Nombre")) = False Then
                                .Add(0, rs("Nombre"), 0, 0)
                            End If
                        End If

                    End With
                    rr = rr + 1  '***************** PSV *************'
                End While
                rs.Close()

                '***************** PSV *************'
                ACTIVA = cConexion.SlqExecuteScalar(conectadobd, "Select activa From Mesas where Id=" & ID_MESA)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "CargarDatos_Comanda_Factura", ex.Message)
        End Try
    End Sub

    Private Sub MuestraTotales()
        Dim Desc As Decimal
        Dim Subto As Decimal
        Dim Des As Decimal
        Dim Imser As Decimal
        Dim Imven As Decimal
        total = 0

		Try
			'If PorcExonerado > 0 And PorcExonerado - 100 Then
			'	spCalculoPorcentajeExoneracion()
			'End If
			Desc = Math.Round((txtdescuento.Value / 100), 3)
			'Calculo del descuento general.
			Descuento = Math.Round((Global_subtotal * Desc), 3)
			'Si se cobra una comisición por servicio a la habitación.
			iss = IIf(CheckBoxISH.Checked, (Global_subtotal * (User_Log.ISH / 100) + iss), iss)
			iss = IIf(CheckBoxIS.Checked = False, 0, iss)

			If codMoneda = CodMonedaC Then
				Des = Descuento
				Global_subtotal = Math.Round(Global_subtotal, 3)
				Subto = Math.Round(Global_subtotal, 3)
				Imser = Math.Round(iss - (iss * Desc), 3)
				Imven = Math.Round(iv - (iv * Desc), 3)

			ElseIf codMoneda = 1 Then
				Des = Math.Round((Descuento * ValorMonedaC), 3)
				Global_subtotal = Math.Round(Global_subtotal, 3)
				Subto = Math.Round(((Global_subtotal) * ValorMonedaC), 3)
				Imser = Math.Round(((iss - (iss * Desc)) * ValorMonedaC), 3)
				Imven = Math.Round(((iv - (iv * Desc)) * ValorMonedaC), 3)

			Else
				Des = Math.Round((Descuento / valor), 3)
				Global_subtotal = Math.Round(Global_subtotal, 3)
				Subto = Math.Round(((Global_subtotal) / valor), 3)
				Imser = Math.Round(((iss - (iss * Desc)) / valor), 3)
				Imven = Math.Round(((iv - (iv * Desc)) / valor), 3)
			End If

			If CheckExonerar.Checked = False And PorcExonerado = 0 Then
				Imven = 0
			End If

			total = Math.Round((Subto - Des + Imser + Imven + NumericUpDownExtraPropina.Value), 3)
			If Des > -1 Then TextBoxDescuento.Text = Format(Des, "#,#0.000")

            If Subto > -1 Then txtSubtotal.Text = Format(Subto, "###,##0.000")
			If Imser > -1 Then txtIS.Text = Format(Imser, "###,##0.000")
			If Imven > -1 Then txtIV.Text = Format(Imven, "###,##0.000")
			If total > -1 Then txtTotal.Text = Format(total, "###,##0.000")

		Catch ex As Exception
			MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "MuestraTotales", ex.Message)
        End Try
    End Sub
    'Private Function spCalculoPorcentajeExoneracion() As Double
    '       Dim valor As Integer = 0
    '       If PorcExonerado <> 0 Then
    '           valor = 13 - PorcExonerado
    '       Else
    '           valor = 0
    '       End If

    '       Return valor

    'End Function

    Private Function cargarImpuestoIVA(ByVal CodigoCliente As Integer, ByVal CodigoArticulo As Integer) As Double
        Return ImpuestoProducto.Obtener(CodigoCliente, CodigoArticulo)
    End Function


    Private Sub calculo_descuento()
        'Dim Desc As Decimal
        'Dim descuentoCliente As Double = 0
        'Dim ISH_IS As Double = IIf(CheckBoxISH.Checked, User_Log.ISH, 0) + IIf(CheckBoxIS.Checked, User_Log.ISS, 0)

        'Desc = (txtdescuento.Value / 100)
        'TextBoxDescuento.Text = Format((subtotal * Desc), "#,#0.00")

        'Dim subtotal1 As Double

        'subtotal1 = subtotal - (subtotal * Desc)
        'iv = subtotal1 * (User_Log.IVI / 100)
        'iss = subtotal1 * (ISH_IS / 100)
        'txtSubtotal.Text = Format(subtotal / valor, "###,##0.00")
        'txtIS.Text = Format(iss / valor, "###,##0.00")
        'txtIV.Text = Format(iv / valor, "###,##0.00")
        'txtTotal.Text = Format(total / valor, "###,##0.00")
        'NumericUpDownExtraPropina.Value = Format(ExtraPropina / valor, "###,##0.00")
        'total = subtotal1 + iss + iv + NumericUpDownExtraPropina.Value
        'txtTotal.Text = Format(total, "###,##0.00")
    End Sub
#End Region

#Region "Modificar Precios"
    Private Sub modificarPrecios(ByVal p As String)
        Try
            DataGridView1.Rows.Clear()
            Dim str As String

            If separada = False Then
                If int = 1 Then 'psv
                    str = "SELECT ComandaTemporal1.*, 'BUFFET' as Nombre_Mesa FROM ComandaTemporal1  WHERE Estado='Activo' AND cMesa = " & idmesa & " "

                Else 'psv
                    str = "SELECT ComandasActivas.*, Mesas.Nombre_Mesa FROM ComandasActivas inner join Mesas on ComandasActivas.cMesa = Mesas.Id WHERE Estado='Activo' and cMesa = " & idmesa & " "
                End If 'psv

            Else
				'str = "SELECT ComandasActivas.Tipo_Plato, ComandasActivas.cCodigo, Mesas.Nombre_Mesa, ComandasActivas.Comenzales, Cuentas.Cantidad as cCantidad, ComandasActivas.cPrecio, ComandasActivas.cDescripcion FROM ComandasActivas INNER JOIN Cuentas ON ComandasActivas.id = Cuentas.idcTemporal inner join Mesas on ComandasActivas.cMesa = Mesas.Id where Cuentas.IdMesa= ComandasActivas.cMesa and ComandasActivas.Estado='Activo' and ComandasActivas.cMesa= " & idmesa & " and ComandasActivas.separada=" & numeroSeparado
				str = "SELECT ComandasActivas.id, ComandasActivas.cProducto, ComandasActivas.Tipo_Plato, ComandasActivas.cCodigo, Mesas.Nombre_Mesa, ComandasActivas.Comenzales, Cuentas.Cantidad as cCantidad, ComandasActivas.cPrecio, ComandasActivas.cDescripcion FROM ComandasActivas INNER JOIN Cuentas ON ComandasActivas.id = Cuentas.idcTemporal inner join Mesas on ComandasActivas.cMesa = Mesas.Id where Cuentas.IdMesa= ComandasActivas.cMesa and ComandasActivas.Estado='Activo' and ComandasActivas.cMesa= " & idmesa & " and ComandasActivas.separada=" & numeroSeparado
			End If

            'SE BUSCAN LOS CODIGOS DE LOS ARTICULOS DE LA COMANDA
            cConexion.GetRecorset(cConexion.Conectar("Restaurante"), str, rs)

            Dim ComandoUpdate As New ConexionR
            While rs.Read
                If int = 0 Then
                    ComandoUpdate.UpdateRecords(ComandoUpdate.Conectar("Restaurante"), "ComandaTemporal", "CPRECIO = " & SelecionarPrecio(rs!cproducto, p, rs!cPrecio), "ID =" & rs!ID)
                Else
                    ComandoUpdate.UpdateRecords(ComandoUpdate.Conectar("Restaurante"), "ComandaTemporal1", "CPRECIO = " & SelecionarPrecio(rs!cproducto, p, rs!cPrecio), "ID =" & rs!ID)
                End If
            End While

            rs.Close()
            ComandoUpdate.DesConectar(ComandoUpdate.SqlConexion)
            Me.CargarDatos_Comanda_Factura()

        Catch ex As Exception
			MsgBox(ex.Message)
			rs.Close()
		End Try
    End Sub

    Private Function SelecionarPrecio(ByVal Codigo As String, ByVal TipoPrecio As String, ByVal Precio As Double) As Double
        Dim Conexiones As New ConexionR
        Dim precioVenta As Double

        Dim dt As New DataTable
        Dim sql As String = "Select Precio_VentaA,Precio_VentaB,Precio_VentaC,Precio_VentaD, moneda From Menu_Restaurante where Id_Menu=" & Codigo
        TipoPrecio = TipoPrecio.ToUpper
        Try
            Select Case TipoPrecio
                Case "TIPO A"
                    precioVenta = Precio
                    'If dt.Rows.Count > 0 Then
                    '    If dt.Rows(0).Item("moneda").ToString() = codMoneda.ToString() Then
                    '        precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaA").ToString())
                    '    ElseIf dt.Rows(0).Item("moneda").ToString() = "2" And codMoneda = 1 Then
                    '        precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaA").ToString()) * TipoCambioDolar
                    '    ElseIf dt.Rows(0).Item("moneda").ToString() = "1" And codMoneda = 2 Then
                    '        precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaA").ToString()) / TipoCambioDolar
                    '    End If
                    'Else
                    '    precioVenta = 0
                    'End If
                    ' precioVenta = Conexiones.SlqExecuteScalar(Conexiones.Conectar("Restaurante"), "Select Precio_VentaA as Precio, moneda From Menu_Restaurante where Id_Menu=" & Codigo)

                Case "TIPO B"
                    cFunciones.Llenar_Tabla_Generico(sql, dt)
                    If dt.Rows.Count > 0 Then
                        If dt.Rows(0).Item("moneda").ToString() = codMoneda.ToString() Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaB").ToString())
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "2" And codMoneda = 1 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaB").ToString()) * TipoCambioDolar
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "1" And codMoneda = 2 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaB").ToString()) / TipoCambioDolar
                        End If
                    Else
                        precioVenta = 0
                    End If
                    ' precioVenta = Conexiones.SlqExecuteScalar(Conexiones.Conectar("Restaurante"), "Select Precio_VentaB as Precio, moneda From Menu_Restaurante where Id_Menu=" & Codigo)

                Case "TIPO C"
                    cFunciones.Llenar_Tabla_Generico(sql, dt)
                    If dt.Rows.Count > 0 Then
                        If dt.Rows(0).Item("moneda").ToString() = codMoneda.ToString() Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaC").ToString())
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "2" And codMoneda = 1 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaC").ToString()) * TipoCambioDolar
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "1" And codMoneda = 2 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaC").ToString()) / TipoCambioDolar
                        End If
                    Else
                        precioVenta = 0
                    End If
                    '  precioVenta = Conexiones.SlqExecuteScalar(Conexiones.Conectar("Restaurante"), "Select Precio_VentaC as Precio, moneda From Menu_Restaurante where Id_Menu=" & Codigo)

                Case "TIPO D"
                    cFunciones.Llenar_Tabla_Generico(sql, dt)
                    If dt.Rows.Count > 0 Then
                        If dt.Rows(0).Item("moneda").ToString() = codMoneda.ToString() Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaD").ToString())
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "2" And codMoneda = 1 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaD").ToString()) * TipoCambioDolar
                        ElseIf dt.Rows(0).Item("moneda").ToString() = "1" And codMoneda = 2 Then
                            precioVenta = Convert.ToDecimal(dt.Rows(0).Item("Precio_VentaD").ToString()) / TipoCambioDolar
                        End If
                    Else
                        precioVenta = 0
                    End If
                    ' precioVenta = Conexiones.SlqExecuteScalar(Conexiones.Conectar("Restaurante"), "Select Precio_VentaD as Precio, moneda From Menu_Restaurante where Id_Menu=" & Codigo)

            End Select
            Conexiones.DesConectar(Conexiones.SqlConexion)
            Conexiones = Nothing

            Return IIf(precioVenta = 0, Precio, precioVenta)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Asientos Contables"
    Public Sub GuardaAsiento(ByVal IdFactura As Integer)
        Dim Fx As New cFunciones

        '------------------------------------------------------------------
        'CREA EL ASIENTO CONTABLE
        DatasetCliente1.DetallesAsientosContable.Clear()
        DatasetCliente1.AsientosContables.Clear()
        BindingContext(DatasetCliente1, "AsientosContables").EndCurrentEdit()
        BindingContext(DatasetCliente1, "AsientosContables").AddNew()
        BindingContext(DatasetCliente1, "AsientosContables").Current("NumAsiento") = Fx.BuscaNumeroAsiento("ING-" & Format(Now.Month, "00") & Format(Now.Date, "yy") & "-")
        BindingContext(DatasetCliente1, "AsientosContables").Current("Fecha") = Now.Date
        BindingContext(DatasetCliente1, "AsientosContables").Current("IdNumDoc") = IdFactura
        BindingContext(DatasetCliente1, "AsientosContables").Current("NumDoc") = NumeroFactura
        BindingContext(DatasetCliente1, "AsientosContables").Current("Beneficiario") = txtcliente.Text
        BindingContext(DatasetCliente1, "AsientosContables").Current("TipoDoc") = 15
        BindingContext(DatasetCliente1, "AsientosContables").Current("Accion") = "AUT"
        BindingContext(DatasetCliente1, "AsientosContables").Current("Anulado") = 0
        BindingContext(DatasetCliente1, "AsientosContables").Current("FechaEntrada") = Now.Date
        BindingContext(DatasetCliente1, "AsientosContables").Current("Mayorizado") = 0
        BindingContext(DatasetCliente1, "AsientosContables").Current("Periodo") = Fx.BuscaPeriodo(Now.Date)
        BindingContext(DatasetCliente1, "AsientosContables").Current("NumMayorizado") = 0
        BindingContext(DatasetCliente1, "AsientosContables").Current("Modulo") = "Facturacion Restaurante"
        BindingContext(DatasetCliente1, "AsientosContables").Current("Observaciones") = "Factura de Restaurante # " & NumeroFactura
        BindingContext(DatasetCliente1, "AsientosContables").Current("NombreUsuario") = lbUsuario.Text
        BindingContext(DatasetCliente1, "AsientosContables").Current("TotalDebe") = CDbl(txtTotal.Text)
        BindingContext(DatasetCliente1, "AsientosContables").Current("TotalHaber") = CDbl(txtTotal.Text)
        BindingContext(DatasetCliente1, "AsientosContables").Current("CodMoneda") = codMoneda
        BindingContext(DatasetCliente1, "AsientosContables").Current("TipoCambio") = valor
        BindingContext(DatasetCliente1, "AsientosContables").EndCurrentEdit()

        '------------------------------------------------------------------
        'CREA TODOS LOS DETALLES DEL ASIENTO
        AsientoDetalle(IdFactura)
        '------------------------------------------------------------------
    End Sub


    Public Sub GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String)
        If Monto <> 0 Then       'CREA LOS DETALLES DE ASIENTOS CONTABLES
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").AddNew()
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NumAsiento") = BindingContext(DatasetCliente1, "AsientosContables").Current("NumAsiento")
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("DescripcionAsiento") = BindingContext(DatasetCliente1, "AsientosContables").Current("Observaciones")
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Cuenta") = Cuenta
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Monto") = Monto
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Debe") = Debe
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Haber") = Haber
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
        End If
    End Sub


    Private Sub AsientoDetalle(ByVal IdFactura As Integer)
        Try
            If rbCredito.Checked Then
                '------------------------------------------------------------------
                'GUARDA ASIENTO DETALLE PARA EL TOTAL DE CREDITO
                GuardaAsientoDetalle(CDbl(txtTotal.Text), True, False, buscacuenta("CuentaContable", "IdCuentaCobrar"), buscacuenta("Descripcion", "IdCuentaCobrar"))
                '------------------------------------------------------------------
            ElseIf rbcarga.Checked Then
                '------------------------------------------------------------------
                'GUARDA ASIENTO DETALLE PARA EL TOTAL CARGO A HABITACION
                GuardaAsientoDetalle(CDbl(txtTotal.Text), True, False, buscacuenta("CuentaContable", "IdCxCHabitacion"), buscacuenta("Descripcion", "IdCxCHabitacion"))
                '------------------------------------------------------------------
            End If

            '------------------------------------------------------------------
            'GUARDA ASIENTO DETALLE PARA LA CUENTA DE INGRESO
            DetallesGruposMenu(IdFactura)
            '------------------------------------------------------------------

            '------------------------------------------------------------------
            'GUARDA ASIENTO DETALLE PARA EL IMPUESTO DE SERVICIO
            GuardaAsientoDetalle(CDbl(txtIS.Text), False, True, buscacuenta("CuentaContable", "IdServicio"), buscacuenta("Descripcion", "IdServicio"))
            '------------------------------------------------------------------

            '------------------------------------------------------------------
            'GUARDA ASIENTO DETALLE PARA EL IMPUESTO DE VENTA
            GuardaAsientoDetalle(CDbl(txtIV.Text), False, True, buscacuenta("CuentaContable", "IdImpuestoVenta"), buscacuenta("Descripcion", "IdImpuestoVenta"))
            '------------------------------------------------------------------

            '------------------------------------------------------------------
            'GUARDA ASIENTO DETALLE PARA LA PROPINA EXTRA
            GuardaAsientoDetalle(CDbl(NumericUpDownExtraPropina.Value), False, True, buscacuenta("CuentaContable", "IdPropina"), buscacuenta("Descripcion", "IdPropina"))
            '------------------------------------------------------------------

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub


    Private Sub DetallesGruposMenu(ByVal IdFactura As Integer)
        Dim DataSet As New DataSet
        Dim Fila As DataRow
        Dim Cuentas(1) As Integer
        Dim Encontrado As Boolean = False
        Dim C As Integer = 0

        Try
            If IdFactura <> 0 Then
                '----------------------------------------------------------------------------
                'ESTABLECE LA CONEXION A VENTAS DETALLE
                cConexion.DesConectar(conectadobd)
                conectadobd = cConexion.Conectar("Hotel")
                cConexion.GetDataSet(conectadobd, "SELECT *, Restaurante.dbo.Categorias_Menu.IdGrupo AS IdGrupo FROM Ventas_Detalle INNER JOIN Restaurante.dbo.Menu_Restaurante ON Ventas_Detalle.Codigo = Restaurante.dbo.Menu_Restaurante.Id_Menu INNER JOIN Restaurante.dbo.Categorias_Menu ON Restaurante.dbo.Menu_Restaurante.Id_Categoria = Restaurante.dbo.Categorias_Menu.Id WHERE Ventas_Detalle.Id_Factura = " & IdFactura, DataSet, "Ventas_Detalle")
                '----------------------------------------------------------------------------

                ReDim Cuentas(DataSet.Tables("Ventas_Detalle").Rows.Count)
                For Each Fila In DataSet.Tables("Ventas_Detalle").Rows
                    '----------------------------------------------------------------------------
                    'RECORRE TODOS LOS DETALLES DE VENTAS
                    For b As Integer = 0 To DataSet.Tables("Ventas_Detalle").Rows.Count - 1
                        If Cuentas(b) = Fila("IdGrupo") Then
                            Encontrado = True
                        End If
                    Next
                    '----------------------------------------------------------------------------

                    '----------------------------------------------------------------------------
                    If Encontrado = False Then  'SI TODAVIA NO SE REALIZADO EL ASIENTO PARA LA CATEGORIA
                        Cuentas(C) = Fila("IdGrupo")
                        C += 1
                        BuscaMontoCuenta(Fila("IdGrupo"), IdFactura) 'GUARDA EL DETALLE DEL ASIENTO PARA LA CATEGORIA
                        '----------------------------------------------------------------------------
                    Else    'SI YA HA SIDO TOMADO EN CUENTA
                        Encontrado = False
                    End If
                    '----------------------------------------------------------------------------
                Next
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub


    Private Sub BuscaMontoCuenta(ByVal Id As Integer, ByVal IdFactura As Integer)
        Dim DataSet As New DataSet
        Dim Fila As DataRow
        Dim Monto As Double

        Try
            '----------------------------------------------------------------------------
            'ESTABLECE LA CONEXION A VENTAS DETALLE
            cConexion.DesConectar(conectadobd)
            conectadobd = cConexion.Conectar("Hotel")
            cConexion.GetDataSet(conectadobd, "SELECT *, Restaurante.dbo.Categorias_Menu.IdGrupo AS IdGrupo FROM Ventas_Detalle INNER JOIN Restaurante.dbo.Menu_Restaurante ON Ventas_Detalle.Codigo = Restaurante.dbo.Menu_Restaurante.Id_Menu INNER JOIN Restaurante.dbo.Categorias_Menu ON Restaurante.dbo.Menu_Restaurante.Id_Categoria = Restaurante.dbo.Categorias_Menu.Id WHERE Ventas_Detalle.Id_Factura = " & IdFactura & " AND IdGrupo = " & Id, DataSet, "Ventas_Detalle")
            '----------------------------------------------------------------------------

            If DataSet.Tables("Ventas_Detalle").Rows.Count > 0 Then
                '----------------------------------------------------------------------------
                For Each Fila In DataSet.Tables("Ventas_Detalle").Rows
                    Monto += (Fila("SubTotal") - Fila("Monto_Descuento"))
                Next

                '----------------------------------------------------------------------------
                'GUARDA EL DETALLE PARA LA CUENTA CONTABLE DEL SERVICIO
                GuardaAsientoDetalle(Math.Abs(Monto), False, True, BuscaCuentaGrupoMenu("CuentaIngreso", Id), BuscaCuentaGrupoMenu("DescripcionCuentaIngreso", Id))
                '----------------------------------------------------------------------------
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub


    Function BuscaCuenta(ByVal Tipo As String, ByVal Id As String) As String
        Dim cConexion As New Conexion
        Try
            BuscaCuenta = cConexion.SlqExecuteScalar(cConexion.Conectar("Contabilidad"), "SELECT TOP 1 (SELECT " & Tipo & " FROM cuentacontable " & _
                            "WHERE (Id = (SELECT " & Id & " FROM settingcuentacontable))) AS Cuenta FROM CuentaContable")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function


    Function BuscaCuentaGrupoMenu(ByVal Tipo As String, ByVal Id As Integer) As String
        Dim cConexion As New Conexion
        Try
            BuscaCuentaGrupoMenu = cConexion.SlqExecuteScalar(cConexion.Conectar("Restaurante"), "SELECT " & Tipo & " FROM Grupos_Menu WHERE Id = " & Id)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function


    Function TransAsiento() As Boolean  'REALIZA LA TRANSACCIÓN DE LOS ASIENTOS CONTABLES
        Dim Trans As SqlClient.SqlTransaction

        Try
            If SqlConnection2.State <> ConnectionState.Open Then SqlConnection2.Open()

            Trans = SqlConnection2.BeginTransaction
            BindingContext(DatasetCliente1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(DatasetCliente1, "AsientosContables").EndCurrentEdit()

            AdapterDetallesAsientos.UpdateCommand.Transaction = Trans
            AdapterDetallesAsientos.DeleteCommand.Transaction = Trans
            AdapterDetallesAsientos.InsertCommand.Transaction = Trans

            AdapterAsientos.UpdateCommand.Transaction = Trans
            AdapterAsientos.DeleteCommand.Transaction = Trans
            AdapterAsientos.InsertCommand.Transaction = Trans

            '-----------------------------------------------------------------------------------
            'Inicia Transacción....
            AdapterDetallesAsientos.Update(DatasetCliente1.DetallesAsientosContable)
            AdapterAsientos.Update(DatasetCliente1.AsientosContables)
            '-----------------------------------------------------------------------------------
            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information)
            Return False
        End Try
    End Function
#End Region

    Private Sub ButtonCambioFV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCambioFV.Click
        If cedula = "" Then cedula = User_Log.Cedula
        PMU = VSM(cedula, Name)
        If PMU.Others = False Then
            MsgBox("NO TIENE PERMISOS, COMUNIQUESE CON EL ADMINISTRADOR", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If MsgBox("¿Al continuar BORRARA POR COMPLETO las opciones de pago anteriores " & Chr(13) & " y deberá volver a incluirlas, desea continuar?", MsgBoxStyle.YesNo, "ATENCION!!!!...") = MsgBoxResult.Yes Then
            OpciondePago(txtTotal.Text)
        End If


    End Sub
    '**********************************************************************************************************
    '                                        OPCIONES DE PAGO
    '**********************************************************************************************************
    Function OpciondePago(ByVal Total As Double) As Boolean
        Try
            Dim Movimiento_Pago_Abonos As New frmMovimientoCajaPagoAbono

            Dim dtDatosFac As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT Cedula_Usuario, Total, Fecha, Num_Factura, Proveniencia_Venta, Num_Apertura FROM Ventas WHERE  (Id = " & Me.idfactura & ")", dtDatosFac)


            Movimiento_Pago_Abonos.Factura = dtDatosFac.Rows(0).Item("Num_Factura")
            Movimiento_Pago_Abonos.fecha = Now.Date
			Movimiento_Pago_Abonos.Total = Math.Round(Total, 2)
			Movimiento_Pago_Abonos.Tipo = "FVC" 'columna!tipo
            Movimiento_Pago_Abonos.codmod = codMoneda


            Movimiento_Pago_Abonos.cedu = dtDatosFac.Rows(0).Item("Cedula_usuario")
            Movimiento_Pago_Abonos.cambioFP = dtDatosFac.Rows(0).Item("Num_Apertura")
            Dim dtusua As New DataTable
            cFunciones.Llenar_Tabla_Generico("Select Nombre From Usuarios Where Cedula = '" & dtDatosFac.Rows(0).Item("Cedula_usuario") & "'", dtusua)
            Movimiento_Pago_Abonos.nombre = dtusua.Rows(0).Item(0)
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT IdPuntoVenta, Nombre, Tipo, BaseDatos FROM PuntoVenta WHERE     (IdPuntoVenta = " & dtDatosFac.Rows(0).Item("Proveniencia_Venta") & ")", dt, GetSetting("SeeSoft", "Hotel", "Conexion"))
            Dim cx As New Conexion
            cx.Conectar(dt.Rows(0).Item("BaseDatos"))
            cx.SlqExecute(cx.sQlconexion, "DELETE OpcionesDePago WHERE Documento = " & dtDatosFac.Rows(0).Item("Num_Factura") & " AND Numapertura = " & dtDatosFac.Rows(0).Item("Num_Apertura"))

            cx.DesConectar(cx.sQlconexion)
            Movimiento_Pago_Abonos.conexion1 = GetSetting("SeeSoft", dt.Rows(0).Item("BaseDatos"), "CONEXION")

            If Total <= 0 Then
                Return True
            Else
                Movimiento_Pago_Abonos.ShowDialog()
                If Movimiento_Pago_Abonos.Registra = True Then
                    MsgBox("Cambio hecho satisfactoriamente")

                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "OpciondePago", ex.Message)
        End Try
    End Function
End Class
