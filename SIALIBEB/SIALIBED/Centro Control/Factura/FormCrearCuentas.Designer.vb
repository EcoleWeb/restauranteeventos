<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCrearCuentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBoxCuentas = New System.Windows.Forms.GroupBox()
        Me.ListViewCuentas = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DataGridViewComandas = New System.Windows.Forms.DataGridView()
        Me.ComandaTemporalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetComandas = New SIALIBEB.DataSetComandas()
        Me.GroupBoxTotales = New System.Windows.Forms.GroupBox()
        Me.TextBoxImpServ = New System.Windows.Forms.TextBox()
        Me.TextBoxTotal = New System.Windows.Forms.TextBox()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.LabelSalonero = New System.Windows.Forms.Label()
        Me.TextBoxImpVenta = New System.Windows.Forms.TextBox()
        Me.LabelImpVenta = New System.Windows.Forms.Label()
        Me.TextBoxSubTotal = New System.Windows.Forms.TextBox()
        Me.LabelTotales = New System.Windows.Forms.Label()
        Me.ComboBoxCuentas = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ButtonEliminar = New System.Windows.Forms.Button()
        Me.ButtonAgregar = New System.Windows.Forms.Button()
        Me.CachedApertura_Cajas1 = New SIALIBEB.CachedApertura_Cajas()
        Me.ComboBoxCantPasar = New System.Windows.Forms.ComboBox()
        Me.LabelCantidad = New System.Windows.Forms.Label()
        Me.LabelCuentas = New System.Windows.Forms.Label()
        Me.DataSetComandasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.RAPIDOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RAPIDOToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComandaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Check1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CantCobrar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCantidadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CDescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPrecioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SeparadaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CProductoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HabitacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBoxCuentas.SuspendLayout()
        CType(Me.DataGridViewComandas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComandaTemporalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetComandas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxTotales.SuspendLayout()
        CType(Me.DataSetComandasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.ComandaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBoxCuentas
        '
        Me.GroupBoxCuentas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxCuentas.Controls.Add(Me.ListViewCuentas)
        Me.GroupBoxCuentas.Location = New System.Drawing.Point(0, 3)
        Me.GroupBoxCuentas.Name = "GroupBoxCuentas"
        Me.GroupBoxCuentas.Size = New System.Drawing.Size(392, 486)
        Me.GroupBoxCuentas.TabIndex = 0
        Me.GroupBoxCuentas.TabStop = False
        Me.GroupBoxCuentas.Text = "Lista de Cuentas Separadas"
        Me.ToolTip1.SetToolTip(Me.GroupBoxCuentas, "Los productos sin cuenta aparecen en la ""Cero""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "De clic en una cuenta para ver lo" &
        " que va a facturar")
        '
        'ListViewCuentas
        '
        Me.ListViewCuentas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListViewCuentas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.ListViewCuentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.ListViewCuentas.FullRowSelect = True
        Me.ListViewCuentas.HideSelection = False
        Me.ListViewCuentas.Location = New System.Drawing.Point(7, 17)
        Me.ListViewCuentas.Name = "ListViewCuentas"
        Me.ListViewCuentas.Size = New System.Drawing.Size(379, 456)
        Me.ListViewCuentas.TabIndex = 2
        Me.ListViewCuentas.UseCompatibleStateImageBehavior = False
        Me.ListViewCuentas.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Cuenta"
        Me.ColumnHeader1.Width = 106
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "    Nombre"
        Me.ColumnHeader2.Width = 269
        '
        'DataGridViewComandas
        '
        Me.DataGridViewComandas.AllowUserToAddRows = False
        Me.DataGridViewComandas.AllowUserToDeleteRows = False
        Me.DataGridViewComandas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewComandas.AutoGenerateColumns = False
        Me.DataGridViewComandas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewComandas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewComandas.ColumnHeadersHeight = 25
        Me.DataGridViewComandas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DataGridViewComandas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.Check1, Me.CantCobrar, Me.CCantidadDataGridViewTextBoxColumn, Me.CDescripcionDataGridViewTextBoxColumn, Me.CPrecioDataGridViewTextBoxColumn, Me.SeparadaDataGridViewTextBoxColumn, Me.CProductoDataGridViewTextBoxColumn, Me.HabitacionDataGridViewTextBoxColumn})
        Me.DataGridViewComandas.DataSource = Me.ComandaTemporalBindingSource
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewComandas.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewComandas.Location = New System.Drawing.Point(398, 79)
        Me.DataGridViewComandas.Name = "DataGridViewComandas"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewComandas.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewComandas.RowHeadersWidth = 20
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewComandas.RowsDefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewComandas.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewComandas.RowTemplate.Height = 40
        Me.DataGridViewComandas.Size = New System.Drawing.Size(602, 397)
        Me.DataGridViewComandas.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.DataGridViewComandas, "De clic sobre el nombre del producto para ver y cambiar la cuenta en la que se va" &
        " a facturar")
        '
        'ComandaTemporalBindingSource
        '
        Me.ComandaTemporalBindingSource.DataMember = "ComandaTemporal"
        Me.ComandaTemporalBindingSource.DataSource = Me.DataSetComandas
        '
        'DataSetComandas
        '
        Me.DataSetComandas.DataSetName = "DataSetComandas"
        Me.DataSetComandas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBoxTotales
        '
        Me.GroupBoxTotales.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxTotales.Controls.Add(Me.TextBoxImpServ)
        Me.GroupBoxTotales.Controls.Add(Me.TextBoxTotal)
        Me.GroupBoxTotales.Controls.Add(Me.LabelTotal)
        Me.GroupBoxTotales.Controls.Add(Me.LabelSalonero)
        Me.GroupBoxTotales.Controls.Add(Me.TextBoxImpVenta)
        Me.GroupBoxTotales.Controls.Add(Me.LabelImpVenta)
        Me.GroupBoxTotales.Controls.Add(Me.TextBoxSubTotal)
        Me.GroupBoxTotales.Controls.Add(Me.LabelTotales)
        Me.GroupBoxTotales.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxTotales.Location = New System.Drawing.Point(480, 484)
        Me.GroupBoxTotales.Name = "GroupBoxTotales"
        Me.GroupBoxTotales.Size = New System.Drawing.Size(511, 67)
        Me.GroupBoxTotales.TabIndex = 2
        Me.GroupBoxTotales.TabStop = False
        Me.GroupBoxTotales.Text = "Totales de Cuenta"
        Me.ToolTip1.SetToolTip(Me.GroupBoxTotales, "Totales de la Cuenta Seleccionada")
        '
        'TextBoxImpServ
        '
        Me.TextBoxImpServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxImpServ.ForeColor = System.Drawing.Color.Black
        Me.TextBoxImpServ.Location = New System.Drawing.Point(253, 37)
        Me.TextBoxImpServ.Name = "TextBoxImpServ"
        Me.TextBoxImpServ.ReadOnly = True
        Me.TextBoxImpServ.Size = New System.Drawing.Size(110, 26)
        Me.TextBoxImpServ.TabIndex = 8
        Me.TextBoxImpServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxTotal
        '
        Me.TextBoxTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTotal.ForeColor = System.Drawing.Color.Black
        Me.TextBoxTotal.Location = New System.Drawing.Point(369, 37)
        Me.TextBoxTotal.Name = "TextBoxTotal"
        Me.TextBoxTotal.ReadOnly = True
        Me.TextBoxTotal.Size = New System.Drawing.Size(140, 26)
        Me.TextBoxTotal.TabIndex = 7
        Me.TextBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelTotal
        '
        Me.LabelTotal.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.LabelTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTotal.Location = New System.Drawing.Point(369, 16)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(140, 20)
        Me.LabelTotal.TabIndex = 6
        Me.LabelTotal.Text = "Total"
        Me.LabelTotal.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelSalonero
        '
        Me.LabelSalonero.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.LabelSalonero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelSalonero.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSalonero.Location = New System.Drawing.Point(253, 16)
        Me.LabelSalonero.Name = "LabelSalonero"
        Me.LabelSalonero.Size = New System.Drawing.Size(110, 20)
        Me.LabelSalonero.TabIndex = 4
        Me.LabelSalonero.Text = "Salonero"
        Me.LabelSalonero.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBoxImpVenta
        '
        Me.TextBoxImpVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxImpVenta.ForeColor = System.Drawing.Color.Black
        Me.TextBoxImpVenta.Location = New System.Drawing.Point(141, 37)
        Me.TextBoxImpVenta.Name = "TextBoxImpVenta"
        Me.TextBoxImpVenta.ReadOnly = True
        Me.TextBoxImpVenta.Size = New System.Drawing.Size(107, 26)
        Me.TextBoxImpVenta.TabIndex = 3
        Me.TextBoxImpVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelImpVenta
        '
        Me.LabelImpVenta.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.LabelImpVenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelImpVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelImpVenta.Location = New System.Drawing.Point(141, 16)
        Me.LabelImpVenta.Name = "LabelImpVenta"
        Me.LabelImpVenta.Size = New System.Drawing.Size(108, 20)
        Me.LabelImpVenta.TabIndex = 2
        Me.LabelImpVenta.Text = "Imp.Venta"
        Me.LabelImpVenta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBoxSubTotal
        '
        Me.TextBoxSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSubTotal.ForeColor = System.Drawing.Color.Black
        Me.TextBoxSubTotal.Location = New System.Drawing.Point(6, 37)
        Me.TextBoxSubTotal.Name = "TextBoxSubTotal"
        Me.TextBoxSubTotal.ReadOnly = True
        Me.TextBoxSubTotal.Size = New System.Drawing.Size(129, 26)
        Me.TextBoxSubTotal.TabIndex = 1
        Me.TextBoxSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelTotales
        '
        Me.LabelTotales.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.LabelTotales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelTotales.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTotales.Location = New System.Drawing.Point(6, 16)
        Me.LabelTotales.Name = "LabelTotales"
        Me.LabelTotales.Size = New System.Drawing.Size(129, 20)
        Me.LabelTotales.TabIndex = 0
        Me.LabelTotales.Text = "SubTotal"
        Me.LabelTotales.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ComboBoxCuentas
        '
        Me.ComboBoxCuentas.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBoxCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCuentas.Enabled = False
        Me.ComboBoxCuentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCuentas.FormattingEnabled = True
        Me.ComboBoxCuentas.Location = New System.Drawing.Point(920, 19)
        Me.ComboBoxCuentas.Name = "ComboBoxCuentas"
        Me.ComboBoxCuentas.Size = New System.Drawing.Size(71, 54)
        Me.ComboBoxCuentas.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(394, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "------------"
        '
        'ToolTip1
        '
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTip1.ToolTipTitle = "Información"
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.Location = New System.Drawing.Point(291, 482)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 79)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Salir"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.Button3, "Salir de separar cuentas y volver a la pantalla de comandas")
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Image = Global.SIALIBEB.My.Resources.Resources.Factura2
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(195, 482)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 79)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "Facturar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.Button2, "Facturar")
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(388, 481)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 79)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Volver"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.Button1, "Vuelve a la Mesa")
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.Image = Global.SIALIBEB.My.Resources.Resources.redshd
        Me.ButtonEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonEliminar.Location = New System.Drawing.Point(101, 482)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(86, 79)
        Me.ButtonEliminar.TabIndex = 5
        Me.ButtonEliminar.Text = "Quitar"
        Me.ButtonEliminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.ButtonEliminar, "Quita el último número de cuenta" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Si tiene platos, los envia a la cuenta ""Uno""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "N" &
        "o puede quitar la cuenta ""Uno""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.Image = Global.SIALIBEB.My.Resources.Resources.AgregarArticulo
        Me.ButtonAgregar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButtonAgregar.Location = New System.Drawing.Point(6, 482)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(89, 78)
        Me.ButtonAgregar.TabIndex = 2
        Me.ButtonAgregar.Text = "Agregar"
        Me.ButtonAgregar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.ButtonAgregar, "Agregar números de cuentas separadas.")
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'ComboBoxCantPasar
        '
        Me.ComboBoxCantPasar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBoxCantPasar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCantPasar.Enabled = False
        Me.ComboBoxCantPasar.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCantPasar.FormattingEnabled = True
        Me.ComboBoxCantPasar.Location = New System.Drawing.Point(843, 19)
        Me.ComboBoxCantPasar.Name = "ComboBoxCantPasar"
        Me.ComboBoxCantPasar.Size = New System.Drawing.Size(71, 54)
        Me.ComboBoxCantPasar.TabIndex = 7
        '
        'LabelCantidad
        '
        Me.LabelCantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelCantidad.BackColor = System.Drawing.Color.White
        Me.LabelCantidad.Location = New System.Drawing.Point(843, 0)
        Me.LabelCantidad.Name = "LabelCantidad"
        Me.LabelCantidad.Size = New System.Drawing.Size(71, 16)
        Me.LabelCantidad.TabIndex = 8
        Me.LabelCantidad.Text = "Cant. pasar"
        '
        'LabelCuentas
        '
        Me.LabelCuentas.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelCuentas.BackColor = System.Drawing.Color.White
        Me.LabelCuentas.Location = New System.Drawing.Point(920, 0)
        Me.LabelCuentas.Name = "LabelCuentas"
        Me.LabelCuentas.Size = New System.Drawing.Size(83, 16)
        Me.LabelCuentas.TabIndex = 9
        Me.LabelCuentas.Text = "Cuenta a Pasar"
        '
        'DataSetComandasBindingSource
        '
        Me.DataSetComandasBindingSource.DataSource = Me.DataSetComandas
        Me.DataSetComandasBindingSource.Position = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RAPIDOToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1005, 24)
        Me.MenuStrip1.TabIndex = 11
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        'RAPIDOToolStripMenuItem
        '
        Me.RAPIDOToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RAPIDOToolStripMenuItem1})
        Me.RAPIDOToolStripMenuItem.Name = "RAPIDOToolStripMenuItem"
        Me.RAPIDOToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.RAPIDOToolStripMenuItem.Text = "RAPIDO"
        '
        'RAPIDOToolStripMenuItem1
        '
        Me.RAPIDOToolStripMenuItem1.Name = "RAPIDOToolStripMenuItem1"
        Me.RAPIDOToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.RAPIDOToolStripMenuItem1.Size = New System.Drawing.Size(135, 22)
        Me.RAPIDOToolStripMenuItem1.Text = "RAPIDO"
        Me.RAPIDOToolStripMenuItem1.Visible = False
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'Check1
        '
        Me.Check1.DataPropertyName = "Check1"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle2.NullValue = False
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow
        Me.Check1.DefaultCellStyle = DataGridViewCellStyle2
        Me.Check1.HeaderText = "Cobrar"
        Me.Check1.Name = "Check1"
        Me.Check1.ReadOnly = True
        Me.Check1.Width = 50
        '
        'CantCobrar
        '
        Me.CantCobrar.DataPropertyName = "CantCobrar"
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow
        Me.CantCobrar.DefaultCellStyle = DataGridViewCellStyle3
        Me.CantCobrar.HeaderText = "Cant.Cobrar"
        Me.CantCobrar.Name = "CantCobrar"
        Me.CantCobrar.Width = 75
        '
        'CCantidadDataGridViewTextBoxColumn
        '
        Me.CCantidadDataGridViewTextBoxColumn.DataPropertyName = "CantCobrar"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CCantidadDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.CCantidadDataGridViewTextBoxColumn.FillWeight = 200.0!
        Me.CCantidadDataGridViewTextBoxColumn.HeaderText = "Cant"
        Me.CCantidadDataGridViewTextBoxColumn.Name = "CCantidadDataGridViewTextBoxColumn"
        Me.CCantidadDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CCantidadDataGridViewTextBoxColumn.Width = 50
        '
        'CDescripcionDataGridViewTextBoxColumn
        '
        Me.CDescripcionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.CDescripcionDataGridViewTextBoxColumn.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.CDescripcionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.CDescripcionDataGridViewTextBoxColumn.FillWeight = 200.0!
        Me.CDescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
        Me.CDescripcionDataGridViewTextBoxColumn.Name = "CDescripcionDataGridViewTextBoxColumn"
        Me.CDescripcionDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CDescripcionDataGridViewTextBoxColumn.Width = 400
        '
        'CPrecioDataGridViewTextBoxColumn
        '
        Me.CPrecioDataGridViewTextBoxColumn.DataPropertyName = "cPrecio"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.CPrecioDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.CPrecioDataGridViewTextBoxColumn.FillWeight = 200.0!
        Me.CPrecioDataGridViewTextBoxColumn.HeaderText = "Precio"
        Me.CPrecioDataGridViewTextBoxColumn.Name = "CPrecioDataGridViewTextBoxColumn"
        Me.CPrecioDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CPrecioDataGridViewTextBoxColumn.Width = 150
        '
        'SeparadaDataGridViewTextBoxColumn
        '
        Me.SeparadaDataGridViewTextBoxColumn.DataPropertyName = "separada"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.SeparadaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.SeparadaDataGridViewTextBoxColumn.HeaderText = "Cuenta"
        Me.SeparadaDataGridViewTextBoxColumn.Name = "SeparadaDataGridViewTextBoxColumn"
        Me.SeparadaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'CProductoDataGridViewTextBoxColumn
        '
        Me.CProductoDataGridViewTextBoxColumn.DataPropertyName = "cProducto"
        Me.CProductoDataGridViewTextBoxColumn.HeaderText = "Producto"
        Me.CProductoDataGridViewTextBoxColumn.Name = "CProductoDataGridViewTextBoxColumn"
        Me.CProductoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CProductoDataGridViewTextBoxColumn.Visible = False
        '
        'HabitacionDataGridViewTextBoxColumn
        '
        Me.HabitacionDataGridViewTextBoxColumn.DataPropertyName = "Habitacion"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.HabitacionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.HabitacionDataGridViewTextBoxColumn.HeaderText = "Hab."
        Me.HabitacionDataGridViewTextBoxColumn.Name = "HabitacionDataGridViewTextBoxColumn"
        Me.HabitacionDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'FormCrearCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1005, 563)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.LabelCuentas)
        Me.Controls.Add(Me.LabelCantidad)
        Me.Controls.Add(Me.ComboBoxCantPasar)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ButtonEliminar)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxCuentas)
        Me.Controls.Add(Me.GroupBoxTotales)
        Me.Controls.Add(Me.DataGridViewComandas)
        Me.Controls.Add(Me.GroupBoxCuentas)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(666, 537)
        Me.Name = "FormCrearCuentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuentas de la Mesa: "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBoxCuentas.ResumeLayout(False)
        CType(Me.DataGridViewComandas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComandaTemporalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetComandas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxTotales.ResumeLayout(False)
        Me.GroupBoxTotales.PerformLayout()
        CType(Me.DataSetComandasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.ComandaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBoxCuentas As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents DataGridViewComandas As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBoxTotales As System.Windows.Forms.GroupBox
    Friend WithEvents LabelTotales As System.Windows.Forms.Label
    Friend WithEvents TextBoxImpVenta As System.Windows.Forms.TextBox
    Friend WithEvents LabelImpVenta As System.Windows.Forms.Label
    Friend WithEvents TextBoxSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents LabelSalonero As System.Windows.Forms.Label
    Friend WithEvents TextBoxTotal As System.Windows.Forms.TextBox
    Friend WithEvents LabelTotal As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCuentas As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetComandasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetComandas As SIALIBEB.DataSetComandas
    Friend WithEvents ComandaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents IdcomandaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumeroComandaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubtotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoventaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CedusuarioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CedSaloneroDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdMesaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComenzalesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FacturadoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NumerofacturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CuentaContableDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContabilizadoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CortesiaDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AnuladoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NumeroCortesiaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodMonedaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComandaTemporalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents TextBoxImpServ As System.Windows.Forms.TextBox
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents CachedApertura_Cajas1 As SIALIBEB.CachedApertura_Cajas
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxCantPasar As System.Windows.Forms.ComboBox
    Friend WithEvents LabelCantidad As System.Windows.Forms.Label
    Friend WithEvents LabelCuentas As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents RAPIDOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RAPIDOToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ListViewCuentas As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Check1 As DataGridViewCheckBoxColumn
    Friend WithEvents CantCobrar As DataGridViewTextBoxColumn
    Friend WithEvents CCantidadDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CDescripcionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CPrecioDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SeparadaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CProductoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HabitacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
