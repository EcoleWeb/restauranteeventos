﻿Public Class frmComision

    Public Nuevo As Boolean
    Public CedulaCliente As String
    Public NombreCliente As String
    Public NombreGuia As String
    Public IdFac As String
    Public Guardo As Boolean = False

    Private Sub frmComision_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub
    Private Sub spIniciarForm()
  If Nuevo Then
            txIdentificacion.Focus()
        Else
            Dim sql As New SqlClient.SqlCommand
            Dim dt As New DataTable
            sql.CommandText = "select * from tb_R_Comision where IdFactura = @IdFac"
            sql.Parameters.AddWithValue("@IdFac", IdFac)
            cFunciones.spCargarDatos(sql, dt)
            If dt.Rows.Count > 0 Then
                txIdentificacion.Text = dt.Rows(0).Item("IdCliente")
                txtComisionista.Text = dt.Rows(0).Item("NombreCliente")
                txNombreGuia.Text = dt.Rows(0).Item("NombreGuia")

            End If
        End If
    End Sub

    Private Function fnValidarDatos() As Boolean
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable
        sql.CommandText = "Select * from tb_FD_Comisionista where Cedula = @Cedula"
        sql.Parameters.AddWithValue("@Cedula", txIdentificacion.Text)
        cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Hotel", "Conexion"))
        If Not dt.Rows.Count > 0 Then
            lbMensaje.Visible = True
            lbMensaje.Text = "Los datos del cliente no son validos."
            txIdentificacion.BackColor = Color.Bisque
            txIdentificacion.Focus()
            Return False
        Else
            lbMensaje.Visible = False
            txIdentificacion.BackColor = Color.White
        End If
        If txNombreGuia.Text = "" Then
            lbMensaje.Visible = True
            lbMensaje.Text = "Por Favor, completar los campos Obligatorios."
            txNombreGuia.BackColor = Color.Bisque
            txNombreGuia.Focus()
            Return False
        Else
            If Not Replace(txNombreGuia.Text, " ", "").Length > 0 Then
                lbMensaje.Visible = True
                lbMensaje.Text = "Por Favor, completar los campos Obligatorios."
                txNombreGuia.BackColor = Color.Bisque
                txNombreGuia.Focus()
                Return False
            Else
                lbMensaje.Visible = False
                txNombreGuia.BackColor = Color.White
            End If
        End If
    
        Return True
    End Function

    Private Sub spGuardar()
        If fnValidarDatos() Then
            CedulaCliente = txIdentificacion.Text
            NombreCliente = txtComisionista.Text
            NombreGuia = txNombreGuia.Text

            MsgBox("La información se guardó exitosamente.")
            DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub spCargarCliente(ByVal _Cliente As String)
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable

        sql.CommandText = "Select * from tb_FD_Comisionista where Cedula = @Cedula"
        sql.Parameters.AddWithValue("@Cedula", _Cliente)
        cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Hotel", "Conexion"))
        If Not dt.Rows.Count > 0 Then
            lbMensaje.Visible = True
            lbMensaje.Text = "Los datos del cliente no son validos."
            txIdentificacion.BackColor = Color.Bisque
            txIdentificacion.Focus()
            txtComisionista.Text = ""
        Else
            lbMensaje.Visible = False
            txIdentificacion.BackColor = Color.White
            txIdentificacion.Text = dt.Rows(0).Item("Cedula")
            txtComisionista.Text = dt.Rows(0).Item("Nombre")
            txNombreGuia.Focus()
        End If
    End Sub


    Private Sub ToolBar2_ButtonClick(sender As Object, e As ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : spNuevo()
            Case 4 : spGuardar()
            Case 8 : spCerrar()
        End Select
    End Sub

    Private Sub spNuevo()
        Try
            Dim frm As New frmComisionistaPuntoVenta
            Me.Visible = False
            frm.ShowDialog()
            Me.Visible = True
            txIdentificacion.Text = ""
            txtComisionista.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    
    End Sub

    Private Sub spCerrar()
        DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub txIdentificacion_KeyDown(sender As Object, e As KeyEventArgs) Handles txIdentificacion.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim sql As New SqlClient.SqlCommand
            Dim dt As New DataTable

            sql.CommandText = "SELECT * FROM tb_FD_Comisionista where Estado = 0"
            cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Hotel", "Conexion"))
            Dim Buscar1 As New BuscarCliente(dt)
            If Buscar1.ShowDialog() = DialogResult.OK Then
                If dt.Rows.Count > 0 Then
                    spCargarCliente(dt.Rows(Buscar1.Pos).Item("Cedula"))
                    Buscar1.Dispose()
                End If
                Buscar1.Dispose()
            Else
            End If
        End If

        If e.KeyCode = Keys.Enter Then
            spCargarCliente(txIdentificacion.Text)
        End If
    End Sub

    Private Sub txtComisionista_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtComisionista.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtComisionista_KeyDown(sender As Object, e As KeyEventArgs) Handles txtComisionista.KeyDown
        e.Handled = True
    End Sub
End Class