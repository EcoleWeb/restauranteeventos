<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GrupoMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GrupoMenu))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtDescripcionCuentaCosto = New System.Windows.Forms.TextBox
        Me.txtDescripcionCuentaIngreso = New System.Windows.Forms.TextBox
        Me.txtIDGrupo = New System.Windows.Forms.TextBox
        Me.txtCosto = New System.Windows.Forms.TextBox
        Me.btCargar = New System.Windows.Forms.Button
        Me.txtnombre = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtCuentaIngreso = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.Label12 = New System.Windows.Forms.Label
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox1.Controls.Add(Me.txtDescripcionCuentaCosto)
        Me.GroupBox1.Controls.Add(Me.txtDescripcionCuentaIngreso)
        Me.GroupBox1.Controls.Add(Me.txtIDGrupo)
        Me.GroupBox1.Controls.Add(Me.txtCosto)
        Me.GroupBox1.Controls.Add(Me.btCargar)
        Me.GroupBox1.Controls.Add(Me.txtnombre)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCuentaIngreso)
        Me.GroupBox1.Controls.Add(Me.Label2)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'txtDescripcionCuentaCosto
        '
        Me.txtDescripcionCuentaCosto.BackColor = System.Drawing.Color.White
        Me.txtDescripcionCuentaCosto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescripcionCuentaCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDescripcionCuentaCosto, "txtDescripcionCuentaCosto")
        Me.txtDescripcionCuentaCosto.Name = "txtDescripcionCuentaCosto"
        Me.txtDescripcionCuentaCosto.ReadOnly = True
        '
        'txtDescripcionCuentaIngreso
        '
        Me.txtDescripcionCuentaIngreso.BackColor = System.Drawing.Color.White
        Me.txtDescripcionCuentaIngreso.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescripcionCuentaIngreso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDescripcionCuentaIngreso, "txtDescripcionCuentaIngreso")
        Me.txtDescripcionCuentaIngreso.Name = "txtDescripcionCuentaIngreso"
        Me.txtDescripcionCuentaIngreso.ReadOnly = True
        '
        'txtIDGrupo
        '
        resources.ApplyResources(Me.txtIDGrupo, "txtIDGrupo")
        Me.txtIDGrupo.Name = "txtIDGrupo"
        '
        'txtCosto
        '
        Me.txtCosto.BackColor = System.Drawing.Color.White
        Me.txtCosto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCosto, "txtCosto")
        Me.txtCosto.Name = "txtCosto"
        '
        'btCargar
        '
        resources.ApplyResources(Me.btCargar, "btCargar")
        Me.btCargar.Name = "btCargar"
        Me.btCargar.UseVisualStyleBackColor = True
        '
        'txtnombre
        '
        Me.txtnombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtnombre, "txtnombre")
        Me.txtnombre.Name = "txtnombre"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'txtCuentaIngreso
        '
        Me.txtCuentaIngreso.BackColor = System.Drawing.Color.White
        Me.txtCuentaIngreso.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCuentaIngreso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtCuentaIngreso, "txtCuentaIngreso")
        Me.txtCuentaIngreso.Name = "txtCuentaIngreso"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'ToolBar1
        '
        resources.ApplyResources(Me.ToolBar1, "ToolBar1")
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarCerrar})
        Me.ToolBar1.ImageList = Me.ImageList
        Me.ToolBar1.Name = "ToolBar1"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "teclado1.gif")
        Me.ImageList.Images.SetKeyName(9, "")
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Name = "Label12"
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Name = "TituloModulo"
        '
        'lbUsuario
        '
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        '
        'GrupoMenu
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.TituloModulo)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "GrupoMenu"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCuentaIngreso As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Protected Friend WithEvents TituloModulo As System.Windows.Forms.Label
    Public WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Protected Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Protected Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btCargar As System.Windows.Forms.Button
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents txtIDGrupo As System.Windows.Forms.TextBox
    Public WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents txtDescripcionCuentaCosto As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionCuentaIngreso As System.Windows.Forms.TextBox
End Class
