﻿Public Class frmConfiguracionCategoria
    Dim colorseleccionado As String
    Dim pricambio As Boolean = False
    Sub spIniciarForm()
        spCargarCategorias()
    End Sub
    Sub spCargarCategorias()
        Dim cmd As New SqlClient.SqlCommand
        cmd.CommandText = "Select * From Categorias_Menu"
        cFunciones.spCargarDatos(cmd, DtsCategoriaMenu1.Categorias_Menu, GetSetting("Seesoft", "Restaurante", "Conexion"))
    End Sub
    Sub spColor()
        Dim MyDialog As New ColorDialog()

        MyDialog.AllowFullOpen = True
        MyDialog.ShowHelp = True
        If (MyDialog.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Dim intColorDec As Integer = Convert.ToInt32(MyDialog.Color.ToArgb)
            colorseleccionado = Microsoft.VisualBasic.Conversion.Hex(intColorDec)
        End If
    End Sub
    Private Function fnPermiteSalir() As Boolean
        If pricambio Then
            Dim frm As New frmMensaje("           ¿Desea guardar los cambios?", 1)
            Dim resultado As Integer = frm.ShowDialog
            If resultado = Windows.Forms.DialogResult.Yes Then
                spGuardar()
                Return pricambio
            ElseIf resultado = Windows.Forms.DialogResult.No Then
                Return True
            Else
                Return False
            End If
        End If

        Return True
    End Function
    Private Sub frmConfiguracionMovil_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub

    Private Sub grCategorias_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grCategorias.CellDoubleClick
        If e.ColumnIndex = 1 Then
            spColor()
            If colorseleccionado <> Nothing Then
                bsCategoriaMenu.Current("Color") = colorseleccionado
                bsCategoriaMenu.EndEdit()
                pricambio = True
            End If
           
        ElseIf e.ColumnIndex = 0 Then
            Dim fr As New frmConfiguracionMenu
            fr.pubIdCategoriaMenu = bsCategoriaMenu.Current("Id")
            fr.ShowDialog()
        End If
    End Sub

    Private Sub grCategorias_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles grCategorias.CellFormatting
        If e.ColumnIndex = 1 Then
            e.CellStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#" & e.Value)
            e.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#" & e.Value)
        End If
    End Sub

  
    Private Sub grCategorias_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grCategorias.DataError
        Try
            If grCategorias.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name.Equals("PosicionApp") Then
                Dim valor As Double = grCategorias.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue
                grCategorias.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = valor
                pricambio = True
            Else
                spMensaje("Datos invalidos.", 2)
                e.Cancel = False
            End If

        Catch ex As Exception
            spMensaje("Datos invalidos.", 2)
            e.Cancel = False
        End Try
    End Sub


    Sub spGuardar()

        Categorias_MenuTableAdapter.Connection.ConnectionString = GetSetting("Seesoft", "Restaurante", "Conexion")
        If bsCategoriaMenu.Count > 1 Then
            For i As Integer = 0 To DtsCategoriaMenu1.Categorias_Menu.Rows.Count - 1
                For j As Integer = 0 To bsCategoriaMenu.Count - 1
                    bsCategoriaMenu.Position = j
                    If DtsCategoriaMenu1.Categorias_Menu(i).Id <> bsCategoriaMenu.Current("Id") Then
                        If DtsCategoriaMenu1.Categorias_Menu(i).PosicionApp = bsCategoriaMenu.Current("PosicionApp") Then
                            spMensaje("Hay posiciones iguales.Debe cambiarlas.", 2)
                            pricambio = False
                            Exit Sub
                        End If
                    End If
                Next
            Next
            Try
                Categorias_MenuTableAdapter.Update(DtsCategoriaMenu1.Categorias_Menu)
                spMensaje("La información se guardó exitosamente.", 2)
                pricambio = False
            Catch ex As Exception
                MsgBox(ex.ToString, 1)
            End Try
        End If
    End Sub
    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click

        spGuardar()

    End Sub

   
    Private Sub frmConfiguracionCategoria_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not fnPermiteSalir() Then
            e.Cancel = True
        End If
    End Sub

  
    Private Sub grCategorias_CellValidating(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles grCategorias.CellValidating
        If e.ColumnIndex = 2 Then
            pricambio = True
        End If
    End Sub
End Class