﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMensaje
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btAceptar = New System.Windows.Forms.Button
        Me.toMensaje = New System.Windows.Forms.ToolTip(Me.components)
        Me.btSi = New System.Windows.Forms.Button
        Me.btNo = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lbMensaje = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btAceptar)
        Me.Panel2.Location = New System.Drawing.Point(17, 65)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(213, 52)
        Me.Panel2.TabIndex = 4
        '
        'btAceptar
        '
        Me.btAceptar.FlatAppearance.BorderSize = 0
        Me.btAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAceptar.Image = Global.SIALIBEB.My.Resources.Resources.Aceptar
        Me.btAceptar.Location = New System.Drawing.Point(92, 15)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(41, 34)
        Me.btAceptar.TabIndex = 2
        Me.toMensaje.SetToolTip(Me.btAceptar, "Aceptar")
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'btSi
        '
        Me.btSi.FlatAppearance.BorderSize = 0
        Me.btSi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btSi.Image = Global.SIALIBEB.My.Resources.Resources.Si
        Me.btSi.Location = New System.Drawing.Point(47, 8)
        Me.btSi.Name = "btSi"
        Me.btSi.Size = New System.Drawing.Size(44, 37)
        Me.btSi.TabIndex = 0
        Me.toMensaje.SetToolTip(Me.btSi, "Sí")
        Me.btSi.UseVisualStyleBackColor = True
        '
        'btNo
        '
        Me.btNo.FlatAppearance.BorderSize = 0
        Me.btNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btNo.Image = Global.SIALIBEB.My.Resources.Resources.No
        Me.btNo.Location = New System.Drawing.Point(138, 8)
        Me.btNo.Name = "btNo"
        Me.btNo.Size = New System.Drawing.Size(44, 37)
        Me.btNo.TabIndex = 1
        Me.toMensaje.SetToolTip(Me.btNo, "No")
        Me.btNo.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lbMensaje)
        Me.Panel3.Location = New System.Drawing.Point(2, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(275, 57)
        Me.Panel3.TabIndex = 4
        '
        'lbMensaje
        '
        Me.lbMensaje.AutoSize = True
        Me.lbMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMensaje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbMensaje.Location = New System.Drawing.Point(13, 24)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(150, 16)
        Me.lbMensaje.TabIndex = 0
        Me.lbMensaje.Text = "¿Desea guardar datos?"
        Me.lbMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btSi)
        Me.Panel1.Controls.Add(Me.btNo)
        Me.Panel1.Location = New System.Drawing.Point(18, 65)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(213, 55)
        Me.Panel1.TabIndex = 8
        '
        'frmMensaje
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 140)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(374, 179)
        Me.Name = "frmMensaje"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents toMensaje As System.Windows.Forms.ToolTip
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lbMensaje As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btSi As System.Windows.Forms.Button
    Friend WithEvents btNo As System.Windows.Forms.Button
End Class
