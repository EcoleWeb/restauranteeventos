Public Class Saloneros

#Region "Variables"
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Dim rs As SqlClient.SqlDataReader
    Dim sexo, modo As String
    Dim PMU As New PerfilModulo_Class
    Dim cedula As String
#End Region

    Private Sub Saloneros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        sexo = "Masculino"
        rbMasculino.Checked = True
        nuevo()
        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            txtClave.Focus()
        End If
       
        '---------------------------------------------------------------
    End Sub

    Private Sub Saloneros_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cConexion.DesConectar(conectadobd)
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 1 : nuevo()
            Case 2 : If PMU.Update Then Me.editar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 3 : If PMU.Find Then Me.buscar() Else MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 4 : If PMU.Update Then Me.guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 5 : If PMU.Delete Then borrar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 6 : If PMU.Others Then relacion() Else MsgBox("No tiene permiso para crear Relaciones...", MsgBoxStyle.Information, "Atención..") : Exit Sub
            Case 7 : If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atención...") : Exit Sub
            Case 8 : teclado()
            Case 9 : Close()
        End Select
    End Sub

    Private Sub nuevo()
        txtCedula.Text = ""
        txtNombre.Text = ""
        txtDireccion.Text = ""
        txtTelefono.Text = ""
        rbMasculino.Checked = True
        txtTurno.Text = ""
        modo = "Nuevo"
        txtCedula.ReadOnly = False
    End Sub

    Private Sub editar()
        modo = "Actualizar"
        Dim Buscador As BuscadorSaloneros = New BuscadorSaloneros
        Buscador.tipo = True
        Buscador.str = "SELECT Cedula, Nombre, Sexo, Telefono, Direccion, Turno from Saloneros"
        Buscador.bd = "Restaurante"
        Buscador.tabla = "Saloneros"
        Buscador.ShowDialog()
        txtCedula.Text = Buscador.cedula
        txtNombre.Text = Buscador.nombre
        If Trim(Buscador.sexo) = "Masculino" Then
            rbMasculino.Checked = True
        Else
            rbFemenino.Checked = True
        End If
        txtDireccion.Text = Buscador.direccion
        txtTurno.Text = Buscador.turno
        txtTelefono.Text = Buscador.telefono
        Buscador.Dispose()
        modo = "Actualizar"
        If txtCedula.Text <> "" Then
            txtCedula.ReadOnly = True
        Else
            txtCedula.ReadOnly = False
        End If
    End Sub

    Private Sub buscar()
        modo = "Nuevo"
        Dim Buscador As BuscadorSaloneros = New BuscadorSaloneros
        Buscador.tipo = False
        Buscador.rbplanilla.Visible = True
        Buscador.rbUsuarios.Visible = True
        Buscador.Label2.Visible = True
        Buscador.bd = "Restaurante"
        Buscador.ShowDialog()
        txtCedula.Text = Buscador.cedula
        txtNombre.Text = Buscador.nombre
        If Buscador.sexo <> "" Then
            If CBool(Buscador.sexo) = True Then
                rbMasculino.Checked = True
            Else
                rbFemenino.Checked = True
            End If
        End If

        txtTelefono.Text = Buscador.telefono
        Buscador.Dispose()
        If txtCedula.Text <> "" Then
            txtCedula.ReadOnly = True
        Else
            txtCedula.ReadOnly = False
        End If
    End Sub

    Private Sub guardar()
        If txtCedula.Text <> "" And txtNombre.Text <> "" And txtDireccion.Text <> "" And txtTelefono.Text <> "" And txtTurno.Text <> "" Then
            Dim mensaje As String = ""
            Dim existe As Boolean = False
            cConexion.GetRecorset(conectadobd, "Select * from Saloneros where Cedula='" & txtCedula.Text & "'", rs)
            While rs.Read
                existe = True
            End While
            rs.Close()

            If rbMasculino.Checked = True Then
                sexo = "Masculino"
            Else
                sexo = "Femenino"
            End If


            If modo = "Nuevo" Then
                If existe = True Then
                    MessageBox.Show("El salonero ya existe!", "Atencion...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
                mensaje = cConexion.AddNewRecord(conectadobd, "Saloneros", "Cedula, Nombre, Telefono, Direccion, Sexo, Turno", "'" & txtCedula.Text & "','" & txtNombre.Text & "','" & txtTelefono.Text & "','" & txtDireccion.Text & "','" & sexo & "','" & txtTurno.Text & "'")
            Else
                If existe = True Then
                    mensaje = cConexion.UpdateRecords(conectadobd, "Saloneros", "Nombre = '" & txtNombre.Text & "', Telefono ='" & txtTelefono.Text & "', Direccion = '" & txtDireccion.Text & "', Sexo= '" & sexo & "', Turno='" & txtTurno.Text & "'", "Cedula='" & txtCedula.Text & "'")
                Else
                    MessageBox.Show("No se puede editar un Salonero que no ha sido registrado", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                End If
            End If

            If mensaje = "" Then
                MessageBox.Show("El salonero ha sido ingresado correctamente", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Close()
                nuevo()
            Else
                MessageBox.Show("El salonero no fue ingresado", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            End If
        Else
            MessageBox.Show("Debe llenar todos los campos", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End If
    End Sub

    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub imprimir()
        Try
            Dim ISaloneros As New rptSaloneros
            Dim visor As New frmVisorReportes
            Me.Hide()
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, ISaloneros, False, Me.conectadobd.ConnectionString)
            ISaloneros.Refresh()
            visor.ShowDialog()
            visor.Dispose()
            Me.Show()
        Catch ex As Exception
            MessageBox.Show("Error al cargar el reporte", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub relacion()
        Dim FRelacion As New Relacion_Saloneros
        Me.Hide()
        FRelacion.ShowDialog()
        FRelacion.Dispose()
        Me.Show()
    End Sub

    Private Sub borrar()
        If txtCedula.Text <> "" And txtNombre.Text <> "" Then
            Dim mensaje As String
            If MessageBox.Show("Desea eliminar el Salonero " & Trim(txtNombre.Text), "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                mensaje = cConexion.DeleteRecords(conectadobd, "Saloneros", "Cedula='" & txtCedula.Text & "'")
                If mensaje = "" Then
                    MessageBox.Show("EL salonero ha sido eliminado correctamente", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    nuevo()
                End If
            End If
        Else
            MessageBox.Show("Debe llenar los campos correctamente", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End If
    End Sub

    Private Sub txtCedula_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCedula.KeyDown
        If e.KeyCode = Keys.F1 And modo = "Nuevo" Then
            txtCedula.ReadOnly = True
            PMU = VSM(cedula, Me.Name)
            If PMU.Find Then Me.buscar() Else MsgBox("No tiene permiso para buscar información...", MsgBoxStyle.Information, "Atención...") : Exit Sub
        End If
        If e.KeyCode = Keys.Enter Then
            txtNombre.Focus()
        End If
    End Sub

    Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtTelefono.Focus()
        End If
    End Sub

    Private Sub txtTelefono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTelefono.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtDireccion.Focus()
        End If
    End Sub


    Private Sub txtDireccion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDireccion.KeyDown
        If e.KeyCode = Keys.Enter Then
            rbMasculino.Focus()
        End If
    End Sub

    Private Sub rbMasculino_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rbMasculino.KeyDown
        If e.KeyCode = Keys.Enter Then
            rbFemenino.Focus()
        End If
    End Sub

    Private Sub rbFemenino_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rbFemenino.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtTurno.Focus()
        End If
    End Sub

#Region "Validacion Usuario"
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarRelacion.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarTeclado.Enabled = True
                Me.txtCedula.Enabled = True
                Me.lbUsuario.Text = usuario
                txtCedula.Focus()
            Else
                Me.Enabled = True
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                txtClave.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarRelacion.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarTeclado.Enabled = True
                Me.txtCedula.Enabled = True
                cedula = User_Log.Cedula
                Me.lbUsuario.Text = User_Log.Nombre
                txtCedula.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub txtClave_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtClave.TextChanged

    End Sub
End Class