﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfiguracionAvanzada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RepV_VentasT = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btn_DesactivarEvacion = New System.Windows.Forms.Button()
        Me.Btn_ActivarEvacion = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Cmb_Moneda = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Cargar = New System.Windows.Forms.Button()
        Me.Ck_Tributables = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Dtp_Final = New System.Windows.Forms.DateTimePicker()
        Me.Dtp_Inicio = New System.Windows.Forms.DateTimePicker()
        Me.Btn_ActualizaContador = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbContador = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'RepV_VentasT
        '
        Me.RepV_VentasT.ActiveViewIndex = -1
        Me.RepV_VentasT.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RepV_VentasT.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RepV_VentasT.Cursor = System.Windows.Forms.Cursors.Default
        Me.RepV_VentasT.DisplayBackgroundEdge = False
        Me.RepV_VentasT.Location = New System.Drawing.Point(15, 134)
        Me.RepV_VentasT.Name = "RepV_VentasT"
        Me.RepV_VentasT.SelectionFormula = ""
        Me.RepV_VentasT.ShowGroupTreeButton = False
        Me.RepV_VentasT.Size = New System.Drawing.Size(946, 377)
        Me.RepV_VentasT.TabIndex = 5
        Me.RepV_VentasT.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.RepV_VentasT.ViewTimeSelectionFormula = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbContador)
        Me.GroupBox1.Controls.Add(Me.Btn_DesactivarEvacion)
        Me.GroupBox1.Controls.Add(Me.Btn_ActivarEvacion)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Btn_ActualizaContador)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(943, 120)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Inicializar"
        '
        'Btn_DesactivarEvacion
        '
        Me.Btn_DesactivarEvacion.Location = New System.Drawing.Point(161, 68)
        Me.Btn_DesactivarEvacion.Name = "Btn_DesactivarEvacion"
        Me.Btn_DesactivarEvacion.Size = New System.Drawing.Size(137, 23)
        Me.Btn_DesactivarEvacion.TabIndex = 6
        Me.Btn_DesactivarEvacion.Text = "Desactivar Evacion"
        Me.Btn_DesactivarEvacion.UseVisualStyleBackColor = True
        '
        'Btn_ActivarEvacion
        '
        Me.Btn_ActivarEvacion.Location = New System.Drawing.Point(25, 68)
        Me.Btn_ActivarEvacion.Name = "Btn_ActivarEvacion"
        Me.Btn_ActivarEvacion.Size = New System.Drawing.Size(122, 23)
        Me.Btn_ActivarEvacion.TabIndex = 5
        Me.Btn_ActivarEvacion.Text = "Activar Evacion "
        Me.Btn_ActivarEvacion.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Cmb_Moneda)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Btn_Cargar)
        Me.GroupBox2.Controls.Add(Me.Ck_Tributables)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Dtp_Final)
        Me.GroupBox2.Controls.Add(Me.Dtp_Inicio)
        Me.GroupBox2.Location = New System.Drawing.Point(464, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(470, 106)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Reporte"
        '
        'Cmb_Moneda
        '
        Me.Cmb_Moneda.FormattingEnabled = True
        Me.Cmb_Moneda.Location = New System.Drawing.Point(69, 74)
        Me.Cmb_Moneda.Name = "Cmb_Moneda"
        Me.Cmb_Moneda.Size = New System.Drawing.Size(88, 21)
        Me.Cmb_Moneda.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Moneda :"
        '
        'Btn_Cargar
        '
        Me.Btn_Cargar.Location = New System.Drawing.Point(267, 75)
        Me.Btn_Cargar.Name = "Btn_Cargar"
        Me.Btn_Cargar.Size = New System.Drawing.Size(182, 23)
        Me.Btn_Cargar.TabIndex = 5
        Me.Btn_Cargar.Text = "Cargar"
        Me.Btn_Cargar.UseVisualStyleBackColor = True
        '
        'Ck_Tributables
        '
        Me.Ck_Tributables.AutoSize = True
        Me.Ck_Tributables.Location = New System.Drawing.Point(163, 75)
        Me.Ck_Tributables.Name = "Ck_Tributables"
        Me.Ck_Tributables.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Ck_Tributables.Size = New System.Drawing.Size(81, 17)
        Me.Ck_Tributables.TabIndex = 4
        Me.Ck_Tributables.Text = "Tributables "
        Me.Ck_Tributables.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(166, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Fecha Fin :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(166, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Fecha Inicio :"
        '
        'Dtp_Final
        '
        Me.Dtp_Final.Location = New System.Drawing.Point(249, 47)
        Me.Dtp_Final.Name = "Dtp_Final"
        Me.Dtp_Final.Size = New System.Drawing.Size(200, 20)
        Me.Dtp_Final.TabIndex = 1
        '
        'Dtp_Inicio
        '
        Me.Dtp_Inicio.Location = New System.Drawing.Point(249, 20)
        Me.Dtp_Inicio.Name = "Dtp_Inicio"
        Me.Dtp_Inicio.Size = New System.Drawing.Size(200, 20)
        Me.Dtp_Inicio.TabIndex = 0
        '
        'Btn_ActualizaContador
        '
        Me.Btn_ActualizaContador.Location = New System.Drawing.Point(192, 16)
        Me.Btn_ActualizaContador.Name = "Btn_ActualizaContador"
        Me.Btn_ActualizaContador.Size = New System.Drawing.Size(85, 23)
        Me.Btn_ActualizaContador.TabIndex = 2
        Me.Btn_ActualizaContador.Text = "Actualizar"
        Me.Btn_ActualizaContador.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Contador : "
        '
        'cmbContador
        '
        Me.cmbContador.FormattingEnabled = True
        Me.cmbContador.Location = New System.Drawing.Point(65, 18)
        Me.cmbContador.Name = "cmbContador"
        Me.cmbContador.Size = New System.Drawing.Size(121, 21)
        Me.cmbContador.TabIndex = 7
        '
        'FrmConfiguracionAvanzada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(975, 542)
        Me.Controls.Add(Me.RepV_VentasT)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmConfiguracionAvanzada"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Analisis de Ventas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RepV_VentasT As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_Cargar As System.Windows.Forms.Button
    Friend WithEvents Ck_Tributables As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Dtp_Final As System.Windows.Forms.DateTimePicker
    Friend WithEvents Dtp_Inicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_ActualizaContador As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cmb_Moneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_DesactivarEvacion As System.Windows.Forms.Button
    Friend WithEvents Btn_ActivarEvacion As System.Windows.Forms.Button
    Friend WithEvents cmbContador As ComboBox
End Class
