﻿Imports System.Data
Imports System.Data.SqlClient

Public Class FrmConfiguracionAvanzada

    Private Sub CrystalReportViewer1_Load(sender As Object, e As EventArgs)

    End Sub

    Private Sub FrmConfiguracionAvanzada_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Dt As DataTable
        Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Hotel", "conexion"))
        Dim Da As New SqlDataAdapter
        Dim Cmd As New SqlCommand



        VerificarEstadoTrigguer()
        LlenarComboContador()
        Me.cmbContador.SelectedValue = OptenerContador()
        Me.Dtp_Inicio.Text = Date.Today
        Me.Dtp_Final.Text = Date.Today


        Try
            With Cmd
                .CommandType = CommandType.Text
                .CommandText = "select * from [Seguridad].[dbo].[Moneda]"
                .Connection = Cn
            End With
            Da.SelectCommand = Cmd
            Dt = New DataTable
            Da.Fill(Dt)
            With Me.Cmb_Moneda
                .DataSource = Dt
                .DisplayMember = "MonedaNombre"
                .ValueMember = "MonedaNombre"
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Sub VerificarEstadoTrigguer()
        Try
            Dim EstadoTrigguer As Integer = 0
            Dim CnRestaurante As New SqlConnection(GetSetting("SeeSOFT", "Restaurante", "conexion"))
            Dim cx As New Conexion

            EstadoTrigguer = cx.SlqExecuteScalar(CnRestaurante, "select  CASE WHEN OBJECTPROPERTY(o.[id], 'ExecIsTriggerDisabled') = 0 THEN 1 ELSE 0 END as Estado FROM sysobjects o WHERE OBJECTPROPERTY(o.[id], 'IsTrigger') = 1 and  OBJECT_NAME(o.parent_obj)='OpcionesDePago'")

            If EstadoTrigguer = 1 Then
                Btn_ActivarEvacion.Visible = False
                Btn_DesactivarEvacion.Visible = True
            Else
                Btn_DesactivarEvacion.Visible = False
                Btn_ActivarEvacion.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub LlenarComboContador()
        Try
            Dim dtOpciones As New DataTable

            cFunciones.Llenar_Tabla_Generico("Select Contador, convert(varchar, Contador) + ' - '+ Descripcion as Descripcion from tb_OpcionesEvasion", dtOpciones)
            If dtOpciones.Rows.Count > 0 Then
                cmbContador.DataSource = dtOpciones
                cmbContador.ValueMember = "Contador"
                cmbContador.DisplayMember = "Descripcion"
            End If

        Catch ex As Exception
            MsgBox("No se cargaron las opciones de envio.")
        End Try
    End Sub
    Function OptenerContador() As Integer
        Try
            Dim Contador As Integer
            Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Restaurante", "conexion"))
            Dim cmd As SqlCommand = Cn.CreateCommand()
            cmd.CommandText = "SELECT [Contador] FROM [Restaurante].[dbo].[Tb_Contador]"
            Cn.Open()
            Dim value As Object = cmd.ExecuteScalar()
            Contador = Convert.ToInt64(value)


            Return Contador
        Catch ex As Exception

        End Try

    End Function
    Function OptenerTipoCambio() As Double
        Dim Cambio As Double
        Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Hotel", "conexion"))
        Dim cmd As SqlCommand = Cn.CreateCommand()
        cmd.CommandText = "select ValorCompra from [Seguridad].[dbo].[Moneda] where MonedaNombre='" & Me.Cmb_Moneda.Text & "'"
        Cn.Open()
        Dim value As Object = cmd.ExecuteScalar()
        Cambio = Convert.ToDouble(value)


        Return Cambio
    End Function

    Sub ActualizarContador()
        Dim Cambio As Integer
        Cambio = Me.cmbContador.SelectedValue


        Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Restaurante", "conexion"))
        Dim cmd As SqlCommand = Cn.CreateCommand()
        cmd.CommandText = "Update [Restaurante].[dbo].[Tb_Contador] set [Contador]= " & Cambio & ", [ContadorTemporal]=" & Cambio + 1
        Cn.Open()
        Dim value As Object = cmd.ExecuteScalar()
        Cambio = Convert.ToDouble(value)

    End Sub

    Private Sub Btn_ActualizaContador_Click(sender As Object, e As EventArgs) Handles Btn_ActualizaContador.Click
        ActualizarContador()
        MsgBox("Se actualizo el Contador", MsgBoxStyle.Information, "Alerta")

    End Sub

    Private Sub Btn_Cargar_Click(sender As Object, e As EventArgs) Handles Btn_Cargar.Click
        Dim ReporteT As New VentasGeneral
        Dim ReporteNT As New VentasGeneralNT


        If Me.Ck_Tributables.Checked Then

            ReporteT.SetParameterValue(0, "REPORTE DE VENTAS BRUTAS DESDE EL '" & Me.Dtp_Inicio.Text & "' HASTA EL '" & Me.Dtp_Final.Text & "'")
            ReporteT.SetParameterValue(1, OptenerTipoCambio())
            ReporteT.SetParameterValue(2, CStr(OptenerTipoCambio()))
            ReporteT.SetParameterValue(3, Dtp_Inicio.Value)
            ReporteT.SetParameterValue(4, Dtp_Final.Value.AddDays(1))
            ReporteT.SetParameterValue(5, User_Log.PuntoVenta)
            ReporteT.SetParameterValue(6, CStr(User_Log.NombrePunto))

            Me.RepV_VentasT.SelectionFormula = "{Ventas.Fecha} >= {?FechaInicialPrincipal} and {Ventas.Fecha} <={?FechaFinalPrincipal} and {Ventas.Anulado} = false and {Ventas.Num_Factura} <> 0 and {Ventas.Proveniencia_Venta} = {?PuntodeVenta} and {Ventas.Tributable} = true"
            CrystalReportsConexion.LoadReportViewer(RepV_VentasT, ReporteT, , GetSetting("SeeSOFT", "Hotel", "conexion"))

        Else
            ReporteNT.SetParameterValue(0, "REPORTE DE VENTAS BRUTAS DESDE EL '" & Me.Dtp_Inicio.Text & "' HASTA EL '" & Me.Dtp_Final.Text & "'")
            ReporteNT.SetParameterValue(1, OptenerTipoCambio())
            ReporteNT.SetParameterValue(2, CStr(OptenerTipoCambio()))
            ReporteNT.SetParameterValue(3, Dtp_Inicio.Value)
            ReporteNT.SetParameterValue(4, Dtp_Final.Value.AddDays(1))
            ReporteNT.SetParameterValue(5, User_Log.PuntoVenta)
            ReporteNT.SetParameterValue(6, CStr(User_Log.NombrePunto))

            Me.RepV_VentasT.SelectionFormula = "{Ventas.Fecha} >= {?FechaInicialPrincipal} and {Ventas.Fecha} <={?FechaFinalPrincipal} and {Ventas.Anulado} = false and {Ventas.Num_Factura} <> 0 and {Ventas.Proveniencia_Venta} = {?PuntodeVenta} and {Ventas.Tributable} = false"
            CrystalReportsConexion.LoadReportViewer(RepV_VentasT, ReporteNT, , GetSetting("SeeSOFT", "Hotel", "conexion"))

        End If


    End Sub
    Private Sub Btn_ActivarEvacion_Click(sender As Object, e As EventArgs) Handles Btn_ActivarEvacion.Click
        If ActivarEvacion() = True Then
            MsgBox("Se Activo la evación", MsgBoxStyle.Information, "Alerta")
        Else
            MsgBox("Contacte a su proveedor de servicio algo salio mal..", MsgBoxStyle.Information, "Alerta")
        End If
        VerificarEstadoTrigguer()
    End Sub
    Function ActivarEvacion() As Boolean

        Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Restaurante", "conexion"))
        Dim cmd As SqlCommand = Cn.CreateCommand()
        cmd.CommandText = "alter table dbo.OpcionesDePago enable TRIGGER ActualizaTributableXFormaPagoClienteContado"
        Cn.Open()
        Dim value As Object = cmd.ExecuteScalar()
        Return True
    End Function
    Function DesactivarEvacion() As Boolean

        Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Restaurante", "conexion"))
        Dim cmd As SqlCommand = Cn.CreateCommand()
        cmd.CommandText = "alter table dbo.OpcionesDePago disable TRIGGER ActualizaTributableXFormaPagoClienteContado"
        Cn.Open()
        Dim value As Object = cmd.ExecuteScalar()


        Return True
    End Function

    Private Sub Btn_DesactivarEvacion_Click(sender As Object, e As EventArgs) Handles Btn_DesactivarEvacion.Click
        If DesactivarEvacion() = True Then
            MsgBox("Se Desactivo la evación", MsgBoxStyle.Information, "Alerta")
        Else
            MsgBox("Contacte a su proveedor de servicio algo salio mal..", MsgBoxStyle.Information, "Alerta")
        End If
        VerificarEstadoTrigguer()
    End Sub
End Class