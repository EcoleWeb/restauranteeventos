﻿Imports System.Data.SqlClient

Public Class frmRepartidoresFact

    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Dim rs As SqlClient.SqlDataReader
    Dim cedula As String
    Dim PrimeraCarga As Integer = 0
    Dim dtGeneral As New DataTable
    Dim Unicos As New ArrayList

    Private Sub frmRepartidoresFact_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cedula = User_Log.Cedula
        PrimeraCarga = 1
    End Sub

    Sub BuscarFactura()
        Try

            Dim consulta As SqlCommand
            Dim dtBuscar As New DataTable()
            Unicos.Clear()
            dtBuscar.Clear()

            If txtBuscar.Text.Trim() = "" Then
                consulta = New SqlCommand("Select * from vs_FacturasRepartidores where (dbo.dateonlyInicio(Fecha)>=dbo.dateonlyInicio(@Desde) and dbo.dateonlyInicio(Fecha)<=dbo.dateonlyInicio(@Hasta)) order by Estado desc")

            Else
                consulta = New SqlCommand("Select * from vs_FacturasRepartidores where (dbo.dateonlyInicio(Fecha)>=dbo.dateonlyInicio(@Desde) and dbo.dateonlyInicio(Fecha)<=dbo.dateonlyInicio(@Hasta)) and (Repartidor like '%'+@Nombre+'%')  order by Estado desc")
                consulta.Parameters.AddWithValue("@Nombre", txtBuscar.Text)
            End If

            consulta.Parameters.AddWithValue("@Desde", dtpDesde.Value)
            consulta.Parameters.AddWithValue("@Hasta", dtpHasta.Value)

            cFunciones.spCargarDatos(consulta, dtBuscar, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dtBuscar.Rows.Count > 0 Then

                For b As Integer = 0 To dtBuscar.Rows.Count - 1
                    If Not Unicos.Contains(dtBuscar.Rows(b).Item("Factura")) Then
                        Unicos.Add(dtBuscar.Rows(b).Item("Factura"))
                    Else
                        dtBuscar.Rows(b).Item("TotalSinFlete") = 0
                        dtBuscar.Rows(b).Item("Flete") = 0
                    End If
                Next


                dtGeneral.Rows.Clear()
                dtGeneral.Columns.Clear()
                dtGeneral.Clear()
                dtGeneral = dtBuscar.Copy()
                CalcularTotal(dtGeneral)
            Else
                LimpiarTotales()
            End If

        Catch ex As Exception
            MsgBox("Error al buscar facturas. " & +ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub LimpiarTotales()
        txtTotalSinpeTarSinFlete.Text = "0"
        txtTotalFlete.Text = "0"
        txtTotalGeneral.Text = "0"
        lbContador.Text = "0"
    End Sub
    Sub CalcularTotal(ByVal dt As DataTable)
        Try

            Dim TotalGeneral As Double = 0
            Dim TotalFlete As Double = 0
            Dim Rebajos As Double = 0
            Dim TotalCobrar As Double = 0
            Dim TotalEfectivo As Double = 0
            Dim TotalTarConFlete As Double = 0
            Dim TotalSinpeConFlete As Double = 0
            Dim TotalIngreso As Double = 0
            Dim TotalCreditoConFlete As Double = 0

            For a As Integer = 0 To dt.Rows.Count - 1
                TotalGeneral += CDbl(dt.Rows(a).ItemArray(9)) ' itemarray 9 es la columna de total General
                TotalFlete += CDbl(dt.Rows(a).ItemArray(8)) 'itemarray 8 es la columna de flete

                If dt.Rows(a).ItemArray(12) = "TAR" Then
                    TotalTarConFlete += CDbl(dt.Rows(a).ItemArray(9))
                End If

                If dt.Rows(a).ItemArray(12) = "SINPE" Then
                    TotalSinpeConFlete += CDbl(dt.Rows(a).ItemArray(9))
                End If

                If dt.Rows(a).ItemArray(12) = "CRE" Then
                    TotalCreditoConFlete += CDbl(dt.Rows(a).ItemArray(9))
                End If
            Next

            Rebajos = TotalSinpeConFlete + TotalTarConFlete + TotalCreditoConFlete
            TotalEfectivo = TotalGeneral - TotalSinpeConFlete - TotalTarConFlete - TotalCreditoConFlete
            TotalCobrar = TotalGeneral - TotalFlete - Rebajos
            TotalIngreso = TotalGeneral - TotalFlete

            txtTotalGeneral.Text = "₡ " & TotalGeneral.ToString("#,##0.00")
            txtTotalFlete.Text = "₡ " & TotalFlete.ToString("#,##0.00")
            txtTotalSinpeTarSinFlete.Text = "₡ " & Rebajos.ToString("#,##0.00")
            txtTotalCobrar.Text = "₡ " & TotalCobrar.ToString("#,##0.00")
            txtCreditoConFlete.Text = "₡ " & TotalCreditoConFlete.ToString("#,##0.00")
            txtEfectivo.Text = "₡ " & TotalEfectivo.ToString("#,##0.00")
            txtTarConFlete.Text = "₡ " & TotalTarConFlete.ToString("#,##0.00")
            txtSinpeConFlete.Text = "₡ " & TotalSinpeConFlete.ToString("#,##0.00")
            txtTotalIngreso.Text = "₡ " & TotalIngreso.ToString("#,##0.00")
            lbContador.Text = dt.Rows.Count

        Catch ex As Exception
            MsgBox("Error al calcular totales. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btBuscar_Click(sender As Object, e As EventArgs) Handles btBuscar.Click
        BuscarFactura()
        PasarDatos(dtGeneral)
    End Sub

    Private Sub btDetalle_Click(sender As Object, e As EventArgs) Handles btDetalle.Click

        Dim numerocomanda, comenzales As Integer
        Dim factura As Int64
        Dim Anula As Boolean
        Dim mesa As String = ""
        Dim idmesa As String = ""

        If dgvFacturas.SelectedCells.Count > 0 Then

            Try
                factura = dgvFacturas.CurrentRow.Cells(3).Value
            Catch ex As Exception
                MsgBox("Debe de seleccionar una factura antes de continuar...", MsgBoxStyle.Information, "Atención")
                Exit Sub
            End Try

            Try
                Dim cont As Integer = 0
                If IsNumeric(factura) = False Then
                    MessageBox.Show("Verifique la selección del registro", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                cConexion.DesConectar(conectadobd)
                conectadobd = cConexion.Conectar("Restaurante")
                cConexion.GetRecorset(conectadobd, "SELECT Comanda.NumeroComanda, Comanda.Comenzales, isnull( Mesas.Nombre_Mesa,'Express') as 'Nombre_Mesa' ,isnull( Mesas.Id,0) as IdMesa FROM Comanda left outer JOIN Mesas ON Comanda.IdMesa = Mesas.Id WHERE Comanda.Numerofactura = " & factura, rs)
                While rs.Read
                    numerocomanda = rs("NumeroComanda")
                    comenzales = rs("Comenzales")
                    mesa = rs("Nombre_Mesa")
                    idmesa = rs("IdMesa")
                    cont += 1
                End While
                rs.Close()
                If cont = 0 Then
                    cConexion.GetRecorset(conectadobd, "SELECT NumeroComanda,Comenzales, IdMesa FROM Comanda WHERE Numerofactura = " & factura, rs)
                    While rs.Read
                        numerocomanda = rs("NumeroComanda")
                        comenzales = rs("Comenzales")
                        idmesa = rs("IdMesa")
                        mesa = 0
                    End While
                    rs.Close()
                End If
                cConexion.DesConectar(conectadobd)


                Dim cFacturaVenta As New Factura
                If cont = 0 Then
                    cFacturaVenta.tipo_factura = False
                Else
                    cFacturaVenta.tipo_factura = True
                End If
                cFacturaVenta.nombremesa = mesa
                cFacturaVenta.idmesa = idmesa
                cFacturaVenta.numeroComanda = numerocomanda
                cFacturaVenta.Comenzales = comenzales
                cFacturaVenta.separada = False
                cFacturaVenta.reimprimir = True
                cFacturaVenta.ButtonCambioFV.Visible = True
                cFacturaVenta.NFactura = factura
                cFacturaVenta.idfactura = dgvFacturas(0, dgvFacturas.CurrentRow.Index).Value()
                conectadobd = cConexion.Conectar("Restaurante")
                cFacturaVenta.anular = True
                cConexion.DesConectar(conectadobd)

                Dim dtVentas As New DataTable
                cFunciones.Llenar_Tabla_Generico("Select * from Ventas where Id=" & dgvFacturas(0, dgvFacturas.CurrentRow.Index).Value() & "", dtVentas)

                If dtVentas.Rows.Count > 0 Then
                    cFacturaVenta.txtcliente.Text = dtVentas.Rows(0).Item("Nombre_Cliente")
                    cFacturaVenta.TextBoxDescuento.Text = dtVentas.Rows(0).Item("Descuento")
                    cFacturaVenta.txtSubtotal.Text = dtVentas.Rows(0).Item("SubTotal")
                    cFacturaVenta.txtIV.Text = dtVentas.Rows(0).Item("Imp_Venta")
                    cFacturaVenta.txtIS.Text = dtVentas.Rows(0).Item("Monto_Saloero")
                    cFacturaVenta.cboxMoneda.Text = dtVentas.Rows(0).Item("Cod_Moneda")
                    cFacturaVenta.txtTotal.Text = dtVentas.Rows(0).Item("Total")
                    cFacturaVenta.txtObservacion1.Text = dtVentas.Rows(0).Item("Observacion")

                    If dtVentas.Rows(0).Item("Tipo") = "CON" Then
                        cFacturaVenta.rbContado.Checked = True
                    ElseIf dtVentas.Rows(0).Item("Tipo") = "CRE" Then
                        cFacturaVenta.rbCredito.Checked = True
                    End If
                End If


                cFacturaVenta.GroupBox2.Enabled = False
                cFacturaVenta.txtObservaciones.Enabled = False
                cFacturaVenta.cboxMoneda.Enabled = False

                cFacturaVenta.CedulaU = User_Log.Cedula

                cConexion.Conectar("Restaurante")
                Anula = cConexion.SlqExecuteScalar(cConexion.SqlConexion, "SELECT ANULADO FROM VENTAS WHERE ID =" & cFacturaVenta.idfactura)
                cFacturaVenta.anular = Anula
                cFacturaVenta.ToolBarEliminar.Enabled = Anula
                cFacturaVenta.ToolBarImprimir.Visible = True

                cFacturaVenta.ShowDialog()

            Catch ex As Exception
                MessageBox.Show("Error al anular la factura, verifique la selección del registro. " & vbCrLf & Err.Description, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            End Try
        End If

    End Sub

    Private Sub btHoy_Click(sender As Object, e As EventArgs) Handles btHoy.Click
        Try
            Me.dtpDesde.Value = Date.Now
            Me.dtpHasta.Value = Date.Now

            Me.BuscarFactura()
            PasarDatos(dtGeneral)
        Catch ex As Exception
            MsgBox("Error al buscar facturas de hoy. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btAyer_Click(sender As Object, e As EventArgs) Handles btAyer.Click
        Try

            Me.dtpDesde.Value = Date.Now.Subtract(TimeSpan.FromDays(1))
            Me.dtpHasta.Value = Date.Now.Subtract(TimeSpan.FromDays(1))

            Me.BuscarFactura()
            PasarDatos(dtGeneral)
        Catch ex As Exception
            MsgBox("Error al buscar facturas del día de ayer. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btSemanaPadada_Click(sender As Object, e As EventArgs) Handles btSemanaPadada.Click
        Try

            Me.dtpDesde.Value = Date.Now.Subtract(TimeSpan.FromDays(7))
            Me.dtpHasta.Value = Date.Now

            Me.BuscarFactura()
            PasarDatos(dtGeneral)
        Catch ex As Exception
            MsgBox("Error al buscar facturas de la semana pasada. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtBuscar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyUp
        BuscarFactura()
        PasarDatos(dtGeneral)
    End Sub

    Private Sub btReporte_Click(sender As Object, e As EventArgs) Handles btReporte.Click

        PasarDatos(dtGeneral)
        rptFacturas()
    End Sub

    Public Sub PasarDatos(ByVal dt As DataTable)

        Try
            DtsRepartidores1.dtFacturas.Clear()


            For b As Integer = 0 To dt.Rows.Count - 1
                With bsFacturas
                    .AddNew()
                    .Current("Id") = dt.Rows(b).ItemArray(0)
                    .Current("Fecha") = dt.Rows(b).ItemArray(1)
                    .Current("Inicial") = dt.Rows(b).ItemArray(2)
                    .Current("Factura") = dt.Rows(b).ItemArray(3)
                    .Current("Cliente") = dt.Rows(b).ItemArray(4)
                    .Current("Direccion") = dt.Rows(b).ItemArray(5)
                    .Current("Repartidor") = dt.Rows(b).ItemArray(6)
                    .Current("TotalSinFlete") = dt.Rows(b).ItemArray(7)
                    .Current("Flete") = dt.Rows(b).ItemArray(8)
                    .Current("TotalGeneral") = dt.Rows(b).ItemArray(9)
                    .Current("Estado") = dt.Rows(b).ItemArray(11)
                    .Current("Pago") = dt.Rows(b).ItemArray(12)
                    .Current("IVA") = dt.Rows(b).ItemArray(13)
                    .EndEdit()
                End With
            Next


            dtGeneral.Rows.Clear()
            dtGeneral.Columns.Clear()
            dtGeneral.Clear()

            dtGeneral = dt.Copy()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub rptFacturas()
        Try
            Dim rpt As New rptFacturasRepartidor
            Dim impresora As String
            impresora = Busca_Impresora()
            If impresora = "" Then
                MessageBox.Show("No se seleccionó ninguna impresora", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                impresora = Busca_Impresora()
                If impresora = "" Then
                    Exit Sub
                End If
            End If
            rpt.Refresh()
            rpt.PrintOptions.PrinterName = impresora
            rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            rpt.SetDataSource(DtsRepartidores1)
            rpt.Subreports(0).SetDataSource(glodtsGlobal)
            rpt.SetParameterValue("Desde", Convert.ToDateTime(dtpDesde.Value))
            rpt.SetParameterValue("Hasta", Convert.ToDateTime(dtpHasta.Value))
            rpt.PrintToPrinter(1, True, 0, 0)
            rpt.Dispose()
            rpt.Close()
            rpt = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
            cFunciones.spEnviar_Correo(Me.Name, "Imprimir Reporte Repartidores.", ex.Message)
        End Try
    End Sub

    Sub CambiarEstado(ByVal NumFactura As Integer, ByVal Estado As Integer)
        Try
            Dim cx As New Conexion
            Dim Sql As SqlCommand

            If Estado = 1 Then
                Sql = New SqlCommand("Update tb_RepartidorAsignado set Estado=1 where IdFactura=@NumFactura")
            Else
                Sql = New SqlCommand("Update tb_RepartidorAsignado set Estado=0 where IdFactura=@NumFactura")
            End If

            Sql.Parameters.AddWithValue("@NumFactura", NumFactura)

            cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception
            MsgBox("Error al cambiar de estado la factura. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub Pagar(ByVal _dt As DataTable)
        Try
            For p As Integer = 0 To _dt.Rows.Count - 1
                If _dt.Rows(p).ItemArray(11) = True Then
                    CambiarEstado(_dt.Rows(p).ItemArray(3), 1)
                Else
                    CambiarEstado(_dt.Rows(p).ItemArray(3), 0)
                End If
            Next

            MsgBox("Facturas pagadas.", MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox("Error al pagar facturas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub SolicitarClave()
        Try
ReintentarClave:

            Dim rEmpleado0 As New registro
            rEmpleado0.Text = "DIGITE CONTRASEÑA"
            rEmpleado0.txtCodigo.PasswordChar = "*"

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO

            rEmpleado0.ShowDialog()
            clave = rEmpleado0.txtCodigo.Text

            '---------------------------------------------------------------
            rEmpleado0.Dispose()
            If rEmpleado0.iOpcion = 0 Then Exit Sub

            If clave <> "" Then
                cedula = cConexion.SlqExecuteScalar(cConexion.Conectar(), "Select Cedula from Usuarios WITH (NOLOCK) where Clave_Interna='" & clave & "'")
                If Trim(cedula) = "" Or Trim(cedula) = vbNullString Or rEmpleado0.iOpcion = 0 Then
                    MsgBox("La clave ingresada es incorrecta..", MsgBoxStyle.Information, "Atención...")
                    GoTo ReintentarClave
                    Exit Sub
                End If
            Else
                GoTo ReintentarClave
            End If
            PasarGridADataTable()
            Pagar(dtGeneral)
            BuscarFactura()
            PasarDatos(dtGeneral)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub btPagar_Click(sender As Object, e As EventArgs) Handles btPagar.Click
        SolicitarClave()
    End Sub

    Private Sub dgvFacturas_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvFacturas.CellFormatting
        Try
            If dgvFacturas.Columns(e.ColumnIndex).Name = "EstadoDataGridViewCheckBoxColumn" Then
                If e.Value IsNot Nothing Then
                    If e.Value.ToString() = "True" Then
                        dgvFacturas.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleGreen
                        e.Value = "Pagado"
                    Else
                        dgvFacturas.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                        e.Value = "Sin Pagar"
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub PasarGridADataTable()
        Try
            Dim dtFiltrados As New DataTable

            For Each col As DataGridViewColumn In dgvFacturas.Columns
                Dim columna As New DataColumn(col.Name, Type.GetType("System.String"))
                dtFiltrados.Columns.Add(columna)
            Next

            For Each viewRow As DataGridViewRow In dgvFacturas.Rows
                Dim row As DataRow = dtFiltrados.NewRow()

                For Each col As DataGridViewColumn In dgvFacturas.Columns
                    Dim value As Object = viewRow.Cells(col.Name).Value
                    row.Item(col.Name) = If(value Is Nothing, 0, value)
                Next col
                dtFiltrados.Rows.Add(row)
            Next viewRow

            dtGeneral.Rows.Clear()
            dtGeneral.Columns.Clear()
            dtGeneral.Clear()

            dtGeneral = dtFiltrados.Copy()
        Catch ex As Exception

        End Try
    End Sub



    Private Sub dgvFacturas_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles dgvFacturas.RowPostPaint
        Try
            PasarGridADataTable()
            CalcularTotal(dtGeneral)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ckMarcar_CheckedChanged(sender As Object, e As EventArgs) Handles ckMarcar.CheckedChanged
        Try
            Dim dtCheck As New DataTable
            dtCheck.Clear()

            dtCheck = dtGeneral.Copy()

            If dtCheck.Rows.Count > 0 Then

                If ckMarcar.Checked Then 'Marca o desmarca los check de la columna de estados
                    For Each fila As DataRow In dtCheck.Rows
                        fila.SetField("Estado", True)
                    Next
                Else
                    For Each fila As DataRow In dtCheck.Rows
                        fila.SetField("Estado", False)
                    Next
                End If
                PasarDatos(dtCheck)
            End If

        Catch ex As Exception

        End Try
    End Sub


    ' EN ESTE EVENTO SE AGREGA EL EVENTO DEL KEYPRESS A LA COLUMNA DE TIPO TEXTBOX "CODIGO", LAS OTRAS COLUMAS SON DE OTRO TIPO
    Private Sub dgvFacturas_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dgvFacturas.EditingControlShowing
        Try
            If TypeOf e.Control Is System.Windows.Forms.TextBox Then
                Dim validar As TextBox = CType(e.Control, TextBox)
                ' Agregar el controlador de eventos para el KeyPress  
                AddHandler validar.KeyPress, AddressOf validar_keypress
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub validar_keypress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Try
            Dim dtIniciales As New DataTable
            Dim Iniciales As Char()

            dtIniciales.Clear()

            If dgvFacturas.CurrentCell.ColumnIndex <> 12 Then
                cFunciones.Llenar_Tabla_Generico("Select Id,Inicial from tb_Repartidores where estado=1", dtIniciales)

                If dtIniciales.Rows.Count > 0 Then

                    For a As Integer = 0 To dtIniciales.Rows.Count - 1
                        Iniciales += dtIniciales.Rows(a).Item("Inicial").ToString().Trim().ToCharArray()
                    Next

                    For f As Integer = 0 To Iniciales.Length - 1
                        If Not e.KeyChar.ToString().ToUpper().Contains(Iniciales(f)) And Not Char.IsControl(e.KeyChar) Then
                            e.Handled = True
                        Else
                            e.Handled = False
                            Exit For
                        End If
                    Next
                End If
            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvFacturas_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFacturas.CellEnter
        dgvFacturas.BeginEdit(False)
    End Sub

    Sub BuscarRepartidor(ByVal _dt As DataTable)
        Try

            If dgvFacturas.CurrentRow.Cells(2).Value.ToString().Trim() <> "" Then
                AsignarRepartidor(dgvFacturas.CurrentRow.Cells(2).Value, dgvFacturas.CurrentRow.Cells(3).Value)
            Else
                DesligarRepartidor(dgvFacturas.CurrentRow.Cells(3).Value)
            End If

        Catch ex As Exception
            MsgBox("Error al asignar repartidor. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub DesligarRepartidor(ByVal NumFactura As Integer)
        Try
            Dim cx As New Conexion
            Dim Sql As SqlCommand

            Sql = New SqlCommand("Update tb_RepartidorAsignado set IdRepartidor=0 where Idfactura=@NumFactura")

            Sql.Parameters.AddWithValue("@NumFactura", NumFactura)

            cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception

        End Try
    End Sub

    Sub AsignarRepartidor(ByVal Inicial As String, ByVal NumFactura As Integer)
        Try
            Dim cx As New Conexion
            Dim Sql As SqlCommand
            Dim dtInfoRepartidor As New DataTable
            Dim IdRepartidor As Integer = 0

            cFunciones.Llenar_Tabla_Generico("Select Id from tb_Repartidores where Inicial='" & Inicial & "'", dtInfoRepartidor)

            If dtInfoRepartidor.Rows.Count > 0 Then
                IdRepartidor = dtInfoRepartidor.Rows(0).Item("Id")
            End If

            Sql = New SqlCommand("Update tb_RepartidorAsignado set IdRepartidor=@IdRepartidor where Idfactura=@NumFactura")

            Sql.Parameters.AddWithValue("@NumFactura", NumFactura)
            Sql.Parameters.AddWithValue("@IdRepartidor", IdRepartidor)

            cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
            cx.DesConectar(cx.sQlconexion)

        Catch ex As Exception

        End Try
    End Sub


    Private Sub dgvFacturas_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFacturas.CellValueChanged
        If PrimeraCarga <> 0 Then
            If e.ColumnIndex() = 2 Then
                PasarGridADataTable()
                BuscarRepartidor(dtGeneral)
                BuscarFactura()
                PasarDatos(dtGeneral)
            End If

            If e.ColumnIndex() = 12 Then
                PasarGridADataTable()
                ValidarTipoDePago(dtGeneral)
                BuscarFactura()
                PasarDatos(dtGeneral)
            End If
        End If
    End Sub

    Sub ValidarTipoDePago(ByVal _dt As DataTable)
        Try

            If dgvFacturas.CurrentRow.Cells(12).Value.ToString().Trim().ToUpper() = "EFE" Or dgvFacturas.CurrentRow.Cells(12).Value.ToString().Trim().ToUpper() = "SINPE" Or _dgvFacturas.CurrentRow.Cells(12).Value.ToString().Trim().ToUpper() = "TAR" Or dgvFacturas.CurrentRow.Cells(12).Value.ToString().Trim().ToUpper() = "CRE" Then
                ActualizarTipoDePago(dgvFacturas.CurrentRow.Cells(3).Value, dgvFacturas.CurrentRow.Cells(12).Value)
            End If

        Catch ex As Exception
            MsgBox("Error al validar tipo de pago. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub ActualizarTipoDePago(ByVal NumFactura As Integer, ByVal TipoPago As String)
        Try
            Dim cx As New Conexion
            Dim Sql As SqlCommand
            Dim dtOpcionesDepago As New DataTable
            Dim IdOpcionesDePago As Integer = 0
            dtOpcionesDepago.Clear()

            cFunciones.Llenar_Tabla_Generico("Select Id from OpcionesDePago where Documento=" & NumFactura & "", dtOpcionesDepago)

            If dtOpcionesDepago.Rows.Count > 0 Then

                Sql = New SqlCommand("Update OpcionesDePago set FormaPago=@FormaDePago where Id=@Id")

                Sql.Parameters.AddWithValue("@FormaDePago", TipoPago.ToUpper())
                Sql.Parameters.AddWithValue("@Id", dtOpcionesDepago.Rows(0).Item("Id"))

                cFunciones.spEjecutar(Sql, GetSetting("SeeSOFT", "Restaurante", "CONEXION"))
                cx.DesConectar(cx.sQlconexion)
            End If
        Catch ex As Exception
            MsgBox("Error al actualizar tipo de pago. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

End Class