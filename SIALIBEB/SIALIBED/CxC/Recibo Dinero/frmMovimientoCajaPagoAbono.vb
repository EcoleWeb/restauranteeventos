Imports System.data.SqlClient

Public Class frmMovimientoCajaPagoAbono2
    Inherits System.Windows.Forms.Form
    Public TipoCambioDolar As Double = 0
    Public TipoCambioRecibo As Double = 0
    Public Trans As SqlTransaction
    Public monedaVuelto As Integer
    Dim estaMalo As Boolean = False
    Public Total As Double
    Public Factura As Double
    Public fecha As DateTime
    Public Tipo As String
    Public codmod As Integer
    Dim loSaqueAqui As Boolean = False
    Public Shared Numero_Apertura As Long
    Public Shared NApertura As Long
    Public cargando As Boolean
    Public Conversion As Double 'almacena el valor de conversion entre la moneda de la factura y la modeda en que se va a hacer el pago
    Public TipoCambio_Factura As Double
    Public mon As DataRow()
    Dim ds As New DataSet
    Public mode As DataRow
    Public Tipo_CambioOpciones As Double
    Public Seleccionado As Integer
    Public Forma_pago As Integer
    Public Registra As Boolean = False
    Public bandera As Boolean = False
    Public vuelto As Double
    Public cedu As String
    Public nombre As String
    Public Prepago As Boolean = False
    Dim incializando As Boolean = True
    Public cambioFP As Integer = 0
    Public conexion As String = ""
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        AddHandler Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").PositionChanged, AddressOf Me.Position_Changed
    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtauxcoin As System.Windows.Forms.TextBox
    Friend WithEvents txtauxtipocambio As System.Windows.Forms.TextBox
    Friend WithEvents txtformapago As System.Windows.Forms.TextBox
    Friend WithEvents txttipofact As System.Windows.Forms.TextBox
    Friend WithEvents txtfactura As System.Windows.Forms.TextBox
    Friend WithEvents txttotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblvuelto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblmontopagado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblmontofact As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gbmoneda As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtranferencia As System.Windows.Forms.RadioButton
    Friend WithEvents rbcheque As System.Windows.Forms.RadioButton
    Friend WithEvents rbtarjeta As System.Windows.Forms.RadioButton
    Friend WithEvents rbefectivo As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarExcel As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Public WithEvents dadetalleopcionpago As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents damoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtreftipo As System.Windows.Forms.TextBox
    Public WithEvents daopcionpago As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtauxformapago As System.Windows.Forms.TextBox
    Friend WithEvents txtauxtipodocumento As System.Windows.Forms.TextBox
    Friend WithEvents txtfecha As System.Windows.Forms.TextBox
    Friend WithEvents txtcodcoin As System.Windows.Forms.TextBox
    Friend WithEvents txttc As System.Windows.Forms.TextBox
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents dgdetopcpago As DevExpress.XtraGrid.GridControl

    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtmonto As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtnommoneda As System.Windows.Forms.TextBox
    Friend WithEvents colDenominacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipoCambio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombremoneda As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontoPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFormaPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtNumApertura As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Public WithEvents DataSet_Opciones_Pago1 As DataSet_Opciones_Pago1
    Friend WithEvents Adapter_apertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colDocumento1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipoDocumento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontoPago1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFormaPago1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDenominacion1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombremoneda1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFormaPago2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDocumento2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReferenciaTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReferenciaDoc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents label_Tipo_Cambio As System.Windows.Forms.Label
    Friend WithEvents TxtDocu As System.Windows.Forms.TextBox
    Friend WithEvents cbomoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtdenominacion As System.Windows.Forms.TextBox
    Friend WithEvents Monto_Su_Moneda As System.Windows.Forms.Label
    Friend WithEvents Pagado_Su_Moneda As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TxtMontoPagar_Sumoneda As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TxtReferenciaTipo As System.Windows.Forms.TextBox
    Friend WithEvents TxtDocumentoF As System.Windows.Forms.TextBox
    Friend WithEvents TxtReferenciaF As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox_Detalle_Pago As System.Windows.Forms.GroupBox
    Friend WithEvents Label_RefTipo As System.Windows.Forms.Label
    Friend WithEvents Label_Referencia As System.Windows.Forms.Label
    Friend WithEvents Label_Documento As System.Windows.Forms.Label
    Friend WithEvents Label_RefDocumento As System.Windows.Forms.Label
    Friend WithEvents Combo_Bancos As System.Windows.Forms.ComboBox
    Friend WithEvents Combo_Tarjeta As System.Windows.Forms.ComboBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents AdapterTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents TxtVoucher As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Adapter_Deposito As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents LabelConvertir As System.Windows.Forms.Label
    Friend WithEvents ComboBoxConvertir As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMovimientoCajaPagoAbono2))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo8 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo9 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo10 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo11 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo12 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo13 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo14 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo15 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo16 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo17 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo18 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Me.txtauxcoin = New System.Windows.Forms.TextBox
        Me.DataSet_Opciones_Pago1 = New SIALIBEB.DataSet_Opciones_Pago1
        Me.txtauxtipocambio = New System.Windows.Forms.TextBox
        Me.txtformapago = New System.Windows.Forms.TextBox
        Me.txttipofact = New System.Windows.Forms.TextBox
        Me.txtfactura = New System.Windows.Forms.TextBox
        Me.txttotal = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblvuelto = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblmontopagado = New DevExpress.XtraEditors.TextEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblmontofact = New DevExpress.XtraEditors.TextEdit
        Me.gbmoneda = New System.Windows.Forms.GroupBox
        Me.cbomoneda = New System.Windows.Forms.ComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbtranferencia = New System.Windows.Forms.RadioButton
        Me.rbcheque = New System.Windows.Forms.RadioButton
        Me.rbtarjeta = New System.Windows.Forms.RadioButton
        Me.rbefectivo = New System.Windows.Forms.RadioButton
        Me.Combo_Tarjeta = New System.Windows.Forms.ComboBox
        Me.GroupBox_Detalle_Pago = New System.Windows.Forms.GroupBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.TxtVoucher = New System.Windows.Forms.TextBox
        Me.Label_RefTipo = New System.Windows.Forms.Label
        Me.Label_Referencia = New System.Windows.Forms.Label
        Me.TxtReferenciaTipo = New System.Windows.Forms.TextBox
        Me.Combo_Bancos = New System.Windows.Forms.ComboBox
        Me.TxtDocumentoF = New System.Windows.Forms.TextBox
        Me.TxtReferenciaF = New System.Windows.Forms.TextBox
        Me.Label_Documento = New System.Windows.Forms.Label
        Me.Label_RefDocumento = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarExcel = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.dadetalleopcionpago = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.damoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.txtreftipo = New System.Windows.Forms.TextBox
        Me.daopcionpago = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.TxtDocu = New System.Windows.Forms.TextBox
        Me.txtdenominacion = New System.Windows.Forms.TextBox
        Me.txtauxformapago = New System.Windows.Forms.TextBox
        Me.txtauxtipodocumento = New System.Windows.Forms.TextBox
        Me.txtfecha = New System.Windows.Forms.TextBox
        Me.txtcodcoin = New System.Windows.Forms.TextBox
        Me.txttc = New System.Windows.Forms.TextBox
        Me.dgdetopcpago = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFormaPago2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colReferencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDocumento2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colReferenciaTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colReferenciaDoc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtnommoneda = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.txtmonto = New System.Windows.Forms.TextBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtNumApertura = New System.Windows.Forms.TextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.colDenominacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTipoCambio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNombremoneda = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMontoPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colFormaPago = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colDocumento1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTipoDocumento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMontoPago1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colFormaPago1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDenominacion1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNombremoneda1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label39 = New System.Windows.Forms.Label
        Me.Adapter_apertura = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.label_Tipo_Cambio = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Monto_Su_Moneda = New System.Windows.Forms.Label
        Me.Pagado_Su_Moneda = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.TxtMontoPagar_Sumoneda = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.AdapterTarjeta = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.Adapter_Deposito = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.LabelConvertir = New System.Windows.Forms.Label
        Me.ComboBoxConvertir = New System.Windows.Forms.ComboBox
        CType(Me.DataSet_Opciones_Pago1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txttotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblvuelto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblmontopagado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblmontofact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbmoneda.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox_Detalle_Pago.SuspendLayout()
        CType(Me.dgdetopcpago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtauxcoin
        '
        Me.txtauxcoin.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.Moneda", True))
        Me.txtauxcoin.Location = New System.Drawing.Point(192, 64)
        Me.txtauxcoin.Name = "txtauxcoin"
        Me.txtauxcoin.Size = New System.Drawing.Size(56, 20)
        Me.txtauxcoin.TabIndex = 66
        Me.txtauxcoin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DataSet_Opciones_Pago1
        '
        Me.DataSet_Opciones_Pago1.DataSetName = "DataSet_Opciones_Pago"
        Me.DataSet_Opciones_Pago1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSet_Opciones_Pago1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtauxtipocambio
        '
        Me.txtauxtipocambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.TipoCambio", True))
        Me.txtauxtipocambio.Location = New System.Drawing.Point(264, 64)
        Me.txtauxtipocambio.Name = "txtauxtipocambio"
        Me.txtauxtipocambio.Size = New System.Drawing.Size(72, 20)
        Me.txtauxtipocambio.TabIndex = 67
        Me.txtauxtipocambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtformapago
        '
        Me.txtformapago.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.FormaPago", True))
        Me.txtformapago.Location = New System.Drawing.Point(16, 64)
        Me.txtformapago.Name = "txtformapago"
        Me.txtformapago.Size = New System.Drawing.Size(128, 20)
        Me.txtformapago.TabIndex = 65
        '
        'txttipofact
        '
        Me.txttipofact.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txttipofact.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.TipoFactura", True))
        Me.txttipofact.ForeColor = System.Drawing.Color.Blue
        Me.txttipofact.Location = New System.Drawing.Point(88, 32)
        Me.txttipofact.Name = "txttipofact"
        Me.txttipofact.Size = New System.Drawing.Size(80, 13)
        Me.txttipofact.TabIndex = 57
        Me.txttipofact.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtfactura
        '
        Me.txtfactura.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtfactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.NumeroFactura", True))
        Me.txtfactura.ForeColor = System.Drawing.Color.Blue
        Me.txtfactura.Location = New System.Drawing.Point(16, 32)
        Me.txtfactura.Name = "txtfactura"
        Me.txtfactura.Size = New System.Drawing.Size(56, 13)
        Me.txtfactura.TabIndex = 55
        Me.txtfactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttotal
        '
        Me.txttotal.EditValue = 0
        Me.txttotal.Location = New System.Drawing.Point(128, 107)
        Me.txttotal.Name = "txttotal"
        '
        '
        '
        Me.txttotal.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txttotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txttotal.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txttotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txttotal.Properties.Enabled = False
        Me.txttotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txttotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txttotal.Size = New System.Drawing.Size(216, 41)
        Me.txttotal.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(88, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Tipo Factura"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Location = New System.Drawing.Point(16, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 16)
        Me.Label9.TabIndex = 56
        Me.Label9.Text = "Factura"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(528, 305)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(192, 16)
        Me.Label3.TabIndex = 87
        Me.Label3.Text = "Vuelto"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblvuelto
        '
        Me.lblvuelto.EditValue = "0"
        Me.lblvuelto.Location = New System.Drawing.Point(528, 321)
        Me.lblvuelto.Name = "lblvuelto"
        '
        '
        '
        Me.lblvuelto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.lblvuelto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblvuelto.Properties.EditFormat.FormatString = "#,#0.00"
        Me.lblvuelto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblvuelto.Properties.ReadOnly = True
        Me.lblvuelto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Khaki, System.Drawing.Color.Blue)
        Me.lblvuelto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblvuelto.Size = New System.Drawing.Size(192, 38)
        Me.lblvuelto.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(8, 306)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 16)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "Monto Pagado"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblmontopagado
        '
        Me.lblmontopagado.EditValue = "0"
        Me.lblmontopagado.Location = New System.Drawing.Point(8, 322)
        Me.lblmontopagado.Name = "lblmontopagado"
        '
        '
        '
        Me.lblmontopagado.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.lblmontopagado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblmontopagado.Properties.EditFormat.FormatString = "#,#0.00"
        Me.lblmontopagado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblmontopagado.Properties.ReadOnly = True
        Me.lblmontopagado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Khaki, System.Drawing.Color.Blue)
        Me.lblmontopagado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblmontopagado.Size = New System.Drawing.Size(192, 38)
        Me.lblmontopagado.TabIndex = 84
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label10.Location = New System.Drawing.Point(208, 306)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(192, 16)
        Me.Label10.TabIndex = 81
        Me.Label10.Text = "Monto Total Factura"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblmontofact
        '
        Me.lblmontofact.EditValue = "0"
        Me.lblmontofact.Location = New System.Drawing.Point(208, 322)
        Me.lblmontofact.Name = "lblmontofact"
        '
        '
        '
        Me.lblmontofact.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.lblmontofact.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblmontofact.Properties.EditFormat.FormatString = "#,#0.00"
        Me.lblmontofact.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblmontofact.Properties.ReadOnly = True
        Me.lblmontofact.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Khaki, System.Drawing.Color.Blue)
        Me.lblmontofact.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblmontofact.Size = New System.Drawing.Size(192, 38)
        Me.lblmontofact.TabIndex = 80
        '
        'gbmoneda
        '
        Me.gbmoneda.Controls.Add(Me.cbomoneda)
        Me.gbmoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbmoneda.ForeColor = System.Drawing.Color.MidnightBlue
        Me.gbmoneda.Location = New System.Drawing.Point(123, 33)
        Me.gbmoneda.Name = "gbmoneda"
        Me.gbmoneda.Size = New System.Drawing.Size(136, 48)
        Me.gbmoneda.TabIndex = 63
        Me.gbmoneda.TabStop = False
        Me.gbmoneda.Text = "Moneda"
        '
        'cbomoneda
        '
        Me.cbomoneda.DataSource = Me.DataSet_Opciones_Pago1
        Me.cbomoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.cbomoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbomoneda.Location = New System.Drawing.Point(8, 16)
        Me.cbomoneda.Name = "cbomoneda"
        Me.cbomoneda.Size = New System.Drawing.Size(120, 21)
        Me.cbomoneda.TabIndex = 0
        Me.cbomoneda.ValueMember = "Moneda.CodMoneda"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbtranferencia)
        Me.GroupBox1.Controls.Add(Me.rbcheque)
        Me.GroupBox1.Controls.Add(Me.rbtarjeta)
        Me.GroupBox1.Controls.Add(Me.rbefectivo)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(8, 33)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(112, 120)
        Me.GroupBox1.TabIndex = 50
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Forma Pago"
        '
        'rbtranferencia
        '
        Me.rbtranferencia.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtranferencia.Location = New System.Drawing.Point(8, 88)
        Me.rbtranferencia.Name = "rbtranferencia"
        Me.rbtranferencia.Size = New System.Drawing.Size(96, 24)
        Me.rbtranferencia.TabIndex = 54
        Me.rbtranferencia.Text = "Transferencia"
        '
        'rbcheque
        '
        Me.rbcheque.AccessibleName = ""
        Me.rbcheque.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbcheque.Location = New System.Drawing.Point(8, 64)
        Me.rbcheque.Name = "rbcheque"
        Me.rbcheque.Size = New System.Drawing.Size(80, 24)
        Me.rbcheque.TabIndex = 53
        Me.rbcheque.Text = "Cheque"
        '
        'rbtarjeta
        '
        Me.rbtarjeta.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtarjeta.Location = New System.Drawing.Point(8, 40)
        Me.rbtarjeta.Name = "rbtarjeta"
        Me.rbtarjeta.Size = New System.Drawing.Size(80, 24)
        Me.rbtarjeta.TabIndex = 52
        Me.rbtarjeta.Text = "Tarjeta"
        '
        'rbefectivo
        '
        Me.rbefectivo.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbefectivo.Location = New System.Drawing.Point(8, 17)
        Me.rbefectivo.Name = "rbefectivo"
        Me.rbefectivo.Size = New System.Drawing.Size(80, 24)
        Me.rbefectivo.TabIndex = 51
        Me.rbefectivo.Text = "Efectivo"
        '
        'Combo_Tarjeta
        '
        Me.Combo_Tarjeta.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.ReferenciaTipo", True))
        Me.Combo_Tarjeta.DataSource = Me.DataSet_Opciones_Pago1
        Me.Combo_Tarjeta.DisplayMember = "TipoTarjeta.Nombre"
        Me.Combo_Tarjeta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Tarjeta.ForeColor = System.Drawing.Color.Blue
        Me.Combo_Tarjeta.Location = New System.Drawing.Point(112, 72)
        Me.Combo_Tarjeta.Name = "Combo_Tarjeta"
        Me.Combo_Tarjeta.Size = New System.Drawing.Size(184, 21)
        Me.Combo_Tarjeta.TabIndex = 3
        Me.Combo_Tarjeta.ValueMember = "TipoTarjeta.Id"
        Me.Combo_Tarjeta.Visible = False
        '
        'GroupBox_Detalle_Pago
        '
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.ComboBox1)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.TxtVoucher)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Combo_Tarjeta)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Label_RefTipo)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Label_Referencia)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.TxtReferenciaTipo)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Combo_Bancos)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.TxtDocumentoF)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.TxtReferenciaF)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Label_Documento)
        Me.GroupBox_Detalle_Pago.Controls.Add(Me.Label_RefDocumento)
        Me.GroupBox_Detalle_Pago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox_Detalle_Pago.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox_Detalle_Pago.Location = New System.Drawing.Point(8, 168)
        Me.GroupBox_Detalle_Pago.Name = "GroupBox_Detalle_Pago"
        Me.GroupBox_Detalle_Pago.Size = New System.Drawing.Size(304, 128)
        Me.GroupBox_Detalle_Pago.TabIndex = 0
        Me.GroupBox_Detalle_Pago.TabStop = False
        Me.GroupBox_Detalle_Pago.Text = "Detalle Forma Pago"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.cuentaBancaria", True))
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox1.Location = New System.Drawing.Point(112, 96)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(184, 21)
        Me.ComboBox1.TabIndex = 150
        Me.ComboBox1.Visible = False
        '
        'TxtVoucher
        '
        Me.TxtVoucher.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtVoucher.ForeColor = System.Drawing.Color.Blue
        Me.TxtVoucher.Location = New System.Drawing.Point(112, 96)
        Me.TxtVoucher.Name = "TxtVoucher"
        Me.TxtVoucher.Size = New System.Drawing.Size(184, 20)
        Me.TxtVoucher.TabIndex = 149
        Me.TxtVoucher.Visible = False
        '
        'Label_RefTipo
        '
        Me.Label_RefTipo.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label_RefTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_RefTipo.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label_RefTipo.Location = New System.Drawing.Point(12, 72)
        Me.Label_RefTipo.Name = "Label_RefTipo"
        Me.Label_RefTipo.Size = New System.Drawing.Size(96, 16)
        Me.Label_RefTipo.TabIndex = 127
        Me.Label_RefTipo.Text = "Referencia Tipo"
        Me.Label_RefTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Referencia
        '
        Me.Label_Referencia.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label_Referencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Referencia.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label_Referencia.Location = New System.Drawing.Point(20, 24)
        Me.Label_Referencia.Name = "Label_Referencia"
        Me.Label_Referencia.Size = New System.Drawing.Size(88, 16)
        Me.Label_Referencia.TabIndex = 128
        Me.Label_Referencia.Text = "Referencia"
        Me.Label_Referencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TxtReferenciaTipo
        '
        Me.TxtReferenciaTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtReferenciaTipo.ForeColor = System.Drawing.Color.Blue
        Me.TxtReferenciaTipo.Location = New System.Drawing.Point(112, 72)
        Me.TxtReferenciaTipo.Name = "TxtReferenciaTipo"
        Me.TxtReferenciaTipo.Size = New System.Drawing.Size(184, 20)
        Me.TxtReferenciaTipo.TabIndex = 2
        '
        'Combo_Bancos
        '
        Me.Combo_Bancos.DataSource = Me.DataSet_Opciones_Pago1.Bancos
        Me.Combo_Bancos.DisplayMember = "Descripcion"
        Me.Combo_Bancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Bancos.ForeColor = System.Drawing.Color.Blue
        Me.Combo_Bancos.Location = New System.Drawing.Point(112, 96)
        Me.Combo_Bancos.Name = "Combo_Bancos"
        Me.Combo_Bancos.Size = New System.Drawing.Size(184, 21)
        Me.Combo_Bancos.TabIndex = 3
        Me.Combo_Bancos.ValueMember = "Codigo_banco"
        '
        'TxtDocumentoF
        '
        Me.TxtDocumentoF.AcceptsTab = True
        Me.TxtDocumentoF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtDocumentoF.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.Documento", True))
        Me.TxtDocumentoF.ForeColor = System.Drawing.Color.Blue
        Me.TxtDocumentoF.Location = New System.Drawing.Point(112, 48)
        Me.TxtDocumentoF.Name = "TxtDocumentoF"
        Me.TxtDocumentoF.Size = New System.Drawing.Size(184, 20)
        Me.TxtDocumentoF.TabIndex = 1
        '
        'TxtReferenciaF
        '
        Me.TxtReferenciaF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtReferenciaF.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.Referencia", True))
        Me.TxtReferenciaF.ForeColor = System.Drawing.Color.Blue
        Me.TxtReferenciaF.Location = New System.Drawing.Point(112, 24)
        Me.TxtReferenciaF.Name = "TxtReferenciaF"
        Me.TxtReferenciaF.Size = New System.Drawing.Size(184, 20)
        Me.TxtReferenciaF.TabIndex = 0
        '
        'Label_Documento
        '
        Me.Label_Documento.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label_Documento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Documento.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label_Documento.Location = New System.Drawing.Point(20, 48)
        Me.Label_Documento.Name = "Label_Documento"
        Me.Label_Documento.Size = New System.Drawing.Size(88, 16)
        Me.Label_Documento.TabIndex = 130
        Me.Label_Documento.Text = "Documento"
        Me.Label_Documento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_RefDocumento
        '
        Me.Label_RefDocumento.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label_RefDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_RefDocumento.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label_RefDocumento.Location = New System.Drawing.Point(11, 99)
        Me.Label_RefDocumento.Name = "Label_RefDocumento"
        Me.Label_RefDocumento.Size = New System.Drawing.Size(96, 16)
        Me.Label_RefDocumento.TabIndex = 129
        Me.Label_RefDocumento.Text = "Referencia Doc"
        Me.Label_RefDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Image = CType(resources.GetObject("Label2.Image"), System.Drawing.Image)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(736, 32)
        Me.Label2.TabIndex = 103
        Me.Label2.Text = "Movimiento de Pago"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.VarChar, 50, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.BigInt, 8, "id")})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha")})
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing)})
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "")
        Me.ImageList2.Images.SetKeyName(1, "")
        Me.ImageList2.Images.SetKeyName(2, "")
        Me.ImageList2.Images.SetKeyName(3, "")
        Me.ImageList2.Images.SetKeyName(4, "")
        Me.ImageList2.Images.SetKeyName(5, "")
        Me.ImageList2.Images.SetKeyName(6, "")
        Me.ImageList2.Images.SetKeyName(7, "")
        Me.ImageList2.Images.SetKeyName(8, "")
        Me.ImageList2.Images.SetKeyName(9, "")
        Me.ImageList2.Images.SetKeyName(10, "")
        Me.ImageList2.Images.SetKeyName(11, "")
        Me.ImageList2.Images.SetKeyName(12, "")
        Me.ImageList2.Images.SetKeyName(13, "")
        Me.ImageList2.Images.SetKeyName(14, "")
        Me.ImageList2.Images.SetKeyName(15, "")
        Me.ImageList2.Images.SetKeyName(16, "")
        Me.ImageList2.Images.SetKeyName(17, "")
        Me.ImageList2.Images.SetKeyName(18, "")
        Me.ImageList2.Images.SetKeyName(19, "")
        Me.ImageList2.Images.SetKeyName(20, "")
        Me.ImageList2.Images.SetKeyName(21, "")
        Me.ImageList2.Images.SetKeyName(22, "")
        Me.ImageList2.Images.SetKeyName(23, "")
        Me.ImageList2.Images.SetKeyName(24, "")
        Me.ImageList2.Images.SetKeyName(25, "")
        Me.ImageList2.Images.SetKeyName(26, "")
        Me.ImageList2.Images.SetKeyName(27, "")
        Me.ImageList2.Images.SetKeyName(28, "")
        Me.ImageList2.Images.SetKeyName(29, "")
        Me.ImageList2.Images.SetKeyName(30, "")
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.VarChar, 50, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTi" & _
            "po, ReferenciaDoc, Moneda, TipoCambio, Id FROM Detalle_pago_caja"
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Usuario, Nombre, CodMo" & _
            "neda, TipoCambio, Fecha FROM OpcionesDePago"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        Me.ImageList1.Images.SetKeyName(7, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarExcel, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 366)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(728, 56)
        Me.ToolBar1.TabIndex = 1
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        Me.ToolBarNuevo.Text = "Nuevo"
        Me.ToolBarNuevo.Visible = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        Me.ToolBarBuscar.Text = "Buscar"
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        Me.ToolBarEliminar.Text = "Eliminar"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        Me.ToolBarImprimir.Text = "Imprimir"
        Me.ToolBarImprimir.Visible = False
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 5
        Me.ToolBarExcel.Name = "ToolBarExcel"
        Me.ToolBarExcel.Text = "Exportar"
        Me.ToolBarExcel.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DIEGO;packet size=4096;integrated security=SSPI;data source=""(loca" & _
            "l)"";persist security info=False;initial catalog=Hotel"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'dadetalleopcionpago
        '
        Me.dadetalleopcionpago.DeleteCommand = Me.SqlDeleteCommand4
        Me.dadetalleopcionpago.InsertCommand = Me.SqlInsertCommand4
        Me.dadetalleopcionpago.SelectCommand = Me.SqlSelectCommand4
        Me.dadetalleopcionpago.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Detalle_pago_caja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroFactura", "NumeroFactura"), New System.Data.Common.DataColumnMapping("TipoFactura", "TipoFactura"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Referencia", "Referencia"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("ReferenciaTipo", "ReferenciaTipo"), New System.Data.Common.DataColumnMapping("ReferenciaDoc", "ReferenciaDoc"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_ODP", "Id_ODP"), New System.Data.Common.DataColumnMapping("cuentaBancaria", "cuentaBancaria")})})
        Me.dadetalleopcionpago.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = resources.GetString("SqlDeleteCommand4.CommandText")
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_ODP", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_ODP", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cuentaBancaria", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cuentaBancaria", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = resources.GetString("SqlInsertCommand4.CommandText")
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.Int, 4, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id_ODP", System.Data.SqlDbType.BigInt, 8, "Id_ODP"), New System.Data.SqlClient.SqlParameter("@cuentaBancaria", System.Data.SqlDbType.VarChar, 50, "cuentaBancaria")})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTi" & _
            "po, ReferenciaDoc, Moneda, TipoCambio, Id, Id_ODP, cuentaBancaria FROM Detalle_p" & _
            "ago_caja"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.Int, 4, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id_ODP", System.Data.SqlDbType.BigInt, 8, "Id_ODP"), New System.Data.SqlClient.SqlParameter("@cuentaBancaria", System.Data.SqlDbType.VarChar, 50, "cuentaBancaria"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_ODP", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_ODP", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cuentaBancaria", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cuentaBancaria", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'damoneda
        '
        Me.damoneda.SelectCommand = Me.SqlSelectCommand5
        Me.damoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'txtreftipo
        '
        Me.txtreftipo.ForeColor = System.Drawing.Color.Blue
        Me.txtreftipo.Location = New System.Drawing.Point(16, 224)
        Me.txtreftipo.Name = "txtreftipo"
        Me.txtreftipo.Size = New System.Drawing.Size(328, 20)
        Me.txtreftipo.TabIndex = 108
        '
        'daopcionpago
        '
        Me.daopcionpago.DeleteCommand = Me.SqlDeleteCommand7
        Me.daopcionpago.InsertCommand = Me.SqlInsertCommand7
        Me.daopcionpago.SelectCommand = Me.SqlSelectCommand7
        Me.daopcionpago.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpcionesDePago", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("TipoDocumento", "TipoDocumento"), New System.Data.Common.DataColumnMapping("MontoPago", "MontoPago"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Nombremoneda", "Nombremoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Numapertura", "Numapertura")})})
        Me.daopcionpago.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = resources.GetString("SqlDeleteCommand7.CommandText")
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = resources.GetString("SqlInsertCommand7.CommandText")
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.Float, 8, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 3, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.BigInt, 8, "Numapertura")})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario," & _
            " Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura FROM OpcionesDe" & _
            "Pago"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = resources.GetString("SqlUpdateCommand7.CommandText")
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.Float, 8, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 3, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.BigInt, 8, "Numapertura"), New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.BigInt, 8, "id")})
        '
        'TxtDocu
        '
        Me.TxtDocu.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Documento", True))
        Me.TxtDocu.Location = New System.Drawing.Point(8, 32)
        Me.TxtDocu.Name = "TxtDocu"
        Me.TxtDocu.Size = New System.Drawing.Size(96, 20)
        Me.TxtDocu.TabIndex = 109
        Me.TxtDocu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtdenominacion
        '
        Me.txtdenominacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Denominacion", True))
        Me.txtdenominacion.Location = New System.Drawing.Point(8, 72)
        Me.txtdenominacion.Name = "txtdenominacion"
        Me.txtdenominacion.Size = New System.Drawing.Size(96, 20)
        Me.txtdenominacion.TabIndex = 110
        Me.txtdenominacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtauxformapago
        '
        Me.txtauxformapago.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.FormaPago", True))
        Me.txtauxformapago.Location = New System.Drawing.Point(8, 152)
        Me.txtauxformapago.Name = "txtauxformapago"
        Me.txtauxformapago.Size = New System.Drawing.Size(96, 20)
        Me.txtauxformapago.TabIndex = 112
        Me.txtauxformapago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtauxtipodocumento
        '
        Me.txtauxtipodocumento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.TipoDocumento", True))
        Me.txtauxtipodocumento.Location = New System.Drawing.Point(8, 192)
        Me.txtauxtipodocumento.Name = "txtauxtipodocumento"
        Me.txtauxtipodocumento.Size = New System.Drawing.Size(96, 20)
        Me.txtauxtipodocumento.TabIndex = 113
        Me.txtauxtipodocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtfecha
        '
        Me.txtfecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Fecha", True))
        Me.txtfecha.Location = New System.Drawing.Point(128, 72)
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.Size = New System.Drawing.Size(96, 20)
        Me.txtfecha.TabIndex = 114
        Me.txtfecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcodcoin
        '
        Me.txtcodcoin.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.CodMoneda", True))
        Me.txtcodcoin.Location = New System.Drawing.Point(128, 112)
        Me.txtcodcoin.Name = "txtcodcoin"
        Me.txtcodcoin.Size = New System.Drawing.Size(96, 20)
        Me.txtcodcoin.TabIndex = 115
        Me.txtcodcoin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttc
        '
        Me.txttc.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.TipoCambio", True))
        Me.txttc.Location = New System.Drawing.Point(128, 192)
        Me.txttc.Name = "txttc"
        Me.txttc.Size = New System.Drawing.Size(72, 20)
        Me.txttc.TabIndex = 116
        Me.txttc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgdetopcpago
        '
        Me.dgdetopcpago.DataMember = "Detalle_pago_caja"
        Me.dgdetopcpago.DataSource = Me.DataSet_Opciones_Pago1
        '
        '
        '
        Me.dgdetopcpago.EmbeddedNavigator.Name = ""
        Me.dgdetopcpago.Location = New System.Drawing.Point(348, 173)
        Me.dgdetopcpago.MainView = Me.GridView2
        Me.dgdetopcpago.Name = "dgdetopcpago"
        Me.dgdetopcpago.Size = New System.Drawing.Size(376, 120)
        Me.dgdetopcpago.TabIndex = 117
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFormaPago2, Me.colReferencia, Me.colDocumento2, Me.colReferenciaTipo, Me.colReferenciaDoc, Me.colCuenta})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colFormaPago2
        '
        Me.colFormaPago2.Caption = "FormaPago"
        Me.colFormaPago2.FieldName = "FormaPago"
        Me.colFormaPago2.FilterInfo = ColumnFilterInfo1
        Me.colFormaPago2.Name = "colFormaPago2"
        Me.colFormaPago2.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFormaPago2.VisibleIndex = 0
        Me.colFormaPago2.Width = 81
        '
        'colReferencia
        '
        Me.colReferencia.Caption = "Ref."
        Me.colReferencia.FieldName = "Referencia"
        Me.colReferencia.FilterInfo = ColumnFilterInfo2
        Me.colReferencia.Name = "colReferencia"
        Me.colReferencia.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colReferencia.VisibleIndex = 1
        Me.colReferencia.Width = 67
        '
        'colDocumento2
        '
        Me.colDocumento2.Caption = "Documento"
        Me.colDocumento2.FieldName = "Documento"
        Me.colDocumento2.FilterInfo = ColumnFilterInfo3
        Me.colDocumento2.Name = "colDocumento2"
        Me.colDocumento2.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDocumento2.VisibleIndex = 2
        Me.colDocumento2.Width = 67
        '
        'colReferenciaTipo
        '
        Me.colReferenciaTipo.Caption = "Ref.Tipo"
        Me.colReferenciaTipo.FieldName = "ReferenciaTipo"
        Me.colReferenciaTipo.FilterInfo = ColumnFilterInfo4
        Me.colReferenciaTipo.Name = "colReferenciaTipo"
        Me.colReferenciaTipo.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colReferenciaTipo.VisibleIndex = 3
        Me.colReferenciaTipo.Width = 67
        '
        'colReferenciaDoc
        '
        Me.colReferenciaDoc.Caption = "Ref.Doc"
        Me.colReferenciaDoc.FieldName = "ReferenciaDoc"
        Me.colReferenciaDoc.FilterInfo = ColumnFilterInfo5
        Me.colReferenciaDoc.Name = "colReferenciaDoc"
        Me.colReferenciaDoc.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colReferenciaDoc.VisibleIndex = 4
        Me.colReferenciaDoc.Width = 72
        '
        'colCuenta
        '
        Me.colCuenta.Caption = "C.Bancaria"
        Me.colCuenta.FieldName = "cuentaBancaria"
        Me.colCuenta.FilterInfo = ColumnFilterInfo6
        Me.colCuenta.Name = "colCuenta"
        Me.colCuenta.VisibleIndex = 5
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label21.Location = New System.Drawing.Point(8, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(96, 16)
        Me.Label21.TabIndex = 119
        Me.Label21.Text = "Documento"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.Location = New System.Drawing.Point(8, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(96, 16)
        Me.Label20.TabIndex = 120
        Me.Label20.Text = "Denominaci�n"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label22.Location = New System.Drawing.Point(8, 136)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(96, 16)
        Me.Label22.TabIndex = 121
        Me.Label22.Text = "FormaPago"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label23.Location = New System.Drawing.Point(8, 176)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(96, 16)
        Me.Label23.TabIndex = 122
        Me.Label23.Text = "Tipo Documento"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label24.Location = New System.Drawing.Point(128, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(96, 16)
        Me.Label24.TabIndex = 123
        Me.Label24.Text = "Fecha"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label25.Location = New System.Drawing.Point(128, 96)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(96, 16)
        Me.Label25.TabIndex = 124
        Me.Label25.Text = "Cod Moneda"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label26.Location = New System.Drawing.Point(128, 176)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(72, 16)
        Me.Label26.TabIndex = 125
        Me.Label26.Text = "Tipo Cambio"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtnommoneda)
        Me.GroupBox2.Controls.Add(Me.Label35)
        Me.GroupBox2.Controls.Add(Me.txtmonto)
        Me.GroupBox2.Controls.Add(Me.Label34)
        Me.GroupBox2.Controls.Add(Me.txttc)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.txtdenominacion)
        Me.GroupBox2.Controls.Add(Me.txtauxformapago)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.txtauxtipodocumento)
        Me.GroupBox2.Controls.Add(Me.txtfecha)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.txtcodcoin)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.TxtDocu)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label38)
        Me.GroupBox2.Controls.Add(Me.txtNumApertura)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(16, 424)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(232, 224)
        Me.GroupBox2.TabIndex = 126
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones de Pago"
        '
        'txtnommoneda
        '
        Me.txtnommoneda.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Nombremoneda", True))
        Me.txtnommoneda.Location = New System.Drawing.Point(128, 152)
        Me.txtnommoneda.Name = "txtnommoneda"
        Me.txtnommoneda.Size = New System.Drawing.Size(96, 20)
        Me.txtnommoneda.TabIndex = 128
        Me.txtnommoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label35.Location = New System.Drawing.Point(128, 136)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(96, 16)
        Me.Label35.TabIndex = 129
        Me.Label35.Text = "Moneda"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtmonto
        '
        Me.txtmonto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.MontoPago", True))
        Me.txtmonto.Location = New System.Drawing.Point(8, 112)
        Me.txtmonto.Name = "txtmonto"
        Me.txtmonto.Size = New System.Drawing.Size(96, 20)
        Me.txtmonto.TabIndex = 126
        Me.txtmonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label34.Location = New System.Drawing.Point(8, 96)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(96, 16)
        Me.Label34.TabIndex = 127
        Me.Label34.Text = "Monto"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label38.Location = New System.Drawing.Point(120, 16)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(104, 16)
        Me.Label38.TabIndex = 137
        Me.Label38.Text = "Num Apertura"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNumApertura
        '
        Me.txtNumApertura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Numapertura", True))
        Me.txtNumApertura.Location = New System.Drawing.Point(120, 32)
        Me.txtNumApertura.Name = "txtNumApertura"
        Me.txtNumApertura.Size = New System.Drawing.Size(100, 20)
        Me.txtNumApertura.TabIndex = 136
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txtreftipo)
        Me.GroupBox3.Controls.Add(Me.txtfactura)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txttipofact)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtformapago)
        Me.GroupBox3.Controls.Add(Me.txtauxcoin)
        Me.GroupBox3.Controls.Add(Me.txtauxtipocambio)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(368, 416)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(360, 128)
        Me.GroupBox3.TabIndex = 131
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle Pago Caja"
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label33.Location = New System.Drawing.Point(264, 48)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(72, 16)
        Me.Label33.TabIndex = 133
        Me.Label33.Text = "Tipo Cambio"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label32.Location = New System.Drawing.Point(192, 48)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(56, 16)
        Me.Label32.TabIndex = 132
        Me.Label32.Text = "Moneda"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label31.Location = New System.Drawing.Point(16, 48)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(128, 16)
        Me.Label31.TabIndex = 131
        Me.Label31.Text = "FormaPago"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'colDenominacion
        '
        Me.colDenominacion.Caption = "Denominaci�n"
        Me.colDenominacion.FieldName = "Denominacion"
        Me.colDenominacion.FilterInfo = ColumnFilterInfo7
        Me.colDenominacion.Name = "colDenominacion"
        Me.colDenominacion.VisibleIndex = 4
        '
        'colTipoCambio
        '
        Me.colTipoCambio.Caption = "Cambio"
        Me.colTipoCambio.FieldName = "TipoCambio"
        Me.colTipoCambio.FilterInfo = ColumnFilterInfo8
        Me.colTipoCambio.Name = "colTipoCambio"
        Me.colTipoCambio.VisibleIndex = 3
        Me.colTipoCambio.Width = 39
        '
        'colDocumento
        '
        Me.colDocumento.Caption = "Factura"
        Me.colDocumento.FieldName = "Documento"
        Me.colDocumento.FilterInfo = ColumnFilterInfo9
        Me.colDocumento.Name = "colDocumento"
        Me.colDocumento.VisibleIndex = 5
        Me.colDocumento.Width = 77
        '
        'colNombremoneda
        '
        Me.colNombremoneda.Caption = "Moneda"
        Me.colNombremoneda.FieldName = "Nombremoneda"
        Me.colNombremoneda.FilterInfo = ColumnFilterInfo10
        Me.colNombremoneda.Name = "colNombremoneda"
        Me.colNombremoneda.VisibleIndex = 2
        Me.colNombremoneda.Width = 71
        '
        'colMontoPago
        '
        Me.colMontoPago.Caption = "Monto"
        Me.colMontoPago.FieldName = "MontoPago"
        Me.colMontoPago.FilterInfo = ColumnFilterInfo11
        Me.colMontoPago.Name = "colMontoPago"
        Me.colMontoPago.VisibleIndex = 0
        '
        'colFormaPago
        '
        Me.colFormaPago.Caption = "Forma Pago"
        Me.colFormaPago.FieldName = "FormaPago"
        Me.colFormaPago.FilterInfo = ColumnFilterInfo12
        Me.colFormaPago.Name = "colFormaPago"
        Me.colFormaPago.VisibleIndex = 1
        Me.colFormaPago.Width = 49
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "OpcionesDePago"
        Me.GridControl1.DataSource = Me.DataSet_Opciones_Pago1
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(348, 37)
        Me.GridControl1.MainView = Me.GridView3
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(376, 112)
        Me.GridControl1.TabIndex = 133
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDocumento1, Me.colTipoDocumento, Me.colMontoPago1, Me.colFormaPago1, Me.colDenominacion1, Me.colNombremoneda1})
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowDetailButtons = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colDocumento1
        '
        Me.colDocumento1.Caption = "Documento"
        Me.colDocumento1.FieldName = "Documento"
        Me.colDocumento1.FilterInfo = ColumnFilterInfo13
        Me.colDocumento1.Name = "colDocumento1"
        Me.colDocumento1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDocumento1.VisibleIndex = 0
        '
        'colTipoDocumento
        '
        Me.colTipoDocumento.Caption = "Tipo Doc."
        Me.colTipoDocumento.FieldName = "TipoDocumento"
        Me.colTipoDocumento.FilterInfo = ColumnFilterInfo14
        Me.colTipoDocumento.Name = "colTipoDocumento"
        Me.colTipoDocumento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipoDocumento.VisibleIndex = 1
        '
        'colMontoPago1
        '
        Me.colMontoPago1.Caption = "Monto Pago"
        Me.colMontoPago1.DisplayFormat.FormatString = "#,#0.00"
        Me.colMontoPago1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMontoPago1.FieldName = "MontoPago"
        Me.colMontoPago1.FilterInfo = ColumnFilterInfo15
        Me.colMontoPago1.Name = "colMontoPago1"
        Me.colMontoPago1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMontoPago1.VisibleIndex = 2
        '
        'colFormaPago1
        '
        Me.colFormaPago1.Caption = "Forma Pago"
        Me.colFormaPago1.FieldName = "FormaPago"
        Me.colFormaPago1.FilterInfo = ColumnFilterInfo16
        Me.colFormaPago1.Name = "colFormaPago1"
        Me.colFormaPago1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFormaPago1.VisibleIndex = 3
        '
        'colDenominacion1
        '
        Me.colDenominacion1.Caption = "Denominacion"
        Me.colDenominacion1.DisplayFormat.FormatString = "#,#0.00"
        Me.colDenominacion1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colDenominacion1.FieldName = "Denominacion"
        Me.colDenominacion1.FilterInfo = ColumnFilterInfo17
        Me.colDenominacion1.Name = "colDenominacion1"
        Me.colDenominacion1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDenominacion1.VisibleIndex = 4
        '
        'colNombremoneda1
        '
        Me.colNombremoneda1.Caption = "Moneda"
        Me.colNombremoneda1.FieldName = "Nombremoneda"
        Me.colNombremoneda1.FilterInfo = ColumnFilterInfo18
        Me.colNombremoneda1.Name = "colNombremoneda1"
        Me.colNombremoneda1.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombremoneda1.VisibleIndex = 5
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(16, 584)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(832, 104)
        Me.Label39.TabIndex = 138
        '
        'Adapter_apertura
        '
        Me.Adapter_apertura.DeleteCommand = Me.SqlDeleteCommand8
        Me.Adapter_apertura.InsertCommand = Me.SqlInsertCommand8
        Me.Adapter_apertura.SelectCommand = Me.SqlSelectCommand6
        Me.Adapter_apertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.Adapter_apertura.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = resources.GetString("SqlDeleteCommand8.CommandText")
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = resources.GetString("SqlInsertCommand8.CommandText")
        Me.SqlInsertCommand8.Connection = Me.SqlConnection1
        Me.SqlInsertCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula")})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
            "turacaja"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = resources.GetString("SqlUpdateCommand8.CommandText")
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura")})
        '
        'label_Tipo_Cambio
        '
        Me.label_Tipo_Cambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "Moneda.ValorVenta", True))
        Me.label_Tipo_Cambio.Location = New System.Drawing.Point(264, 56)
        Me.label_Tipo_Cambio.Name = "label_Tipo_Cambio"
        Me.label_Tipo_Cambio.Size = New System.Drawing.Size(80, 16)
        Me.label_Tipo_Cambio.TabIndex = 140
        '
        'Label40
        '
        Me.Label40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(264, 40)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(80, 16)
        Me.Label40.TabIndex = 141
        Me.Label40.Text = "Tipo Cambio"
        '
        'Label41
        '
        Me.Label41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(128, 89)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(216, 16)
        Me.Label41.TabIndex = 142
        Me.Label41.Text = "Monto a Pagar"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Monto_Su_Moneda
        '
        Me.Monto_Su_Moneda.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Monto_Su_Moneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Monto_Su_Moneda.ForeColor = System.Drawing.Color.Blue
        Me.Monto_Su_Moneda.Location = New System.Drawing.Point(744, 48)
        Me.Monto_Su_Moneda.Name = "Monto_Su_Moneda"
        Me.Monto_Su_Moneda.Size = New System.Drawing.Size(136, 32)
        Me.Monto_Su_Moneda.TabIndex = 143
        Me.Monto_Su_Moneda.Text = "0"
        Me.Monto_Su_Moneda.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Pagado_Su_Moneda
        '
        Me.Pagado_Su_Moneda.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Pagado_Su_Moneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Pagado_Su_Moneda.ForeColor = System.Drawing.Color.Blue
        Me.Pagado_Su_Moneda.Location = New System.Drawing.Point(744, 112)
        Me.Pagado_Su_Moneda.Name = "Pagado_Su_Moneda"
        Me.Pagado_Su_Moneda.Size = New System.Drawing.Size(136, 32)
        Me.Pagado_Su_Moneda.TabIndex = 144
        Me.Pagado_Su_Moneda.Text = "0"
        Me.Pagado_Su_Moneda.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label42
        '
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label42.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label42.Location = New System.Drawing.Point(744, 32)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(136, 16)
        Me.Label42.TabIndex = 145
        Me.Label42.Text = "Monto Total Factura"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label43
        '
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label43.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label43.Location = New System.Drawing.Point(744, 96)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(136, 16)
        Me.Label43.TabIndex = 146
        Me.Label43.Text = "Monto Pagado Factura"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TxtMontoPagar_Sumoneda
        '
        Me.TxtMontoPagar_Sumoneda.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMontoPagar_Sumoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMontoPagar_Sumoneda.ForeColor = System.Drawing.Color.Blue
        Me.TxtMontoPagar_Sumoneda.Location = New System.Drawing.Point(744, 184)
        Me.TxtMontoPagar_Sumoneda.Multiline = True
        Me.TxtMontoPagar_Sumoneda.Name = "TxtMontoPagar_Sumoneda"
        Me.TxtMontoPagar_Sumoneda.Size = New System.Drawing.Size(136, 40)
        Me.TxtMontoPagar_Sumoneda.TabIndex = 147
        Me.TxtMontoPagar_Sumoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label36
        '
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label36.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label36.Location = New System.Drawing.Point(744, 168)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(136, 16)
        Me.Label36.TabIndex = 148
        Me.Label36.Text = "Monto a Pagar"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.Nombre", True))
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(520, 400)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 168
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(464, 400)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 166
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(400, 400)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 167
        Me.Label5.Text = "Usuario->"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AdapterTarjeta
        '
        Me.AdapterTarjeta.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterTarjeta.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterTarjeta.SelectCommand = Me.SqlSelectCommand8
        Me.AdapterTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TipoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        Me.AdapterTarjeta.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM TipoTarjeta WHERE (Id = @Original_Id) AND (Nombre = @Original_Nombre)" & _
            ""
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO TipoTarjeta(Nombre) VALUES (@Nombre); SELECT Id, Nombre FROM TipoTarj" & _
            "eta WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre")})
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT Id, Nombre FROM TipoTarjeta"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE TipoTarjeta SET Nombre = @Nombre WHERE (Id = @Original_Id) AND (Nombre = @" & _
            "Original_Nombre); SELECT Id, Nombre FROM TipoTarjeta WHERE (Id = @Id)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'Adapter_Deposito
        '
        Me.Adapter_Deposito.DeleteCommand = Me.SqlDeleteCommand6
        Me.Adapter_Deposito.InsertCommand = Me.SqlInsertCommand6
        Me.Adapter_Deposito.SelectCommand = Me.SqlSelectCommand9
        Me.Adapter_Deposito.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Deposito", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Deposito", "Id_Deposito"), New System.Data.Common.DataColumnMapping("NumeroDocumento", "NumeroDocumento"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Concepto", "Concepto"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Conciliado", "Conciliado"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("Ced_Usuario", "Ced_Usuario"), New System.Data.Common.DataColumnMapping("Asiento", "Asiento"), New System.Data.Common.DataColumnMapping("Num_Conciliacion", "Num_Conciliacion"), New System.Data.Common.DataColumnMapping("Id_CuentaBancaria", "Id_CuentaBancaria"), New System.Data.Common.DataColumnMapping("CodigoMoneda", "CodigoMoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio")})})
        Me.Adapter_Deposito.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = resources.GetString("SqlDeleteCommand6.CommandText")
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection2
        Me.SqlDeleteCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id_Deposito", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Deposito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodigoMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Concepto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Concepto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Conciliado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Conciliado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CuentaBancaria", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Num_Conciliacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Conciliacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroDocumento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=SEESERVER;packet size=4096;integrated security=SSPI;initial catalo" & _
            "g=Bancos;persist security info=False"
        Me.SqlConnection2.FireInfoMessageEventOnUserErrors = False
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = resources.GetString("SqlInsertCommand6.CommandText")
        Me.SqlInsertCommand6.Connection = Me.SqlConnection2
        Me.SqlInsertCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroDocumento", System.Data.SqlDbType.BigInt, 8, "NumeroDocumento"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Concepto", System.Data.SqlDbType.VarChar, 250, "Concepto"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Conciliado", System.Data.SqlDbType.Bit, 1, "Conciliado"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 75, "Ced_Usuario"), New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.VarChar, 15, "Asiento"), New System.Data.SqlClient.SqlParameter("@Num_Conciliacion", System.Data.SqlDbType.BigInt, 8, "Num_Conciliacion"), New System.Data.SqlClient.SqlParameter("@Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, "Id_CuentaBancaria"), New System.Data.SqlClient.SqlParameter("@CodigoMoneda", System.Data.SqlDbType.Int, 4, "CodigoMoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio")})
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT Id_Deposito, NumeroDocumento, Fecha, Monto, Concepto, Anulado, Conciliado," & _
            " Contabilizado, Ced_Usuario, Asiento, Num_Conciliacion, Id_CuentaBancaria, Codig" & _
            "oMoneda, TipoCambio FROM Deposito"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection2
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = resources.GetString("SqlUpdateCommand6.CommandText")
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection2
        Me.SqlUpdateCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroDocumento", System.Data.SqlDbType.BigInt, 8, "NumeroDocumento"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Concepto", System.Data.SqlDbType.VarChar, 250, "Concepto"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Conciliado", System.Data.SqlDbType.Bit, 1, "Conciliado"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 75, "Ced_Usuario"), New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.VarChar, 15, "Asiento"), New System.Data.SqlClient.SqlParameter("@Num_Conciliacion", System.Data.SqlDbType.BigInt, 8, "Num_Conciliacion"), New System.Data.SqlClient.SqlParameter("@Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, "Id_CuentaBancaria"), New System.Data.SqlClient.SqlParameter("@CodigoMoneda", System.Data.SqlDbType.Int, 4, "CodigoMoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Original_Id_Deposito", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Deposito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodigoMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Concepto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Concepto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Conciliado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Conciliado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CuentaBancaria", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Num_Conciliacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Conciliacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroDocumento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id_Deposito", System.Data.SqlDbType.BigInt, 8, "Id_Deposito")})
        '
        'LabelConvertir
        '
        Me.LabelConvertir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelConvertir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelConvertir.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LabelConvertir.Location = New System.Drawing.Point(408, 304)
        Me.LabelConvertir.Name = "LabelConvertir"
        Me.LabelConvertir.Size = New System.Drawing.Size(112, 16)
        Me.LabelConvertir.TabIndex = 169
        Me.LabelConvertir.Text = "Convertir Vuelto"
        Me.LabelConvertir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ComboBoxConvertir
        '
        Me.ComboBoxConvertir.BackColor = System.Drawing.Color.Khaki
        Me.ComboBoxConvertir.DataSource = Me.DataSet_Opciones_Pago1.Moneda_Vuelto
        Me.ComboBoxConvertir.DisplayMember = "MonedaNombre"
        Me.ComboBoxConvertir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxConvertir.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ComboBoxConvertir.Location = New System.Drawing.Point(408, 320)
        Me.ComboBoxConvertir.Name = "ComboBoxConvertir"
        Me.ComboBoxConvertir.Size = New System.Drawing.Size(112, 28)
        Me.ComboBoxConvertir.TabIndex = 170
        Me.ComboBoxConvertir.ValueMember = "CodMoneda"
        '
        'frmMovimientoCajaPagoAbono2
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(728, 422)
        Me.Controls.Add(Me.ComboBoxConvertir)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.TxtMontoPagar_Sumoneda)
        Me.Controls.Add(Me.LabelConvertir)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Pagado_Su_Moneda)
        Me.Controls.Add(Me.Monto_Su_Moneda)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.label_Tipo_Cambio)
        Me.Controls.Add(Me.GroupBox_Detalle_Pago)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.txttotal)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.dgdetopcpago)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblvuelto)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblmontopagado)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblmontofact)
        Me.Controls.Add(Me.gbmoneda)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label39)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja.ReferenciaTipo", True))
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(736, 456)
        Me.Name = "frmMovimientoCajaPagoAbono2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Movimiento Caja de Pago"
        CType(Me.DataSet_Opciones_Pago1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txttotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblvuelto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblmontopagado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblmontofact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbmoneda.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox_Detalle_Pago.ResumeLayout(False)
        Me.GroupBox_Detalle_Pago.PerformLayout()
        CType(Me.dgdetopcpago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "variables"
    Private cConexion As New Conexion
    Private sqlConexion As SqlConnection
#End Region
    Public Sub open_cashdrawer()

        Dim Ubicacion As String = ""
        Dim escapes As String = GetSetting("SeeSoft", "Restaurante", "SinAbrir")
        Dim puerto As String = GetSetting("SeeSoft", "Restaurante", "PuertoCaja")

        If GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes") = "" Then
            SaveSetting("SeeSoft", "Restaurante", "UbicacionEscapes", "c:\escapes.txt")
        Else
            Ubicacion = GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes")
        End If

        If puerto.Equals("") Then
            puerto = "LPT1"
            SaveSetting("SeeSoft", "Restaurante", "PuertoCaja", "LPT1")
        End If

        If escapes.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "SinAbrir", "0")
        Else
            If escapes.Equals("1") Then
                Dim intFileNo As Integer = FreeFile()
                FileOpen(1, Ubicacion, OpenMode.Output)
                PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
                FileClose(1)
                Shell("print /d:" & puerto & " " & Ubicacion, AppWinStyle.NormalFocus)
            End If
        End If

    End Sub
    Sub cargarMonedaVuelto()
        Dim cf As New cFunciones
        Dim dt As New DataTable

        cf.Llenar_Tabla_Generico("Select Moneda_Vuelto From configuraciones ", dt)
        If dt.Rows.Count > 0 Then
            Me.monedaVuelto = dt.Rows(0).Item("Moneda_Vuelto")

        End If
    End Sub
    Private Sub Position_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Me.BindingContext(Me.DataSet_Opciones_Pago1, "Detalle_pago_caja").Count > 0 Then
            bandera = True
            Select Case Me.txtauxformapago.Text
                Case "EFE" : Me.efectivo()

                Case "TAR" : Me.tarjeta()

                Case "TRA" : Me.transferencia()

                Case "CHE" : Me.cheque()


            End Select
            bandera = False
        End If

    End Sub



    Private Sub frmMovimientoCajaPagoAbono_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.conexion.Equals("") Then
                SqlConnection1.ConnectionString = GetSetting("Hotel", "Hotel", "CONEXION")
            Else
                SqlConnection1.ConnectionString = conexion
            End If
            Me.SqlConnection2.ConnectionString = GetSetting("SeeSoft", "Bancos", "CONEXION")

            Me.DataSet_Opciones_Pago1.OpcionesDePago.idColumn.AutoIncrement = True
            Me.DataSet_Opciones_Pago1.OpcionesDePago.idColumn.AutoIncrementSeed = -1
            Me.DataSet_Opciones_Pago1.OpcionesDePago.idColumn.AutoIncrementStep = -1

            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.IdColumn.AutoIncrement = True
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.IdColumn.AutoIncrementSeed = -1
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.IdColumn.AutoIncrementStep = -1

            Me.txtUsuario.Text = Me.cedu
            Me.txtNombreUsuario.Text = Me.nombre
            Me.open_cashdrawer()
            If Tipo = "ABO" Then
                If Me.cambioFP = 0 Then
                    inicializar()
                Else
                    If Not Buscar_Apertura(cedu) Then
                        Me.txtUsuario.Focus()
                        Me.txtUsuario.Text = ""
                        Me.txtNombreUsuario.Text = ""
                    Else
                        inicializar()
                    End If

                End If

            Else
                If Not Buscar_Apertura(cedu) Then
                    Me.txtUsuario.Focus()
                    Me.txtUsuario.Text = ""
                    Me.txtNombreUsuario.Text = ""
                Else
                    inicializar()
                End If

            End If
            'inicializar()
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub


    Sub inicializar()
        Try
            cargando = True
            Me.rbefectivo.Checked = True
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.Clear()
            Me.DataSet_Opciones_Pago1.OpcionesDePago.Clear()
            Me.damoneda.Fill(Me.DataSet_Opciones_Pago1, "Moneda")
            Me.damoneda.Fill(Me.DataSet_Opciones_Pago1.Moneda_Vuelto)
            Me.AdapterTarjeta.Fill(Me.DataSet_Opciones_Pago1.TipoTarjeta)

            cFunciones.Llenar_Tabla_Generico("Select * From Bancos", Me.DataSet_Opciones_Pago1.Bancos, GetSetting("SeeSoft", "Bancos", "Conexion"))

            defaultvalue()
            Nuevo()
            Me.cargando = True
            cargarMonedaVuelto()
            Me.cbomoneda.SelectedValue = Me.codmod
            Me.ComboBoxConvertir.SelectedValue = Me.monedaVuelto
            Me.Monto_Su_Moneda.Text = Me.Total
            Me.Pagado_Su_Moneda.Text = 0

            recargar()
            sacar_conversion()
            Registra = False
            Me.txttotal.Enabled = True
            Me.cargando = False

            Me.txtUsuario.Enabled = False
            Me.txtNombreUsuario.Enabled = False
            Me.incializando = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbefectivo.CheckedChanged
        If Me.rbefectivo.Checked = True Then
            Me.efectivo()
        End If
    End Sub
    Sub efectivo()
        Try
            Me.Seleccionado = 1

            Me.GroupBox_Detalle_Pago.Visible = False

            Me.txtauxformapago.Text = "EFE"
            Combo_Bancos.Top = 96

            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").CancelCurrentEdit()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub rbtarjeta_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtarjeta.CheckedChanged
        If Me.rbtarjeta.Checked = True Then
            Me.tarjeta()
        End If
    End Sub

    Sub tarjeta()
        Try
            Me.Seleccionado = 2
            Me.GroupBox_Detalle_Pago.Visible = True
            Combo_Tarjeta.Visible = True

            Label_RefTipo.Visible = True
            TxtReferenciaTipo.Visible = True
            TxtVoucher.Visible = True
            Label_Referencia.Visible = True
            TxtReferenciaF.Visible = True
            Combo_Bancos.Top = 96
            ComboBox1.Visible = False

            If bandera = False Then nuevo_detalle()

            Me.GroupBox_Detalle_Pago.Text = "Pago con Tarjeta"
            Label_Referencia.Text = "N� Tarjeta"
            Label_Documento.Text = "Autorizaci�n"
            Label_RefTipo.Text = "Tipo Tarjeta"
            Label_RefDocumento.Text = "Voucher"
            '   Me.Combo_Tarjeta.DisplayMember = "ATH"

            Me.txtformapago.Text = "TAR"
            Me.txtauxformapago.Text = "TAR"

            Me.TxtReferenciaF.Focus()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub nuevo_detalle()
        Try
            Me.defaul_value_detalle()

            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Position = Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Count

            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").CancelCurrentEdit()
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").AddNew()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub
    'Radio Button de Cheque
    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbcheque.CheckedChanged
        If Me.rbcheque.Checked = True Then
            Me.cheque()
        End If
    End Sub

    Sub cheque()
        Try
            Me.GroupBox_Detalle_Pago.Visible = True
            Combo_Tarjeta.Visible = False
            Label_RefTipo.Visible = True
            TxtReferenciaTipo.Visible = True
            TxtVoucher.Visible = False
            Label_Referencia.Visible = True
            TxtReferenciaF.Visible = True
            Combo_Bancos.Top = 96
            ComboBox1.Visible = False

            Me.Seleccionado = 3
            If bandera = False Then nuevo_detalle()

            Me.GroupBox_Detalle_Pago.Text = "Pago con Cheque"
            Label_Referencia.Text = "Emisor"
            Label_Documento.Text = "N� Cheque"
            Label_RefTipo.Text = "Tel�fono"
            Label_RefDocumento.Text = "Banco"

            Me.txtformapago.Text = "CHE"
            Me.txtauxformapago.Text = "CHE"
            'Me.txtreftipo.Text = Me.cbobancocheq.Text

            Me.TxtReferenciaF.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub
    Private Sub rbtranferencia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtranferencia.CheckedChanged
        If Me.rbtranferencia.Checked = True Then
            transferencia()
            cargarbancos()
        End If
    End Sub

    Sub transferencia()
        Try
            Seleccionado = 4
            Forma_pago = 4
            GroupBox_Detalle_Pago.Visible = True
            Combo_Tarjeta.Visible = False
            TxtVoucher.Visible = False
            TxtReferenciaF.Visible = False
            If bandera = False Then nuevo_detalle()
            GroupBox_Detalle_Pago.Text = "Pago con Transferencia"
            Label_Referencia.Text = "Emisor"
            Label_Referencia.Visible = False
            Label_Documento.Text = "N� Transf."
            Label_RefTipo.Text = "Banco"
            Combo_Bancos.Top = 72
            ComboBox1.Visible = True
            TxtReferenciaTipo.Visible = False
            Label_RefDocumento.Text = "Cuentas"
            Me.txtformapago.Text = "TRA"
            Me.txtauxformapago.Text = "TRA"
            Me.TxtReferenciaF.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub defaultvalue()
        Try

            Me.DataSet_Opciones_Pago1.OpcionesDePago.TipoDocumentoColumn.DefaultValue = Tipo
            Me.DataSet_Opciones_Pago1.OpcionesDePago.DocumentoColumn.DefaultValue = Factura
            Me.DataSet_Opciones_Pago1.OpcionesDePago.FechaColumn.DefaultValue = fecha
            Me.DataSet_Opciones_Pago1.OpcionesDePago.MontoPagoColumn.DefaultValue = Me.Total

            Me.DataSet_Opciones_Pago1.OpcionesDePago.FormaPagoColumn.DefaultValue = "EFE"
            Me.DataSet_Opciones_Pago1.OpcionesDePago.CodMonedaColumn.DefaultValue = codmod


            Me.DataSet_Opciones_Pago1.OpcionesDePago.UsuarioColumn.DefaultValue = Me.cedu
            Me.DataSet_Opciones_Pago1.OpcionesDePago.NombreColumn.DefaultValue = Me.nombre
            Me.DataSet_Opciones_Pago1.OpcionesDePago.NumaperturaColumn.DefaultValue = NApertura
            Me.Numero_Apertura = NApertura
            Me.DataSet_Opciones_Pago1.OpcionesDePago.MontoPagoColumn.DefaultValue = 0
            Me.DataSet_Opciones_Pago1.OpcionesDePago.DenominacionColumn.DefaultValue = 0
            Me.DataSet_Opciones_Pago1.OpcionesDePago.NombremonedaColumn.DefaultValue = Me.cbomoneda.Text
            Me.DataSet_Opciones_Pago1.OpcionesDePago.TipoCambioColumn.DefaultValue = 0


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub defaul_value_detalle()
        Try
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.NumeroFacturaColumn.DefaultValue = Factura
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.TipoFacturaColumn.DefaultValue = Tipo
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.FormaPagoColumn.DefaultValue = "EFE"
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.ReferenciaColumn.DefaultValue = ""
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.DocumentoColumn.DefaultValue = ""
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.ReferenciaDocColumn.DefaultValue = ""
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.ReferenciaTipoColumn.DefaultValue = "0"
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.MonedaColumn.DefaultValue = Me.cbomoneda.SelectedValue
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.TipoCambioColumn.DefaultValue = txttc.Text
            Me.DataSet_Opciones_Pago1.Detalle_pago_caja.cuentaBancariaColumn.DefaultValue = ""

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub Nuevo()
        Try
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").AddNew()
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub recargar()
        'Me.txttotal.EditValue = Total
        Try
            Me.txttotal.EditValue = Total
            Me.lblmontofact.Text = Format(CDbl(Me.txttotal.EditValue), "#,#0.00")
            'Me.lblcontador.Text = "0"

            Me.lblmontopagado.Text = 0
            Me.lblvuelto.Text = 0
            Me.cbomoneda.Enabled = True

            'Me.txtMontoRestante.Text = Me.txttotal.EditValue
            Me.txtnommoneda.Text = Me.cbomoneda.Text




        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txttotal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txttotal.KeyDown
        If e.KeyCode = Keys.Escape Then
            Cerrar_Opciones()
        End If
        If e.KeyCode = Keys.C Then
            cbomoneda.Focus()
            SendKeys.Send("{F4}")
        End If
        If e.KeyCode = Keys.Enter Then
            pagar()
        End If
        If e.KeyCode = Keys.F2 Then
            If Me.txttotal.EditValue <> "" Then
                If CDbl(txttotal.EditValue) = 0 Then
                    registrar()
                End If
            End If
        End If
    End Sub
    Sub validaciones()
        '******************Validaciones**********************
        If Me.txttotal.EditValue = 0 Then
            MsgBox("Digite el Monto", MsgBoxStyle.Exclamation)
            Me.txttotal.EditValue = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
            Exit Sub
        End If

        If CDbl(Me.txttotal.EditValue) <= 0 Then
            Me.txttotal.EditValue = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
            Exit Sub
        End If


        If Me.Total = CDbl(Me.Pagado_Su_Moneda.Text) Then
            Me.txttotal.EditValue = 0
            Exit Sub
        End If

        If Me.Validar Then
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
        Else
            Exit Sub
        End If
        '******************Fin Validaciones**********************

    End Sub

    Function pagar()
        Try
            If Math.Round((CDbl(Pagado_Su_Moneda.Text) - Total), 2) >= 0 Then
                ' Cerrar_Form = True
                registrar()
            Else
                Dim editando As Boolean = False
                'conversion
                Dim Monto_Pagar As Double
                vuelto = 0

                'If Me.txttotal.Text = "" Then
                '    MsgBox("Digite el Monto", MsgBoxStyle.Exclamation)
                '    Me.txttotal.Text = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
                '    txttotal.EditValue = (CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text))
                '    Exit Function
                'End If

                If CDbl(Me.txttotal.EditValue) <= 0 Then
                    txttotal.Text = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
                    txttotal.EditValue = (CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text))
                    Exit Function
                End If


                If Me.Total = CDbl(Me.Pagado_Su_Moneda.Text) Then
                    Me.txttotal.Text = 0
                    Me.txttotal.EditValue = 0
                    Exit Function
                End If

                If Me.Validar Then
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
                Else
                    Exit Function
                End If



                Monto_Pagar = CDbl(Me.txttotal.EditValue)

                txtdenominacion.Text = Math.Round(Monto_Pagar + CDbl(Me.txtdenominacion.Text), 2)
                txtcodcoin.Text = Me.cbomoneda.SelectedValue
                Me.txttc.Text = Me.label_Tipo_Cambio.Text
                Me.txtnommoneda.Text = Me.cbomoneda.Text
                txtauxtipodocumento.Text = Me.Tipo


                Me.TxtMontoPagar_Sumoneda.Text = Math.Round(CDbl(Me.txttotal.EditValue) * (Me.Tipo_CambioOpciones / TipoCambio_Factura), 2)

                Pagado_Su_Moneda.Text = Math.Round(CDbl(Pagado_Su_Moneda.Text) + CDbl(Me.TxtMontoPagar_Sumoneda.Text), 2)

                If Prepago = True Then
                    Me.ToolBar1.Buttons(2).Enabled = True
                End If


                ' si el monto restante es igual a 0, osea si ya se pago todo
                If CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text) <= 0 Then  ' si con este pago se tremina de cancelar el monto de la factura


                    Me.ToolBar1.Buttons(2).Enabled = True

                    If CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text) < 0 Then
                        vuelto = Math.Round((CDbl(Pagado_Su_Moneda.Text) - Me.Total), 2)
                    Else
                        vuelto = 0.0
                    End If

                    Me.lblvuelto.Text = Math.Round(vuelto * Conversion, 2)

                    txtmonto.Text = Math.Round(CDbl(Me.txttotal.EditValue) + CDbl(txtmonto.Text) - CDbl(vuelto) * Conversion, 2)
                    If Conversion < 1 Then
                        sacar_conversion_vuelto(vuelto, True)
                    Else
                        Me.monedaVuelto = Me.ComboBoxConvertir.SelectedValue
                        vuelto = Math.Round(vuelto * Conversion, 2)
                        Me.loSaqueAqui = True
                    End If

                    Me.txttotal.Text = 0.0
                    Me.txttotal.EditValue = 0.0
                    Pagado_Su_Moneda.Text = Math.Round(Me.Total, 2)
                    lblmontopagado.Text = Math.Round(CDbl(lblmontofact.Text), 2)
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()

                    Me.TxtMontoPagar_Sumoneda.Text = 0.0
                    Me.cbomoneda.Enabled = False
                    Me.Label3.Text = "Vuelto"
                    verificar_ultimo()
                    Me.rbefectivo.Checked = True
                    conservarDatosTransferencia()
                Else ' si con este pago no se cancela la factura

                    If CDbl(Me.txtmonto.Text) <> 0 Then editando = True ' si se est� editando que se active la bandera
                    txtmonto.Text = Math.Round(CDbl(Me.txttotal.EditValue) + CDbl(txtmonto.Text), 2)
                    lblmontopagado.Text = Math.Round(CDbl(Pagado_Su_Moneda.Text), 2)
                    vuelto = Math.Round((CDbl(Pagado_Su_Moneda.Text) - Total), 2)
                    Me.lblvuelto.Text = vuelto * Conversion
                    Me.TxtMontoPagar_Sumoneda.Text = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
                    Me.txttotal.Text = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)) * Conversion, 2)
                    Me.txttotal.EditValue = (CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)) * Conversion
                    Me.Label3.Text = "Faltante"
                    sacar_conversion_vuelto(vuelto, True)
                    conservarDatosTransferencia()
                    If editando Then
                        Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()
                        editando = False
                    Else
                        Me.Nuevo()
                    End If
                    Me.rbefectivo.Checked = True
                End If
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Function


    Sub conservarDatosTransferencia()
        If Me.DataSet_Opciones_Pago1.Detalle_pago_caja.Count > 0 Then
            If Me.txtformapago.Text = "TAR" Then
                Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").Current("ReferenciaDoc") = Me.TxtVoucher.Text
            End If
            If Me.txtformapago.Text = "TRA" Or Me.txtformapago.Text = "CHE" Then
                Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").Current("ReferenciaDoc") = Me.Combo_Bancos.Text

            End If
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").Current("cuentabancaria") = Me.ComboBox1.Text
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
        End If
    End Sub


    Function verificar_ultimo() As Boolean
        Try
            Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Position = Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Count

            If CDbl(Me.txtmonto.Text) = 0 Then ' si el ultimo es el que se esta editando,se elimina
                Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").RemoveAt(Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Position)
                Me.BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Function Validar() As Boolean
        Try

            txtauxcoin.Text = cbomoneda.SelectedValue
            Select Case Me.Seleccionado

                Case 1


                Case 2
                    If Me.TxtReferenciaF.Text = "" Then
                        MsgBox("Digite el N� de Tarjeta", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                    If Me.TxtDocumentoF.Text = "" Then
                        MsgBox("Digite la autorizaci�n", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                    If Me.Combo_Tarjeta.Text = "" Then
                        MsgBox("Seleccione el Operador de Tarjetas", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                    If Me.TxtVoucher.Text = "" Then
                        MsgBox("Digite el Numero de Voucher", MsgBoxStyle.Exclamation)
                        Return False
                    End If

                Case 3
                    If Me.TxtReferenciaF.Text = "" Then
                        MsgBox("Digite el nombre Emisor del Cheque", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                    If Me.TxtDocumentoF.Text = "" Then
                        MsgBox("Digite el N� de Cheque", MsgBoxStyle.Exclamation)
                        Return False
                    End If


                Case 4
                    'If Me.TxtReferenciaF.Text = "" Then
                    '    MsgBox("Digite el nombre Emisor de la transferencia", MsgBoxStyle.Exclamation)
                    '    Return False
                    'End If
                    If Me.TxtDocumentoF.Text = "" Then
                        MsgBox("Digite el N� de Transferencia", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                    If Me.Combo_Bancos.Text = "" Then
                        MsgBox("Digite el Nombre del Banco", MsgBoxStyle.Exclamation)
                        Return False
                    End If

            End Select

            Return True



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    'Private Sub cbbancotarj_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Tarjeta.SelectedIndexChanged
    '    Me.TxtReferenciaTipo.Text = Combo_Tarjeta.Text
    'End Sub


    Function Buscar_Apertura(ByVal usuario As String) As Boolean
        Try


            Dim func As New cFunciones

            Dim i As Integer
            If Me.cambioFP = 0 Then
                ' no pide apertura
                If Tipo.Equals("ABO") Then
                    NApertura = cambioFP
                    Return True
                End If
            End If

            If Not Me.cambioFP = 0 Then
                If Not Tipo.Equals("ABO") Then
                    NApertura = cambioFP
                    Return True

                End If
            End If
            func.Cargar_Tabla_Generico(Me.Adapter_apertura, "SELECT * FROM AperturaCaja WHERE (Anulado = 0) AND (Estado = 'A') AND (Cedula = '" & usuario & "')")
            i = Me.Adapter_apertura.Fill(Me.DataSet_Opciones_Pago1.aperturacaja)

            Select Case i
                Case 1
                    NApertura = Me.DataSet_Opciones_Pago1.aperturacaja.Rows(0).Item("NApertura")
                    Me.txtNombreUsuario.Text = Me.DataSet_Opciones_Pago1.aperturacaja.Rows(0).Item("Nombre")
                    Return True
                Case 0
                    ' If Tipo = "ABO" Then
                    'NApertura = 0
                    '  Return True
                    ' Else
                    MsgBox(Me.nombre & " " & "No tiene una apertura de caja abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    Return False
                    ' End If

                Case Else
                    MsgBox(Me.nombre & " " & "tiene mas de una abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    Return False
            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function
    Function huboDeposito() As Boolean
        Dim i As Integer = 0
        For i = 0 To Me.DataSet_Opciones_Pago1.OpcionesDePago.Count - 1
            If Me.DataSet_Opciones_Pago1.OpcionesDePago(i).FormaPago = "TRA" Then Return True
        Next

    End Function
    Private Sub registrar()
        Dim i As Integer = 0
        For i = 0 To Me.DataSet_Opciones_Pago1.OpcionesDePago.Count - 1
            If Me.DataSet_Opciones_Pago1.OpcionesDePago(i).FormaPago <> "TRA" Then
                If Not Me.Buscar_Apertura(Me.cedu) Then
                    Exit Sub
                End If
            End If
        Next

        'Segun la forma de pago que escojan
        If huboDeposito() Then
            Registra_Deposito()
        End If
        If estaMalo Then

            MsgBox("No se puede continuar", MsgBoxStyle.OKOnly)

            Exit Sub
        End If
        BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").Current("Numapertura") = Me.NApertura
        BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()
        BindingContext(Me.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
        If RegistrarOpcionesPago() = True Then
            Registra = True
        Else
            Registra = False
        End If
        Me.Close()
    End Sub

    Public Function RegistrarOpcionesPago() As Boolean

        Dim Trans2 As SqlTransaction
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Trans = Me.SqlConnection1.BeginTransaction


        Try

            Me.daopcionpago.InsertCommand.Transaction = Trans
            Me.daopcionpago.UpdateCommand.Transaction = Trans
            Me.daopcionpago.DeleteCommand.Transaction = Trans



            Me.dadetalleopcionpago.InsertCommand.Transaction = Trans
            Me.dadetalleopcionpago.UpdateCommand.Transaction = Trans
            Me.dadetalleopcionpago.DeleteCommand.Transaction = Trans

            'Partes de Deposito
            If Me.SqlConnection2.State <> Me.SqlConnection2.State.Open Then Me.SqlConnection2.Open()
            Trans2 = Me.SqlConnection2.BeginTransaction
            Me.Adapter_Deposito.InsertCommand.Transaction = Trans2
            Me.Adapter_Deposito.UpdateCommand.Transaction = Trans2
            Me.Adapter_Deposito.DeleteCommand.Transaction = Trans2
            Me.Adapter_Deposito.Update(DataSet_Opciones_Pago1, "Deposito")
            Trans2.Commit()

            Me.daopcionpago.Update(DataSet_Opciones_Pago1, "OpcionesDePago")
            Me.dadetalleopcionpago.Update(DataSet_Opciones_Pago1, "Detalle_pago_caja")

            Trans.Commit()
            Return True


        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function


    Private Sub Eliminar_opcionpago()
        Dim resp As Integer

        Try 'se intenta hacer
            If Me.BindingContext(Me.DataSet_Opciones_Pago1, "opcionesdepago").Count > 0 Then  ' si hay opciones de pago

                If CDbl(txtmonto.Text) = 0 Then
                    Exit Sub
                End If
                Conversion = TipoCambio_Factura / Tipo_CambioOpciones
                resp = MessageBox.Show("�Desea eliminar esta opci�n de pago?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                If resp = 6 Then

                    Pagado_Su_Moneda.Text = Math.Round(CDbl(Pagado_Su_Moneda.Text) - CDbl(txtmonto.Text) * (Me.Tipo_CambioOpciones / TipoCambio_Factura), 2)

                    lblmontopagado.Text = Math.Round(CDbl(Pagado_Su_Moneda.Text) * Conversion, 2)

                    vuelto = Math.Round((CDbl(Pagado_Su_Moneda.Text) - Me.Total) * Conversion, 2)

                    Me.lblvuelto.Text = vuelto

                    Me.TxtMontoPagar_Sumoneda.Text = Math.Round((CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)), 2)
                    Me.Label3.Text = "Faltante"
                    Me.rbefectivo.Checked = True

                    Me.txttotal.Text = Math.Round((CDbl(Me.TxtMontoPagar_Sumoneda.Text)) * Conversion, 2)

                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "opcionesdepago").RemoveAt(Me.BindingContext(Me.DataSet_Opciones_Pago1, "opcionesdepago").Position)
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "opcionesdepago").EndCurrentEdit()
                    Me.cbomoneda.Enabled = True
                    If Me.BindingContext(Me.DataSet_Opciones_Pago1, "opcionesdepago").Count = 0 Then
                        Me.Nuevo()
                    End If

                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 3 : registrar()

            Case 7 : Cerrar_Opciones()
        End Select
    End Sub
    Private Sub Cerrar_Opciones()
        Dim resp As Integer
        resp = MessageBox.Show("�La opci�n de pago a�n no ha sido registrada; Desea salir sin registrar el documento como cancelado?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        If resp = 6 Then
            DataSet_Opciones_Pago1.Detalle_pago_caja.Clear()
            DataSet_Opciones_Pago1.OpcionesDePago.Clear()
            Me.txtUsuario.Enabled = True
            Me.txtNombreUsuario.Enabled = True
            Me.lblmontofact.Text = "0.00"
            Me.txttotal.EditValue = "0.00"
            Me.Registra = False
            Me.Close()
        End If
    End Sub
    Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Eliminar_opcionpago()
        End If
    End Sub

    Private Sub txtnumtarjeta_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtReferenciaTipo.KeyPress, TxtDocumentoF.KeyPress, TxtReferenciaF.KeyPress, Combo_Bancos.KeyPress, Combo_Tarjeta.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub txttotal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txttotal.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub



    Private Sub cbomoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbomoneda.SelectedIndexChanged
        Try
            Dim TotalFactura As Double
            sacar_conversion()
            If Me.cargando Then
                Me.txttotal.EditValue = Me.Total
                Me.TxtMontoPagar_Sumoneda.Text = Me.Total
                Me.cargando = False
                lblmontofact.Text = Me.Total
                Me.lblmontopagado.Text = "0.00"
            Else

                Me.TxtMontoPagar_Sumoneda.Text = (CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text))
                Me.txttotal.EditValue = (CDbl(Monto_Su_Moneda.Text) - CDbl(Pagado_Su_Moneda.Text)) * Conversion
                Me.lblmontopagado.Text = Math.Round(CDbl(Pagado_Su_Moneda.Text), 2)
                If Me.Total > CDbl(Me.Pagado_Su_Moneda.Text) Then 'si el monto total y afue cancelado
                    Me.lblvuelto.Text = Math.Round((CDbl(Me.Pagado_Su_Moneda.Text) - Me.Total), 2)
                    Me.sacar_conversion_vuelto(Me.lblvuelto.Text, True)
                Else
                    Me.lblvuelto.Text = "0.00"
                End If

            End If
            If Me.rbtranferencia.Checked = True Then
                cargarbancos()
            End If
            Me.txttotal.Focus()
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Sub sacar_conversion()
        Me.txtnommoneda.Text = Me.cbomoneda.Text
        mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & codmod)
        mode = mon(0)
        'If Me.Tipo = "ABO" Then
        '    If codmod = 2 Then
        '        TipoCambio_Factura = Me.TipoCambioRecibo
        '        If Me.cbomoneda.SelectedValue = 1 Then
        '            Me.label_Tipo_Cambio.Text = 1
        '        Else
        '            Me.label_Tipo_Cambio.Text = Me.TipoCambioRecibo
        '        End If

        '    Else
        '        TipoCambio_Factura = mode("ValorVenta")
        '    End If
        'Else
        If codmod = 2 Then
            If Me.Tipo = "ABO" Then
                If Not (Me.TipoCambioDolar = 1 Or Me.TipoCambioDolar = 0) Then
                    TipoCambio_Factura = Me.TipoCambioRecibo
                Else
                    TipoCambio_Factura = mode("ValorVenta")
                End If
            Else
                If Not (Me.TipoCambioDolar = 1 Or Me.TipoCambioDolar = 0) Then

                    TipoCambio_Factura = TipoCambioDolar
                Else
                    TipoCambio_Factura = mode("ValorVenta")
                End If
            End If


        Else
            TipoCambio_Factura = mode("ValorVenta")
        End If
        '   End If



        mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & Me.cbomoneda.SelectedValue)
        mode = mon(0)
        Tipo_CambioOpciones = mode("ValorVenta")
        If Me.Tipo = "ABO" Then
            If Me.cbomoneda.SelectedValue = 1 Then

            Else
                Tipo_CambioOpciones = Me.TipoCambioRecibo

            End If

        End If

        Conversion = TipoCambio_Factura / Tipo_CambioOpciones
    End Sub
    Sub sacar_conversion_vuelto(ByRef vueltoI As Double, ByVal p As Boolean)
        If Me.incializando Then Exit Sub
        Dim convertir As Double = 0
        Dim tipoCambio As Double
        If Not Me.loSaqueAqui Then
            If p Then
                mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & Me.codmod)
                mode = mon(0)
                tipoCambio = mode("ValorCompra")
            Else
                mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & Me.monedaVuelto)
                mode = mon(0)
                tipoCambio = mode("ValorCompra")
            End If
        Else
            If p Then
                mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & Me.codmod)
                mode = mon(0)
                tipoCambio = mode("ValorVenta")
            Else
                mon = Me.DataSet_Opciones_Pago1.Moneda.Select("CodMoneda =" & Me.monedaVuelto)
                mode = mon(0)
                tipoCambio = mode("ValorVenta")
            End If
        End If


        mon = Me.DataSet_Opciones_Pago1.Moneda_Vuelto.Select("CodMoneda =" & Me.ComboBoxConvertir.SelectedValue)
        Me.monedaVuelto = Me.ComboBoxConvertir.SelectedValue
        mode = mon(0)
        If Not Me.loSaqueAqui Then
            convertir = tipoCambio / mode("ValorCompra")
        Else
            convertir = tipoCambio / mode("ValorVenta")
        End If

        Me.vuelto = vueltoI * convertir
        Me.lblvuelto.Text = Me.vuelto
    End Sub
    Private Sub label_Tipo_Cambio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_Tipo_Cambio.TextChanged
        Me.txttc.Text = Me.label_Tipo_Cambio.Text
    End Sub


    Private Sub TxtVoucher_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtVoucher.TextChanged
        Me.Combo_Bancos.Text = TxtVoucher.Text
    End Sub



    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try

                Dim con As New Conexion
                Dim rs As SqlDataReader
                Dim enco As Boolean = False ' determina si el usuario fue o no encontrado

                If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open() ' si la conexion esta cerrada la abre

                rs = con.GetRecorset(Me.SqlConnection1, "Select Cedula, Nombre from usuarios where Clave_Interna = '" & Me.txtUsuario.Text & "'")

                While rs.Read
                    enco = True
                    Me.cedu = rs("Cedula")
                    Me.nombre = rs("Nombre")
                End While

                rs.Close()

                If Not enco Then
                    MsgBox("Clave incorrecta, digitela de nuevo", MsgBoxStyle.Exclamation)
                    Me.txtUsuario.Text = ""
                    Exit Sub
                End If

                If Not Me.Buscar_Apertura(Me.cedu) Then
                    Me.txtUsuario.Focus()
                Else : inicializar()
                End If


            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

    End Sub


    Private Sub Combo_Bancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Bancos.SelectedIndexChanged
        If rbtranferencia.Checked = True Then
            Try
                cargarCuentasBancarias(Combo_Bancos.Text)
            Catch ex As Exception
            End Try

        End If
    End Sub

    Private Sub cargarbancos()
        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open() ' si la conexion esta cerrada la abre
        Dim ds As New DataSet
        Dim mycomand As SqlDataAdapter = New SqlDataAdapter("Select codigo_banco,descripcion from bancos.dbo.bancos", SqlConnection1)
        mycomand.Fill(ds, "bancos")
        Combo_Bancos.DataSource = ds.Tables("bancos")
        Combo_Bancos.ValueMember = "codigo_banco"
        Combo_Bancos.DisplayMember = "descripcion"
    End Sub

    Private Sub cargarCuentasBancarias(ByVal nombreCuenta As String)
        Dim myComand As SqlDataAdapter = New SqlDataAdapter("Select c.id_cuentaBancaria,c.Cuenta, c.Cod_Moneda from bancos.dbo.cuentas_bancarias as c, bancos.dbo.bancos as b where c.codigo_banco=b.codigo_banco and b.descripcion = '" & nombreCuenta & "' and c.cod_moneda = " & Me.cbomoneda.SelectedValue, SqlConnection1)
        myComand.Fill(ds, "Cuentas")
        ComboBox1.DataSource = ds.Tables("Cuentas")
        ComboBox1.ValueMember = "id_cuentaBancaria"
        ComboBox1.DisplayMember = "Cuenta"
    End Sub
    Function recuperarId_Cuenta(ByVal CuentaBancaria As String) As Integer
        Dim i As Integer = 0
        For i = 0 To Me.ds.Tables("Cuentas").Rows.Count - 1
            If Me.ds.Tables("Cuentas").Rows(i).Item("Cuenta") = CuentaBancaria Then
                Return Me.ds.Tables("Cuentas").Rows(i).Item("id_cuentaBancaria")
            End If
        Next
    End Function


    Sub Registra_Deposito()
        Try
            Dim i As Integer = 0
            For i = 0 To Me.DataSet_Opciones_Pago1.OpcionesDePago.Count - 1
                If Me.DataSet_Opciones_Pago1.OpcionesDePago(i).FormaPago = "TRA" Then

                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").AddNew()
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("NumeroDocumento") = Me.DataSet_Opciones_Pago1.OpcionesDePago(i).Documento()
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Fecha") = fecha
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("monto") = Me.DataSet_Opciones_Pago1.OpcionesDePago(i).MontoPago
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Concepto") = Me.Tipo + Me.Factura.ToString
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Anulado") = 0
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Conciliado") = 0
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Contabilizado") = 1
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Ced_Usuario") = cedu
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Asiento") = 0
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Num_Conciliacion") = 0
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("CodigoMoneda") = Me.DataSet_Opciones_Pago1.OpcionesDePago(i).CodMoneda
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("TipoCambio") = Me.DataSet_Opciones_Pago1.OpcionesDePago(i).TipoCambio

                    Dim j As Integer = 0
                    For j = 0 To Me.DataSet_Opciones_Pago1.Detalle_pago_caja.Count - 1
                        If Me.DataSet_Opciones_Pago1.Detalle_pago_caja(j).Id_ODP = Me.DataSet_Opciones_Pago1.OpcionesDePago(i).id Then
                            Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Id_CuentaBancaria") = recuperarId_Cuenta(Me.DataSet_Opciones_Pago1.Detalle_pago_caja(j).cuentaBancaria)
                            Dim dt As New DataTable
                            cFunciones.Llenar_Tabla_Generico("Select * From Deposito WHERE NumeroDocumento = " & Me.DataSet_Opciones_Pago1.Detalle_pago_caja(i).Documento & " AND Id_CuentaBancaria = " & Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("Id_CuentaBancaria"), dt, GetSetting("SEESOFT", "BANCOS", "conexion"))
                            If dt.Rows.Count > 0 Then
                                MsgBox("YA ESTE DEPOSITO ESTA REGISTRADO, vuelva a incluir las opciones de pago correctas")
                                Me.estaMalo = True
                                Exit Sub

                            End If
                            Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").Current("NumeroDocumento") = Me.DataSet_Opciones_Pago1.Detalle_pago_caja(i).Documento
                        End If

                    Next
                    Me.BindingContext(Me.DataSet_Opciones_Pago1, "Deposito").EndCurrentEdit()

                End If
            Next



        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation)
        End Try

    End Sub


    Function Valida_Conciliacion(ByVal fecha As String) As Boolean
        Dim scomando As String
        Dim reader As SqlDataReader
        Dim comando As SqlCommand
        Dim respuesta As String
        Dim fecha_doc As Date
        Dim fecha_con As Date
        Try
            If Me.SqlConnection2.State = ConnectionState.Closed Then
                Me.SqlConnection2.Open()
            End If
            scomando = "SELECT MAX(Fecha) FROM dbo.Conciliacion  ORDER BY MAX(Fecha)"
            comando = New SqlCommand(scomando, Me.SqlConnection2)
            comando.CommandType = CommandType.Text
            reader = comando.ExecuteReader
            reader.Read()
            respuesta = reader.GetSqlValue(0).ToString
            reader.Close()
            Me.SqlConnection2.Close()
            fecha_doc = FormatDateTime(fecha, DateFormat.ShortDate)
            fecha_con = FormatDateTime(respuesta, DateFormat.ShortDate)

            If fecha_con > fecha_doc Then 'La fecha de conciliacion es mas reciente
                Return True               'que la fecha del documento.
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function


    Private Sub TxtDocumentoF_KeyPress(ByVal sender As System.Object, ByVal e As Windows.Forms.KeyPressEventArgs) Handles TxtDocumentoF.KeyPress
        ' en la siguiente l�nea de c�digo se comprueba si el caracter es d�gito...
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then
            ' de igual forma se podr�a comprobar si es caracter: e.KeyChar.IsLetter
            ' si es un caracter minusculas: e.KeyChar.IsLower ...etc
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub ComboBoxConvertir_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxConvertir.SelectedValueChanged
        Me.sacar_conversion_vuelto(vuelto, False)
    End Sub


    Private Sub Combo_Tarjeta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Tarjeta.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxConvertir_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxConvertir.SelectedIndexChanged

    End Sub
End Class
