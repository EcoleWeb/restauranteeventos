Public Class FormOpcionesPago
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents DataSetCierreDiario1 As DataSetCierreDiario
    Friend WithEvents ButtonPasarA As System.Windows.Forms.Button
    Friend WithEvents TextBoxApertura As System.Windows.Forms.TextBox
    Friend WithEvents ButtonCambiarForma As System.Windows.Forms.Button
    Friend WithEvents GroupBoxMover As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxNuevoNumAper As System.Windows.Forms.TextBox
    Friend WithEvents GroupBoxTipoPago As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxTipoPago As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBoxPagoTarjeta As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonCambiarTP As System.Windows.Forms.Button
    Friend WithEvents GroupBoxMP As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonCambiarMP As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTipoTarjeta As System.Windows.Forms.ComboBox
    Friend WithEvents TextBoxNumTarj As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxAutorizacion As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxVoucher As System.Windows.Forms.TextBox
    Friend WithEvents ButtonListo As System.Windows.Forms.Button
    Friend WithEvents GroupBoxControles As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
    Friend WithEvents TextBoxMontoPago As System.Windows.Forms.TextBox
    Friend WithEvents GroupBoxMoneda As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonMoneda As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.DataSetCierreDiario1 = New DataSetCierreDiario
        Me.ButtonPasarA = New System.Windows.Forms.Button
        Me.TextBoxApertura = New System.Windows.Forms.TextBox
        Me.ButtonCambiarForma = New System.Windows.Forms.Button
        Me.GroupBoxMover = New System.Windows.Forms.GroupBox
        Me.TextBoxNuevoNumAper = New System.Windows.Forms.TextBox
        Me.GroupBoxTipoPago = New System.Windows.Forms.GroupBox
        Me.ComboBoxTipoPago = New System.Windows.Forms.ComboBox
        Me.ButtonCambiarTP = New System.Windows.Forms.Button
        Me.GroupBoxPagoTarjeta = New System.Windows.Forms.GroupBox
        Me.ButtonCancel = New System.Windows.Forms.Button
        Me.ButtonListo = New System.Windows.Forms.Button
        Me.TextBoxVoucher = New System.Windows.Forms.TextBox
        Me.TextBoxAutorizacion = New System.Windows.Forms.TextBox
        Me.TextBoxNumTarj = New System.Windows.Forms.TextBox
        Me.ComboBoxTipoTarjeta = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBoxMP = New System.Windows.Forms.GroupBox
        Me.ButtonCambiarMP = New System.Windows.Forms.Button
        Me.GroupBoxControles = New System.Windows.Forms.GroupBox
        Me.TextBoxMontoPago = New System.Windows.Forms.TextBox
        Me.GroupBoxMoneda = New System.Windows.Forms.GroupBox
        Me.ButtonMoneda = New System.Windows.Forms.Button
        Me.ComboBoxMoneda = New System.Windows.Forms.ComboBox
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetCierreDiario1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxMover.SuspendLayout()
        Me.GroupBoxTipoPago.SuspendLayout()
        Me.GroupBoxPagoTarjeta.SuspendLayout()
        Me.GroupBoxMP.SuspendLayout()
        Me.GroupBoxControles.SuspendLayout()
        Me.GroupBoxMoneda.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGrid1
        '
        Me.DataGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGrid1.DataMember = "OpcionesPago"
        Me.DataGrid1.DataSource = Me.DataSetCierreDiario1
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(8, 0)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(840, 336)
        Me.DataGrid1.TabIndex = 0
        '
        'DataSetCierreDiario1
        '
        Me.DataSetCierreDiario1.DataSetName = "DataSetCierreDiario"
        Me.DataSetCierreDiario1.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'ButtonPasarA
        '
        Me.ButtonPasarA.Location = New System.Drawing.Point(136, 16)
        Me.ButtonPasarA.Name = "ButtonPasarA"
        Me.ButtonPasarA.Size = New System.Drawing.Size(56, 23)
        Me.ButtonPasarA.TabIndex = 1
        Me.ButtonPasarA.Text = "Guardar"
        '
        'TextBoxApertura
        '
        Me.TextBoxApertura.Location = New System.Drawing.Point(8, 16)
        Me.TextBoxApertura.Name = "TextBoxApertura"
        Me.TextBoxApertura.ReadOnly = True
        Me.TextBoxApertura.Size = New System.Drawing.Size(56, 20)
        Me.TextBoxApertura.TabIndex = 2
        Me.TextBoxApertura.Text = "0"
        '
        'ButtonCambiarForma
        '
        Me.ButtonCambiarForma.Location = New System.Drawing.Point(8, 24)
        Me.ButtonCambiarForma.Name = "ButtonCambiarForma"
        Me.ButtonCambiarForma.Size = New System.Drawing.Size(72, 23)
        Me.ButtonCambiarForma.TabIndex = 4
        Me.ButtonCambiarForma.Text = "Eliminar"
        '
        'GroupBoxMover
        '
        Me.GroupBoxMover.Controls.Add(Me.TextBoxNuevoNumAper)
        Me.GroupBoxMover.Controls.Add(Me.ButtonPasarA)
        Me.GroupBoxMover.Controls.Add(Me.TextBoxApertura)
        Me.GroupBoxMover.Location = New System.Drawing.Point(88, 16)
        Me.GroupBoxMover.Name = "GroupBoxMover"
        Me.GroupBoxMover.Size = New System.Drawing.Size(200, 48)
        Me.GroupBoxMover.TabIndex = 6
        Me.GroupBoxMover.TabStop = False
        Me.GroupBoxMover.Text = "Mover a otra apertura"
        '
        'TextBoxNuevoNumAper
        '
        Me.TextBoxNuevoNumAper.Location = New System.Drawing.Point(72, 16)
        Me.TextBoxNuevoNumAper.Name = "TextBoxNuevoNumAper"
        Me.TextBoxNuevoNumAper.Size = New System.Drawing.Size(64, 20)
        Me.TextBoxNuevoNumAper.TabIndex = 2
        Me.TextBoxNuevoNumAper.Text = "0"
        Me.TextBoxNuevoNumAper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBoxTipoPago
        '
        Me.GroupBoxTipoPago.Controls.Add(Me.ComboBoxTipoPago)
        Me.GroupBoxTipoPago.Controls.Add(Me.ButtonCambiarTP)
        Me.GroupBoxTipoPago.Location = New System.Drawing.Point(296, 16)
        Me.GroupBoxTipoPago.Name = "GroupBoxTipoPago"
        Me.GroupBoxTipoPago.Size = New System.Drawing.Size(160, 48)
        Me.GroupBoxTipoPago.TabIndex = 7
        Me.GroupBoxTipoPago.TabStop = False
        Me.GroupBoxTipoPago.Text = "Cambiar tipo pago"
        '
        'ComboBoxTipoPago
        '
        Me.ComboBoxTipoPago.Items.AddRange(New Object() {"EFE", "TAR", "TRA", "CHE"})
        Me.ComboBoxTipoPago.Location = New System.Drawing.Point(8, 16)
        Me.ComboBoxTipoPago.Name = "ComboBoxTipoPago"
        Me.ComboBoxTipoPago.Size = New System.Drawing.Size(80, 21)
        Me.ComboBoxTipoPago.TabIndex = 2
        '
        'ButtonCambiarTP
        '
        Me.ButtonCambiarTP.Location = New System.Drawing.Point(96, 16)
        Me.ButtonCambiarTP.Name = "ButtonCambiarTP"
        Me.ButtonCambiarTP.Size = New System.Drawing.Size(56, 23)
        Me.ButtonCambiarTP.TabIndex = 1
        Me.ButtonCambiarTP.Text = "Guardar"
        '
        'GroupBoxPagoTarjeta
        '
        Me.GroupBoxPagoTarjeta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.ButtonCancel)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.ButtonListo)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.TextBoxVoucher)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.TextBoxAutorizacion)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.TextBoxNumTarj)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.ComboBoxTipoTarjeta)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.Label4)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.Label3)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.Label2)
        Me.GroupBoxPagoTarjeta.Controls.Add(Me.Label1)
        Me.GroupBoxPagoTarjeta.Location = New System.Drawing.Point(256, 136)
        Me.GroupBoxPagoTarjeta.Name = "GroupBoxPagoTarjeta"
        Me.GroupBoxPagoTarjeta.Size = New System.Drawing.Size(256, 200)
        Me.GroupBoxPagoTarjeta.TabIndex = 8
        Me.GroupBoxPagoTarjeta.TabStop = False
        Me.GroupBoxPagoTarjeta.Text = "Informaci�n Pago Tarjeta"
        Me.GroupBoxPagoTarjeta.Visible = False
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(168, 120)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.TabIndex = 9
        Me.ButtonCancel.Text = "Cancel"
        '
        'ButtonListo
        '
        Me.ButtonListo.Location = New System.Drawing.Point(88, 120)
        Me.ButtonListo.Name = "ButtonListo"
        Me.ButtonListo.TabIndex = 8
        Me.ButtonListo.Text = "Listo"
        '
        'TextBoxVoucher
        '
        Me.TextBoxVoucher.Location = New System.Drawing.Point(88, 88)
        Me.TextBoxVoucher.Name = "TextBoxVoucher"
        Me.TextBoxVoucher.Size = New System.Drawing.Size(152, 20)
        Me.TextBoxVoucher.TabIndex = 7
        Me.TextBoxVoucher.Text = "0"
        '
        'TextBoxAutorizacion
        '
        Me.TextBoxAutorizacion.Location = New System.Drawing.Point(88, 64)
        Me.TextBoxAutorizacion.Name = "TextBoxAutorizacion"
        Me.TextBoxAutorizacion.Size = New System.Drawing.Size(152, 20)
        Me.TextBoxAutorizacion.TabIndex = 6
        Me.TextBoxAutorizacion.Text = "0"
        '
        'TextBoxNumTarj
        '
        Me.TextBoxNumTarj.Location = New System.Drawing.Point(88, 40)
        Me.TextBoxNumTarj.Name = "TextBoxNumTarj"
        Me.TextBoxNumTarj.Size = New System.Drawing.Size(152, 20)
        Me.TextBoxNumTarj.TabIndex = 5
        Me.TextBoxNumTarj.Text = "0"
        '
        'ComboBoxTipoTarjeta
        '
        Me.ComboBoxTipoTarjeta.DataSource = Me.DataSetCierreDiario1.TipoTarjeta
        Me.ComboBoxTipoTarjeta.DisplayMember = "Nombre"
        Me.ComboBoxTipoTarjeta.Location = New System.Drawing.Point(88, 16)
        Me.ComboBoxTipoTarjeta.Name = "ComboBoxTipoTarjeta"
        Me.ComboBoxTipoTarjeta.Size = New System.Drawing.Size(152, 21)
        Me.ComboBoxTipoTarjeta.TabIndex = 4
        Me.ComboBoxTipoTarjeta.ValueMember = "Id"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 23)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Voucher:"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 23)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Autorizaci�n:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tarjeta:"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TipoTarjeta:"
        '
        'GroupBoxMP
        '
        Me.GroupBoxMP.Controls.Add(Me.TextBoxMontoPago)
        Me.GroupBoxMP.Controls.Add(Me.ButtonCambiarMP)
        Me.GroupBoxMP.Location = New System.Drawing.Point(472, 16)
        Me.GroupBoxMP.Name = "GroupBoxMP"
        Me.GroupBoxMP.Size = New System.Drawing.Size(160, 48)
        Me.GroupBoxMP.TabIndex = 11
        Me.GroupBoxMP.TabStop = False
        Me.GroupBoxMP.Text = "Cambiar Monto Pago"
        '
        'ButtonCambiarMP
        '
        Me.ButtonCambiarMP.Location = New System.Drawing.Point(96, 16)
        Me.ButtonCambiarMP.Name = "ButtonCambiarMP"
        Me.ButtonCambiarMP.Size = New System.Drawing.Size(56, 23)
        Me.ButtonCambiarMP.TabIndex = 1
        Me.ButtonCambiarMP.Text = "Guardar"
        '
        'GroupBoxControles
        '
        Me.GroupBoxControles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxControles.Controls.Add(Me.GroupBoxMoneda)
        Me.GroupBoxControles.Controls.Add(Me.GroupBoxMP)
        Me.GroupBoxControles.Controls.Add(Me.ButtonCambiarForma)
        Me.GroupBoxControles.Controls.Add(Me.GroupBoxMover)
        Me.GroupBoxControles.Controls.Add(Me.GroupBoxTipoPago)
        Me.GroupBoxControles.Location = New System.Drawing.Point(8, 336)
        Me.GroupBoxControles.Name = "GroupBoxControles"
        Me.GroupBoxControles.Size = New System.Drawing.Size(840, 72)
        Me.GroupBoxControles.TabIndex = 13
        Me.GroupBoxControles.TabStop = False
        Me.GroupBoxControles.Text = "controles"
        '
        'TextBoxMontoPago
        '
        Me.TextBoxMontoPago.Location = New System.Drawing.Point(8, 16)
        Me.TextBoxMontoPago.Name = "TextBoxMontoPago"
        Me.TextBoxMontoPago.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxMontoPago.TabIndex = 3
        Me.TextBoxMontoPago.Text = "0"
        Me.TextBoxMontoPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBoxMoneda
        '
        Me.GroupBoxMoneda.Controls.Add(Me.ComboBoxMoneda)
        Me.GroupBoxMoneda.Controls.Add(Me.ButtonMoneda)
        Me.GroupBoxMoneda.Location = New System.Drawing.Point(648, 16)
        Me.GroupBoxMoneda.Name = "GroupBoxMoneda"
        Me.GroupBoxMoneda.Size = New System.Drawing.Size(184, 48)
        Me.GroupBoxMoneda.TabIndex = 12
        Me.GroupBoxMoneda.TabStop = False
        Me.GroupBoxMoneda.Text = "Cambiar Moneda"
        '
        'ButtonMoneda
        '
        Me.ButtonMoneda.Location = New System.Drawing.Point(96, 16)
        Me.ButtonMoneda.Name = "ButtonMoneda"
        Me.ButtonMoneda.Size = New System.Drawing.Size(56, 23)
        Me.ButtonMoneda.TabIndex = 1
        Me.ButtonMoneda.Text = "Guardar"
        '
        'ComboBoxMoneda
        '
        Me.ComboBoxMoneda.Items.AddRange(New Object() {"COLON", "DOLAR"})
        Me.ComboBoxMoneda.Location = New System.Drawing.Point(8, 16)
        Me.ComboBoxMoneda.Name = "ComboBoxMoneda"
        Me.ComboBoxMoneda.Size = New System.Drawing.Size(80, 21)
        Me.ComboBoxMoneda.TabIndex = 2
        '
        'FormOpcionesPago
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(856, 414)
        Me.Controls.Add(Me.GroupBoxControles)
        Me.Controls.Add(Me.GroupBoxPagoTarjeta)
        Me.Controls.Add(Me.DataGrid1)
        Me.Name = "FormOpcionesPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones Pago"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetCierreDiario1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxMover.ResumeLayout(False)
        Me.GroupBoxTipoPago.ResumeLayout(False)
        Me.GroupBoxPagoTarjeta.ResumeLayout(False)
        Me.GroupBoxMP.ResumeLayout(False)
        Me.GroupBoxControles.ResumeLayout(False)
        Me.GroupBoxMoneda.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public NApertura As Integer = 0
    Public BaseDatos As String

    Dim tipo As String = "EFE"
    Private Sub FormOpcionesPago_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        actualizar()
        Me.TextBoxApertura.Text = Me.NApertura
        Me.Text = "Opciones de Pago : " & Me.NApertura

        cFunciones.Llenar_Tabla_Generico("Select * From TipoTarjeta", Me.DataSetCierreDiario1.TipoTarjeta, GetSetting("SeeSoft", "Hotel", "Conexion"))

    End Sub
    Sub actualizar()
        Me.DataSetCierreDiario1.OpcionesPago.Clear()
        cFunciones.Llenar_Tabla_Generico("SELECT OpcionesDePago.id AS Id_Opciones, OpcionesDePago.Documento, OpcionesDePago.FormaPago AS [Tipo Pago], OpcionesDePago.MontoPago AS Monto," & _
                       " Moneda.MonedaNombre AS Moneda, '" & BaseDatos & "' AS BD, TipoDocumento As TipoDoc, OpcionesDePago.CodMoneda As MonedaCod, TipoCambio, Fecha" & _
                        " FROM OpcionesDePago INNER JOIN " & _
                      " Moneda ON OpcionesDePago.CodMoneda = Moneda.CodMoneda WHERE Numapertura = " & NApertura, Me.DataSetCierreDiario1.OpcionesPago, GetSetting("SEESOFT", BaseDatos, "Conexion"))
    End Sub
    Private Sub ButtonPasarA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPasarA.Click
        If MsgBox("�Desea realmente cambiar esta apertura por la apertura digitada?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim cx As New Conexion
            cx.Conectar1(, Me.BaseDatos)
            cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set Numapertura = " & Me.TextBoxNuevoNumAper.Text & " WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
            cx.DesConectar(cx.sQlconexion)
            Me.actualizar()
        End If
    End Sub

    Private Sub ButtonCambiarTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCambiarTP.Click
        tipo = Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Tipo Pago")
        If tipo.Equals("TAR") Then
            DataSetCierreDiario1.Detalle_pago_caja.Clear()
            cFunciones.Llenar_Tabla_Generico("Select * From Detalle_pago_caja WHERE Id_ODP = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"), Me.DataSetCierreDiario1.Detalle_pago_caja, GetSetting("SeeSOFT", Me.BaseDatos, "conexion"))
            If Me.DataSetCierreDiario1.Detalle_pago_caja.Count > 0 Then
                Me.TextBoxNumTarj.Text = Me.DataSetCierreDiario1.Detalle_pago_caja(0).Referencia
                Me.TextBoxVoucher.Text = Me.DataSetCierreDiario1.Detalle_pago_caja(0).Documento
                Me.TextBoxAutorizacion.Text = Me.DataSetCierreDiario1.Detalle_pago_caja(0).ReferenciaDoc
                Me.ComboBoxTipoTarjeta.SelectedValue = Me.DataSetCierreDiario1.Detalle_pago_caja(0).ReferenciaTipo

            End If

        End If
        If Me.ComboBoxTipoPago.Text.Equals("TAR") Then
            Me.GroupBoxControles.Enabled = False
            Me.GroupBoxPagoTarjeta.Enabled = True
            Me.GroupBoxPagoTarjeta.Visible = True
        ElseIf Me.ComboBoxTipoPago.Text.Equals("EFE") And tipo = "EFE" Then
            MsgBox("Ya esta en EFE")
        ElseIf tipo.Equals("TAR") And Me.ComboBoxTipoPago.Text.Equals("EFE") Then
            If MsgBox("�Desea realmente la opcion de pago?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim cx As New Conexion
                cx.Conectar1("SEESOFT", Me.BaseDatos)
                cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set FormaPago = 'EFE' WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
                cx.SlqExecute(cx.sQlconexion, "Delete Detalle_pago_caja WHERE id_ODP = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
                cx.DesConectar(cx.sQlconexion)
                Me.actualizar()

            End If

        End If


    End Sub

    Private Sub ButtonCambiarForma_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCambiarForma.Click
        If MsgBox("�Desea realmente eliminar esta opcion de pago?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim cx As New Conexion
            cx.Conectar1(, Me.BaseDatos)
            cx.SlqExecute(cx.sQlconexion, "Delete OpcionesDePago WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))

            cx.DesConectar(cx.sQlconexion)
            Me.actualizar()
        End If
    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Me.GroupBoxControles.Enabled = True
        Me.GroupBoxPagoTarjeta.Enabled = True
        Me.GroupBoxPagoTarjeta.Visible = False
        Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Tipo Pago") = tipo


    End Sub

    Private Sub ButtonListo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonListo.Click

        If MsgBox("�Desea realmente cambiar la opcion de pago?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim factura As String = Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Documento")
            Dim tipoFactura As String = Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("TipoDoc")

            Dim cx As New Conexion
            cx.Conectar1(, Me.BaseDatos)
            cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set FormaPago = 'TAR' WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
            cx.SlqExecute(cx.sQlconexion, "DELETE Detalle_pago_caja WHERE id_ODP = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
            Dim ingresa As String = ""
            If tipo.Equals("TAR") Then

                If Me.DataSetCierreDiario1.Detalle_pago_caja.Count > 0 Then
                    ingresa = "INSERT INTO Detalle_pago_caja" & _
                                                " (NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTipo, ReferenciaDoc, Moneda, TipoCambio, Id_ODP, Cancelado, " & _
                                                " Deposito)" & _
                                                        " VALUES     (" & factura & ", '" & tipoFactura & "', '" & tipo & "','" & Me.TextBoxNumTarj.Text & "','" & Me.TextBoxVoucher.Text & "', " & Me.ComboBoxTipoTarjeta.SelectedValue & ", '" & Me.TextBoxAutorizacion.Text & "', " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("MonedaCod") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("TipoCambio") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones") & ", " & Me.DataSetCierreDiario1.Detalle_pago_caja(0).Cancelado & "," & Me.DataSetCierreDiario1.Detalle_pago_caja(0).Deposito & ")"
                Else
                    ingresa = "INSERT INTO Detalle_pago_caja" & _
                                                " (NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTipo, ReferenciaDoc, Moneda, TipoCambio, Id_ODP, Cancelado, " & _
                                                " Deposito)" & _
                                                        " VALUES     (" & factura & ", '" & tipoFactura & "', '" & tipo & "','" & Me.TextBoxNumTarj.Text & "','" & Me.TextBoxVoucher.Text & "', " & Me.ComboBoxTipoTarjeta.SelectedValue & ", '" & Me.TextBoxAutorizacion.Text & "', " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("MonedaCod") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("TipoCambio") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones") & ",0,0)"
                End If



            Else

                ingresa = "INSERT INTO Detalle_pago_caja" & _
                                                                " (NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTipo, ReferenciaDoc, Moneda, TipoCambio, Id_ODP, Cancelado, " & _
                                                                " Deposito)" & _
                                                                        " VALUES     (" & factura & ", '" & tipoFactura & "', '" & tipo & "','" & Me.TextBoxNumTarj.Text & "','" & Me.TextBoxVoucher.Text & "', " & Me.ComboBoxTipoTarjeta.SelectedValue & ", '" & Me.TextBoxAutorizacion.Text & "', " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("MonedaCod") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("TipoCambio") & ", " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones") & ",0,0)"
            End If
            cx.SlqExecute(cx.sQlconexion, ingresa)
            cx.DesConectar(cx.sQlconexion)
            Me.actualizar()

        End If
        Me.GroupBoxControles.Enabled = True
        Me.GroupBoxPagoTarjeta.Enabled = True
        Me.GroupBoxPagoTarjeta.Visible = False


    End Sub

    Private Sub ButtonCambiarMP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCambiarMP.Click
        If MsgBox("�Desea realmente cambiar el monto de la opcion de pago?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim cx As New Conexion
            cx.Conectar1(, Me.BaseDatos)
            cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set MontoPago = " & Me.TextBoxMontoPago.Text & " WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
            cx.DesConectar(cx.sQlconexion)
            Me.actualizar()
        End If
    End Sub

    Private Sub ButtonMoneda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMoneda.Click
        If MsgBox("Desea cambiar la moneda", MsgBoxStyle.YesNo) Then
            Dim cx As New Conexion
            cx.Conectar1(, Me.BaseDatos)
            If Me.ComboBoxMoneda.Text.Equals("COLON") Then
                cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set CodMoneda = 1 WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
                cx.SlqExecute(cx.sQlconexion, "UPDATE Detalle_pago_caja Set Moneda = 1 WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
            Else
                cx.SlqExecute(cx.sQlconexion, "UPDATE OpcionesDePago Set CodMoneda = 2 WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))
                cx.SlqExecute(cx.sQlconexion, "UPDATE Detalle_pago_caja Set Moneda = 2 WHERE id = " & Me.BindingContext(Me.DataSetCierreDiario1, "OpcionesPago").Current("Id_Opciones"))

            End If
            cx.DesConectar(cx.sQlconexion)
            Me.actualizar()

        End If
    End Sub
End Class
