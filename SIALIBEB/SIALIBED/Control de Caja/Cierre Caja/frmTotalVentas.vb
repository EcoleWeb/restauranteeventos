﻿Public Class frmTotalVentas
    Public pubApertura As Integer = 0

    Private Sub frmTotalVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        spCargarDatos()
    End Sub

    Private Sub spCargarDatos()
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable

        Try
            If pubApertura <> 0 Then

                sql.CommandText = " SELECT  * FROM [Restaurante].[dbo].[vs_R_VentasCierreCaja] " & _
                          " where [Num_Apertura] = '" & pubApertura & "' And [Proveniencia_Venta] = '" & User_Log.PuntoVenta & "'"


                cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))

                If dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("Tipo").Equals("CON") Then
                            txVentaGravadaCON.EditValue += dt.Rows(i).Item("SubTotalGravada")
                            txVentaExcentaCON.EditValue += dt.Rows(i).Item("SubTotalExento")
                            txSaloneroCON.EditValue += dt.Rows(i).Item("Monto_Saloero")
                            txImpVentaCON.EditValue += dt.Rows(i).Item("Imp_Venta")
                            txExtraPropinaCON.EditValue += dt.Rows(i).Item("ExtraPropina")
                            txSubTotalGeneralCON.EditValue += dt.Rows(i).Item("SubTotal")
                            txTotalGeneralCON.EditValue += dt.Rows(i).Item("Total")
                        End If
                        If dt.Rows(i).Item("Tipo").Equals("CRE") Then
                            txVentaGravadaCRE.EditValue += dt.Rows(i).Item("SubTotalGravada")
                            txVentaExcentaCRE.EditValue += dt.Rows(i).Item("SubTotalExento")
                            txSaloneroCRE.EditValue += dt.Rows(i).Item("Monto_Saloero")
                            txImpVentaCRE.EditValue += dt.Rows(i).Item("Imp_Venta")
                            txExtraPropinaCRE.EditValue += dt.Rows(i).Item("ExtraPropina")
                            txSubTotalGeneralCRE.EditValue += dt.Rows(i).Item("SubTotal")
                            txTotalGeneralCRE.EditValue += dt.Rows(i).Item("Total")
                        End If
                        If dt.Rows(i).Item("Tipo").Equals("CAR") Then
                            txVentaGravadaCAR.EditValue += dt.Rows(i).Item("SubTotalGravada")
                            txVentaExcentaCAR.EditValue = dt.Rows(i).Item("SubTotalExento")
                            txSaloneroCAR.EditValue = dt.Rows(i).Item("Monto_Saloero")
                            txImpVentaCAR.EditValue = dt.Rows(i).Item("Imp_Venta")
                            txExtraPropinaCAR.EditValue = dt.Rows(i).Item("ExtraPropina")
                            txSubTotalGeneralCAR.EditValue = dt.Rows(i).Item("SubTotal")
                            txTotalGeneralCAR.EditValue = dt.Rows(i).Item("Total")
                        End If
                
                    Next
               
                End If
                txVentaGravada.Text = Format(CDbl(txVentaGravadaCON.EditValue) + CDbl(txVentaGravadaCRE.EditValue) + CDbl(txVentaGravadaCAR.EditValue), "##,##0.00")
                txVentaExcenta.Text = Format(CDbl(txVentaExcentaCON.EditValue) + CDbl(txVentaExcentaCRE.EditValue) + CDbl(txVentaExcentaCAR.EditValue), "##,##0.00")
                txSalonero.Text = Format(CDbl(txSaloneroCON.EditValue) + CDbl(txSaloneroCRE.EditValue) + CDbl(txSaloneroCAR.EditValue), "##,##0.00")
                txImpVenta.Text = Format(CDbl(txImpVentaCON.EditValue) + CDbl(txImpVentaCRE.EditValue) + CDbl(txImpVentaCAR.EditValue), "##,##0.00")
                txExtraPropina.Text = Format(CDbl(txExtraPropinaCON.EditValue) + CDbl(txExtraPropinaCRE.EditValue) + CDbl(txExtraPropinaCAR.EditValue), "##,##0.00")
                txSubTotalGeneral.Text = Format(CDbl(txSubTotalGeneralCON.EditValue) + CDbl(txSubTotalGeneralCRE.EditValue) + CDbl(txSubTotalGeneralCAR.EditValue), "##,##0.00")
                txTotalGeneral.Text = Format(CDbl(txTotalGeneralCON.EditValue) + CDbl(txTotalGeneralCRE.EditValue) + CDbl(txTotalGeneralCAR.EditValue), "##,##0.00")
                lbApertura.Text = pubApertura
            End If
            btAceptar.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK)
            Me.Close()
        End Try
    End Sub

    Private Sub ButtonBuscar_Click(sender As Object, e As EventArgs) Handles btAceptar.Click
        Me.Close()
    End Sub
End Class