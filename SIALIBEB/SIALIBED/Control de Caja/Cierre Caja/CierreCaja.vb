Imports System.data.SqlClient


Public Class CierreCaja
    Inherits Detalle_Cortesia

#Region "Variables Publicas"
    Public Usuario As String
    Public Cedula_Usuario As String
    Public tabla As New DataTable
    Public tablaresumen As New DataTable
    Public tablacierre As New DataTable
    Public SubTotal As Double
    Public Total As Double
    Public Devolucion As Double
    Public TipoCambioDolar As Double
    Public TipoCambioEuro As Double
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents Textentradas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TxtSalidas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonAperturas As System.Windows.Forms.Button
    Friend WithEvents check_PVE As System.Windows.Forms.CheckBox
    Friend WithEvents btVentas As System.Windows.Forms.Button
#End Region

#Region "Variables Privadas"
    Dim PMU As PerfilModulo_Class
    Dim MontoSalonero As Double = 0
    Dim Imp_Venta As Double = 0
    Friend WithEvents txTotalComision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbTotalComision As System.Windows.Forms.Label
    Dim subTotalVenta As Double = 0
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSetCierreCaja1 As SIALIBEB.DataSetCierreCaja
    Friend WithEvents AdapterCierre As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterOpciones As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtnomcajero As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtcodcajero As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents DTPFechafinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTPFechainicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btndone As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtfechaapertura As System.Windows.Forms.Label
    Friend WithEvents txtcodaperturacajero As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgResumen As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterTotalTope As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterArqueo As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AdapterTipoTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterDetallePago As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterVentas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterVentasContado As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterCobros As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents AdapterCierreMonto As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCierreTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents lblanulado As System.Windows.Forms.Label
    Friend WithEvents AdapterVentasCredito As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CierreCaja))
        Dim ColumnFilterInfo11 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo12 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo13 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo14 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo15 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo16 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo17 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo18 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo19 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo20 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.AdapterUsuarios = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.DataSetCierreCaja1 = New SIALIBEB.DataSetCierreCaja()
        Me.AdapterCierre = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterApertura = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterOpciones = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.txtnomcajero = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtcodcajero = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit()
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit()
        Me.DTPFechafinal = New System.Windows.Forms.DateTimePicker()
        Me.DTPFechainicial = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btndone = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtfechaapertura = New System.Windows.Forms.Label()
        Me.txtcodaperturacajero = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgResumen = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterTotalTope = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterArqueo = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterTarjeta = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Textentradas = New DevExpress.XtraEditors.TextEdit()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TxtSalidas = New DevExpress.XtraEditors.TextEdit()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.AdapterTipoTarjeta = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.AdapterDetallePago = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterVentas = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterVentasContado = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand12 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterCobros = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand13 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand13 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterCierreMonto = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand14 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand14 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterCierreTarjeta = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand15 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand15 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.lblanulado = New System.Windows.Forms.Label()
        Me.AdapterVentasCredito = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand16 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand16 = New System.Data.SqlClient.SqlCommand()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.ButtonAperturas = New System.Windows.Forms.Button()
        Me.check_PVE = New System.Windows.Forms.CheckBox()
        Me.btVentas = New System.Windows.Forms.Button()
        Me.txTotalComision = New DevExpress.XtraEditors.TextEdit()
        Me.lbTotalComision = New System.Windows.Forms.Label()
        CType(Me.DataSetCierreCaja1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Textentradas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSalidas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTotalComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 579)
        Me.ToolBar1.Size = New System.Drawing.Size(778, 56)
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(642, 522)
        '
        'TituloModulo
        '
        Me.TituloModulo.Text = "Cierre de Caja"
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(438, 598)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 60
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(555, 599)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(208, 13)
        Me.txtNombreUsuario.TabIndex = 61
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.ForeColor = System.Drawing.Color.Blue
        Me.TextBox6.Location = New System.Drawing.Point(516, 598)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox6.Size = New System.Drawing.Size(33, 13)
        Me.TextBox6.TabIndex = 59
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=.;Initial Catalog=Restaurante;Integrated Security=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterUsuarios
        '
        Me.AdapterUsuarios.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterUsuarios.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Id_Usuario", "Id_Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Perfil", "Perfil")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 50, "Cedula"), New System.Data.SqlClient.SqlParameter("@Id_Usuario", System.Data.SqlDbType.VarChar, 50, "Id_Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 30, "Clave_Entrada"), New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 30, "Clave_Interna"), New System.Data.SqlClient.SqlParameter("@Perfil", System.Data.SqlDbType.Int, 4, "Perfil")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Id_Usuario, Nombre, Clave_Entrada, Clave_Interna, Perfil FROM Usua" & _
    "rios"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'DataSetCierreCaja1
        '
        Me.DataSetCierreCaja1.DataSetName = "DataSetCierreCaja"
        Me.DataSetCierreCaja1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSetCierreCaja1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AdapterCierre
        '
        Me.AdapterCierre.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterCierre.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterCierre.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterCierre.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "cierrecaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroCierre", "NumeroCierre"), New System.Data.Common.DataColumnMapping("Cajera", "Cajera"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Apertura", "Apertura"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Devoluciones", "Devoluciones"), New System.Data.Common.DataColumnMapping("Subtotal", "Subtotal"), New System.Data.Common.DataColumnMapping("TotalSistema", "TotalSistema"), New System.Data.Common.DataColumnMapping("Equivalencia", "Equivalencia"), New System.Data.Common.DataColumnMapping("TravelCheckSistema", "TravelCheckSistema"), New System.Data.Common.DataColumnMapping("TravelCheckCajero", "TravelCheckCajero"), New System.Data.Common.DataColumnMapping("EfectivoColones", "EfectivoColones"), New System.Data.Common.DataColumnMapping("EfectivoDolares", "EfectivoDolares"), New System.Data.Common.DataColumnMapping("TarjetaColones", "TarjetaColones"), New System.Data.Common.DataColumnMapping("TarjetaDolares", "TarjetaDolares")})})
        Me.AdapterCierre.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TravelCheckSistema", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TravelCheckSistema", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TravelCheckSistema", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheckSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TravelCheckCajero", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TravelCheckCajero", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TravelCheckCajero", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheckCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_EfectivoColones", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_EfectivoDolares", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TarjetaColones", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TarjetaDolares", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 0, "Cajera"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 0, "Apertura"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 0, "Usuario"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 0, "NombreUsuario"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 0, "Devoluciones"), New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 0, "Subtotal"), New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 0, "TotalSistema"), New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 0, "Equivalencia"), New System.Data.SqlClient.SqlParameter("@TravelCheckSistema", System.Data.SqlDbType.Float, 0, "TravelCheckSistema"), New System.Data.SqlClient.SqlParameter("@TravelCheckCajero", System.Data.SqlDbType.Float, 0, "TravelCheckCajero"), New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = resources.GetString("SqlSelectCommand2.CommandText")
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 0, "Cajera"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 0, "Apertura"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 0, "Usuario"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 0, "NombreUsuario"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 0, "Devoluciones"), New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 0, "Subtotal"), New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 0, "TotalSistema"), New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 0, "Equivalencia"), New System.Data.SqlClient.SqlParameter("@TravelCheckSistema", System.Data.SqlDbType.Float, 0, "TravelCheckSistema"), New System.Data.SqlClient.SqlParameter("@TravelCheckCajero", System.Data.SqlDbType.Float, 0, "TravelCheckCajero"), New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares"), New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TravelCheckSistema", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TravelCheckSistema", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TravelCheckSistema", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheckSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TravelCheckCajero", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TravelCheckCajero", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TravelCheckCajero", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheckCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_EfectivoColones", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_EfectivoDolares", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TarjetaColones", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TarjetaDolares", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@NumeroCierre", System.Data.SqlDbType.Int, 4, "NumeroCierre")})
        '
        'AdapterApertura
        '
        Me.AdapterApertura.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterApertura.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterApertura.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.AdapterApertura.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula")})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
    "turacaja"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura")})
        '
        'AdapterOpciones
        '
        Me.AdapterOpciones.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterOpciones.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterOpciones.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterOpciones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpcionesDePago", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("TipoDocumento", "TipoDocumento"), New System.Data.Common.DataColumnMapping("MontoPago", "MontoPago"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Nombremoneda", "Nombremoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Numapertura", "Numapertura")})})
        Me.AdapterOpciones.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = resources.GetString("SqlInsertCommand4.CommandText")
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.Float, 8, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.Int, 4, "Numapertura")})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario," & _
    " Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura FROM OpcionesDe" & _
    "Pago"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.Float, 8, "Documento"), New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"), New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.Int, 4, "Numapertura"), New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.BigInt, 8, "id")})
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ButtonBuscar)
        Me.GroupBox1.Controls.Add(Me.DateEdit1)
        Me.GroupBox1.Controls.Add(Me.txtnomcajero)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtcodcajero)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TimeEdit2)
        Me.GroupBox1.Controls.Add(Me.TimeEdit1)
        Me.GroupBox1.Controls.Add(Me.DTPFechafinal)
        Me.GroupBox1.Controls.Add(Me.DTPFechainicial)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.btndone)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox1.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(256, 96)
        Me.GroupBox1.TabIndex = 62
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Per�odo Cierre"
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Enabled = False
        Me.ButtonBuscar.Location = New System.Drawing.Point(105, 29)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(41, 23)
        Me.ButtonBuscar.TabIndex = 15
        Me.ButtonBuscar.Text = "Buscar"
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'DateEdit1
        '
        Me.DateEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.Fecha", True))
        Me.DateEdit1.EditValue = New Date(2006, 5, 10, 0, 0, 0, 0)
        Me.DateEdit1.Location = New System.Drawing.Point(152, 32)
        Me.DateEdit1.Name = "DateEdit1"
        '
        '
        '
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.Enabled = False
        Me.DateEdit1.Size = New System.Drawing.Size(88, 23)
        Me.DateEdit1.TabIndex = 5
        '
        'txtnomcajero
        '
        Me.txtnomcajero.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtnomcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Nombre", True))
        Me.txtnomcajero.ForeColor = System.Drawing.SystemColors.Info
        Me.txtnomcajero.Location = New System.Drawing.Point(8, 72)
        Me.txtnomcajero.Name = "txtnomcajero"
        Me.txtnomcajero.Size = New System.Drawing.Size(240, 16)
        Me.txtnomcajero.TabIndex = 14
        Me.txtnomcajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label25.Location = New System.Drawing.Point(152, 16)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 16)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Fecha Cierre"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(112, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "C�digo Cajero"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodcajero
        '
        Me.txtcodcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Cajera", True))
        Me.txtcodcajero.ForeColor = System.Drawing.Color.Blue
        Me.txtcodcajero.Location = New System.Drawing.Point(8, 32)
        Me.txtcodcajero.Multiline = True
        Me.txtcodcajero.Name = "txtcodcajero"
        Me.txtcodcajero.Size = New System.Drawing.Size(101, 16)
        Me.txtcodcajero.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label8.Location = New System.Drawing.Point(8, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(240, 16)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Nombre"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit2.Location = New System.Drawing.Point(496, 56)
        Me.TimeEdit2.Name = "TimeEdit2"
        '
        '
        '
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit2.Properties.UseCtrlIncrement = False
        Me.TimeEdit2.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit2.TabIndex = 12
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit1.Location = New System.Drawing.Point(496, 32)
        Me.TimeEdit1.Name = "TimeEdit1"
        '
        '
        '
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit1.Properties.UseCtrlIncrement = False
        Me.TimeEdit1.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit1.TabIndex = 8
        '
        'DTPFechafinal
        '
        Me.DTPFechafinal.Enabled = False
        Me.DTPFechafinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechafinal.Location = New System.Drawing.Point(360, 56)
        Me.DTPFechafinal.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.Name = "DTPFechafinal"
        Me.DTPFechafinal.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechafinal.TabIndex = 11
        Me.DTPFechafinal.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'DTPFechainicial
        '
        Me.DTPFechainicial.Enabled = False
        Me.DTPFechainicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechainicial.Location = New System.Drawing.Point(360, 32)
        Me.DTPFechainicial.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.Name = "DTPFechainicial"
        Me.DTPFechainicial.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechainicial.TabIndex = 7
        Me.DTPFechainicial.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(304, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Final"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(304, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Inicial"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(496, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Hora"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(368, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Fecha"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btndone
        '
        Me.btndone.Enabled = False
        Me.btndone.Location = New System.Drawing.Point(624, 24)
        Me.btndone.Name = "btndone"
        Me.btndone.Size = New System.Drawing.Size(32, 56)
        Me.btndone.TabIndex = 13
        Me.btndone.Text = "OK"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtfechaapertura)
        Me.GroupBox2.Controls.Add(Me.txtcodaperturacajero)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(8, 144)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(256, 64)
        Me.GroupBox2.TabIndex = 63
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Apertura"
        '
        'txtfechaapertura
        '
        Me.txtfechaapertura.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtfechaapertura.ForeColor = System.Drawing.SystemColors.Info
        Me.txtfechaapertura.Location = New System.Drawing.Point(80, 40)
        Me.txtfechaapertura.Name = "txtfechaapertura"
        Me.txtfechaapertura.Size = New System.Drawing.Size(168, 16)
        Me.txtfechaapertura.TabIndex = 3
        Me.txtfechaapertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodaperturacajero
        '
        Me.txtcodaperturacajero.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtcodaperturacajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Apertura", True))
        Me.txtcodaperturacajero.ForeColor = System.Drawing.SystemColors.Info
        Me.txtcodaperturacajero.Location = New System.Drawing.Point(8, 40)
        Me.txtcodaperturacajero.Name = "txtcodaperturacajero"
        Me.txtcodaperturacajero.Size = New System.Drawing.Size(64, 16)
        Me.txtcodaperturacajero.TabIndex = 2
        Me.txtcodaperturacajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(80, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Fecha / Hora"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "N�mero"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgResumen)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox4.Location = New System.Drawing.Point(280, 465)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(488, 112)
        Me.GroupBox4.TabIndex = 64
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Detalle Operaciones"
        '
        'dgResumen
        '
        Me.dgResumen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.dgResumen.EmbeddedNavigator.Name = ""
        Me.dgResumen.Location = New System.Drawing.Point(8, 16)
        Me.dgResumen.MainView = Me.GridView2
        Me.dgResumen.Name = "dgResumen"
        Me.dgResumen.Size = New System.Drawing.Size(472, 88)
        Me.dgResumen.TabIndex = 1
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn5, Me.GridColumn4, Me.GridColumn3, Me.GridColumn2, Me.GridColumn1})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowFilterPanel = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Factura"
        Me.GridColumn6.FieldName = "Factura"
        Me.GridColumn6.FilterInfo = ColumnFilterInfo11
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 55
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tipo"
        Me.GridColumn5.FieldName = "Tipo"
        Me.GridColumn5.FilterInfo = ColumnFilterInfo12
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 46
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Moneda"
        Me.GridColumn4.FieldName = "Moneda"
        Me.GridColumn4.FilterInfo = ColumnFilterInfo13
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 69
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Pago"
        Me.GridColumn3.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "Pago"
        Me.GridColumn3.FilterInfo = ColumnFilterInfo14
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.VisibleIndex = 4
        Me.GridColumn3.Width = 69
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Forma Pago"
        Me.GridColumn2.FieldName = "Forma Pago"
        Me.GridColumn2.FilterInfo = ColumnFilterInfo15
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 3
        Me.GridColumn2.Width = 76
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Equivalencia"
        Me.GridColumn1.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "Equivalencia"
        Me.GridColumn1.FilterInfo = ColumnFilterInfo16
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.VisibleIndex = 5
        Me.GridColumn1.Width = 79
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = resources.GetString("SqlInsertCommand5.CommandText")
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'AdapterTotalTope
        '
        Me.AdapterTotalTope.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterTotalTope.InsertCommand = Me.SqlInsertCommand6
        Me.AdapterTotalTope.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterTotalTope.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Apertura_Total_Tope", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id_total_tope", "id_total_tope"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Monto_Tope", "Monto_Tope"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre")})})
        Me.AdapterTotalTope.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = resources.GetString("SqlDeleteCommand4.CommandText")
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = resources.GetString("SqlInsertCommand6.CommandText")
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre")})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertur" & _
    "a_Total_Tope"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id_total_tope", System.Data.SqlDbType.Int, 4, "id_total_tope")})
        '
        'AdapterArqueo
        '
        Me.AdapterArqueo.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterArqueo.InsertCommand = Me.SqlInsertCommand7
        Me.AdapterArqueo.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterArqueo.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoCajas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("EfectivoColones", "EfectivoColones"), New System.Data.Common.DataColumnMapping("EfectivoDolares", "EfectivoDolares"), New System.Data.Common.DataColumnMapping("TarjetaColones", "TarjetaColones"), New System.Data.Common.DataColumnMapping("TarjetaDolares", "TarjetaDolares"), New System.Data.Common.DataColumnMapping("TravelCheck", "TravelCheck"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("IdApertura", "IdApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Cajero", "Cajero"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("TipoCambioD", "TipoCambioD"), New System.Data.Common.DataColumnMapping("TipoCambioE", "TipoCambioE")})})
        Me.AdapterArqueo.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = resources.GetString("SqlDeleteCommand5.CommandText")
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TravelCheck", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheck", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdApertura", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cajero", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambioD", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambioD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambioE", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambioE", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = resources.GetString("SqlInsertCommand7.CommandText")
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares"), New System.Data.SqlClient.SqlParameter("@TravelCheck", System.Data.SqlDbType.Float, 0, "TravelCheck"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@IdApertura", System.Data.SqlDbType.Int, 0, "IdApertura"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cajero", System.Data.SqlDbType.VarChar, 0, "Cajero"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@TipoCambioD", System.Data.SqlDbType.Float, 0, "TipoCambioD"), New System.Data.SqlClient.SqlParameter("@TipoCambioE", System.Data.SqlDbType.Float, 0, "TipoCambioE")})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = resources.GetString("SqlSelectCommand7.CommandText")
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = resources.GetString("SqlUpdateCommand5.CommandText")
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares"), New System.Data.SqlClient.SqlParameter("@TravelCheck", System.Data.SqlDbType.Float, 0, "TravelCheck"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@IdApertura", System.Data.SqlDbType.Int, 0, "IdApertura"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cajero", System.Data.SqlDbType.VarChar, 0, "Cajero"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@TipoCambioD", System.Data.SqlDbType.Float, 0, "TipoCambioD"), New System.Data.SqlClient.SqlParameter("@TipoCambioE", System.Data.SqlDbType.Float, 0, "TipoCambioE"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TravelCheck", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TravelCheck", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdApertura", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cajero", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambioD", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambioD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambioE", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambioE", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'AdapterTarjeta
        '
        Me.AdapterTarjeta.DeleteCommand = Me.SqlDeleteCommand6
        Me.AdapterTarjeta.InsertCommand = Me.SqlInsertCommand8
        Me.AdapterTarjeta.SelectCommand = Me.SqlSelectCommand8
        Me.AdapterTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_Arqueo", "Id_Arqueo"), New System.Data.Common.DataColumnMapping("Id_Tarjeta", "Id_Tarjeta"), New System.Data.Common.DataColumnMapping("Monto", "Monto")})})
        Me.AdapterTarjeta.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM ArqueoTarjeta WHERE (Id = @Original_Id) AND (Id_Arqueo = @Original_Id" & _
    "_Arqueo) AND (Id_Tarjeta = @Original_Id_Tarjeta) AND (Monto = @Original_Monto)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO ArqueoTarjeta(Id_Arqueo, Id_Tarjeta, Monto) VALUES (@Id_Arqueo, @Id_T" & _
    "arjeta, @Monto); SELECT Id, Id_Arqueo, Id_Tarjeta, Monto FROM ArqueoTarjeta WHER" & _
    "E (Id = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection1
        Me.SqlInsertCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Tarjeta", System.Data.SqlDbType.Int, 4, "Id_Tarjeta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto")})
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT Id, Id_Arqueo, Id_Tarjeta, Monto FROM ArqueoTarjeta"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = resources.GetString("SqlUpdateCommand6.CommandText")
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Tarjeta", System.Data.SqlDbType.Int, 4, "Id_Tarjeta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(24, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(200, 16)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "Fondo Caja"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Textentradas)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.TxtSalidas)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.TextEdit20)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.TextEdit16)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.TextEdit6)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.TextEdit15)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.TextEdit4)
        Me.GroupBox3.Controls.Add(Me.TextEdit14)
        Me.GroupBox3.Controls.Add(Me.TextEdit3)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.TextEdit17)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox3.Location = New System.Drawing.Point(280, 40)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(488, 251)
        Me.GroupBox3.TabIndex = 66
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cierre Caja"
        '
        'Textentradas
        '
        Me.Textentradas.EditValue = "0.00"
        Me.Textentradas.Location = New System.Drawing.Point(264, 111)
        Me.Textentradas.Name = "Textentradas"
        '
        '
        '
        Me.Textentradas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.Textentradas.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.Textentradas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Textentradas.Properties.EditFormat.FormatString = "#,#0.00"
        Me.Textentradas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Textentradas.Properties.ReadOnly = True
        Me.Textentradas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.Textentradas.Size = New System.Drawing.Size(200, 19)
        Me.Textentradas.TabIndex = 89
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label24.Location = New System.Drawing.Point(272, 95)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(192, 16)
        Me.Label24.TabIndex = 88
        Me.Label24.Text = "MC Entradas"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtSalidas
        '
        Me.TxtSalidas.EditValue = "0.00"
        Me.TxtSalidas.Location = New System.Drawing.Point(24, 110)
        Me.TxtSalidas.Name = "TxtSalidas"
        '
        '
        '
        Me.TxtSalidas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TxtSalidas.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtSalidas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtSalidas.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtSalidas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtSalidas.Properties.ReadOnly = True
        Me.TxtSalidas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TxtSalidas.Size = New System.Drawing.Size(200, 19)
        Me.TxtSalidas.TabIndex = 87
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label26.Location = New System.Drawing.Point(24, 94)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(200, 16)
        Me.Label26.TabIndex = 86
        Me.Label26.Text = "MC  Salidas"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit20
        '
        Me.TextEdit20.EditValue = "0.00"
        Me.TextEdit20.Location = New System.Drawing.Point(264, 184)
        Me.TextEdit20.Name = "TextEdit20"
        '
        '
        '
        Me.TextEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit20.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit20.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit20.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit20.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit20.Properties.ReadOnly = True
        Me.TextEdit20.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit20.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit20.TabIndex = 85
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label23.Location = New System.Drawing.Point(264, 168)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(208, 16)
        Me.Label23.TabIndex = 84
        Me.Label23.Text = "Total Cr�dito"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit16
        '
        Me.TextEdit16.EditValue = "0.00"
        Me.TextEdit16.Location = New System.Drawing.Point(264, 144)
        Me.TextEdit16.Name = "TextEdit16"
        '
        '
        '
        Me.TextEdit16.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit16.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit16.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit16.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit16.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit16.Properties.ReadOnly = True
        Me.TextEdit16.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit16.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit16.TabIndex = 83
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label20.Location = New System.Drawing.Point(264, 128)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(200, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Total Cajero"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit6
        '
        Me.TextEdit6.EditValue = "0.00"
        Me.TextEdit6.Location = New System.Drawing.Point(24, 144)
        Me.TextEdit6.Name = "TextEdit6"
        '
        '
        '
        Me.TextEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit6.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit6.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit6.Properties.ReadOnly = True
        Me.TextEdit6.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit6.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit6.TabIndex = 81
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label13.Location = New System.Drawing.Point(24, 128)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(200, 16)
        Me.Label13.TabIndex = 80
        Me.Label13.Text = "Total Sistema"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit15
        '
        Me.TextEdit15.EditValue = "0.00"
        Me.TextEdit15.Location = New System.Drawing.Point(264, 72)
        Me.TextEdit15.Name = "TextEdit15"
        '
        '
        '
        Me.TextEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit15.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit15.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit15.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit15.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit15.Properties.ReadOnly = True
        Me.TextEdit15.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit15.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit15.TabIndex = 79
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label12.Location = New System.Drawing.Point(272, 56)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(192, 16)
        Me.Label12.TabIndex = 78
        Me.Label12.Text = "Pre Pagos"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "0.00"
        Me.TextEdit4.Location = New System.Drawing.Point(24, 72)
        Me.TextEdit4.Name = "TextEdit4"
        '
        '
        '
        Me.TextEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit4.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit4.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit4.Properties.ReadOnly = True
        Me.TextEdit4.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit4.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit4.TabIndex = 77
        '
        'TextEdit14
        '
        Me.TextEdit14.EditValue = "0.00"
        Me.TextEdit14.Location = New System.Drawing.Point(264, 32)
        Me.TextEdit14.Name = "TextEdit14"
        '
        '
        '
        Me.TextEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit14.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit14.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit14.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit14.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit14.Properties.ReadOnly = True
        Me.TextEdit14.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit14.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit14.TabIndex = 76
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "0.00"
        Me.TextEdit3.Location = New System.Drawing.Point(24, 32)
        Me.TextEdit3.Name = "TextEdit3"
        '
        '
        '
        Me.TextEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit3.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit3.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit3.Properties.ReadOnly = True
        Me.TextEdit3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit3.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit3.TabIndex = 72
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label11.Location = New System.Drawing.Point(24, 56)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(200, 16)
        Me.Label11.TabIndex = 67
        Me.Label11.Text = "Cobros"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label10.Location = New System.Drawing.Point(272, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(192, 16)
        Me.Label10.TabIndex = 66
        Me.Label10.Text = "Ventas Contado"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit17
        '
        Me.TextEdit17.EditValue = "0.00"
        Me.TextEdit17.Location = New System.Drawing.Point(24, 184)
        Me.TextEdit17.Name = "TextEdit17"
        '
        '
        '
        Me.TextEdit17.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit17.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit17.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit17.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit17.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit17.Properties.ReadOnly = True
        Me.TextEdit17.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit17.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit17.TabIndex = 83
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label21.Location = New System.Drawing.Point(24, 168)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(208, 16)
        Me.Label21.TabIndex = 82
        Me.Label21.Text = "Diferencial Caja"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = "0.00"
        Me.TextEdit2.Location = New System.Drawing.Point(8, 112)
        Me.TextEdit2.Name = "TextEdit2"
        '
        '
        '
        Me.TextEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit2.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit2.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit2.TabIndex = 71
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "0.00"
        Me.TextEdit1.Location = New System.Drawing.Point(8, 64)
        Me.TextEdit1.Name = "TextEdit1"
        '
        '
        '
        Me.TextEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit1.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit1.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit1.TabIndex = 70
        '
        'AdapterTipoTarjeta
        '
        Me.AdapterTipoTarjeta.DeleteCommand = Me.SqlDeleteCommand7
        Me.AdapterTipoTarjeta.InsertCommand = Me.SqlInsertCommand9
        Me.AdapterTipoTarjeta.SelectCommand = Me.SqlSelectCommand9
        Me.AdapterTipoTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TipoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("CuentaContable", "CuentaContable"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta")})})
        Me.AdapterTipoTarjeta.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = resources.GetString("SqlDeleteCommand7.CommandText")
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = resources.GetString("SqlInsertCommand9.CommandText")
        Me.SqlInsertCommand9.Connection = Me.SqlConnection1
        Me.SqlInsertCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 75, "CuentaContable"), New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta")})
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT Id, Nombre, Moneda, Observaciones, CuentaContable, NombreCuenta FROM TipoT" & _
    "arjeta"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = resources.GetString("SqlUpdateCommand7.CommandText")
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 75, "CuentaContable"), New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.GridControl2)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox5.Location = New System.Drawing.Point(280, 297)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(488, 160)
        Me.GroupBox5.TabIndex = 67
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Detalles Trajetas de Cr�dito"
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.DataMember = "TipoTarjeta"
        Me.GridControl2.DataSource = Me.DataSetCierreCaja1
        '
        '
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(8, 16)
        Me.GridControl2.MainView = Me.GridView1
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(472, 136)
        Me.GridControl2.TabIndex = 12
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView1.GroupPanelText = "Agrupe de acuerdo a una columna si lo desea"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Tarjeta"
        Me.GridColumn7.FieldName = "Nombre"
        Me.GridColumn7.FilterInfo = ColumnFilterInfo17
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 125
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Total Cajero"
        Me.GridColumn8.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Total"
        Me.GridColumn8.FilterInfo = ColumnFilterInfo18
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.VisibleIndex = 2
        Me.GridColumn8.Width = 118
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Moneda"
        Me.GridColumn9.FieldName = "Monedas"
        Me.GridColumn9.FilterInfo = ColumnFilterInfo19
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 102
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Total Sistema"
        Me.GridColumn10.FieldName = "TotalS"
        Me.GridColumn10.FilterInfo = ColumnFilterInfo20
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn10.VisibleIndex = 3
        Me.GridColumn10.Width = 113
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextEdit18)
        Me.GroupBox6.Controls.Add(Me.TextEdit19)
        Me.GroupBox6.Controls.Add(Me.Label22)
        Me.GroupBox6.Controls.Add(Me.TextEdit12)
        Me.GroupBox6.Controls.Add(Me.TextEdit13)
        Me.GroupBox6.Controls.Add(Me.TextEdit10)
        Me.GroupBox6.Controls.Add(Me.TextEdit11)
        Me.GroupBox6.Controls.Add(Me.TextEdit8)
        Me.GroupBox6.Controls.Add(Me.TextEdit9)
        Me.GroupBox6.Controls.Add(Me.TextEdit7)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.Label18)
        Me.GroupBox6.Controls.Add(Me.Label17)
        Me.GroupBox6.Controls.Add(Me.TextEdit5)
        Me.GroupBox6.Controls.Add(Me.Label16)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Controls.Add(Me.Label14)
        Me.GroupBox6.Controls.Add(Me.TextEdit1)
        Me.GroupBox6.Controls.Add(Me.TextEdit2)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox6.Location = New System.Drawing.Point(8, 235)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(256, 342)
        Me.GroupBox6.TabIndex = 68
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "General"
        '
        'TextEdit18
        '
        Me.TextEdit18.EditValue = "0.00"
        Me.TextEdit18.Location = New System.Drawing.Point(128, 314)
        Me.TextEdit18.Name = "TextEdit18"
        '
        '
        '
        Me.TextEdit18.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit18.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit18.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit18.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit18.Properties.ReadOnly = True
        Me.TextEdit18.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit18.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit18.TabIndex = 92
        '
        'TextEdit19
        '
        Me.TextEdit19.EditValue = "0.00"
        Me.TextEdit19.Location = New System.Drawing.Point(8, 314)
        Me.TextEdit19.Name = "TextEdit19"
        '
        '
        '
        Me.TextEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit19.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit19.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit19.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit19.Properties.ReadOnly = True
        Me.TextEdit19.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit19.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit19.TabIndex = 91
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(4, 290)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(248, 16)
        Me.Label22.TabIndex = 90
        Me.Label22.Text = "      Total Cajero      /      Total Sistema"
        '
        'TextEdit12
        '
        Me.TextEdit12.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TravelCheckSistema", True))
        Me.TextEdit12.EditValue = "0.00"
        Me.TextEdit12.Location = New System.Drawing.Point(128, 255)
        Me.TextEdit12.Name = "TextEdit12"
        '
        '
        '
        Me.TextEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit12.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit12.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit12.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit12.Properties.ReadOnly = True
        Me.TextEdit12.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit12.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit12.TabIndex = 89
        '
        'TextEdit13
        '
        Me.TextEdit13.EditValue = "0.00"
        Me.TextEdit13.Location = New System.Drawing.Point(8, 255)
        Me.TextEdit13.Name = "TextEdit13"
        '
        '
        '
        Me.TextEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit13.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit13.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit13.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit13.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit13.Properties.ReadOnly = True
        Me.TextEdit13.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit13.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit13.TabIndex = 88
        '
        'TextEdit10
        '
        Me.TextEdit10.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TarjetaDolares", True))
        Me.TextEdit10.EditValue = "0.00"
        Me.TextEdit10.Location = New System.Drawing.Point(128, 208)
        Me.TextEdit10.Name = "TextEdit10"
        '
        '
        '
        Me.TextEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit10.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit10.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit10.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit10.Properties.ReadOnly = True
        Me.TextEdit10.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit10.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit10.TabIndex = 87
        '
        'TextEdit11
        '
        Me.TextEdit11.EditValue = "0.00"
        Me.TextEdit11.Location = New System.Drawing.Point(8, 208)
        Me.TextEdit11.Name = "TextEdit11"
        '
        '
        '
        Me.TextEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit11.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit11.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit11.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit11.Properties.ReadOnly = True
        Me.TextEdit11.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit11.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit11.TabIndex = 86
        '
        'TextEdit8
        '
        Me.TextEdit8.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TarjetaColones", True))
        Me.TextEdit8.EditValue = "0.00"
        Me.TextEdit8.Location = New System.Drawing.Point(128, 160)
        Me.TextEdit8.Name = "TextEdit8"
        '
        '
        '
        Me.TextEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit8.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit8.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit8.Properties.ReadOnly = True
        Me.TextEdit8.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit8.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit8.TabIndex = 85
        '
        'TextEdit9
        '
        Me.TextEdit9.EditValue = "0.00"
        Me.TextEdit9.Location = New System.Drawing.Point(8, 160)
        Me.TextEdit9.Name = "TextEdit9"
        '
        '
        '
        Me.TextEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit9.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit9.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit9.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit9.Properties.ReadOnly = True
        Me.TextEdit9.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit9.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit9.TabIndex = 84
        '
        'TextEdit7
        '
        Me.TextEdit7.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.EfectivoDolares", True))
        Me.TextEdit7.EditValue = "0.00"
        Me.TextEdit7.Location = New System.Drawing.Point(128, 112)
        Me.TextEdit7.Name = "TextEdit7"
        '
        '
        '
        Me.TextEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit7.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.ReadOnly = True
        Me.TextEdit7.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit7.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit7.TabIndex = 83
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label19.Location = New System.Drawing.Point(8, 239)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(240, 16)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "Travel Check"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label18.Location = New System.Drawing.Point(8, 192)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(240, 16)
        Me.Label18.TabIndex = 81
        Me.Label18.Text = "Tarjeta Dolares"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label17.Location = New System.Drawing.Point(8, 144)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(240, 16)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Tarjeta Colones"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit5
        '
        Me.TextEdit5.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.EfectivoColones", True))
        Me.TextEdit5.EditValue = "0.00"
        Me.TextEdit5.Location = New System.Drawing.Point(128, 64)
        Me.TextEdit5.Name = "TextEdit5"
        '
        '
        '
        Me.TextEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit5.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit5.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit5.Properties.ReadOnly = True
        Me.TextEdit5.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit5.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit5.TabIndex = 79
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label16.Location = New System.Drawing.Point(8, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(240, 16)
        Me.Label16.TabIndex = 78
        Me.Label16.Text = "Efectivo Dolares"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(248, 24)
        Me.Label15.TabIndex = 77
        Me.Label15.Text = "           Cajero          /          Sistema"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label14.Location = New System.Drawing.Point(8, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(240, 16)
        Me.Label14.TabIndex = 76
        Me.Label14.Text = "Efectivo Colones"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AdapterDetallePago
        '
        Me.AdapterDetallePago.DeleteCommand = Me.SqlDeleteCommand8
        Me.AdapterDetallePago.InsertCommand = Me.SqlInsertCommand10
        Me.AdapterDetallePago.SelectCommand = Me.SqlSelectCommand10
        Me.AdapterDetallePago.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Detalle_pago_caja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroFactura", "NumeroFactura"), New System.Data.Common.DataColumnMapping("TipoFactura", "TipoFactura"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Referencia", "Referencia"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("ReferenciaTipo", "ReferenciaTipo"), New System.Data.Common.DataColumnMapping("ReferenciaDoc", "ReferenciaDoc"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_ODP", "Id_ODP")})})
        Me.AdapterDetallePago.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = resources.GetString("SqlDeleteCommand8.CommandText")
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_ODP", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_ODP", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = resources.GetString("SqlInsertCommand10.CommandText")
        Me.SqlInsertCommand10.Connection = Me.SqlConnection1
        Me.SqlInsertCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.Int, 4, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id_ODP", System.Data.SqlDbType.BigInt, 8, "Id_ODP")})
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTi" & _
    "po, ReferenciaDoc, Moneda, TipoCambio, Id, Id_ODP FROM Detalle_pago_caja"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = resources.GetString("SqlUpdateCommand8.CommandText")
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NumeroFactura", System.Data.SqlDbType.Float, 8, "NumeroFactura"), New System.Data.SqlClient.SqlParameter("@TipoFactura", System.Data.SqlDbType.VarChar, 3, "TipoFactura"), New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"), New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 50, "Referencia"), New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"), New System.Data.SqlClient.SqlParameter("@ReferenciaTipo", System.Data.SqlDbType.Int, 4, "ReferenciaTipo"), New System.Data.SqlClient.SqlParameter("@ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, "ReferenciaDoc"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"), New System.Data.SqlClient.SqlParameter("@Id_ODP", System.Data.SqlDbType.BigInt, 8, "Id_ODP"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_ODP", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_ODP", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumeroFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaDoc", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ReferenciaTipo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ReferenciaTipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TipoFactura", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoFactura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'AdapterVentas
        '
        Me.AdapterVentas.InsertCommand = Me.SqlInsertCommand11
        Me.AdapterVentas.SelectCommand = Me.SqlSelectCommand11
        Me.AdapterVentas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VENTASPREPAGOS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Proveniencia_Venta", "Proveniencia_Venta"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura")})})
        '
        'SqlInsertCommand11
        '
        Me.SqlInsertCommand11.CommandText = resources.GetString("SqlInsertCommand11.CommandText")
        Me.SqlInsertCommand11.Connection = Me.SqlConnection1
        Me.SqlInsertCommand11.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 8, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Proveniencia_Venta", System.Data.SqlDbType.Int, 4, "Proveniencia_Venta"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.BigInt, 8, "Num_Apertura")})
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT Num_Factura, Proveniencia_Venta, Total, Monto, Cod_Moneda, Num_Apertura FR" & _
    "OM VENTASPREPAGOS"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection1
        '
        'AdapterVentasContado
        '
        Me.AdapterVentasContado.InsertCommand = Me.SqlInsertCommand12
        Me.AdapterVentasContado.SelectCommand = Me.SqlSelectCommand12
        Me.AdapterVentasContado.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VENTASCONTADO", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Proveniencia_Venta", "Proveniencia_Venta")})})
        '
        'SqlInsertCommand12
        '
        Me.SqlInsertCommand12.CommandText = resources.GetString("SqlInsertCommand12.CommandText")
        Me.SqlInsertCommand12.Connection = Me.SqlConnection1
        Me.SqlInsertCommand12.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 8, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.BigInt, 8, "Num_Apertura"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Proveniencia_Venta", System.Data.SqlDbType.Int, 4, "Proveniencia_Venta")})
        '
        'SqlSelectCommand12
        '
        Me.SqlSelectCommand12.CommandText = "SELECT Num_Factura, Total, Num_Apertura, Anulado, Tipo, Cod_Moneda, Proveniencia_" & _
    "Venta FROM VENTASCONTADO"
        Me.SqlSelectCommand12.Connection = Me.SqlConnection1
        '
        'AdapterCobros
        '
        Me.AdapterCobros.InsertCommand = Me.SqlInsertCommand13
        Me.AdapterCobros.SelectCommand = Me.SqlSelectCommand13
        Me.AdapterCobros.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "COBROS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo")})})
        '
        'SqlInsertCommand13
        '
        Me.SqlInsertCommand13.CommandText = resources.GetString("SqlInsertCommand13.CommandText")
        Me.SqlInsertCommand13.Connection = Me.SqlConnection1
        Me.SqlInsertCommand13.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.BigInt, 8, "NApertura"), New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 8, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo")})
        '
        'SqlSelectCommand13
        '
        Me.SqlSelectCommand13.CommandText = "SELECT NApertura, Num_Factura, Anulado, Cod_Moneda, Total, Id, Tipo FROM COBROS"
        Me.SqlSelectCommand13.Connection = Me.SqlConnection1
        '
        'AdapterCierreMonto
        '
        Me.AdapterCierreMonto.DeleteCommand = Me.SqlDeleteCommand9
        Me.AdapterCierreMonto.InsertCommand = Me.SqlInsertCommand14
        Me.AdapterCierreMonto.SelectCommand = Me.SqlSelectCommand14
        Me.AdapterCierreMonto.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CierreCaja_DetMon", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_DetaMoneda", "Id_DetaMoneda"), New System.Data.Common.DataColumnMapping("Id_CierreCaja", "Id_CierreCaja"), New System.Data.Common.DataColumnMapping("Id_Moneda", "Id_Moneda"), New System.Data.Common.DataColumnMapping("MontoSistema", "MontoSistema"), New System.Data.Common.DataColumnMapping("MontoCajero", "MontoCajero")})})
        Me.AdapterCierreMonto.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = resources.GetString("SqlDeleteCommand9.CommandText")
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id_DetaMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetaMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand14
        '
        Me.SqlInsertCommand14.CommandText = resources.GetString("SqlInsertCommand14.CommandText")
        Me.SqlInsertCommand14.Connection = Me.SqlConnection1
        Me.SqlInsertCommand14.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"), New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.Int, 4, "Id_Moneda"), New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"), New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero")})
        '
        'SqlSelectCommand14
        '
        Me.SqlSelectCommand14.CommandText = "SELECT Id_DetaMoneda, Id_CierreCaja, Id_Moneda, MontoSistema, MontoCajero FROM Ci" & _
    "erreCaja_DetMon"
        Me.SqlSelectCommand14.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = resources.GetString("SqlUpdateCommand9.CommandText")
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"), New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.Int, 4, "Id_Moneda"), New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"), New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"), New System.Data.SqlClient.SqlParameter("@Original_Id_DetaMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetaMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id_DetaMoneda", System.Data.SqlDbType.Int, 4, "Id_DetaMoneda")})
        '
        'AdapterCierreTarjeta
        '
        Me.AdapterCierreTarjeta.DeleteCommand = Me.SqlDeleteCommand10
        Me.AdapterCierreTarjeta.InsertCommand = Me.SqlInsertCommand15
        Me.AdapterCierreTarjeta.SelectCommand = Me.SqlSelectCommand15
        Me.AdapterCierreTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CierreCaja_DetTarj", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_DetalleTarjeta", "Id_DetalleTarjeta"), New System.Data.Common.DataColumnMapping("Id_CierreCaja", "Id_CierreCaja"), New System.Data.Common.DataColumnMapping("Id_TipoTarjeta", "Id_TipoTarjeta"), New System.Data.Common.DataColumnMapping("MontoSistema", "MontoSistema"), New System.Data.Common.DataColumnMapping("MontoCajero", "MontoCajero")})})
        Me.AdapterCierreTarjeta.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = resources.GetString("SqlDeleteCommand10.CommandText")
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetalleTarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TipoTarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand15
        '
        Me.SqlInsertCommand15.CommandText = resources.GetString("SqlInsertCommand15.CommandText")
        Me.SqlInsertCommand15.Connection = Me.SqlConnection1
        Me.SqlInsertCommand15.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"), New System.Data.SqlClient.SqlParameter("@Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, "Id_TipoTarjeta"), New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"), New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero")})
        '
        'SqlSelectCommand15
        '
        Me.SqlSelectCommand15.CommandText = "SELECT Id_DetalleTarjeta, Id_CierreCaja, Id_TipoTarjeta, MontoSistema, MontoCajer" & _
    "o FROM CierreCaja_DetTarj"
        Me.SqlSelectCommand15.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = resources.GetString("SqlUpdateCommand10.CommandText")
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"), New System.Data.SqlClient.SqlParameter("@Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, "Id_TipoTarjeta"), New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"), New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"), New System.Data.SqlClient.SqlParameter("@Original_Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetalleTarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TipoTarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, "Id_DetalleTarjeta")})
        '
        'lblanulado
        '
        Me.lblanulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 34.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblanulado.ForeColor = System.Drawing.Color.Red
        Me.lblanulado.Location = New System.Drawing.Point(229, 343)
        Me.lblanulado.Name = "lblanulado"
        Me.lblanulado.Size = New System.Drawing.Size(320, 96)
        Me.lblanulado.TabIndex = 69
        Me.lblanulado.Text = "Anulado"
        Me.lblanulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblanulado.Visible = False
        '
        'AdapterVentasCredito
        '
        Me.AdapterVentasCredito.InsertCommand = Me.SqlInsertCommand16
        Me.AdapterVentasCredito.SelectCommand = Me.SqlSelectCommand16
        Me.AdapterVentasCredito.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VENTASCREDITO", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Expr1", "Expr1"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Proveniencia_Venta", "Proveniencia_Venta"), New System.Data.Common.DataColumnMapping("Id_Reservacion", "Id_Reservacion"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda")})})
        '
        'SqlInsertCommand16
        '
        Me.SqlInsertCommand16.CommandText = resources.GetString("SqlInsertCommand16.CommandText")
        Me.SqlInsertCommand16.Connection = Me.SqlConnection1
        Me.SqlInsertCommand16.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Expr1", System.Data.SqlDbType.VarChar, 3, "Expr1"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 8, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Proveniencia_Venta", System.Data.SqlDbType.Int, 4, "Proveniencia_Venta"), New System.Data.SqlClient.SqlParameter("@Id_Reservacion", System.Data.SqlDbType.Float, 8, "Id_Reservacion"), New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.BigInt, 8, "Num_Apertura"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda")})
        '
        'SqlSelectCommand16
        '
        Me.SqlSelectCommand16.CommandText = "SELECT Expr1, SubTotal, Total, Num_Factura, Proveniencia_Venta, Id_Reservacion, N" & _
    "um_Apertura, Id, Cod_Moneda FROM VENTASCREDITO"
        Me.SqlSelectCommand16.Connection = Me.SqlConnection1
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(464, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(85, 20)
        Me.DateTimePicker1.TabIndex = 71
        '
        'ButtonAperturas
        '
        Me.ButtonAperturas.Enabled = False
        Me.ButtonAperturas.Location = New System.Drawing.Point(555, 3)
        Me.ButtonAperturas.Name = "ButtonAperturas"
        Me.ButtonAperturas.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAperturas.TabIndex = 72
        Me.ButtonAperturas.Text = "Aperturas"
        Me.ButtonAperturas.UseVisualStyleBackColor = True
        '
        'check_PVE
        '
        Me.check_PVE.AutoEllipsis = True
        Me.check_PVE.BackColor = System.Drawing.Color.Transparent
        Me.check_PVE.Location = New System.Drawing.Point(721, 0)
        Me.check_PVE.Name = "check_PVE"
        Me.check_PVE.Size = New System.Drawing.Size(58, 25)
        Me.check_PVE.TabIndex = 73
        Me.check_PVE.Text = "PVE"
        Me.check_PVE.UseVisualStyleBackColor = False
        '
        'btVentas
        '
        Me.btVentas.Enabled = False
        Me.btVentas.Location = New System.Drawing.Point(632, 3)
        Me.btVentas.Name = "btVentas"
        Me.btVentas.Size = New System.Drawing.Size(75, 23)
        Me.btVentas.TabIndex = 74
        Me.btVentas.Text = "Ventas"
        Me.btVentas.UseVisualStyleBackColor = True
        '
        'txTotalComision
        '
        Me.txTotalComision.EditValue = "0.00"
        Me.txTotalComision.Location = New System.Drawing.Point(544, 267)
        Me.txTotalComision.Name = "txTotalComision"
        '
        '
        '
        Me.txTotalComision.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txTotalComision.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txTotalComision.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalComision.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txTotalComision.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txTotalComision.Properties.ReadOnly = True
        Me.txTotalComision.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txTotalComision.Size = New System.Drawing.Size(200, 19)
        Me.txTotalComision.TabIndex = 91
        '
        'lbTotalComision
        '
        Me.lbTotalComision.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTotalComision.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbTotalComision.Location = New System.Drawing.Point(544, 251)
        Me.lbTotalComision.Name = "lbTotalComision"
        Me.lbTotalComision.Size = New System.Drawing.Size(208, 16)
        Me.lbTotalComision.TabIndex = 90
        Me.lbTotalComision.Text = "Total Comisi�n"
        Me.lbTotalComision.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CierreCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(778, 635)
        Me.Controls.Add(Me.txTotalComision)
        Me.Controls.Add(Me.lbTotalComision)
        Me.Controls.Add(Me.btVentas)
        Me.Controls.Add(Me.check_PVE)
        Me.Controls.Add(Me.ButtonAperturas)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.lblanulado)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.TextBox6)
        Me.Name = "CierreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ""
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.TextBox6, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox6, 0)
        Me.Controls.SetChildIndex(Me.lblanulado, 0)
        Me.Controls.SetChildIndex(Me.DateTimePicker1, 0)
        Me.Controls.SetChildIndex(Me.ButtonAperturas, 0)
        Me.Controls.SetChildIndex(Me.check_PVE, 0)
        Me.Controls.SetChildIndex(Me.btVentas, 0)
        Me.Controls.SetChildIndex(Me.lbTotalComision, 0)
        Me.Controls.SetChildIndex(Me.txTotalComision, 0)
        CType(Me.DataSetCierreCaja1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.Textentradas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSalidas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTotalComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Load"
    Private Sub CierreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conectadobd As New SqlClient.SqlConnection
        Dim cConexion As New ConexionR

        Try
            Me.check_PVE.Checked = GetSetting("SeeSoft", "Restaurante", "PVE")
        Catch ex As Exception
            SaveSetting("SeeSoft", "Restaurante", "PVE", 0)
        End Try

        If GetSetting("SeeSOFT", "Hotel", "ComisionPuntoVenta").Equals("1") Then
            txTotalComision.Visible = True
            lbTotalComision.Visible = True
        Else
            txTotalComision.Visible = False
            lbTotalComision.Visible = False
        End If

        Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
        Carga()
        If Not GetSetting("SeeSoft", "Restaurante", "ReporteCierre").Equals("1") Then SaveSetting("SeeSoft", "Restaurante", "ReporteCierre", "0")

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
       
        If gloNoClave Then
            Loggin_Usuario()
        Else
            TextBox6.Focus()
        End If
       
        '---------------------------------------------------------------
    End Sub

    Public Sub Carga()
        Me.AdapterMoneda.Fill(Me.DataSetCierreCaja1.Moneda)
        Me.AdapterUsuarios.Fill(Me.DataSetCierreCaja1.Usuarios)
        Me.AdapterTipoTarjeta.Fill(Me.DataSetCierreCaja1.TipoTarjeta)
        Me.AdapterDetallePago.Fill(Me.DataSetCierreCaja1.Detalle_pago_caja)
        FormatoTablas()
        defaultvalue()
    End Sub

    Private Sub defaultvalue()
        Dim i, j As Integer
        Me.DataSetCierreCaja1.cierrecaja.AperturaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.CajeraColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.NombreColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.AnuladoColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.FechaColumn.DefaultValue = Now
        Me.DataSetCierreCaja1.cierrecaja.DevolucionesColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.SubtotalColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.DevolucionesColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.TotalSistemaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.EquivalenciaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.TravelCheckCajeroColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.TravelCheckSistemaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.CierreCaja_DetMon.Id_MonedaColumn.DefaultValue = 1
        Me.DataSetCierreCaja1.CierreCaja_DetMon.MontoCajeroColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.CierreCaja_DetMon.MontoSistemaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.cierrecaja.TravelCheckCajeroColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.CierreCaja_DetTarj.Id_TipoTarjetaColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.CierreCaja_DetTarj.MontoCajeroColumn.DefaultValue = 0
        Me.DataSetCierreCaja1.CierreCaja_DetTarj.MontoSistemaColumn.DefaultValue = 0


        For i = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Rows.Count - 1
            Me.DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Total") = 0
            Me.DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("TotalS") = 0
            For j = 0 To Me.DataSetCierreCaja1.Moneda.Rows.Count - 1
                If Me.DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Moneda") = Me.DataSetCierreCaja1.Moneda.Rows(j).Item("CodMoneda") Then
                    Me.DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Monedas") = Me.DataSetCierreCaja1.Moneda.Rows(j).Item("MonedaNombre")
                End If
            Next
        Next
    End Sub

    Public Sub FormatoTablas()
        'Declara de columnas
        Dim dtfact, dttipo, dtcoin, dtpago, dtformapago, dtmonto As System.Data.DataColumn
        Dim dtmoneda, dtefectivo, dttarjeta, dtcheque, dttransferencia, dtdevolucion, dttotal As System.Data.DataColumn
        Dim dtMonto_cierre, dtmoneda_cierre, dtequivalencia_cierre As System.Data.DataColumn
        'Creacion de las columnas  
        '*******************************************Tabla
        dtmoneda = New System.Data.DataColumn("Moneda")
        dtefectivo = New System.Data.DataColumn("Efectivo")
        dttarjeta = New System.Data.DataColumn("Tarjeta")
        dtcheque = New System.Data.DataColumn("Cheque")
        dttransferencia = New System.Data.DataColumn("Transferencia")
        dtdevolucion = New System.Data.DataColumn("Devoluciones")
        dttotal = New System.Data.DataColumn("Total")
        '********************************************************Tabla Resumen
        dtfact = New System.Data.DataColumn("Factura")
        dttipo = New System.Data.DataColumn("Tipo")
        dtcoin = New System.Data.DataColumn("Moneda")
        dtpago = New System.Data.DataColumn("Pago")
        dtformapago = New System.Data.DataColumn("Forma Pago")
        dtmonto = New System.Data.DataColumn("Equivalencia")
        '**********************************************************Tabla Cierre
        dtMonto_cierre = New System.Data.DataColumn("Monto")
        dtmoneda_cierre = New System.Data.DataColumn("Moneda")
        dtequivalencia_cierre = New System.Data.DataColumn("Equivalencia")
        'Me.chkAnulado.Checked = False

        'Agregar columnas a la data tabla
        '*********************************************************Tabla
        tabla.Columns.Clear()
        tabla.Columns.Add(dtmoneda)
        tabla.Columns.Add(dtefectivo)
        tabla.Columns.Add(dttarjeta)
        tabla.Columns.Add(dtcheque)
        tabla.Columns.Add(dttransferencia)
        tabla.Columns.Add(dtdevolucion)
        tabla.Columns.Add(dttotal)
        '*********************************************************Tabla Resumen
        tablaresumen.Columns.Clear()
        tablaresumen.Columns.Add(dtfact)
        tablaresumen.Columns.Add(dttipo)
        tablaresumen.Columns.Add(dtcoin)
        tablaresumen.Columns.Add(dtpago)
        tablaresumen.Columns.Add(dtformapago)
        tablaresumen.Columns.Add(dtmonto)
        '*********************************************************Tabla Cierre
        tablacierre.Columns.Clear()
        tablacierre.Columns.Add(dtMonto_cierre)
        tablacierre.Columns.Add(dtmoneda_cierre)
        tablacierre.Columns.Add(dtequivalencia_cierre)
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub TextBox6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox6.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                Dim clave As String
                Dim usuarios() As System.Data.DataRow 'almacena temporalmente los datos de un art�culo
                Dim usuario As System.Data.DataRow 'almacena temporalmente los datos de un art�culo

                clave = Me.TextBox6.Text
                usuarios = Me.DataSetCierreCaja1.Usuarios.Select("clave_interna= '" & CStr(clave) & "'")

                If usuarios.Length <> 0 Then
                    usuario = usuarios(0) 'se almacena la info del usuario
                    If usuario!nombre <> "" Then
                        Me.DataSetCierreCaja1.cierrecaja.UsuarioColumn.DefaultValue = usuario!cedula
                        Me.DataSetCierreCaja1.cierrecaja.NombreUsuarioColumn.DefaultValue = usuario!nombre
                        Me.txtNombreUsuario.Text = usuario!nombre
                        Me.Usuario = usuario!nombre
                        Me.Cedula_Usuario = usuario!Cedula
                        PMU = VSM(Me.Cedula_Usuario, Me.Name) 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                        Me.txtcodcajero.Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ButtonBuscar.Enabled = True
                        Me.ButtonAperturas.Enabled = True
                        IniciarEdicion()
                        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
                        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()

                        Me.txtcodcajero.Focus()
                    Else
                        MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                        Me.TextBox6.Text = ""
                        txtNombreUsuario.Text = Nothing
                        Me.TextBox6.Focus()
                    End If
                Else
                    MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                    Me.TextBox6.Text = ""
                    txtNombreUsuario.Text = Nothing
                    Me.TextBox6.Focus()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Me.DataSetCierreCaja1.cierrecaja.UsuarioColumn.DefaultValue = User_Log.Cedula
                Me.DataSetCierreCaja1.cierrecaja.NombreUsuarioColumn.DefaultValue = User_Log.Nombre
                Me.txtNombreUsuario.Text = User_Log.Nombre
                Me.Usuario = User_Log.Nombre
                Me.Cedula_Usuario = User_Log.Cedula
                PMU = VSM(Me.Cedula_Usuario, Me.Name) 'Carga los privilegios del usuario con el modulo 
                If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Me.txtcodcajero.Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                IniciarEdicion()
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()

                Me.txtcodcajero.Focus()
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Function IniciarEdicion()
        Me.ToolBar1.Buttons(0).Text = "Cancelar"
        Me.ToolBar1.Buttons(0).ImageIndex = 8
        Me.ToolBar1.Buttons(0).Enabled = True
        Me.ToolBar1.Buttons(1).Enabled = False
        Me.ToolBar1.Buttons(2).Enabled = True
        Me.ToolBar1.Buttons(3).Enabled = False
        Me.ToolBar1.Buttons(4).Enabled = False
    End Function
#End Region
    Sub LimpiaDataSet()
        Me.DataSetCierreCaja1.VENTASCONTADO.Clear()
        Me.DataSetCierreCaja1.VENTASCREDITO.Clear()
        Me.DataSetCierreCaja1.VENTASPREPAGOS.Clear()
        Me.DataSetCierreCaja1.COBROS.Clear()
        'Me.DataBindings.Clear()
    End Sub

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1
                nuevos()
            Case 2
                If PMU.Find Then Consultar_Cierre() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3
                If PMU.Update Then Guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4
                If PMU.Delete Then anular() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5
                If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7
                Me.Close()
        End Select
    End Sub
#End Region

#Region "Nuevo"

    Public Shared Sub LimpiaXtraEditors(ByVal obj As Control)
        For Each Control As Object In obj.Controls
            If TypeOf Control Is DevExpress.XtraEditors.TextEdit Then Control.text = "0"
        Next
    End Sub

    Sub nuevos()
        Try
            If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then
                'Restricci�n botones
                Me.lblanulado.Visible = False
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.ToolBar1.Buttons(1).Enabled = False
                Me.ToolBar1.Buttons(2).Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                btVentas.Enabled = False
                Me.txtcodcajero.Enabled = True
                Me.txtcodcajero.Focus()
                Me.ButtonBuscar.Enabled = True
                Me.ButtonAperturas.Enabled = True
                'Limpia los campos
                Me.dgResumen.DataSource = Nothing
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()

                DataSetCierreCaja1.aperturacaja.Clear()
                DataSetCierreCaja1.CierreCaja_DetMon.Clear()
                DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
                DataSetCierreCaja1.cierrecaja.Clear()
                DataSetCierreCaja1.ArqueoCajas.Clear()
                DataSetCierreCaja1.Apertura_Total_Tope.Clear()

                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()

                LimpiaXtraEditors(Me.GroupBox3)

                TextEdit17.EditValue = 0
                TextEdit5.EditValue = 0
                TextEdit18.EditValue = 0
                TextEdit6.EditValue = 0
                TextEdit1.EditValue = 0
                TextEdit2.EditValue = 0
                TextEdit9.EditValue = 0
                TextEdit11.EditValue = 0
                TextEdit16.EditValue = 0
                Me.TextEdit3.EditValue = 0
                TextEdit19.EditValue = 0
                Textentradas.EditValue = 0
                TxtSalidas.EditValue = 0
                TextEdit4.EditValue = 0
                TextEdit20.EditValue = 0
                TextEdit14.EditValue = 0
                Me.TextEdit15.EditValue = 0

                For i As Integer = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Count - 1
                    With Me.DataSetCierreCaja1.TipoTarjeta(i)
                        .TotalS = 0
                        .Total = 0
                        .EndEdit()
                    End With
                Next

            Else
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                Me.ToolBar1.Buttons(5).Enabled = True
                btVentas.Enabled = False
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").CancelCurrentEdit()
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()
                Me.DataSetCierreCaja1.aperturacaja.Clear()
                Me.DataSetCierreCaja1.CierreCaja_DetMon.Clear()
                Me.DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
                Me.DataSetCierreCaja1.cierrecaja.Clear()
                Me.txtcodcajero.Enabled = False
                ButtonBuscar.Enabled = False
                Me.ButtonAperturas.Enabled = False
                txtfechaapertura.Text = ""

                TextEdit17.EditValue = 0
                TextEdit5.EditValue = 0
                TextEdit18.EditValue = 0
                TextEdit6.EditValue = 0
                TextEdit1.EditValue = 0
                TextEdit2.EditValue = 0
                TextEdit9.EditValue = 0
                TextEdit11.EditValue = 0
                TextEdit16.EditValue = 0
                Me.TextEdit3.EditValue = 0
                TextEdit19.EditValue = 0
                Textentradas.EditValue = 0
                TxtSalidas.EditValue = 0
                TextEdit4.EditValue = 0
                TextEdit20.EditValue = 0
                TextEdit14.EditValue = 0
                Me.TextEdit15.EditValue = 0

                For i As Integer = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Count - 1
                    With Me.DataSetCierreCaja1.TipoTarjeta(i)
                        .TotalS = 0
                        .Total = 0
                        .EndEdit()
                    End With
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Anular"
    Sub anular()
        Dim resp As Integer
        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Dim myCommand1 As SqlCommand = SqlConnection1.CreateCommand()
        Dim myCommand2 As SqlCommand = SqlConnection1.CreateCommand()
        resp = MessageBox.Show("�Deseas Anular este Cierre?", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        If resp = 6 Then
            myCommand1.CommandText = "UPDATE CierreCaja SET Anulado =  1 WHERE NumeroCierre = " & Me.BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("NumeroCierre")
            myCommand2.CommandText = "UPDATE aperturacaja SET Estado = '" & "M" & "' WHERE NApertura = " & Me.BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("Apertura")
            myCommand1.Transaction = Trans
            myCommand2.Transaction = Trans
            myCommand1.ExecuteNonQuery()
            myCommand2.ExecuteNonQuery()
            Trans.Commit()
            MsgBox("Datos Anulados Correctamente....", MsgBoxStyle.Information, "Atenci�n...")
            lblanulado.Visible = True
            Me.ToolBarEliminar.Enabled = False
            btVentas.Enabled = False
            ' Me.Inhabilitar()
        Else
        End If
    End Sub
#End Region

#Region "Guardar"
    Sub Guardar()
        CargarDetalleCierreCaja()

        If Registar() Then
            CambiarEstadoApertura()
            Me.DataSetCierreCaja1.AcceptChanges()

            If MsgBox("Desea imprimir el cierre de Caja", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Imprimir()
            End If
            Me.tabla.Clear()
            Me.tablacierre.Clear()
            Me.tablaresumen.Clear()
            Me.DataSetCierreCaja1.aperturacaja.Clear()
            'Me.DataSetCierreCaja1.Detalle_Cierrecaja.Clear()
            Me.DataSetCierreCaja1.CierreCaja_DetMon.Clear()
            Me.DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
            Me.DataSetCierreCaja1.cierrecaja.Clear()
            'Me.DataSetCierreCaja1 .DenominacionesAperturas.Clear()
            Me.txtcodcajero.Enabled = False
            MessageBox.Show("El cierre de caja ha sido registrado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.ToolBar1.Buttons(0).Enabled = True
            Me.ToolBar1.Buttons(1).Enabled = True
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.ToolBar1.Buttons(3).Enabled = False
            Me.ToolBar1.Buttons(4).Enabled = False
            Me.ToolBar1.Buttons(5).Enabled = True
            txtfechaapertura.Text = ""
        Else
            MessageBox.Show("Problemas al tratar de registrar el  Cierre, Intente de nuevo, si el problema persiste pongase en contacto con el adminstrador del sistema", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Function CambiarEstadoApertura()

        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Dim myCommand2 As SqlCommand = SqlConnection1.CreateCommand()

            myCommand2.CommandText = "UPDATE aperturacaja SET Estado = '" & "C" & "' WHERE NApertura = " & Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Apertura")

            myCommand2.Transaction = Trans

            myCommand2.ExecuteNonQuery()
            Trans.Commit()
        Catch eEndEdit As System.Exception
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            Return False
        End Try
    End Function

    Function Registar() As Boolean
        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            'Apertura
            'Me.SqlUpdateCommand7.Transaction = Trans
            'Me.SqlUpdateCommand7.Transaction = Trans
            'Cierre Caja
            Me.SqlInsertCommand2.Transaction = Trans
            Me.SqlUpdateCommand1.Transaction = Trans
            Me.SqlInsertCommand14.Transaction = Trans
            Me.SqlUpdateCommand9.Transaction = Trans
            Me.SqlInsertCommand15.Transaction = Trans
            Me.SqlUpdateCommand10.Transaction = Trans
            'Detalle Cierre Caja
            'Me.SqlInsertCommand9.Transaction = Trans
            'Me.SqlUpdateCommand14.Transaction = Trans

            'Me.daAperturacaja.Update(Me.DataSetCierreCaja1.aperturacaja)
            Me.AdapterCierre.Update(Me.DataSetCierreCaja1.cierrecaja)
            Me.AdapterCierreTarjeta.Update(Me.DataSetCierreCaja1.CierreCaja_DetTarj)
            Me.AdapterCierreMonto.Update(Me.DataSetCierreCaja1.CierreCaja_DetMon)
            Trans.Commit()
            Return True
        Catch eEndEdit As System.Exception
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            Return False
        End Try
    End Function
#End Region

#Region "Cargar Cierre"
    Private Sub txtcodcajero_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtcodcajero.KeyDown
        If e.KeyCode = Keys.F1 Then
            If (txtNombreUsuario.Text = Nothing) Then
                MsgBox("Para realizar un cierre de caja es necesario que ingreses el nombre de usuario", MsgBoxStyle.Critical, "Usuario desconocido")
                Exit Sub
            Else
                LimpiaDataSet()
                Carga()
                consultarcliente()
            End If
        End If
    End Sub

    Private Sub consultarcliente()
        Dim cFunciones As New cFunciones
        Dim Cedula As String = cFunciones.BuscarDatos("select NApertura as Identificaci�n,nombre as Nombre from aperturacaja where Anulado = 0 and estado =  '" & "M" & "'", "nombre", "CAJEROS")
        If Cedula = Nothing Then
            MsgBox("Debes selecionar un usuario para realizar el cierre de Caja", MsgBoxStyle.Exclamation)
        Else
            Me.DataSetCierreCaja1.aperturacaja.Clear()
            Me.DataSetCierreCaja1.Apertura_Total_Tope.Clear()
            'Me.DataSetCierreCaja1.denominacionesapertura.Clear()

            CargarApertura(Cedula)
            CargarAperturaTotal(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            'CargarPrepagos(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            Me.DataSetCierreCaja1.ArqueoCajas.Clear()
            CargarArqueo(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            If Me.DataSetCierreCaja1.ArqueoCajas.Count > 0 Then
            Else
                MsgBox("Es Necesario Ingresar Un Arqueo De Caja")
                Exit Sub
            End If
            CargarVentasContado(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarVentasCredito(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarCobros(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarArqueoTarjeta(Me.DataSetCierreCaja1.ArqueoCajas.Rows(0).Item("Id")) 'Id
            Me.Cargando()
            If Me.DataSetCierreCaja1.aperturacaja.Count > 0 Then
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()
                'Me.DataSetCierreCaja1.DenominacionesAperturas.Clear()
                Me.DataSetCierreCaja1.OpcionesDePago.Clear()
                CargarOpcionesPago(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
                'CargarDenominacionesApertura(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura"))
                txtcodaperturacajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
                txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
                CargarCierre()
                btVentas.Enabled = True
            Else
                MsgBox("Este Usuario no tiene una caja abierta")
            End If
        End If
    End Sub

    Sub CargarApertura(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM aperturacaja where NApertura = '" & Id & "' AND Estado = 'M'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.aperturacaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarAperturaTotal(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM Apertura_Total_Tope where NApertura  = @Numapertura "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.Apertura_Total_Tope)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarPrepagos(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM VENTASPREPAGOS where Num_Apertura  = @Numapertura "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.VENTASPREPAGOS)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarArqueo(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM ArqueoCajas where IdApertura  = @Numapertura  AND Anulado = 0 "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.ArqueoCajas)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarArqueoTarjeta(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM ArqueoTarjeta where Id_Arqueo  = @Numapertura "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            Me.DataSetCierreCaja1.ArqueoTarjeta.Clear()
            da.Fill(Me.DataSetCierreCaja1.ArqueoTarjeta)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarOpcionesPago(ByVal Numapertura As String)
        Dim cnnv As SqlConnection = Nothing
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM OpcionesDePago WHERE Numapertura = @Numapertura AND FormaPago <> 'ANU'"
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmdv.Parameters("@Numapertura").Value = Numapertura
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            Me.DataSetCierreCaja1.OpcionesDePago.Clear()
            dv.Fill(Me.DataSetCierreCaja1.OpcionesDePago)
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Sub

    Sub CargarCierre()
        Dim I, J, K As Integer
        Dim Dr As System.Data.DataRow
        Dim Dr1 As System.Data.DataRow
        Dim Dr2 As System.Data.DataRow
        'Valores Vacios a los grid
        Me.dgResumen.DataSource = Nothing
        'Me.dgOperaciones.DataSource = Nothing
        'Me.dgCierre.DataSource = Nothing
        Me.txtcodcajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula")
        Me.txtnomcajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Nombre")
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Cajera") = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula")
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Nombre") = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Nombre")
        txtcodaperturacajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
        txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Apertura") = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString


        SubTotal = 0
        Devolucion = 0

        TextEdit5.EditValue = 0
        TextEdit8.EditValue = 0
        TextEdit7.EditValue = 0
        TextEdit10.EditValue = 0
        TextEdit12.EditValue = 0
        txTotalComision.EditValue = 0

        'LLenar Los Totales Por monedas

        For I = 0 To (Me.DataSetCierreCaja1.Moneda.Count - 1)
            Dr2 = tablacierre.NewRow
            Dr = tabla.NewRow
            Dr(0) = Me.DataSetCierreCaja1.Moneda.Rows(I).Item("MonedaNombre")
            TotalesPorMonedas(Dr, Dr2, Me.DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda"), Me.DataSetCierreCaja1.Moneda.Rows(I).Item("ValorCompra"), Me.DataSetCierreCaja1.Moneda.Rows(I).Item("MonedaNombre"))
            Select Case Me.DataSetCierreCaja1.Moneda.Rows(I).Item("Simbolo")
                Case "�"
                    TextEdit5.EditValue = Dr(1)
                    TextEdit8.EditValue = Dr(2)

                Case "$"
                    TextEdit7.EditValue = Dr(1)
                    TextEdit10.EditValue = Dr(2)
                    TextEdit12.EditValue = Dr(3)
                Case "�"
                    'Dr(1)
                    'Dr(2)
            End Select
            tabla.Rows.Add(Dr)
            tablacierre.Rows.Add(Dr2)
        Next

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'TextEdit4.EditValue = 0
        'For I = 0 To (Me.DataSetCierreCaja1.COBROS.Count - 1)
        '    If Me.DataSetCierreCaja1.COBROS.Rows(I).Item("Cod_Moneda") = 2 Then
        '        TextEdit4.EditValue += (Me.DataSetCierreCaja1.COBROS.Rows(I).Item("Total") * TipoCambioDolar)
        '    Else
        '        TextEdit4.EditValue += Me.DataSetCierreCaja1.COBROS.Rows(I).Item("Total")
        '    End If
        'Next
        spCargarDatosVenta()
        CargarCobros(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString)
        TextEdit14.EditValue = 0
        For I = 0 To (Me.DataSetCierreCaja1.VENTASCONTADO.Count - 1)
            If Me.DataSetCierreCaja1.VENTASCONTADO.Rows(I).Item("Cod_Moneda") = 2 Then
                TextEdit14.EditValue += (Me.DataSetCierreCaja1.VENTASCONTADO.Rows(I).Item("Total") * TipoCambioDolar)
            Else
                TextEdit14.EditValue += Me.DataSetCierreCaja1.VENTASCONTADO.Rows(I).Item("Total")
            End If
        Next
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Llenar las Operaciones
        'For I = 0 To (Me.DataSetCierreCaja1.OpcionesDePago.Count - 1)

        '    If Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "TAR" Then
        '        For K = 0 To (Me.DataSetCierreCaja1.Detalle_pago_caja.Count - 1)
        '            If Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Id") = Me.DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("Id_ODP") Then
        '                For J = 0 To (Me.DataSetCierreCaja1.TipoTarjeta.Count - 1)
        '                    If Me.DataSetCierreCaja1.TipoTarjeta.Rows(J).Item("Id") = Me.DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("ReferenciaTipo") Then
        '                        Me.DataSetCierreCaja1.TipoTarjeta.Rows(J).Item("TotalS") += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
        '                    End If
        '                Next
        '            End If
        '        Next
        '    End If
        For I = 0 To (Me.DataSetCierreCaja1.OpcionesDePago.Count - 1)
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "TAR" Then

                If Me.DataSetCierreCaja1.TipoTarjeta.Count < 2 Then
                    DataSetCierreCaja1.TipoTarjeta.Rows(0).Item("TotalS") += DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                Else
                    Dim meSumaron As Boolean = False
                    For K = 0 To (Me.DataSetCierreCaja1.Detalle_pago_caja.Count - 1)
                        If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Id") = DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("Id_ODP") Then

                            For J = 0 To (Me.DataSetCierreCaja1.TipoTarjeta.Count - 1)
                                If DataSetCierreCaja1.TipoTarjeta.Rows(J).Item("Id") = DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("ReferenciaTipo") _
                                And DataSetCierreCaja1.TipoTarjeta(J).Nombre.Equals(Me.DataSetCierreCaja1.Detalle_pago_caja(K).ReferenciaTipo) Then
                                    DataSetCierreCaja1.TipoTarjeta.Rows(J).Item("TotalS") += DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                                    meSumaron = True
                                End If
                            Next

                        End If
                    Next
                    If Not meSumaron Then
                        DataSetCierreCaja1.TipoTarjeta.Rows(0).Item("TotalS") += DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                    End If
                End If

            End If

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE ENTRADAS DE LA CAJA - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "MCE" Then
                Textentradas.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio")
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE SALIDAS DE LA CAJA - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "MCS" Then
                TxtSalidas.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio")
            End If
            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE COMISIONES DE LA CAJA 
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "COM" Then
                txTotalComision.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio")
            End If
            '------------------------------------------------------------------------------------------




            Dr1 = Me.tablaresumen.NewRow
            Dr1(0) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Documento")
            Dr1(1) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento")
            Dr1(2) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Nombremoneda")
            Dr1(3) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            'Si es una devoluci�n
            If Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "DVE" Then
                Dr1(4) = "DVE"
            Else 'Si no es una devoluci�n
                Select Case Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago")
                    Case "EFE"
                        Dr1(4) = "EFE"
                    Case "MCS"
                        Dr1(4) = "MCS"
                    Case "MCE"
                        Dr1(4) = "MCE"
                    Case "TAR"
                        Dr1(4) = "TAR"
                    Case "CHE"
                        Dr1(4) = "CHE"
                    Case "TRA"
                        Dr1(4) = "TRA"
                    Case "COM"
                        Dr1(4) = "COM"
                End Select
            End If
            Dr1(5) = (Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio"))
            tablaresumen.Rows.Add(Dr1)
        Next
        DescontarVuelto()
        Me.dgResumen.DataSource = tablaresumen
        'Me.dgOperaciones.DataSource = tabla
        'Me.dgCierre.DataSource = Me.tablacierre
        SubTotal = (TextEdit5.EditValue + (TextEdit7.EditValue * Me.TipoCambioDolar) + TextEdit8.EditValue + (TextEdit10.EditValue * TipoCambioDolar) + (TextEdit12.EditValue * TipoCambioDolar))
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Subtotal") = (SubTotal + Devolucion)
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Devoluciones") = Devolucion
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TotalSistema") = SubTotal
        TextEdit18.EditValue = SubTotal
        TextEdit6.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TotalSistema")
        'TextEdit14.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TotalSistema")
        TextEdit17.EditValue = TextEdit16.EditValue - TextEdit6.EditValue
        TextEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        TextEdit5.Properties.DisplayFormat.FormatString = "#,#0.00"

        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
        'CargarDetalleCierreCaja()
    End Sub

    Private Sub spCargarDatosVenta()
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable
        MontoSalonero = 0
        Imp_Venta = 0
        subTotalVenta = 0
        Try
            If Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString <> 0 Then

                sql.CommandText = " SELECT  * FROM [Restaurante].[dbo].[vs_R_VentasCierreCaja] " & _
                          " where [Num_Apertura] = '" & Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString & "' And [Proveniencia_Venta] = '" & User_Log.PuntoVenta & "'"


                cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))

                If dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("Tipo").Equals("CON") Then
                            subTotalVenta += dt.Rows(i).Item("SubTotal") * dt.Rows(i).Item("Tipo_Cambio")
                        End If
                        MontoSalonero += dt.Rows(i).Item("Monto_Saloero") * dt.Rows(i).Item("Tipo_Cambio")
                        Imp_Venta += dt.Rows(i).Item("Imp_Venta") * dt.Rows(i).Item("Tipo_Cambio")
                    Next
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atenci�n...", MessageBoxButtons.OK)
            Me.Close()
        End Try
    End Sub

    Sub CargarVentasContado(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM VENTASCONTADO where Num_Apertura  = @Numapertura  AND Proveniencia_Venta = @PuntoVenta"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            cmd.Parameters.Add(New SqlParameter("@PuntoVenta", SqlDbType.Int))
            cmd.Parameters("@PuntoVenta").Value = User_Log.PuntoVenta
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.VENTASCONTADO)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarVentasCredito(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM VENTASCREDITO where Num_Apertura  = @Numapertura AND Proveniencia_Venta = @PuntoVenta"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            cmd.Parameters.Add(New SqlParameter("@PuntoVenta", SqlDbType.Int))
            cmd.Parameters("@PuntoVenta").Value = User_Log.PuntoVenta
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.VENTASCREDITO)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Sub CargarAbonos(ByVal Numapertura As String)
        Dim datos As New DataTable
        cFunciones.Llenar_Tabla_Generico("select isnull(sum(MontoPago),0) as Abonos from opcionesdepago where NUMAPERTURA = " & Numapertura, datos, GetSetting("SeeSoft", "REstaurante", "Conexion"))

    End Sub

    Sub CargarCobros(ByVal Numapertura As String)
        'Dim cnn As SqlConnection = Nothing
        '' Dentro de un Try/Catch por si se produce un error
        'Try
        '    Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
        '    cnn = New SqlConnection(sConn)
        '    cnn.Open()
        '    ' Creamos el comando para la consulta
        '    Dim cmd As SqlCommand = New SqlCommand
        '    Dim sel As String = "SELECT * FROM COBROS where NApertura  = @Numapertura "
        '    cmd.CommandText = sel
        '    cmd.Connection = cnn
        '    cmd.CommandType = CommandType.Text
        '    cmd.CommandTimeout = 90
        '    cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
        '    cmd.Parameters("@Numapertura").Value = Numapertura
        '    Dim da As New SqlDataAdapter
        '    da.SelectCommand = cmd
        '    da.Fill(Me.DataSetCierreCaja1.COBROS)
        'Catch ex As System.Exception
        '    MsgBox(ex.ToString)
        'Finally
        '    If Not cnn Is Nothing Then
        '        cnn.Close()
        '    End If
        'End Try
        Try
            Dim datos As New DataTable
            cFunciones.Llenar_Tabla_Generico("select isnull(sum(MontoPago),0) as Abonos from opcionesdepago where tipodocumento = 'ABO' and NUMAPERTURA = " & Numapertura, datos, GetSetting("SeeSoft", "REstaurante", "Conexion"))
            TextEdit4.Text = datos.Rows(0).Item(0)
        Catch ex As Exception
            TextEdit4.Text = 0
        End Try
    End Sub

    Function TotalesPorMonedas(ByRef Dr As DataRow, ByRef Dr1 As DataRow, ByVal CodigoMoneda As String, ByVal ValorCompra As Double, ByVal NombreMoneda As String) As DataRow
        Try
            Dim OperacionesMoneda() As DataRow
            Dim DenominacionesApertura() As DataRow
            Dim Operacion As DataRow
            Dim Denominacion As DataRow
            Dim J As Integer = 0
            Dim Vueltos As Double
            Dim Efectivo, Targeta, Cheque, Transferencia, devoluciones As Double
            Dim Denominaciones As Double
            OperacionesMoneda = Me.DataSetCierreCaja1.OpcionesDePago.Select("CodMoneda = '" & CodigoMoneda & "'")

            'DenominacionesApertura = Me.DataSetCierreCaja1.DenominacionesAperturas.Select("CodMoneda = '" & CodigoMoneda & "'")
            'Cargar opciones de pago

            While J < OperacionesMoneda.Length
                Operacion = OperacionesMoneda(J)
                If Operacion("FormaPago") = "DVE" Then
                    devoluciones += Operacion("MontoPago")
                    Devolucion += (Operacion("MontoPago") * ValorCompra)
                Else
                    ' Vuelto += (Operacion("Denominacion") * Operacion("TipoCambio")) - (Operacion("MontoPago"))
                    Select Case Operacion("FormaPago")
                        Case "EFE", "MCE"
                            Efectivo += Operacion("MontoPago")
                        Case "MCS", "COM"
                            Efectivo -= Operacion("MontoPago")
                        Case "TAR"
                            Targeta += Operacion("MontoPago")
                        Case "CHE"
                            Cheque += Operacion("MontoPago")
                        Case "TRA"
                            Transferencia += Operacion("MontoPago")
                    End Select
                End If
                J += 1
            End While
            J = 0
            'Cargar Denominaciones apertura
            'While J < DenominacionesApertura.Length
            '    Denominacion = DenominacionesApertura(J)
            '    Efectivo += Denominacion("Monto_Total")
            '    J += 1
            'End While

            Dr(1) = (Efectivo)
            Dr(2) = Targeta
            Dr(3) = Cheque
            Dr(4) = Transferencia
            Dr(5) = devoluciones
            Dr(6) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra
            Dr1(0) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones)
            Dr1(1) = NombreMoneda
            Dr1(2) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function

    Sub DescontarVuelto()
        Dim i As Integer
        'Efectivo Colones se le resta Los Vueltos
        ' tabla.Rows(0).Item(1) = (tabla.Rows(0).Item(1) - Vuelto)
        'Total Colones se le resta Los vueltos
        'tabla.Rows(0).Item(6) = (tabla.Rows(0).Item(6) - Vuelto)
        'tablacierre.Rows(0).Item(0) = tabla.Rows(0).Item(6)
        'tablacierre.Rows(0).Item(2) = tabla.Rows(0).Item(6)
        For i = 0 To (tabla.Rows.Count - 1)
            SubTotal += tabla.Rows(i).Item(6)
        Next
    End Sub

    Sub Cargando()
        Dim I As Integer

        Try
            If Me.DataSetCierreCaja1.ArqueoTarjeta.Rows.Count <> 0 Then
                For I = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Count - 1
                    Me.DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Total") = Me.DataSetCierreCaja1.ArqueoTarjeta.Rows(I).Item("Monto")
                Next
            End If
            TextEdit15.EditValue = 0
            For I = 0 To Me.DataSetCierreCaja1.VENTASPREPAGOS.Count - 1
                If Me.DataSetCierreCaja1.VENTASPREPAGOS.Rows(I).Item("Cod_Moneda") = 2 Then
                    TextEdit15.EditValue += (Me.DataSetCierreCaja1.VENTASPREPAGOS.Rows(I).Item("Monto") * Me.TipoCambioDolar)
                Else

                    TextEdit15.EditValue += (Me.DataSetCierreCaja1.VENTASPREPAGOS.Rows(I).Item("Monto") * Me.TipoCambioDolar)
                End If
            Next
            TextEdit20.EditValue = 0
            For I = 0 To Me.DataSetCierreCaja1.VENTASCREDITO.Count - 1
                If Me.DataSetCierreCaja1.VENTASCREDITO.Rows(I).Item("Cod_Moneda") = 2 Then
                    TextEdit20.EditValue += (Me.DataSetCierreCaja1.VENTASCREDITO.Rows(I).Item("Total") * Me.TipoCambioDolar)
                Else

                    TextEdit20.EditValue += Me.DataSetCierreCaja1.VENTASCREDITO.Rows(I).Item("Total")
                End If
            Next
            TextEdit1.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("EfectivoColones")
            TextEdit2.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("EfectivoDolares")
            TextEdit9.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TarjetaColones")
            TextEdit11.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TarjetaDolares")
            TextEdit13.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TravelCheck")
            TextEdit16.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Total")
            TextEdit3.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "Apertura_Total_Tope").Current("Monto_Tope")
            TextEdit19.EditValue = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Total")
            Me.TipoCambioDolar = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TipoCambioD")
            Me.TipoCambioEuro = Me.BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TipoCambioE")

            ' TextEdit18.EditValue = (TextEdit5.EditValue + (TextEdit7.EditValue * Me.TipoCambioDolar) + TextEdit8.EditValue + (TextEdit10.EditValue * TipoCambioDolar) + (TextEdit12.EditValue * TipoCambioDolar))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub CargarDetalleCierreCaja()
        Dim I As Integer
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Usuario") = TextBox6.Text
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NombreUsuario") = txtNombreUsuario.Text
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Anulado") = 0
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Devoluciones") = 0
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Subtotal") = TextEdit6.EditValue
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TotalSistema") = TextEdit6.EditValue
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Equivalencia") = 0
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TravelCheckSistema") = TextEdit12.EditValue
        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TravelCheckCajero") = TextEdit13.EditValue


        Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
        '   Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()
        '  Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").CancelCurrentEdit()

        'Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").EndCurrentEdit()
        'Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").AddNew()

        'Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").EndCurrentEdit()
        'Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()

        For I = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Rows.Count - 1
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").EndCurrentEdit()
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").AddNew()
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("Id_TipoTarjeta") = Me.DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Id")
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("MontoSistema") = Me.DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("TotalS")
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("MontoCajero") = Me.DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Total")
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").EndCurrentEdit()
        Next
        For I = 0 To Me.DataSetCierreCaja1.Moneda.Rows.Count - 1
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").EndCurrentEdit()
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").AddNew()
            If Me.DataSetCierreCaja1.Moneda.Rows(I).Item("Simbolo") = "$" Then
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("Id_Moneda") = Me.DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda")
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoSistema") = TextEdit7.EditValue
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoCajero") = TextEdit2.EditValue
            Else
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("Id_Moneda") = Me.DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda")
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoSistema") = TextEdit5.EditValue
                Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoCajero") = TextEdit1.EditValue
            End If
            Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").EndCurrentEdit()
        Next
    End Sub
#End Region

    Private Function getHoraApertura(ByVal _id As Long) As Date
        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("select fecha from aperturacaja where napertura = " & _id, dts, GetSetting("Seesoft", "Restaurante", "Conexion"))
        Return CDate(dts.Rows(0).Item(0))
    End Function

    Private Function getHoraCierre(ByVal _id As Long) As Date
        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT FECHA from CIERRECAJA WHERE NUMEROCIERRE  = " & _id, dts, GetSetting("Seesoft", "Restaurante", "Conexion"))
        Return CDate(dts.Rows(0).Item(0))
    End Function

    Private Function FnCargarMontoSalonero() As Decimal
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable
        If Me.DataSetCierreCaja1.cierrecaja.Rows.Count > 0 Then

            If Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura") <> 0 Then

                sql.CommandText = " SELECT  isnull(sum(Monto_Saloero),0) as Monto FROM [Restaurante].[dbo].[vs_R_VentasCierreCaja] " & _
                          " where [Num_Apertura] = '" & Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura") & "' And [Proveniencia_Venta] = '" & User_Log.PuntoVenta & "'"


                cFunciones.spCargarDatos(sql, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))

                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item("Monto")
                End If
                Return 0
            End If
        End If
    End Function
#Region "Imprimir"
    Private Sub Imprimir()
        Try
            If check_PVE.Checked = True Then
                Dim rpt As New cierre_caja
                Dim pantalla As New frmVisorReportes
                pantalla.MdiParent = Me.ParentForm
                CrystalReportsConexion.LoadReportViewer(pantalla.rptViewer, rpt)
                rpt.SetParameterValue(0, Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NumeroCierre")) 'cierre
                rpt.SetParameterValue(1, TextEdit14.Text) 'contado
                rpt.SetParameterValue(2, TextEdit20.Text) 'credito
                rpt.SetParameterValue(3, TextEdit4.Text) 'abono  
                rpt.SetParameterValue(4, MontoSalonero)
                rpt.SetParameterValue(5, Imp_Venta)
                rpt.SetParameterValue(6, subTotalVenta)
                rpt.SetParameterValue(7, CDbl(txTotalComision.EditValue))
                pantalla.Show()
            Else
                If Not GetSetting("SeeSoft", "Restaurante", "ReporteCierre").Equals("1") Then

                    Dim rptCierreCaja As New ReporteCierreCaja1
                    Dim visor As New frmVisorReportes
                    visor.MdiParent = Me.ParentForm
                    rptCierreCaja.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptCierreCaja)
                    rptCierreCaja.SetParameterValue(0, Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NumeroCierre"))
                    rptCierreCaja.SetParameterValue(1, Format(Me.totalEfectivoSinImpuesto, "##,##0.00"))
                    rptCierreCaja.SetParameterValue(2, Format(Me.totalTarjetaSinImpuesto, "##,##0.00"))
                    rptCierreCaja.SetParameterValue(3, Format(Me.deposito, "##,##0.00"))
                    visor.Show()
                Else
                    Dim rptCierreCaja As New rptnewCierre
                    Dim visor As New frmVisorReportes
                    visor.MdiParent = Me.ParentForm
                    CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptCierreCaja)
                    rptCierreCaja.SetParameterValue(0, Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NumeroCierre"))
                    rptCierreCaja.SetParameterValue(1, getHoraApertura(txtcodaperturacajero.Text))
                    rptCierreCaja.SetParameterValue(2, getHoraCierre(Me.BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NumeroCierre")))
                    rptCierreCaja.SetParameterValue(3, TxtSalidas.Text)
                    rptCierreCaja.SetParameterValue(4, CDec(TextEdit6.Text) + CDec(TxtSalidas.Text) - CDec(Textentradas.Text))
                    rptCierreCaja.SetParameterValue(5, FnCargarMontoSalonero())
                    visor.Show()
                End If
            End If
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    Function totalEfectivoSinImpuesto() As Double
        Dim totalEfec As Double = 0
        Dim dtOP As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT MontoPago, TipoCambio, Documento, MontoPago * TipoCambio as Monto" & _
        " FROM OpcionesDePago " & _
        "WHERE (Numapertura = " & Me.txtcodaperturacajero.Text & ") AND (FormaPago = 'EFE')", dtOP)
        Dim dtFac As New DataTable

        cFunciones.Llenar_Tabla_Generico("SELECT     Num_Factura, Imp_Venta, Monto_Saloero, Num_Apertura, Proveniencia_Venta " & _
        "FROM Ventas " & _
        "WHERE     (Proveniencia_Venta = " & User_Log.PuntoVenta & ") AND (Num_Apertura = " & DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura") & ")", dtFac, GetSetting("SeeSoft", "Hotel", "Conexion"))
        Dim encontre As Boolean = False
        For i As Integer = 0 To dtOP.Rows.Count - 1
            Dim Total As Double = 0
            For j As Integer = 0 To dtFac.Rows.Count - 1
                If dtOP.Rows(i).Item("Documento") = dtFac.Rows(j).Item("Num_Factura") Then
                    encontre = True
                    If dtFac.Rows(j).Item("Imp_Venta") > 0 And dtFac.Rows(j).Item("Monto_Saloero") > 0 Then
                        Total = dtOP.Rows(i).Item("Monto") / 1.23
                    Else
                        If dtFac.Rows(j).Item("Imp_Venta") > 0 Then
                            Total = dtOP.Rows(i).Item("Monto") / 1.13
                        Else
                            Total = dtOP.Rows(i).Item("Monto")
                        End If
                        If dtFac.Rows(j).Item("Monto_Saloero") > 0 Then
                            Total = dtOP.Rows(i).Item("Monto") / 1.1
                        Else
                            Total = dtOP.Rows(i).Item("Monto")
                        End If

                    End If


                End If
            Next
            If Not encontre Then
                totalEfec += dtOP.Rows(i).Item("Monto")
            Else
                totalEfec += Total

            End If

        Next

        Return totalEfec

    End Function
    Function totalTarjetaSinImpuesto() As Double
        Dim totalTarj As Double = 0
        Dim dtOP As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT MontoPago, TipoCambio, Documento, MontoPago * TipoCambio as Monto" & _
        " FROM OpcionesDePago " & _
        "WHERE (Numapertura = " & Me.txtcodaperturacajero.Text & ") AND (FormaPago = 'TAR')", dtOP)
        Dim dtFac As New DataTable

        cFunciones.Llenar_Tabla_Generico("SELECT     Num_Factura, Imp_Venta, Monto_Saloero, Num_Apertura, Proveniencia_Venta " & _
        "FROM Ventas " & _
        "WHERE     (Proveniencia_Venta = " & User_Log.PuntoVenta & ") AND (Num_Apertura = " & DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura") & ")", dtFac, GetSetting("SeeSoft", "Hotel", "Conexion"))
        Dim encontre As Boolean = False
        For i As Integer = 0 To dtOP.Rows.Count - 1
            Dim Total As Double = 0
            For j As Integer = 0 To dtFac.Rows.Count - 1
                If dtOP.Rows(i).Item("Documento") = dtFac.Rows(j).Item("Num_Factura") Then
                    encontre = True
                    If dtFac.Rows(j).Item("Imp_Venta") > 0 And dtFac.Rows(j).Item("Monto_Saloero") > 0 Then
                        Total = dtOP.Rows(i).Item("Monto") / 1.23
                    Else
                        If dtFac.Rows(j).Item("Imp_Venta") > 0 Then
                            Total = dtOP.Rows(i).Item("Monto") / 1.13
                        Else
                            Total = dtOP.Rows(i).Item("Monto")
                        End If
                        If dtFac.Rows(j).Item("Monto_Saloero") > 0 Then
                            Total = dtOP.Rows(i).Item("Monto") / 1.1
                        Else
                            Total = dtOP.Rows(i).Item("Monto")
                        End If

                    End If


                End If
            Next
            If Not encontre Then
                totalTarj += dtOP.Rows(i).Item("Monto")
            Else
                totalTarj += Total

            End If

        Next

        Return totalTarj

    End Function
    Function deposito() As Double
        Dim totaldep As Double = 0
        Dim dtOP As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT Sum(MontoPago * TipoCambio) AS TotalEfec" & _
        " FROM OpcionesDePago" & _
        " WHERE (Numapertura = " & Me.txtcodaperturacajero.Text & ") AND (FormaPago = 'EFE')", dtOP)
        If dtOP.Rows.Count > 0 Then

            If dtOP.Rows(0).Item("TotalEfec") Is DBNull.Value Then
                totaldep = 0
            Else
                totaldep = dtOP.Rows(0).Item("TotalEfec")
            End If

        End If
        Return totaldep

    End Function

#End Region

#Region "Buscar"
    Private Sub Consultar_Cierre()
        Dim BuscarCierreCajas As New BuscarCierreCaja
        'Me.dgCierre.DataSource = Nothing
        'Me.dgOperaciones.DataSource = Nothing
        Me.dgResumen.DataSource = Nothing
        Me.tabla.Clear()
        Me.tablacierre.Clear()
        Me.tablaresumen.Clear()
        'Me.Dscierrecaja1.DenominacionesAperturas.Clear()
        DataSetCierreCaja1.aperturacaja.Clear()
        DataSetCierreCaja1.CierreCaja_DetMon.Clear()
        DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
        DataSetCierreCaja1.cierrecaja.Clear()
        DataSetCierreCaja1.ArqueoCajas.Clear()
        DataSetCierreCaja1.Apertura_Total_Tope.Clear()

        AdapterTipoTarjeta.Fill(Me.DataSetCierreCaja1.TipoTarjeta)
        For i As Integer = 0 To Me.DataSetCierreCaja1.TipoTarjeta.Count - 1
            With Me.DataSetCierreCaja1.TipoTarjeta(i)
                .TotalS = 0
                .Total = 0
                .EndEdit()
            End With
        Next

        If BuscarCierreCajas.ShowDialog = DialogResult.OK Then
            LimpiaDataSet()
            CargarCierre1(BuscarCierreCajas.Numapertura.Text)
            CargarAperturaConsulta(Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura"))
            CargarOpcionesPago(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarAperturaTotal(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarPrepagos(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarArqueo(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarVentasContado(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarVentasCredito(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarCobros(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            If Me.DataSetCierreCaja1.ArqueoCajas.Rows.Count <> 0 Then CargarArqueoTarjeta(Me.DataSetCierreCaja1.ArqueoCajas.Rows(0).Item("Id"))

            Me.Cargando()
            Me.CargarCierre()
            txtcodaperturacajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
            txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
            'CargarCierreConsulta()
            Me.ToolBar1.Buttons(3).Enabled = True
            Me.ToolBar1.Buttons(4).Enabled = True
            If Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Anulado") = True Then
                Me.lblanulado.Visible = True
                Me.ToolBarEliminar.Enabled = False
            Else
                Me.lblanulado.Visible = False
                Me.ToolBarEliminar.Enabled = True
                btVentas.Enabled = True
            End If
        Else
            MsgBox("No se obtuvieron resultados", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub CargarCierre1(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM  cierrecaja where NumeroCierre = '" & Id & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.cierrecaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub CargarAperturaConsulta(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM aperturacaja where  NApertura = '" & Id & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.aperturacaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub CargarCierreConsulta()
        Dim I As Integer
        Dim Dr As System.Data.DataRow
        Dim Dr1 As System.Data.DataRow
        Dim Dr2 As System.Data.DataRow
        'Valores Vacios a los grid
        Me.dgResumen.DataSource = Nothing
        'Me.dgOperaciones.DataSource = Nothing
        'Me.dgCierre.DataSource = Nothing
        Me.txtcodcajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula")
        Me.txtnomcajero.Text = Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Nombre")
        SubTotal = 0
        Devolucion = 0
        'Vuelto = 0
        'LLenar Los Totales Por monedas

        'Llenar las Operaciones
        For I = 0 To (Me.DataSetCierreCaja1.OpcionesDePago.Count - 1)
            Dr1 = Me.tablaresumen.NewRow
            Dr1(0) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Documento")
            Dr1(1) = 0
            Dr1(2) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Nombremoneda")
            Dr1(3) = Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            'Si es una devoluci�n
            If Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "DVE" Then
                Dr1(4) = "DVE"
            Else 'Si no es una devoluci�n
                Select Case Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago")
                    Case "EFE"
                        Dr1(4) = "EFE"
                    Case "TAR"
                        Dr1(4) = "TAR"
                    Case "MCS"
                        Dr1(4) = "MCS"
                    Case "MCE"
                        Dr1(4) = "MCE"
                    Case "CHE"
                        Dr1(4) = "CHE"
                    Case "TRA"
                        Dr1(4) = "TRA"
                    Case "COM"
                        Dr1(4) = "COM"
                End Select
            End If
            Dr1(5) = (Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Denominacion") * Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio"))
            tablaresumen.Rows.Add(Dr1)
        Next
        DescontarVuelto()
        Me.dgResumen.DataSource = tablaresumen
    End Sub
#End Region

    Private Sub txtcodcajero_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcodcajero.TextChanged

    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        If (txtNombreUsuario.Text = Nothing) Then
            MsgBox("Para realizar un cierre de caja es necesario que ingreses el nombre de usuario", MsgBoxStyle.Critical, "Usuario desconocido")
            Exit Sub
        Else
            LimpiaDataSet()
            Carga()
            consultarcliente()
        End If
    End Sub

    Private Sub ButtonAperturas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAperturas.Click
        If PMU.Others Then Me.verCajas() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub verCajas()
        Dim vCajas As New FormCajas
        vCajas.fecha = Me.DateTimePicker1.Value
        vCajas.ShowDialog()

    End Sub

    Private Sub TextEdit4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextEdit4.EditValueChanged

    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub btVentas_Click(sender As Object, e As EventArgs) Handles btVentas.Click
        Dim frm As New frmTotalVentas
        If Me.DataSetCierreCaja1.cierrecaja.Rows.Count > 0 Then
            frm.pubApertura = Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura")
            frm.ShowDialog()
        End If
    End Sub
End Class
