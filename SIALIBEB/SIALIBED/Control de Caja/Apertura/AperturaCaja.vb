Imports System.Data.SqlClient

Public Class AperturaCaja
    Inherits Detalle_Cortesia
    Dim cedula As String
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDolar As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBoxColon As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents check_PVE As System.Windows.Forms.CheckBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Dim PMU As New PerfilModulo_Class

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents NOM As System.Windows.Forms.Label
    Friend WithEvents IDEN As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents smpBtnNuevo1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents n_t_t As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMonto_Tope As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonedaNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtobservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents smpBtnNuevo2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetAperturaCaja1 As SIALIBEB.DataSetAperturaCaja
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterAperturaCaja As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterAperturaTotalTope As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterUsuario As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents AdapterDenominacionesApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colDenominacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonto_Total As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonedaNombre1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Vt_monto_tope As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ValidText2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ValidText1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents VtMontoTopeCol As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterMoneda2 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsApertura1 As DsApertura
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AperturaCaja))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.check_PVE = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.NOM = New System.Windows.Forms.Label
        Me.IDEN = New System.Windows.Forms.Label
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.DataSetAperturaCaja1 = New SIALIBEB.DataSetAperturaCaja
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Vt_monto_tope = New DevExpress.XtraEditors.TextEdit
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
        Me.smpBtnNuevo1 = New DevExpress.XtraEditors.SimpleButton
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMonto_Tope = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMonedaNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.n_t_t = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lblAnulado = New System.Windows.Forms.Label
        Me.txtobservacion = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.TextBoxColon = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextBoxDolar = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.VtMontoTopeCol = New DevExpress.XtraEditors.TextEdit
        Me.ValidText1 = New DevExpress.XtraEditors.TextEdit
        Me.ValidText2 = New DevExpress.XtraEditors.TextEdit
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMonedaNombre1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDenominacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colValor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMonto_Total = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label5 = New System.Windows.Forms.Label
        Me.smpBtnNuevo2 = New DevExpress.XtraEditors.SimpleButton
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.AdapterAperturaCaja = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterAperturaTotalTope = New System.Data.SqlClient.SqlDataAdapter
        Me.AdapterUsuario = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.AdapterDenominacionesApertura = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.Label7 = New System.Windows.Forms.Label
        Me.AdapterMoneda2 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsApertura1 = New SIALIBEB.DsApertura
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataSetAperturaCaja1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.Vt_monto_tope.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.VtMontoTopeCol.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidText1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidText2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DsApertura1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        '
        'ToolBarImprimir
        '
        resources.ApplyResources(Me.ToolBarImprimir, "ToolBarImprimir")
        '
        'ToolBarExcel
        '
        resources.ApplyResources(Me.ToolBarExcel, "ToolBarExcel")
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        '
        'ToolBar1
        '
        resources.ApplyResources(Me.ToolBar1, "ToolBar1")
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.DataMember = "aperturacaja"
        Me.DataNavigator.DataSource = Me.DataSetAperturaCaja1
        resources.ApplyResources(Me.DataNavigator, "DataNavigator")
        '
        'TituloModulo
        '
        resources.ApplyResources(Me.TituloModulo, "TituloModulo")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.check_PVE)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.NOM)
        Me.GroupBox1.Controls.Add(Me.IDEN)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'check_PVE
        '
        Me.check_PVE.AutoEllipsis = True
        Me.check_PVE.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.check_PVE, "check_PVE")
        Me.check_PVE.Name = "check_PVE"
        Me.check_PVE.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'NOM
        '
        Me.NOM.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        resources.ApplyResources(Me.NOM, "NOM")
        Me.NOM.ForeColor = System.Drawing.Color.White
        Me.NOM.Name = "NOM"
        '
        'IDEN
        '
        Me.IDEN.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        resources.ApplyResources(Me.IDEN, "IDEN")
        Me.IDEN.ForeColor = System.Drawing.Color.White
        Me.IDEN.Name = "IDEN"
        '
        'RadioButton1
        '
        resources.ApplyResources(Me.RadioButton1, "RadioButton1")
        Me.RadioButton1.ForeColor = System.Drawing.Color.Red
        Me.RadioButton1.Name = "RadioButton1"
        '
        'DataSetAperturaCaja1
        '
        Me.DataSetAperturaCaja1.DataSetName = "DataSetAperturaCaja"
        Me.DataSetAperturaCaja1.Locale = New System.Globalization.CultureInfo("es")
        Me.DataSetAperturaCaja1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Vt_monto_tope)
        Me.GroupBox5.Controls.Add(Me.SimpleButton2)
        Me.GroupBox5.Controls.Add(Me.smpBtnNuevo1)
        Me.GroupBox5.Controls.Add(Me.GridControl3)
        Me.GroupBox5.Controls.Add(Me.n_t_t)
        Me.GroupBox5.Controls.Add(Me.Label11)
        Me.GroupBox5.Controls.Add(Me.ComboBox3)
        Me.GroupBox5.Controls.Add(Me.Label4)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'Vt_monto_tope
        '
        Me.Vt_monto_tope.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope.Monto_Tope", True))
        resources.ApplyResources(Me.Vt_monto_tope, "Vt_monto_tope")
        Me.Vt_monto_tope.Name = "Vt_monto_tope"
        '
        '
        '
        Me.Vt_monto_tope.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.Vt_monto_tope.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.Vt_monto_tope.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Vt_monto_tope.Properties.Enabled = False
        '
        'SimpleButton2
        '
        resources.ApplyResources(Me.SimpleButton2, "SimpleButton2")
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        '
        'smpBtnNuevo1
        '
        resources.ApplyResources(Me.smpBtnNuevo1, "smpBtnNuevo1")
        Me.smpBtnNuevo1.Name = "smpBtnNuevo1"
        Me.smpBtnNuevo1.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        '
        'GridControl3
        '
        resources.ApplyResources(Me.GridControl3, "GridControl3")
        Me.GridControl3.DataSource = Me.DataSetAperturaCaja1
        '
        '
        '
        Me.GridControl3.EmbeddedNavigator.Name = ""
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMonto_Tope, Me.colMonedaNombre})
        resources.ApplyResources(Me.GridView3, "GridView3")
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colMonto_Tope
        '
        resources.ApplyResources(Me.colMonto_Tope, "colMonto_Tope")
        Me.colMonto_Tope.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colMonto_Tope.DisplayFormat.FormatString = "#,#0.00"
        Me.colMonto_Tope.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMonto_Tope.FilterInfo = ColumnFilterInfo1
        Me.colMonto_Tope.Name = "colMonto_Tope"
        Me.colMonto_Tope.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonto_Tope.VisibleIndex = 0
        Me.colMonto_Tope.Width = 171
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colMonedaNombre
        '
        resources.ApplyResources(Me.colMonedaNombre, "colMonedaNombre")
        Me.colMonedaNombre.FilterInfo = ColumnFilterInfo2
        Me.colMonedaNombre.Name = "colMonedaNombre"
        Me.colMonedaNombre.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonedaNombre.VisibleIndex = 1
        Me.colMonedaNombre.Width = 167
        '
        'n_t_t
        '
        resources.ApplyResources(Me.n_t_t, "n_t_t")
        Me.n_t_t.ForeColor = System.Drawing.SystemColors.Control
        Me.n_t_t.Name = "n_t_t"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Name = "Label11"
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.DataSetAperturaCaja1
        Me.ComboBox3.DisplayMember = "Moneda.MonedaNombre"
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox3, "ComboBox3")
        Me.ComboBox3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.ValueMember = "Moneda.CodMoneda"
        '
        'Label4
        '
        Me.Label4.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Name = "Label4"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblAnulado)
        Me.GroupBox3.Controls.Add(Me.txtobservacion)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'lblAnulado
        '
        resources.ApplyResources(Me.lblAnulado, "lblAnulado")
        Me.lblAnulado.BackColor = System.Drawing.Color.White
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Name = "lblAnulado"
        '
        'txtobservacion
        '
        resources.ApplyResources(Me.txtobservacion, "txtobservacion")
        Me.txtobservacion.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtobservacion.Name = "txtobservacion"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.ForeColor = System.Drawing.SystemColors.Control
        Me.Label6.Name = "Label6"
        '
        'GroupBox2
        '
        Me.GroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.TextBoxColon)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.TextBoxDolar)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.VtMontoTopeCol)
        Me.GroupBox2.Controls.Add(Me.ValidText1)
        Me.GroupBox2.Controls.Add(Me.ValidText2)
        Me.GroupBox2.Controls.Add(Me.ComboBox1)
        Me.GroupBox2.Controls.Add(Me.SimpleButton3)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.GridControl2)
        Me.GroupBox2.Controls.Add(Me.GridControl1)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.smpBtnNuevo2)
        Me.GroupBox2.Controls.Add(Me.ComboBox2)
        Me.GroupBox2.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TextBox2, "TextBox2")
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'TextBoxColon
        '
        Me.TextBoxColon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TextBoxColon, "TextBoxColon")
        Me.TextBoxColon.Name = "TextBoxColon"
        Me.TextBoxColon.ReadOnly = True
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'TextBoxDolar
        '
        Me.TextBoxDolar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TextBoxDolar, "TextBoxDolar")
        Me.TextBoxDolar.Name = "TextBoxDolar"
        Me.TextBoxDolar.ReadOnly = True
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'VtMontoTopeCol
        '
        Me.VtMontoTopeCol.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas.Monto_Total", True))
        resources.ApplyResources(Me.VtMontoTopeCol, "VtMontoTopeCol")
        Me.VtMontoTopeCol.Name = "VtMontoTopeCol"
        '
        '
        '
        Me.VtMontoTopeCol.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.VtMontoTopeCol.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.VtMontoTopeCol.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.VtMontoTopeCol.Properties.Enabled = False
        Me.VtMontoTopeCol.Properties.ReadOnly = True
        '
        'ValidText1
        '
        Me.ValidText1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas.Valor", True))
        resources.ApplyResources(Me.ValidText1, "ValidText1")
        Me.ValidText1.Name = "ValidText1"
        '
        '
        '
        Me.ValidText1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.ValidText1.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.ValidText1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.ValidText1.Properties.Enabled = False
        '
        'ValidText2
        '
        Me.ValidText2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas.Cantidad", True))
        resources.ApplyResources(Me.ValidText2, "ValidText2")
        Me.ValidText2.Name = "ValidText2"
        '
        '
        '
        Me.ValidText2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.ValidText2.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.ValidText2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.ValidText2.Properties.Enabled = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas.Denominacion", True))
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox1, "ComboBox1")
        Me.ComboBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ComboBox1.Items.AddRange(New Object() {resources.GetString("ComboBox1.Items"), resources.GetString("ComboBox1.Items1")})
        Me.ComboBox1.Name = "ComboBox1"
        '
        'SimpleButton3
        '
        resources.ApplyResources(Me.SimpleButton3, "SimpleButton3")
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        '
        'Label10
        '
        Me.Label10.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Label10.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.Name = "Label10"
        '
        'Label9
        '
        Me.Label9.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Name = "Label9"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'GridControl2
        '
        '
        '
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        resources.ApplyResources(Me.GridControl2, "GridControl2")
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        '
        'GridView2
        '
        resources.ApplyResources(Me.GridView2, "GridView2")
        Me.GridView2.Name = "GridView2"
        '
        'GridControl1
        '
        resources.ApplyResources(Me.GridControl1, "GridControl1")
        Me.GridControl1.DataSource = Me.DataSetAperturaCaja1
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMonedaNombre1, Me.colCantidad, Me.colDenominacion, Me.colValor, Me.colMonto_Total})
        resources.ApplyResources(Me.GridView1, "GridView1")
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colMonedaNombre1
        '
        resources.ApplyResources(Me.colMonedaNombre1, "colMonedaNombre1")
        Me.colMonedaNombre1.FilterInfo = ColumnFilterInfo3
        Me.colMonedaNombre1.Name = "colMonedaNombre1"
        Me.colMonedaNombre1.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonedaNombre1.VisibleIndex = 0
        Me.colMonedaNombre1.Width = 111
        '
        'colCantidad
        '
        resources.ApplyResources(Me.colCantidad, "colCantidad")
        Me.colCantidad.DisplayFormat.FormatString = "#,#0.00"
        Me.colCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCantidad.FilterInfo = ColumnFilterInfo4
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 1
        Me.colCantidad.Width = 80
        '
        'colDenominacion
        '
        resources.ApplyResources(Me.colDenominacion, "colDenominacion")
        Me.colDenominacion.FilterInfo = ColumnFilterInfo5
        Me.colDenominacion.Name = "colDenominacion"
        Me.colDenominacion.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDenominacion.VisibleIndex = 2
        Me.colDenominacion.Width = 113
        '
        'colValor
        '
        resources.ApplyResources(Me.colValor, "colValor")
        Me.colValor.DisplayFormat.FormatString = "#,#0.00"
        Me.colValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colValor.FilterInfo = ColumnFilterInfo6
        Me.colValor.Name = "colValor"
        Me.colValor.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colValor.VisibleIndex = 3
        Me.colValor.Width = 105
        '
        'colMonto_Total
        '
        resources.ApplyResources(Me.colMonto_Total, "colMonto_Total")
        Me.colMonto_Total.DisplayFormat.FormatString = "#,#0.00"
        Me.colMonto_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMonto_Total.FilterInfo = ColumnFilterInfo7
        Me.colMonto_Total.Name = "colMonto_Total"
        Me.colMonto_Total.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonto_Total.VisibleIndex = 4
        Me.colMonto_Total.Width = 150
        '
        'Label5
        '
        Me.Label5.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Name = "Label5"
        '
        'smpBtnNuevo2
        '
        resources.ApplyResources(Me.smpBtnNuevo2, "smpBtnNuevo2")
        Me.smpBtnNuevo2.Name = "smpBtnNuevo2"
        Me.smpBtnNuevo2.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas.CodMoneda", True))
        Me.ComboBox2.DataSource = Me.DataSetAperturaCaja1
        Me.ComboBox2.DisplayMember = "Moneda2.MonedaNombre"
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox2, "ComboBox2")
        Me.ComboBox2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.ValueMember = "Moneda2.CodMoneda"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Name = "Label3"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        resources.ApplyResources(Me.Label36, "Label36")
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Name = "Label36"
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.txtNombreUsuario, "txtNombreUsuario")
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.txtUsuario, "txtUsuario")
        Me.txtUsuario.Name = "txtUsuario"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=JORDAN-PC\PRUEBAS;Initial Catalog=Restaurante;Integrated Security=Tru" & _
            "e"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
            "turacaja"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula")})
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura")})
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterAperturaCaja
        '
        Me.AdapterAperturaCaja.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterAperturaCaja.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterAperturaCaja.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterAperturaCaja.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.AdapterAperturaCaja.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertur" & _
            "a_Total_Tope"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre")})
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id_total_tope", System.Data.SqlDbType.Int, 4, "id_total_tope")})
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterAperturaTotalTope
        '
        Me.AdapterAperturaTotalTope.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterAperturaTotalTope.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterAperturaTotalTope.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterAperturaTotalTope.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Apertura_Total_Tope", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id_total_tope", "id_total_tope"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Monto_Tope", "Monto_Tope"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre")})})
        Me.AdapterAperturaTotalTope.UpdateCommand = Me.SqlUpdateCommand3
        '
        'AdapterUsuario
        '
        Me.AdapterUsuario.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterUsuario.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Perfil", "Perfil")})})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT     Cedula, Nombre, Clave_Entrada, Clave_Interna, Perfil" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         Usu" & _
            "arios"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'AdapterDenominacionesApertura
        '
        Me.AdapterDenominacionesApertura.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterDenominacionesApertura.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterDenominacionesApertura.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterDenominacionesApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DenominacionesAperturas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Valor", "Valor"), New System.Data.Common.DataColumnMapping("Monto_Total", "Monto_Total"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura")})})
        Me.AdapterDenominacionesApertura.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = resources.GetString("SqlDeleteCommand4.CommandText")
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = resources.GetString("SqlInsertCommand4.CommandText")
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"), New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"), New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura")})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Denominacion, Cantidad, Valor, Monto_Total, CodMoneda, MonedaNombre, Id, N" & _
            "Apertura FROM DenominacionesAperturas"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"), New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"), New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'Label7
        '
        Me.Label7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAperturaCaja1, "Moneda.CodMoneda", True))
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'AdapterMoneda2
        '
        Me.AdapterMoneda2.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterMoneda2.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterMoneda2.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterMoneda2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda2", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.AdapterMoneda2.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'DsApertura1
        '
        Me.DsApertura1.DataSetName = "DsApertura"
        Me.DsApertura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AperturaCaja
        '
        resources.ApplyResources(Me, "$this")
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label6)
        Me.Name = "AperturaCaja"
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataSetAperturaCaja1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.Vt_monto_tope.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.VtMontoTopeCol.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidText1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidText2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DsApertura1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub AperturaCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim conectadobd As New SqlClient.SqlConnection
            Dim cConexion As New ConexionR

            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            Me.DataSetAperturaCaja1.aperturacaja.NAperturaColumn.AutoIncrement = True
            Me.DataSetAperturaCaja1.aperturacaja.NAperturaColumn.AutoIncrementSeed = -1
            Me.DataSetAperturaCaja1.aperturacaja.NAperturaColumn.AutoIncrementStep = -1

            Me.DataSetAperturaCaja1.Apertura_Total_Tope.id_total_topeColumn.AutoIncrement = True
            Me.DataSetAperturaCaja1.Apertura_Total_Tope.id_total_topeColumn.AutoIncrementSeed = -1
            Me.DataSetAperturaCaja1.Apertura_Total_Tope.id_total_topeColumn.AutoIncrementStep = -1

            Binding()

            Me.AdapterMoneda.Fill(Me.DataSetAperturaCaja1.Moneda)
            Me.AdapterMoneda2.Fill(Me.DataSetAperturaCaja1.Moneda2)
            Me.AdapterUsuario.Fill(Me.DataSetAperturaCaja1.Usuarios)
            ValorPorDefecto()

            Try
                Me.check_PVE.Checked = GetSetting("SeeSoft", "Restaurante", "PVE")
            Catch ex As Exception
                SaveSetting("SeeSoft", "Restaurante", "PVE", 0)
            End Try

            Me.GroupBox5.Enabled = False
            Me.GroupBox2.Enabled = False

            '---------------------------------------------------------------
            'VERIFICA SI PIDE O NO EL USUARIO
           
            If gloNoClave Then
                Loggin_Usuario()
            Else
                txtUsuario.Focus()
            End If

            '---------------------------------------------------------------
        Catch ex As Exception

            MsgBox(ex.Message)
            MsgBox("Error al tratar de cargar el formulario, Intenete otra vez, si el problema persiste comuniqueselo al administrador de Sistema", MsgBoxStyle.Critical)
        End Try
    End Sub
#End Region

#Region "Binding"
    Sub Binding()
        Me.NOM.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAperturaCaja1, "aperturacaja.Nombre"))
        Me.IDEN.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAperturaCaja1, "aperturacaja.Cedula"))
        Me.RadioButton1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetAperturaCaja1, "aperturacaja.Anulado"))
        Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope.CodMoneda"))
        Me.txtobservacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAperturaCaja1, "aperturacaja.Observaciones"))
    End Sub
#End Region

#Region "Valores Por Defecto"
    Private Sub ValorPorDefecto()
        'Apertura Caja
        Me.DataSetAperturaCaja1.aperturacaja.EstadoColumn.DefaultValue = "A"
        Me.DataSetAperturaCaja1.aperturacaja.FechaColumn.DefaultValue = Now
        Me.DataSetAperturaCaja1.aperturacaja.AnuladoColumn.DefaultValue = "False"
        Me.DataSetAperturaCaja1.aperturacaja.ObservacionesColumn.DefaultValue = " "
        'Monto Tope
        Me.DataSetAperturaCaja1.Apertura_Total_Tope.CodMonedaColumn.DefaultValue = Me.DataSetAperturaCaja1.Moneda.Rows(0).Item("CodMoneda")
        Me.DataSetAperturaCaja1.Apertura_Total_Tope.Monto_TopeColumn.DefaultValue = "0.00"
        'Denominaciones
        Me.DataSetAperturaCaja1.DenominacionesAperturas.CantidadColumn.DefaultValue = "1"
        Me.DataSetAperturaCaja1.DenominacionesAperturas.DenominacionColumn.DefaultValue = "BILLETE"
        Me.DataSetAperturaCaja1.DenominacionesAperturas.ValorColumn.DefaultValue = "0.00"
        Me.DataSetAperturaCaja1.DenominacionesAperturas.Monto_TotalColumn.DefaultValue = "0.00"
        Me.DataSetAperturaCaja1.DenominacionesAperturas.CodMonedaColumn.DefaultValue = Me.DataSetAperturaCaja1.Moneda.Rows(0).Item("CodMoneda")
    End Sub
#End Region

#Region "Validar Usuario"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                Me.AdapterUsuario.Fill(Me.DsApertura1)
                Dim Usuario_autorizadores() As System.Data.DataRow
                Dim Usua As System.Data.DataRow
                Usuario_autorizadores = Me.DsApertura1.Usuarios.Select("Clave_Interna ='" & txtUsuario.Text & "'")
                ' Si existe el Usuario
                If Usuario_autorizadores.Length <> 0 Then

                    Usua = Usuario_autorizadores(0)
                    Me.txtNombreUsuario.Text = Usua("Nombre")
                    Me.cedula = Usua("Cedula")
                    txtUsuario.Enabled = False
                    'Activar ToolBar
                    Me.ToolBarNuevo.Enabled = True
                    Me.ToolBarEliminar.Enabled = False
                    Me.ToolBarRegistrar.Enabled = True
                    PMU = VSM(cedula, Me.Name)
                    If PMU.Find Then Me.NuevaApertura() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Else
                    MsgBox("Contrase�a interna incorrecta", MsgBoxStyle.Exclamation)
                    txtUsuario.Text = ""
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Restaurante - Entrada _Usuario")
            End Try
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Me.txtNombreUsuario.Text = User_Log.Nombre
                Me.cedula = User_Log.Cedula
                txtUsuario.Enabled = False
                'Activar ToolBar
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarRegistrar.Enabled = True
                PMU = VSM(cedula, Me.Name)
                If PMU.Find Then Me.NuevaApertura() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Nueva Aperura"
    Private Sub NuevaApertura()
        Dim FrmDatos_Cajeros As New FrmDatos_Cajeros

        Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
        Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
        Me.DataSetAperturaCaja1.aperturacaja.Clear()
        muestraTotal()

        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            If FrmDatos_Cajeros.ShowDialog = Windows.Forms.DialogResult.OK Then
                open_cashdrawer()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").AddNew()
                Me.NOM.Text = FrmDatos_Cajeros.nombre_cajero
                Me.IDEN.Text = FrmDatos_Cajeros.cedula_cajero
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").AddNew()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").CancelCurrentEdit()
                Me.GroupBox5.Enabled = True
                Me.GroupBox2.Enabled = True
                smpBtnNuevo1.Focus()
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = False
                Me.ToolBar1.Buttons(2).Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False


                'TextBoxColon.Text = 0.0
                'TextBox1.Text = 0.0
                'TextBoxDolar.Text = 0.0
                'TextBox2.Text = 0.0


            Else
                If FrmDatos_Cajeros.DialogResult = Windows.Forms.DialogResult.Abort Then
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").CancelCurrentEdit()
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(1).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = False
                    'Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
                    'Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
                    'Me.DataSetAperturaCaja1.aperturacaja.Clear()

                Else
                    MsgBox("Debes Seleccionar Un cajero")
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").CancelCurrentEdit()
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(1).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = False
                    'Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
                    'Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
                    'Me.DataSetAperturaCaja1.aperturacaja.Clear()
                    'TextBoxColon.Text = 0.0
                    'TextBox1.Text = 0.0
                End If
            End If
        Else
            Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").CancelCurrentEdit()
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.ToolBar1.Buttons(1).Enabled = True
            Me.ToolBar1.Buttons(2).Enabled = False
            'Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
            'Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
            'Me.DataSetAperturaCaja1.aperturacaja.Clear()
            'TextBoxColon.Text = 0.0
            'TextBox1.Text = 0.0
            Exit Sub
        End If

    End Sub
    Public Sub open_cashdrawer()

        Dim Ubicacion As String = ""
        Dim escapes As String = GetSetting("SeeSoft", "Restaurante", "SinAbrir")
        Dim puerto As String = GetSetting("SeeSoft", "Restaurante", "PuertoCaja")

        If GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes") = "" Then
            SaveSetting("SeeSoft", "Restaurante", "UbicacionEscapes", "c:\escapes.txt")
        Else
            Ubicacion = GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes")
        End If

        If puerto.Equals("") Then
            puerto = "LPT1"
            SaveSetting("SeeSoft", "Restaurante", "PuertoCaja", "LPT1")
        End If

        If escapes.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "SinAbrir", "0")
        Else
            If escapes.Equals("1") Then
                Dim intFileNo As Integer = FreeFile()
                FileOpen(1, Ubicacion, OpenMode.Output)
                PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
                FileClose(1)
                Shell("print /d:" & puerto & " " & Ubicacion, AppWinStyle.NormalFocus)
            End If
        End If

    End Sub
#End Region

#Region "Guardar Apertura"
    Private Sub GuardarApertura()
        Try


            If DataSetAperturaCaja1.Apertura_Total_Tope.Count = 0 Then
                MsgBox("NO se puede guardar sin al menos un detalle Total/Tope ", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If DataSetAperturaCaja1.DenominacionesAperturas.Count = 0 Then
                MsgBox("NO se puede guardar sin al menos un detalle Denominacion ", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If RegistarApertura() Then
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.GroupBox2.Enabled = False
                Me.GroupBox5.Enabled = False
                If (MsgBox("Desea Imprimir el reporte de Apertura ", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes Then
                    Imprimir()
                End If
                Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
                Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
                Me.DataSetAperturaCaja1.aperturacaja.Clear()
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                Me.ToolBar1.Buttons(5).Enabled = True
                TextBoxColon.Text = 0.0
                TextBox1.Text = 0.0

                MsgBox("Apertura de caja realizada Satisfactoriamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function RegistarApertura() As Boolean
        If SqlConnection1.State <> ConnectionState.Open Then SqlConnection1.Open()
        Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction
        Try
            'Apertura
            SqlInsertCommand2.Transaction = Trans
            SqlUpdateCommand2.Transaction = Trans
            SqlUpdateCommand2.Transaction = Trans
            'Denominaciones
            SqlInsertCommand4.Transaction = Trans
            SqlUpdateCommand4.Transaction = Trans
            SqlUpdateCommand4.Transaction = Trans
            'Tope
            SqlInsertCommand3.Transaction = Trans
            SqlUpdateCommand3.Transaction = Trans
            SqlUpdateCommand3.Transaction = Trans

            Me.AdapterAperturaCaja.Update(Me.DataSetAperturaCaja1.aperturacaja)

            Me.AdapterDenominacionesApertura.Update(Me.DataSetAperturaCaja1.DenominacionesAperturas)
            Me.AdapterAperturaTotalTope.Update(Me.DataSetAperturaCaja1.Apertura_Total_Tope)
            Me.DataSetAperturaCaja1.AcceptChanges()

            Trans.Commit()
            Return True
        Catch ex As Exception
            Trans.Rollback()
            MsgBox("Problemas para registar la apertura, intente de nuevo, si el problema persiste comuniqueselo al encargado del sistema", MsgBoxStyle.Critical)
        End Try

    End Function
#End Region

#Region "Nuevo Tope"
    Private Sub smpBtnNuevo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles smpBtnNuevo1.Click
        If smpBtnNuevo1.Text = "Nuevo" Then
            Try
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()
                smpBtnNuevo1.Text = "Cancelar"
                Me.Vt_monto_tope.Enabled = True
                Me.ComboBox3.Enabled = True
                Me.SimpleButton2.Enabled = True
                Me.Vt_monto_tope.Focus()
                Me.GridControl3.Enabled = False
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            Try
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").CancelCurrentEdit()
                smpBtnNuevo1.Text = "Nuevo"
                Me.Vt_monto_tope.Enabled = False
                Me.ComboBox3.Enabled = False
                Me.SimpleButton2.Enabled = False
                Me.GridControl3.Enabled = True
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
#End Region

#Region "Guardar Tope"
    Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
        If ValidarTope() Then
            Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("MonedaNombre") = ComboBox3.Text
            Try
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").CancelCurrentEdit()
                Me.smpBtnNuevo1.Text = "Nuevo"
                Me.smpBtnNuevo1.Enabled = True
                Me.SimpleButton2.Enabled = False
                Me.GridControl3.Enabled = True
                Me.Vt_monto_tope.Enabled = False
                Me.ComboBox3.Enabled = False
            Catch EX As SystemException
                MsgBox(EX.Message)
            End Try
        End If
    End Sub
#End Region

#Region "Imprimir Apertura"
    Private Sub Imprimir()

        If Me.check_PVE.Checked = False Then
            Dim Apertura As New Apertura_Cajas
            Dim visor As New frmVisorReportes
            Dim servidor As String = Me.SqlConnection1.DataSource
            'Apertura.SetDatabaseLogon("sa", "", Me.SqlConnection1.DataSource, Me.SqlConnection1.Database)
            Apertura.SetParameterValue(0, Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").Current("NApertura"))
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Apertura, False, Me.SqlConnection1.ConnectionString)

            'visor.rptViewer.ReportSource = Apertura_Cajas
            visor.rptViewer.Visible = True
            Apertura = Nothing
            visor.ShowDialog()
        Else
            Dim Apertura As New Apertura_CajasPVE
            Dim visor As New frmVisorReportes
            Dim servidor As String = Me.SqlConnection1.DataSource
            'Apertura.SetDatabaseLogon("sa", "", Me.SqlConnection1.DataSource, Me.SqlConnection1.Database)
            Apertura.SetParameterValue(0, Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").Current("NApertura"))
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Apertura, False, Me.SqlConnection1.ConnectionString)

            'visor.rptViewer.ReportSource = Apertura_Cajas
            visor.rptViewer.Visible = True
            Apertura = Nothing
            visor.ShowDialog()
        End If

    End Sub

#End Region

#Region "ValidarTope"
    Function ValidarTope() As Boolean
        Dim Topes() As System.Data.DataRow
        Topes = Me.DataSetAperturaCaja1.Apertura_Total_Tope.Select("CodMoneda =" & Me.Label7.Text)
        ' Si existe el Usuario
        If Topes.Length <> 0 Then
            MsgBox("Ya Fue Ingresado un Tope para esa Moneda", MsgBoxStyle.Information)
            Return False
        Else
            Return True
        End If
    End Function

#End Region

#Region "Nueva Denominacion"
    Private Sub smpBtnNuevo2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles smpBtnNuevo2.Click
        If smpBtnNuevo2.Text = "Nuevo" Then
            Try
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").AddNew()
                smpBtnNuevo2.Text = "Cancelar"
                ComboBox2.Focus()
                GridControl1.Enabled = False
                Me.SimpleButton3.Enabled = True
                ComboBox2.Enabled = True
                ValidText2.Enabled = True
                ComboBox1.Enabled = True
                ValidText1.Enabled = True
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            Try
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").CancelCurrentEdit()
                smpBtnNuevo2.Text = "Nuevo"
                GridControl1.Enabled = True
                Me.SimpleButton3.Enabled = False
                ComboBox2.Enabled = False
                ValidText2.Enabled = False
                ComboBox1.Enabled = False
                ValidText1.Enabled = False
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If

    End Sub
#End Region

#Region "Guardar Deniminacion"
    Private Sub SimpleButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton3.Click
        Dim Cantidad As Double
        Dim Valor As Double
        Dim Total As Double

        Try
            Cantidad = CDbl(ValidText2.Text)
        Catch ex As Exception
            Cantidad = 0
        End Try

        Try
            Valor = CDbl(ValidText1.Text)
        Catch ex As Exception
            Valor = 0
        End Try

        Try
            Total = Cantidad * Valor
            VtMontoTopeCol.Text = Total
            BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").Current("MonedaNombre") = ComboBox2.Text
            BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").Current("Denominacion") = ComboBox1.Text
            BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").EndCurrentEdit()
            BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").AddNew()
            BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").CancelCurrentEdit()
            smpBtnNuevo2.Text = "Nuevo"
            smpBtnNuevo2.Enabled = True
            SimpleButton3.Enabled = False
            GridControl1.Enabled = True
            ComboBox2.Enabled = False
            ValidText2.Enabled = False
            ComboBox1.Enabled = False
            ValidText1.Enabled = False

            AgregaTotalMonto()
            muestraTotal()
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1

            Case 1 : If PMU.Find Then Me.NuevaApertura() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 2 : If PMU.Find Then Me.BuscarAperturas() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Me.GuardarApertura() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Others Then Anular_Apertura() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7
                If MessageBox.Show("�Desea Cerrar el M�dulo de Apertura de caja?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    Me.Close()
                End If
        End Select
    End Sub
#End Region

#Region "Anular Apertura"
    Private Sub Anular_Apertura()
        If MessageBox.Show("�Desea anular la Apertura de caja?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
            Me.RadioButton1.Checked = True
            Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").Current("Estado") = "C"
            Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja").EndCurrentEdit()
            RegistarApertura()
            MsgBox("Apertura Anulada", MsgBoxStyle.Information)
        End If
    End Sub
#End Region

#Region "BuscarApertura"
    Private Sub BuscarAperturas()

        Dim BuscarApertura As New BuscarApertura
        If BuscarApertura.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.DataSetAperturaCaja1.DenominacionesAperturas.Clear()
            Me.DataSetAperturaCaja1.Apertura_Total_Tope.Clear()
            Me.DataSetAperturaCaja1.aperturacaja.Clear()
            CargarApertura(BuscarApertura.Numapertura.Text)
            Me.ValidarApertura(BuscarApertura.Numapertura.Text)
            CargarTope(BuscarApertura.Numapertura.Text)
            CargarDenominacion(BuscarApertura.Numapertura.Text)
            muestraTotal()
            Me.ToolBar1.Buttons(4).Enabled = True
            Me.ToolBar1.Buttons(2).Enabled = False
            'Me.ToolBar1.Buttons(3).Enabled = True
            Me.GridControl2.Enabled = False
            Me.GridControl3.Enabled = False
        End If
    End Sub
#End Region

    Function ValidarApertura(ByVal IdApertura As String) As Boolean
        Try
            Dim dta, dta1, dta2 As New DataTable
            Dim cf, cf1, cf2 As New cFunciones

            'busca el estado de la caja
            cf.Llenar_Tabla_Generico("SELECT     dbo.aperturacaja.Estado, dbo.aperturacaja.anulado" & _
                                    " FROM        dbo.aperturacaja" & _
                                    " WHERE  (dbo.aperturacaja.NApertura = " & IdApertura & ")", dta)
            If dta.Rows.Count = 0 Or dta.Rows.Count = Nothing Then 'si no tiene datos
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Return True
            Else
                If dta.Rows(0).Item("Estado") = "A" And dta.Rows(0).Item("anulado") = 0 Then
                    Me.ToolBarRegistrar.Enabled = True
                    Me.ToolBarEliminar.Enabled = True
                    Return True
                ElseIf dta.Rows(0).Item("Estado") = "M" And dta.Rows(0).Item("anulado") = 0 Then ' si esta arqueada, busca el estado del arqueo
                    cf1.Llenar_Tabla_Generico("SELECT     Id, IdApertura, Anulado " & _
                   "FROM ArqueoCajas " & _
                    " WHERE anulado=0 and    (IdApertura = " & IdApertura & ")", dta1)
                    If dta1.Rows.Count = 0 Or dta1.Rows.Count = Nothing Then ' si no returna datos
                        Me.ToolBarRegistrar.Enabled = True
                        Me.ToolBarEliminar.Enabled = True
                        Return True
                    Else
                        If dta1.Rows(0).Item("anulado") = 0 Then 'si no esta anulado entonces
                            Me.ToolBarEliminar.Enabled = False
                            Me.ToolBarRegistrar.Enabled = False

                            MsgBox("El n�mero de Apertura " & dta1.Rows(0).Item("IdApertura") & " Tiene el arqueo n� " & dta1.Rows(0).Item("Id") & " realizado", MsgBoxStyle.Information, "Atenci�n...")

                        End If
                    End If
                ElseIf dta.Rows(0).Item("Estado") = "C" And dta.Rows(0).Item("anulado") = 0 Then 'si esta tiene cierre, busca el estado del cierre
                    cf2.Llenar_Tabla_Generico("SELECT     NumeroCierre, Apertura, Anulado " & _
                    "FROM CierreCaja " & _
                    "WHERE     Anulado = 0 and (Apertura = " & IdApertura & ")", dta2)
                    If dta2.Rows.Count = 0 Or dta2.Rows.Count = Nothing Then ' si no returna datos
                        Me.ToolBarRegistrar.Enabled = True
                        Me.ToolBarEliminar.Enabled = True
                        Return True
                    Else
                        If dta2.Rows(0).Item("anulado") = 0 Then 'si no esta anulado entonces
                            Me.ToolBarEliminar.Enabled = False
                            Me.ToolBarRegistrar.Enabled = False

                            MsgBox("El n�mero de Apertura " & dta2.Rows(0).Item("Apertura") & " Tiene el cierre n� " & dta2.Rows(0).Item("NumeroCierre") & " realizado", MsgBoxStyle.Information, "Atenci�n...")

                        End If
                    End If
                End If
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

#Region "Cargar Apertura"
    Private Sub CargarApertura(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        Dim napertura As Integer = CInt(Numapertura)
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM aperturacaja" & _
            " where NApertura  = @Id"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Id", SqlDbType.BigInt))
            cmd.Parameters("@Id").Value = napertura 'Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetAperturaCaja1.aperturacaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub
#End Region

#Region "Cargar Denominaciones"

    Private Sub CargarDenominacion(ByVal Numapertura As String)

        Dim cnn As SqlConnection = Nothing

        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM DenominacionesAperturas" & _
            " where NApertura  = @Id"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Id", SqlDbType.BigInt))
            cmd.Parameters("@Id").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetAperturaCaja1.DenominacionesAperturas)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

#End Region

#Region "Cargar Tope "
    Private Sub CargarTope(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing

        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM Apertura_Total_Tope" & _
            " where NApertura  = @Id"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Id", SqlDbType.BigInt))
            cmd.Parameters("@Id").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetAperturaCaja1.Apertura_Total_Tope)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub

#End Region

#Region "Eliminar Tope"
    Private Sub GridControl3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl3.KeyDown
        If e.KeyCode = Keys.Delete Then
            If Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Count > 0 Then
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").RemoveAt(Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position)
            End If
        End If
    End Sub
#End Region

#Region "Eliminar Denominacion"
    Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim MontoEli As Double = 0
            If Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").Count > 0 Then
                MontoEli = Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").Current("Monto_Total")
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").RemoveAt(Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaDenominacionesAperturas").Position)
                AgregaTotalMonto(MontoEli)
                muestraTotal()
            End If
        End If
    End Sub

#End Region

    Private Sub AgregaTotalMonto(Optional ByVal Elim As Double = 0)
        Try


            'Variables que almacenaran los valores para la tabla "Apertura_Total_Tope"
            Dim dblCodigoMoneda As Double = 0
            Dim dblMontoTope As Double = 0
            Dim strMonedaNombre As String = ""

            dblCodigoMoneda = CDbl(Me.ComboBox2.SelectedValue)
            If VtMontoTopeCol.Text = "" Then
                dblMontoTope = "0.00"
            Else
                dblMontoTope = CDbl(VtMontoTopeCol.Text)
            End If

            strMonedaNombre = Me.ComboBox2.Text


            'Evaluo si existen datos en el total 

            If Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Count > 0 Then


                Dim dtrTotalA() As System.Data.DataRow

                dtrTotalA = Me.DataSetAperturaCaja1.Apertura_Total_Tope.Select("CodMoneda = " & dblCodigoMoneda)
                'Evaluo si existen datos ingresados sobre esta moneda ya en el total de la apertura
                If dtrTotalA.Length > 0 Then

                    'si existen datos 
                    Dim i As Integer = 0

                    'Recorro el dataset en busca de los datos a los que se le sumara la nueva cantidad
                    For i = 0 To Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Count - 1

                        Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position = i

                        If Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("CodMoneda") = dblCodigoMoneda Then

                            If Elim > 0 Then
                                Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("Monto_Tope") -= Elim
                            Else
                                Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("Monto_Tope") += CDbl(VtMontoTopeCol.Text)
                            End If
                            Dim dblposicion As Double = Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position
                            Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position += 1
                            Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position -= 1
                            Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Position = dblposicion
                            Exit For

                        End If

                    Next



                Else
                    'si no existen datos agrego estos nuevos 
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()

                    'Asigno los valores a los campos de la tabla correspondiente
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("CodMoneda") = dblCodigoMoneda
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("Monto_Tope") = dblMontoTope
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("MonedaNombre") = strMonedaNombre

                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()
                    Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").CancelCurrentEdit()


                End If


            Else
                'Asigno los valores a las respectivas variables
                'dblCodigoMoneda = CDbl(Me.ComboBox2.SelectedValue)
                'dblMontoTope = CDbl(VtMontoTopeCol.Text)
                'strMonedaNombre = Me.ComboBox2.Text

                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()

                'Asigno los valores a los campos de la tabla correspondiente
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("CodMoneda") = dblCodigoMoneda
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("Monto_Tope") = dblMontoTope
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("MonedaNombre") = strMonedaNombre

                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").EndCurrentEdit()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").AddNew()
                Me.BindingContext(Me.DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").CancelCurrentEdit()

            End If
            ' Me.TextBox1.Text = CDbl(Me.BindingContext(DataSetAperturaCaja1, "aperturacaja.aperturacajaApertura_Total_Tope").Current("Monto_Tope"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al agregar el total de la apertura")
        End Try

    End Sub

    Sub muestraTotal()
        Dim Totalisimo As Double = 0
        Dim totalDolar As Double = 0
        Dim totalColon As Double = 0
        Dim totalEuro As Double = 0
        Try
            For i As Integer = 0 To Me.DataSetAperturaCaja1.DenominacionesAperturas.Count - 1
                If Me.DataSetAperturaCaja1.DenominacionesAperturas(i).CodMoneda = 1 Then
                    Totalisimo += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total
                    totalColon += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total
                ElseIf Me.DataSetAperturaCaja1.DenominacionesAperturas(i).CodMoneda = 2 Then
                    Totalisimo += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total * Me.DataSetAperturaCaja1.Moneda(1).ValorCompra
                    totalDolar += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total
                ElseIf Me.DataSetAperturaCaja1.DenominacionesAperturas(i).CodMoneda = 3 Then
                    Totalisimo += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total * Me.DataSetAperturaCaja1.Moneda(2).ValorCompra
                    totalEuro += Me.DataSetAperturaCaja1.DenominacionesAperturas(i).Monto_Total
                End If

            Next
            Me.TextBoxDolar.Text = Format(Math.Round(totalDolar, 2), "##,##00.00")
            Me.TextBoxColon.Text = Format(Math.Round(totalColon, 2), "##,##00.00")
            Me.TextBox1.Text = Format(Math.Round(Totalisimo, 2), "##,##00.00")
            Me.TextBox2.Text = Format(Math.Round(totalEuro, 2), "##,##00.00")

        Catch ex As Exception
            Me.TextBoxDolar.Text = "0.00"
            Me.TextBoxColon.Text = "0.00"
            Me.TextBox1.Text = "0.00"
        End Try

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            lblAnulado.Visible = True
            Me.ToolBar1.Buttons(3).Enabled = False
        ElseIf Me.RadioButton1.Checked = False Then
            lblAnulado.Visible = False
            Me.ToolBar1.Buttons(3).Enabled = True
        End If
    End Sub
End Class
