Public Class FrmDatos_Cajeros
    Inherits System.Windows.Forms.Form
    Public nombre_cajero As String
    Public cedula_cajero As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtIdentificacion As System.Windows.Forms.TextBox
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents Adapter_Cajeros_disponibles As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Combo_nom_cajero As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataSet_Aper_Caja1 As DataSet_Aper_Caja
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDatos_Cajeros))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Combo_nom_cajero = New System.Windows.Forms.ComboBox
        Me.DataSet_Aper_Caja1 = New SIALIBEB.DataSet_Aper_Caja
        Me.TxtIdentificacion = New System.Windows.Forms.TextBox
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.Adapter_Cajeros_disponibles = New System.Data.SqlClient.SqlDataAdapter
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataSet_Aper_Caja1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.AccessibleDescription = Nothing
        Me.GroupBox1.AccessibleName = Nothing
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.BackgroundImage = Nothing
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Combo_nom_cajero)
        Me.GroupBox1.Controls.Add(Me.TxtIdentificacion)
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AccessibleDescription = Nothing
        Me.Label3.AccessibleName = Nothing
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = Nothing
        Me.Label3.Name = "Label3"
        '
        'Label2
        '
        Me.Label2.AccessibleDescription = Nothing
        Me.Label2.AccessibleName = Nothing
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = Nothing
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        Me.Label1.AccessibleDescription = Nothing
        Me.Label1.AccessibleName = Nothing
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Font = Nothing
        Me.Label1.Name = "Label1"
        '
        'Combo_nom_cajero
        '
        Me.Combo_nom_cajero.AccessibleDescription = Nothing
        Me.Combo_nom_cajero.AccessibleName = Nothing
        resources.ApplyResources(Me.Combo_nom_cajero, "Combo_nom_cajero")
        Me.Combo_nom_cajero.BackgroundImage = Nothing
        Me.Combo_nom_cajero.DataSource = Me.DataSet_Aper_Caja1
        Me.Combo_nom_cajero.DisplayMember = "Vista_Cajeros_Disponibles_Abrir_Caja.Nombre"
        Me.Combo_nom_cajero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_nom_cajero.Font = Nothing
        Me.Combo_nom_cajero.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Combo_nom_cajero.Name = "Combo_nom_cajero"
        Me.Combo_nom_cajero.ValueMember = "Usuarios.Nombre"
        '
        'DataSet_Aper_Caja1
        '
        Me.DataSet_Aper_Caja1.DataSetName = "DataSet_Aper_Caja"
        Me.DataSet_Aper_Caja1.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.DataSet_Aper_Caja1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TxtIdentificacion
        '
        Me.TxtIdentificacion.AccessibleDescription = Nothing
        Me.TxtIdentificacion.AccessibleName = Nothing
        resources.ApplyResources(Me.TxtIdentificacion, "TxtIdentificacion")
        Me.TxtIdentificacion.BackgroundImage = Nothing
        Me.TxtIdentificacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Aper_Caja1, "Vista_Cajeros_Disponibles_Abrir_Caja.Cedula", True))
        Me.TxtIdentificacion.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtIdentificacion.Name = "TxtIdentificacion"
        Me.TxtIdentificacion.ReadOnly = True
        '
        'SimpleButton1
        '
        Me.SimpleButton1.AccessibleDescription = Nothing
        Me.SimpleButton1.AccessibleName = Nothing
        resources.ApplyResources(Me.SimpleButton1, "SimpleButton1")
        Me.SimpleButton1.BackgroundImage = Nothing
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Nombre FROM Vista_Cajeros_Disponibles_Abrir_Caja"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=SERVER;packet size=4096;user id=sa;data source=SERVER;persist secu" & _
            "rity info=False;initial catalog=Hotel"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Vista_Cajeros_Disponibles_Abrir_Caja(Cedula, Nombre) VALUES (@Cedula," & _
            " @Nombre); SELECT Cedula, Nombre FROM Vista_Cajeros_Disponibles_Abrir_Caja"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre")})
        '
        'Adapter_Cajeros_disponibles
        '
        Me.Adapter_Cajeros_disponibles.InsertCommand = Me.SqlInsertCommand1
        Me.Adapter_Cajeros_disponibles.SelectCommand = Me.SqlSelectCommand1
        Me.Adapter_Cajeros_disponibles.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Vista_Cajeros_Disponibles_Abrir_Caja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'FrmDatos_Cajeros
        '
        Me.AccessibleDescription = Nothing
        Me.AccessibleName = Nothing
        resources.ApplyResources(Me, "$this")
        Me.BackgroundImage = Nothing
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = Nothing
        Me.Icon = Nothing
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDatos_Cajeros"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataSet_Aper_Caja1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FrmDatos_Cajeros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
        Me.Adapter_Cajeros_disponibles.Fill(Me.DataSet_Aper_Caja1, "Vista_Cajeros_Disponibles_Abrir_Caja")
        If Me.DataSet_Aper_Caja1.Vista_Cajeros_Disponibles_Abrir_Caja.Count > 0 Then
        Else
            MsgBox("No hay cajeros disponibles, para hacer una apertura", MsgBoxStyle.Information)
            Me.DialogResult = DialogResult.Abort
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        nombre_cajero = Me.Combo_nom_cajero.Text
        cedula_cajero = Me.TxtIdentificacion.Text
        Me.DialogResult = DialogResult.OK
    End Sub
End Class
