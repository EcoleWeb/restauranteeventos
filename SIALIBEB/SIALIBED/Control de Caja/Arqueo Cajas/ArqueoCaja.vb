Imports System.data.SqlClient

Public Class ArqueoCaja
    Inherits Detalle_Cortesia

    Dim TipoCambioDolar, TipoCambioEuro As Double
    Dim PMU As PerfilModulo_Class
    Dim Cedula As String
    Public val_id_ArqueoCaja As Int64
    Public NApertura As Long
    Public nombre As String
    Public statNuevo As Boolean
    Friend WithEvents check_PVE As System.Windows.Forms.CheckBox
    Dim dt_datosEmpresa As New DataTable
#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMonedaNombre1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDenominacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterDenominacion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetArqueo1 As SIALIBEB.DataSetArqueO
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents colNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents AdapterEfectivo As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterTarjetas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterArqueodeCaja As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ArqueoCaja))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Dim ColumnFilterInfo8 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataSetArqueo1 = New SIALIBEB.DataSetArqueo
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMonedaNombre1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDenominacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNumero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.colTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterDenominacion = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterTarjeta = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.AdapterEfectivo = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTarjetas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.AdapterArqueodeCaja = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterApertura = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.check_PVE = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetArqueo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.ImageIndex = 5
        Me.ToolBarEliminar.Text = "Editar"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 3
        Me.ToolBarExcel.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton
        Me.ToolBarExcel.Text = "Anular"
        Me.ToolBarExcel.Visible = True
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 476)
        Me.ToolBar1.Size = New System.Drawing.Size(650, 52)
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(506, 478)
        '
        'TituloModulo
        '
        Me.TituloModulo.Size = New System.Drawing.Size(650, 32)
        Me.TituloModulo.Text = "Arqueo de Caja"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.GridControl1)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.GroupBox1.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(632, 224)
        Me.GroupBox1.TabIndex = 59
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Desgloce Efectivo"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Red
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(224, 160)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 56)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "ANULADO"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label7.Visible = False
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "Denominacion_Cierre"
        Me.GridControl1.DataSource = Me.DataSetArqueo1
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(16, 24)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(608, 192)
        Me.GridControl1.TabIndex = 11
        Me.GridControl1.Text = "GridControl1"
        '
        'DataSetArqueo1
        '
        Me.DataSetArqueo1.DataSetName = "DataSetArqueo"
        Me.DataSetArqueo1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSetArqueo1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMonedaNombre1, Me.colCantidad, Me.colDenominacion, Me.colNumero, Me.colTotal})
        Me.GridView1.GroupPanelText = "Agrupe de acuerdo a una columna si lo desea"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colMonedaNombre1
        '
        Me.colMonedaNombre1.Caption = "Moneda"
        Me.colMonedaNombre1.FieldName = "Moneda"
        Me.colMonedaNombre1.FilterInfo = ColumnFilterInfo1
        Me.colMonedaNombre1.Name = "colMonedaNombre1"
        Me.colMonedaNombre1.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonedaNombre1.VisibleIndex = 0
        Me.colMonedaNombre1.Width = 129
        '
        'colCantidad
        '
        Me.colCantidad.Caption = "Tipo"
        Me.colCantidad.DisplayFormat.FormatString = "#,#0.00"
        Me.colCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCantidad.FieldName = "Tipo"
        Me.colCantidad.FilterInfo = ColumnFilterInfo2
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 1
        Me.colCantidad.Width = 129
        '
        'colDenominacion
        '
        Me.colDenominacion.Caption = "Denominacion"
        Me.colDenominacion.FieldName = "Denominacion"
        Me.colDenominacion.FilterInfo = ColumnFilterInfo3
        Me.colDenominacion.Name = "colDenominacion"
        Me.colDenominacion.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDenominacion.VisibleIndex = 2
        Me.colDenominacion.Width = 110
        '
        'colNumero
        '
        Me.colNumero.Caption = "Cantidad"
        Me.colNumero.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.colNumero.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colNumero.FieldName = "Cantidad"
        Me.colNumero.FilterInfo = ColumnFilterInfo4
        Me.colNumero.Name = "colNumero"
        Me.colNumero.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNumero.VisibleIndex = 3
        Me.colNumero.Width = 107
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'colTotal
        '
        Me.colTotal.Caption = "Total"
        Me.colTotal.FieldName = "Total"
        Me.colTotal.FilterInfo = ColumnFilterInfo5
        Me.colTotal.Name = "colTotal"
        Me.colTotal.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTotal.VisibleIndex = 4
        Me.colTotal.Width = 111
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=JORDAN-PC\PRUEBAS;Initial Catalog=Restaurante;Integrated Security=Tru" & _
            "e"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterDenominacion
        '
        Me.AdapterDenominacion.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterDenominacion.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterDenominacion.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterDenominacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Denominacion_Cierre", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo")})})
        Me.AdapterDenominacion.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Denominacion_Cierre WHERE (Id = @Original_Id) AND (CodMoneda = @Origi" & _
            "nal_CodMoneda) AND (Denominacion = @Original_Denominacion) AND (Tipo = @Original" & _
            "_Tipo)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Denominacion_Cierre(CodMoneda, Denominacion, Tipo) VALUES (@CodMoneda" & _
            ", @Denominacion, @Tipo); SELECT Id, CodMoneda, Denominacion, Tipo FROM Denominac" & _
            "ion_Cierre WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Int, 4, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 20, "Tipo")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, CodMoneda, Denominacion, Tipo FROM Denominacion_Cierre"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Int, 4, "Denominacion"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 20, "Tipo"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GridControl2)
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.GroupBox2.Location = New System.Drawing.Point(8, 272)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(328, 200)
        Me.GroupBox2.TabIndex = 60
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tarjetas de Cr�dito"
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "TipoTarjeta"
        Me.GridControl2.DataSource = Me.DataSetArqueo1
        '
        '
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(8, 16)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(312, 176)
        Me.GridControl2.TabIndex = 12
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn5, Me.GridColumn2})
        Me.GridView2.GroupPanelText = "Agrupe de acuerdo a una columna si lo desea"
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsView.ShowFilterPanel = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Tarjeta"
        Me.GridColumn1.FieldName = "Nombre"
        Me.GridColumn1.FilterInfo = ColumnFilterInfo6
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 134
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Total"
        Me.GridColumn5.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "Total"
        Me.GridColumn5.FilterInfo = ColumnFilterInfo7
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Moneda"
        Me.GridColumn2.FieldName = "Monedas"
        Me.GridColumn2.FilterInfo = ColumnFilterInfo8
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 100
        '
        'AdapterTarjeta
        '
        Me.AdapterTarjeta.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterTarjeta.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterTarjeta.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TipoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("CuentaContable", "CuentaContable"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta")})})
        Me.AdapterTarjeta.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 75, "CuentaContable"), New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta")})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Id, Nombre, Moneda, Observaciones, CuentaContable, NombreCuenta FROM TipoT" & _
            "arjeta"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.Int, 4, "Moneda"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 75, "CuentaContable"), New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextEdit7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.TextEdit6)
        Me.GroupBox3.Controls.Add(Me.TextEdit5)
        Me.GroupBox3.Controls.Add(Me.TextEdit4)
        Me.GroupBox3.Controls.Add(Me.TextEdit3)
        Me.GroupBox3.Controls.Add(Me.TextEdit2)
        Me.GroupBox3.Controls.Add(Me.TextEdit1)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.GroupBox3.Location = New System.Drawing.Point(344, 272)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(296, 200)
        Me.GroupBox3.TabIndex = 61
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Total  General"
        '
        'TextEdit7
        '
        Me.TextEdit7.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.EfectivoEuros", True))
        Me.TextEdit7.EditValue = "0.00"
        Me.TextEdit7.Location = New System.Drawing.Point(120, 64)
        Me.TextEdit7.Name = "TextEdit7"
        '
        '
        '
        Me.TextEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.ReadOnly = True
        Me.TextEdit7.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit7.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit7.TabIndex = 46
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "Euros"
        '
        'TextEdit6
        '
        Me.TextEdit6.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.Total", True))
        Me.TextEdit6.EditValue = "0.00"
        Me.TextEdit6.Location = New System.Drawing.Point(120, 168)
        Me.TextEdit6.Name = "TextEdit6"
        '
        '
        '
        Me.TextEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit6.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit6.Properties.ReadOnly = True
        Me.TextEdit6.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit6.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit6.TabIndex = 44
        '
        'TextEdit5
        '
        Me.TextEdit5.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.TravelCheck", True))
        Me.TextEdit5.EditValue = "0.00"
        Me.TextEdit5.Location = New System.Drawing.Point(120, 136)
        Me.TextEdit5.Name = "TextEdit5"
        '
        '
        '
        Me.TextEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit5.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit5.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit5.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit5.TabIndex = 43
        '
        'TextEdit4
        '
        Me.TextEdit4.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.TarjetaDolares", True))
        Me.TextEdit4.EditValue = "0.00"
        Me.TextEdit4.Location = New System.Drawing.Point(120, 112)
        Me.TextEdit4.Name = "TextEdit4"
        '
        '
        '
        Me.TextEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit4.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit4.Properties.ReadOnly = True
        Me.TextEdit4.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit4.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit4.TabIndex = 42
        '
        'TextEdit3
        '
        Me.TextEdit3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.TarjetaColones", True))
        Me.TextEdit3.EditValue = "0.00"
        Me.TextEdit3.Location = New System.Drawing.Point(120, 88)
        Me.TextEdit3.Name = "TextEdit3"
        '
        '
        '
        Me.TextEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit3.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit3.Properties.ReadOnly = True
        Me.TextEdit3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit3.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit3.TabIndex = 41
        '
        'TextEdit2
        '
        Me.TextEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.EfectivoDolares", True))
        Me.TextEdit2.EditValue = "0.00"
        Me.TextEdit2.Location = New System.Drawing.Point(120, 40)
        Me.TextEdit2.Name = "TextEdit2"
        '
        '
        '
        Me.TextEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit2.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit2.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit2.TabIndex = 40
        '
        'TextEdit1
        '
        Me.TextEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetArqueo1, "ArqueoCajas.EfectivoColones", True))
        Me.TextEdit1.EditValue = "0.00"
        Me.TextEdit1.Location = New System.Drawing.Point(120, 16)
        Me.TextEdit1.Name = "TextEdit1"
        '
        '
        '
        Me.TextEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit1.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit1.Size = New System.Drawing.Size(160, 19)
        Me.TextEdit1.TabIndex = 39
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(8, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tarjeta Dolares"
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Total Sistema"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label4.Location = New System.Drawing.Point(8, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Travel Ck"
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Tarjeta Colones"
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Location = New System.Drawing.Point(8, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Dolares"
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Colones"
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(368, 496)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 146
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(448, 512)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(200, 13)
        Me.txtNombreUsuario.TabIndex = 148
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.ForeColor = System.Drawing.Color.Blue
        Me.TextBox6.Location = New System.Drawing.Point(448, 496)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox6.Size = New System.Drawing.Size(200, 13)
        Me.TextBox6.TabIndex = 0
        '
        'AdapterEfectivo
        '
        Me.AdapterEfectivo.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterEfectivo.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterEfectivo.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterEfectivo.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoEfectivo", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_Arqueo", "Id_Arqueo"), New System.Data.Common.DataColumnMapping("Id_Denominacion", "Id_Denominacion"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad")})})
        Me.AdapterEfectivo.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = resources.GetString("SqlDeleteCommand4.CommandText")
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Denominacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = resources.GetString("SqlInsertCommand5.CommandText")
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Denominacion", System.Data.SqlDbType.BigInt, 8, "Id_Denominacion"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad")})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Id, Id_Arqueo, Id_Denominacion, Monto, Cantidad FROM ArqueoEfectivo"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Denominacion", System.Data.SqlDbType.BigInt, 8, "Id_Denominacion"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Denominacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Denominacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'AdapterTarjetas
        '
        Me.AdapterTarjetas.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterTarjetas.InsertCommand = Me.SqlInsertCommand6
        Me.AdapterTarjetas.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterTarjetas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_Arqueo", "Id_Arqueo"), New System.Data.Common.DataColumnMapping("Id_Tarjeta", "Id_Tarjeta"), New System.Data.Common.DataColumnMapping("Monto", "Monto")})})
        Me.AdapterTarjetas.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM ArqueoTarjeta WHERE (Id = @Original_Id) AND (Id_Arqueo = @Original_Id" & _
            "_Arqueo) AND (Id_Tarjeta = @Original_Id_Tarjeta) AND (Monto = @Original_Monto)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO ArqueoTarjeta(Id_Arqueo, Id_Tarjeta, Monto) VALUES (@Id_Arqueo, @Id_T" & _
            "arjeta, @Monto); SELECT Id, Id_Arqueo, Id_Tarjeta, Monto FROM ArqueoTarjeta WHER" & _
            "E (Id = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Tarjeta", System.Data.SqlDbType.Int, 4, "Id_Tarjeta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto")})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT Id, Id_Arqueo, Id_Tarjeta, Monto FROM ArqueoTarjeta"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = resources.GetString("SqlUpdateCommand5.CommandText")
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Arqueo", System.Data.SqlDbType.BigInt, 8, "Id_Arqueo"), New System.Data.SqlClient.SqlParameter("@Id_Tarjeta", System.Data.SqlDbType.Int, 4, "Id_Tarjeta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Arqueo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Arqueo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tarjeta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'AdapterArqueodeCaja
        '
        Me.AdapterArqueodeCaja.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterArqueodeCaja.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterArqueodeCaja.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterArqueodeCaja.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoCajas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("EfectivoColones", "EfectivoColones"), New System.Data.Common.DataColumnMapping("EfectivoDolares", "EfectivoDolares"), New System.Data.Common.DataColumnMapping("TarjetaColones", "TarjetaColones"), New System.Data.Common.DataColumnMapping("TarjetaDolares", "TarjetaDolares"), New System.Data.Common.DataColumnMapping("TravelCheck", "TravelCheck"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("IdApertura", "IdApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Cajero", "Cajero"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("EfectivoEuros", "EfectivoEuros")})})
        Me.AdapterArqueodeCaja.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM [ArqueoCajas] WHERE (([Id] = @Original_Id))"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = resources.GetString("SqlInsertCommand4.CommandText")
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares"), New System.Data.SqlClient.SqlParameter("@TravelCheck", System.Data.SqlDbType.Float, 0, "TravelCheck"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@IdApertura", System.Data.SqlDbType.Int, 0, "IdApertura"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cajero", System.Data.SqlDbType.VarChar, 0, "Cajero"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@EfectivoEuros", System.Data.SqlDbType.Float, 0, "EfectivoEuros")})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT        Id, EfectivoColones, EfectivoDolares, TarjetaColones, TarjetaDolare" & _
            "s, TravelCheck, Total, IdApertura, Fecha, Cajero, Anulado, EfectivoEuros" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM  " & _
            "          ArqueoCajas"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 0, "EfectivoColones"), New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 0, "EfectivoDolares"), New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 0, "TarjetaColones"), New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 0, "TarjetaDolares"), New System.Data.SqlClient.SqlParameter("@TravelCheck", System.Data.SqlDbType.Float, 0, "TravelCheck"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@IdApertura", System.Data.SqlDbType.Int, 0, "IdApertura"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cajero", System.Data.SqlDbType.VarChar, 0, "Cajero"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@EfectivoEuros", System.Data.SqlDbType.Float, 0, "EfectivoEuros"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'AdapterApertura
        '
        Me.AdapterApertura.DeleteCommand = Me.SqlDeleteCommand6
        Me.AdapterApertura.InsertCommand = Me.SqlInsertCommand7
        Me.AdapterApertura.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.AdapterApertura.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = resources.GetString("SqlDeleteCommand6.CommandText")
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = resources.GetString("SqlInsertCommand7.CommandText")
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula")})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
            "turacaja"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = resources.GetString("SqlUpdateCommand6.CommandText")
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"), New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura")})
        '
        'check_PVE
        '
        Me.check_PVE.AutoEllipsis = True
        Me.check_PVE.BackColor = System.Drawing.Color.Transparent
        Me.check_PVE.Location = New System.Drawing.Point(592, 0)
        Me.check_PVE.Name = "check_PVE"
        Me.check_PVE.Size = New System.Drawing.Size(58, 25)
        Me.check_PVE.TabIndex = 149
        Me.check_PVE.Text = "PVE"
        Me.check_PVE.UseVisualStyleBackColor = False
        '
        'ArqueoCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(650, 528)
        Me.Controls.Add(Me.check_PVE)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ArqueoCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arqueo de Caja"
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.TextBox6, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.check_PVE, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetArqueo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub ArqueoCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conectadobd As New SqlClient.SqlConnection
        Dim cConexion As New ConexionR

        Try
            Me.check_PVE.Checked = GetSetting("SeeSoft", "Restaurante", "PVE")
        Catch ex As Exception
            SaveSetting("SeeSoft", "Restaurante", "PVE", 0)
        End Try

        Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "CONEXION")
        Me.AdapterMoneda.Fill(Me.DataSetArqueo1.Moneda)
        Me.AdapterDenominacion.Fill(Me.DataSetArqueo1.Denominacion_Cierre)
        Me.AdapterTarjeta.Fill(Me.DataSetArqueo1.TipoTarjeta)
        cFunciones.Llenar_Tabla_Generico("Select Empresa,Cedula From configuraciones", dt_datosEmpresa, GetSetting("SeeSoft", "Hotel", "Conexion"))
        ValoresDefecto()
        Me.Inhabilitar()

        '---------------------------------------------------------------
        'VERIFICA SI PIDE O NO EL USUARIO
      
        If gloNoClave Then
            Loggin_Usuario()
        Else
            Me.TextBox6.Focus()
        End If

        '---------------------------------------------------------------
    End Sub

    Private Sub ValoresDefecto()
        Dim i, j As Integer
        Me.TextEdit1.EditValue = 0
        Me.TextEdit2.EditValue = 0
        Me.TextEdit3.EditValue = 0
        Me.TextEdit4.EditValue = 0
        Me.TextEdit5.EditValue = 0
        Me.TextEdit6.EditValue = 0
        TextEdit7.EditValue = 0
        For i = 0 To Me.DataSetArqueo1.Denominacion_Cierre.Rows.Count - 1
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Total") = 0
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Cantidad") = 0
            For j = 0 To Me.DataSetArqueo1.Moneda.Rows.Count - 1
                If Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("CodMoneda") = Me.DataSetArqueo1.Moneda.Rows(j).Item("CodMoneda") Then
                    Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Moneda") = Me.DataSetArqueo1.Moneda.Rows(j).Item("MonedaNombre")
                End If
            Next
        Next
        For i = 0 To Me.DataSetArqueo1.TipoTarjeta.Rows.Count - 1
            Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Total") = 0

            For j = 0 To Me.DataSetArqueo1.Moneda.Rows.Count - 1
                If Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Moneda") = Me.DataSetArqueo1.Moneda.Rows(j).Item("CodMoneda") Then
                    Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Monedas") = Me.DataSetArqueo1.Moneda.Rows(j).Item("MonedaNombre")
                End If
                If Me.DataSetArqueo1.Moneda.Rows(j).Item("Simbolo") = "$" Then
                    TipoCambioDolar = Me.DataSetArqueo1.Moneda.Rows(j).Item("ValorCompra")
                End If
                If Me.DataSetArqueo1.Moneda.Rows(j).Item("Simbolo") = "�" Then
                    TipoCambioEuro = Me.DataSetArqueo1.Moneda.Rows(j).Item("ValorCompra")
                End If
            Next
        Next

        DataSetArqueo1.ArqueoCajas.EfectivoEurosColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.EfectivoColonesColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.EfectivoDolaresColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.TarjetaColonesColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.TarjetaDolaresColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.TravelCheckColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.TotalColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.IdAperturaColumn.DefaultValue = "0"
        DataSetArqueo1.ArqueoCajas.FechaColumn.DefaultValue = Now.Date
        DataSetArqueo1.ArqueoCajas.IdAperturaColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoCajas.CajeroColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoCajas.AnuladoColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoEfectivo.Id_ArqueoColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoEfectivo.Id_DenominacionColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoEfectivo.MontoColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoEfectivo.CantidadColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoTarjeta.Id_ArqueoColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoTarjeta.Id_TarjetaColumn.DefaultValue = 0
        DataSetArqueo1.ArqueoTarjeta.MontoColumn.DefaultValue = 0

    End Sub


    Private Sub Recalcular_Fila(ByVal x As Integer)
        Dim Denominacion, Cantidad As Double
        'If x <> 0 Then
        '    x = x - 1
        'End If
        If Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("CodMoneda") = 1 Then
            Denominacion = Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Denominacion")
            Cantidad = Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Cantidad")
            'Total = (Denominacion * Cantidad) * TipoCambioDolar
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Total") = Denominacion * Cantidad
        Else
            Denominacion = Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Denominacion")
            Cantidad = Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Cantidad")
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(x).Item("Total") = Denominacion * Cantidad
        End If
        Me.CalcularTotales()
    End Sub


    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Me.Recalcular_Fila(Me.BindingContext(Me.DataSetArqueo1, "Denominacion_Cierre").Position())
    End Sub


    Private Sub GridView2_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView2.CellValueChanged
        Me.Recalcular(0)
    End Sub


    Private Sub CalcularTotales()
        Dim i As Integer
        Me.TextEdit1.EditValue = 0
        Me.TextEdit2.EditValue = 0
        For i = 0 To Me.DataSetArqueo1.Denominacion_Cierre.Rows.Count - 1


            If Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("CodMoneda") = 1 Then
                Me.TextEdit1.EditValue += Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Total")
            ElseIf Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("CodMoneda") = 2 Then
                Me.TextEdit2.EditValue += Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Total")
            ElseIf Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("CodMoneda") = 3 Then
                TextEdit7.EditValue += Me.DataSetArqueo1.Denominacion_Cierre.Rows(i).Item("Total")
            End If
        Next

        TotalGeneral()

    End Sub


    Private Sub Recalcular(ByVal x As Integer)
        Dim i As Double
        Me.TextEdit3.EditValue = 0
        Me.TextEdit4.EditValue = 0
        For i = 0 To Me.DataSetArqueo1.TipoTarjeta.Rows.Count - 1


            If Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Moneda") = 1 Then
                Me.TextEdit3.EditValue += Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Total")
            Else
                Me.TextEdit4.EditValue += Me.DataSetArqueo1.TipoTarjeta.Rows(i).Item("Total")
            End If
        Next
        TotalGeneral()
    End Sub

    Private Sub TotalGeneral()
        If IsDBNull(Me.TextEdit2.EditValue) Or IsDBNull(Me.TextEdit4.EditValue) Or IsDBNull(TextEdit7.EditValue) Or IsDBNull(TextEdit5.EditValue) Then
            Exit Sub
        End If
        Dim EfectivoDolares, TarjetaDolares, CK, Euros As Double
        EfectivoDolares = Me.TextEdit2.EditValue * TipoCambioDolar
        TarjetaDolares = Me.TextEdit4.EditValue * TipoCambioDolar
        Euros = TextEdit7.EditValue * TipoCambioEuro
        CK = Me.TextEdit5.EditValue * TipoCambioDolar
        Me.TextEdit6.EditValue = EfectivoDolares + TarjetaDolares + CK + TextEdit1.EditValue + TextEdit3.EditValue + Euros
    End Sub

    Private Sub TextEdit5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextEdit5.EditValueChanged
        Me.TotalGeneral()
    End Sub

    Sub Habilitar()
        GroupBox1.Enabled = True
        GroupBox2.Enabled = True
        GroupBox3.Enabled = True
    End Sub

    Sub Inhabilitar()
        GroupBox1.Enabled = False
        GroupBox2.Enabled = False
        GroupBox3.Enabled = False
    End Sub

#Region "Validar Usuario"
    Private Sub TextBox6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox6.KeyDown
        Dim cConexion As New Conexion
        Dim rs As SqlDataReader
        If e.KeyCode = Keys.Enter Then
            If TextBox6.Text <> "" Then
                rs = cConexion.GetRecorset(cConexion.Conectar("Restaurante"), "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & TextBox6.Text & "'")
                If rs.HasRows = False Then
                    MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                    TextBox6.Focus()
                End If
                While rs.Read
                    Try
                        PMU = VSM(rs("Cedula"), Me.Name) 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub


5:                      'Me.BindingContext(Me.AjusteCxC1, "ajustesccobrar").AddNew()
                        txtNombreUsuario.Text = rs("Nombre")
                        Cedula = rs("Cedula")
                        nombre = rs("Nombre")

                        If Not Me.Buscar_Apertura(Cedula) Then
                            'Me.TextBox6.Focus()
                            'Me.TextBox6.Text = ""
                            'Me.txtNombreUsuario.Text = ""
                            Me.ToolBarNuevo.Enabled = False
                            Me.ToolBarRegistrar.Enabled = False
                           Me.ToolBarBuscar.Enabled = True

                        Else
                            Me.ToolBarNuevo.Enabled = True
                            Me.ToolBarRegistrar.Enabled = False
                            Me.ToolBarBuscar.Enabled = True
                        End If
                        TextBox6.Enabled = False ' se inabilita el campo de la contrase�a

                    Catch ex As SystemException
                        MsgBox(ex.Message)
                    End Try
                End While
                rs.Close()
                cConexion.DesConectar(cConexion.sQlconexion)
            Else
                MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                TextBox6.Focus()
            End If
        End If
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If User_Log.Cedula <> "" Then
                Cedula = User_Log.Cedula
                PMU = VSM(Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
                If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

5:              'Me.BindingContext(Me.AjusteCxC1, "ajustesccobrar").AddNew()
                txtNombreUsuario.Text = User_Log.Nombre
                nombre = User_Log.Nombre

                If Not Me.Buscar_Apertura(Cedula) Then
                    'Me.TextBox6.Focus()
                    'Me.TextBox6.Text = ""
                    'Me.txtNombreUsuario.Text = ""
                    Me.ToolBarNuevo.Enabled = False
                    Me.ToolBarRegistrar.Enabled = False
                    Me.ToolBarBuscar.Enabled = True

                Else
                    Me.ToolBarNuevo.Enabled = True
                    Me.ToolBarRegistrar.Enabled = False
                    Me.ToolBarBuscar.Enabled = True
                End If
                TextBox6.Enabled = False ' se inabilita el campo de la contrase�a
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
                Me.Enabled = True
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

    Function Buscar_Apertura(ByVal usuario As String) As Boolean
        Try
            Dim func As New cFunciones
            Dim i As Integer
            func.Llenar_Tabla_Generico("SELECT * FROM AperturaCaja WHERE (Anulado = 0) AND (Estado = 'A') AND (Cedula = '" & usuario & "')", Me.DataSetArqueo1.aperturacaja)
            'i = Me.AdapterApertura.Fill(Me.DataSetArqueo1, "aperturacaja")

            i = Me.DataSetArqueo1.aperturacaja.Count
            Select Case i
                Case 1
                    NApertura = Me.DataSetArqueo1.aperturacaja.Rows(0).Item("NApertura")
                    Me.txtNombreUsuario.Text = Me.DataSetArqueo1.aperturacaja.Rows(0).Item("Nombre")
                    statNuevo = True
                    Return True

                Case 0
                    MsgBox(Me.nombre & " " & "No tiene una apertura de caja abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    statNuevo = False
                    Return False

                Case Else
                    MsgBox(Me.nombre & " " & "tiene mas de una abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    statNuevo = False
                    Return False

            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Me.Nuevo()
            Case 2 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Update Then Me.Editar() Else MsgBox("No tiene permiso para editar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : Me.Close()
        End Select
    End Sub
    Function Nuevo()
        Me.Loggin_Usuario()

        Label7.Visible = False
        For i As Integer = 0 To Me.DataSetArqueo1.Denominacion_Cierre.Count - 1
            With Me.DataSetArqueo1.Denominacion_Cierre(i)
                .Cantidad = 0
                .Total = 0
                .EndEdit()
            End With
        Next

        For i As Integer = 0 To Me.DataSetArqueo1.TipoTarjeta.Count - 1
            With Me.DataSetArqueo1.TipoTarjeta(i)
                .Total = 0
                .EndEdit()

            End With
        Next


        Me.ToolBarBuscar.Enabled = True
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarImprimir.Enabled = False

        If Me.ToolBarNuevo.Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            Try 'inicia la edicion
                Me.DataSetArqueo1.ArqueoEfectivo.Clear()
                Me.DataSetArqueo1.ArqueoTarjeta.Clear()
                Me.DataSetArqueo1.ArqueoCajas.Clear()

                open_cashdrawer()
                Me.BindingContext(Me.DataSetArqueo1.ArqueoCajas).CancelCurrentEdit()
                Me.BindingContext(Me.DataSetArqueo1.ArqueoEfectivo).CancelCurrentEdit()
                Me.BindingContext(Me.DataSetArqueo1.ArqueoTarjeta).CancelCurrentEdit()
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").AddNew()
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoEfectivo").AddNew()
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoTarjeta").AddNew()
                Me.ToolBarNuevo.Text = "Cancelar"
                Me.ToolBarNuevo.ImageIndex = 8

                ' Me.GridControl1.Enabled = False

                'Me.ToolBarEditar.Enabled = False
                'Me.ToolBarBuscar.Enabled = False
                'Me.ToolBarEliminar.Enabled = False

                If statNuevo = True Then
                    Me.ToolBarRegistrar.Enabled = True
                    Me.ToolBarImprimir.Enabled = True
                Else
                    Me.ToolBarRegistrar.Enabled = False
                    'Me.ToolBarImprimir.Enabled = False
                End If


                Habilitar()


            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                BindingContext(DataSetArqueo1.ArqueoEfectivo).CancelCurrentEdit()
                BindingContext(DataSetArqueo1.ArqueoTarjeta).CancelCurrentEdit()
                BindingContext(DataSetArqueo1.ArqueoCajas).CancelCurrentEdit()

                ToolBarNuevo.Text = "Nuevo"
                ToolBarNuevo.ImageIndex = 0
                ToolBarRegistrar.Enabled = False

                Inhabilitar()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Function
    Public Sub open_cashdrawer()

        Dim Ubicacion As String = ""
        Dim escapes As String = GetSetting("SeeSoft", "Restaurante", "SinAbrir")
        Dim puerto As String = GetSetting("SeeSoft", "Restaurante", "PuertoCaja")

        If GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes") = "" Then
            SaveSetting("SeeSoft", "Restaurante", "UbicacionEscapes", "c:\escapes.txt")
        Else
            Ubicacion = GetSetting("SeeSoft", "Restaurante", "UbicacionEscapes")
        End If

        If puerto.Equals("") Then
            puerto = "LPT1"
            SaveSetting("SeeSoft", "Restaurante", "PuertoCaja", "LPT1")
        End If

        If escapes.Equals("") Then
            SaveSetting("SeeSoft", "Restaurante", "SinAbrir", "0")
        Else
            If escapes.Equals("1") Then
                Dim intFileNo As Integer = FreeFile()
                FileOpen(1, Ubicacion, OpenMode.Output)
                PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
                FileClose(1)
                Shell("print /d:" & puerto & " " & Ubicacion, AppWinStyle.NormalFocus)
            End If
        End If

    End Sub
    Function Buscar()
        Try
            Dim cFunciones As New cFunciones
            Dim Id_ArqueoCaja As String = cFunciones.Buscar_X_Descripcion_Fecha("select top 200 cast(Id as varchar) as Arqueo,IdApertura, Cajero,Fecha from ArqueoCajas Order by id DESC", "Cajero", "Fecha", "Arqueo Caja ....")
            val_id_ArqueoCaja = Id_ArqueoCaja
            If Id_ArqueoCaja <> Nothing Then
                Cargar(Id_ArqueoCaja)
                If Me.ToolBarNuevo.Text = "Cancelar" Then 'cambia el estado del boton del toolbar
                    Me.ToolBarNuevo.Text = "Nuevo"
                    Me.ToolBarNuevo.ImageIndex = 0
                End If
                ToolBarEliminar.Enabled = True
                'ToolBarExcel.Enabled = True
                ToolBarImprimir.Enabled = True
                If DataSetArqueo1.ArqueoCajas.Rows.Count > 0 Then
                    If BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Anulado") = True Then
                        Label7.Visible = True
                        Me.ToolBarExcel.Enabled = False 'es de anular
                        Me.ToolBarEliminar.Enabled = False ' este es de editar
                        Me.ToolBarNuevo.Enabled = True
                    Else
                        Label7.Visible = False
                        Me.ToolBarEliminar.Enabled = True 'editar
                        ToolBarExcel.Enabled = True ' este es de eliminar
                    End If
                    Me.Habilitar()

                    ValidarApertura(Id_ArqueoCaja) 'valida si no tiene cierre
                    Me.ToolBarNuevo.Enabled = True
                End If
            Else
                MsgBox("El n�mero de Arqueo no puede ser ninguno", MsgBoxStyle.Question, "Atenci�n...")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Function

    Function ValidarApertura(ByVal IdArqueo As String) As Boolean
        Try
            Dim IdApertura As Int64
            Dim dta, dta1, dta2 As New DataTable
            Dim cf, cf1, cf2 As New cFunciones
            cf.Llenar_Tabla_Generico("SELECT     IdApertura, anulado " & _
            "FROM ArqueoCajas " & _
            "WHERE     (Id = " & IdArqueo & ")", dta)
            If dta.Rows.Count = 0 Or dta.Rows.Count = Nothing Then 'si no tiene datos
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarExcel.Enabled = True
                Return True
            ElseIf dta.Rows(0).Item("anulado") = 0 Then 'si no es nula entonces busca 
                'el estado de la caja y el estado de anulacion
                cf1.Llenar_Tabla_Generico("SELECT     dbo.aperturacaja.Estado, dbo.aperturacaja.anulado" & _
                                    " FROM        dbo.aperturacaja" & _
                                    " WHERE  (dbo.aperturacaja.NApertura = " & dta.Rows(0).Item("idapertura") & ")", dta1)
                If dta1.Rows.Count = 0 Or dta1.Rows.Count = Nothing Then 'si no tiene datos
                    Me.ToolBarRegistrar.Enabled = True
                    Me.ToolBarEliminar.Enabled = True
                    Me.ToolBarExcel.Enabled = True
                    Return True
                Else
                    If dta1.Rows(0).Item("Estado") = "C" And dta1.Rows(0).Item("anulado") = 0 Then 'si tiene datos

                        cf2.Llenar_Tabla_Generico("SELECT     NumeroCierre, Apertura, Anulado " & _
                    "FROM CierreCaja " & _
                    "WHERE     Anulado = 0 and (Apertura = " & dta.Rows(0).Item("idapertura") & ")", dta2)
                        If dta2.Rows.Count = 0 Or dta2.Rows.Count = Nothing Then ' si no returna datos
                            Me.ToolBarExcel.Enabled = True
                            Me.ToolBarRegistrar.Enabled = True
                            Me.ToolBarEliminar.Enabled = True
                            Return True
                        Else
                            If dta2.Rows(0).Item("anulado") = 0 Then 'si no esta anulado entonces
                                Me.ToolBarExcel.Enabled = False
                                Me.ToolBarRegistrar.Enabled = False
                                Me.ToolBarEliminar.Enabled = False

                                MsgBox("El n�mero de Apertura " & dta2.Rows(0).Item("Apertura") & " Tiene el cierre n� " & dta2.Rows(0).Item("NumeroCierre") & " realizado", MsgBoxStyle.Information, "Atenci�n...")

                            End If
                        End If
                    End If
                    End If
                Return False
                End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Sub Cargar(ByVal IdArqueo As String)
        Dim cFunciones As New cFunciones
        DataSetArqueo1.ArqueoEfectivo.Clear()
        DataSetArqueo1.ArqueoTarjeta.Clear()
        DataSetArqueo1.ArqueoCaja.Clear()
        cFunciones.Llenar_Tabla_Generico("Select * from ArqueoCajas Where Id = " & IdArqueo, Me.DataSetArqueo1.ArqueoCajas)
        cFunciones.Llenar_Tabla_Generico("Select * from ArqueoTarjeta Where Id_Arqueo = " & IdArqueo, Me.DataSetArqueo1.ArqueoTarjeta)
        cFunciones.Llenar_Tabla_Generico("Select * from ArqueoEfectivo Where Id_Arqueo = " & IdArqueo, Me.DataSetArqueo1.ArqueoEfectivo)
        Cargando()
        'si el contrato trabaja con las temporadas del hotel

    End Sub
    Function Cargando()
        Dim I As Integer
        For I = 0 To Me.DataSetArqueo1.Denominacion_Cierre.Count - 1
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(I).Item("Total") = Me.DataSetArqueo1.ArqueoEfectivo.Rows(I).Item("Monto")
            Me.DataSetArqueo1.Denominacion_Cierre.Rows(I).Item("Cantidad") = Me.DataSetArqueo1.ArqueoEfectivo.Rows(I).Item("Cantidad")
        Next
        For I = 0 To Me.DataSetArqueo1.TipoTarjeta.Count - 1
            Me.DataSetArqueo1.TipoTarjeta.Rows(I).Item("Total") = Me.DataSetArqueo1.ArqueoTarjeta.Rows(I).Item("Monto")
        Next
        TextEdit7.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("EfectivoEuros")
        TextEdit1.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("EfectivoColones")
        TextEdit2.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("EfectivoDolares")
        TextEdit3.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("TarjetaColones")
        TextEdit4.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("TarjetaDolares")
        TextEdit5.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("TravelCheck")
        TextEdit6.EditValue = Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("Total")
    End Function



    Sub evaluarArqueoTarjetas()
        Dim i As Integer = 0
        Dim d As Boolean = False
        For i = 0 To Me.DataSetArqueo1.TipoTarjeta.Count - 1
            If Me.DataSetArqueo1.TipoTarjeta(i).Total > Me.DataSetArqueo1.ArqueoTarjeta(i).Monto Then
                d = True
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").Position = i
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").Current("Monto") = Me.DataSetArqueo1.TipoTarjeta(i).Total
                Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").EndCurrentEdit()
            End If
        Next
        If d Then
            MsgBox("No se registro correctamente los montos de las tarjetas")
        End If
    End Sub
    Function Registrar()
        Dim numDepCol As String
        Dim numDepDol As String

        Dim resp As Integer
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Dim myCommand1 As SqlCommand = SqlConnection1.CreateCommand()
        Dim myCommandArqueoTarjeta As SqlCommand = SqlConnection1.CreateCommand()
        Dim myCommandArqueoEfectivo As SqlCommand = SqlConnection1.CreateCommand()

        If Validar() Then
            resp = MessageBox.Show("�Deseas Guardar los cambios?", "Restaurante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            If resp = 6 Then
                Try

                    If ToolBarRegistrar.Text = "Actualizar" Then

                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Anulado") = 0
                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("EfectivoEuros") = TextEdit7.EditValue
                        BindingContext(DataSetArqueo1, "ArqueoCajas").EndCurrentEdit()

                        CargarDatos()

                        If Me.dt_datosEmpresa.Rows.Count > 0 Then
                            If dt_datosEmpresa.Rows(0).Item("Cedula").Equals("3-101-374928-30") And (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0 Or Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0) Then

                                Dim frmDatosNumDep As New FormNumDepositos
                                frmDatosNumDep.TextBoxNumDepColon.Enabled = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0)
                                frmDatosNumDep.TextBoxNumDepDolar.Enabled = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0)

                                If frmDatosNumDep.ShowDialog() = Windows.Forms.DialogResult.OK Then
                                    numDepCol = frmDatosNumDep.TextBoxNumDepColon.Text   ' = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0)
                                    numDepDol = frmDatosNumDep.TextBoxNumDepDolar.Text ' = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0)

                                End If

                            End If
                        End If

                        AdapterEfectivo.InsertCommand.Transaction = Trans
                        AdapterTarjetas.InsertCommand.Transaction = Trans
                        AdapterArqueodeCaja.InsertCommand.Transaction = Trans

                        AdapterEfectivo.UpdateCommand.Transaction = Trans
                        AdapterTarjetas.UpdateCommand.Transaction = Trans
                        AdapterArqueodeCaja.UpdateCommand.Transaction = Trans

                        'evaluarArqueoTarjetas()

                        myCommandArqueoTarjeta.CommandText = "DELETE ArqueoTarjeta WHERE Id_Arqueo = " & BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Id")
                        myCommandArqueoTarjeta.Transaction = Trans
                        myCommandArqueoTarjeta.ExecuteNonQuery()

                        myCommandArqueoEfectivo.CommandText = "DELETE arqueoEfectivo WHERE Id_Arqueo = " & BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Id")
                        myCommandArqueoEfectivo.Transaction = Trans
                        myCommandArqueoEfectivo.ExecuteNonQuery()

                        BindingContext(DataSetArqueo1, "ArqueoCajas").EndCurrentEdit()
                        AdapterArqueodeCaja.Update(DataSetArqueo1, "ArqueoCajas")
                        AdapterEfectivo.Update(DataSetArqueo1, "ArqueoEfectivo")
                        AdapterTarjetas.Update(DataSetArqueo1, "ArqueoTarjeta")
                        Trans.Commit()

                        MsgBox("Datos Actualizados Satisfactoriamente....", MsgBoxStyle.Information, "Atenci�n...")
                        Imprimir()

                    ElseIf ToolBarRegistrar.Text = "Registrar" Then
                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Fecha") = Date.Today.ToShortDateString
                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Anulado") = 0
                        BindingContext(DataSetArqueo1, "ArqueoCajas").EndCurrentEdit()

                        CargarDatos()

                        If Me.dt_datosEmpresa.Rows.Count > 0 Then
                            If dt_datosEmpresa.Rows(0).Item("Cedula").Equals("3-101-374928-30") And (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0 Or Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0) Then

                                Dim frmDatosNumDep As New FormNumDepositos
                                frmDatosNumDep.TextBoxNumDepColon.Enabled = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0)
                                frmDatosNumDep.TextBoxNumDepDolar.Enabled = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0)

                                If frmDatosNumDep.ShowDialog() = Windows.Forms.DialogResult.OK Then
                                    numDepCol = frmDatosNumDep.TextBoxNumDepColon.Text   ' = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoColones > 0)
                                    numDepDol = frmDatosNumDep.TextBoxNumDepDolar.Text ' = (Me.DataSetArqueo1.ArqueoCajas(0).EfectivoDolares > 0)

                                End If

                            End If
                        End If

                        AdapterEfectivo.InsertCommand.Transaction = Trans
                        AdapterTarjetas.InsertCommand.Transaction = Trans
                        AdapterArqueodeCaja.InsertCommand.Transaction = Trans

                        AdapterEfectivo.UpdateCommand.Transaction = Trans
                        AdapterTarjetas.UpdateCommand.Transaction = Trans
                        AdapterArqueodeCaja.UpdateCommand.Transaction = Trans

                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("IdApertura") = NApertura
                        BindingContext(DataSetArqueo1, "ArqueoCajas").Current("Cajero") = txtNombreUsuario.Text
                        evaluarArqueoTarjetas()

                        myCommand1.CommandText = "UPDATE aperturacaja SET Estado = '" & "M" & "' WHERE NApertura = " & NApertura
                        myCommand1.Transaction = Trans
                        myCommand1.ExecuteNonQuery()


                        BindingContext(DataSetArqueo1, "ArqueoCajas").EndCurrentEdit()
                        AdapterArqueodeCaja.Update(DataSetArqueo1, "ArqueoCajas")
                        AdapterEfectivo.Update(DataSetArqueo1, "ArqueoEfectivo")
                        AdapterTarjetas.Update(DataSetArqueo1, "ArqueoTarjeta")
                        Trans.Commit()

                        Dim cConexion As New Conexion
                        Dim sqlConexionC As New SqlConnection
                        sqlConexionC = cConexion.Conectar
                        cConexion.SlqExecute(sqlConexionC, "Update ArqueoCajas set TipoCambioD=" & TipoCambioDolar & ", TipoCambioE= " & TipoCambioEuro & ", NumDepDolar= '" & numDepDol & "', NumDepColon= '" & numDepCol & "' where idApertura=" & NApertura)
                        cConexion.DesConectar(sqlConexionC)
                        sqlConexionC = Nothing
                        cConexion = Nothing

                        MsgBox("Datos Ingresados Satisfactoriamente....", MsgBoxStyle.Information, "Atenci�n...")
                        Imprimir()
                    End If

                    Inhabilitar()

                    'Para boton Nuevo
                    Me.ToolBarNuevo.Text = "Nuevo"
                    Me.ToolBarNuevo.ImageIndex = 0
                    'Para boton Acualizar
                    ToolBarRegistrar.Enabled = False
                    ToolBarEliminar.ImageIndex = 5
                    '  Me.GridControl1.Enabled = True

                    ToolBarNuevo.Enabled = True
                    Me.ToolBarBuscar.Enabled = True



                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)

                    MsgBox(eEndEdit.Message)
                End Try
            Else
                Me.BindingContext(Me.DataSetArqueo1.ArqueoCaja).CancelCurrentEdit()
                Me.DataSetArqueo1.RejectChanges()
                'Para boton Nuevo
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                'Para boton Acualizar
                Me.ToolBarEliminar.Text = "Editar"
                Me.ToolBarEliminar.ImageIndex = 5
                Me.Inhabilitar()
            End If
        Else
            MsgBox("Debes Ingresar Campos....", MsgBoxStyle.Information, "Atenci�n...")
        End If
    End Function

    Function Editar()
        If ToolBarEliminar.Text = "Editar" Then
            'ToolBarEliminar.Text = "Cancelar"
            'ToolBarEliminar.ImageIndex = 8
            ToolBarRegistrar.Text = "Actualizar"
            ToolBarRegistrar.Enabled = True
            ToolBarNuevo.Enabled = True
            ToolBarNuevo.Text = "Cancelar"
            Me.ToolBarNuevo.ImageIndex = 8
            ToolBarBuscar.Enabled = False
            ToolBarEliminar.Enabled = False
            ToolBarExcel.Enabled = False
            Me.ToolBarImprimir.Enabled = False

            Habilitar()
        Else
            'ToolBarEliminar.Text = "Editar"
            ToolBarRegistrar.Text = "Registrar"
            ToolBarEliminar.ImageIndex = 5
            ToolBarRegistrar.Enabled = False
            ToolBarNuevo.Enabled = True
            ToolBarBuscar.Enabled = True
            ToolBarEliminar.Enabled = True
            Me.ToolBarImprimir.Enabled = True
            Inhabilitar()
        End If
    End Function
    Function Imprimir()
        Try
            If Me.check_PVE.Checked = True Then
                Dim ReporteArqueo As New Arqueo_PVE
                Dim visor As New frmVisorReportes
                'visor.MdiParent = Me.ParentForm
                CrystalReportsConexion.LoadReportViewer(visor.rptViewer, ReporteArqueo)
                ReporteArqueo.SetParameterValue(0, Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("Id"))
                visor.Show()
            Else
                Dim ReporteArqueo As New ReporteArqueo
                Dim visor As New frmVisorReportes
                'visor.MdiParent = Me.ParentForm
                CrystalReportsConexion.LoadReportViewer(visor.rptViewer, ReporteArqueo)
                ReporteArqueo.SetParameterValue(0, Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("Id"))
                visor.Show()
            End If
            
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function

    Function Anular()
        Dim resp As Integer
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Dim myCommand1 As SqlCommand = SqlConnection1.CreateCommand()
        Dim myCommand2 As SqlCommand = SqlConnection1.CreateCommand()
        resp = MessageBox.Show("�Deseas Anular el Arqueo?", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
		Dim conextion As New ConexionR
		Dim existeUnaCajaAbierta As Boolean = conextion.SlqExecuteScalar(conextion.Conectar(), "Select COUNT(*) from aperturacaja  where Nombre = '" & Me.BindingContext(Me.DataSetArqueo1, "aperturacaja").Current("Nombre") & "' and Estado = 'A' ")
		If existeUnaCajaAbierta = False Then
			If resp = 6 Then
				myCommand1.CommandText = "UPDATE ArqueoCajas SET Anulado =  1 WHERE Id = " & Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("Id")
				myCommand2.CommandText = "UPDATE aperturacaja SET Estado = '" & "A" & "' WHERE NApertura = " & Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas").Current("IdApertura")
				myCommand1.Transaction = Trans
				myCommand2.Transaction = Trans
				myCommand1.ExecuteNonQuery()
				myCommand2.ExecuteNonQuery()
				Trans.Commit()
				MsgBox("Datos Anulados Correctamente....", MsgBoxStyle.Information, "Atenci�n...")
				Label7.Visible = True
				Me.ToolBarEliminar.Enabled = False
				Me.ToolBarExcel.Enabled = False
				Me.Inhabilitar()

				Me.Loggin_Usuario()
			Else
			End If
		Else
			MsgBox("Existe una caja abierta de est Usuario, no puede Anular esta apertura", MsgBoxStyle.Information, "Atenci�n...")
		End If
		conextion.DesConectar(conextion.SqlConexion)

	End Function

    Function Validar()
        Return True
    End Function

    Function CargarDatos()
        Dim I As Integer
        For I = 0 To Me.DataSetArqueo1.Denominacion_Cierre.Count - 1

            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoEfectivo").AddNew()
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoEfectivo").Current("Id_Denominacion") = Me.DataSetArqueo1.Denominacion_Cierre.Rows(I).Item("Id")
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoEfectivo").Current("Monto") = Me.DataSetArqueo1.Denominacion_Cierre.Rows(I).Item("Total")
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoEfectivo").Current("Cantidad") = Me.DataSetArqueo1.Denominacion_Cierre.Rows(I).Item("Cantidad")
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoEfectivo").EndCurrentEdit()
        Next
        For I = 0 To Me.DataSetArqueo1.TipoTarjeta.Count - 1

            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").AddNew()
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").Current("Id_Tarjeta") = Me.DataSetArqueo1.TipoTarjeta.Rows(I).Item("Id")
            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").Current("Monto") = Me.DataSetArqueo1.TipoTarjeta.Rows(I).Item("Total")


            Me.BindingContext(Me.DataSetArqueo1, "ArqueoCajas.ArqueoCajasArqueoTarjeta").EndCurrentEdit()
        Next
    End Function


End Class
