﻿Imports System.ComponentModel
Imports System.Data.SqlClient

Public Class frmCentroProduccionCocina

    Dim PMU As New PerfilModulo_Class
    Dim NumCentros As Integer = 0
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlClient.SqlConnection
    Dim rs As SqlClient.SqlDataReader
    Dim dt As New DataTable
    Dim cambio As Boolean = False
    Public Shared IdCentro As Integer = 0
    Public TiempoEspera As Double
    Dim TiempoInicial As Date
    Public NombreCentro As String = ""
    Dim aButton As ucCocina
    Dim ListaColores As New ArrayList
    Dim CantDetalles As Integer = 0
    Dim TiempoTotal As Integer = 0
    Dim Tiempo As Integer = 0
    Public TiempoExtra As Integer = 0
    Public NombreControl As String = ""
    Public TiempoPrioridad As Double = 0

    Private Sub frmCentroProduccionCocina_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        lbCentroProduccion.Text = NombreCentro
        CargarColores()
        TiempoInicial = Now
        txtTiempoExtra.Text = TiempoExtra.ToString()
        Me.Timer1.Enabled = True
        Me.tmHora.Start()
    End Sub

    Public Sub CargarColores()

        ListaColores.Add("IndianRed")
        ListaColores.Add("LightCoral")
        ListaColores.Add("Tomato")
        ListaColores.Add("OrangeRed")
        ListaColores.Add("Chocolate")
        ListaColores.Add("SandyBrown")
        ListaColores.Add("Goldenrod")
        ListaColores.Add("Orange")
        ListaColores.Add("Gold")
        ListaColores.Add("Yellow")
        ListaColores.Add("YellowGreen")
        ListaColores.Add("GreenYellow")
        ListaColores.Add("RosyBrown")
        ListaColores.Add("LimeGreen")
        ListaColores.Add("SpringGreen")
        ListaColores.Add("Turquoise")
        ListaColores.Add("Aqua")
        ListaColores.Add("HotPink")
        ListaColores.Add("DarkKhaki")
        ListaColores.Add("Tan")
        ListaColores.Add("NavajoWhite")
        ListaColores.Add("Lime")
        ListaColores.Add("DeepPink")
        ListaColores.Add("Crimson")
        ListaColores.Add("Magenta")
        ListaColores.Add("DarkSeaGreen")
        ListaColores.Add("PaleTurquoise")
        ListaColores.Add("CadetBlue")
        ListaColores.Add("DodgerBlue")
        ListaColores.Add("MediumVioletRed")
        ListaColores.Add("Violet")
        ListaColores.Add("Olive")
        ListaColores.Add("Coral")
        ListaColores.Add("Peru")
        ListaColores.Add("DarkOrange")
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        RefrescarOrdenes()
    End Sub

    Public Function RandomColor() As Color
        Dim random As New Random
        Dim ColorDevuelto As Color
        Dim MiColor As String = ""
        Dim dt As New DataTable

        Try

OtraVez:
            ColorDevuelto = Color.FromName(ListaColores(random.Next(0, 35)))
            MiColor = ColorDevuelto.ToString()
            MiColor = MiColor.Replace("Color [", "").Replace("]", "")

            If IsNumeric(MiColor) Then
                GoTo OtraVez
            End If
            Dim sql As New SqlCommand("Select IdOrden from tb_Orden with (Nolock) where Color='" & ColorDevuelto.ToString() & "' and Estado=1 ")

            cFunciones.spCargarDatos(sql, dt)

            If dt.Rows.Count > 0 Then
                GoTo OtraVez
            End If

            Return ColorDevuelto

        Catch ex As Exception
            GoTo OtraVez
        End Try

    End Function

    Public Function CalcularColumas() As Integer
        Try
            Dim AnchoPanel As Double = 0
            Dim AnchoControl As Double = 0
            Dim Columnas As Integer = 0
            aButton = New ucCocina
            AnchoPanel = PanelOrdenes.Size.Width
            AnchoControl = aButton.Size.Width

            Columnas = Math.Floor((AnchoPanel / AnchoControl))
            Return Columnas

        Catch ex As Exception
            Return 6
        End Try
    End Function

    Public Sub CargarComandas() 'Panel principal 
        Try

            Dim i, ii, n As Integer
            Dim y As Integer = 3
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas()

            Dim num_controles As Int32 = Me.PanelOrdenes.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.PanelOrdenes.Controls(z)
                Me.PanelOrdenes.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            PanelOrdenes.Controls.Clear()

            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim LosNuevos As Integer = 0

            cFunciones.Llenar_Tabla_Generico("select tb_DetalleOrden.IdOrden,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden with (Nolock) right outer join tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden
            where tb_detalleOrden.IdCentroProduccion=" & IdCentro & " and tb_DetalleOrden.Estado=1 order by Prioridad desc,NumOrden asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("IdOrden")) Then
                        unicos.Add(dt.Rows(k).Item("IdOrden"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next


                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc()
                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                TiempoTotal = TiempoEspera + Tiempo

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select Descripcion,IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color,NumOrden from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " and tb_DetalleOrden.IdCentroProduccion=" & IdCentro & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        aButton = New ucCocina
                        aButton.Top = y
                        aButton.Left = x
                        aButton.Width = 326
                        aButton.Height = LargoUc + 6
                        aButton.pnDetalle.Height = LargoUc
                        aButton.Max = LargoUc
                        aButton.Name = "Orden#" & dt.Rows(0).Item("IdOrden")
                        aButton.NumOrden = "Orden #" & dt.Rows(0).Item("NumOrden")
                        aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                        aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                        aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                        aButton.frm = Me
                        aButton.NameOrden = "Orden#" & dt.Rows(0).Item("IdOrden")
                        aButton.txtnumOrden.BackColor = System.Drawing.Color.FromName(Convert.ToString(dt.Rows(0).Item("Color")).Replace("Color [", "").Replace("]", ""))

                        If RevisarTiempo(dt.Rows(0).Item("TiempoTranscurrido").ToString()) Then
                            aButton.Urgente = 1
                        Else
                            aButton.Urgente = 0
                        End If

                        If i = 0 And ii = 0 Then
                            aButton.Index = 1
                        End If

                        Me.PanelOrdenes.Controls.Add(aButton)
                        x = x + 328
                        n = n + 1

                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    y = y + LargoUc + 7
                    x = 3
                Next
            Else

                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al crear mesas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Public Function RevisarTiempo(ByVal Tiempo As String) As Boolean
        Try
            Dim n As Integer = TiempoPrioridad + TiempoExtra
            Dim horas As Integer = n \ 60
            Dim minutos As Integer = n Mod 60
            Dim Formato As String = Convert.ToString(horas) + ":" + Convert.ToString(minutos) + ":00"
            Dim TiempoTrascurrido As DateTime = CType(Tiempo, DateTime)
            Dim resultado As DateTime = CType(Formato, DateTime)
            Dim Transcurrido As Double = 0

            Transcurrido = DateTime.Compare(TiempoTrascurrido, resultado)
            If Transcurrido > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Dim msg As String = ex.Message
            Return False
        End Try
    End Function

    Function procesarTiempo(ByVal tiempo As DateTime, ByVal IdOrden As Integer) As String

        If tiempo.Equals("") Then
            Return ""
        End If

        Return tiempo.ToString("hh:mm")

    End Function

    Private Sub RefrescarOrdenes(Optional refrescar As Integer = 0)
        Try
            If refrescar = 1 Then
                GoTo refrescar
            End If

            If DateDiff(DateInterval.Second, TiempoInicial, Now) <= TiempoEspera Then
                lbTimer.Text = TiempoEspera - DateDiff(DateInterval.Second, TiempoInicial, Now) + 1
            Else
                If lbTimer.Text <> "Actualizando..." Then
                    TiempoInicial = Now
                    lbTimer.Text = "Actualizando..."
refrescar:
                    Dim dtNuevos As New DataTable()
                    Dim dtOrden As New DataTable()
                    Dim dtDetalles As New DataTable()
                    Dim IdOrden As Integer
                    Dim Entrar As Integer = 0
                    dtNuevos.Clear()
                    dtOrden.Clear()

                    cFunciones.Llenar_Tabla_Generico("EXEC[dbo].[DevolverComandas] @IdCentro = " & IdCentro & "", dtNuevos, GetSetting("Seesoft", "Restaurante", "Conexion"))

                    If dtNuevos.Rows.Count > 0 Then
                        For n As Integer = 0 To dtNuevos.Rows.Count - 1

                            If dtNuevos.Rows(n).Item("IdCentro") = IdCentro Then
                                Entrar = 1
                                Exit For
                            End If

                        Next
                    End If


                    If Entrar = 1 Then
                        Dim cx As New Conexion
                        Dim Sql As String = ""
                        Dim dtNumOrden As New DataTable
                        Dim NumOrden As Integer = 0
                        dtNumOrden.Clear()

                        cFunciones.Llenar_Tabla_Generico("Select isnull( Max(NumOrden),0) as NumOrden from tb_Orden with (Nolock) where Estado=1", dtNumOrden)

                        If CDbl(dtNumOrden.Rows(0).Item("NumOrden")) = 0 Then
                            NumOrden = 1
                        Else
                            NumOrden = CDbl(dtNumOrden.Rows(0).Item("NumOrden")) + 1
                        End If

                        Try
                            My.Computer.Audio.Play(My.Resources.Campana, AudioPlayMode.Background)
                        Catch ex As Exception

                        End Try

                        Sql = "Insert Into tb_Orden ([FechaIngreso],[IdCentroProduccion],[Color],[Estado],[NumOrden],[TiempoTranscurrido]) Values (GETDATE()," & IdCentro & ",'" & RandomColor().ToString() & "',1," & NumOrden & ",'0:0:0')"
                        cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                        cx.DesConectar(cx.sQlconexion)



                        cFunciones.Llenar_Tabla_Generico("Select Max(IdOrden) as IdOrden from tb_Orden with (Nolock)", dtOrden, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        IdOrden = dtOrden.Rows(0).Item("IdOrden")

                        For h As Integer = 0 To dtNuevos.Rows.Count - 1
                            If dtNuevos.Rows(h).Item("IdCentro") = IdCentro Then
                                Dim Express As Integer = 0

                                If dtNuevos.Rows(h).Item("Express") = "False" Then
                                    Express = 0
                                Else
                                    Express = 1
                                End If

                                Sql = "Insert Into tb_DetalleOrden ([IdOrden],[IdComandaTemporal],[Cantidad],[Descripcion],[CodProducto],[Estado],[IdCentroProduccion],[CodMesa],[NombreMesa],[CantidadDespachada],[ParaEntregar],[Prioridad],[Express],[EstadoComanda],[Entregado],[Agrupar],[NumComanda],[FueImpresa]) 
                            Values (" & IdOrden & "," & dtNuevos.Rows(h).Item("Id") & "," & dtNuevos.Rows(h).Item("cCantidad") & ",'" & dtNuevos.Rows(h).Item("cDescripcion") & "'," & dtNuevos.Rows(h).Item("cProducto") & ",1," & dtNuevos.Rows(h).Item("IdCentro") & "," & dtNuevos.Rows(h).Item("cMesa") & ",'" & dtNuevos.Rows(h).Item("Nombre_Mesa") & "',0,0," & dtNuevos.Rows(h).Item("Primero") & "," & Express & ",1,0,'" & dtNuevos.Rows(h).Item("Agrupar") & "'," & dtNuevos.Rows(h).Item("cCodigo") & ",0)"
                                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                                cx.DesConectar(cx.sQlconexion)
                            End If

                        Next
                        Entrar = 0
                    End If
                    GuardarTiempoTranscurrido()
                    CargarComandas()
                    PanelOrdenes.Visible = True

                ElseIf DateDiff(DateInterval.Second, TiempoInicial, Now) >= TiempoEspera Then
                    TiempoInicial = Now
                End If
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "Timer1_Tick", ex.Message)
        End Try
    End Sub

    Sub GuardarTiempoTranscurrido()
        For Each control As ucCocina In PanelOrdenes.Controls
            control.GuardarTiempoTranscurrido()
        Next
    End Sub

    Private Sub btAgrupar_Click(sender As Object, e As EventArgs) Handles btAgrupar.Click

        RefrescarOrdenes(1)
    End Sub

    Public Sub EliminarListos(Optional IdCentro As Integer = 0, Optional IdOrden As Integer = 0)
        Try

            PanelOrdenes.Visible = False

            Dim cx As New Conexion
            Dim Sql As String = ""

            If IdCentro <> 0 Then
                Sql = "Update tb_DetalleOrden set Estado=0 where IdCentroProduccion=" & IdCentro & " and IdOrden=" & IdOrden & ""
                cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
            End If

            PanelOrdenes.Visible = True
            If IdCentro <> 0 Then
                CargarComandas()
            End If
        Catch ex As Exception
            MsgBox("Error al regresar mesa al panel superior. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub frmCentroProduccionCocina_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Timer1.Enabled = False
        Me.tmHora.Stop()
    End Sub


    Public Function CalcularLargoUc() As Integer
        Try
            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim Max As Integer = 0

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico("select tb_DetalleOrden.IdOrden,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden with (Nolock) right outer join tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden
            where tb_DetalleOrden.Estado=1 and tb_DetalleOrden.IdCentroProduccion=" & IdCentro & " order by NumOrden asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("IdOrden")) Then
                        unicos.Add(dt.Rows(k).Item("IdOrden"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                dt.Clear()
                For o As Integer = 0 To UnicosId.Count - 1
                    cFunciones.Llenar_Tabla_Generico("select Descripcion,IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color,NumOrden from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_DetalleOrden.Estado=1 and tb_detalleOrden.IdComandaTemporal=" & UnicosId(o) & " and tb_DetalleOrden.IdCentroProduccion=" & IdCentro & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                    If VerificarDetalles(IdCentro, Convert.ToInt32(dt.Rows(0).Item("IdOrden"))) Then
                        If Max < CantDetalles Then
                            Max = CantDetalles
                        End If
                    End If
                Next


            End If

            Max = Max + 2
            Return Max * 25 '25 es el ancho del textbox que contiene las descripciones de las comandas en los user control

        Catch ex As Exception
            Return 125
        End Try
    End Function

    Public Function VerificarDetalles(ByVal Centro As Integer, ByVal _IdOrden As Integer) As Boolean
        Try
            Dim dt As New DataTable

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico("Select sum(Cantidad) as TotalOrden,Descripcion,Despachado, Prioridad, 0 as IdComandaTemporal from vs_OrdenOrdenada with (Nolock) where IdCentroProduccion=" & Centro & " and IdOrden=" & _IdOrden & " and Agrupar='Si'
group by CodProducto,Descripcion,Despachado,Prioridad union Select sum(Cantidad) as TotalOrden,Descripcion,Despachado, Prioridad,IdComandaTemporal from vs_OrdenOrdenada with (Nolock) where IdCentroProduccion=" & Centro & " and IdOrden=" & _IdOrden & " and Agrupar='No'
group by CodProducto,Descripcion,Despachado,Prioridad,IdComandaTemporal order by Prioridad desc,IdComandaTemporal asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                CantDetalles = dt.Rows.Count
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtTiempoExtra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTiempoExtra.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub

    Private Sub txtTiempoExtra_KeyUp(sender As Object, e As KeyEventArgs) Handles txtTiempoExtra.KeyUp
        If e.KeyCode = Keys.Enter Then
            Dim cx As New Conexion
            Dim Sql As String = ""


            If txtTiempoExtra.Text.Trim() = "" Then
                Tiempo = 0
            Else
                Tiempo = Convert.ToInt32(txtTiempoExtra.Text)
            End If

            If IdCentro <> 0 Then
                    Sql = "Update tb_CentroProduccion set TiempoExtra=" & Tiempo & " where Id=" & IdCentro & ""
                    cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                cx.DesConectar(cx.sQlconexion)
                TiempoExtra = Tiempo
            End If
            End If
    End Sub

    Private Sub tmHora_Tick(sender As Object, e As EventArgs) Handles tmHora.Tick
        lbHora.Text = DateTime.Now.ToString("hh:mm:ssss")
    End Sub
End Class