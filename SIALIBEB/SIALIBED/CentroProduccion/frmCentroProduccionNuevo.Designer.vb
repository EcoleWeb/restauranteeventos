﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCentroProduccionNuevo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btNuevo = New System.Windows.Forms.Button()
        Me.btCancelar = New System.Windows.Forms.Button()
        Me.txtNombreCentro = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.numDuracion = New System.Windows.Forms.NumericUpDown()
        Me.cbTipo = New System.Windows.Forms.ComboBox()
        Me.cbEstado = New System.Windows.Forms.ComboBox()
        Me.lbPrioridad = New System.Windows.Forms.Label()
        Me.numPrioridad = New System.Windows.Forms.NumericUpDown()
        Me.lbPrioridad2 = New System.Windows.Forms.Label()
        CType(Me.numDuracion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numPrioridad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(86, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nombre:"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label2.Location = New System.Drawing.Point(86, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(117, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tiempo Act.:"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label3.Location = New System.Drawing.Point(86, 159)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 24)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tipo:"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(86, 204)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 24)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Estado:"
        '
        'btNuevo
        '
        Me.btNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btNuevo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.btNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btNuevo.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btNuevo.Location = New System.Drawing.Point(163, 261)
        Me.btNuevo.Name = "btNuevo"
        Me.btNuevo.Size = New System.Drawing.Size(97, 40)
        Me.btNuevo.TabIndex = 5
        Me.btNuevo.Text = "Guardar"
        Me.btNuevo.UseVisualStyleBackColor = False
        '
        'btCancelar
        '
        Me.btCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btCancelar.BackColor = System.Drawing.Color.DimGray
        Me.btCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btCancelar.Location = New System.Drawing.Point(343, 261)
        Me.btCancelar.Name = "btCancelar"
        Me.btCancelar.Size = New System.Drawing.Size(97, 40)
        Me.btCancelar.TabIndex = 6
        Me.btCancelar.Text = "Cancelar"
        Me.btCancelar.UseVisualStyleBackColor = False
        '
        'txtNombreCentro
        '
        Me.txtNombreCentro.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreCentro.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtNombreCentro.Location = New System.Drawing.Point(200, 32)
        Me.txtNombreCentro.MinimumSize = New System.Drawing.Size(4, 25)
        Me.txtNombreCentro.Name = "txtNombreCentro"
        Me.txtNombreCentro.Size = New System.Drawing.Size(251, 26)
        Me.txtNombreCentro.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(259, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 17)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Segundos."
        '
        'numDuracion
        '
        Me.numDuracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.numDuracion.Location = New System.Drawing.Point(200, 78)
        Me.numDuracion.MinimumSize = New System.Drawing.Size(53, 0)
        Me.numDuracion.Name = "numDuracion"
        Me.numDuracion.Size = New System.Drawing.Size(53, 26)
        Me.numDuracion.TabIndex = 9
        '
        'cbTipo
        '
        Me.cbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbTipo.FormattingEnabled = True
        Me.cbTipo.Location = New System.Drawing.Point(200, 159)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Size = New System.Drawing.Size(121, 28)
        Me.cbTipo.TabIndex = 10
        '
        'cbEstado
        '
        Me.cbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbEstado.FormattingEnabled = True
        Me.cbEstado.Location = New System.Drawing.Point(200, 204)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(121, 28)
        Me.cbEstado.TabIndex = 11
        '
        'lbPrioridad
        '
        Me.lbPrioridad.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbPrioridad.AutoSize = True
        Me.lbPrioridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPrioridad.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbPrioridad.Location = New System.Drawing.Point(86, 117)
        Me.lbPrioridad.Name = "lbPrioridad"
        Me.lbPrioridad.Size = New System.Drawing.Size(112, 24)
        Me.lbPrioridad.TabIndex = 12
        Me.lbPrioridad.Text = "T. Prioridad:"
        Me.lbPrioridad.Visible = False
        '
        'numPrioridad
        '
        Me.numPrioridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.numPrioridad.Location = New System.Drawing.Point(200, 115)
        Me.numPrioridad.MinimumSize = New System.Drawing.Size(53, 0)
        Me.numPrioridad.Name = "numPrioridad"
        Me.numPrioridad.Size = New System.Drawing.Size(53, 26)
        Me.numPrioridad.TabIndex = 13
        Me.numPrioridad.Visible = False
        '
        'lbPrioridad2
        '
        Me.lbPrioridad2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbPrioridad2.AutoSize = True
        Me.lbPrioridad2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.lbPrioridad2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbPrioridad2.Location = New System.Drawing.Point(263, 124)
        Me.lbPrioridad2.Name = "lbPrioridad2"
        Me.lbPrioridad2.Size = New System.Drawing.Size(34, 17)
        Me.lbPrioridad2.TabIndex = 14
        Me.lbPrioridad2.Text = "Min."
        Me.lbPrioridad2.Visible = False
        '
        'frmCentroProduccionNuevo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkGray
        Me.ClientSize = New System.Drawing.Size(595, 313)
        Me.Controls.Add(Me.lbPrioridad2)
        Me.Controls.Add(Me.numPrioridad)
        Me.Controls.Add(Me.lbPrioridad)
        Me.Controls.Add(Me.cbEstado)
        Me.Controls.Add(Me.cbTipo)
        Me.Controls.Add(Me.numDuracion)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtNombreCentro)
        Me.Controls.Add(Me.btCancelar)
        Me.Controls.Add(Me.btNuevo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "frmCentroProduccionNuevo"
        Me.Text = "Centro de Producción (Nuevo)"
        CType(Me.numDuracion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numPrioridad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btNuevo As Button
    Friend WithEvents btCancelar As Button
    Friend WithEvents txtNombreCentro As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents numDuracion As NumericUpDown
    Friend WithEvents cbTipo As ComboBox
    Friend WithEvents cbEstado As ComboBox
    Friend WithEvents lbPrioridad As Label
    Friend WithEvents numPrioridad As NumericUpDown
    Friend WithEvents lbPrioridad2 As Label
End Class
