﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCentroProduccionBebidas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelMesas = New System.Windows.Forms.Panel()
        Me.PanelResumen = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanelSeleccionados = New System.Windows.Forms.Panel()
        Me.pnTimer = New System.Windows.Forms.Panel()
        Me.lbHora = New System.Windows.Forms.Label()
        Me.btActualizar = New System.Windows.Forms.Button()
        Me.lbTimer = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lbNombreCentro = New System.Windows.Forms.Label()
        Me.lbCentroProduccion = New System.Windows.Forms.Label()
        Me.btLimpiar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tmHora = New System.Windows.Forms.Timer(Me.components)
        Me.PanelResumen.SuspendLayout()
        Me.pnTimer.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelMesas
        '
        Me.PanelMesas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelMesas.AutoScroll = True
        Me.PanelMesas.BackColor = System.Drawing.Color.White
        Me.PanelMesas.Location = New System.Drawing.Point(12, 81)
        Me.PanelMesas.Name = "PanelMesas"
        Me.PanelMesas.Size = New System.Drawing.Size(496, 102)
        Me.PanelMesas.TabIndex = 0
        '
        'PanelResumen
        '
        Me.PanelResumen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelResumen.BackColor = System.Drawing.Color.White
        Me.PanelResumen.Controls.Add(Me.Label1)
        Me.PanelResumen.Location = New System.Drawing.Point(514, 81)
        Me.PanelResumen.Name = "PanelResumen"
        Me.PanelResumen.Size = New System.Drawing.Size(274, 308)
        Me.PanelResumen.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(78, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 26)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Preparación"
        '
        'PanelSeleccionados
        '
        Me.PanelSeleccionados.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelSeleccionados.AutoScroll = True
        Me.PanelSeleccionados.BackColor = System.Drawing.Color.White
        Me.PanelSeleccionados.Location = New System.Drawing.Point(12, 219)
        Me.PanelSeleccionados.Name = "PanelSeleccionados"
        Me.PanelSeleccionados.Size = New System.Drawing.Size(496, 170)
        Me.PanelSeleccionados.TabIndex = 1
        '
        'pnTimer
        '
        Me.pnTimer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnTimer.BackColor = System.Drawing.Color.White
        Me.pnTimer.Controls.Add(Me.lbHora)
        Me.pnTimer.Controls.Add(Me.btActualizar)
        Me.pnTimer.Controls.Add(Me.lbTimer)
        Me.pnTimer.Controls.Add(Me.Label2)
        Me.pnTimer.Location = New System.Drawing.Point(514, 12)
        Me.pnTimer.Name = "pnTimer"
        Me.pnTimer.Size = New System.Drawing.Size(274, 63)
        Me.pnTimer.TabIndex = 6
        '
        'lbHora
        '
        Me.lbHora.AutoSize = True
        Me.lbHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbHora.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbHora.Location = New System.Drawing.Point(18, 30)
        Me.lbHora.Name = "lbHora"
        Me.lbHora.Size = New System.Drawing.Size(90, 25)
        Me.lbHora.TabIndex = 11
        Me.lbHora.Text = "00:00:00"
        '
        'btActualizar
        '
        Me.btActualizar.BackColor = System.Drawing.Color.OrangeRed
        Me.btActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btActualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btActualizar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btActualizar.Location = New System.Drawing.Point(162, 27)
        Me.btActualizar.Name = "btActualizar"
        Me.btActualizar.Size = New System.Drawing.Size(108, 33)
        Me.btActualizar.TabIndex = 9
        Me.btActualizar.Text = "Actualizar"
        Me.btActualizar.UseVisualStyleBackColor = False
        '
        'lbTimer
        '
        Me.lbTimer.AutoSize = True
        Me.lbTimer.BackColor = System.Drawing.Color.White
        Me.lbTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTimer.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbTimer.Location = New System.Drawing.Point(175, 3)
        Me.lbTimer.Name = "lbTimer"
        Me.lbTimer.Size = New System.Drawing.Size(20, 24)
        Me.lbTimer.TabIndex = 8
        Me.lbTimer.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 24)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Actualizando en:"
        '
        'Timer1
        '
        '
        'lbNombreCentro
        '
        Me.lbNombreCentro.AutoSize = True
        Me.lbNombreCentro.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombreCentro.ForeColor = System.Drawing.Color.White
        Me.lbNombreCentro.Location = New System.Drawing.Point(12, 26)
        Me.lbNombreCentro.Name = "lbNombreCentro"
        Me.lbNombreCentro.Size = New System.Drawing.Size(281, 31)
        Me.lbNombreCentro.TabIndex = 7
        Me.lbNombreCentro.Text = "Centro de producción:"
        '
        'lbCentroProduccion
        '
        Me.lbCentroProduccion.AutoSize = True
        Me.lbCentroProduccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCentroProduccion.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbCentroProduccion.Location = New System.Drawing.Point(299, 26)
        Me.lbCentroProduccion.Name = "lbCentroProduccion"
        Me.lbCentroProduccion.Size = New System.Drawing.Size(160, 31)
        Me.lbCentroProduccion.TabIndex = 8
        Me.lbCentroProduccion.Text = "Preparación"
        '
        'btLimpiar
        '
        Me.btLimpiar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btLimpiar.BackColor = System.Drawing.Color.Peru
        Me.btLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btLimpiar.ForeColor = System.Drawing.Color.White
        Me.btLimpiar.Location = New System.Drawing.Point(367, 192)
        Me.btLimpiar.Name = "btLimpiar"
        Me.btLimpiar.Size = New System.Drawing.Size(140, 27)
        Me.btLimpiar.TabIndex = 10
        Me.btLimpiar.Text = "Limpiar Panel"
        Me.btLimpiar.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label3.Location = New System.Drawing.Point(13, 190)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 26)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Listos"
        '
        'tmHora
        '
        '
        'frmCentroProduccionBebidas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(800, 401)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btLimpiar)
        Me.Controls.Add(Me.lbCentroProduccion)
        Me.Controls.Add(Me.lbNombreCentro)
        Me.Controls.Add(Me.pnTimer)
        Me.Controls.Add(Me.PanelSeleccionados)
        Me.Controls.Add(Me.PanelResumen)
        Me.Controls.Add(Me.PanelMesas)
        Me.Name = "frmCentroProduccionBebidas"
        Me.Text = "Centro de Producción (Bebidas)"
        Me.PanelResumen.ResumeLayout(False)
        Me.PanelResumen.PerformLayout()
        Me.pnTimer.ResumeLayout(False)
        Me.pnTimer.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents pnTimer As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents lbTimer As Label
    Friend WithEvents Timer1 As Timer
    Public WithEvents PanelSeleccionados As Panel
    Friend WithEvents lbNombreCentro As Label
    Friend WithEvents lbCentroProduccion As Label
    Public WithEvents PanelMesas As Panel
    Public WithEvents PanelResumen As Panel
    Friend WithEvents btActualizar As Button
    Friend WithEvents btLimpiar As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents tmHora As Timer
    Friend WithEvents lbHora As Label
End Class
