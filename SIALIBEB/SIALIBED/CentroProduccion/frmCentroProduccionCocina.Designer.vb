﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCentroProduccionCocina
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelOrdenes = New System.Windows.Forms.Panel()
        Me.lbNombreCentro = New System.Windows.Forms.Label()
        Me.pnTimer = New System.Windows.Forms.Panel()
        Me.lbHora = New System.Windows.Forms.Label()
        Me.lbTimer = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbCentroProduccion = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PanelResumen = New System.Windows.Forms.Panel()
        Me.btAgrupar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTiempoExtra = New System.Windows.Forms.TextBox()
        Me.tmHora = New System.Windows.Forms.Timer(Me.components)
        Me.pnTimer.SuspendLayout()
        Me.PanelResumen.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelOrdenes
        '
        Me.PanelOrdenes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelOrdenes.AutoScroll = True
        Me.PanelOrdenes.BackColor = System.Drawing.Color.White
        Me.PanelOrdenes.Location = New System.Drawing.Point(12, 74)
        Me.PanelOrdenes.Name = "PanelOrdenes"
        Me.PanelOrdenes.Size = New System.Drawing.Size(554, 322)
        Me.PanelOrdenes.TabIndex = 1
        '
        'lbNombreCentro
        '
        Me.lbNombreCentro.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombreCentro.ForeColor = System.Drawing.Color.White
        Me.lbNombreCentro.Location = New System.Drawing.Point(12, 19)
        Me.lbNombreCentro.Name = "lbNombreCentro"
        Me.lbNombreCentro.Size = New System.Drawing.Size(281, 31)
        Me.lbNombreCentro.TabIndex = 8
        Me.lbNombreCentro.Text = "Centro de producción:"
        '
        'pnTimer
        '
        Me.pnTimer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnTimer.BackColor = System.Drawing.Color.White
        Me.pnTimer.Controls.Add(Me.lbHora)
        Me.pnTimer.Controls.Add(Me.lbTimer)
        Me.pnTimer.Controls.Add(Me.Label2)
        Me.pnTimer.Location = New System.Drawing.Point(572, 9)
        Me.pnTimer.Name = "pnTimer"
        Me.pnTimer.Size = New System.Drawing.Size(216, 59)
        Me.pnTimer.TabIndex = 9
        '
        'lbHora
        '
        Me.lbHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbHora.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbHora.Location = New System.Drawing.Point(31, 28)
        Me.lbHora.Name = "lbHora"
        Me.lbHora.Size = New System.Drawing.Size(154, 24)
        Me.lbHora.TabIndex = 14
        Me.lbHora.Text = "Centro de producción:"
        Me.lbHora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbTimer
        '
        Me.lbTimer.AutoSize = True
        Me.lbTimer.BackColor = System.Drawing.Color.White
        Me.lbTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTimer.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbTimer.Location = New System.Drawing.Point(170, 3)
        Me.lbTimer.Name = "lbTimer"
        Me.lbTimer.Size = New System.Drawing.Size(24, 26)
        Me.lbTimer.TabIndex = 8
        Me.lbTimer.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 26)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Actualizando en:"
        '
        'lbCentroProduccion
        '
        Me.lbCentroProduccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCentroProduccion.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.lbCentroProduccion.Location = New System.Drawing.Point(294, 20)
        Me.lbCentroProduccion.Name = "lbCentroProduccion"
        Me.lbCentroProduccion.Size = New System.Drawing.Size(160, 31)
        Me.lbCentroProduccion.TabIndex = 11
        Me.lbCentroProduccion.Text = "Preparación"
        '
        'Timer1
        '
        '
        'PanelResumen
        '
        Me.PanelResumen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelResumen.BackColor = System.Drawing.Color.White
        Me.PanelResumen.Controls.Add(Me.btAgrupar)
        Me.PanelResumen.Location = New System.Drawing.Point(572, 74)
        Me.PanelResumen.Name = "PanelResumen"
        Me.PanelResumen.Size = New System.Drawing.Size(216, 322)
        Me.PanelResumen.TabIndex = 10
        '
        'btAgrupar
        '
        Me.btAgrupar.BackColor = System.Drawing.Color.White
        Me.btAgrupar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAgrupar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAgrupar.ForeColor = System.Drawing.Color.Red
        Me.btAgrupar.Location = New System.Drawing.Point(53, 15)
        Me.btAgrupar.Name = "btAgrupar"
        Me.btAgrupar.Size = New System.Drawing.Size(118, 56)
        Me.btAgrupar.TabIndex = 0
        Me.btAgrupar.Text = "Agrupar"
        Me.btAgrupar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.DimGray
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label1.Location = New System.Drawing.Point(397, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 20)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "T. Extra(Min):"
        '
        'txtTiempoExtra
        '
        Me.txtTiempoExtra.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTiempoExtra.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTiempoExtra.Location = New System.Drawing.Point(509, 25)
        Me.txtTiempoExtra.Name = "txtTiempoExtra"
        Me.txtTiempoExtra.Size = New System.Drawing.Size(45, 26)
        Me.txtTiempoExtra.TabIndex = 13
        Me.txtTiempoExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tmHora
        '
        '
        'frmCentroProduccionCocina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(800, 408)
        Me.Controls.Add(Me.txtTiempoExtra)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbCentroProduccion)
        Me.Controls.Add(Me.PanelResumen)
        Me.Controls.Add(Me.pnTimer)
        Me.Controls.Add(Me.lbNombreCentro)
        Me.Controls.Add(Me.PanelOrdenes)
        Me.Name = "frmCentroProduccionCocina"
        Me.Text = "Centro de Producción (Cocina)"
        Me.pnTimer.ResumeLayout(False)
        Me.pnTimer.PerformLayout()
        Me.PanelResumen.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents PanelOrdenes As Panel
    Friend WithEvents lbNombreCentro As Label
    Friend WithEvents pnTimer As Panel
    Friend WithEvents lbTimer As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lbCentroProduccion As Label
    Friend WithEvents Timer1 As Timer
    Public WithEvents PanelResumen As Panel
    Friend WithEvents btAgrupar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTiempoExtra As TextBox
    Friend WithEvents tmHora As Timer
    Friend WithEvents lbHora As Label
End Class
