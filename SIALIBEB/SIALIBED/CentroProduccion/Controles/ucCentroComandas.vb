﻿Imports System.Drawing.Printing

Public Class ucCentroComandas

    Public NumMesa As String = ""
    Public Tomado As String = ""
    Public NameMesa As String = ""
    Dim NumCentros As Double = 0
    Dim Filas As Integer = 0
    Public IdMesa As Integer = 0
    Public IdCentro As Integer = 0
    Public Index As Integer = 0
    Public IdOrden As Integer = 0
    Public frm As New frmCentroComandas
    Public QuienMeLlama As String = ""
    Public Detalles As String = ""
    Public Max As Integer = 0
    Dim Milisegundos As String = "0"
    Dim Segundos As String = "0"
    Dim Minutos As String = "0"
    Dim Hora As String = "0"
    Dim Tiempo() As String
    Public cConexion As New ConexionR
    Public _MiNumComanda As Integer = 0
    Public Shared conectadobd As New SqlClient.SqlConnection
    Dim NumComanda As Integer = 0
    Dim DtsImprimeComanda1 As SIALIBEB.DtsImprimeComanda
    Dim atxt As New TextBox
    Dim cx As New Conexion
    Private Sub ucCentroComandas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        conectadobd.Close()
        conectadobd = cConexion.Conectar("Restaurante")
        txtnumMesa.Text = NumMesa
        txtnumMesa.Name = NameMesa

        CargarDetalles(IdMesa, Detalles, _MiNumComanda)
        txtnumMesa.TabIndex = 0

        Tiempo = Tomado.Split(":")
        If Tiempo.Count > 0 Then
            Hora = Tiempo(0)
            Minutos = Tiempo(1)
            Segundos = Tiempo(2)
        End If
        tmTiempoTranscurrido.Start()

        If Index = 1 Then
            txtnumMesa.Select()
        End If
    End Sub

    Public Function IdComanda(ByVal Control As String) As Integer
        Try
            Dim datos As String = Control
            Dim _info() As String
            Dim _IdComanda As Integer = 0

            _info = datos.Split("_")

            If _info.Count > 0 Then
                _IdComanda = _info(1)
            End If

            Return _IdComanda
        Catch ex As Exception

        End Try
    End Function

    Private Function ValidarOrden(ByVal IdComanda As Integer) As Boolean
        Try

            Dim dt As New DataTable
            Dim TodoListo As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Despachado from with (Nolock) tb_DetalleOrden where IdComandaTemporal=" & IdComanda & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                If dt.Rows(0).Item("Despachado") = "False" Then
                    TodoListo = 1
                End If

                If TodoListo = 1 Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Error al validar orden. " & ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    Private Sub CargarDetalles(ByVal Mesa As Integer, ByVal Cargar As String, Optional _NumComanda As Integer = 0)
        Try


            Dim ii As Integer
            Dim y As Integer = 26
            Dim x As Integer = 11
            Dim dt As New DataTable
            Dim dtColores As New DataTable
            Dim Consulta As String = ""
            Dim Filtro As String = ""
            Dim unicos As New ArrayList
            Dim CantDetalles As Integer = 0
            Dim CantDetalleMax As Integer = 0
            Dim AnchoTextbox As Integer = 300

            Filtro = frm._Filtro(Cargar)

            Select Case QuienMeLlama
                Case "ListoParaEntregar"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal,NumComanda from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden on a.IdOrden=tb_Orden.IdOrden  where a.IdOrden<>0 and Express=0 and CodMesa=" & Mesa & "  and EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,Entregado,IdComandaTemporal,NumComanda order by Despachado asc, Prioridad desc,IdComandaTemporal asc"
                Case "PanelExpressListas"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal,NumComanda from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden on a.IdOrden=tb_Orden.IdOrden where a.IdOrden<>0 and Express=1 and NumComanda=" & _NumComanda & "  and EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,Entregado,IdComandaTemporal,NumComanda order by Despachado asc, Prioridad desc,IdComandaTemporal asc"
                Case "Comanda"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,a.IdOrden,Despachado, Color,Prioridad, IdComandaTemporal,NumComanda from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden on a.IdOrden=tb_Orden.IdOrden where a.IdOrden<>0 and Express=0 and CodMesa=" & Mesa & "  and EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,IdComandaTemporal,NumComanda order by Despachado asc, Prioridad desc,IdComandaTemporal asc"
                Case "PanelExpress"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,a.IdOrden,Despachado, Color,Prioridad, IdComandaTemporal,NumComanda from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden on a.IdOrden=tb_Orden.IdOrden where a.IdOrden<>0 and Express=1 and NumComanda=" & _NumComanda & "  and EstadoComanda=1  " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,IdComandaTemporal,NumComanda order by Despachado asc, Prioridad desc,IdComandaTemporal asc"
            End Select

            dt.Clear()
            dtColores.Clear()
            cFunciones.Llenar_Tabla_Generico(Consulta, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
            NumComanda = dt.Rows(0).Item("NumComanda")
            CantDetalleMax = dt.Rows.Count

            If CantDetalleMax > 10 Then
                CantDetalles = CantDetalleMax + 2
                CantDetalles = CantDetalles * 25
                y = 28
                AnchoTextbox = 280
                txtnumMesa.Left = 18
            Else
                CantDetalles = Max
                txtnumMesa.Left = x
            End If


            txtnumMesa.Width = AnchoTextbox
            pnDetalle.Controls.Clear()
            pnDetalle.Controls.Add(txtnumMesa)
            If dt.Rows.Count > 0 Then
                For ii = 0 To dt.Rows.Count - 1

                    If Not unicos.Contains(dt.Rows(ii).Item("IdComandaTemporal")) Then
                        unicos.Add(dt.Rows(ii).Item("IdComandaTemporal"))

                        Dim aButton As New TextBox
                        Dim Entregado As Integer = 0
                        aButton.Top = y
                        aButton.Left = x
                        aButton.Width = AnchoTextbox
                        aButton.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                        Select Case QuienMeLlama
                            Case "PanelExpress"
                                If dt.Rows(ii).Item("Despachado") = "True" Then
                                    Entregado = 1
                                    aButton.BackColor = Color.PaleGreen
                                Else
                                    aButton.BackColor = Color.FromName(Convert.ToString(dt.Rows(ii).Item("Color")).Replace("Color [", "").Replace("]", ""))
                                End If
                                aButton.ReadOnly = True
                                AddHandler aButton.DoubleClick, AddressOf Me.Urgente
                            Case "ListoParaEntregar"

                                If dt.Rows(ii).Item("Entregado") = "True" Then
                                    Entregado = 1
                                    aButton.BackColor = Color.PaleGreen
                                Else
                                    aButton.BackColor = Color.FromName(Convert.ToString(dt.Rows(ii).Item("Color")).Replace("Color [", "").Replace("]", ""))
                                End If
                                AddHandler aButton.KeyDown, AddressOf Me.Navegacion
                                AddHandler aButton.KeyPress, AddressOf Me.EvitarEscritura
                            Case "PanelExpressListas"

                                If dt.Rows(ii).Item("Entregado") = "True" Then
                                    Entregado = 1
                                    aButton.BackColor = Color.PaleGreen
                                Else
                                    aButton.BackColor = Color.FromName(Convert.ToString(dt.Rows(ii).Item("Color")).Replace("Color [", "").Replace("]", ""))
                                End If

                                AddHandler aButton.KeyDown, AddressOf Me.Navegacion
                                AddHandler aButton.KeyPress, AddressOf Me.EvitarEscritura
                            Case "Comanda"
                                If dt.Rows(ii).Item("Despachado") = "True" Then
                                    Entregado = 1
                                    aButton.BackColor = Color.PaleGreen
                                Else
                                    aButton.BackColor = Color.FromName(Convert.ToString(dt.Rows(ii).Item("Color")).Replace("Color [", "").Replace("]", ""))
                                End If
                                aButton.ReadOnly = True
                                AddHandler aButton.DoubleClick, AddressOf Me.Urgente
                        End Select

                        aButton.Name = "lb_" & dt.Rows(ii).Item("IdComandaTemporal")

                        If dt.Rows(ii).Item("Prioridad") = "True" Then
                            aButton.BackColor = Color.Red
                            aButton.ForeColor = Color.White
                        Else
                            If Entregado <> 1 Then
                                aButton.BackColor = Color.FromName(Convert.ToString(dt.Rows(ii).Item("Color")).Replace("Color [", "").Replace("]", ""))
                            End If
                        End If

                        If Convert.ToInt32(dt.Rows(ii).Item("TotalOrden")) < 0 Then
                            aButton.Text = "*      " & dt.Rows(ii).Item("Descripcion")
                        Else

                            aButton.Text = dt.Rows(ii).Item("TotalOrden") & " - " & dt.Rows(ii).Item("Descripcion")
                        End If

                        Me.ToolTip1.SetToolTip(aButton, dt.Rows(ii).Item("Descripcion"))
                        aButton.TabIndex = 1 + ii

                        pnDetalle.Controls.Add(aButton)

                        y = y + 25

                    End If
                Next


                atxt = New TextBox
                atxt.Top = CantDetalles - 23
                atxt.Left = x
                atxt.Width = AnchoTextbox
                atxt.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                atxt.BackColor = Color.LightSkyBlue
                atxt.TextAlign = HorizontalAlignment.Center
                AddHandler atxt.Click, AddressOf Me.Desactivar
                pnDetalle.Controls.Add(atxt)

            End If

                txtIngreso.TabIndex = dt.Rows.Count + 1

        Catch ex As Exception
            MsgBox("Error al cargar detalles. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Urgente(sender As Object, e As EventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)
            Dim Sql As String = ""
            Dim dt As New DataTable
            Dim IdAfectado As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Prioridad from tb_DetalleOrden with (Nolock) where IdComandaTemporal=" & IdComanda(datos) & "", dt)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Prioridad") = "False" Then
                    Sql = "Update tb_DetalleOrden set Prioridad=1 OUTPUT INSERTED.Id where EstadoComanda=1 and IdComandaTemporal=" & IdComanda(datos) & " "
                Else
                    Sql = "Update tb_DetalleOrden set Prioridad=0 OUTPUT INSERTED.Id where EstadoComanda=1 and IdComandaTemporal=" & IdComanda(datos) & " "
                End If
            End If

            Try
                IdAfectado = cx.SlqExecuteScalar(cx.Conectar("Restaurante"), Sql)
            Catch ex As Exception
                IdAfectado = 0
            End Try

            If IdAfectado > 0 Then
                Dim dtModificadores As New DataTable
                Dim Sumar As Integer = 1
                cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_DetalleOrden with (Nolock) where Id=" & IdAfectado & "", dtModificadores)
                If dtModificadores.Rows(0).Item("Cantidad") <> -1 Then
Repetir:
                    dtModificadores.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select Cantidad from tb_detalleOrden with (Nolock) where Id=" & IdAfectado + Sumar & "", dtModificadores)
                    If dtModificadores.Rows.Count > 0 Then
                        If dtModificadores.Rows(0).Item("Cantidad") < 0 Then
                            If dt.Rows(0).Item("Prioridad") = "False" Then
                                Sql = "Update tb_DetalleOrden set Prioridad=1 where Id=" & IdAfectado + Sumar & ""
                            Else
                                Sql = "Update tb_DetalleOrden set Prioridad=0 where Id=" & IdAfectado + Sumar & ""

                            End If
                            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                            Sumar = Sumar + 1
                            cx.DesConectar(cx.sQlconexion)
                            GoTo Repetir
                        End If
                    End If
                End If
            End If


            Select Case DevolverEstado(Me.Name)
                Case "Comanda"
                    frm.CargarCentroComanda()
                Case "Listo"
                    frm.CargarListosParaEntrega()
                Case "ListoExpress"
                    frm.CargarComandasExpressListas()
                Case "Express"
                    frm.CargarComandasExpress()
            End Select

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Tachar_DestacharOrden(sender As Object, e As EventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            For Each control As Control In pnDetalle.Controls
                If control.Name = datos Then
                    If RevisarDespacho(control.Name) Then
                        control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                        control.BackColor = Color.White
                        AumentarCantidad(control.Name)
                    Else
                        control.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold, GraphicsUnit.Pixel)
                        control.BackColor = Color.PaleGreen
                        DisminuirCantidad(control.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            MsgBox("Error al regresar orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Function RevisarDespacho(NombreControl As String) As Boolean
        Try
            Dim _info() As String
            Dim _IdComanda As Integer = 0


            _info = NombreControl.Split("_")
            If _info.Count > 0 Then
                _IdComanda = _info(1)
            End If

            Dim dt As New DataTable
            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Entregado from tb_DetalleOrden with (Nolock) where IdComandaTemporal=" & _IdComanda & "", dt)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Entregado") = "True" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub EvitarEscritura(sender As Object, e As KeyPressEventArgs)
        e.Handled = True
    End Sub

    Public Sub Navegacion(sender As Object, e As KeyEventArgs)
        Try
            Dim datos As String
            datos = CStr(sender.name)

            If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
                Select Case e.KeyCode
                    Case Keys.Down
                        If Not Me.GetNextControl(sender, True) Is Nothing Then
                            Me.GetNextControl(sender, True).Focus()
                        End If
                    Case Keys.Up
                        If Not Me.GetNextControl(sender, False) Is Nothing Then
                            Me.GetNextControl(sender, False).Focus()
                        End If
                End Select
            End If

        Catch ex As Exception
            MsgBox("Error al leer Id del Centro de Producción. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub


    Public Sub DisminuirCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim _IdComanda As Integer = 0


            _info = info.Split("_")
            If _info.Count > 0 Then
                _IdComanda = _info(1)
            End If

            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set Entregado=1 where EstadoComanda=1 and IdComandaTemporal=" & _IdComanda & " "
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

            Select Case DevolverEstado(Me.Name)
                Case "Comanda"
                Case "Listo"
                    frm.CargarListosParaEntrega()
                Case "ListoExpress"
                    frm.CargarComandasExpressListas()
                Case "Express"
            End Select


        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub AumentarCantidad(ByVal info As String)
        Try
            Dim _info() As String
            Dim _IdComanda As Integer = 0


            _info = info.Split("_")
            If _info.Count > 0 Then
                _IdComanda = _info(1)
            End If

            Dim Sql As String = ""
            Sql = "Update tb_DetalleOrden set Entregado=0 where EstadoComanda=1 and  IdComandaTemporal=" & _IdComanda & " "
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

            Select Case DevolverEstado(Me.Name)
                Case "Comanda"
                Case "Listo"
                    frm.CargarListosParaEntrega()
                Case "ListoExpress"
                    frm.CargarComandasExpressListas()
                Case "Express"
            End Select

        Catch ex As Exception
            MsgBox("Error al disminuir orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txtnumMesa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtnumMesa.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtIngreso_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIngreso.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtnumMesa_KeyDown(sender As Object, e As KeyEventArgs) Handles txtnumMesa.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
            Select Case e.KeyCode
                Case Keys.Down
                    If Not Me.GetNextControl(sender, True) Is Nothing Then
                        Me.GetNextControl(sender, True).Select()
                    End If
            End Select
        End If
    End Sub

    Private Sub txtIngreso_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIngreso.KeyDown
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode Then
            Dim datos As String
            datos = CStr(sender.name)

            Select Case e.KeyCode
                Case Keys.Down
                    Select Case DevolverEstado(Me.Name)
                            Case "Comanda"
                            Case "Listo"
                                If ValidarMesa(Me.IdMesa) Then
                                    DesActivarComanda(Me.IdMesa)
                                    frm.CargarCentroComanda()
                                    frm.CargarListosParaEntrega()
                                End If
                            Case "ListoExpress"
                                If ValidarMesa(Me.IdMesa) Then
                                    DesActivarComanda(Me.IdMesa)
                                    frm.CargarCentroComanda()
                                    frm.CargarComandasExpressListas()
                                End If
                            Case "Express"
                        End Select


                        Case Keys.Up
                    If Not Me.GetNextControl(sender, False) Is Nothing Then
                        Me.GetNextControl(sender, False).Focus()
                    End If

            End Select

        End If
    End Sub

    Public Function DevolverEstado(ByVal NombreControl As String) As String
        Try
            Dim datos As String = NombreControl
            Dim _info() As String
            Dim Estado As String = ""

            _info = datos.Split("_")

            If _info.Count > 0 Then
                Estado = _info(3)
            End If

            Return Estado

        Catch ex As Exception

        End Try
    End Function

    Private Sub Desactivar(sender As Object, e As EventArgs)
        DesactivarComandas()
    End Sub
    Private Sub txtIngreso_Click(sender As Object, e As EventArgs) Handles txtIngreso.Click
        DesactivarComandas()

    End Sub

    Sub DesactivarComandas()
        If ValidarMesa(Me.IdMesa) Then
            Select Case DevolverEstado(Me.Name)
                Case "Comanda"
                Case "Listo"
                    DesActivarComanda(Me.IdMesa)
                    frm.CargarCentroComanda()
                    frm.CargarListosParaEntrega()
                Case "ListoExpress"
                    DesActivarComanda(Me.IdMesa)
                    frm.CargarComandasExpressListas()
                Case "Express"
            End Select
        End If
    End Sub
    Public Sub spImprimirComanda()
        Dim dt As New DataSet
        Dim fila As DataRow
        Dim deseaImprimir As Boolean

        Try
            Dim impresoraObligada As String = GetSetting("SeeSoft", "Restaurante", "obligaImpresora")
            deseaImprimir = True
            Dim frm As New frmQuestion

            Dim SinImprimir As Integer = cConexion.SlqExecuteScalar(cConexion.Conectar, "select count(*) from ComandaTemporal with (Nolock) where  cMesa = " & IdMesa & " And cCodigo=" & NumComanda & "")
            If SinImprimir > 0 Then
                deseaImprimir = True
            Else
                deseaImprimir = False
                Exit Sub
            End If


            If deseaImprimir Then
                Dim str As String = "SELECT DISTINCT dbo.SplitImpresora(Impresora) as Impresora FROM ComandaTemporal with (NoLock) WHERE  cMesa=" & IdMesa & " and cCodigo=" & NumComanda & ""
                cConexion.GetDataSet(conectadobd, str, dt, "ComandaTemporal")
                If dt.Tables("ComandaTemporal").Rows.Count > 0 Then
                    'QUITAR COMENTARIO PARA Q IMPRIMA
                    For Each fila In dt.Tables("ComandaTemporal").Rows
                        Application.DoEvents()
                        Try
                            If impresoraObligada.Equals("") Then
                                Imprime_Comanda(Verificar_Impresora_Instalada(fila("Impresora"), NumComanda), False)
                            Else
                                Imprime_Comanda(Verificar_Impresora_Instalada(impresoraObligada, NumComanda), False)
                            End If

                            If IdMesa <> 0 Then
                                General.DeleteNombre(IdMesa) 'elimina el nombre de la mesa
                            End If

                        Catch ex As Exception
                            MsgBox("La impresora que indica el menú no ha sido encontrada: [" & fila("Impresora") & "]", MsgBoxStyle.Information, "Atención...")
                            cFunciones.spEnviar_Correo(Me.Name, "spProcesarComanda linea 3122", ex.Message)
                        End Try
                    Next
                End If
            End If
        Finally
        End Try
    End Sub

    Function numeroSolicitud(ByVal cNumero As Integer) As Integer
        Try
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT ISNULL(MAX(solicitud), 0) AS UltimaSolicitud FROM ComandasActivas with (Nolock) WHERE cCodigo=" & NumComanda & " and (cMesa = " & IdMesa & ")", dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(0) + 1
            Else
                Return 0
            End If
        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "numeroSolicitud", ex.Message)
        End Try

    End Function

    Private Function Verificar_Impresora_Instalada(ByVal Impresora As String, ByVal NumeroComanda As Double) As String
        Dim PrinterInstalled As String
        Dim existe As Boolean = False
        Dim arrayImpresora1 As Array

        Try
            For Each PrinterInstalled In PrinterSettings.InstalledPrinters
                arrayImpresora1 = PrinterInstalled.ToUpper.ToString.Split("\")

                If Trim(Impresora.ToUpper) = Trim(PrinterInstalled.ToUpper) Then
                    If Not GetSetting("SeeSoft", "Restaurante", "obligaImpresora").Equals("") Then
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda) '& " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                        cConexion.UpdateRecords(conectadobd, "tb_DetalleOrden", "FueImpresa=1", "NumComanda=" & NumeroComanda)

                    Else
                        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                        cConexion.UpdateRecords(conectadobd, "tb_DetalleOrden", "FueImpresa=1", "NumComanda=" & NumeroComanda)
                    End If

                    Return Trim(PrinterInstalled.ToUpper)
                    existe = True
                    Exit For
                Else
                    If arrayImpresora1.Length > 1 Then
                        If Trim(Impresora.ToUpper) = arrayImpresora1(3) Then
                            If GetSetting("SeeSoft", "Restaurante", "obligaImpresora").Equals("") Then
                                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                                cConexion.UpdateRecords(conectadobd, "tb_DetalleOrden", "FueImpresa=1", "NumComanda=" & NumeroComanda)
                            Else
                                cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Trim(PrinterInstalled.ToUpper) & "'", "cCodigo=" & NumeroComanda) '& " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
                                cConexion.UpdateRecords(conectadobd, "tb_DetalleOrden", "FueImpresa=1", "NumComanda=" & NumeroComanda)
                            End If

                            Return Trim(PrinterInstalled.ToUpper)
                            Exit For
                        End If
                    End If
                End If
            Next
22064020:
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            cFunciones.spEnviar_Correo(Me.Name, "Verificar_Impresora_Instalada", ex.Message)
        End Try

        MessageBox.Show("No se encontró la impresora correspondiente: [" & Impresora & "]", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        Dim Impr As String = Busca_Impresora()
        If Impr = "" Then
            MessageBox.Show("No se seleccionó ninguna impresora, la comanda será eliminada", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

            Return ""
            Exit Function
        End If

        cConexion.UpdateRecords(conectadobd, "ComandaTemporal", "Impresora='" & Impr & "'", "cCodigo=" & NumeroComanda & " and dbo.splitImpresora(Impresora) ='" & Impresora & "'")
        cConexion.UpdateRecords(conectadobd, "tb_DetalleOrden", "FueImpresa=1", "NumComanda=" & NumeroComanda)

        Return Impr
    End Function

    Private Sub Imprime_Comanda(ByVal Impresora As String, ByVal Reimpresion As Boolean)
        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("") Then SaveSetting("SeeSoft", "Restaurante", "TipoReporteComanda", "0")
        If GetSetting("SeeSoft", "Restaurante", "TipoReporteComanda").Equals("1") Then
            spReporteConDTS(Impresora, Reimpresion)
        Else
            spReporteConConexionSql(Impresora, Reimpresion)
        End If
    End Sub

    Private Sub spReporteConConexionSql(ByVal Impresora As String, ByVal Reimpresion As Boolean)
        Try
            If GetSetting("SeeSoft", "Restaurante", "NoComanda").Equals("1") Then Exit Sub

            Dim ImpresoraBar As String = ""
            If Impresora.IndexOf("BAR") > 0 Then
                ImpresoraBar = "BAR"
            End If
            If GetSetting("SeeSoft", "Restaurante", "noComanda") = "2" And ImpresoraBar.Equals("BAR", StringComparison.OrdinalIgnoreCase) Then Exit Sub

            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                rptComanda.Refresh()
                rptComanda.PrintOptions.PrinterName = Impresora
                rptComanda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                rptComanda.SetParameterValue(0, NumComanda)
                rptComanda.SetParameterValue(1, 0)
                rptComanda.SetParameterValue(2, Impresora)
                rptComanda.SetParameterValue(3, User_Log.NombrePunto)
                If Reimpresion = True Then
                    rptComanda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    rptComanda.SetParameterValue(4, "")
                End If

                rptComanda.PrintToPrinter(1, True, 0, 0)

                If Impresora Like "COCINA" Then
                    Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                    If TEM = "" Then Exit Sub
                    rptComanda.Refresh()
                    rptComanda.PrintOptions.PrinterName = TEM
                    rptComanda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComanda.SetParameterValue(0, NumComanda)
                    rptComanda.SetParameterValue(1, 0)
                    rptComanda.SetParameterValue(2, Impresora)
                    rptComanda.SetParameterValue(3, User_Log.NombrePunto)
                    If Reimpresion = True Then
                        rptComanda.SetParameterValue(4, "REIMPRESIÓN")
                    Else
                        rptComanda.SetParameterValue(4, "")
                    End If

                    rptComanda.PrintToPrinter(1, True, 0, 0)
                End If
            Else
                rptComandaIzquierda.Refresh()
                rptComandaIzquierda.PrintOptions.PrinterName = Impresora
                rptComandaIzquierda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                rptComandaIzquierda.SetParameterValue(0, NumComanda)
                rptComandaIzquierda.SetParameterValue(1, 0)
                rptComandaIzquierda.SetParameterValue(2, Impresora)
                rptComandaIzquierda.SetParameterValue(3, User_Log.NombrePunto)
                If Reimpresion = True Then
                    rptComandaIzquierda.SetParameterValue(4, "REIMPRESIÓN")
                Else
                    rptComandaIzquierda.SetParameterValue(4, "")
                End If

                rptComandaIzquierda.PrintToPrinter(1, True, 0, 0)

                If Impresora Like "COCINA" Then
                    Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                    If TEM = "" Then Exit Sub
                    rptComandaIzquierda.Refresh()
                    rptComandaIzquierda.PrintOptions.PrinterName = TEM
                    rptComandaIzquierda.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rptComandaIzquierda.SetParameterValue(0, NumComanda)
                    rptComandaIzquierda.SetParameterValue(1, 0)
                    rptComandaIzquierda.SetParameterValue(2, Impresora)
                    rptComandaIzquierda.SetParameterValue(3, User_Log.NombrePunto)
                    If Reimpresion = True Then
                        rptComandaIzquierda.SetParameterValue(4, "REIMPRESIÓN")
                    Else
                        rptComandaIzquierda.SetParameterValue(4, "")
                    End If

                    rptComandaIzquierda.PrintToPrinter(1, True, 0, 0)
                End If

            End If

        Catch ex As Exception
            cFunciones.spEnviar_Correo(Me.Name, "spReporteConConexionSql", ex.Message)
        End Try
    End Sub

    Private Sub spReporteConDTS(ByVal Impresora As String, ByVal Reimpresion As Boolean)


        Try
            Dim dtFactura As New DataTable
            Dim NumFactura As Integer = 0

            cFunciones.Llenar_Tabla_Generico("Select NumeroFactura from Comanda where NumeroComanda=" & NumComanda & "", dtFactura)
            If dtFactura.Rows.Count > 0 Then
                NumFactura = dtFactura.Rows(0).Item("NumeroFactura")
            End If

            Dim rpt As New rptComandadts
            If GetSetting("SeeSoft", "Restaurante", "Impresion") = "1" Then
                If FnCargarDatosImpresionComanda(Impresora) Then
                    rpt.Refresh()
                    rpt.SetDataSource(DtsImprimeComanda1)
                    rpt.PrintOptions.PrinterName = Impresora
                    rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rpt.SetParameterValue(0, NumComanda)
                    rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rpt.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rpt.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rpt.PrintToPrinter(1, True, 0, 0)

                    If Impresora Like "COCINA" Then
                        Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                        If TEM = "" Then Exit Sub
                        rpt.Refresh()
                        rpt.SetDataSource(DtsImprimeComanda1)
                        rpt.PrintOptions.PrinterName = TEM
                        rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        rpt.SetParameterValue(0, NumComanda)
                        rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                        If Reimpresion = True Then
                            rpt.SetParameterValue(2, "REIMPRESIÓN")
                        Else
                            rpt.SetParameterValue(2, "")
                        End If
                        Dim Peticiones1 As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                        rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                        rpt.PrintToPrinter(1, True, 0, 0)
                    End If
                End If
            Else
                If FnCargarDatosImpresionComanda(Impresora) Then
                    rpt.Refresh()
                    rpt.SetDataSource(DtsImprimeComanda1)
                    rpt.PrintOptions.PrinterName = Impresora
                    rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    rpt.SetParameterValue(0, NumComanda)
                    rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                    If Reimpresion = True Then
                        rpt.SetParameterValue(2, "REIMPRESIÓN")
                    Else
                        rpt.SetParameterValue(2, "")
                    End If
                    Dim Peticiones As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                    rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                    rpt.PrintToPrinter(1, True, 0, 0)

                    If Impresora Like "COCINA" Then
                        Dim TEM As String = GetSetting("SeeSoft", "Restaurante", "CCO")
                        If TEM = "" Then Exit Sub
                        rpt.Refresh()
                        rpt.SetDataSource(DtsImprimeComanda1)
                        rpt.PrintOptions.PrinterName = TEM
                        rpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                        rpt.SetParameterValue(0, NumComanda)
                        rpt.SetParameterValue(1, GetSetting("SeeSOFT", "Restaurante", "LastPoint"))
                        If Reimpresion = True Then
                            rpt.SetParameterValue(2, "REIMPRESIÓN")
                        Else
                            rpt.SetParameterValue(2, "")
                        End If
                        Dim Peticiones1 As String = GetSetting("SeeSoft", "Restaurante", "PeticionesMoviles")
                        rpt.SetParameterValue(3, IIf(Peticiones = 1, True, False))
                        rpt.PrintToPrinter(1, True, 0, 0)
                    End If
                End If
            End If
        Catch ex As Exception
            'ESCRIBIR LOG DE ERRRORES

            cFunciones.spEnviar_Correo(Me.Name, "spReporteConDTS", ex.Message)
        End Try

    End Sub

    Private Function FnCargarDatosImpresionComanda(ByVal _Impresora As String) As Boolean
        Dim conn As New SqlClient.SqlConnection
        Dim sql As New SqlClient.SqlCommand
        Dim adap As New SqlClient.SqlDataAdapter(sql)

        Try
            DtsImprimeComanda1 = ComandaMenu.DtsImprimeComanda1
            DtsImprimeComanda1.Tb_ComandaTemporal.Clear()
            DtsImprimeComanda1.Tb_Mesa.Clear()

            conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            conn.Open()
            sql.Connection = conn
            sql.CommandText = "SELECT d.codproducto as cProducto ,'' as Habitacion,0 as Impreso,'Activo' as Estado,  d.Cantidad as cCantidad ,  d.Descripcion as cDescripcion ,
                               0 as primero, d.Express,0 as Solicitud 
                              ,d.IdComandaTemporal as Id FROM dbo.tb_DetalleOrden as d with (Nolock) 
                              where d.EstadoComanda = 1 and d.NumComanda=@NumComanda and (d.CodMesa = @Mesa)  order by d.IdComandaTemporal asc  "

            sql.Parameters.AddWithValue("@Mesa", IdMesa)
            sql.Parameters.AddWithValue("@NumComanda", NumComanda)
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_ComandaTemporal)

            If IdMesa <> 0 Then
                sql.CommandText = "SELECT  distinct  top 1 dbo.Mesas.Nombre_Mesa , dbo.Grupos_Mesas.Nombre_Grupo,dbo.Usuarios.Nombre , CT.Id" &
                              " FROM dbo.ComandaTemporal CT with (Nolock) INNER JOIN dbo.Usuarios ON CT.Cedula = dbo.Usuarios.Cedula INNER JOIN " &
                              " dbo.Mesas ON CT.cMesa = dbo.Mesas.Id INNER JOIN dbo.Grupos_Mesas ON dbo.Mesas.Id_GrupoMesa = dbo.Grupos_Mesas.id " &
                              " where (UPPER(CT.Impresora) = UPPER(@PImpresora)) and (dbo.Mesas.Id = @Mesa) order by CT.Id desc"
                sql.Parameters.AddWithValue("@PImpresora", _Impresora)
            Else
                sql.CommandText = "SELECT    distinct  top 1 'EXPRESS' as Nombre_Mesa , 'EXPRESS' as Nombre_Grupo,dbo.Usuarios.Nombre , CT.Id
                               FROM dbo.ComandaTemporal CT with (Nolock) INNER JOIN dbo.Usuarios ON CT.Cedula = dbo.Usuarios.Cedula left outer join
							   dbo.tb_DetalleOrden on CT.id=dbo.tb_DetalleOrden.IdComandaTemporal
                              where tb_DetalleOrden.EstadoComanda=1 and (CT.cMesa = 0) and tb_DetalleOrden.NumComanda=@NumComanda order by CT.Id desc"

            End If
            adap.SelectCommand = sql
            adap.Fill(DtsImprimeComanda1.Tb_Mesa)
            conn.Close()
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            My.Computer.FileSystem.WriteAllText("C:\Test.txt", "-ERR [" & Format(Now, "dd/MM/yy HH:mm:ss") & "]:" & ex.Message, True)
            cFunciones.spEnviar_Correo(Me.Name, "FnCargarDatosImpresionComanda", ex.Message)
            Return False
        End Try
    End Function

    Public Sub ValidarEstadoOrden() 'Valida si toda la orden ya se completo
        Try
            Dim dt As New DataTable
            Dim dtEstado As New DataTable
            Dim Estado As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select IdOrden from tb_DetalleOrden with (Nolock) where  EstadoComanda=0 ", dt)
            If dt.Rows.Count > 0 Then
                For p As Integer = 0 To dt.Rows.Count - 1

                    dtEstado.Clear()
                    cFunciones.Llenar_Tabla_Generico("Select EstadoComanda from tb_DetalleOrden with (Nolock) where IdOrden=" & dt.Rows(p).Item("IdOrden") & "", dtEstado)

                    If dtEstado.Rows.Count > 0 Then
                        For a As Integer = 0 To dtEstado.Rows.Count - 1
                            If dtEstado.Rows(a).Item("EstadoComanda") = "True" Then
                                Estado = 1
                                Exit For
                            End If
                        Next

                        If Estado = 0 Then
                            Dim Sql As String = ""
                            Sql = "Update tb_Orden set Estado=0  where IdOrden=" & dt.Rows(p).Item("IdOrden") & " "
                            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                            cx.DesConectar(cx.sQlconexion)
                        End If
                    End If

                Next
            End If
        Catch ex As Exception
            MsgBox("Error el validar estado de la orden. " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Public Sub DesActivarComanda(ByVal Mesa As Integer)
        Try
            Dim Sql As String = ""
            Dim dtComandasEntregadas As New DataTable
            Sql = "Update tb_DetalleOrden set Estado=0 ,EstadoComanda=0 where Entregado=1 and  CodMesa=" & Mesa & " "
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)

            cFunciones.Llenar_Tabla_Generico("Select IdComandaTemporal from tb_DetalleOrden with (nolock) where Express=1 and Entregado=1 and CodMesa=" & Mesa & "", dtComandasEntregadas)
            If dtComandasEntregadas.Rows.Count > 0 Then
                For k As Integer = 0 To dtComandasEntregadas.Rows.Count - 1
                    Sql = "Update ComandaTemporal set Estado='Inactivo' where id=" & dtComandasEntregadas.Rows(k).ItemArray(0) & " "
                    cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
                    cx.DesConectar(cx.sQlconexion)
                Next
            End If
            ValidarEstadoOrden()
        Catch ex As Exception
            MsgBox("Error al quitar comanda del centro de comandas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Function ValidarMesa(ByVal Mesa As Integer) As Boolean
        Try

            Dim ii As Integer
            Dim dt As New DataTable
            Dim TodoListo As Integer = 0

            dt.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Entregado from tb_DetalleOrden with (Nolock) where Entregado=1 and codMesa=" & Mesa & "", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then
                For ii = 0 To dt.Rows.Count - 1

                    If dt.Rows(ii).Item("Entregado") = "False" Then
                        TodoListo = 1
                    End If
                Next

                If TodoListo = 1 Then
                    Return False
                Else
                    Return True
                End If

            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Error al validar orden. " & ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    Private Sub tmTiempoTranscurrido_Tick(sender As Object, e As EventArgs) Handles tmTiempoTranscurrido.Tick

        tmTiempoTranscurrido.Interval = 10

        Milisegundos += 1

        If Milisegundos = "60" Then
            Segundos += 1
            Milisegundos = 0
        End If

        If Segundos = 60 Then
            Minutos += 1
            Segundos = 0
        End If

        If Minutos = 60 Then

            Hora += 1
            Milisegundos = 0

        End If
        atxt.Text = Hora + ":" + Minutos + ":" + Segundos
    End Sub

    Public Sub GuardarTiempoTranscurrido()

        Dim Sql As String = ""
        If Not txtIngreso.Text.Trim().Equals("") Then
            Sql = "Update tb_Orden set TiempoTranscurrido='" & txtIngreso.Text & "' where IdOrden=" & IdOrden & ""
            cx.SlqExecute(cx.Conectar("Restaurante"), Sql)
            cx.DesConectar(cx.sQlconexion)
        End If
        tmTiempoTranscurrido.Stop()
    End Sub

    Private Sub txtnumMesa_Click(sender As Object, e As EventArgs) Handles txtnumMesa.Click
        spImprimirComanda()

        Select Case DevolverEstado(Me.Name)
            Case "Listo"
                frm.CargarListosParaEntrega()
            Case "ListoExpress"
                frm.CargarComandasExpressListas()
        End Select
    End Sub
End Class
