﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ucCentroComandas
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pnDetalle = New System.Windows.Forms.Panel()
        Me.txtIngreso = New System.Windows.Forms.TextBox()
        Me.txtnumMesa = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tmTiempoTranscurrido = New System.Windows.Forms.Timer(Me.components)
        Me.pnDetalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnDetalle
        '
        Me.pnDetalle.AutoScroll = True
        Me.pnDetalle.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.pnDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnDetalle.Controls.Add(Me.txtIngreso)
        Me.pnDetalle.Controls.Add(Me.txtnumMesa)
        Me.pnDetalle.Location = New System.Drawing.Point(2, 3)
        Me.pnDetalle.Name = "pnDetalle"
        Me.pnDetalle.Size = New System.Drawing.Size(321, 156)
        Me.pnDetalle.TabIndex = 2
        '
        'txtIngreso
        '
        Me.txtIngreso.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.txtIngreso.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txtIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIngreso.Location = New System.Drawing.Point(5, 132)
        Me.txtIngreso.Name = "txtIngreso"
        Me.txtIngreso.Size = New System.Drawing.Size(310, 20)
        Me.txtIngreso.TabIndex = 1
        Me.txtIngreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnumMesa
        '
        Me.txtnumMesa.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnumMesa.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txtnumMesa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnumMesa.Location = New System.Drawing.Point(4, 2)
        Me.txtnumMesa.Name = "txtnumMesa"
        Me.txtnumMesa.Size = New System.Drawing.Size(310, 20)
        Me.txtnumMesa.TabIndex = 0
        Me.txtnumMesa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tmTiempoTranscurrido
        '
        '
        'ucCentroComandas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.DimGray
        Me.Controls.Add(Me.pnDetalle)
        Me.Name = "ucCentroComandas"
        Me.Size = New System.Drawing.Size(324, 161)
        Me.pnDetalle.ResumeLayout(False)
        Me.pnDetalle.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents pnDetalle As Panel
    Friend WithEvents txtIngreso As TextBox
    Friend WithEvents txtnumMesa As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents tmTiempoTranscurrido As Timer
End Class
