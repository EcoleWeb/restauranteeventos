﻿Imports System.ComponentModel

Public Class frmCentroComandas

    Dim aButton As ucCentroComandas
    Dim TiempoInicial As Date
    Dim TiempoEspera As Double
    Dim Cargar As String = ""
    Dim Consulta As String = ""
    Dim CantDetalles As Integer = 0
    Dim NoHay As Integer = 0
    Dim FaltaEntregar As Integer = 0

    Private Sub CentroComandas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.WindowState = FormWindowState.Maximized

            Cargar = "Todos"
            rbTodos.Checked = True
            TiempoInicial = Now
            Me.Timer1.Enabled = True
            Me.tmHora.Start()
            txtActualizar.Text = "30"
            TiempoEspera = Convert.ToInt32(txtActualizar.Text)
            Me.ToolTip1.SetToolTip(btLimpiarComandas, "Limpia las comandas del día anterior.")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

#Region "Funciones"

    Public Function CalcularColumas(ByVal panel As Panel) As Integer
        Try
            Dim AnchoPanel As Double = 0
            Dim AnchoControl As Double = 0
            Dim Columnas As Integer = 0
            aButton = New ucCentroComandas
            AnchoPanel = panel.Size.Width
            AnchoControl = aButton.Size.Width

            Columnas = Math.Floor((AnchoPanel / AnchoControl))
            Return Columnas

        Catch ex As Exception
            Return 6
        End Try
    End Function


    Function procesarTiempo(ByVal tiempo As String) As String
        If tiempo.Equals("") Then
            Return ""
        End If
        Return tiempo.Substring(tiempo.Length - 13, 5)
    End Function
#End Region
#Region "Carga de paneles"

    Public Sub CargarPaneles()
        CargarComandasExpress()
        CargarComandasExpressListas()
        CargarCentroComanda()
        CargarListosParaEntrega()
    End Sub

    Public Function _Filtro(ByVal _Cargar As String) As String
        Try
            Dim Filtro As String = ""
            Dim dtBebidas As New DataTable
            Dim dtCocina As New DataTable

            dtBebidas.Clear()
            dtCocina.Clear()

            Select Case _Cargar
                Case "Bebidas"
                    cFunciones.Llenar_Tabla_Generico("Select Id from tb_CentroProduccion with (Nolock) where Tipo='Bebidas'", dtBebidas)
                    If dtBebidas.Rows.Count > 0 Then
                        For l As Integer = 0 To dtBebidas.Rows.Count - 1
                            If l = dtBebidas.Rows.Count - 1 Then
                                Filtro += " a.IdCentroProduccion=" & dtBebidas.Rows(l).Item("Id") & ""
                            Else
                                Filtro += " a.IdCentroProduccion=" & dtBebidas.Rows(l).Item("Id") & " or"
                            End If
                        Next
                        Consulta = " and (" & Filtro & ")"
                    Else
                        Exit Function
                    End If
                Case "Platos"
                    cFunciones.Llenar_Tabla_Generico("Select Id from tb_CentroProduccion with (Nolock) where Tipo='Cocina'", dtCocina)
                    If dtCocina.Rows.Count > 0 Then
                        For l As Integer = 0 To dtCocina.Rows.Count - 1
                            If l = dtCocina.Rows.Count - 1 Then
                                Filtro += " a.IdCentroProduccion=" & dtCocina.Rows(l).Item("Id") & ""
                            Else
                                Filtro += " a.IdCentroProduccion=" & dtCocina.Rows(l).Item("Id") & " or"
                            End If
                        Next
                        Consulta = " and (" & Filtro & ")"
                    Else
                        Exit Function
                    End If

                Case "Todos"
                    Consulta = ""
            End Select

            Return Consulta
        Catch ex As Exception
            Consulta = ""
            Return Consulta
        End Try
    End Function

    Public Sub CargarCentroComanda()
        Try

            Dim i, ii, n As Integer
            Dim y As Integer = 26
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas(panelCentroComandas)  'Modificar si se desan mas columnas por fila
            Dim Orden As Integer = 0

            For Each control As ucCentroComandas In panelCentroComandas.Controls
                control.Dispose()
            Next

            Dim num_controles As Int32 = Me.panelCentroComandas.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.panelCentroComandas.Controls(z)
                Me.panelCentroComandas.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            panelCentroComandas.Controls.Clear()


            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList

            cFunciones.Llenar_Tabla_Generico("select CodMesa,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden a with (Nolock) right outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden 
            where  a.Estado=1 and Express=0 " & _Filtro(Cargar) & " order by Prioridad desc,NumOrden asc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                        unicos.Add(dt.Rows(k).Item("codMesa"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next

                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc("Comanda")

                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles("Comanda", Cargar, Convert.ToInt32(dt.Rows(0).Item("codMesa"))) Then

                            aButton = New ucCentroComandas
                            aButton.Top = y
                            aButton.Left = x
                            aButton.Width = 326
                            aButton.Height = LargoUc + 10
                            aButton.pnDetalle.Height = LargoUc + 3
                            aButton.Max = LargoUc
                            aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_Comanda_" & Orden
                            aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                            aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                            aButton.IdMesa = dt.Rows(0).Item("codMesa")
                            aButton.QuienMeLlama = "Comanda"
                            aButton.Detalles = Cargar
                            aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                            aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                            aButton.frm = Me
                            aButton.NameMesa = "Mesa_" & dt.Rows(0).Item("NombreMesa") & "_" & dt.Rows(0).Item("IdOrden")
                            Orden = Orden + 1

                            If i = 0 And ii = 0 Then
                                aButton.Index = 1
                            End If

                            If FaltaEntregar = 1 Then
                                Me.panelCentroComandas.Controls.Add(aButton)
                            End If

                        End If

                        If FaltaEntregar = 1 Then
                            x = x + 328
                            n = n + 1
                        End If
                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    If FaltaEntregar = 1 Then
                        y = y + LargoUc + 11
                        x = 3
                    End If

                Next
            Else
                If NoHay <> 1 Then

                    NoHay = 1
                End If
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al cargar centro de comandas. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub CargarListosParaEntrega()
        Try
            Dim i, ii, n As Integer
            Dim y As Integer = 26
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas(panelListosEntregar) '3 'Modificar si se desan mas columnas por fila
            Dim Orden As Integer = 0

            Dim num_controles As Int32 = Me.panelListosEntregar.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.panelListosEntregar.Controls(z)
                Me.panelListosEntregar.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next


            panelListosEntregar.Controls.Clear()

            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList

            cFunciones.Llenar_Tabla_Generico("select  CodMesa,IdComandaTemporal from dbo.tb_DetalleOrden with (Nolock) 
            where  EstadoComanda=1 and ParaEntregar=1 and Express=0 order by FueImpresa asc, IdComandaTemporal desc  ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                        unicos.Add(dt.Rows(k).Item("codMesa"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count


                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next


                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc("ListoParaEntregar")

                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles("ListoParaEntregar", Cargar, Convert.ToInt32(dt.Rows(0).Item("codMesa"))) Then
                            aButton = New ucCentroComandas
                            aButton.Top = y
                            aButton.Left = x
                            aButton.Width = 326
                            aButton.Height = LargoUc + 10
                            aButton.pnDetalle.Height = LargoUc + 3
                            aButton.Max = LargoUc
                            aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_Listo_" & Orden
                            aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                            aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                            aButton.IdMesa = dt.Rows(0).Item("codMesa")
                            aButton.QuienMeLlama = "ListoParaEntregar"
                            aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                            aButton.Detalles = Cargar
                            aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                            aButton.frm = Me
                            aButton.NameMesa = "Mesa_" & dt.Rows(0).Item("NombreMesa") & "_" & dt.Rows(0).Item("IdOrden")
                            Orden = Orden + 1

                            If i = 0 And ii = 0 Then
                                aButton.Index = 1
                            End If

                            If FaltaEntregar = 0 Then
                                Me.panelListosEntregar.Controls.Add(aButton)
                            End If

                        End If

                        If FaltaEntregar = 0 Then
                            x = x + 328
                            n = n + 1
                        End If

                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    If FaltaEntregar = 0 Then
                        y = y + LargoUc + 11
                        x = 3
                    End If
                Next
            Else
                ' MsgBox("No hay ordenes para entregar.", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al cargar panel de listos para entregar. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub CargarComandasExpressListas()
        Try
            Dim i, ii, n As Integer
            Dim y As Integer = 26
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas(panelListoExpress) '3 'Modificar si se desan mas columnas por fila
            Dim Orden As Integer = 0

            Dim num_controles As Int32 = Me.panelListoExpress.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.panelListoExpress.Controls(z)
                Me.panelListoExpress.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            panelListoExpress.Controls.Clear()

            ' panelListoExpress.Controls.Add(Label7)


            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList

            cFunciones.Llenar_Tabla_Generico("select  CodMesa,IdComandaTemporal, NumComanda from dbo.tb_DetalleOrden with (Nolock) 
            where  EstadoComanda=1 and Express=1 and ParaEntregar=1 order by FueImpresa asc, IdComandaTemporal desc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("NumComanda")) Then
                        unicos.Add(dt.Rows(k).Item("NumComanda"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next


                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc("PanelExpressListas")

                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color, NumComanda from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles("PanelExpressListas", Cargar, Convert.ToInt32(dt.Rows(0).Item("NumComanda"))) Then

                            aButton = New ucCentroComandas
                            aButton.Top = y
                            aButton.Left = x
                            aButton.Width = 326
                            aButton.Height = LargoUc + 10
                            aButton.pnDetalle.Height = LargoUc + 3
                            aButton.Max = LargoUc
                            aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_ListoExpress_" & Orden
                            If dt.Rows(0).Item("CodMesa") = 0 Then
                                aButton.NumMesa = "Mesa EXPRESS"
                            Else
                                aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                            End If

                            aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                            aButton.IdMesa = dt.Rows(0).Item("codMesa")
                            aButton._MiNumComanda = dt.Rows(0).Item("NumComanda")
                            aButton.QuienMeLlama = "PanelExpressListas"
                            aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                            aButton.Detalles = Cargar
                            aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                            aButton.frm = Me
                            aButton.NameMesa = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden")
                            Orden = Orden + 1

                            If i = 0 And ii = 0 Then
                                aButton.Index = 1
                            End If

                            If FaltaEntregar = 0 Then
                                Me.panelListoExpress.Controls.Add(aButton)
                            End If

                        End If

                        If FaltaEntregar = 0 Then
                            x = x + 328
                            n = n + 1
                        End If

                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    If FaltaEntregar = 0 Then
                        y = y + LargoUc + 11
                        x = 3
                    End If
                Next
            Else
                ' MsgBox("No hay ordenes para entregar del tipo Express.", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al cargar panel de listos express." & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub CargarComandasExpress()
        Try
            Dim i, ii, n As Integer
            Dim y As Integer = 26
            Dim x As Integer = 3
            Dim Filas As Integer = 0
            Dim Columnas As Integer = CalcularColumas(panelLlevarExpress) '3 'Modificar si se desan mas columnas por fila
            Dim Orden As Integer = 0

            Dim num_controles As Int32 = Me.panelLlevarExpress.Controls.Count - 1
            For z As Integer = num_controles To 0 Step -1
                Dim ctrl As Windows.Forms.Control = Me.panelLlevarExpress.Controls(z)
                Me.panelLlevarExpress.Controls.Remove(ctrl)
                ctrl.Dispose()
            Next

            panelLlevarExpress.Controls.Clear()

            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList


            cFunciones.Llenar_Tabla_Generico("select  CodMesa,IdComandaTemporal, NumComanda from dbo.tb_DetalleOrden with (Nolock) 
            where Despachado=0 and EstadoComanda=1 and Express=1 order by IdComandaTemporal desc", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                For k As Integer = 0 To dt.Rows.Count - 1
                    If Not unicos.Contains(dt.Rows(k).Item("NumComanda")) Then
                        unicos.Add(dt.Rows(k).Item("NumComanda"))
                        UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                    End If
                Next

                Dim _Filas As Integer = UnicosId.Count

                For b As Integer = 0 To _Filas 'Calcula cuantas filas son necesarias para mostrar toda la info
                    If _Filas >= 1 Then
                        _Filas = _Filas - Columnas 'modificar para aumentar o disminuir la cantidad a mostrar por filas, 1 = mostrar uno por fila
                        Filas += 1
                    Else
                        If b = 0 Then
                            Filas = 1
                        End If
                    End If
                Next


                Dim quitar As Integer = 0
                Dim recorrer As Integer = Columnas - 1 'Cuantas columnas deseas 0 = 1 columna, 1 = 2 columnas, etc
                Dim LargoUc As Integer = CalcularLargoUc("PanelExpress")
                If Filas = 1 Then
                    recorrer = UnicosId.Count - 1
                End If

                For i = 0 To Filas - 1
                    For ii = 0 To recorrer

                        quitar = ii

                        dt.Clear()
                        cFunciones.Llenar_Tabla_Generico("select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,TiempoTranscurrido,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color, NumComanda from dbo.tb_DetalleOrden with (Nolock)  left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal=" & UnicosId(ii) & " ", dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles("PanelExpress", Cargar, Convert.ToInt32(dt.Rows(0).Item("NumComanda"))) Then
                            aButton = New ucCentroComandas
                            aButton.Top = y
                            aButton.Left = x
                            aButton.Width = 326
                            aButton.Height = LargoUc + 10
                            aButton.pnDetalle.Height = LargoUc + 3
                            aButton.Max = LargoUc
                                aButton.Name = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden") & "_Express_" & Orden
                            If dt.Rows(0).Item("CodMesa") = 0 Then
                                aButton.NumMesa = "Mesa EXPRESS"
                            Else
                                aButton.NumMesa = "Mesa #" & dt.Rows(0).Item("NombreMesa")
                            End If

                            aButton.Tomado = dt.Rows(0).Item("TiempoTranscurrido")
                            aButton.IdMesa = dt.Rows(0).Item("codMesa")
                            aButton._MiNumComanda = dt.Rows(0).Item("NumComanda")
                            aButton.QuienMeLlama = "PanelExpress"
                            aButton.IdOrden = dt.Rows(0).Item("IdOrden")
                            aButton.Detalles = Cargar
                            aButton.IdCentro = dt.Rows(0).Item("IdCentroProduccion")
                            aButton.frm = Me
                            aButton.NameMesa = "Mesa_" & dt.Rows(0).Item("CodMesa") & "_" & dt.Rows(0).Item("IdOrden")
                            Orden = Orden + 1

                            If i = 0 And ii = 0 Then
                                aButton.Index = 1
                            End If

                            If FaltaEntregar = 1 Then
                                Me.panelLlevarExpress.Controls.Add(aButton)
                            End If

                        End If

                        If FaltaEntregar = 1 Then
                            x = x + 328
                            n = n + 1
                        End If

                    Next

                    If UnicosId.Count > 0 Then
                        For g As Integer = 0 To quitar
                            Dim a As Integer = 0
                            UnicosId.RemoveAt(a)
                        Next
                    End If

                    If UnicosId.Count <= Columnas Then
                        recorrer = UnicosId.Count - 1
                    End If

                    If FaltaEntregar = 1 Then
                        y = y + LargoUc + 11
                        x = 3
                    End If
                Next
            Else
                'MsgBox("No hay ordenes para entregar.", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error al cargar panel de express." & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Public Function CalcularLargoUc(ByVal QuienLlama As String) As Integer
        Try
            Dim dt As New DataTable
            Dim unicos As New ArrayList
            Dim UnicosId As New ArrayList
            Dim Max As Integer = 0
            Dim sqlGrupo As String = ""
            Dim SqlDetalle As String = ""
            dt.Clear()

            Select Case QuienLlama
                Case "Comanda"
                    sqlGrupo = "select  CodMesa,IdComandaTemporal,NumOrden from dbo.tb_DetalleOrden a with (Nolock) right outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden 
            where  a.Estado=1 and Express=0 " & _Filtro(Cargar) & " order by NumOrden asc"
                    SqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock) left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
                Case "ListoParaEntregar"
                    sqlGrupo = "select  CodMesa,IdComandaTemporal from dbo.tb_DetalleOrden Nolock 
            where  EstadoComanda=1 and ParaEntregar=1 and Express=0 order by IdComandaTemporal desc"
                    SqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,CodMesa,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock)  left outer join
                        tb_Orden on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
                Case "PanelExpressListas"
                    sqlGrupo = "select  CodMesa,IdComandaTemporal,NumComanda from dbo.tb_DetalleOrden  
            where  EstadoComanda=1 and Express=1 and ParaEntregar=1 order by IdComandaTemporal desc"
                    SqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,NumComanda,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock)  left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
                Case "PanelExpress"
                    sqlGrupo = "select  CodMesa,IdComandaTemporal,NumComanda from dbo.tb_DetalleOrden  with (Nolock)
            where Despachado=0 and EstadoComanda=1 and Express=1 order by IdComandaTemporal desc"
                    SqlDetalle = "select IdComandaTemporal,tb_DetalleOrden.IdOrden,NombreMesa,FechaIngreso,NumComanda,dbo.tb_detalleOrden.IdCentroProduccion, ParaEntregar,Color from dbo.tb_DetalleOrden with (Nolock)  left outer join
                        tb_Orden  on tb_DetalleOrden.IdOrden=tb_Orden.IdOrden where tb_detalleOrden.IdComandaTemporal="
            End Select

            cFunciones.Llenar_Tabla_Generico(sqlGrupo, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

            If dt.Rows.Count > 0 Then

                If QuienLlama = "PanelExpressListas" Or QuienLlama = "PanelExpress" Then
                    For k As Integer = 0 To dt.Rows.Count - 1
                        If Not unicos.Contains(dt.Rows(k).Item("NumComanda")) Then
                            unicos.Add(dt.Rows(k).Item("NumComanda"))
                            UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                        End If
                    Next

                Else
                    For k As Integer = 0 To dt.Rows.Count - 1
                        If Not unicos.Contains(dt.Rows(k).Item("codMesa")) Then
                            unicos.Add(dt.Rows(k).Item("codMesa"))
                            UnicosId.Add(dt.Rows(k).Item("IdComandaTemporal"))
                        End If
                    Next

                End If

                Dim _Filas As Integer = UnicosId.Count

                dt.Clear()
                If QuienLlama = "PanelExpressListas" Or QuienLlama = "PanelExpress" Then
                    For o As Integer = 0 To UnicosId.Count - 1
                        cFunciones.Llenar_Tabla_Generico(SqlDetalle & UnicosId(o), dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles(QuienLlama, Cargar, Convert.ToInt32(dt.Rows(0).Item("NumComanda"))) Then
                            If Max < CantDetalles Then
                                If CantDetalles > 10 Then
                                    Max = 10
                                Else
                                    Max = CantDetalles
                                End If

                            End If
                        End If

                    Next

                Else
                    For o As Integer = 0 To UnicosId.Count - 1
                        cFunciones.Llenar_Tabla_Generico(SqlDetalle & UnicosId(o), dt, GetSetting("Seesoft", "Restaurante", "Conexion"))

                        If VerificarDetalles(QuienLlama, Cargar, Convert.ToInt32(dt.Rows(0).Item("CodMesa"))) Then
                            If Max < CantDetalles Then
                                If CantDetalles > 10 Then
                                    Max = 10
                                Else
                                    Max = CantDetalles
                                End If

                            End If
                        End If

                    Next

                End If


            End If

            Max = Max + 2
            Return Max * 25 '25 es el ancho del textbox que contiene las descripciones de las comandas en los user control

        Catch ex As Exception
            Return 125
        End Try
    End Function

    Public Function VerificarDetalles(ByVal _QuienLlama As String, ByVal _MiFiltro As String, ByVal Mesa As Integer) As Boolean
        Try
            Dim dt As New DataTable
            Dim Filtro As String = _Filtro(_MiFiltro)
            FaltaEntregar = 0

            Select Case _QuienLlama
                Case "ListoParaEntregar"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,Entregado,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden  where Express=0 and CodMesa=" & Mesa & " and EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,Entregado,IdComandaTemporal,Entregado order by Prioridad desc,IdComandaTemporal asc"
                Case "PanelExpressListas"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,Entregado,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden where Express=1 and NumComanda=" & Mesa & " and  EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,Entregado,IdComandaTemporal,Entregado order by Prioridad desc,IdComandaTemporal asc"
                Case "Comanda"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,Entregado,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden where Express=0 and CodMesa=" & Mesa & "  and EstadoComanda=1 " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,IdComandaTemporal,Entregado  order by Prioridad desc,IdComandaTemporal asc"
                Case "PanelExpress"
                    Consulta = "Select sum(Cantidad) as TotalOrden,Descripcion,Entregado,a.IdOrden,Despachado, Color,Prioridad,Entregado, IdComandaTemporal from vs_OrdenOrdenada a with (Nolock) left outer join tb_Orden  on a.IdOrden=tb_Orden.IdOrden where Express=1 and NumComanda=" & Mesa & "  and EstadoComanda=1  " & Filtro & " group by CodProducto,Descripcion,Despachado,a.IdOrden,Color,Prioridad,IdComandaTemporal,Entregado  order by Prioridad desc,IdComandaTemporal asc"
            End Select

            dt.Clear()

            cFunciones.Llenar_Tabla_Generico(Consulta, dt, GetSetting("Seesoft", "Restaurante", "Conexion"))
            Dim MiUnicos As New ArrayList

            For v As Integer = 0 To dt.Rows.Count - 1
                If Not dt.Rows(v).Item("Entregado").ToString().Equals("True") Then
                    FaltaEntregar = 1
                End If

                If Not MiUnicos.Contains(dt.Rows(v).Item("IdComandaTemporal")) Then
                    MiUnicos.Add(dt.Rows(v).Item("IdComandaTemporal"))
                End If
            Next



            If dt.Rows.Count > 0 Then
                CantDetalles = MiUnicos.Count
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        RefrescarComandas()
    End Sub

    Private Sub RefrescarComandas()
        Try
            If DateDiff(DateInterval.Second, TiempoInicial, Now) <= TiempoEspera Then
                lbTimer.Text = TiempoEspera - DateDiff(DateInterval.Second, TiempoInicial, Now) + 1
            Else
                If lbTimer.Text <> "Actualizando..." Then
                    TiempoInicial = Now
                    lbTimer.Text = "Actualizando..."
                    'GuardarTiempoTranscurrido()
                    CargarPaneles()
                ElseIf DateDiff(DateInterval.Second, TiempoInicial, Now) >= TiempoEspera Then
                    TiempoInicial = Now
                End If
            End If
        Catch ex As Exception
            Dim e As String = ex.Message
        End Try
    End Sub

    Sub GuardarTiempoTranscurrido()
        Try
            For Each control As ucCentroComandas In panelCentroComandas.Controls
                control.GuardarTiempoTranscurrido()
            Next
            For Each control As ucCentroComandas In panelLlevarExpress.Controls
                control.GuardarTiempoTranscurrido()
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub frmCentroComandas_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Timer1.Enabled = False
        Me.tmHora.Stop()
    End Sub

    Private Sub rbBebidas_CheckedChanged(sender As Object, e As EventArgs) Handles rbBebidas.CheckedChanged
        If rbBebidas.Checked Then
            Cargar = "Bebidas"
            CargarPaneles()
        End If

    End Sub

    Private Sub rbPlatos_CheckedChanged(sender As Object, e As EventArgs) Handles rbPlatos.CheckedChanged
        If rbPlatos.Checked Then
            Cargar = "Platos"
            CargarPaneles()
        End If

    End Sub

    Private Sub rbTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rbTodos.CheckedChanged
        If rbTodos.Checked Then
            Cargar = "Todos"
            CargarPaneles()
        End If

    End Sub

    Private Sub txtActualizar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtActualizar.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub

    Private Sub txtActualizar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtActualizar.KeyUp
        If e.KeyCode = Keys.Enter Then
            Dim Tiempo As Integer = 0
            If txtActualizar.Text.Trim() = "" Then
                Tiempo = 1
            Else
                Tiempo = Convert.ToInt32(txtActualizar.Text)
            End If
            TiempoEspera = Tiempo
        End If
    End Sub

    Private Sub tmHora_Tick(sender As Object, e As EventArgs) Handles tmHora.Tick
        lbHora.Text = DateTime.Now.ToString("hh:mm:ssss")
    End Sub

    Private Sub btLimpiarComandas_Click(sender As Object, e As EventArgs) Handles btLimpiarComandas.Click
        LimpiarComandasAntiguas()
        CargarPaneles()
    End Sub

    Sub LimpiarComandasAntiguas()
        Try
            Dim FechaHoy As New DateTime
            Dim DtOrden As New DataTable
            Dim conn As New SqlClient.SqlConnection
            Dim sql As New SqlClient.SqlCommand
            Dim adap As New SqlClient.SqlDataAdapter(sql)
            Dim IdOrden As Integer = 0

            FechaHoy = Today
            conn.ConnectionString = GetSetting("SeeSoft", "Restaurante", "Conexion")
            conn.Open()
            sql.Connection = conn
            sql.CommandText = "Select IdOrden from tb_Orden with (Nolock) where Fechaingreso<@FechaHoy"
            sql.Parameters.AddWithValue("@FechaHoy", FechaHoy)
            adap.SelectCommand = sql
            adap.Fill(DtOrden)

            If DtOrden.Rows.Count > 0 Then
                For g As Integer = 0 To DtOrden.Rows.Count - 1
                    IdOrden = DtOrden.Rows(g).Item("IdOrden")
                    sql.CommandText = "Delete tb_Orden where IdOrden=" & IdOrden & ""
                    cFunciones.spEjecutar(sql, conn.ConnectionString)
                    sql.CommandText = "Delete tb_DetalleOrden where IdOrden=" & IdOrden & ""
                    cFunciones.spEjecutar(sql, conn.ConnectionString)
                Next
            End If

        Catch ex As Exception
            MsgBox("No se pudo limpiar las comandas del día anterior. " & ex.Message)
        End Try
    End Sub

#End Region


End Class