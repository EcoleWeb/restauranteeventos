<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Prompt_Categoria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Prompt_Categoria))
        Me.txtDato = New System.Windows.Forms.TextBox()
        Me.txtMEsas = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.txtRecetas = New System.Windows.Forms.TextBox()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Grupo_Bodega = New System.Windows.Forms.GroupBox()
        Me.cmbxBodegas = New System.Windows.Forms.ComboBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.comboGrupos = New System.Windows.Forms.ComboBox()
        Me.ListItems = New System.Windows.Forms.ListBox()
        Me.ButtonAgregados = New System.Windows.Forms.Button()
        Me.PanelCategoria = New System.Windows.Forms.Panel()
        Me.lbDescripcionCostos = New System.Windows.Forms.Label()
        Me.lbDescripcionIngresos = New System.Windows.Forms.Label()
        Me.txtCuentaCostos = New System.Windows.Forms.TextBox()
        Me.txtCuentaIngresos = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbActividadEconomica = New System.Windows.Forms.ComboBox()
        Me.lbIdActividad = New System.Windows.Forms.Label()
        Me.lbActividadEconomica = New System.Windows.Forms.Label()
        Me.GbCabys = New System.Windows.Forms.GroupBox()
        Me.txtDescripcionCabys = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btBuscarCabys = New System.Windows.Forms.Button()
        Me.lbPorcImpuesto = New System.Windows.Forms.Label()
        Me.txtCodigoCabys = New System.Windows.Forms.TextBox()
        Me.lbIdCabys = New System.Windows.Forms.Label()
        Me.RadioButtonAcompanamiento = New System.Windows.Forms.RadioButton()
        Me.RadioButtonModificadores = New System.Windows.Forms.RadioButton()
        Me.GroupBoxPrecio = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBoxPrecio = New System.Windows.Forms.TextBox()
        Me.PanelAgregado = New System.Windows.Forms.Panel()
        Me.ButtonEscogerModificador = New System.Windows.Forms.Button()
        Me.ButtonEscogerACO = New System.Windows.Forms.Button()
        Me.ButtonTeclado = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Grupo_Bodega.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.PanelCategoria.SuspendLayout()
        Me.GbCabys.SuspendLayout()
        Me.GroupBoxPrecio.SuspendLayout()
        Me.PanelAgregado.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDato
        '
        Me.txtDato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtDato, "txtDato")
        Me.txtDato.Name = "txtDato"
        '
        'txtMEsas
        '
        Me.txtMEsas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtMEsas, "txtMEsas")
        Me.txtMEsas.Name = "txtMEsas"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'btnBuscar
        '
        resources.ApplyResources(Me.btnBuscar, "btnBuscar")
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFileDialog1"
        '
        'txtRecetas
        '
        Me.txtRecetas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtRecetas, "txtRecetas")
        Me.txtRecetas.Name = "txtRecetas"
        Me.txtRecetas.ReadOnly = True
        '
        'txtId
        '
        resources.ApplyResources(Me.txtId, "txtId")
        Me.txtId.Name = "txtId"
        '
        'Grupo_Bodega
        '
        Me.Grupo_Bodega.Controls.Add(Me.cmbxBodegas)
        Me.Grupo_Bodega.Controls.Add(Me.ListBox1)
        resources.ApplyResources(Me.Grupo_Bodega, "Grupo_Bodega")
        Me.Grupo_Bodega.Name = "Grupo_Bodega"
        Me.Grupo_Bodega.TabStop = False
        '
        'cmbxBodegas
        '
        Me.cmbxBodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cmbxBodegas, "cmbxBodegas")
        Me.cmbxBodegas.FormattingEnabled = True
        Me.cmbxBodegas.Name = "cmbxBodegas"
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        resources.ApplyResources(Me.ListBox1, "ListBox1")
        Me.ListBox1.Name = "ListBox1"
        '
        'TextBox1
        '
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        '
        'TextBox2
        '
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextBox2, "TextBox2")
        Me.TextBox2.Name = "TextBox2"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.comboGrupos)
        Me.GroupBox1.Controls.Add(Me.ListItems)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'comboGrupos
        '
        Me.comboGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.comboGrupos, "comboGrupos")
        Me.comboGrupos.FormattingEnabled = True
        Me.comboGrupos.Name = "comboGrupos"
        '
        'ListItems
        '
        Me.ListItems.FormattingEnabled = True
        resources.ApplyResources(Me.ListItems, "ListItems")
        Me.ListItems.Name = "ListItems"
        '
        'ButtonAgregados
        '
        resources.ApplyResources(Me.ButtonAgregados, "ButtonAgregados")
        Me.ButtonAgregados.Name = "ButtonAgregados"
        Me.ButtonAgregados.UseVisualStyleBackColor = True
        '
        'PanelCategoria
        '
        Me.PanelCategoria.Controls.Add(Me.lbDescripcionCostos)
        Me.PanelCategoria.Controls.Add(Me.lbDescripcionIngresos)
        Me.PanelCategoria.Controls.Add(Me.txtCuentaCostos)
        Me.PanelCategoria.Controls.Add(Me.txtCuentaIngresos)
        Me.PanelCategoria.Controls.Add(Me.Label4)
        Me.PanelCategoria.Controls.Add(Me.Label3)
        Me.PanelCategoria.Controls.Add(Me.cbActividadEconomica)
        Me.PanelCategoria.Controls.Add(Me.lbIdActividad)
        Me.PanelCategoria.Controls.Add(Me.lbActividadEconomica)
        Me.PanelCategoria.Controls.Add(Me.GbCabys)
        Me.PanelCategoria.Controls.Add(Me.ButtonAgregados)
        Me.PanelCategoria.Controls.Add(Me.GroupBox1)
        resources.ApplyResources(Me.PanelCategoria, "PanelCategoria")
        Me.PanelCategoria.Name = "PanelCategoria"
        '
        'lbDescripcionCostos
        '
        resources.ApplyResources(Me.lbDescripcionCostos, "lbDescripcionCostos")
        Me.lbDescripcionCostos.Name = "lbDescripcionCostos"
        '
        'lbDescripcionIngresos
        '
        resources.ApplyResources(Me.lbDescripcionIngresos, "lbDescripcionIngresos")
        Me.lbDescripcionIngresos.Name = "lbDescripcionIngresos"
        '
        'txtCuentaCostos
        '
        resources.ApplyResources(Me.txtCuentaCostos, "txtCuentaCostos")
        Me.txtCuentaCostos.Name = "txtCuentaCostos"
        '
        'txtCuentaIngresos
        '
        resources.ApplyResources(Me.txtCuentaIngresos, "txtCuentaIngresos")
        Me.txtCuentaIngresos.Name = "txtCuentaIngresos"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'cbActividadEconomica
        '
        Me.cbActividadEconomica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbActividadEconomica.FormattingEnabled = True
        resources.ApplyResources(Me.cbActividadEconomica, "cbActividadEconomica")
        Me.cbActividadEconomica.Name = "cbActividadEconomica"
        '
        'lbIdActividad
        '
        resources.ApplyResources(Me.lbIdActividad, "lbIdActividad")
        Me.lbIdActividad.Name = "lbIdActividad"
        '
        'lbActividadEconomica
        '
        resources.ApplyResources(Me.lbActividadEconomica, "lbActividadEconomica")
        Me.lbActividadEconomica.Name = "lbActividadEconomica"
        '
        'GbCabys
        '
        Me.GbCabys.Controls.Add(Me.txtDescripcionCabys)
        Me.GbCabys.Controls.Add(Me.Label1)
        Me.GbCabys.Controls.Add(Me.btBuscarCabys)
        Me.GbCabys.Controls.Add(Me.lbPorcImpuesto)
        Me.GbCabys.Controls.Add(Me.txtCodigoCabys)
        Me.GbCabys.Controls.Add(Me.lbIdCabys)
        resources.ApplyResources(Me.GbCabys, "GbCabys")
        Me.GbCabys.Name = "GbCabys"
        Me.GbCabys.TabStop = False
        '
        'txtDescripcionCabys
        '
        resources.ApplyResources(Me.txtDescripcionCabys, "txtDescripcionCabys")
        Me.txtDescripcionCabys.Name = "txtDescripcionCabys"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'btBuscarCabys
        '
        resources.ApplyResources(Me.btBuscarCabys, "btBuscarCabys")
        Me.btBuscarCabys.Name = "btBuscarCabys"
        Me.btBuscarCabys.UseVisualStyleBackColor = True
        '
        'lbPorcImpuesto
        '
        resources.ApplyResources(Me.lbPorcImpuesto, "lbPorcImpuesto")
        Me.lbPorcImpuesto.Name = "lbPorcImpuesto"
        '
        'txtCodigoCabys
        '
        resources.ApplyResources(Me.txtCodigoCabys, "txtCodigoCabys")
        Me.txtCodigoCabys.Name = "txtCodigoCabys"
        '
        'lbIdCabys
        '
        resources.ApplyResources(Me.lbIdCabys, "lbIdCabys")
        Me.lbIdCabys.Name = "lbIdCabys"
        '
        'RadioButtonAcompanamiento
        '
        resources.ApplyResources(Me.RadioButtonAcompanamiento, "RadioButtonAcompanamiento")
        Me.RadioButtonAcompanamiento.Name = "RadioButtonAcompanamiento"
        Me.RadioButtonAcompanamiento.UseVisualStyleBackColor = True
        '
        'RadioButtonModificadores
        '
        resources.ApplyResources(Me.RadioButtonModificadores, "RadioButtonModificadores")
        Me.RadioButtonModificadores.Checked = True
        Me.RadioButtonModificadores.Name = "RadioButtonModificadores"
        Me.RadioButtonModificadores.TabStop = True
        Me.RadioButtonModificadores.UseVisualStyleBackColor = True
        '
        'GroupBoxPrecio
        '
        Me.GroupBoxPrecio.Controls.Add(Me.Button1)
        Me.GroupBoxPrecio.Controls.Add(Me.TextBoxPrecio)
        resources.ApplyResources(Me.GroupBoxPrecio, "GroupBoxPrecio")
        Me.GroupBoxPrecio.Name = "GroupBoxPrecio"
        Me.GroupBoxPrecio.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TextBoxPrecio
        '
        resources.ApplyResources(Me.TextBoxPrecio, "TextBoxPrecio")
        Me.TextBoxPrecio.Name = "TextBoxPrecio"
        '
        'PanelAgregado
        '
        Me.PanelAgregado.Controls.Add(Me.GroupBoxPrecio)
        Me.PanelAgregado.Controls.Add(Me.ButtonEscogerModificador)
        Me.PanelAgregado.Controls.Add(Me.ButtonEscogerACO)
        Me.PanelAgregado.Controls.Add(Me.RadioButtonModificadores)
        Me.PanelAgregado.Controls.Add(Me.RadioButtonAcompanamiento)
        resources.ApplyResources(Me.PanelAgregado, "PanelAgregado")
        Me.PanelAgregado.Name = "PanelAgregado"
        '
        'ButtonEscogerModificador
        '
        resources.ApplyResources(Me.ButtonEscogerModificador, "ButtonEscogerModificador")
        Me.ButtonEscogerModificador.Image = Global.SIALIBEB.My.Resources.Resources.Select2
        Me.ButtonEscogerModificador.Name = "ButtonEscogerModificador"
        Me.ButtonEscogerModificador.UseVisualStyleBackColor = True
        '
        'ButtonEscogerACO
        '
        resources.ApplyResources(Me.ButtonEscogerACO, "ButtonEscogerACO")
        Me.ButtonEscogerACO.Image = Global.SIALIBEB.My.Resources.Resources.Select2
        Me.ButtonEscogerACO.Name = "ButtonEscogerACO"
        Me.ButtonEscogerACO.UseVisualStyleBackColor = True
        '
        'ButtonTeclado
        '
        Me.ButtonTeclado.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.ButtonTeclado, "ButtonTeclado")
        Me.ButtonTeclado.Image = Global.SIALIBEB.My.Resources.Resources.teclado1
        Me.ButtonTeclado.Name = "ButtonTeclado"
        Me.ButtonTeclado.UseVisualStyleBackColor = False
        '
        'btnBorrar
        '
        Me.btnBorrar.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
        resources.ApplyResources(Me.btnBorrar, "btnBorrar")
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Image = Global.SIALIBEB.My.Resources.Resources.greenshd
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        resources.ApplyResources(Me.btnCancelar, "btnCancelar")
        Me.btnCancelar.Image = Global.SIALIBEB.My.Resources.Resources.redshd
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Prompt_Categoria
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ButtonTeclado)
        Me.Controls.Add(Me.Grupo_Bodega)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.txtRecetas)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtMEsas)
        Me.Controls.Add(Me.txtDato)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.PanelCategoria)
        Me.Controls.Add(Me.PanelAgregado)
        Me.Name = "Prompt_Categoria"
        Me.Grupo_Bodega.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.PanelCategoria.ResumeLayout(False)
        Me.PanelCategoria.PerformLayout()
        Me.GbCabys.ResumeLayout(False)
        Me.GbCabys.PerformLayout()
        Me.GroupBoxPrecio.ResumeLayout(False)
        Me.GroupBoxPrecio.PerformLayout()
        Me.PanelAgregado.ResumeLayout(False)
        Me.PanelAgregado.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtDato As System.Windows.Forms.TextBox
    Friend WithEvents txtMEsas As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents txtRecetas As System.Windows.Forms.TextBox
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents Grupo_Bodega As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxBodegas As System.Windows.Forms.ComboBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ButtonTeclado As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents comboGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents ListItems As System.Windows.Forms.ListBox
    Friend WithEvents ButtonAgregados As System.Windows.Forms.Button
    Friend WithEvents PanelCategoria As System.Windows.Forms.Panel
    Friend WithEvents RadioButtonAcompanamiento As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonModificadores As System.Windows.Forms.RadioButton
    Friend WithEvents ButtonEscogerACO As System.Windows.Forms.Button
    Friend WithEvents ButtonEscogerModificador As System.Windows.Forms.Button
    Friend WithEvents GroupBoxPrecio As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBoxPrecio As System.Windows.Forms.TextBox
    Friend WithEvents PanelAgregado As System.Windows.Forms.Panel
    Friend WithEvents btBuscarCabys As Button
    Friend WithEvents txtCodigoCabys As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lbIdCabys As Label
    Friend WithEvents lbPorcImpuesto As Label
    Friend WithEvents txtDescripcionCabys As TextBox
    Friend WithEvents GbCabys As GroupBox
    Friend WithEvents lbActividadEconomica As Label
    Friend WithEvents cbActividadEconomica As ComboBox
    Friend WithEvents lbIdActividad As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Public WithEvents lbDescripcionCostos As Label
    Public WithEvents lbDescripcionIngresos As Label
    Public WithEvents txtCuentaCostos As TextBox
    Public WithEvents txtCuentaIngresos As TextBox
End Class
