Imports System.Data.SqlClient

Public Class Clienteexpress
#Region "Variables"
    Public id, nombre, cedula, telefono, Direccion As String
    Dim cConexion As New ConexionR
    Dim conectadobd As New SqlConnection
    Dim DataS As New DataSet
    Dim DataV As DataView
    Public desdedescuento As Boolean
    Public montodescuento As Double

#End Region

    Private Sub ClienteCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conectadobd = cConexion.Conectar("Restaurante")
        Dim str As String

        Me.TituloModulo.Text = "CLIENTES EXPRESS"
        str = "SELECT Id,  Nombre, Telefono,Direccion from ClienteExpress"

        cConexion.GetDataSet(conectadobd, str, DataS, "ClienteExpress")
        DataV = New DataView(DataS.Tables("ClienteExpress"))
        Me.DataGridView1.DataSource = DataV
        Me.DataGridView1.Visible = True
        Me.DataGridView1.Columns(0).Visible = False
        Me.DataGridView1.Columns(1).Width = 350
        Me.DataGridView1.Columns(2).Width = 90
        Me.DataGridView1.Columns(3).Visible = False

        DataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically
    End Sub

    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick
        Select Case ToolBar2.Buttons.IndexOf(e.Button) + 1
            Case 4 : cargar()
            Case 7 : teclado()
            Case 8 : Close()

        End Select
    End Sub

    Private Sub cargar()
        id = DataGridView1(0, DataGridView1.CurrentRow.Index).Value()
        nombre = DataGridView1(1, DataGridView1.CurrentRow.Index).Value()
        telefono = DataGridView1(2, DataGridView1.CurrentRow.Index).Value()
        Direccion = DataGridView1(3, DataGridView1.CurrentRow.Index).Value()
        Close()
    End Sub


    Private Sub teclado()
        Try
            Call Shell("C:\WINDOWS\system32\osk")
        Catch ex As Exception
            MessageBox.Show("Error al abrir el teclado en pantalla, No se encuentra la ubicación", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        cargar()
    End Sub

    Private Sub txtnombre_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtnombre.KeyUp
        If Trim(txtnombre.Text).Length >= 0 Then

            DataV.RowFilter = "nombre Like '%" & txtnombre.Text & "%'"
        ElseIf Trim(txtnombre.Text).Length = 0 Then
            DataV = New DataView(DataS.Tables("Cliente"))
        End If
        DataGridView1.DataSource = DataV ' .Table("Cuentas") 'customerOrders.Tables("Listado")
    End Sub

End Class