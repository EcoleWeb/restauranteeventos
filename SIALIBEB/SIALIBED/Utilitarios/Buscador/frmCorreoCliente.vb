﻿Namespace Utilitarios.Buscador
    Public Class FrmCorreoCliente


        Public IdCliente As String = "0"
        Private Sub frmCorreoCliente_Load(sender As Object, e As EventArgs) Handles Me.Load
            spIniciarForm()
        End Sub

        Private Sub SpIniciarForm()
            Try

                Dim dt As New DataTable
                cFunciones.Llenar_Tabla_Generico("select Email from Cliente where Id = '" & idCliente & "'", dt, GetSetting("SeeSOFT", "Hotel", "CONEXION"))

                If dt.Rows.Count > 0 Then

                    txtCorreo.Text = dt.Rows(0).Item(0)
                Else
                    txtCorreo.Text = ""
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub

        Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
            Try
                If idCliente <> 0 Then
                    Dim sql As New SqlClient.SqlCommand
                    sql.CommandText = "update cliente set Email = '" & txtCorreo.Text & "' where id = '" & idCliente & "'"
                    cFunciones.spEjecutar(sql, GetSetting("SeeSOFT", "Hotel", "CONEXION"))
                End If
                DialogResult = DialogResult.OK
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub
    End Class
End Namespace