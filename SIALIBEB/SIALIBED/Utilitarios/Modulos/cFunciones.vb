Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Net.Mail
Imports System.Threading

Public Class cFunciones
    Public Shared Descripcion As String

    Public Shared Function cadenaConexion() As String
        Return GetSetting("seesoft", "Hotel", "conexion")
    End Function

    Public Shared Sub guardarDatosConParametros(ByVal consulta As SqlCommand)
        Dim enlace As New SqlConnection(cadenaConexion)
        enlace.Open()
        consulta.Connection = enlace
        consulta.ExecuteNonQuery()
        enlace.Close()

    End Sub

    Public Shared Sub spEjecutar(ByVal _sqlcommand As SqlClient.SqlCommand, ByVal _conexion As String)
        Try
            Try

                _sqlcommand.Connection = New SqlClient.SqlConnection(_conexion)
                _sqlcommand.Connection.Open()
                _sqlcommand.ExecuteNonQuery()
                _sqlcommand.Connection.Close()
            Catch ex As SqlClient.SqlException
                MsgBox(ex.Message)
            End Try

        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub CargarTablaSqlCommand(ByVal SQLCommand As SqlCommand, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEGURIDAD", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Try
            ConexionX.Open()

            SQLCommand.Connection = ConexionX
            SQLCommand.CommandType = CommandType.Text
            SQLCommand.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = SQLCommand
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            Exit Sub
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Sub

    Public Shared Function ObtenerIvaProducto(ByVal CodProducto As Integer) As Integer 'OBTIENE EL IMPUESTO DE LA TABLA CABYS RELACIONADO CON LA CATEGORIA DEL PRODUCTO
        Try
            Dim cConexion As New Conexion
            Dim sqlConexion As New SqlConnection
            Dim PorcIva As Integer = 0
            sqlConexion = cConexion.Conectar("Restaurante")
            PorcIva = cConexion.SlqExecuteScalar(sqlConexion, "Select Hotel.dbo.Cabys.PorcentajeIVA from Menu_Restaurante left outer join Categorias_Menu on Menu_Restaurante.Id_Categoria=Categorias_Menu.Id left outer join  hotel.dbo.Cabys on Categorias_Menu.IdCabys=hotel.dbo.Cabys.Id where Id_Menu=" & CodProducto & "")
            cConexion.DesConectar(sqlConexion)
            Return PorcIva
        Catch ex As Exception
            Return 13
        End Try
    End Function
    Public Shared Sub spCargarDatos(ByVal _sqlcommand As SqlClient.SqlCommand, ByRef _dt As Data.DataTable, Optional ByVal _conexion As String = "", Optional ByVal msg As Boolean = True)
        Try
            Try
                Dim sConn As String = IIf(_conexion = "", GetSetting("SeeSOFT", "Restaurante", "CONEXION"), _conexion)
                _sqlcommand.Connection = New SqlClient.SqlConnection(sConn)
                _sqlcommand.CommandTimeout = 90
                Dim aaaa As String = _sqlcommand.CommandTimeout
                Dim Da As New SqlClient.SqlDataAdapter
                Da.SelectCommand = _sqlcommand
                _dt.Clear()
                Da.Fill(_dt)
                _sqlcommand.Connection.Close()
            Catch ex As SqlClient.SqlException
                If msg Then MsgBox(ex.Message)
            End Try
        Catch ex As Exception
            If msg Then MsgBox(ex.Message, 2)

        End Try
    End Sub

    Public Function Saldo_de_Factura(ByVal FacturaNo As Double, ByVal MontoFactura As Double, Optional ByVal strModulo As String = "HOTEL") As Double
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        sqlConexion = cConexion.Conectar(strModulo)
        Saldo_de_Factura = cConexion.SlqExecuteScalar(sqlConexion, "Select  dbo.SaldoFactura(GETDATE(), " & FacturaNo & ")")
        cConexion.DesConectar(sqlConexion)
    End Function

    Public Shared Function Llenar_Tabla_SL(ByVal SQLCommand As String, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String

        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEEPOS", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Comando As SqlCommand = New SqlCommand
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = Comando
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Function
    Public Function BuscarDatos(ByVal strConsulta As String, ByVal Campo As String, Optional ByVal nombre As String = "Buscar...", Optional ByVal NuevaConexionStr As String = "") As String
        Dim frmBuscar As New Buscar
        Dim codigo As String
        frmBuscar.sqlstring = strConsulta
        frmBuscar.Text = nombre
        frmBuscar.campo = Campo
        frmBuscar.NuevaConexion = NuevaConexionStr
        frmBuscar.ShowDialog()
        codigo = frmBuscar.codigo
        Descripcion = frmBuscar.descrip
        Return codigo
    End Function

    Public Function Buscar_X_Descripcion_Fecha(ByVal SQLString As String, ByVal CampoFiltro As String, ByVal CampoFechaFiltro As String, Optional ByVal NombreBuscador As String = "Buscar...", Optional ByVal NuevaConexion As String = "") As String
        Dim frmBuscar As New FrmBuscador
        'Dim codigo As String
        frmBuscar.SQLString = SQLString
        frmBuscar.Text = NombreBuscador
        frmBuscar.CampoFiltro = CampoFiltro
        frmBuscar.CampoFecha = CampoFechaFiltro
        frmBuscar.NuevaConexion = NuevaConexion
        frmBuscar.ShowDialog()
        If frmBuscar.Cancelado Then
            Return Nothing
        Else
            Return frmBuscar.Codigo
        End If
    End Function

    'Public Function Buscar_X_Descripcion_Fecha1(ByVal SQLString As String, ByVal CampoFiltro As String, ByVal strNumeroD As String, ByVal CampoFechaFiltro As String, Optional ByVal NombreBuscador As String = "Buscar...", Optional ByVal NuevaConexion As String = "") As String
    '    Dim frmBuscar As New FrmBuscador
    '    Dim codigo As String
    '    frmBuscar.SQLString = SQLString
    '    frmBuscar.Text = NombreBuscador
    '    'Asigno la primer columna por la buscare
    '    frmBuscar.CampoFiltro = CampoFiltro
    '    'Asigno el otro la otra columna por la que buscare
    '    frmBuscar.strNumeroDocumento = strNumeroD
    '    frmBuscar.CampoFecha = CampoFechaFiltro
    '    frmBuscar.NuevaConexion = NuevaConexion
    '    frmBuscar.ShowDialog()
    '    If frmBuscar.Cancelado Then
    '        Return Nothing
    '    Else
    '        Return frmBuscar.Codigo
    '    End If
    'End Function

    Public Function Buscar_X_Descripcion_Fecha5C(ByVal SQLString As String, ByVal CampoFiltro As String, ByVal CampoFechaFiltro As String, Optional ByVal NombreBuscador As String = "Buscar...") As String
        'BUSCADOR DISE�ADO PARA CINCO COLUMNAS
        Dim frmBuscar As New FrmBuscador5C2
        'Dim codigo As String
        frmBuscar.SQLString = SQLString
        frmBuscar.Text = NombreBuscador
        frmBuscar.CampoFiltro = CampoFiltro
        frmBuscar.CampoFecha = CampoFechaFiltro
        frmBuscar.ShowDialog()
        If frmBuscar.Cancelado Then
            Return Nothing
        Else
            Return frmBuscar.Codigo
        End If
    End Function

    'Esta Funci�n Calcula el saldo de la factura
    Public Function Saldo_de_Factura(ByVal FacturaNo As Double, ByVal MontoFactura As Double, ByVal TipoCambFact As Double, ByVal TipoCambRecibo As Double, Optional ByVal strModulo As String = "Restaurante") As Double
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        sqlConexion = cConexion.Conectar(strModulo)
        Saldo_de_Factura = cConexion.SlqExecuteScalar(sqlConexion, "Select  dbo.SaldoFactura(GETDATE(), " & FacturaNo & ")") * (TipoCambFact / TipoCambRecibo)
        cConexion.DesConectar(sqlConexion)
    End Function

    Public Shared Function Saldo_de_Factura_Proveedor(ByVal FacturaNo As Double, ByVal MontoFactura As Double, ByVal TipoCambFact As Double, ByVal TipoCambRecibo As Double, ByVal Proveedor As Double, Optional ByVal strModulo As String = "") As Double
        Dim cConexion As New Conexion
        If FacturaNo = 0 Then Exit Function
        Saldo_de_Factura_Proveedor = (cConexion.SlqExecuteScalar(cConexion.Conectar(strModulo), "Select dbo.SaldoFacturaCompra(GETDATE(), " & FacturaNo & "," & Proveedor & ")") * TipoCambFact / TipoCambRecibo)
        cConexion.DesConectar(cConexion.sQlconexion)
    End Function

    Public Shared Function BuscarFacturas(ByVal CodigoCliente As Integer) As DataTable
        Dim cnn As SqlConnection = Nothing
        Dim dt As New DataTable
        ' Dentro de un Try/Catch por si se produce un error
        Try
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("Hotel", "Hotel", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = _
                "SELECT Id, Num_Factura as Factura, Fecha, Total, Cod_Moneda, Tipo_Cambio from Ventas WHERE Tipo = 'CRE'  and Anulado = 0 and Cod_Cliente = @Codigo AND (dbo.SaldoFactura(GETDATE(), ID) > 0)"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.Int))
            cmd.Parameters("@Codigo").Value = CodigoCliente
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla

            da.Fill(dt)

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
        ' Devolvemos el objeto DataTable con los datos de la consulta
        Return dt
    End Function

    Public Shared Function BuscarFacturas_Proveedor(ByVal CodigoProv As Integer, Optional ByVal Conexion As String = "") As DataTable
        Dim cnn As SqlConnection = Nothing
        Dim dt As New DataTable
        ' Dentro de un Try/Catch por si se produce un error
        Try
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = IIf(Conexion = "", GetSetting("SeeSOFT", "Restaurante", "CONEXION"), Conexion)
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = _
            "SELECT Factura, Fecha, TotalFactura, Cod_MonedaCompra FROM compras WHERE (FacturaCancelado = 0) AND (TipoCompra = 'CRE') AND (CodigoProv = " & CodigoProv & ") AND (dbo.SaldoFacturaCompra(GETDATE(), Factura, CodigoProv) <> 0)"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.Int))
            cmd.Parameters("@Codigo").Value = CodigoProv
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla

            da.Fill(dt)

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try

        Return dt ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    Public Shared Function Cargar_Tabla_Generico(ByRef DataAdapter As SqlDataAdapter, ByVal SQLCommand As String, Optional ByVal NuevaConexionStr As String = "") As DataTable
        Dim StringConexion As String = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "Restaurante", "CONEXION"), NuevaConexionStr) 'JCGA 19032007
        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Tabla As New DataTable
        Dim Comando As SqlCommand = New SqlCommand
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            DataAdapter.SelectCommand = Comando
            DataAdapter.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.ToString) ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
        Return Tabla ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    Public Shared Function Llenar_Tabla_Generico(ByVal SQLCommand As String, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "Restaurante", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Comando As SqlCommand = New SqlCommand
        ConexionX.Close()
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = Comando
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            ConexionX.Close()
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Function

    Public Shared Function BuscaString(ByVal strQuery As String, ByVal Conected As String, Optional ByVal strCampo As String = "") As String 'JCGA 12042007
        Dim strDatos As String = ""

        Try
            'Creo el Objeto de Conexion para realizar la consulta
            Dim myConnection As New SqlConnection(Conected)
            'Creo el Objeto Command con el que realizare la consulta
            Dim myCommand As New SqlCommand(strQuery, myConnection)

            myConnection.Open()

            'Objeto reader que utilizare en para la consulta
            Dim myReader As SqlDataReader

            'Asigno los datos de la consulta
            myReader = myCommand.ExecuteReader()
            If strCampo = "" Then

                If myReader.Read Then
                    strDatos = myReader.GetString(0)
                End If
            End If

            If strCampo <> "" Then
                If myReader.Read Then
                    strDatos = myReader(strCampo)
                End If
            End If

            myReader.Close()

            'Cierro la conexion
            myConnection.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al mostrar datos", _
                                     MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return strDatos
    End Function

    Public Function VerificarBaseDatos(ByVal BaseDatos As String) As Boolean
        Dim Cx As New Conexion
        Dim Resultado As String
        Try
            Resultado = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT Name FROM MASTER.DBO.sysdatabases WHERE name = N'" & BaseDatos & "'")
            Cx.DesConectar(Cx.sQlconexion)
            If Resultado = Nothing Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al mostrar datos", _
                                                MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Public Function BuscaNumeroAsiento(ByVal InicioAsiento As String) As String
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim Numero As String
        Dim Max As String = "0"
        Dim Ceros, Length As Integer

        Try
            'BUSCA LOS NUMEROS DE ASIENTOS EXISTENTES PARA EL A�O Y MES ESTABLECIDOS
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT NumAsiento from AsientosContables Where NumAsiento Like '" & InicioAsiento & "%'")

            While rs.Read
                Numero = rs("NumAsiento").Substring(9)  'SELECCIONA SOLO EL NUMERO DE CONSECUTIVO DEL ASIENTO SIN EL A�O Y MES
                If CInt(Max) < CInt(Numero) Then        'VERIFICA SI EL NUMERO QUE ESTA LEYENDO ES EL MAYOR
                    Max = Numero                        'DE SER MAYOR SE LO ASIGNA AL NUMERO MAX
                End If
            End While
            rs.Close()

            If Max = 0 Then
                BuscaNumeroAsiento = InicioAsiento & "0001"  'ENVIA EL SIGUIENTE NUMERO DE ASIENTO
            Else
                '-----------------------------------------------------------
                'PARA SABER LA CANTIDAD DE CEROS QUE DEBE HABER EN EL CONSECUTIVO DEL ASIENTO
                Ceros = Max.TrimStart("0").Length
                Max = CInt(Max)
                Length = Max.Length
                Max += 1
                If Max.Length <> Length Then
                    Ceros += 1
                End If
                For i As Integer = 0 To (3 - Ceros)
                    InicioAsiento = InicioAsiento & "0"
                Next
                '-----------------------------------------------------------
                BuscaNumeroAsiento = InicioAsiento & Max  'ENVIA EL SIGUIENTE NUMERO DE ASIENTO
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function


    Public Function ValidarPeriodo(ByVal Fecha As DateTime) As Boolean  'VALIDA SI ESTA EN EL MISMO PERIODO CONTABLE
        Dim cConexion As New Conexion                                   'O SI EL EL PERIODO ESTA CERRADO PARA PERMITIR O NO LA TRANSACCION
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader

        ValidarPeriodo = False
        'BUSCA EL MES Y EL A�O DEL PERIODO QUE SE ENCUENTRA ACTIVO
        rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT Mes, Anno, Estado FROM Periodo WHERE Activo = 1")

        While rs.Read
            Try
                If rs("Estado") = False Then    'VERIFICA SI EL PERIODO ESTA CERRADO
                    If Fecha.Year = rs("Anno") Then     'VERIFICA SI ESTA EN EL MISMO A�O
                        If Fecha.Month = rs("Mes") Then 'VERIFICA SI ESTA EN EL MISMO MES
                            ValidarPeriodo = True       'EN CASO DE QUE SEA EL MISMO PERIODO
                        End If
                    End If
                End If

            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End While

        rs.Close()
        cConexion.DesConectar(sqlConexion)
    End Function


    Public Function BuscaPeriodo(ByVal Fecha As DateTime) As String
        Dim cConexion As New Conexion                   'BUSCA EL PERIODO DE LA TRANSACCI�N DE ACUERDO AL PERIODO FISCAL
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim Inicio, Final As DateTime

        Try
            'BUSCA LOS PERIODOS FISCALES ABIERTOS
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT dbo.DateOnlyInicio(FechaInicio) AS FechaInicio, dbo.DateOnlyFinal(FechaFinal) AS FechaFinal FROM PeriodoFiscal WHERE '" & Fecha & "' BETWEEN FechaInicio AND FechaFinal")

            While rs.Read
                If Fecha >= rs("FechaInicio") Then
                    If Fecha <= rs("FechaFinal") Then
                        Inicio = rs("FechaInicio")
                        Final = rs("FechaFinal")
                        Fecha = CDate("01" & "/" & Fecha.Month & "/" & Fecha.Year)
                        Inicio = CDate("01" & "/" & Inicio.Month & "/" & Inicio.Year)
                        For i As Integer = 1 To 12
                            If Inicio < Fecha Then
                                Inicio = Inicio.AddMonths(1)
                            Else
                                BuscaPeriodo = i & "/" & Final.Year
                                Exit For
                            End If
                        Next
                    End If
                End If
            End While
            rs.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function


    Public Function TipoCambio(ByVal Fecha As DateTime, Optional ByVal Venta As Boolean = False) As Double
        Dim cConexion As New Conexion        'OBTIENE EL TIPO DE CAMBIO DE LA MONEDA DOLAR PARA LA FECHA
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim FechaMax As DateTime

        Try
            Fecha = FormatDateTime(Fecha, DateFormat.ShortDate)
            'BUSCA LOS TIPOS DE CAMBIO
            If Venta Then
                rs = cConexion.GetRecorset(cConexion.Conectar("Seguridad"), "SELECT ISNULL(ValorVenta,1) AS TipoCambio, Fecha FROM dbo.HistoricoMoneda WHERE Id_Moneda = 2 And Fecha <= dbo.DateOnlyFinal('" & Fecha & "')")
            Else
                rs = cConexion.GetRecorset(cConexion.Conectar("Seguridad"), "SELECT ISNULL(ValorCompra,1) AS TipoCambio, Fecha FROM dbo.HistoricoMoneda WHERE Id_Moneda = 2 And Fecha <= dbo.DateOnlyFinal('" & Fecha & "')")
            End If

            While rs.Read
                Try
                    If rs("Fecha") > FechaMax Then
                        FechaMax = rs("Fecha")
                        TipoCambio = rs("TipoCambio")
                    End If

                Catch ex As SystemException
                    MsgBox(ex.Message)
                End Try
            End While
            rs.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function

    Private Shared Formulario As String = ""
    Private Shared Metodo As String = ""
    Private Shared Mensaje As String = ""
    Public Shared Sub spEnviar_Correo(ByVal _Formulario As String, ByVal _Metodo As String, ByVal _Mensaje As String)
        Try
            Formulario = _Formulario
            Metodo = _Metodo
            Mensaje = _Mensaje
            Dim Hilo As New Thread(AddressOf spEnviar)
            Hilo.Start()
        Catch ex As Exception

        End Try
   
    End Sub

    Private Shared Sub spEnviar()
        'Dim correo As New MailMessage
        'Dim smtp As New SmtpClient()
        'Dim Cx As New Conexion

        'correo.From = New MailAddress("gerardo.rodriguez@seesoft-cr.com", "Bug - A&B SeeSoft", System.Text.Encoding.UTF8)
        'correo.To.Add("gerardo.rodriguez@seesoft-cr.com")
        ''correo.To.Add("ialvarez@seesoft-cr.com")
        '' correo.To.Add("rolando.obando@seesoft-cr.com")
        '' correo.To.Add("gdiaz@seesoft-cr.com")
        'correo.SubjectEncoding = System.Text.Encoding.UTF8
        'correo.Subject = Cx.SlqExecuteScalar(Cx.Conectar(), "SELECT [Empresa] FROM [restaurante].[dbo].[Configuraciones]")
        'correo.Body = "Fecha : " & Now & vbCrLf & vbCrLf & "Formulario : " & Formulario & vbCrLf & vbCrLf & "Metodo : " & Metodo & vbCrLf & vbCrLf & "Mensaje : " & Mensaje
        'correo.BodyEncoding = System.Text.Encoding.UTF8
        'correo.IsBodyHtml = False

        'correo.Priority = MailPriority.High

        'smtp.Credentials = New System.Net.NetworkCredential("gerardo.rodriguez@seesoft-cr.com", "Enrique123")
        'smtp.Port = 25
        'smtp.Host = "mail.seesoft-cr.com"
        'smtp.EnableSsl = True

        Try
            'smtp.Send(correo)
            spGuardarErrorBD(Now.ToString)
        Catch ex As System.Net.Mail.SmtpException
            spGuardarErrorBD(ex.Message)
        End Try
    End Sub

    Private Shared Sub spGuardarErrorBD(ByVal _Mensaje As String)
        Try
            Dim cx As New Conexion
            Dim Existe As Integer

            Existe = cx.SlqExecuteScalar(cx.Conectar(), "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tb_R_BitacoraError'")
            If Existe = 0 Then
                cx.SlqExecute(cx.sQlconexion, "CREATE TABLE [dbo].[tb_R_BitacoraError]( " &
    " [Id] [bigint] IDENTITY(1,1) NOT NULL, " &
    " [Fecha] [datetime] NOT NULL," &
    " [Formulario] [varchar](50) NOT NULL," &
    " [Metodo] [varchar](50) NOT NULL," &
    " [Mensaje] [varchar](500) NOT NULL," &
    " [ErrorEnvio] [varchar](500) NOT NULL," &
 " CONSTRAINT [PK_tb_R_BitacoraError] PRIMARY KEY CLUSTERED ( [Id] Asc) " &
 " WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY] " &
" SET ANSI_PADDING OFF  " &
" ALTER TABLE [dbo].[tb_R_BitacoraError] ADD  CONSTRAINT [DF_tb_R_BitacoraError_Fecha]  DEFAULT (getdate()) FOR [Fecha] " &
" ALTER TABLE [dbo].[tb_R_BitacoraError] ADD  CONSTRAINT [DF_tb_R_BitacoraError_Formulario]  DEFAULT ('') FOR [Formulario] " &
" ALTER TABLE [dbo].[tb_R_BitacoraError] ADD  CONSTRAINT [DF_tb_R_BitacoraError_Metodo]  DEFAULT ('') FOR [Metodo] " &
" ALTER TABLE [dbo].[tb_R_BitacoraError] ADD  CONSTRAINT [DF_tb_R_BitacoraError_Mensaje]  DEFAULT ('') FOR [Mensaje]   " &
" ALTER TABLE [dbo].[tb_R_BitacoraError] ADD  CONSTRAINT [DF_tb_R_BitacoraError_ErrorEnvio]  DEFAULT ('') FOR [ErrorEnvio] ")

                cx.SlqExecute(cx.sQlconexion, "INSERT INTO [tb_R_BitacoraError] ([Formulario],[Metodo],[Mensaje] ,[ErrorEnvio]) VALUES ('" & Formulario & "','" & Metodo & "','" & Mensaje.Replace("'", " ") & "','" & _Mensaje & "')")
            Else
                cx.SlqExecute(cx.sQlconexion, "INSERT INTO [tb_R_BitacoraError] ([Formulario],[Metodo],[Mensaje] ,[ErrorEnvio]) VALUES ('" & Formulario & "','" & Metodo & "','" & Mensaje.Replace("'", " ") & "','" & _Mensaje & "')")

            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function fnValidaIntegracionFdNuevo() As Boolean
        Try
            Dim IntegracionFdNuevo As String = GetSetting("SeeSoft", "Restaurante", "IntegracionFdNuevo")

            If IntegracionFdNuevo = "" Then
                SaveSetting("SeeSoft", "Restaurante", "IntegracionFdNuevo", "NO")
            ElseIf IntegracionFdNuevo = "SI" Then
                Return True
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class

