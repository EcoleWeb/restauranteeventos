Public Class Prompt_Categoria
    Public receta As Integer
    Public tipo As Integer
    Public grupo As String
    Public ruta As String
    Public Cargando As Boolean
    Public ID_BODEGA As Integer
    Public ID_RECETA As Integer = 0
    Public COSTO_PROMEDIO As Double
    Public RECETA_COMPLETA As Boolean = True
    Public Id_Acom As Integer = 0
    Public Id_Modfi As Integer = 0
    Public Id_Categoria As Integer = 0
    Dim _dtCabys As New DataTable
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        tipo = 0
        Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If (Me.Text = "Acompañamientos") Then
            If (txtDato.Text = Nothing Or txtRecetas.Text = Nothing) Then
                MsgBox("Verifica que hayas completado todos los datos", MsgBoxStyle.Information, "Faltan datos ...")
                Exit Sub
            End If
        ElseIf (Id_Acom > 0) Or (Id_Modfi > 0) Then
            tipo = 1
            Close()
            Exit Sub

        ElseIf (txtDato.Text = Nothing Or (comboGrupos.Text = Nothing And comboGrupos.Visible = True)) Then
            MsgBox("Verifica que hayas completado todos los datos", MsgBoxStyle.Information, "Faltan datos ...")
            Exit Sub
        End If
        tipo = 1
        Close()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim fImagen As New Imagen
        fImagen.PictureBox1.Image = btnBuscar.Image
        fImagen.ShowDialog()
        btnBuscar.Image = fImagen.PictureBox1.Image
        ruta = fImagen.ruta
        fImagen.Dispose()
    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click

        tipo = 3
        Close()
    End Sub

    

    Private Sub Prompt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cConexion As New ConexionR
        Dim bd As New SqlClient.SqlConnection
        Dim rs As SqlClient.SqlDataReader = Nothing
        Cargando = True
        bd = cConexion.Conectar("Restaurante")
        cConexion.GetRecorset(bd, "Select nombre_grupo ,id from grupos_menu", rs)
        While rs.read
            comboGrupos.Items.Add(rs("nombre_grupo"))
            ListItems.Items.Add(rs("id"))
        End While
        cConexion.DesConectar(bd)

        If grupo <> vbNullString Then
            comboGrupos.SelectedIndex = ListItems.FindString(grupo)
        End If

        If Grupo_Bodega.Visible = True And cmbxBodegas.Visible = True Then
            Cargar_Bodegas()
        End If

        If ID_BODEGA <> 7005 Then
            cmbxBodegas.SelectedValue = ID_BODEGA
        End If
        Cargando = False

        BuscarInfoCabys()
        BuscarCuentasContables()
        CargarActividadEconomica()
        VerificaReporteActividadEconomica()
    End Sub

    Sub VerificaReporteActividadEconomica()
        Try
            Dim UsaReporteActividadEconomica As String = ""

            UsaReporteActividadEconomica = GetSetting("Seesoft", "Restaurante", "ReporteActividadEconomica")

            If UsaReporteActividadEconomica.Equals("1") Then
                lbActividadEconomica.Visible = True
                cbActividadEconomica.Visible = True
                lbIdActividad.Visible = True
            ElseIf UsaReporteActividadEconomica.Equals("") Then
                SaveSetting("SeeSoft", "Restaurante", "ReporteActividadEconomica", "0")
                lbActividadEconomica.Visible = False
                cbActividadEconomica.Visible = False
                lbIdActividad.Visible = True
            Else
                lbActividadEconomica.Visible = False
                cbActividadEconomica.Visible = False
                lbIdActividad.Visible = True
            End If
        Catch ex As Exception
            lbActividadEconomica.Visible = False
            cbActividadEconomica.Visible = False
        End Try
    End Sub
    Sub BuscarCuentasContables()
        Try
            Dim dtInfoCuentas As New DataTable

            cFunciones.Llenar_Tabla_Generico("Select CuentaIngreso, DCuentaIngreso, CuentaCosto, DCuentaCosto from Categorias_Menu where Id=" & Id_Categoria & "", dtInfoCuentas, GetSetting("SeeSoft", "Restaurante", "Conexion"))

            If dtInfoCuentas.Rows.Count > 0 Then
                txtCuentaCostos.Text = dtInfoCuentas.Rows(0).Item("CuentaCosto")
                lbDescripcionCostos.Text = dtInfoCuentas.Rows(0).Item("DCuentaCosto")
                txtCuentaIngresos.Text = dtInfoCuentas.Rows(0).Item("CuentaIngreso")
                lbDescripcionIngresos.Text = dtInfoCuentas.Rows(0).Item("DCuentaIngreso")
            End If

        Catch ex As Exception
            MsgBox("No se pudo obtener las cuentas contables. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
    Sub BuscarInfoCabys()
        Try
            Dim dtInfoCabys As New DataTable

            cFunciones.Llenar_Tabla_Generico("Select IdCabys, IdActividadEconomica as IdActividad from Categorias_Menu where Id=" & Id_Categoria & "", dtInfoCabys, GetSetting("SeeSoft", "Restaurante", "Conexion"))

            If dtInfoCabys.Rows.Count > 0 Then
                cargarCabys(dtInfoCabys.Rows(0).Item("IdCabys"))
                lbIdActividad.Text = dtInfoCabys.Rows(0).Item("IdActividad")
            End If


        Catch ex As Exception
            MsgBox("No se pudo obtener el código cabys relacionado. "& ex.Message,MsgBoxStyle.Critical)
        End Try
    End Sub

    Sub CargarActividadEconomica()
        Try
            Dim dtActividadEconomica As New DataTable

            dtActividadEconomica.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Id, Descripcion from Hotel.dbo.tb_CE_ActividadEconomica where Inactivo=0 ", dtActividadEconomica)
            If dtActividadEconomica.Rows.Count > 0 Then
                cbActividadEconomica.DataSource = dtActividadEconomica
                cbActividadEconomica.DisplayMember = "Descripcion"
                cbActividadEconomica.ValueMember = "Id"
            End If
            AsignarActividadEconomica()

        Catch ex As Exception
            MsgBox("No se pudo cargar las actividades económicas. " & ex.Message)
        End Try
    End Sub

    Sub AsignarActividadEconomica()
        Try
            Dim DtMiActividadEconomica As New DataTable

            DtMiActividadEconomica.Clear()
            cFunciones.Llenar_Tabla_Generico("Select IdActividadEconomica from Categorias_Menu where Id=" & Id_Categoria & "", DtMiActividadEconomica)
            If DtMiActividadEconomica.Rows.Count > 0 Then
                cbActividadEconomica.SelectedValue = DtMiActividadEconomica.Rows(0).Item("IdActividadEconomica")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cargar_Bodegas()
        Try
            Dim Datasets As New DataSet
            Dim Conexiones As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()

            Conexiones.GetDataSet(cnx, "Select idBodega, Nombre  from Proveeduria.dbo.Bodega", Datasets, "Bodega")
            cmbxBodegas.DataSource = Datasets.Tables("Bodega")
            cmbxBodegas.DisplayMember = "Nombre"
            cmbxBodegas.ValueMember = "idBodega"
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        End Try
    End Sub

    Private Sub comboGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboGrupos.SelectedIndexChanged
        ListItems.SelectedIndex = comboGrupos.SelectedIndex
    End Sub

    Private Sub txtDato_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDato.Click
        
    End Sub

    Private Sub txtDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDato.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.Text = "Nuevo Grupo Mesas" Then
                txtMEsas.Focus()
            End If
        End If
    End Sub

    Private Sub txtMEsas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMEsas.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnAceptar.Focus()
        End If
    End Sub



    'ESTA FUNCION CALCULA DE FORMA RECURSIVA EL COSTO DE UNA RECETA EN CASO DE QUE TUVIERA ANIDADAS OTRAS RECETAS
    Private Function CalculaCostoReceta(ByVal codigo_receta As Integer) As Double
        Try
            Dim cnx As New SqlClient.SqlConnection
            Dim exe As New ConexionR
            Dim calculando_costo As Double
            Dim dsDetalle_RecetaAnidada As New DataSet
            Dim fila_receta As DataRow
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            exe.GetDataSet(cnx, "SELECT idDetalle, idReceta,Descripcion, Codigo, Articulo,idBodegaDescarga FROM Recetas_Detalles WHERE IdReceta = " & codigo_receta, dsDetalle_RecetaAnidada, "Recetas_Detalles")
            For Each fila_receta In dsDetalle_RecetaAnidada.Tables("Recetas_Detalles").Rows
                If fila_receta("Articulo") = 0 Then
                    calculando_costo += CalculaCostoReceta(fila_receta("Codigo"))
                Else
                    calculando_costo += CalculaCostoArticulo(fila_receta("Codigo"), fila_receta("idBodegaDescarga"), fila_receta("Descripcion"))
                End If
            Next
            Return calculando_costo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function


    'ESTA FUNCION DEVUELVE EL COSTO PROMEDIO DEL ARTICULO QUE SE BUSCA 
    Private Function CalculaCostoArticulo(ByVal codigo_articulo As Integer, ByVal idbodega_descarga As Integer, ByVal nombre As String) As Double
        Try
            Dim costoXarticulo As Double
            Dim exe As New ConexionR
            Dim cnx As New SqlClient.SqlConnection
            cnx.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            costoXarticulo = exe.SlqExecuteScalar(cnx, "Select CostoTotal From VCostoDetalleReceta Where idBodegaDescarga = " & idbodega_descarga & " AND Codigo = " & codigo_articulo)
            If costoXarticulo = 0 Then
                Receta_Completa = False
                MsgBox("El articulo " + nombre + " no se encuentra registrado en la bodega escogida, no es posible calcular el costo de este plato", MsgBoxStyle.Information, "Sistemas estructurales SeeSoft")
            End If
            Return costoXarticulo
        Catch ex As Exception
            MsgBox("Se produjo el siguiente error" + ex.ToString, MsgBoxStyle.Critical, "Sistemas estructurales SeeSoft")
        End Try
    End Function

    Private Sub cmbxBodegas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxBodegas.SelectedIndexChanged

    End Sub



    Private Sub ButtonAgregados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregados.Click
        Dim modif As New AcompanamientoModif
        modif.Id_Categoria = Id_Categoria
        modif.Text = "Acompañamientos y Modificadores de " & Me.txtDato.Text
        If modif.ShowDialog = Windows.Forms.DialogResult.OK Then

        End If
    End Sub

    Private Sub ButtonTeclado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTeclado.Click
        Dim teclado As String = "osk.exe"
        If Not GetSetting("SeeSoft", "Restaurante", "Teclado").Equals("") Then
            teclado = GetSetting("SeeSoft", "Restaurante", "Teclado")
        End If
        Shell(teclado)
    End Sub

    Private Sub ButtonEscogerModificador_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEscogerModificador.Click
        Dim aMod As New FormLISTADO
        aMod.Text = "Modificadores"
        aMod.tipo = 1
        If aMod.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Id_Modfi = aMod.Id_Select
            Id_Acom = 0
            txtDato.Text = aMod.DataSetReceta1.DataTableLista(aMod.ListBoxEscoge.SelectedIndex).Nombre
            Me.RadioButtonModificadores.Checked = True
        End If
        aMod.Dispose()

    End Sub
    
    Private Sub ButtonEscogerACO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEscogerACO.Click
        Dim aCOM As New FormLISTADO
        aCOM.Text = "Acompañamientos"
        aCOM.tipo = 2
        If aCOM.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Id_Modfi = 0
            Id_Acom = aCOM.Id_Select
            txtDato.Text = aCOM.DataSetReceta1.DataTableLista(aCOM.ListBoxEscoge.SelectedIndex).Nombre
            RadioButtonAcompanamiento.Checked = True
        End If
        aCOM.Dispose()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim d As New registro
        d.ShowDialog()

        If d.iOpcion = 1 Then
            Me.TextBoxPrecio.Text = d.txtCodigo.Text
        End If
        d.Dispose()

    End Sub

    Private Sub txtCodigoCabys_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigoCabys.KeyDown
        If e.KeyCode = Keys.F1 Then
            BuscarCabys()
        End If
    End Sub

    Private Sub btBuscarCabys_Click(sender As Object, e As EventArgs) Handles btBuscarCabys.Click
        BuscarCabys()
    End Sub

    Sub BuscarCabys()
        Try
            Dim _frmBuscarCabys As New frmBuscaCabys
            AddOwnedForm(_frmBuscarCabys)
            _frmBuscarCabys.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub cargarCabys(ByVal id As Integer)
        _dtCabys.Clear()
        cFunciones.Llenar_Tabla_Generico("select id,Descripcion,codigo, PorcentajeIVA from Cabys where id=" & id & "", _dtCabys, GetSetting("seeSoft", "Hotel", "Conexion"))
        If _dtCabys.Rows.Count > 0 Then
            Me.lbIdCabys.Text = _dtCabys.Rows(0).Item("id")
            Me.txtDescripcionCabys.Text = _dtCabys.Rows(0).Item("Descripcion")
            Me.txtCodigoCabys.Text = _dtCabys.Rows(0).Item("codigo")
            Me.lbPorcImpuesto.Text = "Porcentaje CABYS: " & _dtCabys.Rows(0).Item("PorcentajeIVA")
        Else
            Me.lbIdCabys.Text = "0"
            Me.txtDescripcionCabys.Text = ""
            Me.txtCodigoCabys.Text = "0"
            Me.lbPorcImpuesto.Text = "Porcentaje CABYS: 0"
        End If
    End Sub

    Private Sub cbActividadEconomica_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbActividadEconomica.SelectedValueChanged
        Try
            lbIdActividad.Text = cbActividadEconomica.SelectedValue
        Catch ex As Exception

        End Try

    End Sub

    Private Sub buscacuenta(ByVal a1 As Object, ByVal a2 As Object)
        'Buscar Cuenta Contable
        Dim Fbuscador As New buscador
        Dim Conexion As New ConexionR
        Fbuscador.cantidad = 4
        Fbuscador.bd = "Contabilidad"
        Fbuscador.tabla = "CuentaContable"
        Fbuscador.lblEncabezado.Text = "          Buscador de Cuentas"
        Fbuscador.TituloModulo.Text = "Cuentas Contables"
        Fbuscador.consulta = "Select CuentaContable, Descripcion, Tipo, CuentaMadre from CuentaContable where Movimiento=1"
        Fbuscador.ShowDialog()
        a1.Text = Fbuscador.cuenta
        a2.Text = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & Fbuscador.cuenta & "'")
        Fbuscador.Dispose()
    End Sub

    Private Sub txtCuentaCostos_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCuentaCostos.KeyDown
        If e.KeyCode = Keys.F1 Then
            buscacuenta(txtCuentaCostos, lbDescripcionCostos)
        End If
    End Sub

    Private Sub txtCuentaCostos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCuentaCostos.KeyPress
        If e.KeyChar = Chr(13) Then
            Dim datos As String
            Dim Conexion As New ConexionR

            Try
                datos = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & txtCuentaCostos.Text & "'")
                If datos <> vbNullString Then
                    lbDescripcionCostos.Text = datos
                    txtCuentaCostos.Focus()
                Else
                    MessageBox.Show("Cuenta Contable incorrecta")
                    txtCuentaIngresos.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            End Try
        End If
    End Sub

    Private Sub txtCuentaIngresos_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCuentaIngresos.KeyDown
        If e.KeyCode = Keys.F1 Then
            buscacuenta(txtCuentaIngresos, lbDescripcionIngresos)
        End If
    End Sub

    Private Sub txtCuentaIngresos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCuentaIngresos.KeyPress
        If e.KeyChar = Chr(13) Then
            Dim datos As String
            Dim Conexion As New ConexionR

            Try
                datos = Conexion.SlqExecuteScalar(Conexion.Conectar("Contabilidad"), "Select Descripcion from contabilidad.dbo.cuentacontable where cuentaContable='" & txtCuentaIngresos.Text & "'")
                If datos <> vbNullString Then
                    lbDescripcionIngresos.Text = datos
                    txtCuentaIngresos.Focus()
                Else
                    MessageBox.Show("Cuenta Contable incorrecta")
                    txtCuentaCostos.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
            End Try
        End If
    End Sub
End Class