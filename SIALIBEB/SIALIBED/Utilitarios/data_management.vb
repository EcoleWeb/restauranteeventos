Public Class data_management
    Private conexion As SqlClient.SqlConnection
    Private adaptador As SqlClient.SqlDataAdapter

    Sub New(ByVal cadena As String)
        conexion = New SqlClient.SqlConnection(cadena)
    End Sub

    Public Property StringConection() As String
        Get
            Return conexion.ConnectionString
        End Get
        Set(ByVal value As String)
            conexion.ConnectionString = value
        End Set
    End Property

    Public Function Ejecuta(ByVal _strSQL As String) As DataTable
        Try
            Dim dts As New DataTable
            conexion.Open()
            adaptador = New SqlClient.SqlDataAdapter(_strSQL, conexion)
            adaptador.SelectCommand.CommandType = CommandType.Text
            adaptador.Fill(dts)
            conexion.Close()
            Return dts
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
End Class