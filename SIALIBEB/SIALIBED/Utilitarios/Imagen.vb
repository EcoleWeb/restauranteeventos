Imports System.IO

Public Class Imagen

#Region "Variables"
    Public idMesa As String
    Public ruta As String
#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim FileName As String
            Dim result As DialogResult = BuscaImagen.ShowDialog()

            If (result = System.Windows.Forms.DialogResult.OK) Then
                FileName = BuscaImagen.FileName
                ruta = FileName
                Dim SourceImage As Bitmap
                SourceImage = New Bitmap(FileName)
                PictureBox1.Image = CType(SourceImage, Image)
            End If
        Catch ex As Exception
            MessageBox.Show("No se puede convertir el tipo de imagen", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Exit Sub
        End Try
    End Sub


    Private Sub btnListo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListo.Click
        Close()
    End Sub

    Private Sub BEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BEliminar.Click
        ruta = ""
        PictureBox1.Image = Nothing
    End Sub
End Class