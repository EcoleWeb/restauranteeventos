﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SIALIBEB")> 
<Assembly: AssemblyDescription("Sistema de Alimentos & Bebidas")> 
<Assembly: AssemblyCompany("Sistemas Estructurales SeeSOFT")> 
<Assembly: AssemblyProduct("SIALIBEB")> 
<Assembly: AssemblyCopyright("Copyright ©  2014")> 
<Assembly: AssemblyTrademark("SEESOFT")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("dbfbc83f-17cc-4ed8-b7bc-35ffd6122f8f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("7.0.0.1")> 
<Assembly: AssemblyFileVersion("7.0.0.1")> 

<Assembly: NeutralResourcesLanguageAttribute("es")> 