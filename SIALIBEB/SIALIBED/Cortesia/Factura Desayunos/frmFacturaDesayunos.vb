Imports System.data.SqlClient
Imports System.Drawing.Printing
Public Class frmFacturaDesayunos
    Public TablaDesayunos As New DataTable
    Dim conectadobd As New SqlClient.SqlConnection
    Dim cConexion As New ConexionR
    Public CedulaU, CedulaS, nombremesa As String
    Dim cedula As String
    Dim pax As Integer = 0
    Dim rs As SqlClient.SqlDataReader



    Private Sub frmFacturaDesayunos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.conectadobd = Me.SqlConnection1
            conectadobd = cConexion.Conectar("Hotel")
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Hotel", "CONEXION")

            Me.dtpFecha.Value = Today
            Me.Label2.Text = Me.dtpFecha.Value
            Me.AdapterServicios.Fill(Me.DataSetDesayunos1, "Servicios")
            Me.AdapterMoneda.Fill(Me.DataSetDesayunos1, "Moneda")


            CargarFacturas(Me.Label2.Text)
            CargarServicios()
            Me.CrearTabla()
            Me.calcular()
            Panel1.Enabled = True
            'CrearFacturas()

            If Me.cbxMoneda.Items.Count >= 1 Then Me.cbxMoneda.SelectedIndex = 1

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#Region "Crear Columnas"
    Private Sub CrearColumnas(ByVal Nombre As String, ByVal Titulo As String, ByVal Tamano As Int16, ByVal Orden As Int16, ByVal Campo As String, ByVal Numerico As Boolean, ByVal Editable As Boolean, ByVal Calculadora As Boolean)
        Dim Column = Me.GridView1.Columns.Add
        Column.Caption = Titulo
        Column.fieldname = Campo
        Column.Name = Nombre
        Column.VisibleIndex = Orden
        Column.width = Tamano
        If Numerico = True Then
            Column.displayformat.formattype = DevExpress.Utils.FormatType.Numeric
            Column.displayformat.formatstring = "#,#0.00"
        End If
        If Editable = False Then
            Column.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions) Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        Else
            Column.options = DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused
        End If
    End Sub
#End Region

#Region "Crear Tabla"

    Private Sub CrearTabla()
        Me.TablaDesayunos.Clear()
        Me.TablaDesayunos.Columns.Clear()

        Me.CrearColumnas("Id_Reservacion", "Reservaci�n", 10, 1, "Id_Reservacion", False, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("Id_Reservacion", GetType(Double)))

        Me.CrearColumnas("#Pax", "#Pax", 90, 2, "#Pax", False, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("#Pax", GetType(Double)))

        Me.CrearColumnas("Subtotal", "Subtotal", 110, 3, "Subtotal", False, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("Subtotal", GetType(Double)))

        Me.CrearColumnas("Servicio", "10%S", 110, 4, "Servicio", False, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("Servicio", GetType(Double)))

        Me.CrearColumnas("IVenta", "I.V.", 110, 5, "IVenta", False, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("IVenta", GetType(Double)))

        Me.CrearColumnas("Total", "Total", 110, 6, "Total", True, False, False)
        Me.TablaDesayunos.Columns.Add(New DataColumn("Total", GetType(Double)))

    End Sub
#End Region

#Region "Cargar Facturas"
    Function CargarFacturas(ByVal Fech As String)
        Dim cnn As SqlConnection = Nothing
        Dim mensaje As String
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("Seesoft", "Hotel", "Conexion")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String
            'sel = "SELECT Habitaciones_Reservaciones.CantNinos, Habitaciones_Reservaciones.CantAdultos,  Habitaciones_Reservaciones.Tarifa, Reservacion.Id_Reservacion, Reservacion.Fecha_Ingreso, Reservacion.Fecha_Salida FROM Habitaciones_Reservaciones INNER JOIN Reservacion ON Habitaciones_Reservaciones.Id_Reservacion = Reservacion.Id_Reservacion INNER JOIN Check_In On Check_In.Id_Reservacion= Reservacion.Id_Reservacion where Reservacion.Fecha_Ingreso < '" & Fech & "' and Reservacion.Fecha_Salida >= '" & Fech & "' AND Reservacion.Cancelado=0"
            'sel = "SELECT DISTINCT  Habitaciones_Reservaciones.CantNinos + ISNULL(Adicionales.Numero_Nino, 0) AS CantNinos,  Habitaciones_Reservaciones.CantAdultos + ISNULL(Adicionales.Numero_Adulto, 0) AS CantAdultos, Habitaciones_Reservaciones.Tarifa,  Reservacion.Id_Reservacion, Reservacion.Fecha_Ingreso, Reservacion.Fecha_Salida, ISNULL(Adicionales.Numero_Adulto, 0) AS AdultoAdicional,  ISNULL(Adicionales.Numero_Nino, 0) AS NinoAdicional FROM Habitaciones_Reservaciones INNER JOIN  Reservacion ON Habitaciones_Reservaciones.Id_Reservacion = Reservacion.Id_Reservacion INNER JOIN  Check_In ON Check_In.Id_Reservacion = Reservacion.Id_Reservacion LEFT OUTER JOIN  Adicionales ON Reservacion.Id_Reservacion = Adicionales.Id_Reservacion WHERE (Reservacion.Fecha_Ingreso < '" & Fech & "') AND (Reservacion.Fecha_Salida >= '" & Fech & "') AND (Reservacion.Cancelado = 0) ORDER BY Reservacion.Id_Reservacion" ' SAJ 060807 SE LE A�ADE PERSONAS ADICIONALES PARA EL DESAYUNO.. :)
            sel = "SELECT DISTINCT  Reservacion.Id_Reservacion, ISNULL(Adicionales.Numero_Nino, 0) AS CantNinos,  CaracteristicaTipoHab.Cantidad * CaracteristicasReservacionHabitacion.Numero + ISNULL(Adicionales.Numero_Adulto, 0) AS CantAdultos,  ISNULL(Adicionales.Numero_Adulto, 0) AS AdultoAdicional, ISNULL(Adicionales.Numero_Nino, 0) AS NinoAdicional,  Habitaciones_Reservaciones.Tarifa, Reservacion.Fecha_Ingreso, Reservacion.Fecha_Salida, CaracteristicasReservacionHabitacion.Tipo,  CaracteristicasReservacionHabitacion.Numero FROM Habitaciones_Reservaciones INNER JOIN  Reservacion ON Habitaciones_Reservaciones.Id_Reservacion = Reservacion.Id_Reservacion INNER JOIN  Check_In ON Check_In.Id_Reservacion = Reservacion.Id_Reservacion INNER JOIN  CaracteristicasReservacionHabitacion ON Reservacion.Id_Reservacion = CaracteristicasReservacionHabitacion.Id_Reservacion AND  Habitaciones_Reservaciones.CaracteristicaId = CaracteristicasReservacionHabitacion.Id INNER JOIN  CaracteristicaTipoHab ON CaracteristicasReservacionHabitacion.Tipo = CaracteristicaTipoHab.Tipo LEFT OUTER JOIN  Adicionales ON Reservacion.Id_Reservacion = Adicionales.Id_Reservacion WHERE (Reservacion.Cancelado = 0) GROUP BY Reservacion.Id_Reservacion, ISNULL(Adicionales.Numero_Nino, 0),  CaracteristicaTipoHab.Cantidad * CaracteristicasReservacionHabitacion.Numero + ISNULL(Adicionales.Numero_Adulto, 0),  ISNULL(Adicionales.Numero_Adulto, 0), Habitaciones_Reservaciones.Tarifa, Reservacion.Fecha_Ingreso, Reservacion.Fecha_Salida,  CaracteristicasReservacionHabitacion.Tipo, CaracteristicasReservacionHabitacion.Numero HAVING (Reservacion.Fecha_Ingreso < '" & Fech & "') AND (Reservacion.Fecha_Salida >= '" & Fech & "') AND (Habitaciones_Reservaciones.Tarifa <> 0) ORDER BY Reservacion.Id_Reservacion"


            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetDesayunos1.Reservacion)
            If Me.DataSetDesayunos1.Reservacion.Rows.Count = 0 Then
                MsgBox("No hay facturas de desayunos pendientes...", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            mensaje = ex.Message
        Finally

            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#End Region

#Region "Cargar Servicios"
    Function CargarServicios()
        Dim cnn As SqlConnection = Nothing
        Dim mensaje As String
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("Seesoft", "Hotel", "Conexion")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String
            sel = "SELECT * FROM Servicios where Descripcion like '%DESAYUNO%' "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetDesayunos1.Servicios)
        Catch ex As System.Exception
            'MsgBox(ex.ToString)
            mensaje = ex.Message
        Finally

            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#End Region

#Region "Calcular"
    Function calcular()
        Dim idr As Integer
        Dim mens As String
        Me.TablaDesayunos.Clear()
        Try
            Dim dr As DataRow
            dr = Me.TablaDesayunos.NewRow
            Dim contador As Integer

            idr = Me.DataSetDesayunos1.Reservacion.Rows(0).Item("Id_Reservacion")

            For contador = 0 To Me.DataSetDesayunos1.Reservacion.Rows.Count - 1
                If idr = Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("Id_Reservacion") And Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("Tarifa") <> 0 Then
                    If pax = 0 Then
                        pax = Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantNinos") + Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantAdultos")
                    Else
                        pax = pax + (Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantNinos") + Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantAdultos"))
                    End If
                Else '<<
                    If idr <> Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("Id_Reservacion") And Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("Tarifa") <> 0 Then
                        dr = Me.TablaDesayunos.NewRow()
                        dr("Id_Reservacion") = idr
                        dr("#pax") = pax
                        dr("Subtotal") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Tarifa") * pax
                        dr("Servicio") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Monto_Servicio") * pax
                        dr("IVenta") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Monto_Vts") * pax
                        dr("Total") = pax * Me.DataSetDesayunos1.Servicios.Rows(0).Item("Total")
                        Me.TablaDesayunos.Rows.Add(dr)
                        idr = Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("Id_Reservacion")
                        pax = 0
                        pax = Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantNinos") + Me.DataSetDesayunos1.Reservacion.Rows(contador).Item("CantAdultos")
                    End If
                End If
                If contador = Me.DataSetDesayunos1.Reservacion.Rows.Count - 1 Then
                    dr = Me.TablaDesayunos.NewRow()
                    dr("Id_Reservacion") = idr
                    dr("#pax") = pax
                    dr("Subtotal") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Tarifa") * pax
                    dr("Servicio") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Monto_Servicio") * pax
                    dr("IVenta") = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Monto_Vts") * pax
                    dr("Total") = pax * Me.DataSetDesayunos1.Servicios.Rows(0).Item("Total")
                    Me.TablaDesayunos.Rows.Add(dr)
                End If
            Next
            Me.GridControl1.DataSource = Me.TablaDesayunos

        Catch ex As Exception
            mens = ex.Message
            '  MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Crear Facturas"
    Function CrearFacturas()
        Dim num As Integer

        Me.ButtonGenerarImprimir.Enabled = False

        For num = 0 To Me.TablaDesayunos.Rows.Count - 1
            'INGRESO LOS DATOS DE LA FACTURA A LA TABLA VENTAS
            Dim NumeroFactura As String
            Dim NombreJuridico As String
            Dim nombreCliente As String
            Dim codigoCliente As String = 0

            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            nombreCliente = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Cliente as Nombre from Reservacion where Id_Reservacion= " & Me.TablaDesayunos.Rows(num).Item("Id_Reservacion"))
            conectadobd.Close() ''se busca el nombre del cliente al que se le hace la factura
            If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
            codigoCliente = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(Id,0) from Cliente where (Nombre) = ('" & nombreCliente & "')")
            If codigoCliente = Nothing Then ''se busca el codigo del cliente al que se le hace la factura
                nombreCliente = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre_Compa�ia as Nombre from Reservacion where Id_Reservacion= " & Me.TablaDesayunos.Rows(num).Item("Id_Reservacion"))
                If nombreCliente = "DIRECTO" Then
                    codigoCliente = 0
                Else
                    codigoCliente = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(Id,0) from Cliente where Nombre= '" & nombreCliente & "'")
                    NombreJuridico = cConexion.SlqExecuteScalar(conectadobd, "Select nombrejuridico from Cliente where Nombre= '" & nombreCliente & "'")
                    If nombreCliente <> NombreJuridico Then nombreCliente = NombreJuridico & vbCrLf & "(" & nombreCliente & ")"

                End If
            End If
            conectadobd.Close()

            If MsgBox("Desea generar e imprimir la factura por concepto de desayunos " & vbCrLf & "para la reserva " & Me.TablaDesayunos.Rows(num).Item(0) & " : " & nombreCliente, MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                NumeroFactura = cConexion.SlqExecuteScalar(conectadobd, "Select isnull(MAX(Num_Factura),0) as NumeroFactura from Ventas where Proveniencia_Venta= " & User_Log.PuntoVenta)
                conectadobd.Close()
                ''numerofactura es el max numero de factura correspondiente al punto de venta seleccionado
                Dim num_apertura, usuario As String
                conectadobd = cConexion.Conectar("Restaurante")
                If Me.conectadobd.State <> ConnectionState.Open Then Me.conectadobd.Open()

                cConexion.GetRecorset(conectadobd, "Select aperturacaja.NApertura, aperturacaja.Nombre, aperturacaja.Cedula from aperturacaja inner join Usuarios on aperturacaja.cedula = Usuarios.Cedula where Usuarios.Clave_Interna = '" & Me.txtClave.Text & "' and aperturacaja.Estado = 'A'", rs)
                While rs.Read
                    num_apertura = rs("NApertura")
                    usuario = rs("Nombre")
                    CedulaU = rs("Cedula")
                End While

                rs.Close()
                cConexion.DesConectar(Me.conectadobd)
                ''
                If num_apertura = Nothing Then
                    MsgBox("No se ha realizado ninguna apertura de caja...", MsgBoxStyle.Information)
                    Exit Function
                    rs.Close()
                End If
                'Me.conectadobd.Close()
                ''campos es el string que contiene los campos de la tabla ventas de hotel
                Me.conectadobd = cConexion.Conectar("Hotel")
                Dim campos As String = "Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento, Transporte, Tipo_Cambio, Monto_ICT, Id_Reservacion, Monto_Saloero, Proveniencia_Venta, Huesped, Descripcion, TipoCambioDolar,IdCliente"
                ''datos es el string que contiene los datos de la tabla ventas de hotel
                NumeroFactura += 1
                Dim datos As String
                Dim total As Double : Dim iv As Double
                total = Me.TablaDesayunos.Rows(num).Item("Total")
                iv = cConexion.SlqExecuteScalar(conectadobd, "Select Imp_Venta from configuraciones")
                Dim subt, ivt, ivs As Double
                Dim Numero_Pax As Integer

                subt = Me.TablaDesayunos.Rows(num).Item("Subtotal")
                ivt = Me.TablaDesayunos.Rows(num).Item("IVenta")
                ivs = Me.TablaDesayunos.Rows(num).Item("Servicio")
                Numero_Pax = Me.TablaDesayunos.Rows(num).Item(1)
                Dim Cuenta As String
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                Cuenta = cConexion.SlqExecuteScalar(conectadobd, "SELECT Cuentas.Id FROM Cuentas INNER JOIN Reservacion ON Cuentas.Id_Reservacion = Reservacion.Id_Reservacion INNER JOIN HabitacionesAsignadas ON Reservacion.Id_Reservacion = HabitacionesAsignadas.Id_Reservacion WHERE Reservacion.Id_Reservacion = " & Me.TablaDesayunos.Rows(num).Item("Id_Reservacion"))
                'se busca el nombre del cliente al que se le hace la factura
                conectadobd.Close()

                Dim valorC As String
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                valorC = cConexion.SlqExecuteScalar(conectadobd, "SELECT ValorCompra from Moneda Where MonedaNombre like '%" & Me.cbxMoneda.Text & "%'")
                ''se busca el nombre del cliente al que se le hace la factura
                conectadobd.Close()
                Dim codigoM As Integer
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                codigoM = cConexion.SlqExecuteScalar(conectadobd, "SELECT CodMoneda from Moneda Where MonedaNombre like '%" & Me.cbxMoneda.Text & "%'")
                ''se busca el nombre del cliente al que se le hace la factura
                conectadobd.Close()
                ''
                ''datos = numerofactura & ",'" & tipo_pago & "'," & txtcodcliente.Text & ",'" & txtcliente.Text & "',0,'" & CedulaU & "',0," & CDbl(txtSubtotal.Text) & "," & txtdescuento.Value & "," & CDbl(txtIV.Text) & "," & CDbl(txtTotal.Text) & ", GetDate(), GetDate(),'NINGUNO',0,0,0,0,0,0,0," & Num_Apertura & ",0," & codMoneda & ",'" & NombreMoneda & "',' ',' ',0,0,0,1,0," & IdReservacion & "," & CDbl(txtIS.Text) & "," & PuntoVenta & ",'',''," & valor & "," & IdCliente'original

                datos = NumeroFactura & ",'CRE'," & codigoCliente & ",'" & nombreCliente & "',0,'" & cedula & "',0," & subt & ",0," & ivt & "," & total & ", GetDate(), GetDate(),'NINGUNO',0,0,0,0,0,0,0," & num_apertura & ",0," & codigoM & ",'" & Me.cbxMoneda.Text & "',' ',' ',0,0,0," & valorC & ",0," & Cuenta & "," & ivs & "," & User_Log.PuntoVenta & ",'','DESAYUNO INCLUIDO'," & valorC & ",0"  'psv
                If conectadobd.State <> ConnectionState.Open Then conectadobd.Open()
                cConexion.AddNewRecord(conectadobd, "Ventas", campos, datos)

                'numerofactura

                Dim Id_Ventas As Double = cConexion.SlqExecuteScalar(conectadobd, "SELECT Id FROM Ventas WHERE (Proveniencia_Venta = " & User_Log.PuntoVenta & ") AND (Num_Factura = " & NumeroFactura & ") AND (Tipo = 'CRE')")


                campos = "Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo, Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento,Impuesto, Monto_Impuesto, SubtotalGravado, SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento,Tipo_Cambio_ValorCompra, Cod_MonedaVenta, Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina"


                datos = Id_Ventas & "," & 0 & ",'" & "DESAYUNO(S)" & "'," & Numero_Pax & ",0,0,0,0," & subt / Numero_Pax & "," & 0 & "," & 0 & "," & iv & "," & Numero_Pax * ((iv / 100) * (subt / Numero_Pax)) & "," & (subt) & ",0," & (subt) & ",0,0,0," & valorC & "," & codigoM & ",0,0," & (ivs / subt) * 100 & "," & ivs
                cConexion.AddNewRecord(conectadobd, "Ventas_DETALLE", campos, datos)
                Me.conectadobd.Close()

                'Dim id_ventas As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select MAX(Id) as Id from Ventas where Num_Factura=" & numerofactura & " and Proveniencia_Venta =" & PuntoVenta)

                ''HACER QUE INGRESE EL DETALLE DE LAS VENTAS
                'Dim row2 As DataRow
                'Dim precio As Double
                'precio = Me.DataSetDesayunos1.Servicios.Rows(0).Item("Tarifa")
                'campos = "Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo, Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado, SubToTalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento, Tipo_Cambio_ValorCompra, Cod_MonedaVenta, Impuesto_Ict, Monto_Impuesto_Ict, Propina, Monto_Propina"
                ''For Each row2 In Dt.Tables("ComandaTemporal").Rows
                ''    If row2("cCantidad") <> 0 Then
                ''        'If cboxMoneda.SelectedItem = "Dolares" Then
                ''        precio = row2("cPrecio") / valor
                ''        'Else
                ''        '   precio = row2("cPrecio")
                ''        ' End If
                ''        Dim can As Decimal
                ''        If separada = False Then
                ''            can = row2("cCantidad")
                ''        Else
                ''            can = row2("Cantidad")
                ''        End If
                'datos = id_ventas & ",0,'DESAYUNOS'," & Me.TablaDesayunos.Rows(num).Item("#Pax") & ",0,0,0,0," & precio & ",0,0,0," & (precio * Me.TablaDesayunos.Rows(num).Item("#Pax")) & ",0," & (precio * Me.TablaDesayunos.Rows(num).Item("#Pax")) & ",0,0,0,1,1,0,0,13,0,0"
                'cConexion.AddNewRecord(conectadobd, "Ventas_Detalle", campos, datos)

                'If MessageBox.Show("�Desea imprimir la factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                imprimir(NumeroFactura)
                'End If
            End If
            '    End If
            'Next
            'cConexion.DesConectar(conectadobd)
            'conectadobd = cConexion.Conectar("Restaurante")
        Next
        MsgBox("Facturaci�n de desayunos realizada satisfactoriamente!!!", MsgBoxStyle.Information)
        Me.ButtonGenerarImprimir.Enabled = False
    End Function
    'imprimir(NFactura)
    Private Sub imprimir(ByVal NFactura As Integer)

        Dim facturaPVE As New RptFacturaPVE  'FacturaDesayunos
        Dim Impresora As String
        Impresora = Busca_Impresora()
        If Impresora = "" Then
            MessageBox.Show("No se seleccion� ninguna impresora", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Impresora = Busca_Impresora()
            If Impresora = "" Then
                Exit Sub
            End If
        End If

        CrystalReportsConexion.LoadReportViewer(Nothing, facturaPVE, True, GetSetting("SeeSOFT", "Restaurante", "Conexion"))

        facturaPVE.PrintOptions.PrinterName = Impresora
        facturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
        facturaPVE.SetParameterValue(0, NFactura)
        facturaPVE.SetParameterValue(1, User_Log.PuntoVenta)
        CrystalReportViewer1.ReportSource = facturaPVE
        facturaPVE.PrintToPrinter(1, True, 0, 0)
        CrystalReportViewer1.Show()
    End Sub
#End Region

#Region "Evalua Usuario"
    Private Sub evalua_usuario()
        'Dim r As Integer
        'Dim nombre As String
        Try
            If conectadobd.State <> ConnectionState.Open Then Me.conectadobd.Open()
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            Me.conectadobd.Close()
            'Me.Enabled = VerificandoAcceso_a_Modulos(Me, cedula)
            'Dim usuario As String = LoginUsuario(cConexion, conectadobd, clave, Me.Text, Me.Name)
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")


            If usuario <> "" Then
                Me.Label4.Text = usuario
                Me.cbxMoneda.Enabled = True
                Me.GridControl1.Enabled = True
                ButtonGenerarImprimir.Enabled = True
            Else
                Me.Label4.Text = ""
                Me.cbxMoneda.Enabled = False
                Me.GridControl1.Enabled = False
                ButtonGenerarImprimir.Enabled = False
            End If
            'If usuario = "" Then
            '    Me.Enabled = True
            '    If Me.conectadobd.State <> ConnectionState.Open Then Me.conectadobd.Open()
            '    nombre = cConexion.SlqExecuteScalar(conectadobd, "Select Nombre from Usuarios where Clave_Interna='" & Me.txtClave.Text & "'")
            '    Me.conectadobd.Close()
            '    Me.Label4.Text = nombre
            '    Me.Panel1.Enabled = True
            '    Exit Sub
            'End If


            'If anular = True Then ' si se va a anular la factura
            '    Me.ToolBarEliminar.Enabled = True
            '    Me.ToolBarRegistrar.Enabled = False
            '    Exit Sub
            'End If
            'lbUsuario.Text = ""
            'cConexion.GetRecorset(conectadobd, "Select aperturacaja.NApertura, aperturacaja.Nombre, aperturacaja.Cedula from aperturacaja inner join Usuarios on aperturacaja.cedula = Usuarios.Cedula where Usuarios.Clave_Interna = '" & clave & "' and aperturacaja.Estado = 'A'", rs)
            'While rs.Read
            '    Num_Apertura = rs("NApertura")
            '    lbUsuario.Text = rs("Nombre")
            '    CedulaU = rs("Cedula")
            'End While
            'rs.Close()


            'End If
            'If lbUsuario.Text = "" Then
            '    If MessageBox.Show("Clave incorrecta � el usuario no tiene ninguna apertura de caja asociada, �Desea realizar una apertura de caja?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            '        Dim form As New AperturaCaja
            '        Me.Hide()
            '        form.ShowDialog()
            '        Me.Show()
            '    Else
            '        hecho = False
            '        Exit Sub
            '    End If
            'End If
            '
            'Dim i As Integer = cConexion.SlqExecuteScalar(conectadobd, "Select COUNT(*) From RelacionTrabajo where CedulaU='" & CedulaU & "'")
            'If i > 1 Then
            '    Dim saloneros As New ComandaSalonero
            '    saloneros.txtcedulaU.Text = CedulaU
            '    saloneros.lbUsuario.Text = lbUsuario.Text
            '    saloneros.ShowDialog()
            '    CedulaS = saloneros.cedulaS
            '    saloneros.Dispose()
            '    Me.txtcliente.Focus()
            '    If CedulaS = "" Then
            '        MessageBox.Show("No seleccion� ning�n salonero", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            '        ToolBar2.Buttons(0).Enabled = False
            '        Exit Sub
            '    End If
            'ElseIf i = 1 Then
            '    CedulaS = cConexion.SlqExecuteScalar(conectadobd, "Select CedulaS From RelacionTrabajo where CedulaU='" & CedulaU & "'")
            'Else
            '    MessageBox.Show("El usuario no tiene asignado ning�n salonero, no se puede realizar la factura", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            '    ToolBar2.Buttons(0).Enabled = False
            '    Exit Sub
            'End If
            'ToolBar2.Buttons(0).Enabled = True
        Catch ex As Exception
            MessageBox.Show("Error al realizar la factura: " & ex.Message, "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
#End Region

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            evalua_usuario()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGenerarImprimir.Click
        CrearFacturas()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

End Class