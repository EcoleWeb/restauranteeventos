Imports DevExpress.Utils

Public Class Cliente
    Inherits System.Windows.Forms.Form
    Dim conectadobd As New SqlClient.SqlConnection
    Dim codigo As Integer
	Dim cconexion As New ConexionR
	Dim PorcExonerado As Double = 0

#Region "Variables"
    Dim dlg As WaitDialogForm
    Dim cedula As String
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterPrecios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsPrecio1 As SIALIBEB.DsPrecio
    Friend WithEvents TipoPrecioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DsPrecio1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TipoPrecioBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SqlInsertCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents TabControlCliente As System.Windows.Forms.TabControl
    Friend WithEvents TabPageBasico As System.Windows.Forms.TabPage
    Friend WithEvents TabPageOtra As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TextVence As System.Windows.Forms.TextBox
    Friend WithEvents TextCodigo As System.Windows.Forms.TextBox
    Friend WithEvents TextNumeroCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TipoTarjeta As System.Windows.Forms.ComboBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents TextCo As System.Windows.Forms.Label
    Friend WithEvents gbExoneracion As GroupBox
    Friend WithEvents dtpFechaVencimientoExoneracion As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents txtDocExoneracion As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboTipoCliente As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents DataAdapterTipoCliente As SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommandTC As SqlClient.SqlCommand
    Friend WithEvents SqlCommandInsertTC As SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As SqlClient.SqlCommand
    Friend WithEvents SqlCommand3 As SqlClient.SqlCommand
    Friend WithEvents lbIdCliente As Label
    Dim PMU As New PerfilModulo_Class
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Cbcredito As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterNacionalidad As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCliente As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Cbtipoprecio As System.Windows.Forms.ComboBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterTipo_Tarifa As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Txtnombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtcedula As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextContacto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txttel1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txttel2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtfax1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtfax2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtemail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtplazocredito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txdireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtlimitecredito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextComision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents styleController3 As DevExpress.XtraEditors.StyleController
    Friend WithEvents styleController1 As DevExpress.XtraEditors.StyleController
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetClienteNuevo1 As SIALIBEB.DataSetClienteNuevo
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents AdapterContrato As System.Data.SqlClient.SqlDataAdapter
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Cliente))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Cbcredito = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.TextComision = New DevExpress.XtraEditors.TextEdit()
        Me.Txtplazocredito = New DevExpress.XtraEditors.TextEdit()
        Me.Txtlimitecredito = New DevExpress.XtraEditors.TextEdit()
        Me.Txdireccion = New DevExpress.XtraEditors.TextEdit()
        Me.Txtemail = New DevExpress.XtraEditors.TextEdit()
        Me.Txtfax2 = New DevExpress.XtraEditors.TextEdit()
        Me.Txtfax1 = New DevExpress.XtraEditors.TextEdit()
        Me.Txttel2 = New DevExpress.XtraEditors.TextEdit()
        Me.Txttel1 = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Cbtipoprecio = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolBar1 = New System.Windows.Forms.ToolBar()
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.AdapterCliente = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterNacionalidad = New System.Data.SqlClient.SqlDataAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.TipoPrecioBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsPrecio1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsPrecio1 = New SIALIBEB.DsPrecio()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextContacto = New DevExpress.XtraEditors.TextEdit()
        Me.Txtnombre = New DevExpress.XtraEditors.TextEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Txtcedula = New DevExpress.XtraEditors.TextEdit()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TipoPrecioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterTipo_Tarifa = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter()
        Me.styleController3 = New DevExpress.XtraEditors.StyleController()
        Me.styleController1 = New DevExpress.XtraEditors.StyleController()
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterContrato = New System.Data.SqlClient.SqlDataAdapter()
        Me.lbUsuario = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterPrecios = New System.Data.SqlClient.SqlDataAdapter()
        Me.TabControlCliente = New System.Windows.Forms.TabControl()
        Me.TabPageBasico = New System.Windows.Forms.TabPage()
        Me.TabPageOtra = New System.Windows.Forms.TabPage()
        Me.cboTipoCliente = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.gbExoneracion = New System.Windows.Forms.GroupBox()
        Me.dtpFechaVencimientoExoneracion = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtDocExoneracion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextVence = New System.Windows.Forms.TextBox()
        Me.TextCodigo = New System.Windows.Forms.TextBox()
        Me.TextNumeroCuenta = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TipoTarjeta = New System.Windows.Forms.ComboBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.TextCo = New System.Windows.Forms.Label()
        Me.DataAdapterTipoCliente = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlCommandTC = New System.Data.SqlClient.SqlCommand()
        Me.SqlCommandInsertTC = New System.Data.SqlClient.SqlCommand()
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.ClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetClienteNuevo1 = New SIALIBEB.DataSetClienteNuevo()
        Me.lbIdCliente = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        CType(Me.TextComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtplazocredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtlimitecredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txdireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtemail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtfax2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtfax1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txttel2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txttel1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.TipoPrecioBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPrecio1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPrecio1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextContacto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtnombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtcedula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipoPrecioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.styleController3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.styleController1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlCliente.SuspendLayout()
        Me.TabPageBasico.SuspendLayout()
        Me.TabPageOtra.SuspendLayout()
        Me.gbExoneracion.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.ClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetClienteNuevo1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Cbcredito)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'Cbcredito
        '
        resources.ApplyResources(Me.Cbcredito, "Cbcredito")
        Me.Cbcredito.ForeColor = System.Drawing.Color.Blue
        Me.Cbcredito.Items.AddRange(New Object() {resources.GetString("Cbcredito.Items1"), resources.GetString("Cbcredito.Items")})
        Me.Cbcredito.Name = "Cbcredito"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Name = "Label13"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.Name = "Label18"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox3, "ComboBox3")
        Me.ComboBox3.Name = "ComboBox3"
        '
        'TextComision
        '
        resources.ApplyResources(Me.TextComision, "TextComision")
        Me.TextComision.Name = "TextComision"
        '
        '
        '
        Me.TextComision.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtplazocredito
        '
        resources.ApplyResources(Me.Txtplazocredito, "Txtplazocredito")
        Me.Txtplazocredito.Name = "Txtplazocredito"
        '
        '
        '
        Me.Txtplazocredito.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtlimitecredito
        '
        resources.ApplyResources(Me.Txtlimitecredito, "Txtlimitecredito")
        Me.Txtlimitecredito.Name = "Txtlimitecredito"
        '
        '
        '
        Me.Txtlimitecredito.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txdireccion
        '
        resources.ApplyResources(Me.Txdireccion, "Txdireccion")
        Me.Txdireccion.Name = "Txdireccion"
        '
        '
        '
        Me.Txdireccion.Properties.AutoHeight = False
        Me.Txdireccion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtemail
        '
        resources.ApplyResources(Me.Txtemail, "Txtemail")
        Me.Txtemail.Name = "Txtemail"
        '
        '
        '
        Me.Txtemail.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtfax2
        '
        resources.ApplyResources(Me.Txtfax2, "Txtfax2")
        Me.Txtfax2.Name = "Txtfax2"
        '
        '
        '
        Me.Txtfax2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtfax1
        '
        resources.ApplyResources(Me.Txtfax1, "Txtfax1")
        Me.Txtfax1.Name = "Txtfax1"
        '
        '
        '
        Me.Txtfax1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txttel2
        '
        resources.ApplyResources(Me.Txttel2, "Txttel2")
        Me.Txttel2.Name = "Txttel2"
        '
        '
        '
        Me.Txttel2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txttel1
        '
        resources.ApplyResources(Me.Txttel1, "Txttel1")
        Me.Txttel1.Name = "Txttel1"
        '
        '
        '
        Me.Txttel1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        resources.ApplyResources(Me.ComboBox2, "ComboBox2")
        Me.ComboBox2.Name = "ComboBox2"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Name = "Label11"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.Name = "Label8"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Name = "Label7"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Name = "Label6"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Name = "Label5"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.Name = "Label12"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Name = "Label9"
        '
        'Cbtipoprecio
        '
        Me.Cbtipoprecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.Cbtipoprecio, "Cbtipoprecio")
        Me.Cbtipoprecio.Name = "Cbtipoprecio"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.Name = "Label14"
        '
        'CheckBox2
        '
        resources.ApplyResources(Me.CheckBox2, "CheckBox2")
        Me.CheckBox2.Name = "CheckBox2"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'ToolBar1
        '
        resources.ApplyResources(Me.ToolBar1, "ToolBar1")
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarEditar, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarCerrar})
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Name = "ToolBar1"
        '
        'ToolBarNuevo
        '
        resources.ApplyResources(Me.ToolBarNuevo, "ToolBarNuevo")
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        '
        'ToolBarEditar
        '
        resources.ApplyResources(Me.ToolBarEditar, "ToolBarEditar")
        Me.ToolBarEditar.Name = "ToolBarEditar"
        '
        'ToolBarBuscar
        '
        resources.ApplyResources(Me.ToolBarBuscar, "ToolBarBuscar")
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        '
        'ToolBarRegistrar
        '
        resources.ApplyResources(Me.ToolBarRegistrar, "ToolBarRegistrar")
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        '
        'ToolBarEliminar
        '
        resources.ApplyResources(Me.ToolBarEliminar, "ToolBarEliminar")
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        '
        'ToolBarCerrar
        '
        resources.ApplyResources(Me.ToolBarCerrar, "ToolBarCerrar")
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        Me.ImageList1.Images.SetKeyName(7, "")
        Me.ImageList1.Images.SetKeyName(8, "")
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=DESKTOP-BNAPIII\SQLEXPRESS2019;Initial Catalog=Salaberry;Integrated S" &
    "ecurity=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'AdapterCliente
        '
        Me.AdapterCliente.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterCliente.InsertCommand = Me.SqlInsertCommand
        Me.AdapterCliente.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterCliente.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_Clientes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Nombre_Juridico", "Nombre_Juridico"), New System.Data.Common.DataColumnMapping("Telefono_1", "Telefono_1"), New System.Data.Common.DataColumnMapping("Telefono_2", "Telefono_2"), New System.Data.Common.DataColumnMapping("Telefono_3", "Telefono_3"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Direccion_1", "Direccion_1"), New System.Data.Common.DataColumnMapping("Direccion_2", "Direccion_2"), New System.Data.Common.DataColumnMapping("WebPage", "WebPage"), New System.Data.Common.DataColumnMapping("Contacto", "Contacto"), New System.Data.Common.DataColumnMapping("Telefono_Contacto", "Telefono_Contacto"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Inactivo", "Inactivo"), New System.Data.Common.DataColumnMapping("Credito", "Credito"), New System.Data.Common.DataColumnMapping("Limite_Credito", "Limite_Credito"), New System.Data.Common.DataColumnMapping("Plazo_Credito", "Plazo_Credito"), New System.Data.Common.DataColumnMapping("Id_Tipo_Cliente", "Id_Tipo_Cliente"), New System.Data.Common.DataColumnMapping("CuentaContableCxCColon", "CuentaContableCxCColon"), New System.Data.Common.DataColumnMapping("DescripcionCCxCColon", "DescripcionCCxCColon"), New System.Data.Common.DataColumnMapping("CuentaContableCxCDolar", "CuentaContableCxCDolar"), New System.Data.Common.DataColumnMapping("DescripcionCCxCDolar", "DescripcionCCxCDolar"), New System.Data.Common.DataColumnMapping("CuentaContablePrepago", "CuentaContablePrepago"), New System.Data.Common.DataColumnMapping("DescripcionCCPrepago", "DescripcionCCPrepago"), New System.Data.Common.DataColumnMapping("CuentaContablePrepagoD", "CuentaContablePrepagoD"), New System.Data.Common.DataColumnMapping("DescripcionCCPrepagoD", "DescripcionCCPrepagoD"), New System.Data.Common.DataColumnMapping("Genero", "Genero"), New System.Data.Common.DataColumnMapping("DiasNoShow", "DiasNoShow"), New System.Data.Common.DataColumnMapping("Tributable", "Tributable"), New System.Data.Common.DataColumnMapping("NumDocExoneracion", "NumDocExoneracion"), New System.Data.Common.DataColumnMapping("PorcentajeExonerado", "PorcentajeExonerado"), New System.Data.Common.DataColumnMapping("FechaDocExoneracion", "FechaDocExoneracion"), New System.Data.Common.DataColumnMapping("IdTipoCliente", "IdTipoCliente"), New System.Data.Common.DataColumnMapping("Exento", "Exento"), New System.Data.Common.DataColumnMapping("Comicion", "Comicion"), New System.Data.Common.DataColumnMapping("TipoPrecio", "TipoPrecio")})})
        Me.AdapterCliente.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Juridico", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Juridico", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_3", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion_1", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion_1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion_2", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion_2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_WebPage", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "WebPage", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_Contacto", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Inactivo", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inactivo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Credito", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Limite_Credito", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Limite_Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_Credito", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tipo_Cliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tipo_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContableCxCColon", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContableCxCColon", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCxCColon", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCxCColon", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContableCxCDolar", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContableCxCDolar", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCxCDolar", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCxCDolar", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContablePrepago", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContablePrepago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCPrepago", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCPrepago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContablePrepagoD", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContablePrepagoD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCPrepagoD", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCPrepagoD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Genero", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Genero", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Genero", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Genero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DiasNoShow", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DiasNoShow", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tributable", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tributable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumDocExoneracion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDocExoneracion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PorcentajeExonerado", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PorcentajeExonerado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FechaDocExoneracion", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaDocExoneracion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdTipoCliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdTipoCliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Exento", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Exento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comicion", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comicion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TipoPrecio", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TipoPrecio", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TipoPrecio", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoPrecio", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand
        '
        Me.SqlInsertCommand.CommandText = resources.GetString("SqlInsertCommand.CommandText")
        Me.SqlInsertCommand.Connection = Me.SqlConnection1
        Me.SqlInsertCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.NVarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Nombre_Juridico", System.Data.SqlDbType.VarChar, 0, "Nombre_Juridico"), New System.Data.SqlClient.SqlParameter("@Telefono_1", System.Data.SqlDbType.NVarChar, 0, "Telefono_1"), New System.Data.SqlClient.SqlParameter("@Telefono_2", System.Data.SqlDbType.NVarChar, 0, "Telefono_2"), New System.Data.SqlClient.SqlParameter("@Telefono_3", System.Data.SqlDbType.NVarChar, 0, "Telefono_3"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Direccion_1", System.Data.SqlDbType.VarChar, 0, "Direccion_1"), New System.Data.SqlClient.SqlParameter("@Direccion_2", System.Data.SqlDbType.VarChar, 0, "Direccion_2"), New System.Data.SqlClient.SqlParameter("@WebPage", System.Data.SqlDbType.VarChar, 0, "WebPage"), New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 0, "Contacto"), New System.Data.SqlClient.SqlParameter("@Telefono_Contacto", System.Data.SqlDbType.NVarChar, 0, "Telefono_Contacto"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Inactivo", System.Data.SqlDbType.Bit, 0, "Inactivo"), New System.Data.SqlClient.SqlParameter("@Credito", System.Data.SqlDbType.Bit, 0, "Credito"), New System.Data.SqlClient.SqlParameter("@Limite_Credito", System.Data.SqlDbType.Int, 0, "Limite_Credito"), New System.Data.SqlClient.SqlParameter("@Plazo_Credito", System.Data.SqlDbType.Int, 0, "Plazo_Credito"), New System.Data.SqlClient.SqlParameter("@Id_Tipo_Cliente", System.Data.SqlDbType.Int, 0, "Id_Tipo_Cliente"), New System.Data.SqlClient.SqlParameter("@CuentaContableCxCColon", System.Data.SqlDbType.VarChar, 0, "CuentaContableCxCColon"), New System.Data.SqlClient.SqlParameter("@DescripcionCCxCColon", System.Data.SqlDbType.VarChar, 0, "DescripcionCCxCColon"), New System.Data.SqlClient.SqlParameter("@CuentaContableCxCDolar", System.Data.SqlDbType.VarChar, 0, "CuentaContableCxCDolar"), New System.Data.SqlClient.SqlParameter("@DescripcionCCxCDolar", System.Data.SqlDbType.VarChar, 0, "DescripcionCCxCDolar"), New System.Data.SqlClient.SqlParameter("@CuentaContablePrepago", System.Data.SqlDbType.VarChar, 0, "CuentaContablePrepago"), New System.Data.SqlClient.SqlParameter("@DescripcionCCPrepago", System.Data.SqlDbType.VarChar, 0, "DescripcionCCPrepago"), New System.Data.SqlClient.SqlParameter("@CuentaContablePrepagoD", System.Data.SqlDbType.VarChar, 0, "CuentaContablePrepagoD"), New System.Data.SqlClient.SqlParameter("@DescripcionCCPrepagoD", System.Data.SqlDbType.VarChar, 0, "DescripcionCCPrepagoD"), New System.Data.SqlClient.SqlParameter("@Genero", System.Data.SqlDbType.NVarChar, 0, "Genero"), New System.Data.SqlClient.SqlParameter("@DiasNoShow", System.Data.SqlDbType.Int, 0, "DiasNoShow"), New System.Data.SqlClient.SqlParameter("@Tributable", System.Data.SqlDbType.Bit, 0, "Tributable"), New System.Data.SqlClient.SqlParameter("@NumDocExoneracion", System.Data.SqlDbType.VarChar, 0, "NumDocExoneracion"), New System.Data.SqlClient.SqlParameter("@PorcentajeExonerado", System.Data.SqlDbType.Float, 0, "PorcentajeExonerado"), New System.Data.SqlClient.SqlParameter("@FechaDocExoneracion", System.Data.SqlDbType.DateTime, 0, "FechaDocExoneracion"), New System.Data.SqlClient.SqlParameter("@IdTipoCliente", System.Data.SqlDbType.Int, 0, "IdTipoCliente"), New System.Data.SqlClient.SqlParameter("@Exento", System.Data.SqlDbType.Bit, 0, "Exento"), New System.Data.SqlClient.SqlParameter("@Comicion", System.Data.SqlDbType.Float, 0, "Comicion"), New System.Data.SqlClient.SqlParameter("@TipoPrecio", System.Data.SqlDbType.VarChar, 0, "TipoPrecio")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = resources.GetString("SqlSelectCommand1.CommandText")
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.NVarChar, 0, "Cedula"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 0, "Nombre"), New System.Data.SqlClient.SqlParameter("@Nombre_Juridico", System.Data.SqlDbType.VarChar, 0, "Nombre_Juridico"), New System.Data.SqlClient.SqlParameter("@Telefono_1", System.Data.SqlDbType.NVarChar, 0, "Telefono_1"), New System.Data.SqlClient.SqlParameter("@Telefono_2", System.Data.SqlDbType.NVarChar, 0, "Telefono_2"), New System.Data.SqlClient.SqlParameter("@Telefono_3", System.Data.SqlDbType.NVarChar, 0, "Telefono_3"), New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 0, "Email"), New System.Data.SqlClient.SqlParameter("@Direccion_1", System.Data.SqlDbType.VarChar, 0, "Direccion_1"), New System.Data.SqlClient.SqlParameter("@Direccion_2", System.Data.SqlDbType.VarChar, 0, "Direccion_2"), New System.Data.SqlClient.SqlParameter("@WebPage", System.Data.SqlDbType.VarChar, 0, "WebPage"), New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 0, "Contacto"), New System.Data.SqlClient.SqlParameter("@Telefono_Contacto", System.Data.SqlDbType.NVarChar, 0, "Telefono_Contacto"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Inactivo", System.Data.SqlDbType.Bit, 0, "Inactivo"), New System.Data.SqlClient.SqlParameter("@Credito", System.Data.SqlDbType.Bit, 0, "Credito"), New System.Data.SqlClient.SqlParameter("@Limite_Credito", System.Data.SqlDbType.Int, 0, "Limite_Credito"), New System.Data.SqlClient.SqlParameter("@Plazo_Credito", System.Data.SqlDbType.Int, 0, "Plazo_Credito"), New System.Data.SqlClient.SqlParameter("@Id_Tipo_Cliente", System.Data.SqlDbType.Int, 0, "Id_Tipo_Cliente"), New System.Data.SqlClient.SqlParameter("@CuentaContableCxCColon", System.Data.SqlDbType.VarChar, 0, "CuentaContableCxCColon"), New System.Data.SqlClient.SqlParameter("@DescripcionCCxCColon", System.Data.SqlDbType.VarChar, 0, "DescripcionCCxCColon"), New System.Data.SqlClient.SqlParameter("@CuentaContableCxCDolar", System.Data.SqlDbType.VarChar, 0, "CuentaContableCxCDolar"), New System.Data.SqlClient.SqlParameter("@DescripcionCCxCDolar", System.Data.SqlDbType.VarChar, 0, "DescripcionCCxCDolar"), New System.Data.SqlClient.SqlParameter("@CuentaContablePrepago", System.Data.SqlDbType.VarChar, 0, "CuentaContablePrepago"), New System.Data.SqlClient.SqlParameter("@DescripcionCCPrepago", System.Data.SqlDbType.VarChar, 0, "DescripcionCCPrepago"), New System.Data.SqlClient.SqlParameter("@CuentaContablePrepagoD", System.Data.SqlDbType.VarChar, 0, "CuentaContablePrepagoD"), New System.Data.SqlClient.SqlParameter("@DescripcionCCPrepagoD", System.Data.SqlDbType.VarChar, 0, "DescripcionCCPrepagoD"), New System.Data.SqlClient.SqlParameter("@Genero", System.Data.SqlDbType.NVarChar, 0, "Genero"), New System.Data.SqlClient.SqlParameter("@DiasNoShow", System.Data.SqlDbType.Int, 0, "DiasNoShow"), New System.Data.SqlClient.SqlParameter("@Tributable", System.Data.SqlDbType.Bit, 0, "Tributable"), New System.Data.SqlClient.SqlParameter("@NumDocExoneracion", System.Data.SqlDbType.VarChar, 0, "NumDocExoneracion"), New System.Data.SqlClient.SqlParameter("@PorcentajeExonerado", System.Data.SqlDbType.Float, 0, "PorcentajeExonerado"), New System.Data.SqlClient.SqlParameter("@FechaDocExoneracion", System.Data.SqlDbType.DateTime, 0, "FechaDocExoneracion"), New System.Data.SqlClient.SqlParameter("@IdTipoCliente", System.Data.SqlDbType.Int, 0, "IdTipoCliente"), New System.Data.SqlClient.SqlParameter("@Exento", System.Data.SqlDbType.Bit, 0, "Exento"), New System.Data.SqlClient.SqlParameter("@Comicion", System.Data.SqlDbType.Float, 0, "Comicion"), New System.Data.SqlClient.SqlParameter("@TipoPrecio", System.Data.SqlDbType.VarChar, 0, "TipoPrecio"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Juridico", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Juridico", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_3", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion_1", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion_1", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Direccion_2", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion_2", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_WebPage", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "WebPage", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_Contacto", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Inactivo", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inactivo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Credito", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Limite_Credito", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Limite_Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_Credito", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Tipo_Cliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Tipo_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContableCxCColon", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContableCxCColon", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCxCColon", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCxCColon", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContableCxCDolar", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContableCxCDolar", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCxCDolar", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCxCDolar", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContablePrepago", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContablePrepago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCPrepago", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCPrepago", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CuentaContablePrepagoD", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContablePrepagoD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DescripcionCCPrepagoD", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCCPrepagoD", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Genero", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Genero", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Genero", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Genero", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_DiasNoShow", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DiasNoShow", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tributable", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tributable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_NumDocExoneracion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDocExoneracion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PorcentajeExonerado", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PorcentajeExonerado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FechaDocExoneracion", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaDocExoneracion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdTipoCliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdTipoCliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Exento", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Exento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comicion", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comicion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_TipoPrecio", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "TipoPrecio", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_TipoPrecio", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoPrecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT codigo, descripcion, ABREV FROM Nacionalidad"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Nacionalidad(descripcion, ABREV) VALUES (@descripcion, @ABREV); SELEC" &
    "T codigo, descripcion, ABREV FROM Nacionalidad WHERE (codigo = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"), New System.Data.SqlClient.SqlParameter("@ABREV", System.Data.SqlDbType.VarChar, 2, "ABREV")})
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 50, "descripcion"), New System.Data.SqlClient.SqlParameter("@ABREV", System.Data.SqlDbType.VarChar, 2, "ABREV"), New System.Data.SqlClient.SqlParameter("@Original_codigo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ABREV", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ABREV", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descripcion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@codigo", System.Data.SqlDbType.Int, 4, "codigo")})
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_codigo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ABREV", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ABREV", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descripcion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descripcion", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterNacionalidad
        '
        Me.AdapterNacionalidad.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterNacionalidad.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterNacionalidad.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterNacionalidad.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Nacionalidad", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("codigo", "codigo"), New System.Data.Common.DataColumnMapping("descripcion", "descripcion"), New System.Data.Common.DataColumnMapping("ABREV", "ABREV")})})
        Me.AdapterNacionalidad.UpdateCommand = Me.SqlUpdateCommand2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Txdireccion)
        Me.GroupBox1.Controls.Add(Me.GroupBox6)
        Me.GroupBox1.Controls.Add(Me.Txtemail)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Txtfax2)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Txtfax1)
        Me.GroupBox1.Controls.Add(Me.TextEdit1)
        Me.GroupBox1.Controls.Add(Me.Txttel2)
        Me.GroupBox1.Controls.Add(Me.TextContacto)
        Me.GroupBox1.Controls.Add(Me.Txtnombre)
        Me.GroupBox1.Controls.Add(Me.Txttel1)
        Me.GroupBox1.Controls.Add(Me.TextComision)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Txtcedula)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.ComboBox4)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.TipoPrecioBindingSource1
        Me.ComboBox4.DisplayMember = "Descripcion"
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.ComboBox4, "ComboBox4")
        Me.ComboBox4.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.ValueMember = "Descripcion"
        '
        'TipoPrecioBindingSource1
        '
        Me.TipoPrecioBindingSource1.DataMember = "Tipo_Precio"
        Me.TipoPrecioBindingSource1.DataSource = Me.DsPrecio1BindingSource
        '
        'DsPrecio1BindingSource
        '
        Me.DsPrecio1BindingSource.DataSource = Me.DsPrecio1
        Me.DsPrecio1BindingSource.Position = 0
        '
        'DsPrecio1
        '
        Me.DsPrecio1.DataSetName = "DsPrecio"
        Me.DsPrecio1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Name = "Label1"
        '
        'TextEdit1
        '
        resources.ApplyResources(Me.TextEdit1, "TextEdit1")
        Me.TextEdit1.Name = "TextEdit1"
        '
        '
        '
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'TextContacto
        '
        resources.ApplyResources(Me.TextContacto, "TextContacto")
        Me.TextContacto.Name = "TextContacto"
        '
        '
        '
        Me.TextContacto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Txtnombre
        '
        resources.ApplyResources(Me.Txtnombre, "Txtnombre")
        Me.Txtnombre.Name = "Txtnombre"
        '
        '
        '
        Me.Txtnombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.RoyalBlue
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Name = "Label15"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Name = "Label3"
        '
        'ComboBox1
        '
        resources.ApplyResources(Me.ComboBox1, "ComboBox1")
        Me.ComboBox1.Name = "ComboBox1"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.Name = "Label19"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.Name = "Label17"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Name = "Label4"
        '
        'Txtcedula
        '
        resources.ApplyResources(Me.Txtcedula, "Txtcedula")
        Me.Txtcedula.Name = "Txtcedula"
        '
        '
        '
        Me.Txtcedula.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.Name = "Label16"
        '
        'TextBox1
        '
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        '
        '
        '
        Me.TextBox1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.CheckBox1, "CheckBox1")
        Me.CheckBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'TipoPrecioBindingSource
        '
        Me.TipoPrecioBindingSource.DataMember = "Tipo_Precio"
        Me.TipoPrecioBindingSource.DataSource = Me.DsPrecio1BindingSource
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Codigo, Nombre, Cuenta_Contable FROM Tipo_Tarifa"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Tipo_Tarifa(Nombre, Cuenta_Contable) VALUES (@Nombre, @Cuenta_Contabl" &
    "e); SELECT Codigo, Nombre, Cuenta_Contable FROM Tipo_Tarifa WHERE (Codigo = @@ID" &
    "ENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 70, "Nombre"), New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 32, "Cuenta_Contable")})
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 70, "Nombre"), New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 32, "Cuenta_Contable"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 70, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.Int, 4, "Codigo")})
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Tipo_Tarifa WHERE (Codigo = @Original_Codigo) AND (Cuenta_Contable = " &
    "@Original_Cuenta_Contable) AND (Nombre = @Original_Nombre)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 70, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterTipo_Tarifa
        '
        Me.AdapterTipo_Tarifa.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterTipo_Tarifa.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterTipo_Tarifa.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterTipo_Tarifa.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tipo_Tarifa", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Cuenta_Contable", "Cuenta_Contable")})})
        Me.AdapterTipo_Tarifa.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda")})
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterMoneda.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.AdapterMoneda.UpdateCommand = Me.SqlUpdateCommand3
        '
        'styleController3
        '
        Me.styleController3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.styleController3.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Comic Sans MS", 9.0!), "", CType((((((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                Or DevExpress.Utils.StyleOptions.UseDrawEndEllipsis) _
                Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                Or DevExpress.Utils.StyleOptions.UseFont) _
                Or DevExpress.Utils.StyleOptions.UseForeColor) _
                Or DevExpress.Utils.StyleOptions.UseHorzAlignment) _
                Or DevExpress.Utils.StyleOptions.UseImage) _
                Or DevExpress.Utils.StyleOptions.UseWordWrap) _
                Or DevExpress.Utils.StyleOptions.UseVertAlignment), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Yellow, System.Drawing.Color.DarkGreen)
        '
        'styleController1
        '
        Me.styleController1.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Id, Nombre, Observaciones, TemporadaHotel, TemporadasPropias, Id_Temporada" &
    "Generales FROM Contrato"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = resources.GetString("SqlInsertCommand5.CommandText")
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 75, "Nombre"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@TemporadaHotel", System.Data.SqlDbType.Bit, 1, "TemporadaHotel"), New System.Data.SqlClient.SqlParameter("@TemporadasPropias", System.Data.SqlDbType.Bit, 1, "TemporadasPropias"), New System.Data.SqlClient.SqlParameter("@Id_TemporadaGenerales", System.Data.SqlDbType.BigInt, 8, "Id_TemporadaGenerales")})
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = resources.GetString("SqlUpdateCommand5.CommandText")
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 75, "Nombre"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@TemporadaHotel", System.Data.SqlDbType.Bit, 1, "TemporadaHotel"), New System.Data.SqlClient.SqlParameter("@TemporadasPropias", System.Data.SqlDbType.Bit, 1, "TemporadasPropias"), New System.Data.SqlClient.SqlParameter("@Id_TemporadaGenerales", System.Data.SqlDbType.BigInt, 8, "Id_TemporadaGenerales"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_TemporadaGenerales", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TemporadaGenerales", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TemporadaHotel", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TemporadaHotel", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TemporadasPropias", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TemporadasPropias", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = resources.GetString("SqlDeleteCommand5.CommandText")
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_TemporadaGenerales", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TemporadaGenerales", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TemporadaHotel", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TemporadaHotel", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TemporadasPropias", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TemporadasPropias", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterContrato
        '
        Me.AdapterContrato.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterContrato.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterContrato.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterContrato.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Contrato", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("TemporadaHotel", "TemporadaHotel"), New System.Data.Common.DataColumnMapping("TemporadasPropias", "TemporadasPropias"), New System.Data.Common.DataColumnMapping("Id_TemporadaGenerales", "Id_TemporadaGenerales")})})
        Me.AdapterContrato.UpdateCommand = Me.SqlUpdateCommand5
        '
        'lbUsuario
        '
        resources.ApplyResources(Me.lbUsuario, "lbUsuario")
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.Name = "lbUsuario"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'TextBox2
        '
        resources.ApplyResources(Me.TextBox2, "TextBox2")
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Name = "TextBox2"
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT     Descripcion, Codigo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         Hotel.dbo.Tipo_Precio"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = resources.GetString("SqlUpdateCommand6.CommandText")
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.Int, 4, "Codigo")})
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM [Tipo_Precio] WHERE (([Descripcion] = @Original_Descripcion) AND ([Co" &
    "digo] = @Original_Codigo))"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterPrecios
        '
        Me.AdapterPrecios.DeleteCommand = Me.SqlDeleteCommand6
        Me.AdapterPrecios.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterPrecios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Table", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo")})})
        Me.AdapterPrecios.UpdateCommand = Me.SqlUpdateCommand6
        '
        'TabControlCliente
        '
        Me.TabControlCliente.Controls.Add(Me.TabPageBasico)
        Me.TabControlCliente.Controls.Add(Me.TabPageOtra)
        resources.ApplyResources(Me.TabControlCliente, "TabControlCliente")
        Me.TabControlCliente.Name = "TabControlCliente"
        Me.TabControlCliente.SelectedIndex = 0
        '
        'TabPageBasico
        '
        Me.TabPageBasico.Controls.Add(Me.GroupBox1)
        resources.ApplyResources(Me.TabPageBasico, "TabPageBasico")
        Me.TabPageBasico.Name = "TabPageBasico"
        Me.TabPageBasico.UseVisualStyleBackColor = True
        '
        'TabPageOtra
        '
        Me.TabPageOtra.Controls.Add(Me.cboTipoCliente)
        Me.TabPageOtra.Controls.Add(Me.Label23)
        Me.TabPageOtra.Controls.Add(Me.gbExoneracion)
        Me.TabPageOtra.Controls.Add(Me.GroupBox5)
        Me.TabPageOtra.Controls.Add(Me.Label18)
        Me.TabPageOtra.Controls.Add(Me.ComboBox3)
        Me.TabPageOtra.Controls.Add(Me.GroupBox3)
        Me.TabPageOtra.Controls.Add(Me.Label12)
        Me.TabPageOtra.Controls.Add(Me.Label7)
        Me.TabPageOtra.Controls.Add(Me.Txtplazocredito)
        Me.TabPageOtra.Controls.Add(Me.CheckBox1)
        Me.TabPageOtra.Controls.Add(Me.Label8)
        Me.TabPageOtra.Controls.Add(Me.Txtlimitecredito)
        Me.TabPageOtra.Controls.Add(Me.ComboBox2)
        Me.TabPageOtra.Controls.Add(Me.CheckBox2)
        Me.TabPageOtra.Controls.Add(Me.Cbtipoprecio)
        Me.TabPageOtra.Controls.Add(Me.Label9)
        resources.ApplyResources(Me.TabPageOtra, "TabPageOtra")
        Me.TabPageOtra.Name = "TabPageOtra"
        Me.TabPageOtra.UseVisualStyleBackColor = True
        '
        'cboTipoCliente
        '
        Me.cboTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCliente.FormattingEnabled = True
        resources.ApplyResources(Me.cboTipoCliente, "cboTipoCliente")
        Me.cboTipoCliente.Name = "cboTipoCliente"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'gbExoneracion
        '
        Me.gbExoneracion.Controls.Add(Me.dtpFechaVencimientoExoneracion)
        Me.gbExoneracion.Controls.Add(Me.Label22)
        Me.gbExoneracion.Controls.Add(Me.txtDocExoneracion)
        Me.gbExoneracion.Controls.Add(Me.Label10)
        resources.ApplyResources(Me.gbExoneracion, "gbExoneracion")
        Me.gbExoneracion.ForeColor = System.Drawing.Color.Black
        Me.gbExoneracion.Name = "gbExoneracion"
        Me.gbExoneracion.TabStop = False
        '
        'dtpFechaVencimientoExoneracion
        '
        Me.dtpFechaVencimientoExoneracion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        resources.ApplyResources(Me.dtpFechaVencimientoExoneracion, "dtpFechaVencimientoExoneracion")
        Me.dtpFechaVencimientoExoneracion.Name = "dtpFechaVencimientoExoneracion"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'txtDocExoneracion
        '
        resources.ApplyResources(Me.txtDocExoneracion, "txtDocExoneracion")
        Me.txtDocExoneracion.Name = "txtDocExoneracion"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GrayText
        Me.GroupBox5.Controls.Add(Me.TextVence)
        Me.GroupBox5.Controls.Add(Me.TextCodigo)
        Me.GroupBox5.Controls.Add(Me.TextNumeroCuenta)
        Me.GroupBox5.Controls.Add(Me.Label47)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Controls.Add(Me.Label31)
        Me.GroupBox5.Controls.Add(Me.TipoTarjeta)
        Me.GroupBox5.Controls.Add(Me.Label46)
        Me.GroupBox5.Controls.Add(Me.TextCo)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'TextVence
        '
        Me.TextVence.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextVence, "TextVence")
        Me.TextVence.Name = "TextVence"
        '
        'TextCodigo
        '
        Me.TextCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextCodigo, "TextCodigo")
        Me.TextCodigo.Name = "TextCodigo"
        '
        'TextNumeroCuenta
        '
        Me.TextNumeroCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.TextNumeroCuenta, "TextNumeroCuenta")
        Me.TextNumeroCuenta.Name = "TextNumeroCuenta"
        '
        'Label47
        '
        Me.Label47.BackColor = System.Drawing.Color.RoyalBlue
        resources.ApplyResources(Me.Label47, "Label47")
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Name = "Label47"
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label33, "Label33")
        Me.Label33.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label33.Name = "Label33"
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label31, "Label31")
        Me.Label31.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label31.Name = "Label31"
        '
        'TipoTarjeta
        '
        resources.ApplyResources(Me.TipoTarjeta, "TipoTarjeta")
        Me.TipoTarjeta.Items.AddRange(New Object() {resources.GetString("TipoTarjeta.Items"), resources.GetString("TipoTarjeta.Items1"), resources.GetString("TipoTarjeta.Items2"), resources.GetString("TipoTarjeta.Items3")})
        Me.TipoTarjeta.Name = "TipoTarjeta"
        '
        'Label46
        '
        Me.Label46.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.Label46, "Label46")
        Me.Label46.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label46.Name = "Label46"
        '
        'TextCo
        '
        Me.TextCo.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TextCo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.TextCo, "TextCo")
        Me.TextCo.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TextCo.Name = "TextCo"
        '
        'DataAdapterTipoCliente
        '
        Me.DataAdapterTipoCliente.DeleteCommand = Me.SqlCommandTC
        Me.DataAdapterTipoCliente.InsertCommand = Me.SqlCommandInsertTC
        Me.DataAdapterTipoCliente.SelectCommand = Me.SqlCommand2
        Me.DataAdapterTipoCliente.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TipoCliente", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("descripcion", "descripcion"), New System.Data.Common.DataColumnMapping("porcentajeIVA", "porcentajeIVA")})})
        Me.DataAdapterTipoCliente.UpdateCommand = Me.SqlCommand3
        '
        'SqlCommandTC
        '
        Me.SqlCommandTC.CommandText = "DELETE FROM [TipoCliente] WHERE (([id] = @Original_id) AND ([descripcion] = @Orig" &
    "inal_descripcion) AND ([porcentajeIVA] = @Original_porcentajeIVA))"
        Me.SqlCommandTC.Connection = Me.SqlConnection1
        Me.SqlCommandTC.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_porcentajeIVA", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "porcentajeIVA", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlCommandInsertTC
        '
        Me.SqlCommandInsertTC.CommandText = "INSERT INTO [TipoCliente] ([descripcion], [porcentajeIVA]) VALUES (@descripcion, " &
    "@porcentajeIVA);" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "SELECT id, descripcion, porcentajeIVA FROM TipoCliente WHERE (" &
    "id = SCOPE_IDENTITY())"
        Me.SqlCommandInsertTC.Connection = Me.SqlConnection1
        Me.SqlCommandInsertTC.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 0, "descripcion"), New System.Data.SqlClient.SqlParameter("@porcentajeIVA", System.Data.SqlDbType.Int, 0, "porcentajeIVA")})
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT        TipoCliente.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM            TipoCliente"
        Me.SqlCommand2.Connection = Me.SqlConnection1
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = resources.GetString("SqlCommand3.CommandText")
        Me.SqlCommand3.Connection = Me.SqlConnection1
        Me.SqlCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 0, "descripcion"), New System.Data.SqlClient.SqlParameter("@porcentajeIVA", System.Data.SqlDbType.Int, 0, "porcentajeIVA"), New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_porcentajeIVA", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "porcentajeIVA", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int, 4, "id")})
        '
        'ClienteBindingSource
        '
        Me.ClienteBindingSource.DataMember = "Cliente"
        Me.ClienteBindingSource.DataSource = Me.DataSetClienteNuevo1
        '
        'DataSetClienteNuevo1
        '
        Me.DataSetClienteNuevo1.DataSetName = "DataSetClienteNuevo"
        Me.DataSetClienteNuevo1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSetClienteNuevo1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lbIdCliente
        '
        resources.ApplyResources(Me.lbIdCliente, "lbIdCliente")
        Me.lbIdCliente.Name = "lbIdCliente"
        '
        'Cliente
        '
        resources.ApplyResources(Me, "$this")
        Me.Controls.Add(Me.lbIdCliente)
        Me.Controls.Add(Me.TabControlCliente)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Cliente"
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.TextComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtplazocredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtlimitecredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txdireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtemail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtfax2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtfax1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txttel2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txttel1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.TipoPrecioBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPrecio1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPrecio1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextContacto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtnombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtcedula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipoPrecioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.styleController3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.styleController1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlCliente.ResumeLayout(False)
        Me.TabPageBasico.ResumeLayout(False)
        Me.TabPageOtra.ResumeLayout(False)
        Me.TabPageOtra.PerformLayout()
        Me.gbExoneracion.ResumeLayout(False)
        Me.gbExoneracion.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.ClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetClienteNuevo1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Codigo General"
    Private Sub carga()
        dlg = New WaitDialogForm("Cargando Clientes Disponibles...")
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Hotel", "CONEXION")
            AdapterTipo_Tarifa.Fill(DataSetClienteNuevo1.Tipo_Tarifa)
            AdapterNacionalidad.Fill(DataSetClienteNuevo1.Nacionalidad)

            AdapterMoneda.Fill(DataSetClienteNuevo1.Moneda)
            AdapterContrato.Fill(Me.DataSetClienteNuevo1.Contrato)
            AdapterCliente.Fill(Me.DataSetClienteNuevo1.Cliente)
            DataAdapterTipoCliente.Fill(Me.DataSetClienteNuevo1.TipoCliente)

            '  DataSetClienteNuevo1.Cliente.Restriccion_CuentaColumn.DefaultValue = "false"
            '   DataSetClienteNuevo1.Cliente.AgenciaColumn.DefaultValue = "false"
            DataSetClienteNuevo1.Cliente.ExentoColumn.DefaultValue = "false"
            ' DataSetClienteNuevo1.Cliente.ImpustoVentasColumn.DefaultValue = "0"
            DataSetClienteNuevo1.Cliente.ComicionColumn.DefaultValue = "0"
            DataSetClienteNuevo1.Cliente.Limite_CreditoColumn.DefaultValue = "0"
            DataSetClienteNuevo1.Cliente.Plazo_CreditoColumn.DefaultValue = "0"
            '   DataSetClienteNuevo1.Cliente.Cliente_MorosoColumn.DefaultValue = False
            DataSetClienteNuevo1.Cliente.CreditoColumn.DefaultValue = "1"
            '   DataSetClienteNuevo1.Cliente.Id_ContratoColumn.DefaultValue = "0"
            ' DataSetClienteNuevo1.Cliente.ContratoColumn.DefaultValue = False
            ValorDefectoNacionalidad()
            ValorDefectoTipoPrecio()
            ValorDefectoMoneda()
            ValorDefectoTipoCliente()
            Binding()
            '  PorcExonerado = Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("ImpustoVentas")
            InhabilitarControles()
            Me.ToolBar1.Buttons(3).Enabled = False
            dlg.Close()
        Catch ex As Exception
            dlg.Close()
            MsgBox(ex.ToString)
            MsgBox("Problemas al iniciar." & vbCrLf & "El fomulario Intente Iniciar el formulario Otra vez, si el problema persiste comuniqueselo al administrador del sistema", MsgBoxStyle.Critical, "Atenci�n")
            Me.Close()
        End Try
    End Sub

    Private Sub InhabilitarControles()
        ComboBox4.Enabled = True
        Me.ComboBox3.Enabled = False
        Me.CheckBox2.Enabled = False
        Me.Cbtipoprecio.Enabled = False
        Me.gbExoneracion.Enabled = False
        Me.cboTipoCliente.Enabled = False
        Me.TextComision.Enabled = False
        Me.Txtplazocredito.Enabled = False
        Me.Txtlimitecredito.Enabled = False
        TextEdit1.Enabled = False
        Me.Txtfax1.Enabled = False
        Me.Txttel1.Enabled = False
        Me.Txdireccion.Enabled = False
        Me.Txtemail.Enabled = False
        Me.Txtfax2.Enabled = False
        Me.Txttel2.Enabled = False
        Me.ComboBox1.Enabled = False
        Me.CheckBox1.Enabled = False
        Me.TextContacto.Enabled = False
        Me.Txtcedula.Enabled = False
        Me.TextBox1.Enabled = False
        Me.Txtnombre.Enabled = False
        Me.Cbcredito.Enabled = False
        Me.ComboBox2.Enabled = False
        Me.TipoTarjeta.Enabled = False
        Me.TextNumeroCuenta.Enabled = False
        Me.TextCodigo.Enabled = False
        Me.TextVence.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        ComboBox4.Enabled = True
        Me.ComboBox3.Enabled = True
        Me.CheckBox2.Enabled = True
        Me.Cbtipoprecio.Enabled = True
        Me.gbExoneracion.Enabled = True
        Me.cboTipoCliente.Enabled = True
        Me.TextComision.Enabled = True
        TextEdit1.Enabled = True
        Me.Txtplazocredito.Enabled = True
        Me.Txtlimitecredito.Enabled = True
        Me.Txtfax1.Enabled = True
        Me.Txttel1.Enabled = True
        Me.Txdireccion.Enabled = True
        Me.Txtemail.Enabled = True
        Me.Txtfax2.Enabled = True
        Me.Txttel2.Enabled = True
        Me.ComboBox1.Enabled = True
        Me.CheckBox1.Enabled = True
        Me.TextContacto.Enabled = True
        Me.Txtcedula.Enabled = True
        Me.TextBox1.Enabled = True
        Me.Txtnombre.Enabled = True
        Me.Cbcredito.Enabled = True
        Me.ComboBox2.Enabled = True
        Me.TipoTarjeta.Enabled = True
        Me.TextNumeroCuenta.Enabled = True
        Me.TextCodigo.Enabled = True
        Me.TextVence.Enabled = True
    End Sub

    Private Sub Binding()

        Me.lbIdCliente.Text = Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("Id")
        Me.Txtplazocredito.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Plazo_Credito"))
        Me.Txtlimitecredito.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Limite_Credito"))
        'Me.Txtfax1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Fax1"))
        Me.Txttel1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Telefono_1"))
        Me.Txdireccion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Direccion_1"))
        Me.Txtemail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Email"))
        ' Me.Txtfax2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Fax2"))
        Me.Txttel2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Telefono_2"))
        ' Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetClienteNuevo1, "Cliente.Nacionalidad"))
        Me.ComboBox1.DataSource = Me.DataSetClienteNuevo1
        Me.ComboBox1.DisplayMember = "Nacionalidad.descripcion"
        Me.ComboBox1.ValueMember = "Nacionalidad.codigo"
        ' Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetClienteNuevo1, "Cliente.Agencia"))
        Me.TextContacto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Contacto"))
        Me.Txtcedula.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Cedula"))
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Observaciones"))
        Me.Txtnombre.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Nombre"))
        Me.Cbcredito.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.Credito"))
        Me.cboTipoCliente.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetClienteNuevo1, "Cliente.IdTipoCliente"))

        Me.txtDocExoneracion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.NumDocExoneracion"))
        Me.dtpFechaVencimientoExoneracion.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DataSetClienteNuevo1, "Cliente.FechaDocExoneracion"))
        Me.TextComision.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.Comicion")) 'psv
        'Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetClienteNuevo1, "Cliente.CodMoneda"))
        Me.ComboBox2.DataSource = Me.DataSetClienteNuevo1
        Me.ComboBox2.DisplayMember = "Moneda.Simbolo"
        Me.ComboBox2.ValueMember = "Moneda.CodMoneda"
        ' Me.TextEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetClienteNuevo1, "Cliente.NombreJuridico"))
        '  Me.TextVence.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.Vence"))
        ' Me.TextCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.CodigoSeguridad"))
        'Me.TextNumeroCuenta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.NumeroTarjeta"))
        '  Me.TipoTarjeta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetClienteNuevo1, "Cliente.Tipo"))
        '  Me.Cbtipoprecio.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetClienteNuevo1, "Cliente.Tipo_Precio"))
        Me.Cbtipoprecio.DataSource = Me.DataSetClienteNuevo1
        Me.Cbtipoprecio.DisplayMember = "Tipo_Tarifa.Nombre"
        Me.Cbtipoprecio.ValueMember = "Tipo_Tarifa.Codigo"
        'Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetClienteNuevo1, "Cliente.Id_Contrato"))
        Me.ComboBox3.DataSource = Me.DataSetClienteNuevo1
        Me.ComboBox3.DisplayMember = "Contrato.Nombre"
        Me.ComboBox3.ValueMember = "Contrato.Id"
        '     Me.CheckBox2.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetClienteNuevo1, "Cliente.Contrato"))

    End Sub

    Private Sub ValorDefectoNacionalidad()
        If Me.DataSetClienteNuevo1.Nacionalidad.Rows.Count > 0 Then
            'Me.DataSetClienteNuevo1.Cliente.NacionalidadColumn.DefaultValue = Me.DataSetClienteNuevo1.Nacionalidad.Rows(0).Item("Codigo")
        End If
    End Sub

    Private Sub ValorDefectoMoneda()
        If Me.DataSetClienteNuevo1.Moneda.Rows.Count > 0 Then
            'Me.DataSetClienteNuevo1.Cliente.CodMonedaColumn.DefaultValue = Me.DataSetClienteNuevo1.Moneda.Rows(0).Item("CodMoneda")
        End If
    End Sub

    Private Sub ValorDefectoTipoCliente()
        If Me.DataSetClienteNuevo1.TipoCliente.Rows.Count > 0 Then
            Me.DataSetClienteNuevo1.Cliente.IdTipoClienteColumn.DefaultValue = Me.DataSetClienteNuevo1.TipoCliente.Rows(0).Item("Id")
            Me.DataSetClienteNuevo1.Cliente.FechaDocExoneracionColumn.DefaultValue = Today
            Me.DataSetClienteNuevo1.Cliente.NumDocExoneracionColumn.DefaultValue = ""
            Me.cboTipoCliente.DataSource = Me.DataSetClienteNuevo1
            Me.cboTipoCliente.DisplayMember = "TipoCliente.descripcion"
            Me.cboTipoCliente.ValueMember = "TipoCliente.Id"
        End If
    End Sub
    Private Sub ValorDefectoTipoPrecio()
        If Me.DataSetClienteNuevo1.Tipo_Tarifa.Rows.Count > 0 Then
            '  Me.DataSetClienteNuevo1.Cliente.Tipo_PrecioColumn.DefaultValue = Me.DataSetClienteNuevo1.Tipo_Tarifa.Rows(0).Item("Codigo")
        End If
    End Sub

#End Region

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(cedula, Me.Name)
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Agregar()

            Case 2 : If PMU.Update Then Me.Editar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 3 : Buscar()

            Case 4 : If PMU.Update Then Me.Guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 5 : If PMU.Delete Then Eliminar() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 6 : Me.Close()

        End Select
    End Sub

    Private Sub Editar()
        If Me.ToolBar1.Buttons(1).Text = "Editar" Then 'n si ya hay un registropendiente por agregar
            Me.ToolBar1.Buttons(1).Text = "Cancelar"
            Me.ToolBar1.Buttons(1).ImageIndex = 4
            HabilitarControles()
            ToolBar1.Buttons(0).Enabled = False
            ToolBar1.Buttons(2).Enabled = False
            ToolBar1.Buttons(3).Enabled = True
            ToolBar1.Buttons(4).Enabled = False
            'trabaja por contratos
            If Me.CheckBox2.Checked = True Then
                If Me.ToolBarNuevo.Text = "Cancelar" Or Me.ToolBarEditar.Text = "Cancelar" Then
                    ComboBox3.Enabled = True
                End If
                'No trabaja por contratos
            Else
                'si se esta haciendo edici�n
                If Me.ToolBarNuevo.Text = "Cancelar" Or Me.ToolBarEditar.Text = "Cancelar" Then
                    ComboBox3.SelectedIndex = -1
                End If
                ComboBox3.Enabled = False
            End If
        Else
            Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").CancelCurrentEdit()
            Me.DataSetClienteNuevo1.RejectChanges()
            Me.ToolBar1.Buttons(1).Text = "Editar"
            Me.ToolBar1.Buttons(1).ImageIndex = 5
            InhabilitarControles()
            ToolBar1.Buttons(0).Enabled = True
            ToolBar1.Buttons(2).Enabled = True
            ToolBar1.Buttons(3).Enabled = False
            ToolBar1.Buttons(4).Enabled = True

        End If

    End Sub
    Private Sub ValoresRequeridos()
        Txtnombre.BackColor = System.Drawing.Color.Linen
        Txtcedula.BackColor = System.Drawing.Color.Linen
        Txttel1.BackColor = System.Drawing.Color.Linen
    End Sub

    Private Sub QuitarValoresRequeridos()
        Txtnombre.BackColor = System.Drawing.Color.White
        Txtcedula.BackColor = System.Drawing.Color.White
        Txttel1.BackColor = System.Drawing.Color.White
    End Sub

    Private Sub Agregar()
        If ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            ToolBar1.Buttons(0).Text = "Cancelar"
            ToolBar1.Buttons(0).ImageIndex = 4

            Try 'inicia la edicion
                lbIdCliente.Text = "0"
                BindingContext(DataSetClienteNuevo1, "Cliente").EndCurrentEdit()
                BindingContext(DataSetClienteNuevo1, "Cliente").AddNew()

                TextEdit1.Text = ""
                Txttel1.Text = ""
                Txdireccion.Text = ""
                Txtfax1.Text = ""
                Txtemail.Text = ""
                TextContacto.Text = ""
                TextBox1.Text = ""
                ValoresRequeridos()
                ToolBar1.Buttons(1).Enabled = False
                ToolBar1.Buttons(2).Enabled = False
                ToolBar1.Buttons(3).Enabled = True
                ToolBar1.Buttons(4).Enabled = False
                HabilitarControles()
                'trabaja por contratos
                If CheckBox2.Checked = True Then
                    If ToolBarNuevo.Text = "Cancelar" Or ToolBarEditar.Text = "Cancelar" Then
                        ComboBox3.Enabled = True
                    End If
                    'No trabaja por contratos
                Else
                    'si se esta haciendo edici�n
                    If ToolBarNuevo.Text = "Cancelar" Or ToolBarEditar.Text = "Cancelar" Then
                        ComboBox3.SelectedIndex = -1
                    End If
                    ComboBox3.Enabled = False
                End If
                Txtnombre.Focus()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                BindingContext(DataSetClienteNuevo1, "Cliente").CancelCurrentEdit()
                QuitarValoresRequeridos()
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                ToolBar1.Buttons(1).Enabled = True
                ToolBar1.Buttons(2).Enabled = True
                ToolBar1.Buttons(3).Enabled = False
                ToolBar1.Buttons(4).Enabled = True
                InhabilitarControles()

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Sub

    Private Sub Buscar()
        If Me.validar Then
            Dim Buscar1 As New BuscarCliente(Me.DataSetClienteNuevo1.Cliente)
            If Buscar1.ShowDialog() = DialogResult.OK Then
                Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Position = Buscar1.Pos
                '  Me.ComboBox4.Text = Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("TipoPrecio")
                lbIdCliente.Text = Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("Id")
                '   PorcExonerado = Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("ImpustoVentas")
                Buscar1.Dispose()
            Else
            End If
        End If
    End Sub

    Private Sub Eliminar()
        Dim resp As Integer
        Try 'se intenta hacer
            If Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Count > 0 Then   ' si hay Temporadas
                resp = MessageBox.Show("�Desea eliminar este Cliente", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").RemoveAt(BindingContext(Me.DataSetClienteNuevo1, "Cliente").Position)
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Guardar()
                Else
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").CancelCurrentEdit()
                End If
            Else
                MsgBox("No Existen Clientes  para eliminar", MsgBoxStyle.Information)
            End If
        Catch eEndEdit As System.Exception
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
    End Sub

    Private Sub Guardar()
        Dim resp As Integer
        If validar() Then
            resp = MessageBox.Show("�Deseas Guardar los cambios?", "Hotel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            If resp = 6 Then
                Try
                    Dim Id As Integer = 0

                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("TipoPrecio") = Me.ComboBox4.Text

                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("IdTipoCliente") = cboTipoCliente.SelectedValue
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("NumDocExoneracion") = txtDocExoneracion.Text
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("FechaDocExoneracion") = dtpFechaVencimientoExoneracion.Value
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("Nombre_Juridico") = TextEdit1.Text
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("PorcentajeExonerado") = 0
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("Id_Tipo_Cliente") = 1
                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").Current("Credito") = Cbcredito.SelectedIndex


                    Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").EndCurrentEdit()
                    Me.AdapterCliente.Update(Me.DataSetClienteNuevo1, "Cliente")
                    Me.DataSetClienteNuevo1.AcceptChanges()
                    Me.ToolBar1.Buttons(0).Enabled = True
                    Me.ToolBar1.Buttons(1).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = True
                    Me.ToolBar1.Buttons(3).Enabled = False
                    Me.ToolBar1.Buttons(4).Enabled = True
                    Me.ToolBar1.Buttons(1).Text = "Editar"
                    Me.ToolBar1.Buttons(1).ImageIndex = 5
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    InhabilitarControles()
                    QuitarValoresRequeridos()
                    ' PSV
                    If Me.conectadobd.State <> ConnectionState.Open Then Me.conectadobd.Open()
                    codigo = cconexion.SlqExecuteScalar(conectadobd, "Select MAX(Id) as Codigo From Cliente")
                    Me.conectadobd.Close()
                    Dim datos As String
                    Dim sentence As String
                    datos = "TipoPrecio='" & Me.ComboBox4.Text & "'"
                    sentence = " Id=" & codigo
                    If Me.conectadobd.State <> ConnectionState.Open Then Me.conectadobd.Open()
                    '  cconexion.UpdateRecords(conectadobd, "Cliente", datos, sentence)

                    'Try
                    '    Dim Ejecutar As New SqlClient.SqlCommand

                    '    If lbIdCliente.Text <> "0" Then
                    '        Ejecutar.CommandText = "UPDATE Cliente SET NumDocExoneracion='" & txtDocExoneracion.Text & "', IdTipoCliente=" & cboTipoCliente.SelectedValue & ",FechaDocExoneracion=@FechaDocExoneracion , Nombre = '" & Txtnombre.Text & "' ,Telefono1 = '" & Txttel1.Text & "' ,Email = '" & Txtemail.Text & "' WHERE Id = " & lbIdCliente.Text
                    '        Ejecutar.Parameters.AddWithValue("@FechaDocExoneracion", dtpFechaVencimientoExoneracion.Value)
                    '        cFunciones.guardarDatosConParametros(Ejecutar)
                    '    Else
                    '        Ejecutar.CommandText = "INSERT INTO Salaberry.[dbo].[tb_Clientes]  ([Nombre]  ,[Observaciones]  ,[Cedula]  ,[Contacto]   ,[Exento]   ,[Credito]  ,[Telefono_1]  ,[Telefono_2]   ,[Email]  ,[Direccion_1]  ,[Limite_Credito]  ,[Plazo_Credito]  ,[Comicion] ,[TipoPrecio]  ,[Id_tipo_cliente], [NumDocExoneracion], [FechaDocExoneracion], [IdTipoCliente]) " &
                    '" VALUES  ('" & Txtnombre.Text & "'  ,'','" & Txtcedula.Text & "' ,'',0,'' ,'" & Txttel1.Text & "' ,'','" & Txtemail.Text & "' ,0 ,0 ,0   ,0  ,0 ,'" & txtDocExoneracion.Text & "', 1 ,@FechaDocExoneracion," & cboTipoCliente.SelectedValue & ") "
                    '        Ejecutar.Parameters.AddWithValue("@FechaDocExoneracion", dtpFechaVencimientoExoneracion.Value)
                    '        cFunciones.guardarDatosConParametros(Ejecutar)
                    '    End If

                    'Catch ex As Exception

                    'End Try
                    Me.conectadobd.Close()
                    '''''''''
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
            Else
                Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").CancelCurrentEdit()
                Me.BindingContext(Me.DataSetClienteNuevo1, "Cliente").EndCurrentEdit()
                Me.AdapterCliente.Update(Me.DataSetClienteNuevo1.Cliente)
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = True
                Me.ToolBar1.Buttons(1).Text = "Editar"
                Me.ToolBar1.Buttons(1).ImageIndex = 5
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                QuitarValoresRequeridos()
                InhabilitarControles()
            End If
        End If
    End Sub

    Function validar() As Boolean
        Dim Comicion As Double
        If Me.Txtnombre.Text.Length > 0 Then
            Me.ErrorProvider1.SetError(Txtnombre, "")
        Else
            Me.ErrorProvider1.SetError(Txtnombre, "Debes digitar un nombre Comercial")
            Return False
        End If

        If Me.TextEdit1.Text.Length > 0 Then
            Me.ErrorProvider1.SetError(TextEdit1, "")
        Else
            TextEdit1.Text = Txtnombre.Text
            'Me.ErrorProvider1.SetError(TextEdit1, "Debes digitar una Persona Jur�dica")
            'Return False
        End If

        If Me.Txtcedula.Text.Length > 0 Then
            Me.ErrorProvider1.SetError(Txtcedula, "")
        Else
            Txtcedula.Text = "1-0093-0099"
            'Me.ErrorProvider1.SetError(Txtcedula, "Debes digitar una cedula")
            'Return False
        End If

        'If Me.Txttel1.Text.Length > 0 Then
        '    Me.ErrorProvider1.SetError(Txttel1, "")
        'Else
        '    Me.ErrorProvider1.SetError(Txttel1, "Debes digitar un telefono")
        '    Return False
        'End If

        'trabaja por contratos
        If Me.CheckBox2.Checked = True Then
            If Me.ToolBarNuevo.Text = "Cancelar" Or Me.ToolBarEditar.Text = "Cancelar" Then
                If ComboBox3.SelectedIndex = -1 Then
                    MsgBox("Debes escoger un contrato", MsgBoxStyle.Critical)
                    Return False
                End If
            End If
        End If

        Try
            'Comicion = CDbl(Me.TextComision.Text)
            If Comicion > 100 Then
                Me.ErrorProvider1.SetError(TextComision, "Debes digitar una comicion menor que 100")
                Return False
            Else
                Me.ErrorProvider1.SetError(TextComision, "")
            End If
        Catch ex As Exception
            Me.ErrorProvider1.SetError(TextComision, "Debes digitar un digito v�lido")
            Return False
        End Try

        Return True
    End Function




    Private Sub ValidText1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (e.KeyChar.IsLetterOrDigit(e.KeyChar) Or e.KeyChar.IsControl(e.KeyChar) Or e.KeyChar.IsSeparator(e.KeyChar) Or e.KeyChar.IsSurrogate(e.KeyChar) Or e.KeyChar.IsWhiteSpace(e.KeyChar) Or e.KeyChar.IsSymbol(e.KeyChar)) Then           ' valida que en este campo solo se digiten numeros y/o "-"
            e.Handled = True  ' esto invalida la tecla pulsada
        End If
    End Sub

    Private Sub ValidText1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F1 Then
            ' Dim Buscar1 As New Buscar
            ' Buscar1.ShowDialog()

        End If

    End Sub
    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        'trabaja por contratos
        If Me.CheckBox2.Checked = True Then
            If Me.ToolBarNuevo.Text = "Cancelar" Or Me.ToolBarEditar.Text = "Cancelar" Then
                ComboBox3.Enabled = True
            End If
            'No trabaja por contratos
        Else
            'si se esta haciendo edici�n
            If Me.ToolBarNuevo.Text = "Cancelar" Or Me.ToolBarEditar.Text = "Cancelar" Then
                ComboBox3.SelectedIndex = -1
            End If
            ComboBox3.Enabled = False
        End If

    End Sub

    Private Sub Cliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.SqlConnection1.ConnectionString = GetSetting("SEESOFT", "RESTAURANTE", "CONEXION")
            conectadobd = cconexion.Conectar("Hotel")
            AdapterPrecios.Fill(DsPrecio1, "Tipo_Precio")

            carga()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim conectadobd As New SqlClient.SqlConnection
            Dim cConexion As New ConexionR
            conectadobd = cConexion.Conectar("Restaurante")
            cedula = cConexion.SlqExecuteScalar(conectadobd, "Select Cedula from Usuarios where Clave_Interna='" & Me.TextBox2.Text & "'")
            'Me.Enabled = VerificandoAcceso_a_Modulos(Me, cedula)
            'Dim usuario As String = LoginUsuario(cConexion, conectadobd, Me.TextBox2.Text, Me.Text, Me.Name)
            Dim usuario As String = cConexion.SlqExecuteScalar(conectadobd, "Select nombre from Usuarios where Id_Usuario = '" & cedula & "'")

            If usuario <> "" Then
                TextBox2.Text = ""
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEditar.Enabled = True
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.lbUsuario.Text = usuario
            Else
                Me.Enabled = True
            End If
            cConexion.DesConectar(conectadobd)
        End If
    End Sub

    Private Sub Txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtnombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextEdit1.Focus()
        End If
    End Sub

    Private Sub TextEdit1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtcedula.Focus()
        End If
    End Sub

    Private Sub Txtcedula_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtcedula.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextContacto.Focus()
        End If
    End Sub

    Private Sub TextContacto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextContacto.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextBox1.Focus()
        End If
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBox1.Focus()
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Cbcredito.Focus()
        End If
    End Sub

    Private Sub Cbcredito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Cbcredito.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBox4.Focus()
        End If
    End Sub

    Private Sub ComboBox4_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox4.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txttel1.Focus()
        End If
    End Sub

    Private Sub Txttel1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txttel1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txttel2.Focus()
        End If
    End Sub

    Private Sub Txttel2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txttel2.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtfax1.Focus()
        End If
    End Sub

    Private Sub Txtfax1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtfax1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtfax2.Focus()
        End If
    End Sub

    Private Sub Txtfax2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtfax2.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtemail.Focus()
        End If
    End Sub

    Private Sub Txtemail_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtemail.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtlimitecredito.Focus()
        End If
    End Sub

    Private Sub ComboBox3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox3.KeyDown
        If e.KeyCode = Keys.Enter Then
            Cbtipoprecio.Focus()
        End If
    End Sub

    Private Sub Cbtipoprecio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Cbtipoprecio.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtlimitecredito.Focus()
        End If
    End Sub

    Private Sub Txtlimitecredito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtlimitecredito.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtplazocredito.Focus()
        End If
    End Sub

    Private Sub Txtplazocredito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtplazocredito.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextComision.Focus()
        End If
    End Sub

    Private Sub TextComision_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextComision.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txdireccion.Focus()
        End If
    End Sub


    Private Sub cboTipoCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoCliente.SelectedIndexChanged
        Try
            If cboTipoCliente.SelectedValue = 1 Then
                gbExoneracion.Visible = False
            Else
                gbExoneracion.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
