Public Class FrmReporteRecetasCambiosCostos
    Dim Reporte As New RptPreciosRecetasActuales

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        DataSetPreciosRecetas1.Menu_Restaurante.Clear()
        Button1.Enabled = False
        ProgressBar1.Visible = True

        cFunciones.Llenar_Tabla_Generico("SELECT Recetas.ID, Recetas.Nombre, Recetas.Porciones, CASE impventa WHEN 1 THEN imp_venta ELSE 0 END AS IVI,CASE impSERV WHEN 1 THEN imp_SERVICIO ELSE 0 END AS ISI, Recetas.CostoTotal AS CostoAnterior, Menu_Restaurante.UtilidadA,Menu_Restaurante.Precio_VentaA, 0 AS CostoActual FROM         Menu_Restaurante INNER JOIN Recetas ON Menu_Restaurante.Id_Receta = Recetas.ID CROSS JOIN Configuraciones", Me.DataSetPreciosRecetas1.Menu_Restaurante)
        ProgressBar1.Maximum = BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Count

        Reporte.SetDataSource(DataSetPreciosRecetas1)
        CrystalReportViewer1.Show()

        Dim x As Integer = 0
        For x = 0 To BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Count - 1
            BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Position = x
            BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Current("CostoActual") = CostoActual(BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Current("ID"))
            BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Current("NuevoPrecio") = BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Current("CostoActual") * (Me.BindingContext(Me.DataSetPreciosRecetas1, "Menu_Restaurante").Current("UtilidadA") / 100) + Me.BindingContext(Me.DataSetPreciosRecetas1, "Menu_Restaurante").Current("CostoActual")
            ProgressBar1.Value = x
            StatusBarPanel1.Text = "Analizando : " & BindingContext(DataSetPreciosRecetas1, "Menu_Restaurante").Current("Nombre")
            Application.DoEvents()
        Next
        Application.DoEvents()

        Reporte.SetDataSource(DataSetPreciosRecetas1)
        CrystalReportViewer1.ReportSource = Reporte
        CrystalReportViewer1.Show()

        StatusBarPanel1.Text = ""
        Me.ProgressBar1.Value = 0
        Button1.Enabled = True
        ProgressBar1.Visible = False
    End Sub

    Private Function CostoActual(ByVal ID_Receta As Double) As Double
        Dim Valor As Double
        Dim Cx As New Conexion
        Valor = Cx.SQLExeScalar("select dbo.FxCostoActualReceta(" & ID_Receta & " )")
        Cx.DesConectar(Cx.sQlconexion)
        Return Valor
    End Function

    Private Sub FrmReporteRecetasCambiosCostos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Restaurante", "Conexion")
        cFunciones.Llenar_Tabla_Generico("SELECT * from Configuraciones", Me.DataSetPreciosRecetas1.Configuraciones)
    End Sub

End Class