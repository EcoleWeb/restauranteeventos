if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FxCostoActualReceta]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[FxCostoActualReceta]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


CREATE        function dbo.FxCostoActualReceta(@Id_Receta bigint)
RETURNS FLOAT
AS
BEGIN
DECLARE @Codigo float,
	@Cantidad float,
	@CostoTotal float,
	@PUConversion float,
	@Costo float,
	@Multiplo float,
	@Estado varchar,
	@CostoAnterior float,
	@CostoActual float,
	@CostoActualizado float

SET @CostoAnterior = 0
SET @CostoActual   = 0

DECLARE CursorDetalle CURSOR 
	FOR SELECT  
	Recetas_Detalles.Codigo, 
	Recetas_Detalles.Cantidad, 
	Recetas_Detalles.CostoTotal, 
	Recetas_Detalles.PUConversion,
	Proveeduria.dbo.Inventario.Costo, 
	Conversiones.dbo.Conversiones.Multiplo, 
	ISNULL(Conversiones.dbo.Conversiones.Estado, 'R') AS Estado,
	CASE   ISNULL(Conversiones.dbo.Conversiones.Estado, 'R') 
	WHEN '<' THEN (Proveeduria.dbo.Inventario.Costo / (Conversiones.dbo.Conversiones.Multiplo * Proveeduria.dbo.Inventario.PRESENTACANT)) * Recetas_Detalles.Cantidad
	WHEN '>' THEN (Proveeduria.dbo.Inventario.Costo / (Conversiones.dbo.Conversiones.Multiplo * Proveeduria.dbo.Inventario.PRESENTACANT)) * Recetas_Detalles.Cantidad
	WHEN '=' THEN (Proveeduria.dbo.Inventario.Costo) / (Conversiones.dbo.Conversiones.Multiplo * Proveeduria.dbo.Inventario.PRESENTACANT) * Recetas_Detalles.Cantidad
	WHEN 'R' THEN dbo.FxCostoActualSubReceta(Recetas_Detalles.Codigo)
	END AS CostoActual
	FROM	Recetas_Detalles 
		LEFT OUTER JOIN Proveeduria.dbo.Presentaciones 
		INNER JOIN Conversiones.dbo.Unidades 
		INNER JOIN Conversiones.dbo.Conversiones ON Conversiones.dbo.Unidades.Id = Conversiones.dbo.Conversiones.Id_Unidad ON Proveeduria.dbo.Presentaciones.Presentaciones = Conversiones.dbo.Unidades.NombreUnidad 
		INNER JOIN Proveeduria.dbo.Inventario ON Proveeduria.dbo.Presentaciones.CodPres = Proveeduria.dbo.Inventario.CodPresentacion ON Recetas_Detalles.UConversion = Conversiones.dbo.Conversiones.ConvertirA AND Recetas_Detalles.Codigo = Proveeduria.dbo.Inventario.Codigo
	WHERE     (Recetas_Detalles.IdReceta =  @Id_Receta) 
	
	OPEN CursorDetalle
	FETCH NEXT FROM CursorDetalle INTO @Codigo,@Cantidad,@CostoTotal,@PUConversion,@Costo,@Multiplo,@Estado,@CostoActualizado
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @CostoAnterior  = @CostoAnterior  + @CostoTotal
		SET @CostoActual  = @CostoActual  + @CostoActualizado
	FETCH NEXT FROM CursorDetalle INTO @Codigo,@Cantidad,@CostoTotal,@PUConversion,@Costo,@Multiplo,@Estado,@CostoActualizado
	END 
CLOSE CursorDetalle
DEALLOCATE CursorDetalle

RETURN(@CostoActual)
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

