﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteVarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbTipoVentaReporte = New System.Windows.Forms.ComboBox()
        Me.lbUsuario = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.txtmonto = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.txtcodcliente = New System.Windows.Forms.TextBox()
        Me.txtvalormoneda = New System.Windows.Forms.TextBox()
        Me.cboxmoneda = New System.Windows.Forms.ComboBox()
        Me.fechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.fechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cboxcliente = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RBtnCostoVentas = New System.Windows.Forms.RadioButton()
        Me.rbdetalladas = New System.Windows.Forms.RadioButton()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.cmbTipoVentaReporte)
        Me.Panel2.Controls.Add(Me.lbUsuario)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.TextBox3)
        Me.Panel2.Controls.Add(Me.txtmonto)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.TextBox2)
        Me.Panel2.Controls.Add(Me.txtcodcliente)
        Me.Panel2.Controls.Add(Me.txtvalormoneda)
        Me.Panel2.Controls.Add(Me.cboxmoneda)
        Me.Panel2.Controls.Add(Me.fechaFinal)
        Me.Panel2.Controls.Add(Me.fechaInicio)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.cboxcliente)
        Me.Panel2.Controls.Add(Me.GroupBox1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 741)
        Me.Panel2.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(21, 509)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 13)
        Me.Label5.TabIndex = 220
        Me.Label5.Text = "Ver"
        '
        'cmbTipoVentaReporte
        '
        Me.cmbTipoVentaReporte.AutoCompleteCustomSource.AddRange(New String() {"Todas las Facturas", "Solo Facturas Express", "Solo Facturas Salón", "Solo Facturas para Llevar"})
        Me.cmbTipoVentaReporte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoVentaReporte.Font = New System.Drawing.Font("Times New Roman", 9.0!)
        Me.cmbTipoVentaReporte.FormattingEnabled = True
        Me.cmbTipoVentaReporte.Items.AddRange(New Object() {"Todas las Facturas", "Solo Facturas Express", "Solo Facturas Salon", "Solo Facturas para Llevar"})
        Me.cmbTipoVentaReporte.Location = New System.Drawing.Point(56, 506)
        Me.cmbTipoVentaReporte.Name = "cmbTipoVentaReporte"
        Me.cmbTipoVentaReporte.Size = New System.Drawing.Size(138, 23)
        Me.cmbTipoVentaReporte.TabIndex = 219
        '
        'lbUsuario
        '
        Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lbUsuario.ForeColor = System.Drawing.Color.White
        Me.lbUsuario.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbUsuario.Location = New System.Drawing.Point(53, 652)
        Me.lbUsuario.Name = "lbUsuario"
        Me.lbUsuario.Size = New System.Drawing.Size(84, 14)
        Me.lbUsuario.TabIndex = 218
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(6, 652)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 217
        Me.Label10.Text = "Usuario"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 630)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 216
        Me.Label11.Text = "Clave"
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TextBox3.Location = New System.Drawing.Point(53, 630)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox3.Size = New System.Drawing.Size(84, 15)
        Me.TextBox3.TabIndex = 1
        '
        'txtmonto
        '
        Me.txtmonto.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtmonto.Location = New System.Drawing.Point(56, 480)
        Me.txtmonto.Name = "txtmonto"
        Me.txtmonto.Size = New System.Drawing.Size(138, 20)
        Me.txtmonto.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(9, 483)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Monto >="
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Button2.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button2.Location = New System.Drawing.Point(12, 673)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(182, 61)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "      Salir"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox2.Location = New System.Drawing.Point(12, 25)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(182, 20)
        Me.TextBox2.TabIndex = 8
        Me.TextBox2.Text = "Tipos de Reportes"
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodcliente
        '
        Me.txtcodcliente.Location = New System.Drawing.Point(0, 524)
        Me.txtcodcliente.Name = "txtcodcliente"
        Me.txtcodcliente.Size = New System.Drawing.Size(10, 20)
        Me.txtcodcliente.TabIndex = 7
        Me.txtcodcliente.Visible = False
        '
        'txtvalormoneda
        '
        Me.txtvalormoneda.Location = New System.Drawing.Point(0, 609)
        Me.txtvalormoneda.Name = "txtvalormoneda"
        Me.txtvalormoneda.Size = New System.Drawing.Size(10, 20)
        Me.txtvalormoneda.TabIndex = 2
        Me.txtvalormoneda.Visible = False
        '
        'cboxmoneda
        '
        Me.cboxmoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboxmoneda.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cboxmoneda.FormattingEnabled = True
        Me.cboxmoneda.Location = New System.Drawing.Point(56, 453)
        Me.cboxmoneda.Name = "cboxmoneda"
        Me.cboxmoneda.Size = New System.Drawing.Size(138, 21)
        Me.cboxmoneda.TabIndex = 6
        '
        'fechaFinal
        '
        Me.fechaFinal.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.fechaFinal.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.fechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaFinal.Location = New System.Drawing.Point(56, 427)
        Me.fechaFinal.Name = "fechaFinal"
        Me.fechaFinal.Size = New System.Drawing.Size(138, 20)
        Me.fechaFinal.TabIndex = 5
        Me.fechaFinal.Value = New Date(2007, 4, 12, 0, 0, 0, 0)
        '
        'fechaInicio
        '
        Me.fechaInicio.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.fechaInicio.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.fechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaInicio.Location = New System.Drawing.Point(56, 401)
        Me.fechaInicio.Name = "fechaInicio"
        Me.fechaInicio.Size = New System.Drawing.Size(138, 20)
        Me.fechaInicio.TabIndex = 2
        Me.fechaInicio.Value = New Date(2013, 10, 16, 0, 0, 0, 0)
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.Image = Global.SIALIBEB.My.Resources.Resources.reporte
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(12, 547)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(182, 61)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "    Mostrar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(9, 458)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Moneda:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(15, 432)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Hasta:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(15, 407)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Desde:"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(12, 357)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(182, 20)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = "Cliente"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cboxcliente
        '
        Me.cboxcliente.Enabled = False
        Me.cboxcliente.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cboxcliente.FormattingEnabled = True
        Me.cboxcliente.Location = New System.Drawing.Point(12, 374)
        Me.cboxcliente.Name = "cboxcliente"
        Me.cboxcliente.Size = New System.Drawing.Size(182, 21)
        Me.cboxcliente.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RBtnCostoVentas)
        Me.GroupBox1.Controls.Add(Me.rbdetalladas)
        Me.GroupBox1.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(182, 302)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ventas"
        '
        'RBtnCostoVentas
        '
        Me.RBtnCostoVentas.AutoSize = True
        Me.RBtnCostoVentas.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.RBtnCostoVentas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.RBtnCostoVentas.Location = New System.Drawing.Point(6, 41)
        Me.RBtnCostoVentas.Name = "RBtnCostoVentas"
        Me.RBtnCostoVentas.Size = New System.Drawing.Size(108, 17)
        Me.RBtnCostoVentas.TabIndex = 12
        Me.RBtnCostoVentas.Text = "Costo de Ventas"
        Me.RBtnCostoVentas.UseVisualStyleBackColor = True
        '
        'rbdetalladas
        '
        Me.rbdetalladas.AutoSize = True
        Me.rbdetalladas.Checked = True
        Me.rbdetalladas.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbdetalladas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rbdetalladas.Location = New System.Drawing.Point(6, 20)
        Me.rbdetalladas.Name = "rbdetalladas"
        Me.rbdetalladas.Size = New System.Drawing.Size(111, 17)
        Me.rbdetalladas.TabIndex = 2
        Me.rbdetalladas.TabStop = True
        Me.rbdetalladas.Text = "Detalladas X Día"
        Me.rbdetalladas.UseVisualStyleBackColor = True
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.DisplayBackgroundEdge = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(200, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(713, 741)
        Me.CrystalReportViewer1.TabIndex = 5
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'frmReporteVarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 741)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmReporteVarios"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbTipoVentaReporte As ComboBox
    Friend WithEvents lbUsuario As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents txtmonto As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents txtcodcliente As TextBox
    Friend WithEvents txtvalormoneda As TextBox
    Friend WithEvents cboxmoneda As ComboBox
    Friend WithEvents fechaFinal As DateTimePicker
    Friend WithEvents fechaInicio As DateTimePicker
    Friend WithEvents Button1 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents cboxcliente As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RBtnCostoVentas As RadioButton
    Friend WithEvents rbdetalladas As RadioButton
    Friend WithEvents CrystalReportViewer1 As Forms.CrystalReportViewer
End Class
