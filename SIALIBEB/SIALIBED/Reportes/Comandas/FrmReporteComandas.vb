Imports DevExpress.Utils

Public Class FrmReporteComandas

#Region "Variables"
    Public punto_venta As Integer
    Private FilaXProducto As DataRow
    Dim Wait As WaitDialogForm
#End Region

#Region "Load"
    Sub cargarHoraUltimaApertura()
        Dim dt As New DataTable
        Dim sql As String = "SELECT ISNULL(MAX(Fecha),getDate()) AS ULTIMA fROM         aperturacaja WHERE     (Anulado = 0)"
        cFunciones.Llenar_Tabla_Generico(sql, dt, GetSetting("SeeSoft", "Restaurante", "Conexion"))
        If dt.Rows.Count > 0 Then
            Me.fechaInicio.Value = dt.Rows(0).Item(0)
        End If
    End Sub
    Private Sub FrmReporteComandas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.fechaInicio.Value = DateAdd(DateInterval.Day, -Now.Day + 1, Now.Date).AddHours(6)
        cargarHoraUltimaApertura()
        Me.fechaFinal.Value = Now
        'Me.fechaFinal.Value = Now.Date.AddHours(23).AddMinutes(59)
        ComboBox1.Focus()
    End Sub
#End Region

#Region "Mostrar"
    Private Sub Mostrar()
        Button3.Enabled = False
        Wait = New WaitDialogForm("Generando Reporte")

        Try
            Select Case Me.ComboBox1.SelectedIndex
                Case -1
                    MsgBox("Seleccione un tipo de reporte..", MsgBoxStyle.Information, "Atención..")
                Case 0 'reporte de comandas facuradas o de cortesia.
                    Dim Reporte As New ReporteComandas
                    CrystalReportViewer1.SelectionFormula = "{Comanda.Fecha}>= {?FechaDesde} and {Comanda.Fecha} <= {?FechaHasta}"
                    Reporte.SetParameterValue(0, fechaInicio.Value)
                    Reporte.SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte, , )

                Case 1 'Costos de Artículos Comandados

                    Dim reporte As New ReporteGastosProductosComandados
                    CrystalReportViewer1.SelectionFormula = "{Comanda.Fecha} in {?FechaDesde} to {?FechaHasta} and " & _
                                                            "{Ventas.IdPuntoVenta} = {?id_puntoventa} and not {Comanda.cortesia} and not {Comanda.anulado}"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    reporte.SetParameterValue("id_puntoventa", punto_venta)
                    ' CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte,, )
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSoft", "Restaurante", "Conexion"))


                Case 2 'Articulos Descargados Por Venta
                    Dim Reporte As New ReporteArticulosDescargados
                    CrystalReportViewer1.SelectionFormula = "{Vista_Proveeduria_SubFamilias.Codigo} = {Vista_Final.Familia}"
                    Ds_productos1.Clear()
                    'Aqui se va a hacer el Dataset de los productos descargados:
                    Cargar_Dataset(fechaInicio.Value, fechaFinal.Value)
                    Dim confi As New ds_productosTableAdapters.ConfiguracionesTableAdapter
                    Dim Familia As New ds_productosTableAdapters.Vista_Proveeduria_FAMILIATableAdapter
                    Dim sub_familia As New ds_productosTableAdapters.Vista_Proveeduria_SubFamiliasTableAdapter
                    confi.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    Familia.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    sub_familia.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    Ds_productos1.Configuraciones.Clear()
                    confi.Fill(Ds_productos1.Configuraciones)
                    Ds_productos1.Vista_Proveeduria_FAMILIA.Clear()
                    Familia.Fill(Ds_productos1.Vista_Proveeduria_FAMILIA)
                    Ds_productos1.Vista_Proveeduria_SubFamilias.Clear()
                    sub_familia.Fill(Ds_productos1.Vista_Proveeduria_SubFamilias)
                    Reporte.SetDataSource(Ds_productos1)
                    Reporte.Subreports(0).SetDataSource(Ds_productos1)
                    Reporte.SetParameterValue(0, Me.fechaInicio.Value)
                    Reporte.SetParameterValue(1, Me.fechaFinal.Value)
                    CrystalReportViewer1.ReportSource = Reporte

                Case 3 'reporte de comandas por saloneros.
                    Dim Reporte As New ReporteIS
                    CrystalReportViewer1.SelectionFormula = "{VISTAXSALONERO.Fecha} in {?FechaDesde} to {?FechaHasta}"
                    Reporte.SetParameterValue(0, Me.fechaInicio.Value)
                    Reporte.SetParameterValue(1, Me.fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte, , )

                Case 4 'Reporte de Cortesias
                    Dim reporte As New Reporte_Cortesias
                    CrystalReportViewer1.SelectionFormula = "{Comanda.Fecha} in {?FechaDesde} to {?FechaHasta} and {Comanda.cortesia} and not {Comanda.anulado}"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False)

                Case 5 'Reporte de Comandas Eliminadas
                    Dim reporte As New Reporte_ComandasEliminadas
                    CrystalReportViewer1.SelectionFormula = "{Comanda_Eliminados.HoraEliminacion} in {?FechaDesde} to {?FechaHasta} AND {Comanda_Eliminados.Tipo_Plato} <> 'Modificador'"
                    reporte.SetParameterValue("FechaDesde", fechaInicio.Value)
                    reporte.SetParameterValue("FechaHasta", fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False)
                  
                Case 6 'Reporte de Comandas Resta
                    Dim reporte As New CrystalReport_PorResta
                    CrystalReportViewer1.SelectionFormula = "CDATE({VISTASeaFood.Fecha})>= {?FechaI} ANd CDAte({VISTASeaFood.Fecha}) <= {?FechaF}"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    '                    reporte.Subreports(0).SetParameterValue(0, fechaInicio.Value)
                    '                   reporte.Subreports(0).SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSoft", "Hotel", "Conexion"))
                Case 7 'Reporte de Comandas Resta
                    Dim reporte As New Reporte_Cortesias_Clasificadas
                    CrystalReportViewer1.SelectionFormula = "{Comanda.Fecha} in {?FechaDesde} to {?FechaHasta} and {Comanda.cortesia} and not {Comanda.anulado}"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    '                    reporte.Subreports(0).SetParameterValue(0, fechaInicio.Value)
                    '                   reporte.Subreports(0).SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                Case 8 'Reporte por cantidad de personas
                    Dim reporte As New RCantidadPersonas
                    CrystalReportViewer1.SelectionFormula = "{Comanda.anulado}=false and {Comanda.Facturado}=true and {Comanda.cortesia}=false  and {Comanda.Comenzales}>0 and ({Comanda.Fecha}>={?Finicial} and {Comanda.Fecha}<={?Ffinal})"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                Case 9 'Reporte de cortesias detalladas
                    Dim reporte As New CortesiasDetalladas
                    CrystalReportViewer1.SelectionFormula = "{Comanda.Fecha}>={?FechaDesde} and {Comanda.Fecha} <={?FechaHasta} and{Comanda.anulado}=false"
                    reporte.SetParameterValue(0, fechaInicio.Value)
                    reporte.SetParameterValue(1, fechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, reporte, False, GetSetting("SeeSoft", "Restaurante", "Conexion"))
                Case 10 'Reporte de descargados por num de factura
                    Dim Reporte As New vista_final_2
                    CrystalReportViewer1.SelectionFormula = "{Vista_Proveeduria_SubFamilias.Codigo} = {Vista_Final.Familia}"
                    Ds_productos1.Clear()
                    'Aqui se va a hacer el Dataset de los productos descargados:
                    Me.Cargar_Dataset_NumFac(fechaInicio.Value, fechaFinal.Value)
                    Dim confi As New ds_productosTableAdapters.ConfiguracionesTableAdapter
                    Dim Familia As New ds_productosTableAdapters.Vista_Proveeduria_FAMILIATableAdapter
                    Dim sub_familia As New ds_productosTableAdapters.Vista_Proveeduria_SubFamiliasTableAdapter
                    confi.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    Familia.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    sub_familia.Connection.ConnectionString = GetSetting("SeeSoft", "Restaurante", "conexion")
                    Ds_productos1.Configuraciones.Clear()
                    confi.Fill(Ds_productos1.Configuraciones)
                    Ds_productos1.Vista_Proveeduria_FAMILIA.Clear()
                    Familia.Fill(Ds_productos1.Vista_Proveeduria_FAMILIA)
                    Ds_productos1.Vista_Proveeduria_SubFamilias.Clear()
                    sub_familia.Fill(Ds_productos1.Vista_Proveeduria_SubFamilias)
                    Reporte.SetDataSource(Ds_productos1)
                    Reporte.Subreports(0).SetDataSource(Ds_productos1)
                    Reporte.SetParameterValue(0, Me.fechaInicio.Value)
                    Reporte.SetParameterValue(1, Me.fechaFinal.Value)
                    CrystalReportViewer1.ReportSource = Reporte

            End Select

        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            Wait.Close()
            Button3.Enabled = True
        End Try
    End Sub
#End Region

#Region "Cargar DataSet"
    Private Sub Cargar_Dataset(ByVal Fecha_inicio As DateTime, ByVal fecha_final As DateTime)
        Dim exe As New ConexionR
        Try
            Dim fecha_venta As Date
            Dim ds_ProductosInventario As New DataSet
            Dim posicion As Integer
            Dim ds_VentasDetalle As New DataSet
            Dim fila_inventario As DataRow
            Dim cantidad_descarga As Double

            exe.GetDataSet(exe.Conectar, "Select codigo, descripcion, subFamilia,PrecioBase, PresentaCant, Presentaciones from InventarioUsadoEnResta", ds_ProductosInventario, "Productos_Inventario")
            exe.DesConectar(exe.SqlConexion)

            exe.GetDataSet(exe.Conectar, "SELECT     SUM(Vd.Cantidad) AS CantidadVenta, Mr.Tipo, Mr.bodega,  MR.disminuye AS disminuye, SUM(Mr.Cantidad) AS Cantidad, Mr.Id_Receta FROM Vista_Hotel_Ventas V INNER JOIN Vista_Hotel_VentasDetalle Vd ON V.Id = Vd.Id_Factura INNER JOIN  Menu_Restaurante MR ON Vd.Codigo = MR.Id_Menu WHERE V.Fecha >= '" & Format(Fecha_inicio, "dd/MM/yyyy HH:mm") & "' and V.Fecha <= '" & Format(fecha_final, "dd/MM/yyyy HH:mm") & "' and V.Proveniencia_Venta = " & User_Log.PuntoVenta & " AND (Vd.Cantidad > 0) GROUP BY Mr.Tipo, Mr.bodega, Mr.Id_Receta, MR.disminuye ", ds_VentasDetalle, "Ventas_Menu")
            exe.DesConectar(exe.SqlConexion)

            For Each fila_inventario In ds_ProductosInventario.Tables("Productos_Inventario").Rows
                BindingContext(ds_VentasDetalle, "Ventas_Menu").Position = 0
                cantidad_descarga = 0
                With BindingContext(ds_VentasDetalle, "Ventas_Menu")
                    For posicion = 0 To .Count - 1
                        .Position = posicion
                        If .Current("Tipo") = 2 Then
                            If .Current("Id_Receta") = fila_inventario("codigo") Then
                                If .Current("CantidadVenta") < 0 Then
                                    cantidad_descarga = 1
                                Else
                                    cantidad_descarga += .Current("CantidadVenta") * .Current("disminuye")
                                End If
                                ' fecha_venta = FormatDateTime(.Current("Fecha"), DateFormat.ShortDate)
                            End If
                        ElseIf .Current("Tipo") = 1 Then
                            cantidad_descarga += Descarga_Receta(.Current("id_receta"), fila_inventario("codigo")) * .Current("CantidadVenta")
                        End If
                    Next
                End With

                If cantidad_descarga <> 0 Then
                    BindingContext(Ds_productos1.Vista_Final).AddNew()
                    BindingContext(Ds_productos1.Vista_Final).Current("codigo_producto") = fila_inventario("codigo")
                    BindingContext(Ds_productos1.Vista_Final).Current("cantidad_descargada") = cantidad_descarga
                    BindingContext(Ds_productos1.Vista_Final).Current("descripcion") = fila_inventario("descripcion")
                    BindingContext(Ds_productos1.Vista_Final).Current("precio_unitario") = fila_inventario("PrecioBase")
                    BindingContext(Ds_productos1.Vista_Final).Current("familia") = fila_inventario("subFamilia")
                    BindingContext(Ds_productos1.Vista_Final).Current("Fecha_Venta") = Now
                    BindingContext(Ds_productos1.Vista_Final).Current("Presentacion") = fila_inventario("PresentaCant") & " " & fila_inventario("Presentaciones")

                    BindingContext(Ds_productos1.Vista_Final).EndCurrentEdit()
                End If
            Next

        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            exe.DesConectar(exe.SqlConexion)
        End Try
    End Sub
    Private Sub Cargar_Dataset_NumFac(ByVal Fecha_inicio As DateTime, ByVal fecha_final As DateTime)
        Dim exe As New ConexionR
        Try
            Dim fecha_venta As Date
            Dim ds_ProductosInventario As New DataSet
            Dim posicion As Integer
            Dim ds_VentasDetalle As New DataSet
            Dim fila_inventario As DataRow
            Dim cantidad_descarga As Double

            exe.GetDataSet(exe.Conectar, "Select codigo, descripcion, subFamilia,PrecioBase, PresentaCant, Presentaciones from InventarioUsadoEnResta", ds_ProductosInventario, "Productos_Inventario")
            exe.DesConectar(exe.SqlConexion)

            exe.GetDataSet(exe.Conectar, "SELECT    V.Num_Factura,V.Fecha As FechaVenta, SUM(Vd.Cantidad) AS CantidadVenta, Mr.Tipo, Mr.bodega,  MR.disminuye AS disminuye, SUM(Mr.Cantidad) AS Cantidad, Mr.Id_Receta FROM Vista_Hotel_Ventas V INNER JOIN Vista_Hotel_VentasDetalle Vd ON V.Id = Vd.Id_Factura INNER JOIN  Menu_Restaurante MR ON Vd.Codigo = MR.Id_Menu WHERE V.Fecha >= '" & Format(Fecha_inicio, "dd/MM/yyyy HH:mm") & "' and V.Fecha <= '" & Format(fecha_final, "dd/MM/yyyy HH:mm") & "' and V.Proveniencia_Venta = " & User_Log.PuntoVenta & " AND (Vd.Cantidad > 0) GROUP BY Mr.Tipo, Mr.bodega,V.Num_Factura,V.Fecha, Mr.Id_Receta, MR.disminuye ", ds_VentasDetalle, "Ventas_Menu")
            exe.DesConectar(exe.SqlConexion)
            With BindingContext(ds_VentasDetalle, "Ventas_Menu")
                For posicion = 0 To .Count - 1
                    For Each fila_inventario In ds_ProductosInventario.Tables("Productos_Inventario").Rows
                        BindingContext(ds_VentasDetalle, "Ventas_Menu").Position = 0
                        cantidad_descarga = 0

                        .Position = posicion
                        If .Current("Tipo") = 2 Then
                            If .Current("Id_Receta") = fila_inventario("codigo") Then
                                If .Current("CantidadVenta") < 0 Then
                                    cantidad_descarga = 1
                                Else
                                    cantidad_descarga += .Current("CantidadVenta") * .Current("disminuye")
                                End If
                                ' fecha_venta = FormatDateTime(.Current("Fecha"), DateFormat.ShortDate)
                            End If
                        ElseIf .Current("Tipo") = 1 Then
                            cantidad_descarga += Descarga_Receta(.Current("id_receta"), fila_inventario("codigo")) * .Current("CantidadVenta")
                        End If

                        If cantidad_descarga <> 0 Then
                            BindingContext(Ds_productos1.Vista_Final2).AddNew()
                            BindingContext(Ds_productos1.Vista_Final2).Current("codigo_producto") = fila_inventario("codigo")
                            BindingContext(Ds_productos1.Vista_Final2).Current("cantidad_descargada") = cantidad_descarga
                            BindingContext(Ds_productos1.Vista_Final2).Current("descripcion") = fila_inventario("descripcion")
                            BindingContext(Ds_productos1.Vista_Final2).Current("precio_unitario") = fila_inventario("PrecioBase")
                            BindingContext(Ds_productos1.Vista_Final2).Current("familia") = fila_inventario("subFamilia")
                            BindingContext(Ds_productos1.Vista_Final2).Current("Fecha_Venta") = .Current("FechaVenta")
                            BindingContext(Ds_productos1.Vista_Final2).Current("NumFactura") = .Current("Num_Factura")
                            BindingContext(Ds_productos1.Vista_Final2).Current("Presentacion") = fila_inventario("PresentaCant") & " " & fila_inventario("Presentaciones")

                            BindingContext(Ds_productos1.Vista_Final2).EndCurrentEdit()
                        End If
                    Next

                Next
            End With

        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema:" + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            exe.DesConectar(exe.SqlConexion)
        End Try
    End Sub

    Private Function Descarga_Receta(ByVal id_receta As Double, ByVal id_articulo As Integer) As Double
        Dim exe2 As New ConexionR
        Try
            Dim cantidad_descarga As Double
            Dim ds_recetadetalle As New DataSet
            Dim fila_receta As DataRow
            
            exe2.GetDataSet(exe2.Conectar, "SELECT R.ID AS id_receta, Rd.Codigo, Rd.Cantidad, Rd.Disminuye, Rd.Articulo, R.Porciones FROM  Recetas R INNER JOIN Recetas_Detalles Rd ON R.ID = Rd.IdReceta WHERE idreceta = " & id_receta, ds_recetadetalle, "RecetaDetalle")
            exe2.DesConectar(exe2.SqlConexion)

            For Each fila_receta In ds_recetadetalle.Tables("RecetaDetalle").Rows
                If fila_receta("articulo") = True Then
                    If fila_receta("codigo") = id_articulo Then
                        cantidad_descarga += fila_receta("Disminuye") / fila_receta("Porciones")
                    End If
                Else
                    cantidad_descarga += Descarga_Receta(fila_receta("codigo"), id_articulo) * fila_receta("Cantidad")
                End If
            Next

            Return cantidad_descarga
        Catch ex As Exception
            MsgBox("Ocurrio el siguiente problema: " + ex.ToString, MsgBoxStyle.Critical, "Servicios Estructurales SeeSoft")
        Finally
            exe2.DesConectar(exe2.SqlConexion)
        End Try
    End Function
#End Region

#Region "Cerrar"
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
#End Region

#Region "Controles Funciones"
    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            fechaInicio.Focus()
        End If
    End Sub

    Private Sub fechaInicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles fechaInicio.KeyDown
        If e.KeyCode = Keys.Enter Then
            fechaFinal.Focus()
        End If
    End Sub

    Private Sub fechaFinal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles fechaFinal.KeyDown
        If e.KeyCode = Keys.Enter Then
            Button3.Focus()
        End If
    End Sub

    Private Sub Button3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button3.KeyDown
        If e.KeyCode = Keys.Enter Then
            Mostrar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Mostrar()
    End Sub
#End Region

    Private Sub fechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaInicio.ValueChanged
        If Me.fechaInicio.Value.Date > Me.fechaFinal.Value.Date Then
            Me.fechaFinal.Value = Me.fechaInicio.Value
            Me.fechaFinal.Value = fechaFinal.Value.AddDays(1)
        End If
    End Sub

    Private Sub fechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fechaFinal.ValueChanged
        If Me.fechaFinal.Value.Date < Me.fechaInicio.Value.Date Then
            Me.fechaInicio.Value = Me.fechaFinal.Value
            Me.fechaInicio.Value = Me.fechaInicio.Value.AddDays(-1)
        End If
    End Sub
End Class