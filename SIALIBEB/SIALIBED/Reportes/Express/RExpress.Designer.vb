<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RExpress
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.BSalir = New System.Windows.Forms.Button()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.TxtValorMoneda = New System.Windows.Forms.TextBox()
		Me.CbMoneda = New System.Windows.Forms.ComboBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.DPhasta = New System.Windows.Forms.DateTimePicker()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.DPfechadesde = New System.Windows.Forms.DateTimePicker()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.RadioButton1 = New System.Windows.Forms.RadioButton()
		Me.RBCantidad = New System.Windows.Forms.RadioButton()
		Me.RBMonto = New System.Windows.Forms.RadioButton()
		Me.TextBox2 = New System.Windows.Forms.TextBox()
		Me.BMostrar = New System.Windows.Forms.Button()
		Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
		Me.Panel1.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.BSalir)
		Me.Panel1.Controls.Add(Me.GroupBox3)
		Me.Panel1.Controls.Add(Me.GroupBox2)
		Me.Panel1.Controls.Add(Me.GroupBox1)
		Me.Panel1.Controls.Add(Me.TextBox2)
		Me.Panel1.Controls.Add(Me.BMostrar)
		Me.Panel1.Location = New System.Drawing.Point(1, 3)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(213, 754)
		Me.Panel1.TabIndex = 0
		'
		'BSalir
		'
		Me.BSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
		Me.BSalir.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
		Me.BSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.BSalir.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.BSalir.Location = New System.Drawing.Point(25, 490)
		Me.BSalir.Name = "BSalir"
		Me.BSalir.Size = New System.Drawing.Size(182, 61)
		Me.BSalir.TabIndex = 20
		Me.BSalir.Text = "      Salir"
		Me.BSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.BSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.BSalir.UseVisualStyleBackColor = True
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.TxtValorMoneda)
		Me.GroupBox3.Controls.Add(Me.CbMoneda)
		Me.GroupBox3.Controls.Add(Me.Label3)
		Me.GroupBox3.Location = New System.Drawing.Point(19, 318)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(187, 83)
		Me.GroupBox3.TabIndex = 18
		Me.GroupBox3.TabStop = False
		'
		'TxtValorMoneda
		'
		Me.TxtValorMoneda.Location = New System.Drawing.Point(70, 57)
		Me.TxtValorMoneda.Name = "TxtValorMoneda"
		Me.TxtValorMoneda.Size = New System.Drawing.Size(111, 20)
		Me.TxtValorMoneda.TabIndex = 19
		Me.TxtValorMoneda.Visible = False
		'
		'CbMoneda
		'
		Me.CbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.CbMoneda.FormattingEnabled = True
		Me.CbMoneda.Location = New System.Drawing.Point(70, 27)
		Me.CbMoneda.Name = "CbMoneda"
		Me.CbMoneda.Size = New System.Drawing.Size(111, 21)
		Me.CbMoneda.TabIndex = 18
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(2, 27)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(68, 18)
		Me.Label3.TabIndex = 17
		Me.Label3.Text = "Moneda"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Label2)
		Me.GroupBox2.Controls.Add(Me.DPhasta)
		Me.GroupBox2.Controls.Add(Me.Label1)
		Me.GroupBox2.Controls.Add(Me.DPfechadesde)
		Me.GroupBox2.Location = New System.Drawing.Point(19, 212)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(188, 100)
		Me.GroupBox2.TabIndex = 17
		Me.GroupBox2.TabStop = False
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(2, 71)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(57, 18)
		Me.Label2.TabIndex = 15
		Me.Label2.Text = "Hasta:"
		'
		'DPhasta
		'
		Me.DPhasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DPhasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.DPhasta.Location = New System.Drawing.Point(70, 67)
		Me.DPhasta.Name = "DPhasta"
		Me.DPhasta.Size = New System.Drawing.Size(117, 26)
		Me.DPhasta.TabIndex = 14
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(2, 28)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(61, 18)
		Me.Label1.TabIndex = 13
		Me.Label1.Text = "Desde:"
		'
		'DPfechadesde
		'
		Me.DPfechadesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DPfechadesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.DPfechadesde.Location = New System.Drawing.Point(70, 22)
		Me.DPfechadesde.Name = "DPfechadesde"
		Me.DPfechadesde.Size = New System.Drawing.Size(117, 26)
		Me.DPfechadesde.TabIndex = 12
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.RadioButton1)
		Me.GroupBox1.Controls.Add(Me.RBCantidad)
		Me.GroupBox1.Controls.Add(Me.RBMonto)
		Me.GroupBox1.Location = New System.Drawing.Point(3, 71)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(203, 130)
		Me.GroupBox1.TabIndex = 13
		Me.GroupBox1.TabStop = False
		'
		'RadioButton1
		'
		Me.RadioButton1.AutoSize = True
		Me.RadioButton1.Checked = True
		Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.RadioButton1.Location = New System.Drawing.Point(17, 19)
		Me.RadioButton1.Name = "RadioButton1"
		Me.RadioButton1.Size = New System.Drawing.Size(118, 17)
		Me.RadioButton1.TabIndex = 10
		Me.RadioButton1.TabStop = True
		Me.RadioButton1.Text = "Clientes Express"
		Me.RadioButton1.UseVisualStyleBackColor = True
		'
		'RBCantidad
		'
		Me.RBCantidad.AutoSize = True
		Me.RBCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.RBCantidad.Location = New System.Drawing.Point(17, 53)
		Me.RBCantidad.Name = "RBCantidad"
		Me.RBCantidad.Size = New System.Drawing.Size(125, 17)
		Me.RBCantidad.TabIndex = 12
		Me.RBCantidad.Text = "Cantidad Vendida"
		Me.RBCantidad.UseVisualStyleBackColor = True
		'
		'RBMonto
		'
		Me.RBMonto.AutoSize = True
		Me.RBMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.RBMonto.Location = New System.Drawing.Point(17, 89)
		Me.RBMonto.Name = "RBMonto"
		Me.RBMonto.Size = New System.Drawing.Size(110, 17)
		Me.RBMonto.TabIndex = 11
		Me.RBMonto.Text = "Monto Vendido"
		Me.RBMonto.UseVisualStyleBackColor = True
		'
		'TextBox2
		'
		Me.TextBox2.BackColor = System.Drawing.SystemColors.Control
		Me.TextBox2.Location = New System.Drawing.Point(11, 33)
		Me.TextBox2.Name = "TextBox2"
		Me.TextBox2.ReadOnly = True
		Me.TextBox2.Size = New System.Drawing.Size(182, 20)
		Me.TextBox2.TabIndex = 9
		Me.TextBox2.Text = "Tipos de Reportes"
		Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'BMostrar
		'
		Me.BMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
		Me.BMostrar.Image = Global.SIALIBEB.My.Resources.Resources.reporte
		Me.BMostrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.BMostrar.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.BMostrar.Location = New System.Drawing.Point(25, 406)
		Me.BMostrar.Name = "BMostrar"
		Me.BMostrar.Size = New System.Drawing.Size(182, 61)
		Me.BMostrar.TabIndex = 3
		Me.BMostrar.Text = "    Mostrar"
		Me.BMostrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.BMostrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.BMostrar.UseVisualStyleBackColor = True
		'
		'CrystalReportViewer1
		'
		Me.CrystalReportViewer1.ActiveViewIndex = -1
		Me.CrystalReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
		Me.CrystalReportViewer1.Location = New System.Drawing.Point(216, 3)
		Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
		Me.CrystalReportViewer1.SelectionFormula = ""
		Me.CrystalReportViewer1.ShowCloseButton = False
		Me.CrystalReportViewer1.ShowGotoPageButton = False
		Me.CrystalReportViewer1.ShowGroupTreeButton = False
		Me.CrystalReportViewer1.ShowTextSearchButton = False
		Me.CrystalReportViewer1.ShowZoomButton = False
		Me.CrystalReportViewer1.Size = New System.Drawing.Size(1067, 754)
		Me.CrystalReportViewer1.TabIndex = 1
		Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
		'
		'RExpress
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1284, 746)
		Me.Controls.Add(Me.CrystalReportViewer1)
		Me.Controls.Add(Me.Panel1)
		Me.Name = "RExpress"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Reportes de Clientes Express"
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.GroupBox3.ResumeLayout(False)
		Me.GroupBox3.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BMostrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RBCantidad As System.Windows.Forms.RadioButton
    Friend WithEvents RBMonto As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtValorMoneda As System.Windows.Forms.TextBox
    Friend WithEvents CbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DPhasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DPfechadesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents BSalir As System.Windows.Forms.Button
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
