<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MuestraReportes
	Inherits System.Windows.Forms.Form

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MuestraReportes))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.cmbTipoVentaReporte = New System.Windows.Forms.ComboBox()
		Me.lbUsuario = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.TextBox3 = New System.Windows.Forms.TextBox()
		Me.txtmonto = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Button2 = New System.Windows.Forms.Button()
		Me.TextBox2 = New System.Windows.Forms.TextBox()
		Me.txtcodcliente = New System.Windows.Forms.TextBox()
		Me.txtvalormoneda = New System.Windows.Forms.TextBox()
		Me.cboxmoneda = New System.Windows.Forms.ComboBox()
		Me.fechaFinal = New System.Windows.Forms.DateTimePicker()
		Me.fechaInicio = New System.Windows.Forms.DateTimePicker()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.TextBox1 = New System.Windows.Forms.TextBox()
		Me.cboxcliente = New System.Windows.Forms.ComboBox()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.rbVentaXEmpleado = New System.Windows.Forms.RadioButton()
		Me.cxPqCate = New System.Windows.Forms.CheckBox()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.cbxTodCate = New System.Windows.Forms.RadioButton()
		Me.cbxCarCate = New System.Windows.Forms.RadioButton()
		Me.cbxConCate = New System.Windows.Forms.RadioButton()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cbxTodMenu = New System.Windows.Forms.RadioButton()
		Me.cbxCarMenu = New System.Windows.Forms.RadioButton()
		Me.cbxConMenu = New System.Windows.Forms.RadioButton()
		Me.CheckBoxPeq = New System.Windows.Forms.CheckBox()
		Me.rbVentasxCaja = New System.Windows.Forms.RadioButton()
		Me.RBcateg = New System.Windows.Forms.RadioButton()
		Me.RBCantidad = New System.Windows.Forms.RadioButton()
		Me.opCostoVenSalonero = New System.Windows.Forms.RadioButton()
		Me.porcen = New System.Windows.Forms.TextBox()
		Me.tarjetas = New System.Windows.Forms.Label()
		Me.RBtnCostoVentas = New System.Windows.Forms.RadioButton()
		Me.rbVentasFormaPago = New System.Windows.Forms.RadioButton()
		Me.rbAnuladas = New System.Windows.Forms.RadioButton()
		Me.rbdetalladascliente = New System.Windows.Forms.RadioButton()
		Me.rbdetalladas = New System.Windows.Forms.RadioButton()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
		Me.TreeView1 = New System.Windows.Forms.TreeView()
		Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
		Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.Panel2.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.Panel4.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.Label5)
		Me.Panel2.Controls.Add(Me.cmbTipoVentaReporte)
		Me.Panel2.Controls.Add(Me.lbUsuario)
		Me.Panel2.Controls.Add(Me.Label10)
		Me.Panel2.Controls.Add(Me.Label11)
		Me.Panel2.Controls.Add(Me.TextBox3)
		Me.Panel2.Controls.Add(Me.txtmonto)
		Me.Panel2.Controls.Add(Me.Label4)
		Me.Panel2.Controls.Add(Me.Button2)
		Me.Panel2.Controls.Add(Me.TextBox2)
		Me.Panel2.Controls.Add(Me.txtcodcliente)
		Me.Panel2.Controls.Add(Me.txtvalormoneda)
		Me.Panel2.Controls.Add(Me.cboxmoneda)
		Me.Panel2.Controls.Add(Me.fechaFinal)
		Me.Panel2.Controls.Add(Me.fechaInicio)
		Me.Panel2.Controls.Add(Me.Button1)
		Me.Panel2.Controls.Add(Me.Label3)
		Me.Panel2.Controls.Add(Me.Label2)
		Me.Panel2.Controls.Add(Me.Label1)
		Me.Panel2.Controls.Add(Me.TextBox1)
		Me.Panel2.Controls.Add(Me.cboxcliente)
		Me.Panel2.Controls.Add(Me.GroupBox1)
		resources.ApplyResources(Me.Panel2, "Panel2")
		Me.Panel2.Name = "Panel2"
		'
		'Label5
		'
		resources.ApplyResources(Me.Label5, "Label5")
		Me.Label5.Name = "Label5"
		'
		'cmbTipoVentaReporte
		'
		Me.cmbTipoVentaReporte.AutoCompleteCustomSource.AddRange(New String() {resources.GetString("cmbTipoVentaReporte.AutoCompleteCustomSource"), resources.GetString("cmbTipoVentaReporte.AutoCompleteCustomSource1"), resources.GetString("cmbTipoVentaReporte.AutoCompleteCustomSource2"), resources.GetString("cmbTipoVentaReporte.AutoCompleteCustomSource3")})
		Me.cmbTipoVentaReporte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		resources.ApplyResources(Me.cmbTipoVentaReporte, "cmbTipoVentaReporte")
		Me.cmbTipoVentaReporte.FormattingEnabled = True
		Me.cmbTipoVentaReporte.Items.AddRange(New Object() {resources.GetString("cmbTipoVentaReporte.Items"), resources.GetString("cmbTipoVentaReporte.Items1"), resources.GetString("cmbTipoVentaReporte.Items2"), resources.GetString("cmbTipoVentaReporte.Items3")})
		Me.cmbTipoVentaReporte.Name = "cmbTipoVentaReporte"
		'
		'lbUsuario
		'
		Me.lbUsuario.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
		resources.ApplyResources(Me.lbUsuario, "lbUsuario")
		Me.lbUsuario.ForeColor = System.Drawing.Color.White
		Me.lbUsuario.Name = "lbUsuario"
		'
		'Label10
		'
		resources.ApplyResources(Me.Label10, "Label10")
		Me.Label10.Name = "Label10"
		'
		'Label11
		'
		resources.ApplyResources(Me.Label11, "Label11")
		Me.Label11.Name = "Label11"
		'
		'TextBox3
		'
		Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
		resources.ApplyResources(Me.TextBox3, "TextBox3")
		Me.TextBox3.Name = "TextBox3"
		'
		'txtmonto
		'
		resources.ApplyResources(Me.txtmonto, "txtmonto")
		Me.txtmonto.Name = "txtmonto"
		'
		'Label4
		'
		resources.ApplyResources(Me.Label4, "Label4")
		Me.Label4.Name = "Label4"
		'
		'Button2
		'
		resources.ApplyResources(Me.Button2, "Button2")
		Me.Button2.Image = Global.SIALIBEB.My.Resources.Resources.TL_prog
		Me.Button2.Name = "Button2"
		Me.Button2.UseVisualStyleBackColor = True
		'
		'TextBox2
		'
		Me.TextBox2.BackColor = System.Drawing.SystemColors.Control
		resources.ApplyResources(Me.TextBox2, "TextBox2")
		Me.TextBox2.Name = "TextBox2"
		Me.TextBox2.ReadOnly = True
		'
		'txtcodcliente
		'
		resources.ApplyResources(Me.txtcodcliente, "txtcodcliente")
		Me.txtcodcliente.Name = "txtcodcliente"
		'
		'txtvalormoneda
		'
		resources.ApplyResources(Me.txtvalormoneda, "txtvalormoneda")
		Me.txtvalormoneda.Name = "txtvalormoneda"
		'
		'cboxmoneda
		'
		Me.cboxmoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		resources.ApplyResources(Me.cboxmoneda, "cboxmoneda")
		Me.cboxmoneda.FormattingEnabled = True
		Me.cboxmoneda.Name = "cboxmoneda"
		'
		'fechaFinal
		'
		resources.ApplyResources(Me.fechaFinal, "fechaFinal")
		Me.fechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.fechaFinal.Name = "fechaFinal"
		Me.fechaFinal.Value = New Date(2007, 4, 12, 0, 0, 0, 0)
		'
		'fechaInicio
		'
		resources.ApplyResources(Me.fechaInicio, "fechaInicio")
		Me.fechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.fechaInicio.Name = "fechaInicio"
		Me.fechaInicio.Value = New Date(2013, 10, 16, 0, 0, 0, 0)
		'
		'Button1
		'
		resources.ApplyResources(Me.Button1, "Button1")
		Me.Button1.Image = Global.SIALIBEB.My.Resources.Resources.reporte
		Me.Button1.Name = "Button1"
		Me.Button1.UseVisualStyleBackColor = True
		'
		'Label3
		'
		resources.ApplyResources(Me.Label3, "Label3")
		Me.Label3.Name = "Label3"
		'
		'Label2
		'
		resources.ApplyResources(Me.Label2, "Label2")
		Me.Label2.Name = "Label2"
		'
		'Label1
		'
		resources.ApplyResources(Me.Label1, "Label1")
		Me.Label1.Name = "Label1"
		'
		'TextBox1
		'
		Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
		Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		resources.ApplyResources(Me.TextBox1, "TextBox1")
		Me.TextBox1.Name = "TextBox1"
		Me.TextBox1.ReadOnly = True
		'
		'cboxcliente
		'
		resources.ApplyResources(Me.cboxcliente, "cboxcliente")
		Me.cboxcliente.FormattingEnabled = True
		Me.cboxcliente.Name = "cboxcliente"
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.rbVentaXEmpleado)
		Me.GroupBox1.Controls.Add(Me.cxPqCate)
		Me.GroupBox1.Controls.Add(Me.Panel4)
		Me.GroupBox1.Controls.Add(Me.Panel1)
		Me.GroupBox1.Controls.Add(Me.CheckBoxPeq)
		Me.GroupBox1.Controls.Add(Me.rbVentasxCaja)
		Me.GroupBox1.Controls.Add(Me.RBcateg)
		Me.GroupBox1.Controls.Add(Me.RBCantidad)
		Me.GroupBox1.Controls.Add(Me.opCostoVenSalonero)
		Me.GroupBox1.Controls.Add(Me.porcen)
		Me.GroupBox1.Controls.Add(Me.tarjetas)
		Me.GroupBox1.Controls.Add(Me.RBtnCostoVentas)
		Me.GroupBox1.Controls.Add(Me.rbVentasFormaPago)
		Me.GroupBox1.Controls.Add(Me.rbAnuladas)
		Me.GroupBox1.Controls.Add(Me.rbdetalladascliente)
		Me.GroupBox1.Controls.Add(Me.rbdetalladas)
		resources.ApplyResources(Me.GroupBox1, "GroupBox1")
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.TabStop = False
		'
		'rbVentaXEmpleado
		'
		resources.ApplyResources(Me.rbVentaXEmpleado, "rbVentaXEmpleado")
		Me.rbVentaXEmpleado.Name = "rbVentaXEmpleado"
		Me.rbVentaXEmpleado.UseVisualStyleBackColor = True
		'
		'cxPqCate
		'
		resources.ApplyResources(Me.cxPqCate, "cxPqCate")
		Me.cxPqCate.Name = "cxPqCate"
		Me.cxPqCate.UseVisualStyleBackColor = True
		'
		'Panel4
		'
		Me.Panel4.Controls.Add(Me.cbxTodCate)
		Me.Panel4.Controls.Add(Me.cbxCarCate)
		Me.Panel4.Controls.Add(Me.cbxConCate)
		resources.ApplyResources(Me.Panel4, "Panel4")
		Me.Panel4.Name = "Panel4"
		'
		'cbxTodCate
		'
		resources.ApplyResources(Me.cbxTodCate, "cbxTodCate")
		Me.cbxTodCate.Checked = True
		Me.cbxTodCate.Name = "cbxTodCate"
		Me.cbxTodCate.TabStop = True
		Me.cbxTodCate.UseVisualStyleBackColor = True
		'
		'cbxCarCate
		'
		resources.ApplyResources(Me.cbxCarCate, "cbxCarCate")
		Me.cbxCarCate.Name = "cbxCarCate"
		Me.cbxCarCate.UseVisualStyleBackColor = True
		'
		'cbxConCate
		'
		resources.ApplyResources(Me.cbxConCate, "cbxConCate")
		Me.cbxConCate.Name = "cbxConCate"
		Me.cbxConCate.UseVisualStyleBackColor = True
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cbxTodMenu)
		Me.Panel1.Controls.Add(Me.cbxCarMenu)
		Me.Panel1.Controls.Add(Me.cbxConMenu)
		resources.ApplyResources(Me.Panel1, "Panel1")
		Me.Panel1.Name = "Panel1"
		'
		'cbxTodMenu
		'
		resources.ApplyResources(Me.cbxTodMenu, "cbxTodMenu")
		Me.cbxTodMenu.Checked = True
		Me.cbxTodMenu.Name = "cbxTodMenu"
		Me.cbxTodMenu.TabStop = True
		Me.cbxTodMenu.UseVisualStyleBackColor = True
		'
		'cbxCarMenu
		'
		resources.ApplyResources(Me.cbxCarMenu, "cbxCarMenu")
		Me.cbxCarMenu.Name = "cbxCarMenu"
		Me.cbxCarMenu.UseVisualStyleBackColor = True
		'
		'cbxConMenu
		'
		resources.ApplyResources(Me.cbxConMenu, "cbxConMenu")
		Me.cbxConMenu.Name = "cbxConMenu"
		Me.cbxConMenu.UseVisualStyleBackColor = True
		'
		'CheckBoxPeq
		'
		resources.ApplyResources(Me.CheckBoxPeq, "CheckBoxPeq")
		Me.CheckBoxPeq.Name = "CheckBoxPeq"
		Me.CheckBoxPeq.UseVisualStyleBackColor = True
		'
		'rbVentasxCaja
		'
		resources.ApplyResources(Me.rbVentasxCaja, "rbVentasxCaja")
		Me.rbVentasxCaja.Name = "rbVentasxCaja"
		Me.rbVentasxCaja.TabStop = True
		Me.rbVentasxCaja.UseVisualStyleBackColor = True
		'
		'RBcateg
		'
		resources.ApplyResources(Me.RBcateg, "RBcateg")
		Me.RBcateg.Name = "RBcateg"
		Me.RBcateg.TabStop = True
		Me.RBcateg.UseVisualStyleBackColor = True
		'
		'RBCantidad
		'
		resources.ApplyResources(Me.RBCantidad, "RBCantidad")
		Me.RBCantidad.Name = "RBCantidad"
		Me.RBCantidad.TabStop = True
		Me.RBCantidad.UseVisualStyleBackColor = True
		'
		'opCostoVenSalonero
		'
		resources.ApplyResources(Me.opCostoVenSalonero, "opCostoVenSalonero")
		Me.opCostoVenSalonero.Name = "opCostoVenSalonero"
		Me.opCostoVenSalonero.UseVisualStyleBackColor = True
		'
		'porcen
		'
		resources.ApplyResources(Me.porcen, "porcen")
		Me.porcen.Name = "porcen"
		'
		'tarjetas
		'
		resources.ApplyResources(Me.tarjetas, "tarjetas")
		Me.tarjetas.Name = "tarjetas"
		'
		'RBtnCostoVentas
		'
		resources.ApplyResources(Me.RBtnCostoVentas, "RBtnCostoVentas")
		Me.RBtnCostoVentas.Name = "RBtnCostoVentas"
		Me.RBtnCostoVentas.UseVisualStyleBackColor = True
		'
		'rbVentasFormaPago
		'
		resources.ApplyResources(Me.rbVentasFormaPago, "rbVentasFormaPago")
		Me.rbVentasFormaPago.Name = "rbVentasFormaPago"
		Me.rbVentasFormaPago.UseVisualStyleBackColor = True
		'
		'rbAnuladas
		'
		resources.ApplyResources(Me.rbAnuladas, "rbAnuladas")
		Me.rbAnuladas.Name = "rbAnuladas"
		Me.rbAnuladas.UseVisualStyleBackColor = True
		'
		'rbdetalladascliente
		'
		resources.ApplyResources(Me.rbdetalladascliente, "rbdetalladascliente")
		Me.rbdetalladascliente.Name = "rbdetalladascliente"
		Me.rbdetalladascliente.UseVisualStyleBackColor = True
		'
		'rbdetalladas
		'
		resources.ApplyResources(Me.rbdetalladas, "rbdetalladas")
		Me.rbdetalladas.Checked = True
		Me.rbdetalladas.Name = "rbdetalladas"
		Me.rbdetalladas.TabStop = True
		Me.rbdetalladas.UseVisualStyleBackColor = True
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.CrystalReportViewer1)
		Me.Panel3.Controls.Add(Me.TreeView1)
		resources.ApplyResources(Me.Panel3, "Panel3")
		Me.Panel3.Name = "Panel3"
		'
		'CrystalReportViewer1
		'
		Me.CrystalReportViewer1.ActiveViewIndex = -1
		Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
		resources.ApplyResources(Me.CrystalReportViewer1, "CrystalReportViewer1")
		Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
		Me.CrystalReportViewer1.SelectionFormula = ""
		Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
		'
		'TreeView1
		'
		resources.ApplyResources(Me.TreeView1, "TreeView1")
		Me.TreeView1.Name = "TreeView1"
		'
		'ImageList
		'
		Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList.Images.SetKeyName(0, "")
		Me.ImageList.Images.SetKeyName(1, "images[65].jpg")
		Me.ImageList.Images.SetKeyName(2, "")
		Me.ImageList.Images.SetKeyName(3, "")
		Me.ImageList.Images.SetKeyName(4, "")
		Me.ImageList.Images.SetKeyName(5, "")
		Me.ImageList.Images.SetKeyName(6, "")
		Me.ImageList.Images.SetKeyName(7, "")
		Me.ImageList.Images.SetKeyName(8, "")
		Me.ImageList.Images.SetKeyName(9, "teclado1.gif")
		Me.ImageList.Images.SetKeyName(10, "")
		'
		'ContextMenuStrip1
		'
		Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
		resources.ApplyResources(Me.ContextMenuStrip1, "ContextMenuStrip1")
		'
		'MuestraReportes
		'
		resources.ApplyResources(Me, "$this")
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.Controls.Add(Me.Panel3)
		Me.Controls.Add(Me.Panel2)
		Me.Name = "MuestraReportes"
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.Panel4.ResumeLayout(False)
		Me.Panel4.PerformLayout()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.Panel3.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
	Public WithEvents ImageList As System.Windows.Forms.ImageList
	Friend WithEvents cboxcliente As System.Windows.Forms.ComboBox
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents rbdetalladas As System.Windows.Forms.RadioButton
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
	Friend WithEvents fechaFinal As System.Windows.Forms.DateTimePicker
	Friend WithEvents fechaInicio As System.Windows.Forms.DateTimePicker
	Friend WithEvents Button1 As System.Windows.Forms.Button
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents cboxmoneda As System.Windows.Forms.ComboBox
	Friend WithEvents txtvalormoneda As System.Windows.Forms.TextBox
	Friend WithEvents rbAnuladas As System.Windows.Forms.RadioButton
	Friend WithEvents rbdetalladascliente As System.Windows.Forms.RadioButton
	Friend WithEvents txtcodcliente As System.Windows.Forms.TextBox
	Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
	Friend WithEvents Button2 As System.Windows.Forms.Button
	Friend WithEvents txtmonto As System.Windows.Forms.TextBox
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents lbUsuario As System.Windows.Forms.Label
	Friend WithEvents Label10 As System.Windows.Forms.Label
	Friend WithEvents Label11 As System.Windows.Forms.Label
	Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
	Friend WithEvents rbVentasFormaPago As System.Windows.Forms.RadioButton
	Friend WithEvents RBtnCostoVentas As System.Windows.Forms.RadioButton
	Friend WithEvents tarjetas As System.Windows.Forms.Label
	Friend WithEvents porcen As System.Windows.Forms.TextBox
	Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents opCostoVenSalonero As System.Windows.Forms.RadioButton
	Friend WithEvents RBCantidad As System.Windows.Forms.RadioButton
	Friend WithEvents RBcateg As System.Windows.Forms.RadioButton
	Friend WithEvents rbVentasxCaja As System.Windows.Forms.RadioButton
	Friend WithEvents CheckBoxPeq As System.Windows.Forms.CheckBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cbxTodMenu As System.Windows.Forms.RadioButton
	Friend WithEvents cbxCarMenu As System.Windows.Forms.RadioButton
	Friend WithEvents cbxConMenu As System.Windows.Forms.RadioButton
	Friend WithEvents Panel4 As System.Windows.Forms.Panel
	Friend WithEvents cbxTodCate As System.Windows.Forms.RadioButton
	Friend WithEvents cbxCarCate As System.Windows.Forms.RadioButton
	Friend WithEvents cbxConCate As System.Windows.Forms.RadioButton
	Friend WithEvents cxPqCate As System.Windows.Forms.CheckBox
	Friend WithEvents rbVentaXEmpleado As System.Windows.Forms.RadioButton
	Friend WithEvents CrystalReportViewer1 As Forms.CrystalReportViewer
	Friend WithEvents Label5 As Label
	Friend WithEvents cmbTipoVentaReporte As ComboBox
End Class
